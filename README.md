## What is this repository for? ##

Dec.31.2016 (Ver.1.0.0)

Hyun Lim

* This repository contains sparse wavelet represented numerical relativity code that simulate binary compact object mergers such as black hole and neutron star
* Further, we adapt highly scalable realtime parallelism called [Dendro](https://github.com/paralab/Dendro-5.0/wiki)

## Contents ##

Current repository has different directories that perform different tasks:

* Dendro5 directory contains Dendro version 5 utilities
* em_dendro directory contains BSSN type Maxwell equation codes with Dendro mesh generation functionality. This is not fully compatible with Dendro. We will modify this to work fully.
* bssn directory contains sparse wavelet represented Einstein equation code with BSSN formalism
* ccz4 directory contains sparse wavelet represented Einstein equation code with CCZ4 formalism
* Both BSSN and CCZ4 codes are not finished to merge with Dendro. Also, we use puncture initial data for binary black hole.
* grmhd directory contains relativistic fluid codes. It will be merged with Einstein codes
* miscell directory contains all miscellaneous codes, documents, and scripts etc. Currently, it has RK4 and RKF45 integrator for time evolution

## How do I get set up? ##

To compile, you need different procedures for each directories so far. (We will merge it eventually)

### Build Dendro-5 ###

- To build dendro, you need [PETSc](https://www.mcs.anl.gov/petsc/) version 3.5.4. and [cmake](https://cmake.org/)

- Configure PETSC using ./configure --download-fblaslapack=1
(I assume that you have some MPI implementation already installed. PETSC configure program automatically detect mpicc & mpicxx, and configure PETSC with MPI)

- To build and install PETSC run make all test

- Set environment variables PETSC DIR=<PETSC home directory> & PETSC ARCH=<arch-folder>

- Once you finish to install required package, go to Dendro home directory and create build directory.

- Go to the build directory and execute ccmake .. and configure dendro. You will get a list of option to set.

- After generate configuration, make all to build Dendro

- If you build Dendro successfully, you will get different binary files as several examples

### Build BSSN and CCZ4 codes ###

- To build this, you need [silo](https://wci.llnl.gov/simulation/computer-codes/silo) and [RNPL](http://laplace.physics.ubc.ca/Doc/rnpletal/)

- Once you have all required packages/libs, type make to build it.

- After build, you should find bin files called 'bssn' or 'ccz4'. You also can find 'idbssn' or 'idccz4' as parameter files. You can run using: ./bssn(ccz4) idbssn(ccz4)

- To use cilk, you need to make with make CILK=1 

- Note that grmhd code can be built via exactly same way in bssn(ccz4) codes

### Build em_dendro ###

- If you can build dendro-5 and bssn(ccz4), you have all required packages/libs/implementation you need.

- To use dendro stencil operation in em code, you need to add a dendro include path (e.g. ~/Dendro-5/include) and dendro lib (e.g. ~/Dendro-5/build/libdendro5.a) in your Makefile

- Also, you need to use c++ complier to use dendro utilities

- Once you finish to build it, you will get 'em' as a bin file. Use 'iddipole' as your parameter file

### For Mac User ###

- Because of MacOS use LLVM complier, gcc (or g++) links to clang complier. This is not compatible OpenMp. (You cannot use -fopenmp as the flag). Here is two possible solution.

- You can install gnu complier manually, and use gnu complier as your complier. Then, gnu links OpenMP automatically.

- Mac has own version of OpenMP, called [clang-omp](https://clang-omp.github.io/). We can install this, and link this for using OpenMp in Mac.

- We will update if there will be more problem that related with the version of software you use.

## Who do I talk to? ##

* If you have problems, contact Hyun Lim via hyun.lim@byu.edu or hylim1988@gmail.com
* If you have detail questions about Dendro, please contact Milinda Fernando (milindasf@gmail.com) or Hari Sundar (hari@cs.utah.edu)