#include <stdlib.h>
#include <stdio.h>
#include "hawaii.h"
#include <math.h>

unsigned time_stamp;


void apply_time_integrator(const double t, const double dt)
{
  void(*rk4_helpers[4])(coll_point_t *, const double) =
    {rk4_helper1, rk4_helper2, rk4_helper3, rk4_helper4};

  //const wavelet_trans_t type = primary_set_wavelet;

  for (int gen = 0; gen < n_gen; gen++) {
    //TODO: Be aware that this is commented out because we are not using the
    // wavelet derivative, and so the wavelet coefficients are not needed
    // until grid adaptation. If you are going to do something with the
    // wavelet coefficients in the rhs computation, then add these back.
    //
    //if (gen) {
    //  for (int i = 0; i < npts_in_array; i++) {
    //    coll_point_t *point = &coll_points->array[i];
    //    if (point->level == 1) {
    //      wavelet_trans(point, type, gen, primary_mask);
    //    }
    //  }
    //  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    //    hash_entry_t *ptr = coll_points->hash_table[i];
    //    while (ptr != NULL) {
    //      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
    //        wavelet_trans(ptr->point, type, gen, primary_mask);
    //      }
    //      ptr = ptr->next;
    //    }
    //  }
    //}

    compute_rhs(t, gen); 
    time_stamp++; 

    void(*rk4_helper)(coll_point_t *, const double) = rk4_helpers[gen]; 


    for (int i = 0; i < npts_in_array; i++) {
      coll_point_t *point = &coll_points->array[i]; 
      rk4_helper(point, dt); 

    }
    for (int i = 0; i < HASH_TBL_SIZE; ++i) {
      hash_entry_t *ptr = coll_points->hash_table[i];
      while (ptr != NULL) {
        if (ptr->point->status[CURRENT_STATUS] > nonessential) {
          rk4_helper(ptr->point, dt);
        }
        ptr = ptr->next;
      }
    }
  }
}

void rk4_helper1(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0]; 
  double *u_next = point->u[1]; 
  double *rhs = point->rhs[0]; 
  for (int i = 0; i < n_variab; i++)
    u_next[i] = u_curr[i] + 0.5 * dt * rhs[i];
  point->time_stamp++; 
  //Constraint for forcing psi and gam have zero. 
  //em_constraints(u_next);
}

void rk4_helper2(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0]; 
  double *u_next = point->u[2]; 
  double *rhs = point->rhs[1]; 
#if 0
  printf("-----------------------------------------------------------------\n");
  printf("-----------------------------------------------------------------\n");
  printf("--B E G I N    R K  S T A G E  2  -------------------------------\n");
  printf("-----------------------------------------------------------------\n");
  printf("-----------------------------------------------------------------\n");
#endif

  for (int i = 0; i < n_variab; i++)
    u_next[i] = u_curr[i] + 0.5 * dt * rhs[i];
  point->time_stamp++;
  //em_constraints(u_next);
}

void rk4_helper3(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *u_next = point->u[3];
  double *rhs = point->rhs[2]; 
  for (int i = 0; i < n_variab; i++) 
    u_next[i] = u_curr[i] + dt * rhs[i]; 
  point->time_stamp++; 
  //em_constraints(u_next);
}

void rk4_helper4(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *k1 = point->rhs[0];
  double *k2 = point->rhs[1];
  double *k3 = point->rhs[2];
  double *k4 = point->rhs[3];

  for (int i = 0; i < n_variab; i++) 
    u_curr[i] += dt * (k1[i] + 2.0 * k2[i] + 2.0 * k3[i] + k4[i]) / 6.0;

  point->time_stamp++; 
  //em_constraints(u_curr);
}
