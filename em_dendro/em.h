#ifndef HAWAII_EM_H
#define HAWAII_EM_H

#include "param.h"
#include "types.h"

#define NSTENCIL 7

/* conservative variables */
#define U_AX 0
#define U_AY 1
#define U_AZ 2
#define U_EX 3
#define U_EY 4
#define U_EZ 5
#define U_PSI 6
#define U_GAM 7
#define U_CON 8
#endif 
