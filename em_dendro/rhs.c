#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"

extern double global_time;
extern double global_dt;

// em_rhs {{{
/*---------------------------------------------------------------------------*
 *
 *
 *
 *---------------------------------------------------------------------------*/
void em_rhs(coll_point_t *point, const int gen)
{
  
  //Declare the variables
  double Gam, con;
  double Efield[3], Afield[3]; //Electric field and vector potential

  double Efield_rhs[3], Afield_rhs[3];
  double psi_rhs;
  double Gam_rhs;

  const double fourpi = 4.0*acos(-1.0);
  const double rhoe = 0.0;
  double jcurr[3] = {0.0, 0.0, 0.0};

  double *rhs = point->rhs[gen];


#if 0
  printf(" time_stamp=%d, gen=%d\n",point->time_stamp,gen);
  printf(" idx = (%d, %d, %d), (x,y,z)=( %g, %g, %g)\n",
           point->index.idx[0], point->index.idx[1], point->index.idx[2],
           point->coords.pos[0],point->coords.pos[1],point->coords.pos[2]);
#endif
 

  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = point->index.idx[i];
  }

  double dx[n_dim];
  int h[n_dim];

  for (int dir = x_dir; dir < n_dim; dir++) {
    int closest_level = get_closest_level(point, dir);
    assert(closest_level  >= 1 );
    dx[dir] = L_dim[dir] / ns[dir] / (1 << closest_level);
    h[dir] = 1 << (JJ - closest_level);
  }

 /* 
  * Build the stencil. This is the original choice
  */
  // FIXME: This whole section needs to be rewritten.
  double *u[3][7], *du[3][7], *pos[3][7];
  coll_point_t *stcl[3][7];

  int nnull = 0;
  for (int dir = 0; dir < n_dim; dir++) {
    for (int qq = x_dir; qq < n_dim; qq++) {
      id.idx[qq] = point->index.idx[qq];
    }
    for (int ll = 0; ll < 7; ll++) {
      if ( ll == 3 ) {
        u[dir][ll] = point->u[gen];
        du[dir][ll] = point->du[gen];
        pos[dir][ll] = point->coords.pos;
        stcl[dir][ll] = point;
      } else {
        coll_point_t *cpm=NULL;
        id.idx[dir] = point->index.idx[dir] + (ll-3)*h[dir];
        if (check_index(&id)) {
          cpm = get_coll_point(&id);
          //Stage 1 should have assured that all points exist, so...
          if (cpm == NULL) {
            printf("...trap me...\n");
            printf("     RHS point = (%d, %d, %d)\n",point->index.idx[0],point->index.idx[1],point->index.idx[2]);
            printf("     dir = %d, id.idx = (%d, %d, %d)\n",dir,id.idx[0],id.idx[1],id.idx[2]);
            printf("     level = %d, max_level = %d, ll=%d\n",point->level, max_level,ll);
            printf("     status= %d",point->status[0]);
          }
          assert(cpm != NULL);
  
          u[dir][ll] = cpm->u[gen];
          du[dir][ll] = cpm->du[gen];
          stcl[dir][ll] = cpm;
          //TODO, this should actually be the closest position to the center;
          // this will be corrected below
          pos[dir][ll] = cpm->coords.pos;
        } else {
          u[dir][ll] = NULL;
          du[dir][ll] = NULL;
          pos[dir][ll] = NULL;
          stcl[dir][ll] = NULL;
          nnull++;
        }
      }
    }
  }

  int bi[3] = {0, 0, 0};
  int npts[3] = {7, 7, 7}; 
  int indx[3] = {3, 3, 3};

  if (nnull > 0) {
   /* Apply boundary conditions and return */
    for (int d = 0; d < 3; d++) {
     /* check to see if neighbors don't exist. If so, then we are on boundary */
      if (u[d][2] == NULL || u[d][4]== NULL) {
        sommerfeld_boundary(point, gen);
        return;
      }

      if (u[d][1] == NULL) {
        bi[d]   = 2;
        npts[d] = 5;
        indx[d] = 1;
      }
      else if (u[d][0] == NULL) {
        bi[d]   = 1;
        npts[d] = 6; 
        indx[d] = 2;
      }
      else if (u[d][5] == NULL) {
        bi[d]   = 0;
        npts[d] = 5;
        indx[d] = 3;
      }
      else if (u[d][6] == NULL) {
        bi[d]   = 0;
        npts[d] = 6;
        indx[d] = 3;
      }
    } 
}
if (nnull > 0){
    //* check to see if neighbors don't exist. If so, then we are on boundary *
     for(int dd = 0; dd < 3; dd++) {
      if (du[dd][2] == NULL || du[dd][4]== NULL) {
        sommerfeld_boundary(point, gen);
        return;
      }
/*
      if (du[dd][1] == NULL) {
        bi[dd]   = 2;
        npts[dd] = 5;
        indx[dd] = 1;
      }
      else if (du[dd][0] == NULL) {
        bi[dd]   = 1;
        npts[dd] = 6; 
        indx[dd] = 2;
      }
      else if (du[dd][5] == NULL) {
        bi[dd]   = 0;
        npts[dd] = 5;
        indx[dd] = 3;
      }
      else if (du[dd][6] == NULL) {
        bi[dd]   = 0;
        npts[dd] = 6;
        indx[dd] = 3;
      } */
    } 
  }

  double *upt = point->u[gen];

 /* Define variables */
 double psi = upt[U_PSI];
 Gam = upt[U_GAM];
 con = upt[U_CON];
 Efield[0] = upt[U_EX];
 Efield[1] = upt[U_EY];
 Efield[2] = upt[U_EZ];
 Afield[0] = upt[U_AX];
 Afield[1] = upt[U_AY];
 Afield[2] = upt[U_AZ];

#if 1
  if (fabs(Gam) > 1.0e-10) {
    printf("## Gamma not zero! Gam=%g. gen=%d\n",Gam,gen);
  }
  if (fabs(psi) > 1.0e-10) {
    printf("## psi not zero! psi=%g\n",psi);
  }
#endif

 /* derivatives */
  double d_psi[3], d_Gam[3];
  double dd_Afield[3][3][3], dd_psi[3][3]; 

 /* u1d[7] is used for the derivative variable for 7 stencil */
 double u1d[7];

 //Derivative routines

  for (int d = 0; d < 3; d++) {

    for (int p = 0; p < npts[d]; p++) {
      u1d[p] = u[d][p+bi[d]][U_PSI];
    }
    d_psi[d] = deriv42_x(u1d, dx[d], indx[d], npts[d]);
    dd_psi[d][d] = deriv42_xx(u1d, dx[d], indx[d], npts[d]);

    for (int m = 0; m < 3; m++) {
      for (int p = 0; p < npts[d]; p++) {
        u1d[p] = u[d][p+bi[d]][U_AX+m];
      }
      dd_Afield[d][d][m] = deriv42_xx(u1d, dx[d], indx[d], npts[d]);
    }

    for (int p = 0; p < npts[d]; p++) {
      u1d[p] = u[d][p+bi[d]][U_GAM];
    }
    d_Gam[d] = deriv42_x(u1d, dx[d], indx[d], npts[d]);
}

 /* define current */
  double time = global_time;
  unsigned ts = point->time_stamp;
  int stage = ts % n_gen;
  
  if (stage == 1 || stage == 2) {
    time += 0.5*global_dt;
  }
  else if (stage == 3) {
    time += global_dt;
  }

  double x = point->coords.pos[0];
  double y = point->coords.pos[1];
  double z = point->coords.pos[2];
  double vec[3]={x,y,z};
  double trav=traverse(vec);
  double distance=dist(vec);
  //if(distance<.25
  //   && trav >-2 && trav<2){
  //  printf("vec={%f,%f,%f}\tdist=%f\ttrav=%f\n",vec[0],vec[1],vec[2],distance,trav);
  //}
  if (distance < .25 && trav<=1 && trav>=-1) 
  {// Regi
    //printf("vec={%f,%f,%f}\ttrav=%f\n",vec[0],vec[1],vec[2],trav);
    if ( pars.antenna_poles==1 ){      // monopole
      jcurr[0] = pars.antenna_J0 * sin(pars.antenna_omega*time);
    }
    else if ( pars.antenna_poles==2 ){ // dipole
      jcurr[0] = pars.antenna_J0 * trav * sin(pars.antenna_omega*time);
    }
    else if ( pars.antenna_poles==4 ){ // quadrupole 
      jcurr[0] = pars.antenna_J0 * (trav*trav-0.25) * sin(pars.antenna_omega*time);
    }
    else{
      printf("Unset pole count (%d)\n",pars.antenna_poles);
      jcurr[0] = 0;
      //come up with a clever way of doing interesting poles... 
    }
    //printf("|j|=%f\n",jcurr[0]);
    jcurr[2]=jcurr[0]*unit(2); 
    jcurr[1]=jcurr[0]*unit(1);
    jcurr[0]=jcurr[0]*unit(0);
    //printf("jcurr={%f,%f,%f}\n",jcurr[0],jcurr[1],jcurr[2]);
  }
   
  /* Define rhs variables here */
  psi_rhs = -Gam - fourpi * rhoe;
  Gam_rhs = - fourpi * rhoe - (dd_psi[0][0] + dd_psi[1][1] + dd_psi[2][2]);

  Afield_rhs[0] = - Efield[0] - d_psi[0];
  Afield_rhs[1] = - Efield[1] - d_psi[1];
  Afield_rhs[2] = - Efield[2] - d_psi[2];

  Efield_rhs[0] = - (dd_Afield[0][0][0] + dd_Afield[1][1][0] + dd_Afield[2][2][0])
                  +  d_Gam[0] - fourpi * jcurr[0];
  Efield_rhs[1] = - (dd_Afield[0][0][1] + dd_Afield[1][1][1] + dd_Afield[2][2][1])
                  +  d_Gam[1] - fourpi * jcurr[1];
  Efield_rhs[2] = - (dd_Afield[0][0][2] + dd_Afield[1][1][2] + dd_Afield[2][2][2])
                  +  d_Gam[2] - fourpi * jcurr[2];

  rhs[U_EX] = Efield_rhs[0];
  rhs[U_EY] = Efield_rhs[1];
  rhs[U_EZ] = Efield_rhs[2];

  rhs[U_PSI] = psi_rhs;
  rhs[U_GAM] = Gam_rhs;

  rhs[U_AX] = Afield_rhs[0];
  rhs[U_AY] = Afield_rhs[1];
  rhs[U_AZ] = Afield_rhs[2];

#if 0
  if (fabs(Gam_rhs) > 1.0e-8 || fabs(psi_rhs) > 1.0e-8 ) {
    printf("hi\n");
  }
#endif

#if 0
  if ( point->coords.pos[0] > -7.21 &&  point->coords.pos[0] < -7.19 &&
       point->coords.pos[1] > -7.21 &&  point->coords.pos[1] < -7.19 &&
       point->coords.pos[2] > -7.21 &&  point->coords.pos[2] < -7.19) {

  printf(" x=%g, y=%g, z=%g\n",point->coords.pos[0],point->coords.pos[1],point->coords.pos[2]); 
    printf("gtu[0][0]     = %g\n",gtu[0][0]);
    printf("gtu[1][1]     = %g\n",gtu[1][1]);
    printf("d_Gamt[0][0]  = %g\n",d_Gamt[0][0]);
    printf("d_Gamt[1][0]  = %g\n",d_Gamt[1][0]);
    printf("d_Gamt[1][1]  = %g\n",d_Gamt[1][1]);
    printf("d_Gamt[2][2]  = %g\n",d_Gamt[2][2]);
    printf("Atu[0][0]     = %g\n",Atu[0][0]);
    printf("Atu[1][1]     = %g\n",Atu[1][1]);
    printf("Atu[2][2]     = %g\n",Atu[2][2]);
    printf("Atd_rhs[0][0] = %g\n",Atd_rhs[0][0]);
    printf("Atd_rhs[0][1] = %g\n",Atd_rhs[0][1]);
    printf("Atd_rhs[1][1] = %g\n",Atd_rhs[1][1]);
    printf("Atd_rhs[2][2] = %g\n",Atd_rhs[2][2]);
    printf("Gamt_rhs[0]   = %g\n",Gamt_rhs[0]);
    printf("Gamt_rhs[1]   = %g\n",Gamt_rhs[1]);
    printf("Gamt_rhs[2]   = %g\n",Gamt_rhs[2]);
    exit(1);
  }
#endif
}
// }}}

// sommerfeld_boundary {{{
/*---------------------------------------------------------------------------*
 *
 *
 *
 *---------------------------------------------------------------------------*/
void sommerfeld_boundary(coll_point_t *point, const int gen)
{


  const double x = point->coords.pos[0];
  const double y = point->coords.pos[1];
  const double z = point->coords.pos[2];
  const double inv_r = 1.0 / sqrt(x*x + y*y + z*z);

  double *rhs = point->rhs[gen];
  double *u = point->u[gen];
  double *du = point->du[gen];

  const double f_falloff[8] = { 1.0, 1.0, 1.0, 2.0, 2.0, 2.0, 1.0, 2.0 };
  const double f_asymptotic[8] = { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
                                   0.0, 0.0 };

  for (int i = 0; i < n_variab; i++) {
    rhs[i] = - inv_r * (
                         x * du[i*n_dim + 0]
                       + y * du[i*n_dim + 1]
                       + z * du[i*n_dim + 2]
                       + f_falloff[i]
                           * (   u[i]
                               - f_asymptotic[i] )
                             );
    
  }

#if 0
  /* Some debugging stuff */
  if (y < -11.99 && y > -12.01 && x < -4.3 && x > -4.33 
                               && z > 3.82 && z < 3.89) {
    printf(" x = %g\n",x);
    printf(" y = %g\n",x);
    printf(" z = %g\n",x);
    //printf(" x = %g, y = %g, z = %g\n",x,y,z);
  }
#endif

}
// }}}

// em_constrains {{{
/*---------------------------------------------------------------------------*
 *
 *
 *
 *---------------------------------------------------------------------------*/
void em_constraints(double *upt)
{

  double psi = upt[U_PSI];
  double Gam = upt[U_GAM];
  //Psi and Gam should be zero for this case TODO : Do we need this?

  if( psi != 0.0 ) {
   /* now place the floor on psi */
    upt[U_PSI] = 0.0;
  }

  if ( Gam != 0.0 ) {
    /* now place the floor on Gam*/
    upt[U_GAM] = 0.0;
  }

}
// }}}


// dist {{{
// //WKB: calculated dist from point to the wire.
double dist(double x0[3]){
  //from http://mathworld.wolfram.com/Point-LineDistance3-Dimensional.html
  //d=|(x0-x1)x(x0-x2)|/|x2-x1|
  //d=|(x2-x1)x(x1-x0)|/|x2-x1|
  double x1[3]={pars.antenna_min_x,pars.antenna_min_y,pars.antenna_min_z};
  double x2[3]={pars.antenna_max_x,pars.antenna_max_y,pars.antenna_max_z};
  
  double A[3]={x2[0]-x1[0],x2[1]-x1[1],x2[2]-x1[2]}; //x2-x1
  double B[3]={x1[0]-x0[0],x1[1]-x0[1],x1[2]-x0[2]}; //x1-x0
  //double C[3]={x0[0]-x1[0],x0[1]-x1[1],x0[2]-x1[2]}; //x0-x1
  //double D[3]={x0[0]-x2[0],x0[1]-x2[1],x0[2]-x2[2]}; //x0-x2
  
  double AcrossB[3]={A[1]*B[2]-A[2]*B[1], 
                    -A[0]*B[2]+A[2]*B[0], 
                     A[0]*B[1]-A[1]*B[0]};
  //double CcrossD[3]={C[1]*D[2]-C[2]*D[1], 
  //                  -C[0]*D[2]+C[2]*D[0], 
  //                   C[0]*D[1]-C[1]*D[0]};
  if(magnitude(A)<1e-6) printf("near-zero magnitude in dist\n"); 
  return magnitude(AcrossB)/magnitude(A);
}
// }}}

// Add in dist formula for two vectors. 
        
// magnitude {{{ 
// calculate magnitude of 3-vector 
double magnitude(double vec[3]){
  return sqrt(vec[0]*vec[0]+vec[1]*vec[1]+vec[2]*vec[2]);
}
// }}}


// traversal distance {{{ 
//   calculate distance along antenna
//   will return a value in the range [-1,1] 
//   if on the antenna. 
double traverse(double vec[3]){
  //printf("vec={%f,%f,%f}\t",vec[0],vec[1],vec[2]);
  double x1[3]={pars.antenna_min_x,pars.antenna_min_y,pars.antenna_min_z};
  double x2[3]={pars.antenna_max_x,pars.antenna_max_y,pars.antenna_max_z};
  double x2_x1[3]={x2[0]-x1[0],x2[1]-x1[1],x2[2]-x1[2]}; 
  
  double report=(vec[0]-x1[0])*(x2_x1[0])+
                (vec[1]-x1[1])*(x2_x1[1])+
                (vec[2]-x1[2])*(x2_x1[2]); // dot product
  if(magnitude(x2_x1)<1e-6) printf("near-zero magnitude in traverse\n");
  report=(report/magnitude(x2_x1)
                -magnitude(x2_x1)/2)/magnitude(x2_x1)/.5;
  // WKB-TODO: debug this... 
  return report; 
}
// }}}

// unit {{{
// returns requested index of unit vector in direction of the antenna
double unit(int index){
  double unitv[3];
  double x1[3]={pars.antenna_min_x,pars.antenna_min_y,pars.antenna_min_z};
  double x2[3]={pars.antenna_max_x,pars.antenna_max_y,pars.antenna_max_z};
  //create vector in direction of antenna
  unitv[0]=x2[0]-x1[0];
  unitv[1]=x2[1]-x1[1];
  unitv[2]=x2[2]-x1[2];
  
  //Normalize... 
  double mag=magnitude(unitv); 
  if(mag>1e-8){ 
    unitv[0]/=mag;
    unitv[1]/=mag;
    unitv[2]/=mag;
  }
  else{ //it's the zero vector, and we'd be dividing by zero. 
    printf("near-zero magnitude in unitvector calculation\n");
  }
  //printf("unitv={%f,%f,%f}\n",unitv[0],unitv[1],unitv[2]);
  return unitv[index];
} 
// }}}
