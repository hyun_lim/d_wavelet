#ifndef HAVE_RHS_H
#define HAVE_RHS_H

void em_rhs(coll_point_t *point, const int gen);
void sommerfeld_boundary(coll_point_t *point, const int gen);
void em_constraints(double *upt);
double dist(double x0[3]);
double magnitude(double vec[3]);
double traverse(double vec[3]);
double unit(int index);

#endif
