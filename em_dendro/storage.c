#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include "hawaii.h"

#if 0
//More headers for Dendro
#include "mpi.h" 
#include "TreeNode.h"
#include "sfcSort.h"
#include "octUtils.h"
//#include "interpolate.h" We need this when we get interpolation with Dendro
#endif

int warned = 0;

#if n_dim == 1
const index_t neighbor_offset[2] = { {{-1}}, {{1}} };
#elif n_dim == 2
const index_t neighbor_offset[8] = {
  {{-1, -1}}, {{0, -1}}, {{1, -1}}, {{-1, 0}},
  {{1, 0}}, {{-1, 1}}, {{0, 1}}, {{1, 1}} };
#elif n_dim == 3
const index_t neighbor_offset[26] =
  { {{-1, -1, -1}}, {{0, -1, -1}}, {{1, -1, -1}}, {{-1, 0, -1}}, {{0, 0, -1}},
    {{1, 0, -1}}, {{-1, 1, -1}}, {{0, 1, -1}}, {{1, 1, -1}}, {{-1, -1, 0}},
    {{0, -1, 0}}, {{1, -1, 0}}, {{-1, 0, 0}}, {{1, 0, 0}}, {{-1, 1, 0}},
    {{0, 1, 0}}, {{1, 1, 0}}, {{-1, -1, 1}}, {{0, -1, 1}}, {{1, -1, 1}},
    {{-1, 0, 1}}, {{0, 0, 1}}, {{1, 0, 1}}, {{-1, 1, 1}}, {{0, 1, 1}},
    {{1, 1, 1}} };
#endif

const int base_step_size = 1 << JJ;
const int level_one_step_size = 1 << (JJ - 1);
const int ns[3] = {ns_x, ns_y, ns_z};

static inline uint64_t hash(const uint64_t k) {
  return (k % HASH_TBL_SIZE);
}

//NOTE: in the PERIODIC case, these are open limits; otherwise, the maximum
// index values are allowed.
const int max_index[3] = {ns_x * (1 << JJ),
                          ns_y * (1 << JJ),
                          ns_z * (1 << JJ)};
#ifdef PERIODIC
 #if n_dim == 1
  const int npts_in_array = 2 * ns_x;
 #elif n_dim == 2
  const int npts_in_array = 4 * ns_x * ns_y;
 #else
  const int npts_in_array = 8 * ns_x * ns_y * ns_z;
 #endif
#else //not PERIODIC
const int npts_in_array = (2 * ns_x + 1) * (2 * ns_y + 1) * (2 * ns_z + 1);
#endif

hawaii_storage_t *coll_points = NULL;

#ifdef HPX
static tatas_lock_t lock = SYNC_TATAS_LOCK_INIT;
hpx_hawaii_storage_t hpx_coll_points = {0,{0}};

hpx_action_t _create_full_grids_a = 0;
hpx_action_t _create_full_grids_b = 0;
hpx_action_t _hpx_get_coll_point_local = 0;

hpx_addr_t hash_lookup(hpx_hash_entry_t **table, uint64_t mkey) {
  hpx_addr_t found = HPX_NULL;
  uint64_t hidx = hash(mkey);
  hpx_hash_entry_t *curr = table[hidx];
  while (curr != NULL) {
    if (curr->mkey == mkey) {
      found = curr->point;
      break;
    }
    curr = curr->next;
  }
  return found;
}

int _action_hpx_get_coll_point_local(uint64_t *mkey, size_t size) {
 hpx_addr_t there;
 sync_tatas_acquire(&lock);
 there = hash_lookup(&hpx_coll_points.hash_table[0], *mkey);
 sync_tatas_release(&lock);
 return HPX_THREAD_CONTINUE(there);
}

hpx_addr_t hpx_get_coll_point(const index_t *index) {

  bool stored_in_array = is_array(index);
  if (stored_in_array) {
    hpx_addr_t there = hpx_addr_add(hpx_coll_points.array,
                 index_to_linear(index)*sizeof(coll_point_t),sizeof(coll_point_t));
    return there;
  } else {
    hpx_addr_t there = HPX_NULL;
    uint64_t mkey = morton_key(index);

    coord_t coord = set_coordinate(index);
    double x = coord.pos[x_dir];
    double y = coord.pos[y_dir];
    double pi = 4.0*atan(1.0);
    double theta;
    if ( y >= 0.0 && x > 0.0 ) {
      theta = atan(y/x);
    } else if ( y >= 0.0 && x < 0.0 ) {
      theta = atan(y/x) + pi;
    } else if ( y < 0.0 && x < 0.0 ) {
      theta = atan(y/x) + pi;
    } else if ( y < 0.0 && x > 0.0 ) {
      theta = atan(y/x) + 2.0*pi;
    } else if ( y >= 0.0 && x == 0.0) {
      theta = pi/2.0;
    } else {
      theta = 3.0*pi/2.0;
    }
    double dtheta = 2.0*pi/(HPX_LOCALITIES-1);
    int locality = -1;
    for (int i=0;i<HPX_LOCALITIES-1;i++) {
      if ( theta >= i*dtheta && theta <= (i+1)*dtheta ) {
        locality = i;
        break;
      }
    }
    if ( HPX_LOCALITIES == 1 ) locality = 0;
    assert(locality > -1);
    int e = hpx_call_sync(HPX_THERE(locality), _hpx_get_coll_point_local, &there, sizeof(there), &mkey, sizeof(mkey));
    if (e != HPX_SUCCESS) hpx_abort();
     return there;
  }

  return HPX_NULL;
}

int _create_full_grids_a_action(int *i, size_t size)
{
  hpx_addr_t local = hpx_thread_current_target();
  coll_point_t *point = NULL;
  const int gen = 0;

  if (!hpx_gas_try_pin(local, (void**)&point))
    return HPX_RESEND;

  point->index = linear_to_index(*i);
  point->coords = set_coordinate(&point->index);
  initial_condition(&point->coords, point->u[gen]);
  point->level = 0;
  point->status[CURRENT_STATUS] = essential;
  for (dir_t dir = x_dir; dir < n_dim; dir++) {
    if (point->index.idx[dir] % base_step_size) {
      point->level = 1;
      point->status[CURRENT_STATUS] = neighboring;
      break;
    }
  }
  hpx_gas_unpin(local);
  return HPX_SUCCESS;
}

int _create_full_grids_b_action(void *UNUSED, size_t size)
{
  hpx_addr_t local = hpx_thread_current_target();
  coll_point_t *point = NULL;
  const int gen = 0;

  if (!hpx_gas_try_pin(local, (void**)&point))
    return HPX_RESEND;

  int wavelet_mask[n_variab + n_aux] = {0};
  for (int ivar = 0; ivar < n_variab; ivar++)
    wavelet_mask[ivar] = 1;

  if (point->level == 1) {
    wavelet_trans(point, primary_set_wavelet, gen, wavelet_mask);
    coll_status_t temp = set_point_status(point->wavelet[gen]);
    point->status[CURRENT_STATUS] = superior_status(temp, neighboring);
  }

  hpx_gas_unpin(local);
  return HPX_SUCCESS;
}
#endif // end HPX section

bool is_array(const index_t *index)
{
  bool stored_in_array = true;
  for (int dir = 0; dir < n_dim; dir++)
    stored_in_array &= (index->idx[dir] % level_one_step_size == 0);

  return stored_in_array;
}

int max_level;

uint64_t split(const unsigned k) {
#if n_dim == 1
  uint64_t split = k;
#elif n_dim == 2
  uint64_t split = k & 0xffffffff;
  split = (split | split << 16) & 0xffff0000ffff;
  split = (split | split << 8) & 0xff00ff00ff00ff;
  split = (split | split << 4) & 0xf0f0f0f0f0f0f0f;
  split = (split | split << 2) & 0x3333333333333333;
  split = (split | split << 1) & 0x5555555555555555;
#elif n_dim == 3
  uint64_t split = k & 0x1fffff;
  split = (split | split << 32) & 0x1f00000000ffff;
  split = (split | split << 16) & 0x1f0000ff0000ff;
  split = (split | split << 8)  & 0x100f00f00f00f00f;
  split = (split | split << 4)  & 0x10c30c30c30c30c3;
  split = (split | split << 2)  & 0x1249249249249249;
#endif
  return split;
}

uint64_t morton_key(const index_t *index) {
  uint64_t key = 0;
#if n_dim == 1
  key = index->idx[x_dir];
#elif n_dim == 2
  key |= split(index->idx[x_dir]) | split(index->idx[y_dir]) << 1;
#elif n_dim == 3
  key |= split(index->idx[x_dir]) | split(index->idx[y_dir]) << 1 |
    split(index->idx[z_dir]) << 2;
#endif
  return key;
}

void storage_init(void) {
  coll_points = (hawaii_storage_t*)calloc(1, sizeof(hawaii_storage_t));
  assert(coll_points != NULL);
  coll_points->array = (coll_point_t*)calloc(npts_in_array, sizeof(coll_point_t));
  assert(coll_points->array != NULL);
}

void storage_cleanup(void) {
  free(coll_points->array);
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      hash_entry_t *tmp = ptr->next;
      free(ptr->point);
      free(ptr);
      ptr = tmp;
    }
  }
  free(coll_points);
}

void create_full_grids(void) {
#ifdef HPX
  hpx_addr_t complete = hpx_lco_and_new(npts_in_array);
  // FUTURE IMPROVEMENT: Could use parfor here
  for (int i = 0; i < npts_in_array; i++) {
    hpx_addr_t point = hpx_addr_add(hpx_coll_points.array,i*sizeof(coll_point_t),sizeof(coll_point_t));
    hpx_call(point,_create_full_grids_a,complete,&i,sizeof(i));
  }
  hpx_lco_wait(complete);
  hpx_lco_delete(complete, HPX_NULL);

  complete = hpx_lco_and_new(npts_in_array);
  // FUTURE IMPROVEMENT: Could use parfor here
  for (int i = 0; i < npts_in_array; i++) {
    hpx_addr_t point = hpx_addr_add(hpx_coll_points.array,i*sizeof(coll_point_t),sizeof(coll_point_t));
    hpx_call(point,_create_full_grids_b,complete,NULL,0);
  }
  hpx_lco_wait(complete);
  hpx_lco_delete(complete, HPX_NULL);
#else
  assert(coll_points != NULL);
  max_level = 1;
  const int gen = 0;

  int wavelet_mask[n_variab + n_aux] = {0};
  for (int ivar = 0; ivar < n_variab; ivar++)
    wavelet_mask[ivar] = 1;

  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    point->index = linear_to_index(i);
    point->coords = set_coordinate(&point->index);
    initial_condition(&point->coords, point->u[gen]);
    point->level = 0;
    point->status[CURRENT_STATUS] = essential;
    for (int dir = x_dir; dir < n_dim; dir++) { //TODO : Check dir_t dir typecast
      if (point->index.idx[dir] % base_step_size) {
        point->level = 1;
        point->status[CURRENT_STATUS] = neighboring;
        break;
      }
    }
  }

  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 1) {
      wavelet_trans(point, primary_set_wavelet, gen, wavelet_mask);
      coll_status_t temp = set_point_status(point->wavelet[gen]);
      point->status[CURRENT_STATUS] = superior_status(temp, neighboring);
    }
  }
#endif
}

void create_adap_grids(void) {
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 1 && point->status[CURRENT_STATUS] == essential)
      create_neighboring_point(point);
  }
}


//Stuff for WENO interpolation

double weno_three_point_interp(double f0, double f1, double f2) {
  static double weights[3] = {-1.0 / 8.0, 3.0 / 4.0, 3.0 / 8.0};
  return (weights[0] * f0 + weights[1] * f1 + weights[2] * f2);
}


double weno_three_point_smoothing(double f0, double f1, double f2) {
  static double weights[6] = { 13.0 / 12.0, 16.0 / 3.0, 25.0 / 12.0,
                              -13.0 / 3.0, 13.0 / 6.0, -19.0 / 3.0 };
  double retval = 0.0;
  retval += weights[0] * f0 * f0;
  retval += weights[1] * f1 * f1;
  retval += weights[2] * f2 * f2;
  retval += weights[3] * f0 * f1;
  retval += weights[4] * f0 * f2;
  retval += weights[5] * f1 * f2;
  return retval;
}


double weno_1d_interp3(double *u) {
  double us[2];
  us[0] = weno_three_point_interp(u[0], u[1], u[2]);
  us[1] = weno_three_point_interp(u[3], u[2], u[1]);

  double beta[3];
  beta[0] = weno_three_point_smoothing(u[0], u[1], u[2]);
  beta[1] = weno_three_point_smoothing(u[3], u[2], u[1]);

  const double gamma[2] = {0.5, 0.5};

  const double epsilon = 1.0e-6;
  double omega[2];
  omega[0] = gamma[0] / ((epsilon + beta[0])*(epsilon + beta[0]));
  omega[1] = gamma[1] / ((epsilon + beta[1])*(epsilon + beta[1]));

  double omega_sum = omega[0] + omega[1];
  omega[0] /= omega_sum;
  omega[1] /= omega_sum;

  return omega[0]*us[0] + omega[1]*us[1];
}

/*----------------------------------------------------------------------
 *

 This is the nine point interpolation. This can be used for all four
 sub-stencils, provided the right inputs are provided.  The locations of
 the inputs are shown, and the x indicates the interpolation point.

 f02     f12     f22

 f01     f11     f21
      x
 f00     f10     f20

 To use this for other substencils, just reflect across lines passing though
 the x. See the weno2d_interp3 function for an example.

 *
 *---------------------------------------------------------------------*/
double weno_nine_point_interp(double f00, double f10, double f20,
                         double f01, double f11, double f21,
                         double f02, double f12, double f22) {
  static double weights[9] = { 9.0 / 64.0,
                               18.0 / 64.0,
                               -3.0 / 64.0,
                               18.0 / 64.0,
                               36.0 / 64.0,
                               -6.0 / 64.0,
                               -3.0 / 64.0,
                               -6.0 / 64.0,
                               1.0 / 64.0 };
  double retval = 0.0;
  retval += weights[0] * f00;
  retval += weights[1] * f01;
  retval += weights[2] * f02;
  retval += weights[3] * f10;
  retval += weights[4] * f11;
  retval += weights[5] * f12;
  retval += weights[6] * f20;
  retval += weights[7] * f21;
  retval += weights[8] * f22;
  return retval;
}

/*----------------------------------------------------------------------
 *

 This computes the nine-point smoothing coefficient. These numbers
 are the result of generalizing the method in the 1d case to the 2d
 case. There was some question as to which derivatives to integrate into
 beta, so we tested various combinations until we reproduced the 1d
 case for a function with no x or no y dependence. This may not be
 the only set of weights that reproduce that behaviour, but it is
 _one_ set.

 *
 *----------------------------------------------------------------------*/
double weno_nine_point_smoothing(double f00, double f10, double f20,
                            double f01, double f11, double f21,
                            double f02, double f12, double f22) {
  static double weights[9][9] = {
    {    331.0 / 72.0, -1561.0 / 240.0,  451.0 / 240.0,
      -1561.0 / 240.0,  -767.0 / 180.0, 1771.0 / 720.0,
        451.0 / 240.0,  1771.0 / 720.0, -217.0 / 360.0 },
    {             0.0,    172.0 / 45.0, -487.0 / 240.0,
        883.0 / 180.0,            -1.0, -143.0 / 180.0,
      -1229.0 / 720.0,    -13.0 / 15.0,  253.0 / 720.0 },
    {             0.0,             0.0,  229.0 / 720.0,
      -1229.0 / 720.0,    67.0 / 180.0,   47.0 / 240.0,
        203.0 / 360.0,   133.0 / 720.0,  -23.0 / 240.0 },
    {             0.0,             0.0,            0.0,
         172.0 / 45.0,            -1.0,   -13.0 / 15.0,
       -487.0 / 240.0,  -143.0 / 180.0,  253.0 / 720.0 },
    {             0.0,             0.0,            0.0,
                  0.0,    316.0 / 45.0,    -23.0 / 5.0,
         67.0 / 180.0,     -23.0 / 5.0,  121.0 / 180.0 },
    {             0.0,             0.0,            0.0,
                  0.0,             0.0,    43.0 / 45.0,
        133.0 / 720.0,   331.0 / 180.0,  -79.0 / 240.0 },
    {             0.0,             0.0,            0.0,
                  0.0,             0.0,            0.0,
        229.0 / 720.0,    47.0 / 240.0,  -23.0 / 240.0 },
    {             0.0,             0.0,            0.0,
                  0.0,             0.0,            0.0,
                  0.0,     43.0 / 45.0,  -79.0 / 240.0 },
    {             0.0,             0.0,            0.0,
                  0.0,             0.0,            0.0,
                  0.0,            0.0,     7.0 / 180.0 }  };
  double fields[9] = {f00, f10, f20, f01, f11, f21, f02, f12, f22};
  double retval = 0.0;
  for (int i = 0; i < 9; ++i) {
    for (int j = i; j < 9; ++j) {
      retval += fields[i] * weights[i][j] * fields[j];
    }
  }
  return retval;
}

/*----------------------------------------------------------------------
 *

 This interpolation is from a sixteen point stencil. For simplicity, we shall
 label the points with two indices, one for x and one for y, each starting at
 zero and growing to 3. So f00 is the bottom left corner. We are trying to
 interpolate to f3/2,3/2. The first index is the x index.

 This stencil is broken into 4 sub-stencils of nine points each. They each
 leave out one corner. For definiteness, the sub-stencils are labeled in the
 following order: top right, top left, bottom right, bottom left. So the point
 f00 only appears in the last stencil. The stencils are typically labeled with
 k, so the first is stencil 0 and the last stencil 3.

 The nine point interpolation is the following for stencil 0:

 f(3/2,3/2) = 1/64 * [ 9 f11 + 18 f12 - 3 f13 + 18 f20 + 36 f22 - 6 f23
                       -3 f31 - 6 f32 + f33 ]

 The other interpolations can be made from this with relabling of the x and y
 indices.

 The C_k factors are all 1/4.

 The beta factor for the same nine points listed above is viewable in
 nine_point_smoothing above

 The input to this function should be in the order

 f00 f10 f20 f30 f01 f11 f21 f31 f02 f12 f22 f23 f03 f13 f23 f33

 *
 *----------------------------------------------------------------------*/
double weno_2d_interp3(double *u) {
  //create aliases for the fields
  double f00 = u[0];
  double f10 = u[1];
  double f20 = u[2];
  double f30 = u[3];
  double f01 = u[4];
  double f11 = u[5];
  double f21 = u[6];
  double f31 = u[7];
  double f02 = u[8];
  double f12 = u[9];
  double f22 = u[10];
  double f32 = u[11];
  double f03 = u[12];
  double f13 = u[13];
  double f23 = u[14];
  double f33 = u[15];

  //comppute the four nine-point interpolations
  double us[4];
  us[0] = weno_nine_point_interp(f11, f21, f31, f12, f22, f32, f13, f23, f33);
  us[1] = weno_nine_point_interp(f21, f11, f01, f22, f12, f02, f23, f13, f03);
  us[2] = weno_nine_point_interp(f12, f22, f32, f11, f21, f31, f10, f20, f30);
  us[3] = weno_nine_point_interp(f22, f12, f02, f21, f11, f01, f20, f10, f00);

  //compute the smoothing factors
  double beta[4];
  beta[0] =
        weno_nine_point_smoothing(f11, f21, f31, f12, f22, f32, f13, f23, f33);
  beta[1] =
        weno_nine_point_smoothing(f21, f11, f01, f22, f12, f02, f23, f13, f03);
  beta[2] =
        weno_nine_point_smoothing(f12, f22, f32, f11, f21, f31, f10, f20, f30);
  beta[3] =
        weno_nine_point_smoothing(f22, f12, f02, f21, f11, f01, f20, f10, f00);

  //compute the weights
  const double gamma[4] = {0.25, 0.25, 0.25, 0.25};
  const double regular = 1.0e-6;
  double omega[4];
  omega[0] = gamma[0] / ((beta[0] + regular) * (beta[0] + regular));
  omega[1] = gamma[1] / ((beta[1] + regular) * (beta[1] + regular));
  omega[2] = gamma[2] / ((beta[2] + regular) * (beta[2] + regular));
  omega[3] = gamma[3] / ((beta[3] + regular) * (beta[3] + regular));

  double omega_sum = omega[0] + omega[1] + omega[2] + omega[3];
  omega[0] /= omega_sum;
  omega[1] /= omega_sum;
  omega[2] /= omega_sum;
  omega[3] /= omega_sum;

  //finally, form the approximation
  double retval = 0.0;
  retval += omega[0] * us[0];
  retval += omega[1] * us[1];
  retval += omega[2] * us[2];
  retval += omega[3] * us[3];

  return retval;
}


void weno_interp3(coll_point_t *point, double *approx, int gen) {
  int h = 1 << (JJ - point->level);      //spacing at the level of the point
  int h2 = 2 * h;                        //spacing at the next coarser level

  //how many directions should we interpolate?
  int interp_count = 0;
  int dirs[n_dim];
  for (int idir = 0; idir < n_dim; ++idir) {
    dirs[idir] = -1;
    if (point->index.idx[idir] % h2) {
      dirs[interp_count] = idir;
      ++interp_count;
    }
  }

  if (interp_count == 1) {
    //collect fields
    double u[4][n_variab + n_aux];
    for (int stcl = 0; stcl < 4; ++stcl) {
      index_t index = point->index;
      index.idx[dirs[0]] += h * (2 * stcl - 3);
      if (check_index(&index)) {
        coll_point_t *stcl_point = get_coll_point(&index);
        for (int ivar = 0; ivar < n_variab; ++ivar) {
          u[stcl][ivar] = stcl_point->u[gen][ivar];
        }
      }
      else {
        ext_func(&index, primary_mask, gen, u[stcl]);
      }
    }

    //do interpolation
    double slice[4];
    for (int ivar = 0; ivar < n_variab; ++ivar) {
      for (int i = 0; i < 4; ++i) {
        slice[i] = u[i][ivar];
      }
      approx[ivar] = weno_1d_interp3(slice);
    }
  }
#if n_dim > 1
  else if (interp_count == 2) {
    //collect fields
    double u[16][n_variab + n_aux];
    for (int loopa = 0; loopa < 4; ++loopa) {
      index_t indexa = point->index;
      indexa.idx[dirs[0]] += h * (2 * loopa - 3);
      for (int loopb = 0; loopb < 4; ++loopb) {
        index_t indexb = indexa;
        indexb.idx[dirs[1]] += h * (2 * loopb - 3);

        //NOTE that the WENO interpolation is invariant under an x->y exchange
        // so the exact x,y ordering of the points in u is not needed.
        int offset = loopa + 4 * loopb;

        if (check_index(&indexb)) {
          coll_point_t *stcl_point = get_coll_point(&indexb);
          for (int ivar = 0; ivar < n_variab; ++ivar) {
            u[offset][ivar] = stcl_point->u[gen][ivar];
          }
        }
        else {
          ext_func(&indexb, primary_mask, gen, u[offset]);
        }
      }
    }

    //do the interpolation
    double slice[16];
    for (int ivar = 0; ivar < n_variab; ++ivar) {
      for (int i = 0; i < 16; ++i) {
        slice[i] = u[i][ivar];
      }
      approx[ivar] = weno_2d_interp3(slice);
    }
  }
#endif
  else {
    assert(0 && "Problem identifying interpolation direction for WENO");
  }
}


void test_weno_interpolation(coll_point_t *point, int gen) {
  //We only have this worked out for up to two dimensions
  assert(n_dim <= 2);

  //first do the standard wavelet computation
  wavelet_trans(point, primary_set_approx, gen, primary_mask);
  double u_lag[n_variab] = {0};
  for (int ivar = 0; ivar < n_variab; ++ivar) {
    u_lag[ivar] = point->u[gen][ivar];
  }

  //then do the weno computation
  double u_weno[n_variab] = {0};
  weno_interp3(point, u_weno, gen);

  //coll_status_t stat = nonessential;
  for (int ivar = 0; ivar < n_variab; ++ivar) {
    point->u[gen][ivar] = u_weno[ivar];
    point->wavelet[gen][ivar] = u_weno[ivar] - u_lag[ivar];
  }
}


void create_neighboring_point(coll_point_t *essen_point) {
  int wavelet_mask[n_variab + n_aux] = {0};
  for (int ivar = 0; ivar < n_variab; ivar++)
    wavelet_mask[ivar] = 1;

  const int gen = 0;
  const unsigned initial_stamp = 0;

  if ( essen_point->level >= JJ ) {
    if ( warned == 0 ) {
      printf(" WARNING:  JJ is insufficient to adequately resolve the system.\n");
      printf("           However, we are running anyway.  Good luck!\n");
      warned = 1;
    }
    max_level = fmax(max_level, essen_point->level);
    return;
  }

  if ( JJ != 1 ) assert(essen_point->level < JJ);

  int level = essen_point->level;
  int step_size_essen = 1 << (JJ - level);
  int step_size_neighbor = step_size_essen / 2;

  for (int i = 0; i < n_neighbors; i++) {
    index_t index = add_index(&essen_point->index, &neighbor_offset[i],
                              step_size_neighbor);
    if (check_index(&index)) { // Neighbor point's index is within range
      int flag;
      coll_point_t *neighbor = add_coll_point(&index, &flag);
      if (flag == 0) { // The point was just created
#ifdef PERIODIC
        neighbor->index = map_index_into_grid(&index);
#else
        neighbor->index = index;
#endif
        neighbor->coords = set_coordinate(&neighbor->index);
        neighbor->level = essen_point->level + 1;
        neighbor->time_stamp = time_stamp;
        check_wavelet_stencil(neighbor, gen);

        if (time_stamp == initial_stamp) {
          neighbor->status[CURRENT_STATUS] = neighboring;
          initial_condition(&neighbor->coords, neighbor->u[gen]);
          wavelet_trans(neighbor, primary_set_wavelet, gen, wavelet_mask);
          coll_status_t temp = set_point_status(neighbor->wavelet[gen]);
          neighbor->status[CURRENT_STATUS] = superior_status(temp, neighboring);

          if (neighbor->status[CURRENT_STATUS] == essential) {
            create_neighboring_point(neighbor);
          } else { // Update max_level when local refinement terminates
            max_level = fmax(max_level, neighbor->level);
          }
        } else {
          neighbor->status[1] = neighboring;
#ifdef WENO
          test_weno_interpolation(neighbor, gen);
#else
          wavelet_trans(neighbor, primary_set_approx, gen, wavelet_mask);
#endif
          max_level = fmax(max_level, neighbor->level);
        }
      } else { // The point already exists in the data store
        if (time_stamp == initial_stamp) {
          // This branch occurs when the point was previously created as an
          // nonessential point in the grid construction process. Now we need to
          // promote its status to neighboring and further checks if it can be
          // an essential point
          if (neighbor->status[CURRENT_STATUS] == nonessential) {
            neighbor->status[CURRENT_STATUS] = neighboring;
            wavelet_trans(neighbor, primary_set_wavelet, gen, wavelet_mask);
            coll_status_t temp = set_point_status(neighbor->wavelet[gen]);
            neighbor->status[CURRENT_STATUS] = superior_status(temp,
                                                               neighboring);
            if (neighbor->status[CURRENT_STATUS] == essential) {
              create_neighboring_point(neighbor);
            } else {
              max_level = fmax(max_level, neighbor->level);
            }
          }
        } else {
          // This branch occurs when a neighboring point becomes an essential
          // point. The neighboring points it tries to add already exist in the
          // grid.
          if (neighbor->status[CURRENT_STATUS] == nonessential) {
            // The point exists in the grid as a nonessential point

            // Promote the status
            neighbor->status[FUTURE_STATUS] = neighboring;

            // Check wavelet and derivative
            check_wavelet_stencil(neighbor, gen);

            // Set neighboring point's value
#ifdef WENO
            test_weno_interpolation(neighbor, gen);
#else
            wavelet_trans(neighbor, primary_set_approx, gen, wavelet_mask);
#endif
            // Update time stamp of the point
            neighbor->time_stamp = time_stamp;
          } else if (neighbor->status[CURRENT_STATUS] == neighboring) {
            // make sure the future status of this neighboring point is at least
            // neighboring
            neighbor->status[FUTURE_STATUS] =
              superior_status(neighboring, neighbor->status[FUTURE_STATUS]);
          }
        }
      }
    } // Move on to the next neighboring point
  } // End of i
}

void create_nonessential_point(coll_point_t *nonessen_point,
                               const index_t *index) {
  const int gen = 0;
  const unsigned initial_stamp = 0;

#ifdef PERIODIC
  index_t mapped_index = map_index_into_grid(index);
#else
  index_t mapped_index = *index;
#endif

  int level = get_level(&mapped_index);
  assert(level > 1);

  nonessen_point->level = level;
  nonessen_point->index = mapped_index;
  nonessen_point->coords = set_coordinate(&mapped_index);
  nonessen_point->time_stamp = time_stamp;
  check_wavelet_stencil(nonessen_point, gen);

  if (time_stamp == initial_stamp) {
    initial_condition(&nonessen_point->coords, nonessen_point->u[gen]);
  } else {
#ifdef WENO
    test_weno_interpolation(nonessen_point, gen);
#else
    wavelet_trans(nonessen_point, primary_set_approx, gen, primitive_mask);
#endif
  }

  nonessen_point->status[CURRENT_STATUS] = nonessential;
}

coll_point_t *get_coll_point(const index_t *index) {
#ifdef PERIODIC
  index_t mapped_index = map_index_into_grid(index);
#endif

  coll_point_t *retval = NULL;
  bool stored_in_array = true;
  for (int dir = x_dir; dir < n_dim; dir++) { //Check typecast dir_t dir
#ifdef PERIODIC
    stored_in_array &= (mapped_index.idx[dir] % level_one_step_size == 0);
#else
    stored_in_array &= (index->idx[dir] % level_one_step_size == 0);
#endif
  }

  if (stored_in_array) {
#ifdef PERIODIC
    retval = &coll_points->array[index_to_linear(&mapped_index)];
#else
    retval = &coll_points->array[index_to_linear(index)];
#endif
  } else {
#ifdef PERIODIC
    uint64_t mkey = morton_key(&mapped_index);
#else
    uint64_t mkey = morton_key(index);
#endif
    uint64_t hidx = hash(mkey);
    hash_entry_t *curr = coll_points->hash_table[hidx];
    while (curr != NULL) {
      if (curr->mkey == mkey) {
        retval = curr->point;
        break;
      }
      curr = curr->next;
    }
  }

  return retval;
}

coll_point_t *add_coll_point(const index_t *index, int *flag) {
  coll_point_t *retval = get_coll_point(index);

  if (retval != NULL) {
    *flag = 1; // the point already exists
  } else {
    *flag = 0; // the point does not exist
#ifdef PERIODIC
    index_t mapped_index = map_index_into_grid(index);
    uint64_t mkey = morton_key(&mapped_index);
#else
    uint64_t mkey = morton_key(index);
#endif
    uint64_t hidx = hash(mkey);
    hash_entry_t *h_entry = (hash_entry_t*)calloc(1, sizeof(hash_entry_t));
    retval = (coll_point_t*)calloc(1, sizeof(coll_point_t));
    assert(h_entry != NULL);
    assert(retval != NULL);
    h_entry->point = retval;
    h_entry->mkey = mkey;
    h_entry->next = coll_points->hash_table[hidx];
    coll_points->hash_table[hidx] = h_entry;
  }

  return retval;
}


void update_all_statuses(void) {
  for (int i = 0; i < npts_in_array; ++i) {
    if (coll_points->array[i].status[FUTURE_STATUS] != uninitialized) {
      coll_points->array[i].status[CURRENT_STATUS] =
                        coll_points->array[i].status[FUTURE_STATUS];
      coll_points->array[i].status[FUTURE_STATUS] = uninitialized;
    }
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *curr = coll_points->hash_table[i];
    while (curr != NULL) {
      if (curr->point->status[FUTURE_STATUS] != uninitialized) {
        curr->point->status[CURRENT_STATUS] =
                        curr->point->status[FUTURE_STATUS];
        curr->point->status[FUTURE_STATUS] = uninitialized;
      }
      curr = curr->next;
    }
  }
}


//TODO: Factor more - give each section a meaningful name
void adjust_grids(void) {
  const int gen = 0;

  //TODO: Here is where we would implement the ask N times demotion scheme
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 0) {
      point->status[FUTURE_STATUS] = essential;
    } else {
      wavelet_trans(point, primary_set_wavelet, gen, primary_mask);
      coll_status_t temp = set_point_status(point->wavelet[gen]);
      point->status[FUTURE_STATUS] = superior_status(temp, neighboring);
    }
  }
  //This should not add points, but it may on occasion update a nonessential
  // point to what it's current value ought to be.
  //TODO: We could perhaps cilkify this. Look at comments in the
  // check_wavelet_stencil implementation
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        check_wavelet_stencil(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        //do we need check_wavelet_stencil anywhere?
        wavelet_trans(ptr->point, primary_set_wavelet, gen, primary_mask);
        ptr->point->status[FUTURE_STATUS] =
              set_point_status(ptr->point->wavelet[gen]);
      }
      ptr = ptr->next;
    }
  }




  // We look at points that are becoming essential, and add neighbors to those
  // points.
  //TODO: CILKify? This would be difficult as well, so for now we leave it
  // as serial. Perhaps with a better concurrent data store we could make
  // this happen.
  for (int i = 0; i < npts_in_array; ++i) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 1
            && point->status[CURRENT_STATUS] != essential
            && point->status[FUTURE_STATUS] == essential) {
      create_neighboring_point(point);
    }
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] != essential
              && ptr->point->status[FUTURE_STATUS] == essential) {
        create_neighboring_point(ptr->point);
      }
      ptr = ptr->next;
    }
  }

  //make sure essential points have neighboring neighbors
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 0) continue;

    if (point->level < JJ && point->status[FUTURE_STATUS] == essential) {
      status_validate_down_helper(point);
    }
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->level < JJ
              && ptr->point->status[FUTURE_STATUS] == essential) {
        status_validate_down_helper(ptr->point);
      }
      ptr = ptr->next;
    }
  }

  update_all_statuses();

  //Now we perform a stage to make sure that each active point has a full
  // HLLE stencil.
  //TODO: CILKify? This is hard to protect against race conditions, so for the
  // time being, we leave it.
  complete_deriv_stencils();

  //now we perform a stage to make sure that each nonessential point in
  // a stencil on the axis has points from which it can interpolate the
  // primitives. Basically we are completing the stencil for c2p_axis.
}



void prune_grids(void) {
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *head = coll_points->hash_table[i];
    if (head != NULL) {
      hash_entry_t *prev = head;
      hash_entry_t *curr = prev->next;
      // Leave the head of the list for the last
      while (curr != NULL) {
        if (curr->point->time_stamp < time_stamp) {
          hash_entry_t *tmp = curr;
          prev->next = curr->next;
          curr = curr->next;
          free(tmp->point);
          free(tmp);
        } else {
          prev = curr;
          curr = curr->next;
        }
      }
      // Process the head of the list
      if (head->point->time_stamp < time_stamp) {
        hash_entry_t *tmp = head;
        coll_points->hash_table[i] = head->next;
        free(tmp->point);
        free(tmp);
      }
    }
  }
}


// THis counts the number of points in the grid which have the given status
// or better at the given timestamp.
int count_points(coll_status_t minstat, unsigned stamp) {
  int retval = 0;
  if (minstat == essential) {
    for (int i = 0; i < npts_in_array; ++i) {
      if (coll_points->array[i].status[CURRENT_STATUS] >= minstat
                    && coll_points->array[i].time_stamp == stamp) {
        ++retval;
      }
    }
  } else {
    retval += npts_in_array;
  }

  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->time_stamp == time_stamp &&
          ptr->point->status[CURRENT_STATUS] >= minstat) {
        ++retval;
      }
      ptr = ptr->next;
    }
  }

  return retval;
}


#ifdef PERIODIC
index_t map_index_into_grid(const index_t *in) {
  index_t retval = *in;
  for (dir_t dir = x_dir; dir < n_dim; ++dir) {
    while (retval.idx[dir] < 0) {
      retval.idx[dir] += max_index[dir];
    }
    while (retval.idx[dir] >= max_index[dir]) {
      retval.idx[dir] -= max_index[dir];
    }
  }
  return retval;
}
#endif
