#ifndef HAWAII_PARAM_H
#define HAWAII_PARAM_H
#include <stdbool.h>


#define ns_x 15  // number of points in the x-direction of the base grid
#define ns_y 15  // number of points in the y-direction of the base grid
#define ns_z 15  // number of points in the z-direction of the base grid
#define JJ    3  // maximum number of resolution levels to use
#define n_dim 3  // dimensionality of the problem

#define n_gen 4

#define n_variab 8 // number of primary variables
#define n_aux    1 // number of auxiliary variables
#define n_deriv  18 // number of derivatives needed in the right-hand side
#define n_rhs    8 // number of right-hand side functions

extern double t0; // initial time stamp of the simulation
extern double tf; // final time stamp of the simulation

//These set up common masking patterns once; they are set in main();
extern int primary_mask[n_variab + n_aux];
extern int primitive_mask[n_variab + n_aux];
extern int auxiliary_mask[n_variab + n_aux];
extern int allvars_mask[n_variab + n_aux];
#endif
