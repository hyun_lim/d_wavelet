#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdbool.h>
#include <limits.h>
#include <assert.h>
#include <float.h>
#include "hawaii.h"

double eps[n_variab];
double eps_scale[n_variab];
double L_dim[n_dim];
const int deriv_offset[6] = {-3, -2, -1, 1, 2, 3};

//This is used in the output routine. We had to make this global so that
// we can correctly output after a restart.
static int filenum_ = 0;


int floatcmp(double a,double b) {
  double epsilon = 1.e-8;
  if ( a < b + epsilon && a > b - epsilon ) return 1;
  else return 0;
}

// quickSort {{{
int cmpvertices(const void *vpa, const void *vpb)
{
  const vizpoint *va = (const vizpoint*)vpa;
  const vizpoint *vb = (const vizpoint*)vpb;

  int diff = (va->z > vb->z) - (va->z < vb->z);
  if(0 == diff) /* primary keys the same? */
  {
    diff = (va->y > vb->y) - (va->y < vb->y);
  }
  if(0 == diff) /* secondary keys the same? */
  {
    diff = (va->x > vb->x) - (va->x < vb->x);
  }
  return diff;
}
// }}}


void validate_parameters(void) {
  const int max_direction = 3;
  //TODO : Check typecast dir_t dir in below looping
  for (int dir = x_dir; dir < n_dim; dir++)
    assert(ns[dir] != 0);

  for (int dir = n_dim + 1; dir < max_direction; dir++)
    assert(ns[dir] == 0);

  // The index in each direction takes (JJ + log_2(ns)) bits to represent and
  // this should not exceed 21. Otherwise, the current hashing function does not
  // work.
  const int max_bits_per_direction = 21;
  const char direction[3] = {'x', 'y', 'z'};
  for (int dir = x_dir; dir < n_dim; dir++) {
    if (ceil(log2(ns[dir])) + JJ > max_bits_per_direction) {
      fprintf(stderr,
              "index in %c direction requires more than 21 bits\n",
              direction[dir]);
      abort();
    }
  }

// RK4
  assert(n_gen == 4);
}

index_t linear_to_index(const int rank) {
  index_t retval;
#if n_dim == 1
  retval.idx[x_dir] = rank * level_one_step_size;
#elif n_dim == 2
 #ifdef PERIODIC
  div_t temp = div(rank, 2 * ns_y);
 #else
  div_t temp = div(rank, 2 * ns_y + 1);
 #endif
  retval.idx[x_dir] = temp.quot * level_one_step_size;
  retval.idx[y_dir] = temp.rem * level_one_step_size;
#elif n_dim == 3
 #ifdef PERIODIC
  div_t temp = div(rank, 4 * ns_y * ns_z);
 #else
  div_t temp = div(rank, (2 * ns_y + 1) * (2 * ns_z + 1));
 #endif
  retval.idx[x_dir] = temp.quot * level_one_step_size;
 #ifdef PERIODIC
  temp = div(temp.rem, 2 * ns_z);
 #else
  temp = div(temp.rem, 2 * ns_z + 1);
 #endif
  retval.idx[y_dir] = temp.quot * level_one_step_size;
  retval.idx[z_dir] = temp.rem * level_one_step_size;
#endif
  return retval;
}

int index_to_linear(const index_t *index) {
  int rank;
#if n_dim == 1
  rank = index->idx[x_dir] / level_one_step_size;
#elif n_dim == 2
  int ix = index->idx[x_dir] / level_one_step_size;
  int iy = index->idx[y_dir] / level_one_step_size;
 #ifdef PERIODIC
  rank = ix * 2 * ns_y + iy;
 #else
  rank = ix * (2 * ns_y + 1) + iy;
 #endif
#elif n_dim == 3
  int ix = index->idx[x_dir] / level_one_step_size;
  int iy = index->idx[y_dir] / level_one_step_size;
  int iz = index->idx[z_dir] / level_one_step_size;
 #ifdef PERIODIC
  rank = ix * 4 * ns_y * ns_z + iy * 2 * ns_z + iz;
 #else
  rank = ix * (2 * ns_y + 1) * (2 * ns_z + 1) +
    iy * (2 * ns_z + 1) + iz;
 #endif
#endif
  return rank;
}

index_t add_index(const index_t *lhs, const index_t *rhs, const int scalar) {
  index_t retval;
  for (int dir = x_dir; dir < n_dim; dir++)
    retval.idx[dir] = lhs->idx[dir] + rhs->idx[dir] * scalar;
#ifdef PERIODIC
  retval = map_index_into_grid(&retval);
#endif
  return retval;
}

bool check_index(const index_t *index) {
#ifdef PERIODIC
  return true;
#else
  bool retval = true;
  for (int dir = x_dir; dir < n_dim; dir++)
    retval &= (index->idx[dir] >= 0) && (index->idx[dir] <= max_index[dir]);
  return retval;
#endif
}

/*----------------------------------------------------------------------
 *
 *  Set coordinates for points.
 *
 *----------------------------------------------------------------------*/
coord_t set_coordinate(const index_t *index) {
  coord_t coord;

  coord.pos[x_dir] = L_dim[x_dir] * index->idx[x_dir] / max_index[x_dir]
                   + pars.xmin;

#if n_dim == 2 || n_dim == 3
  coord.pos[y_dir] = L_dim[y_dir] * index->idx[y_dir] / max_index[y_dir]
                   + pars.ymin;
#endif
#if n_dim == 3
  coord.pos[z_dir] = L_dim[z_dir] * index->idx[z_dir] / max_index[z_dir]
                   + pars.zmin;
#endif

  return coord;
}

double norm2(const coord_t *point1, const coord_t *point2) {
  double dist = 0.0;
  for (int dir = x_dir; dir < n_dim; dir++)
    dist += pow(point1->pos[dir] - point2->pos[dir], 2);
  return sqrt(dist);
}

coll_status_t set_point_status(const double *wavelet) {
  coll_status_t retval = nonessential;
  for (int ivar = 0; ivar < n_variab; ivar++) {
    if (fabs(wavelet[ivar]) >= eps_scale[ivar]) {
      retval = essential;
      break;
    }
  }
  return retval;
}

coll_status_t superior_status(const coll_status_t lhs,
                              const coll_status_t rhs) {
  return (lhs >= rhs ? lhs : rhs);
}

coll_status_t inferior_status(const coll_status_t lhs,
                              const coll_status_t rhs) {
  return (lhs <= rhs ? lhs : rhs);
}

bool has_superior_status(const coll_status_t lhs,
                         const coll_status_t rhs) {
  return (lhs > rhs ? true : false);
}

bool has_inferior_status(const coll_status_t lhs,
                         const coll_status_t rhs) {
  return (lhs < rhs ? true : false);
}

bool has_same_status(const coll_status_t lhs,
                     const coll_status_t rhs) {
  return (lhs == rhs ? true : false);
}

int get_level(const index_t *index) {
  int level = 0;
  for (int dir = x_dir; dir < n_dim; dir++) {
    for (int i = JJ; i >= 0; i--) {
      int step = 1 << i;
      if (index->idx[dir] % step == 0) {
        level = fmax(level, JJ - i);
        break;
      }
    }
  }
  return level;
}

void check_wavelet_stencil(coll_point_t *point, const int gen) {
  // Step size of the point's refinement level
  const int h = 1 << (JJ - point->level);
  const int h2 = 2 * h;

  //how many directions should we interpolate?
  int interp_count = 0;
  int dirs[n_dim];
  for (int idir = 0; idir < n_dim; ++idir) {
    dirs[idir] = -1;
    if (point->index.idx[idir] % h2) {
      dirs[interp_count] = idir;
      ++interp_count;
    }
  }

  //get the starting index for each direction
  int starting[n_dim];
  for (int idir = 0; idir < interp_count; ++idir) {
    starting[idir] = point->index.idx[dirs[idir]] + h * wavelet_offset[0];
#ifndef PERIODIC
    //We only have to modify the above if we are non-periodic
    const int upper_limit = wavelet_offset[3];
    const int lower_limit = wavelet_offset[0];
    const int delta_limit = upper_limit - lower_limit;
    if (starting[idir] < 0) {
      starting[idir] = 0;
    } else if ((starting[idir] + delta_limit * h) > max_index[dirs[idir]]) {
      starting[idir] = max_index[dirs[idir]] - delta_limit * h;
    }
#endif
  }

  //loop over stencil to check for existence
  if (interp_count == 1) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {
      //get index
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      //check point
      int flag;
      //TODO: is it enough to LOCK here...
      coll_point_t *temp = add_coll_point(&stcl, &flag);
      if (!flag) {
        create_nonessential_point(temp, &stcl);
      } else if (temp->time_stamp < time_stamp) {
        temp->status[CURRENT_STATUS] = nonessential;
        temp->status[FUTURE_STATUS] = uninitialized;
        advance_time_stamp(temp, gen);
      }
      //TODO: and unlock here?
      assert(temp->level < point->level);
    }
  }
#if n_dim > 1
  else if (interp_count == 2) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      for (int loopb = 0; loopb < 4; ++loopb) {
        stcl.idx[dirs[1]] = starting[1] + h2 * loopb;

        int flag;
        coll_point_t *temp = add_coll_point(&stcl, &flag);
        if (!flag) {
          create_nonessential_point(temp, &stcl);
        } else if (temp->time_stamp < time_stamp) {
          temp->status[CURRENT_STATUS] = nonessential;
          temp->status[FUTURE_STATUS] = uninitialized;
          advance_time_stamp(temp, gen);
        }
      }
    }
  }
#endif
#if n_dim > 2
  else if (interp_count == 3) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      for (int loopb = 0; loopb < 4; ++loopb) {
        stcl.idx[dirs[1]] = starting[1] + h2 * loopb;
        for (int loopc = 0; loopc < 4; ++loopc) {
          stcl.idx[dirs[2]] = starting[2] + h2 * loopc;

          int flag;
          coll_point_t *temp = add_coll_point(&stcl, &flag);
          if (!flag) {
            create_nonessential_point(temp, &stcl);
          } else if (temp->time_stamp < time_stamp) {
            temp->status[CURRENT_STATUS] = nonessential;
            temp->status[FUTURE_STATUS] = uninitialized;
            advance_time_stamp(temp, gen);
          }
        }
      }
    }
  }
#endif
  else {
    assert(0 && "Problem with checking wavelet stencils");
  }
}

/*----------------------------------------------------------------------
 *
 *  check_derivative_stencil.  
 *           This routine is called at the beginning of the rhs computation
 *           to advance non-essential points.
 *
 *----------------------------------------------------------------------*/
void check_derivative_stencil(coll_point_t *point, const int gen) {
 // int ltrace = 0;

  for (int dir = x_dir; dir < n_dim; dir++) {

    int closest_level = get_closest_level(point, dir);
    assert( closest_level >= 1 );

    const int h = 1 << (JJ - closest_level);

    index_t index = point->index;

    int bi, ci;
    int npts = NSTENCIL;

    int bi2 = fmax(0, index.idx[dir] - 3*h);
    if (index.idx[dir] + 3*h > max_index[dir]) {
      bi2 = max_index[dir] - (NSTENCIL-1)*h;
    }
    assert(bi2 >= 0);
    assert(bi2 + (NSTENCIL-1)*h <= max_index[dir]);


    if (index.idx[dir] - 3*h >= 0 && index.idx[dir] + 3*h <= max_index[dir]) {
      bi = index.idx[dir] - 3*h;
      ci = 3;
    }
    else if (point->index.idx[dir] - 2*h == 0) {
      bi = index.idx[dir] - 2*h;
      ci = 2;
    }
    else if (point->index.idx[dir] - h == 0) {
      bi = index.idx[dir] - h;
      ci = 1;
    }
    else if (point->index.idx[dir] == 0) {
      bi = index.idx[dir];
      ci = 0;
    }
    else if (point->index.idx[dir] + 2*h == max_index[dir]) {
      bi = index.idx[dir] - 4*h;
      ci = 4;
    }
    else if (point->index.idx[dir] + h == max_index[dir]) {
      bi = index.idx[dir] - 5*h;
      ci = 5;
    }
    else if (point->index.idx[dir] == max_index[dir]) {
      bi = index.idx[dir] - 6*h;
      ci = 6;
    }
    else {
      printf("check_derivative_stencil: Stencil problem.\n");
      exit(2);
    }

    assert(bi==bi2);

/*
    if (index.idx[0] == 6 && index.idx[1] == 6 && index.idx[2] == 8) {
      ltrace = 1;
    }
*/

    for (int i = 0; i < npts; i++) {
      index.idx[dir] = bi + i * h;
/*
      if (ltrace) {
        printf("checking point (%d, %d, %d), ",index.idx[0],index.idx[1],index.idx[2]);
      }
*/
      int flag;
      coll_point_t *temp = add_coll_point(&index, &flag);
      if (!flag) {
        create_nonessential_point(temp, &index);
      } else if (temp->time_stamp < time_stamp) {
        temp->status[CURRENT_STATUS] = nonessential;
        temp->status[FUTURE_STATUS] = uninitialized;
        advance_time_stamp(temp, gen);
      }
/*
      if (ltrace)
        printf("flag=%d\n",flag);
*/
    }
  }

/*
  if (ltrace)
    printf("---------------------------\n");
*/
}

/*----------------------------------------------------------------------
 *
 *  advance_time_stamp.  This routine sets a point's values with 
 *                       using interpolation or the wavelet transformation.
 *
 *----------------------------------------------------------------------*/
void advance_time_stamp(coll_point_t *point, const int gen) {
  check_wavelet_stencil(point, gen);
#ifdef WENO
  test_weno_interpolation(point, gen);
#else
  wavelet_trans(point, primary_set_approx, gen, primitive_mask);
#endif
  point->time_stamp = time_stamp;
}

double get_global_dt(void) {
  double global_dt = DBL_MAX;
  double valmin[n_variab];
  double valmax[n_variab];
  for (int ivar = 0; ivar < n_variab; ++ivar) {
    valmin[ivar] = DBL_MAX;
    valmax[ivar] = -DBL_MAX;
  }

  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    double local_dt = get_local_dt(point);
    global_dt = fmin(local_dt, global_dt);

    minmax_range_reducer(point->u[0], valmin, valmax);
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        double local_dt = get_local_dt(ptr->point);
        global_dt = fmin(local_dt, global_dt);
        minmax_range_reducer(ptr->point->u[0], valmin, valmax);
      }
      ptr = ptr->next;
    }
  }

  //now find the ranges, and set epsilon accordingly
  set_eps_scale(valmin, valmax);

  return global_dt;
}


void minmax_range_reducer(double *u, double *mins, double *maxs) {
  for (int ivar = 0; ivar < n_variab; ++ivar) {
    mins[ivar] = fmin(mins[ivar], u[ivar]);
    maxs[ivar] = fmax(maxs[ivar], u[ivar]);
  }
}

void set_eps_scale(double *mins, double *maxs) {
  double range[n_variab];
  for (int i = 0; i < n_variab; ++i) {
    range[i] = maxs[i] - mins[i];

    //we deal with strange ranges by setting them to one
    if (range[i] <= 1.0e-12) {
      range[i] = 1.0;
    }
  }

  for (int i = 0; i < n_variab; ++i) {
    eps_scale[i] = eps[i];
  }
#ifndef NO_SCALED_EPS
  //fprintf(stdout, "ranges: ");
  for (int i = 0; i < n_variab; ++i) {
    eps_scale[i] = eps[i] * range[i];
  //  fprintf(stdout, "%lg ", range[i]);
  }
  //fprintf(stdout, "\n");
#endif

}


void status_validate_down_helper(coll_point_t *point) {
  const int h2 = 1 << (JJ - point->level - 1);

  for (int i = 0; i < n_neighbors; i++) {
    index_t index = add_index(&point->index, &neighbor_offset[i], h2);
    if (check_index(&index)) {
      coll_point_t *neighbor = get_coll_point(&index);
      neighbor->status[FUTURE_STATUS] =
        superior_status(neighbor->status[FUTURE_STATUS], neighboring);
    }
  }
}


//We need this function, so that we can start the loop.
void complete_deriv_stencils() {
  for (int i = 0; i < npts_in_array; ++i) {
    coll_point_t *point = &coll_points->array[i];
    deriv_stencil_completion_helper(point);
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        deriv_stencil_completion_helper(ptr->point);
      }
      ptr = ptr->next;
    }
  }
}


void deriv_stencil_completion_helper(coll_point_t *point) {
  const int gen = 0;

  //First check that this active point's derivative stencil is complete
  for (int dir = x_dir; dir < n_dim; ++dir) {
    int closest_level = get_closest_level(point, dir);
    assert(closest_level >= 1);
    int h = 1 << (JJ - closest_level);

    int bi = fmax(0, point->index.idx[dir] - 3*h);
    if (point->index.idx[dir] + 3*h > max_index[dir]) {
      bi = max_index[dir] - 6*h;
    }
    assert(bi >= 0);
    assert(bi + 6*h <= max_index[dir]);

/*
    if (point->index.idx[0] == 67 && point->index.idx[1] == 98 && point->index.idx[2] == 32) {
      printf("here I am.\n");
    }
*/

    for (int stcl = 0; stcl < NSTENCIL; ++stcl) {
      if (stcl*h + bi == point->index.idx[dir]) {
        continue;
      } else {
        index_t index = point->index;
        index.idx[dir] = bi + stcl*h;
#ifdef PERIODIC
        index = map_index_into_grid(&index);
#endif

        if (check_index(&index)) {
          int flag;
          //TODO: Is it sufficient to LOCK here...
          coll_point_t *stcl_point = add_coll_point(&index, &flag);

          if (!flag) {
            create_nonessential_point(stcl_point, &index);
          } else if (stcl_point->time_stamp < time_stamp) {
            stcl_point->status[CURRENT_STATUS] = nonessential;
            stcl_point->status[FUTURE_STATUS] = uninitialized;
            advance_time_stamp(stcl_point, gen);
          }
          //TODO: and UNLOCK here?
        }
      }
    }
  }
}


void visualize_grid(char *fname, int noness) {
  FILE *fd = fopen(fname, "w");
  assert(fd != NULL);

  const int gen = 0;

  if (!noness) {
    for (int i = 0; i < npts_in_array; i++) {
      coll_point_t *point = &coll_points->array[i];

      for (int dir = x_dir; dir < n_dim; dir++)
        fprintf(fd, "%8.7f %d ", point->coords.pos[dir], point->level);
      for (int ivar = 0; ivar < n_variab; ivar++)
        fprintf(fd, "%15.14e ", point->u[gen][ivar]);
      for (int ivar = n_variab; ivar < n_variab; ivar++)
        fprintf(fd, "%15.14e ", point->u[gen][ivar]);
      for (int ivar = 0; ivar < n_rhs; ivar++)
        fprintf(fd, "%15.14e ", point->rhs[gen][ivar]);
      fprintf(fd, "\n");
    }
  }

  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      coll_point_t *point = ptr->point;
      if ((!noness && point->status[CURRENT_STATUS] != nonessential)
                || (noness && point->status[CURRENT_STATUS] == nonessential)) {
        if (point->time_stamp == time_stamp) {
          for (int dir = x_dir; dir < n_dim; dir++)
            fprintf(fd, "%8.7f %d ", point->coords.pos[dir], point->level);
          for (int ivar = 0; ivar < n_variab; ivar++)
            fprintf(fd, "%15.14e ", point->u[gen][ivar]);
          for (int ivar = n_variab; ivar < n_variab; ivar++)
            fprintf(fd, "%15.14e ", point->u[gen][ivar]);
          for (int ivar = 0; ivar < n_rhs; ivar++)
            fprintf(fd, "%15.14e ", point->rhs[gen][ivar]);
          fprintf(fd, "\n");
        }
      }
      ptr = ptr->next;
    }
  }

  fclose(fd);
}


// index_on_boundary
int index_on_boundary(const index_t index, int dir) {
  assert(dir <= n_dim);

  if(index.idx[dir] == 0) {
    return -1;
  } else if(index.idx[dir] == max_index[dir]) {
    return 1;
  } else {
    return 0;
  }
}

/// Gets the boundary status of an index
void boundary_check(const index_t index, int *bdy_check) {
  for(int dir = 0; dir < n_dim; ++dir) {
    bdy_check[dir] = index_on_boundary(index, dir);
  }
}

//
// get the skip at the supplied level
//
int64_t skip_at_level(int level) {
  assert( level <= JJ );
  return ( (int64_t)1 << (JJ - level) );
}

//
// shift the index by so many units in the given direction at level
//
index_t index_shift(index_t idx, int dir, int delta, int level, int *flag) {
  assert(dir <= n_dim);
  assert(level <= JJ);

  int64_t shift = skip_at_level(level);

  index_t retval;
  for(int i = 0; i < n_dim; ++i) {
    retval.idx[i] = idx.idx[i];
  }
  retval.idx[dir] += delta * shift;
#ifdef PERIODIC
  retval = map_index_into_grid(&retval);
#endif

  int64_t bounds = max_index[dir];

  //check bounds
  if ( retval.idx[dir] <= bounds && retval.idx[dir] >= 0 ) {
    *flag = 0;
  } else {
    *flag = 1;
  }

  return retval;
}


//The difference between get_ and try_closest_level is that try_ can
// fail to find a point. In that case it will return -1, and the caller
// will have to handle this possibility. get_ enforces success with an
// assertion.
int try_closest_level(const coll_point_t *point, const int dir) {
  index_t search_index;
  index_t index = point->index;
  int level = point->level;

  int type = CURRENT_STATUS;

  int bdy_check[n_dim];
  boundary_check(index, bdy_check);

  int flag;

  //The closest active point will be at max_level or lower
  int ll = -1;
  for(ll = max_level; ll >= level; --ll) {
    if(bdy_check[dir] <= 0) {
      //search (+)dir at level ll for a point
      search_index = index_shift(index, dir, 1, ll, &flag);
      coll_point_t *search_point = get_coll_point(&search_index);
      if(search_point != NULL) {
        if(search_point->status[type] == essential ||
                search_point->status[type] == neighboring) {
          break;
        }
      }
    }

    if(bdy_check[dir] >= 0) {
      //search (-)dir at level ll for a point
      search_index = index_shift(index, dir, -1, ll, &flag);
      coll_point_t *search_point = get_coll_point(&search_index);
      if(search_point != NULL) {
        if(search_point->status[type] == essential ||
                search_point->status[type] == neighboring) {
          break;
        }
      }
    }
  }

  //search index is the one we want
  return ll;
}


//NOTE that this is now implemented in terms of try_closest_level because of
// the DRY principle
int get_closest_level(const coll_point_t *point, const int dir) {
  index_t index = point->index;
  int ll = try_closest_level(point, dir);

#ifndef SKIPTWOAWAYCHECK
  //check if the point two away is essential
  int increment = 0;
  //we only need to check the neighboring points, as the essential points
  //will definitely have found the point closest that will generate a stencil
  //that does not skip over an active point.
  if (point->status[CURRENT_STATUS] == neighboring) {
    int flag;
    index_t search_index = index_shift(index, dir, 2, ll, &flag);
    if (!flag) {
      coll_point_t *search_point = get_coll_point(&search_index);
      if (search_point != NULL
            && search_point->status[CURRENT_STATUS] == essential) {
        //The point two units away is essential, which means there is another
        // active point between the outer two stencil points for this point in
        // this direction. So we actually want to increase the level so that
        // the outermost stencil point is at the point we found; we increase
        // the level by 1.
        increment = 1;
      }
    }
    search_index = index_shift(index, dir, -2, ll, &flag);
    if (!flag) {
      coll_point_t *search_point = get_coll_point(&search_index);
      if (search_point != NULL
            && search_point->status[CURRENT_STATUS] == essential) {
        increment = 1;
      }
    }
  }

  ll += increment;
  if (ll > JJ) {
    ll = JJ;
  }
#endif

  //search index is the one we want
  if (ll < point->level) {
    ll = point->level;
  }
  return ll;
}


int get_next_file_number() {
  return filenum_;
}


void set_next_file_number(int snap) {
  filenum_ = snap;
}


// grid_sdf_output {{{
void grid_sdf_output(unsigned int stamp, double t, int gen)
{
#ifdef RNPL
#if n_dim == 1
  // dim = 1 {{{
  // FIXME -- we shouldn't have fixed limits like this in output
  const int max_points_level = 5000;
  int max_levels = JJ+1;

  vizpoint **vpoints;
  int *indx;

  vpoints = (vizpoint **) malloc(max_levels*sizeof(vizpoint *));
  for (int j=0;j<max_levels;j++) {
    vpoints[j] = (vizpoint *) malloc(max_points_level*sizeof(vizpoint));
  }
  indx = (int *) calloc(max_levels,sizeof(int));

  vizpoint *full;
  full = (vizpoint *) malloc(max_points_level*max_levels*sizeof(vizpoint));
  int full_indx = 0;


  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    int j = indx[point->level];
    vpoints[point->level][j].x = point->coords.pos[x_dir];
    vpoints[point->level][j].y = 0.0;
    vpoints[point->level][j].z = 0.0;
    full[full_indx].x = point->coords.pos[x_dir];
    full[full_indx].y = 0.0;
    full[full_indx].z = 0.0;
    for (int k=0;k<n_variab+n_aux;k++) {
      vpoints[point->level][j].value[k] = point->u[gen][k];
      full[full_indx].value[k] = point->u[gen][k];
    }
    indx[point->level] += 1;
    full_indx++;

    if ( full_indx >= max_points_level*max_levels ||
         indx[point->level] >= max_points_level ) {
      printf(" PROBLEM:  increase max_points_level in 1-d output!\n");
      exit(0);
    }
  }

  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      coll_point_t *point = ptr->point;
      if (point->time_stamp == stamp) {
      //if (point->status[0] == nonessential) {
        //printf("%8.7f %d %14.13e\n", point->coords, point->level, point->u[0]);
        int j = indx[point->level];
        //printf("level=%d, j=%d, JJ=%d\n",point->level,j,JJ);
        vpoints[point->level][j].x = point->coords.pos[x_dir];
        vpoints[point->level][j].y = 0.0;
        vpoints[point->level][j].z = 0.0;
        full[full_indx].x = point->coords.pos[x_dir];
        full[full_indx].y = 0.0;
        full[full_indx].z = 0.0;
        for (int k=0;k<n_variab+n_aux;k++) {
          vpoints[point->level][j].value[k] = point->u[gen][k];
          full[full_indx].value[k] = point->u[gen][k];
        }
        indx[point->level] += 1;
        full_indx++;
        if ( full_indx >= max_points_level*max_levels ||
          indx[point->level] >= max_points_level ) {
          printf(" PROBLEM:  increase max_points_level in 1-d output!\n");
          exit(0);
        }
      }
      ptr = ptr->next;
    }
  }

  // sort the values for each level
  for (int i=0;i<max_levels;i++) {
    qsort(vpoints[i],indx[i],sizeof(vpoints[i][0]),cmpvertices);
  }
  // sort everything
  qsort(full,full_indx,sizeof(full[0]),cmpvertices);

  // establish bboxes
  const int numbbox = 500;
  double **bbox_minx = (double **) calloc(max_levels,sizeof(double *));
  double **bbox_maxx = (double **) calloc(max_levels,sizeof(double *));
  int *bbox_counter = (int *) calloc(max_levels,sizeof(int));
  for (int i=0;i<max_levels;i++) {
    bbox_minx[i] = (double *) calloc(numbbox,sizeof(double));
    bbox_maxx[i] = (double *) calloc(numbbox,sizeof(double));
  }

  double dx0 = L_dim[x_dir] / max_index[x_dir];

  // the base grid is unigrid -- no issues there
  bbox_minx[0][0] = pars.xmin;
  bbox_maxx[0][0] = pars.xmax;
  bbox_counter[0] = 1;

  // get the rest of the levels
  for (int i=1;i<max_levels;i++) {
    int h = 1 << (JJ - i);
    // "effective" search resolution for this level (ignoring contribution from lower level)
    double search_dx = 2*dx0*h;

    bbox_minx[i][bbox_counter[i]] = vpoints[i][0].x;
    for (int j=1;j<indx[i];j++) {
      if ( floatcmp(vpoints[i][j].x,vpoints[i][j-1].x  + search_dx) ) {
        bbox_maxx[i][bbox_counter[i]] = vpoints[i][j].x;
      } else {
        bbox_counter[i] += 1;
        if ( bbox_counter[i] >= numbbox ) {
          printf(" PROBLEM: increase the numbbox parameter size -- limit reached for output!\n");
          exit(0);
        }
        bbox_minx[i][bbox_counter[i]] = vpoints[i][j].x;
      }
      // end condition
      if (j==indx[i]-1) {
        bbox_counter[i] += 1;
        if ( bbox_counter[i] >= numbbox ) {
          printf(" PROBLEM: increase the numbbox parameter size -- limit reached for output!\n");
          exit(0);
        }
      }
    }
  }
  // DEBUG
  //for (int i = 1; i < max_levels; i++) {
  //  printf(" Number of bbx %d for level %d\n",bbox_counter[i],i);
  //  for (int j=0;j<bbox_counter[i];j++) {
  //    printf(" bbox minx %g maxx %g\n",bbox_minx[i][j],bbox_maxx[i][j]);
  //  }
  //}

 /* output sdf's */
  int rank = 1;
  char *cnames = "x";
  char fname[16];
  double **sdf_func;
  int *outfields;
  double *sdf_coords;
  int shape[3];
  sdf_func = (double **) malloc(pars.num_req_fields*sizeof(double *));
  outfields = (int *) malloc(pars.num_req_fields*sizeof(int));
  int count = 0;
  for (int n=0;n<n_variab+n_aux;n++) {
    if ( pars.output_fields[n] == normal_output ) {
      outfields[count] = n;
      count++;
    }
  }

  assert(count == pars.num_req_fields);

  for (int i = 0; i < max_levels; i++) {
    int h = 1 << (JJ - i);
    double dx = dx0*h;
    for (int j=0;j<bbox_counter[i];j++) {
      int nx = nearbyint((bbox_maxx[i][j] - bbox_minx[i][j])/dx)+1;
      shape[0] = nx;
      for (int n=0;n<pars.num_req_fields;n++) {
        sdf_func[n] = malloc(nx*sizeof(double));
      }
      sdf_coords = malloc(nx*sizeof(double));
      int m;
      for (m=0;m<full_indx;m++) {
        if ( floatcmp(full[m].x,bbox_minx[i][j]) ) break;
      }
      for (int k=0;k<nx;k++) {
        sdf_coords[k] = bbox_minx[i][j] + dx*k;

        // base grid case
        if ( i ==0 ) {
          for (int n=0;n<pars.num_req_fields;n++) {
            sdf_func[n][k] = vpoints[i][k].value[outfields[n]];
          }
        } else {
          while (floatcmp(full[m].x,sdf_coords[k]) != 1) {
            m++;
            if (full[m].x > sdf_coords[k] + dx) {
              printf(" PROBLEM: point not found! full %g coord %g level %d\n",full[m].x,sdf_coords[k],i);
              exit(0);
            }
          }
          for (int n=0;n<pars.num_req_fields;n++) {
            sdf_func[n][k] = full[m].value[outfields[n]];
          }
        }
      }
      for (int n=0;n<pars.num_req_fields;n++) {
        if (outfields[n] == 0 ) sprintf(fname,"%s_%d","Ax",gen);
        if (outfields[n] == 1 ) sprintf(fname,"%s_%d","Ay",gen);
        if (outfields[n] == 2 ) sprintf(fname,"%s_%d","Az",gen);
        if (outfields[n] == 3 ) sprintf(fname,"%s_%d","Ex",gen);
        if (outfields[n] == 4 ) sprintf(fname,"%s_%d","Ey",gen);
        if (outfields[n] == 5 ) sprintf(fname,"%s_%d","Ez",gen);
        if (outfields[n] == 6 ) sprintf(fname,"%s_%d","psi",gen);
        if (outfields[n] == 7 ) sprintf(fname,"%s_%d","gam",gen);
        if (outfields[n] == 8 ) sprintf(fname,"%s_%d","con",gen);
        gft_out_full(fname, t, shape, cnames, rank,
                   sdf_coords, sdf_func[n]);
        free(sdf_func[n]);
      }
      free(sdf_coords);
    }
    if ( pars.output_by_level == 1 ) {
      if (indx[i] > 0) {
        for (int n=0;n<pars.num_req_fields;n++) {
          sdf_func[n] = malloc(indx[i]*sizeof(double));
        }
        sdf_coords = malloc(indx[i]*sizeof(double));
        for (int k=0;k<indx[i];k++) {
          sdf_coords[k] = vpoints[i][k].x;
        }

        for (int k=0;k<indx[i];k++) {
          for (int n=0;n<pars.num_req_fields;n++) {
            sdf_func[n][k] = vpoints[i][k].value[outfields[n]];
          }
        }

        for (int n=0;n<pars.num_req_fields;n++) {
          if (outfields[n] == 0 ) sprintf(fname,"%s_%d","Ax",gen);
          if (outfields[n] == 1 ) sprintf(fname,"%s_%d","Ay",gen);
          if (outfields[n] == 2 ) sprintf(fname,"%s_%d","Az",gen);
          if (outfields[n] == 3 ) sprintf(fname,"%s_%d","Ex",gen);
          if (outfields[n] == 4 ) sprintf(fname,"%s_%d","Ey",gen);
          if (outfields[n] == 5 ) sprintf(fname,"%s_%d","Ez",gen);
          if (outfields[n] == 6 ) sprintf(fname,"%s_%d","psi",gen);
          if (outfields[n] == 7 ) sprintf(fname,"%s_%d","gam",gen);
          if (outfields[n] == 8 ) sprintf(fname,"%s_%d","con",gen);

          gft_out_full(fname, t, (indx+i), cnames, rank,
                   sdf_coords, sdf_func[n]);
          free(sdf_func[n]);
        }
        free(sdf_coords);
      }
    }
  }
  for (int i=0;i<max_levels;i++) {
    free(bbox_minx[i]);
    free(bbox_maxx[i]);
  }
  free(bbox_counter);
  free(bbox_minx);
  free(bbox_maxx);

  free(indx);
  for (int i = 0; i < max_levels; i++) {
    free(vpoints[i]);
  }
  free(full);
  free(vpoints);
  // }}}
#endif

#if 0
#if n_dim == 2
  // dim = 2 {{{
  vizpoint *full;
  int numpoints = npts_in_array;
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      numpoints++;
      ptr = ptr->next;
    }
  }
  numpoints++;

  int *outfields;
  outfields = (int *) malloc(pars.num_req_fields*sizeof(int));
  int count = 0;
  for (int n=0;n<n_variab+n_aux;n++) {
    if ( pars.output_fields[n] == normal_output ) {
      outfields[count] = n;
      count++;
    }
  }
  assert(count == pars.num_req_fields);

  full = (vizpoint *) malloc(numpoints*sizeof(vizpoint));
  int full_indx = 0;

  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    full[full_indx].x = point->coords.pos[x_dir];
    full[full_indx].y = point->coords.pos[y_dir];
    full[full_indx].z = 0.0;
    for (int l=0;l<pars.num_req_fields;l++) {
      full[full_indx].value[outfields[l]] = point->u[0][outfields[l]];
    }
    full_indx++;

    if ( full_indx >= numpoints ) {
      printf(" PROBLEM:  ran out of storage for output!\n");
      exit(0);
    }
  }

  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      coll_point_t *point = ptr->point;
      if (point->time_stamp == stamp) {
        //printf("%8.7f %d %14.13e\n", point->coords, point->level, point->u[0]);
        //printf("level=%d, j=%d, JJ=%d\n",point->level,j,JJ);
        full[full_indx].x = point->coords.pos[x_dir];
        full[full_indx].y = point->coords.pos[y_dir];
        full[full_indx].z = 0.0;
        for (int l=0;l<pars.num_req_fields;l++) {
          full[full_indx].value[outfields[l]] = point->u[0][outfields[l]];
        }
        full_indx++;
        if ( full_indx >= numpoints ) {
          printf(" PROBLEM:  ran out of storage for output!\n");
          exit(0);
        }
      }
      ptr = ptr->next;
    }
  }

  // sort everything
  qsort(full,full_indx,sizeof(full[0]),cmpvertices);

  double bbox_minx;
  double bbox_maxx;
  double bbox_miny;
  double bbox_maxy;

  /* output sdf's */
  int rank = 2;
  int shape[2];
  char *cnames = "x|y";
  char fname[16];
  double **sdf_func;
  double *sdf_coords;
  sdf_func = (double **) malloc(pars.num_req_fields*sizeof(double *));

  int nx = 2;
  int ny = 2;
  sdf_coords = malloc( (nx + ny)*sizeof(double));
  for (int l=0;l<pars.num_req_fields;l++) {
    sdf_func[l] = malloc(nx*ny*sizeof(double));
  }
  for (int m=0;m<full_indx-1;m++) {
    // produce grid sections in groups of 4
    bbox_minx = full[m].x;
    bbox_miny = full[m].y;
    sdf_coords[0] = bbox_minx;
    sdf_coords[0+nx] = bbox_miny;
    for (int l=0;l<pars.num_req_fields;l++) {
      sdf_func[l][0] = full[m].value[outfields[l]];
    }
    if ( full[m+1].x > full[m].x ) {
      bbox_maxx = full[m+1].x;
      sdf_coords[1] = bbox_maxx;
      for (int l=0;l<pars.num_req_fields;l++) {
        sdf_func[l][1] = full[m+1].value[outfields[l]];
      }
      int fa = 0;
      int fb = 0;
      for (int n=m;n<full_indx;n++) {
        if ( floatcmp(full[m].x,full[n].x) && full[n].y > full[m].y ) {
          bbox_maxy = full[n].y;
          sdf_coords[1+nx] = bbox_maxy;
          for (int l=0;l<pars.num_req_fields;l++) {
            sdf_func[l][2] = full[n].value[outfields[l]];
          }
          fa = 1;
          break;
        }
      }
      for (int n=m;n<full_indx;n++) {
        if ( floatcmp(full[m+1].x,full[n].x) && floatcmp(full[n].y,bbox_maxy) ) {
          for (int l=0;l<pars.num_req_fields;l++) {
            sdf_func[l][3] = full[n].value[outfields[l]];
          }
          fb = 1;
          break;
        }
      }
      if ( fa == 1 && fb == 1 ) {
        for (int n=0;n<pars.num_req_fields;n++) {
          if (outfields[n] == 0 ) sprintf(fname,"%s_%d","Ax",gen);
          if (outfields[n] == 1 ) sprintf(fname,"%s_%d","Ay",gen);
          if (outfields[n] == 2 ) sprintf(fname,"%s_%d","Az",gen);
          if (outfields[n] == 3 ) sprintf(fname,"%s_%d","Ex",gen);
          if (outfields[n] == 4 ) sprintf(fname,"%s_%d","Ey",gen);
          if (outfields[n] == 5 ) sprintf(fname,"%s_%d","Ez",gen);
          if (outfields[n] == 6 ) sprintf(fname,"%s_%d","psi",gen);
          if (outfields[n] == 7 ) sprintf(fname,"%s_%d","gam",gen);
          if (outfields[n] == 8 ) sprintf(fname,"%s_%d","con",gen);
          shape[0] = nx;
          shape[1] = ny;
          gft_out_full(fname, t, shape, cnames, rank,
               sdf_coords, sdf_func[n]);

        }
      }
    }
  }
  for (int l=0;l<pars.num_req_fields;l++) {
    free(sdf_func[l]);
  }
  free(sdf_func);
  free(sdf_coords);
  free(full);
  // }}}
#endif
#endif
#endif

#ifdef SILO
#if n_dim == 2
  // dim = 2 {{{
  int numpoints = npts_in_array;
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->time_stamp == stamp &&
               (ptr->point->status[CURRENT_STATUS] == essential ||
                ptr->point->status[CURRENT_STATUS] == neighboring) ) {
        ++numpoints;
      }
      ptr = ptr->next;
    }
  }

  int *outfields;
  outfields = (int *) malloc(pars.num_req_fields*sizeof(int));
  int count = 0;
  for (int n=0;n<n_variab+n_aux;n++) {
    if ( pars.output_fields[n] == normal_output ) {
      outfields[count] = n;
      count++;
    }
  }
  assert(count == pars.num_req_fields);

  int full_indx = 0;
  float *xarray,*yarray;
  xarray = (float *) malloc(numpoints*sizeof(float));
  yarray = (float *) malloc(numpoints*sizeof(float));
  float **sdf_func;
  sdf_func = (float **) malloc(pars.num_req_fields*sizeof(float *));
  for (int l=0;l<pars.num_req_fields;l++) {
    sdf_func[l] = malloc(numpoints*sizeof(float));
  }

  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    xarray[full_indx] = point->coords.pos[x_dir];
    yarray[full_indx] = point->coords.pos[y_dir];
    for (int l=0;l<pars.num_req_fields;l++) {
      sdf_func[l][full_indx] = point->u[gen][outfields[l]];
    }
    full_indx++;

    if ( full_indx > numpoints ) {
      printf(" PROBLEM:  ran out of storage for output!\n");
      exit(0);
    }
  }

  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      coll_point_t *point = ptr->point;
      if (point->time_stamp == stamp &&
                (point->status[CURRENT_STATUS] == essential ||
                 point->status[CURRENT_STATUS] == neighboring) ) {
        xarray[full_indx] = point->coords.pos[x_dir];
        yarray[full_indx] = point->coords.pos[y_dir];
        for (int l=0;l<pars.num_req_fields;l++) {
          sdf_func[l][full_indx] = point->u[gen][outfields[l]];
        }
        full_indx++;
        if ( full_indx > numpoints ) {
          printf(" PROBLEM:  ran out of storage for output!\n");
          exit(0);
        }
      }
      ptr = ptr->next;
    }
  }

 /* Note: it may seem that we want full_indx = numpoints - 1, but full_indx
  * is incremented after points are defined, so we assert that they are equal
  */
  //printf("... full_indx=%d, numpoints=%d\n",full_indx,numpoints);

  assert(full_indx == numpoints);

  DBfile         *dbfile = NULL;
  static char     meshname[] = {"pmesh"};
  int             driver=DB_PDB;
  char            filename[256];

  sprintf(filename,"mhd.%05d.pdb",filenum_);
  dbfile = DBCreate(filename, 0, DB_LOCAL, "rmhd data", driver);
  assert(dbfile != NULL);

  DBoptlist      *optlist = NULL;
  optlist = DBMakeOptlist(1);
  DBAddOption(optlist, DBOPT_DTIME, &t);
  int cycle = time_stamp/n_gen;
  DBAddOption(optlist, DBOPT_CYCLE, &cycle);
  float          *coords[2], *vars[3];
  int dims = 2;

  coords[0] = xarray;
  coords[1] = yarray;

/* Check the coordinates are in bounds before writing pdb file.
  for (int ii = 0; ii < numpoints; ii++) {
    if (fabs(xarray[ii]) > 1.0 || fabs(yarray[ii]) > 1.0) {
      printf("::: problem with coordinate array. ii=%d, x=%f, y=%f\n",
              ii,xarray[ii],yarray[ii]);
    }
  }
*/

  DBPutPointmesh(dbfile, meshname, dims, coords, numpoints, DB_FLOAT, optlist);

  char fname[80];

  for (int n=0;n<pars.num_req_fields;n++) {
    if (outfields[n] == 0 ) sprintf(fname,"%s","Ax");
    if (outfields[n] == 1 ) sprintf(fname,"%s","Ay");
    if (outfields[n] == 2 ) sprintf(fname,"%s","Az");
    if (outfields[n] == 3 ) sprintf(fname,"%s","Ex");
    if (outfields[n] == 4 ) sprintf(fname,"%s","Ey");
    if (outfields[n] == 5 ) sprintf(fname,"%s","Ez");
    if (outfields[n] == 6 ) sprintf(fname,"%s","psi");
    if (outfields[n] == 7 ) sprintf(fname,"%s","Gam");
    if (outfields[n] == 8 ) sprintf(fname,"%s","con");

    vars[0] = sdf_func[n];
    DBPutPointvar(dbfile, fname, meshname, 1, vars, numpoints, DB_FLOAT, optlist);
  }
  DBFreeOptlist(optlist);
  DBClose(dbfile);

  free(xarray);
  free(yarray);
  for (int l=0;l<pars.num_req_fields;l++) {
    free(sdf_func[l]);
  }
  free(sdf_func);
  free(outfields);
  // }}}
#endif

#if n_dim == 3
  // dim = 3 {{{
  
  // figure out how many points we have in the hash table 
  int numpoints = npts_in_array;
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->time_stamp == stamp &&
               (ptr->point->status[CURRENT_STATUS] == essential ||
                ptr->point->status[CURRENT_STATUS] == neighboring) ) {
        ++numpoints;
      }
      ptr = ptr->next;
    }
  }
  
  // allocate memory 
  int *outfields;
  outfields = (int *) malloc(pars.num_req_fields*sizeof(int));
    // number of fields needed for output 
  int count = 0;
  for (int n=0;n<n_variab+n_aux;n++) {
    if ( pars.output_fields[n] == normal_output ) {
      outfields[count] = n;
      count++;
    }
  }
  assert(count == pars.num_req_fields);
  
  // allocate memory for the coordinates 
  int full_indx = 0;
  float *xarray,*yarray,*zarray;
  xarray = (float *) malloc(numpoints*sizeof(float));
  yarray = (float *) malloc(numpoints*sizeof(float));
  zarray = (float *) malloc(numpoints*sizeof(float));
  float **sdf_func;
  sdf_func = (float **) malloc(pars.num_req_fields*sizeof(float *));
    // contains all data 
  for (int l=0;l<pars.num_req_fields;l++) { // that's an L. 
    sdf_func[l] = (float*)malloc(numpoints*sizeof(float));
  }
  
  // base-level points
  // this takes the x,y,z coords for every point. 
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i]; // This is what's actually 
    xarray[full_indx] = point->coords.pos[x_dir]; // stored in the hash table. 
    yarray[full_indx] = point->coords.pos[y_dir];
    zarray[full_indx] = point->coords.pos[z_dir];
    for (int l=0;l<pars.num_req_fields;l++) {
      // WKB-TODO: add in output weighted by r for A, r^2 for E
      sdf_func[l][full_indx] = (float) point->u[gen][outfields[l]];
    }
    full_indx++;

    if ( full_indx > numpoints ) { // Should be an assert. 
      printf(" PROBLEM:  ran out of storage for output!\n");
      exit(0);
    }
  }
  
  //higher-level points
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      coll_point_t *point = ptr->point;
      if (point->time_stamp == stamp &&
                    (point->status[CURRENT_STATUS] == essential ||
                     point->status[CURRENT_STATUS] == neighboring) ) {
        xarray[full_indx] = point->coords.pos[x_dir];
        yarray[full_indx] = point->coords.pos[y_dir];
        zarray[full_indx] = point->coords.pos[z_dir];
        for (int l=0;l<pars.num_req_fields;l++) {
          sdf_func[l][full_indx] = (float) point->u[gen][outfields[l]];
          // WKB-TODO: add in extra output. 
        }
        full_indx++;
        if ( full_indx > numpoints ) {
          printf(" PROBLEM:  ran out of storage for output!\n");
          exit(0);
        }
      }
      ptr = ptr->next;
    }
  }

  // Weight output by radius if requested.
  const int f_falloff[8] = { 1, 1, 1, 2, 2, 2, 1, 2 };

  if (pars.weight_output_by_radius == 1) {
    for (int i = 0; i < numpoints; i++) {
      float radsq = xarray[i]*xarray[i] + yarray[i]*yarray[i] 
                                            + zarray[i]*zarray[i];
      float rad = sqrt(radsq);
      for (int m = 0; m < pars.num_req_fields; m++) {
        if (f_falloff[outfields[m]] == 1) {
          sdf_func[m][i] *= rad;
        }
        else {
          sdf_func[m][i] *= radsq;
        }
      }
    }
  }

  DBfile         *dbfile = NULL;
  static char     meshname[] = {"pmesh"};
  int             driver=DB_PDB;
  char            filename[256];

  sprintf(filename,"mhd.%05d.pdb",filenum_);
  dbfile = DBCreate(filename, 0, DB_LOCAL, "rmhd data", driver);
  assert(dbfile != NULL);

  DBoptlist      *optlist = NULL;
  optlist = DBMakeOptlist(1);
  DBAddOption(optlist, DBOPT_DTIME, &t);
  int cycle = time_stamp/n_gen;
  DBAddOption(optlist, DBOPT_CYCLE, &cycle);
  float          *coords[3], *vars[3];
  int dims = 3;

  coords[0] = xarray;
  coords[1] = yarray;
  coords[2] = zarray;

 /* Note: it may seem that we want full_indx = numpoints - 1, but full_indx
  * is incremented after points are defined, so we assert that they are equal
  */
  assert(numpoints == full_indx);

  DBPutPointmesh(dbfile, meshname, dims, coords, numpoints, DB_FLOAT, optlist);

  char fname[80];

  for (int n=0;n<pars.num_req_fields;n++) {
//  printf("Doing output for function %d\n",n);
    if (outfields[n] == 0 ) sprintf(fname,"%s","Ax");
    if (outfields[n] == 1 ) sprintf(fname,"%s","Ay");
    if (outfields[n] == 2 ) sprintf(fname,"%s","Az");
    if (outfields[n] == 3 ) sprintf(fname,"%s","Ex");
    if (outfields[n] == 4 ) sprintf(fname,"%s","Ey");
    if (outfields[n] == 5 ) sprintf(fname,"%s","Ez");
    if (outfields[n] == 6 ) sprintf(fname,"%s","psi");
    if (outfields[n] == 7 ) sprintf(fname,"%s","Gam");
    if (outfields[n] == 8 ) sprintf(fname,"%s","con");
    vars[0] = sdf_func[n];
    DBPutPointvar(dbfile, fname, meshname, 1, vars, numpoints, DB_FLOAT, optlist);
  }
  DBFreeOptlist(optlist);
  DBClose(dbfile);

  free(xarray);
  free(yarray);
  free(zarray);
  for (int l=0;l<pars.num_req_fields;l++) {
    free(sdf_func[l]);
  }
  free(sdf_func);
  free(outfields);
  // }}}
#endif
#endif

  filenum_++;
}
// }}}
