#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include "hawaii.h"

#include "bbhutil.h"

char *read_line(FILE *fin) {
    char *buffer;
    char *tmp;
    int read_chars = 0;
    int bufsize = 512;
    char *line = (char*)malloc(bufsize);
   // char *line = malloc(bufsize); //Incompatiable with c++

    if ( !line ) {
        return NULL;
    }

    buffer = line;

    while ( fgets(buffer, bufsize - read_chars, fin) ) {
        read_chars = strlen(line);

        if ( line[read_chars - 1] == '\n' ) {
            line[read_chars - 1] = '\0';
            return line;
        }

        else {
            bufsize = 2 * bufsize;
            tmp = (char*)realloc(line, bufsize);
            //tmp = realloc(line, bufsize); //Incompatible with c++ 
            if ( tmp ) {
                line = tmp;
                buffer = line + read_chars;
            }
            else {
                free(line);
                return NULL;
            }
        }
    }
    return NULL;
}

int read_param_file(char *pfile)
{

  int ok = 1;
  int ok2= 1; 
  const double huge  = 1.0e14;
  const double small = 1.0e-14;

  ok *= read_int_param(pfile, "max_iter", &pars.max_iter, 1, 1, 5000000); 
  ok *= read_int_param(pfile, "prune_frequency", &pars.prune_frequency,1,1,50); 
  ok *= read_real_param(pfile, "epsilon", &pars.epsilon, 1.0e-3, small, 0.5);

  pars.t0 = 0.0; // hard code the initial time.
  ok *= read_real_param(pfile, "tf", &pars.tf, 1.0, pars.t0, huge);
  ok *= read_real_param(pfile, "max_runtime", &pars.max_runtime, 3600.0, 1.0, huge);
 /* code will checkpoint (create restart file) by default every 4 hours, with
  * a minimum an 1 hour */
  ok *= read_real_param(pfile, "time_between_restarts", &pars.time_between_restarts, 14400.0, 3600.0, huge);
  ok *= read_real_param(pfile, "xmin", &pars.xmin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "xmax", &pars.xmax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "ymin", &pars.ymin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "ymax", &pars.ymax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "zmin", &pars.zmin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "zmax", &pars.zmax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "cfl", &pars.cfl, 0.25, small, 0.5);
  // Constraint  
  ok *= read_real_param(pfile, "psi_floor", &pars.psi_floor, 0.001, 1.0e-10, 1.0);
  // EM parameters
  ok *= read_int_param(pfile, "id_type", &pars.id_type, 0, 0, 3); 

  ok *= read_int_param(pfile, "weight_output_by_radius", &pars.weight_output_by_radius, 0, 0, 1); 
  if(pars.weight_output_by_radius==1) printf("weighting the output\n");
  
  // WKB Antenna params
  ok2*= read_real_param(pfile, "antenna_min_x", &pars.antenna_min_x, 0, pars.xmin, pars.xmax); 
  ok2*= read_real_param(pfile, "antenna_min_y", &pars.antenna_min_y, 0, pars.ymin, pars.ymax); 
  ok2*= read_real_param(pfile, "antenna_min_z", &pars.antenna_min_z,-1, pars.zmin, pars.zmax); 
  
  ok2*= read_real_param(pfile, "antenna_max_x", &pars.antenna_max_x, 0, pars.xmin, pars.xmax); 
  ok2*= read_real_param(pfile, "antenna_max_y", &pars.antenna_max_y, 0, pars.ymin, pars.ymax); 
  ok2*= read_real_param(pfile, "antenna_max_z", &pars.antenna_max_z, 1, pars.zmin, pars.zmax); 
  
  if(sqrt((pars.antenna_max_x-pars.antenna_min_x)*(pars.antenna_max_x-pars.antenna_min_x)+
          (pars.antenna_max_y-pars.antenna_min_y)*(pars.antenna_max_y-pars.antenna_min_y)+
          (pars.antenna_max_z-pars.antenna_min_z)*(pars.antenna_max_z-pars.antenna_min_z))==0){
    printf("Warning! Zero-length wire being used!\n");
  }
           

  
  ok2*= read_real_param(pfile, "antenna_omega", &pars.antenna_omega, 3, -huge, huge);
  ok2*= read_real_param(pfile, "antenna_J0", &pars.antenna_J0, 1, -huge, huge);
  ok2*= read_int_param(pfile, "antenna_poles", &pars.antenna_poles, 2, 0, 10);
  
  // boundary conditions
  char **bcs;
  bcs = (char **) malloc(2*n_dim*sizeof(char *));
  for (int i = 0; i < 2*n_dim; i++) {
    bcs[i] = (char *) malloc(32*sizeof(char));
  }
  if (get_str_param(pfile, "boundaries", bcs, 2*n_dim)!= 1) {
    for(int i = 0; i < 2*n_dim; i++) {
      pars.bcs[i] = bc_outflow;
    }
  }
  else {
    for (int i = 0; i < 2*n_dim; i++) {
      if ( strcmp(bcs[i],"outflow") == 0 ) {
        pars.bcs[i] = bc_outflow;
        printf("## i=%d, bc_outflow\n",i);
      }
      else if ( strcmp(bcs[i],"wall") == 0) {
        pars.bcs[i] = bc_wall;
        printf("## i=%d, bc_wall\n",i);
      }
      else {
        pars.bcs[i] = bc_unknown;
        printf("## i=%d, bc_unknown\n",i);
      }
    }
  }
  for (int i = 0; i < 2*n_dim; i++) {
    free(bcs[i]);
  }
  free(bcs);

  ok *= read_int_param(pfile, "output_frequency", &pars.output_frequency,
                       0, 0, 1000000);
  ok *= read_int_param(pfile, "print_frequency", &pars.print_frequency,
                       0, 0, 1000000);

  // output fields
  // find out how many fields for which output is requested
  FILE *fp;
  char *line;
  fp = fopen(pfile,"r");
  int count = 0;
  if ( fp ) {
    while ( (line = read_line(fp))) {
      if ( strstr(line,"output_functions") ) {
        // count how many fields listed here
        const char *tmp = line;
        while( (tmp=strstr(tmp,"\"")) ) {
          count++;
          tmp++;
        }
      }
    }
    fclose(fp);
  }
  int num_req_fields = count/2;
  pars.num_req_fields = num_req_fields;

  // output fields
  // initialize output off
  for (int i=0;i<n_variab+n_aux;i++) {
    pars.output_fields[i] = no_output;
  }

  char **output_fields;
  output_fields = (char **) malloc((num_req_fields)*sizeof(char *));
  for (int i = 0; i < num_req_fields ; i++) {
    output_fields[i] = (char *) malloc(32*sizeof(char));
  }
  
  if (get_str_param(pfile, "output_functions", output_fields, num_req_fields)!= 1) {
    for(int i = 0; i < n_variab+n_aux; i++) {
      pars.output_fields[i] = no_output;
    }
  }
  else {
    for (int i = 0; i < num_req_fields; i++) {
      if ( strcmp(output_fields[i],"Ax") == 0 ) {
        pars.output_fields[U_AX] = normal_output;
        printf(" Ax output selected\n");
      }
      if ( strcmp(output_fields[i],"Ay") == 0 ) {
        pars.output_fields[U_AY] = normal_output;
        printf(" Ay output selected\n");
      }
      if ( strcmp(output_fields[i],"Az") == 0 ) {
        pars.output_fields[U_AZ] = normal_output;
        printf(" Az output selected\n");
      }
      if ( strcmp(output_fields[i],"Ex") == 0 ) {
        pars.output_fields[U_EX] = normal_output;
        printf(" Ex output selected\n");
      }
      if ( strcmp(output_fields[i],"Ey") == 0 ) {
        pars.output_fields[U_EY] = normal_output;
        printf(" Ey output selected\n");
      }
      if ( strcmp(output_fields[i],"Ez") == 0 ) {
        pars.output_fields[U_EZ] = normal_output;
        printf(" Ez output selected\n");
      }
      if ( strcmp(output_fields[i],"gam") == 0 ) {
        pars.output_fields[U_GAM] = normal_output;
        printf(" gam output selected\n");
      }
      if ( strcmp(output_fields[i],"psi") == 0 ) {
        pars.output_fields[U_PSI] = normal_output;
        printf(" psi output selected\n");
      }
      if ( strcmp(output_fields[i],"con") == 0 ) {
        pars.output_fields[U_CON] = normal_output;
        printf(" con output selected\n");
      }
    }
  }

  for (int i = 0; i < num_req_fields; i++) {
    free(output_fields[i]);
  }
  free(output_fields);

  if (ok*ok2 != 1) {
    printf("There was an error reading the parameters.\n");
  }
  if (ok2 != 1) {
    printf("Antenna parameters not read correctly.\n");
  }
  
  return ok*ok2;
}
 
int read_int_param(char* pfile, char *name, int *var, const int def_val,
                   const int min_val, const int max_val)
{
  if (get_param(pfile, name, "long", 1, var) != 1) {
    *var = def_val;
    printf("default used for %s\n",name);
  }
  if (*var > max_val) {
    printf("%s = %d is out of range. max(%s) = %d\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %d is out of range. min(%s) = %d\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}

int read_real_param(char* pfile, char *name, double *var, const double def_val,
                   const double min_val, const double max_val)
{
  if (get_param(pfile, name, "double", 1, var) != 1) {
    *var = def_val;
    printf("default used for %s\n",name);
  }
  if (*var > max_val) {
    printf("%s = %g is out of range. max(%s) = %g\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %g is out of range. min(%s) = %g\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}
int default_params()
{
  // no par file read-in supported; hard code parameters
  pars.max_iter = 10;
  pars.prune_frequency = 1;
  pars.epsilon = 1.e-3;

  pars.t0 = 0.0; // hard code the initial time.
  pars.tf = 1.0;
  pars.xmin = -100.0;
  pars.xmax = 100.0;
  pars.ymin = -100.0;
  pars.ymax = 100.0;
  pars.zmin = -100.0;
  pars.zmax = 100.0;

  pars.cfl = 0.25;

  return 1;
}
