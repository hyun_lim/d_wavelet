#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"
#include "em.h"

double t0;
double tf;

void rhs_stage1(coll_point_t *point, const int gen);
void rhs_stage2(coll_point_t *point, const int gen);
void rhs_stage3(coll_point_t *point, const int gen);

/// ---------------------------------------------------------------------------
/// Solving the BSSN-like formulation of the MAXWELL equations.
/// ---------------------------------------------------------------------------

// get_local_dt  {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
double get_local_dt (const coll_point_t *point)
{
  double dx0 = L_dim[x_dir] / max_index[x_dir];
  double dy0 = L_dim[y_dir] / max_index[y_dir];
  double dz0 = L_dim[z_dir] / max_index[z_dir];

  int h = 1 << (JJ - point->level);
  double dx = dx0 * h;
  double dy = dy0 * h;
  double dz = dz0 * h;
  double delta = fmin(dx, dy);
  delta = fmin(delta, dz);
  double dt = pars.cfl * delta;
  return dt;
}
// }}}

// problem_init {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void problem_init(void) 
{
  t0 = 0.0; // Simulation starting time

  // FIXME set simulation stopping time with a parameter
  tf = 1;   // Simulation stopping time

  double ranges[n_variab];
  initial_ranges(ranges);

  // FIXME Check definition of epsilon
  for (int i=0;i<n_variab;i++) {
    eps[i] = pars.epsilon; // Error tolerance for grid refinement
    eps_scale[i] = ranges[1] * eps[i]; // Error tolerance for grid refinement
  }

  assert(n_dim == 3);
  L_dim[x_dir] = pars.xmax - pars.xmin; // Problem physical dimension
  L_dim[y_dir] = pars.ymax - pars.ymin; // Problem physical dimension
  L_dim[z_dir] = pars.zmax - pars.zmin; // Problem physical dimension

  validate_parameters();
  time_stamp = 0; // Initialize global time stamp
}
// }}}

// {{{
/*----------------------------------------------------------------------
 *
 * initial_ranges:
 *                 This subroutine makes a guess for the initial range
 *                 of the BSSN variables.
 *
 *----------------------------------------------------------------------*/
void initial_ranges(double *ranges) {

  for (int i = 0; i < n_variab; ++i) {
    ranges[i] = 1.0;
  }
}
// }}}

// initial_condition {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void initial_condition(const coord_t *coord, double *u) 
{
  if (pars.id_type == 0) {
    init_data_simple(coord, u);
  }
  else {
    printf("Unknown initial data type = %d\n",pars.id_type);
  }
}
// }}}

// init_data_simple {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void init_data_simple(const coord_t *coord, double *u)
{

  const double lambda = 1.0;
  const double amp    = 0.0;

  double x = coord->pos[x_dir];
  double y = coord->pos[y_dir];
  double z = coord->pos[z_dir];
  double r = sqrt(x*x + y*y + z*z);

  double Afield[3], Efield[3], psi, Gam;

  Afield[0] = 0.0;
  Afield[1] = 0.0;
  Afield[2] = 0.0;

  double Ephi = 8.0 * amp * sqrt(x*x + y*y) * lambda * lambda * exp(-lambda * r * r);
  r = (r > 1.0e-8) ? r : 1.0e-8;
  Efield[0] = -y/r * Ephi;
  Efield[1] =  x/r * Ephi;
  Efield[2] = 0.0;

  psi = 0.0;

  Gam = 0.0;

  u[U_AX] = Afield[0];
  u[U_AY] = Afield[1];
  u[U_AZ] = Afield[2];
  u[U_EX] = Efield[0];
  u[U_EY] = Efield[1];
  u[U_EZ] = Efield[2];
  u[U_PSI] = psi;
  u[U_GAM] = Gam;
  u[U_CON] = 0.0;

}
//   }}}

// compute_rhs {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void compute_rhs(const double t, const int gen) {

 /* compute finite difference derivatives everywhere */
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    rhs_stage1(point, gen);
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        rhs_stage1(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }

 /* call the RHS */
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    rhs_stage2(point, gen);
  }

  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        rhs_stage2(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }

 /* compute KO dissipation */
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    rhs_stage3(point, gen);
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        rhs_stage3(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }
}
// }}}

// rhs_stage1 {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void rhs_stage1(coll_point_t *point, const int gen) {
    if (gen > 0)
      check_derivative_stencil(point, gen);

    // FIXME We aren't using a mask currently.
    int mask[n_variab + n_aux] = {0};
    for (int ivar = 0; ivar < n_variab; ivar++)
      mask[ivar] = 1;

   /* Now calling Finite Difference routine for derivatives */
    //TODO : Check this! for (dir_t dir = x_dir; dir < n_dim; dir++)
    for (int dir = x_dir; dir < n_dim; dir++)
      compute_fd_derivative(point, gen, dir, mask);

}
// }}}

// rhs_stage2 {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void rhs_stage2(coll_point_t *point, const int gen) {
    for (int ivar = 0; ivar < n_variab; ivar++) {
      point->rhs[gen][0] = 0.0;
    }

   /* call RHS routine */
    em_rhs(point, gen);
}
// }}}

// rhs_stage3 {{{
/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void rhs_stage3(coll_point_t *point, const int gen) {
   /* FIXME We aren't using a mask currently. This will determine which fields
    *       get K-O dissipation. 
    */
    int mask[n_variab + n_aux] = {0};
    for (int ivar = 0; ivar < n_variab; ivar++)
      mask[ivar] = 1;

   /* Compute K-O dissipation derivatives in each direction. 
    * K-O derivatives are stored in the standard location for derivatives.
    */
    //TODO : Check this! for (dir_t dir = x_dir; dir < n_dim; dir++)
    for (int dir = x_dir; dir < n_dim; dir++)
      compute_KOdiss(point, gen, dir, mask);

    apply_KOdiss(point, gen, mask);

}
// }}}
