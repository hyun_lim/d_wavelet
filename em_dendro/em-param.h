#ifndef HAWAII_EM_PARAM_H
#define HAWAII_EM_PARAM_H

#include "em.h"

typedef enum {
  bc_unknown = 0,
  bc_outflow = 1,
  bc_wall = 2
} boundary_condition_t;

// this can be expanded to delineate types of output
typedef enum {
  no_output = 0,
  normal_output = 1,
} output_condition_t;

typedef struct {
 /* simulation parameters */
  int max_iter; // max number of allowed iterations in time integrator
  double t0;    // initial time stamp of the simulation
  double tf;    // final time stamp of the simulation
  double xmin;
  double xmax;
  double ymin;
  double ymax;
  double zmin;
  double zmax;
  double cfl;
  
  double sigma_diss;

  int prune_frequency;

//  int JJ;       // maximum number of resolution levels to use
  double epsilon;

  /* output */
  int output_frequency;
  int output_fields[n_variab+n_aux];

 /* boundary conditions */
  boundary_condition_t bcs[6];

 /* internally used variables */
  int base_step_size;
  int max_index_x;
  int num_req_fields;
  int stats_output_frequency;
  int print_frequency;
  int spin1_ph;
  int spin2_ph;
  double max_runtime;
  double time_between_restarts;
	
 /* EM parameters */
  int    id_type;

 /* Antenna Parameters WKB */
  double antenna_min_x;
  double antenna_min_y;
  double antenna_min_z;

  double antenna_max_x;
  double antenna_max_y;
  double antenna_max_z;

  double antenna_omega;
  double antenna_J0;
  int antenna_poles;

 /* Constraint parameters */
  double psi_floor;

  int weight_output_by_radius;
  
} Pars;

extern Pars pars;

int read_param_file(char *pfile);
int default_params();
int read_int_param(char* pfile, char *name, int *var, const int def_val,
                   const int min_val, const int max_val);
int read_real_param(char* pfile, char *name, double *var, const double def_val,
                   const double min_val, const double max_val);


#endif
