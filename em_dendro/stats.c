#include "stats.h"

#include <stdio.h>
#include <stdlib.h>

#include "param.h"


static FILE *stats_file_;


double sum_time_stats(time_stats_t *ts) {
  return (ts->t_integrate + ts->t_adjust + ts->t_startup
                    + ts->t_io + ts->t_prune);
}


bool open_stats_file(int freq, int restart_flag) {
  //only open the file if there will be output
  if (freq == 0) {
    return true;
  }

  stats_file_ = fopen("run_statistics.txt",
                          (restart_flag ? "a" : "w"));
  if (stats_file_ == NULL) {
    perror("Error opening run statistics file:");
    return false;
  }

  return true;
}


void close_stats_file() {
  if (stats_file_ != NULL) {
    fclose(stats_file_);
  }
}


void write_to_stats(double tcurr, unsigned stamp, time_stats_t *ts,
                    int numpoints, int numpoints_noness) {
  //write a line to the file
  double t_total = sum_time_stats(ts);
  unsigned nsteps = stamp / n_gen;
  fprintf(stats_file_, "%lg %u %lg %u %lg %lg %lg %lg %lg %d %d\n",
          tcurr, stamp, t_total, nsteps,
          ts->t_integrate, ts->t_adjust, ts->t_startup, ts->t_io, ts->t_prune,
          numpoints, numpoints_noness);
}
