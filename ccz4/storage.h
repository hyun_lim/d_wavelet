#ifndef HAWAII_STORAGE_H
#define HAWAII_STORAGE_H

#include <stdbool.h>
#include <stdint.h>
#include "types.h"

#if JJ == 1
//if JJ == 1, we are in unigrid, and the hash table is not needed. This saves
// a bunch of pointless loops
//TODO: When we remove the array v. hash table distinction, this should instead
// be the total number of points up to level 1
#define HASH_TBL_SIZE 1
#else
#define HASH_TBL_SIZE 196613
#endif

//The number of locks to allocate for the hash table.
#define LOCK_TBL_SIZE 3145739

#ifdef HPX
extern hpx_addr_t _table_locks[LOCK_TBL_SIZE];  //locks for the rows of the table
#endif

// some larger primes for hash table
// #define HASH_TBL_SIZE 49157
// #define HASH_TBL_SIZE 98317
// #define HASH_TBL_SIZE 196613
// #define HASH_TBL_SIZE 393241
// #define HASH_TBL_SIZE 786433
// #define HASH_TBL_SIZE 1572869
// #define HASH_TBL_SIZE 3145739
// #define HASH_TBL_SIZE 6291469
// #define HASH_TBL_SIZE 12582917
// #define HASH_TBL_SIZE 25165843
// #define HASH_TBL_SIZE 50331653
// #define HASH_TBL_SIZE 100663319
// #define HASH_TBL_SIZE 201326611
// #define HASH_TBL_SIZE 402653189
// #define HASH_TBL_SIZE 805306457
// #define HASH_TBL_SIZE 1610612741

extern int max_level;
extern const int max_index[3];
extern const int ns[3];
extern const int base_step_size;
extern const int level_one_step_size;
extern const int npts_in_array;

typedef struct hash_entry_t{
  coll_point_t *point;
  uint64_t mkey;
  struct hash_entry_t *next;
} hash_entry_t;

typedef struct {
  coll_point_t *array;
  hash_entry_t *hash_table[HASH_TBL_SIZE];
} hawaii_storage_t;

#ifdef HPX
typedef struct hpx_hash_entry_t{
  uint64_t mkey;
  struct hpx_hash_entry_t *next;
  hpx_addr_t point;
} hpx_hash_entry_t;

typedef struct {
  hpx_addr_t array;
  hpx_hash_entry_t *hash_table[HASH_TBL_SIZE];
} hpx_hawaii_storage_t;
#endif

extern hawaii_storage_t *coll_points;
#ifdef HPX
extern hpx_hawaii_storage_t hpx_coll_points;
#endif

bool is_array(const index_t *index);

uint64_t morton_key(const index_t *index);

void storage_init(void);

void test_weno_interpolation(coll_point_t *point, int gen);

void storage_cleanup(void);

void create_full_grids(void);

void create_adap_grids(void);

void create_neighboring_point(coll_point_t *essen_point);

void create_nonessential_point(coll_point_t *nonessen_point,
                               const index_t *index);

coll_point_t *get_coll_point(const index_t *index);

coll_point_t *add_coll_point(const index_t *index, int *flag);

void adjust_grids(void);

void prune_grids(void);

int count_points(coll_status_t minstat, unsigned stamp);

#ifdef PERIODIC
index_t map_index_into_grid(const index_t *in);
#endif

#endif
