#ifndef __STATS_H__
#define __STATS_H__


#include <stdio.h>
#include <stdbool.h>


typedef struct {
  double t_integrate;
  double t_adjust;
  double t_startup;
  double t_io;
  double t_prune;
} time_stats_t;


double sum_time_stats(time_stats_t *ts);
bool open_stats_file(int freq, int restart_flag);
void close_stats_file();
void write_to_stats(double tcurr, unsigned stamp, time_stats_t *ts,
                    int numpoints, int numpoints_noness);


#endif
