      Gamh_rhs[0] = Gamh_rhs[0]+Betau[0]*adv_d_Gamh[0][0]+Betau[1]*
adv_d_Gamh[1][0]+Betau[2]*adv_d_Gamh[2][0];
      Gamh_rhs[1] = Gamh_rhs[1]+Betau[0]*adv_d_Gamh[0][1]+Betau[1]*
adv_d_Gamh[1][1]+Betau[2]*adv_d_Gamh[2][1];
      Gamh_rhs[2] = Gamh_rhs[2]+Betau[0]*adv_d_Gamh[0][2]+Betau[1]*
adv_d_Gamh[1][2]+Betau[2]*adv_d_Gamh[2][2];
      t8 = Atd_rhs[0][1]+Betau[0]*adv_d_Atd[0][0][1]+Betau[1]*adv_d_Atd[1][0]
[1]+Betau[2]*adv_d_Atd[2][0][1];
      t12 = Atd_rhs[0][2]+Betau[0]*adv_d_Atd[0][0][2]+Betau[1]*adv_d_Atd[1][0]
[2]+Betau[2]*adv_d_Atd[2][0][2];
      t20 = Atd_rhs[1][2]+Betau[0]*adv_d_Atd[0][1][2]+Betau[1]*adv_d_Atd[1][1]
[2]+Betau[2]*adv_d_Atd[2][1][2];
      Atd_rhs[0][0] = Atd_rhs[0][0]+Betau[0]*adv_d_Atd[0][0][0]+Betau[1]*
adv_d_Atd[1][0][0]+Betau[2]*adv_d_Atd[2][0][0];
      Atd_rhs[0][1] = t8;
      Atd_rhs[0][2] = t12;
      Atd_rhs[1][0] = t8;
      Atd_rhs[1][1] = Atd_rhs[1][1]+Betau[0]*adv_d_Atd[0][1][1]+Betau[1]*
adv_d_Atd[1][1][1]+Betau[2]*adv_d_Atd[2][1][1];
      Atd_rhs[1][2] = t20;
      Atd_rhs[2][0] = t12;
      Atd_rhs[2][1] = t20;
      Atd_rhs[2][2] = Atd_rhs[2][2]+Betau[0]*adv_d_Atd[0][2][2]+Betau[1]*
adv_d_Atd[1][2][2]+Betau[2]*adv_d_Atd[2][2][2];
      t8 = gtd_rhs[0][1]+Betau[0]*adv_d_gtd[0][0][1]+Betau[1]*adv_d_gtd[1][0]
[1]+Betau[2]*adv_d_gtd[2][0][1];
      t12 = gtd_rhs[0][2]+Betau[0]*adv_d_gtd[0][0][2]+Betau[1]*adv_d_gtd[1][0]
[2]+Betau[2]*adv_d_gtd[2][0][2];
      t20 = gtd_rhs[1][2]+Betau[0]*adv_d_gtd[0][1][2]+Betau[1]*adv_d_gtd[1][1]
[2]+Betau[2]*adv_d_gtd[2][1][2];
      gtd_rhs[0][0] = gtd_rhs[0][0]+Betau[0]*adv_d_gtd[0][0][0]+Betau[1]*
adv_d_gtd[1][0][0]+Betau[2]*adv_d_gtd[2][0][0];
      gtd_rhs[0][1] = t8;
      gtd_rhs[0][2] = t12;
      gtd_rhs[1][0] = t8;
      gtd_rhs[1][1] = gtd_rhs[1][1]+Betau[0]*adv_d_gtd[0][1][1]+Betau[1]*
adv_d_gtd[1][1][1]+Betau[2]*adv_d_gtd[2][1][1];
      gtd_rhs[1][2] = t20;
      gtd_rhs[2][0] = t12;
      gtd_rhs[2][1] = t20;
      gtd_rhs[2][2] = gtd_rhs[2][2]+Betau[0]*adv_d_gtd[0][2][2]+Betau[1]*
adv_d_gtd[1][2][2]+Betau[2]*adv_d_gtd[2][2][2];
      Betau_rhs[0] = Betau_rhs[0]+lambda_2*(Betau[0]*adv_d_Betau[0][0]+
Betau[1]*adv_d_Betau[1][0]+Betau[2]*adv_d_Betau[2][0]);
      Betau_rhs[1] = Betau_rhs[1]+lambda_2*(Betau[0]*adv_d_Betau[0][1]+
Betau[1]*adv_d_Betau[1][1]+Betau[2]*adv_d_Betau[2][1]);
      Betau_rhs[2] = Betau_rhs[2]+lambda_2*(Betau[0]*adv_d_Betau[0][2]+
Betau[1]*adv_d_Betau[1][2]+Betau[2]*adv_d_Betau[2][2]);
      t2 = 0.1E1-lambda_4;
      Bu_rhs[0] = Bu_rhs[0]+Betau[0]*(lambda_3*adv_d_Bu[0][0]+t2*adv_d_Gamh
[0][0])+Betau[1]*(lambda_3*adv_d_Bu[1][0]+t2*adv_d_Gamh[1][0])+Betau[2]*(
lambda_3*adv_d_Bu[2][0]+t2*adv_d_Gamh[2][0]);
      Bu_rhs[1] = Bu_rhs[1]+Betau[0]*(lambda_3*adv_d_Bu[0][1]+t2*adv_d_Gamh
[0][1])+Betau[1]*(lambda_3*adv_d_Bu[1][1]+t2*adv_d_Gamh[1][1])+Betau[2]*(
lambda_3*adv_d_Bu[2][1]+t2*adv_d_Gamh[2][1]);
      Bu_rhs[2] = Bu_rhs[2]+Betau[0]*(lambda_3*adv_d_Bu[0][2]+t2*adv_d_Gamh
[0][2])+Betau[1]*(lambda_3*adv_d_Bu[1][2]+t2*adv_d_Gamh[1][2])+Betau[2]*(
lambda_3*adv_d_Bu[2][2]+t2*adv_d_Gamh[2][2]);
      t1 = Betau[0];
      t4 = Betau[1];
      t7 = Betau[2];
      psi_rhs = psi_rhs+t1*adv_d_psi[0]+t4*adv_d_psi[1]+t7*adv_d_psi[2];
      trK_rhs = trK_rhs+t1*adv_d_trK[0]+t4*adv_d_trK[1]+t7*adv_d_trK[2];
      Alpha_rhs = Alpha_rhs+lambda_1*(t1*adv_d_Alpha[0]+t4*adv_d_Alpha[1]+
t7*adv_d_Alpha[2]);
      Theta_rhs = Theta_rhs+t1*adv_d_Theta[0]+t4*adv_d_Theta[1]+t7*
adv_d_Theta[2];
      emEu_rhs[0] = emEu_rhs[0]+Betau[0]*adv_d_emEu[0][0]+Betau[1]*
adv_d_emEu[1][0]+Betau[2]*adv_d_emEu[2][0];
      emEu_rhs[1] = emEu_rhs[1]+Betau[0]*adv_d_emEu[0][1]+Betau[1]*
adv_d_emEu[1][1]+Betau[2]*adv_d_emEu[2][1];
      emEu_rhs[2] = emEu_rhs[2]+Betau[0]*adv_d_emEu[0][2]+Betau[1]*
adv_d_emEu[1][2]+Betau[2]*adv_d_emEu[2][2];
      emBu_rhs[0] = emBu_rhs[0]+Betau[0]*adv_d_emBu[0][0]+Betau[1]*
adv_d_emBu[1][0]+Betau[2]*adv_d_emBu[2][0];
      emBu_rhs[1] = emBu_rhs[1]+Betau[0]*adv_d_emBu[0][1]+Betau[1]*
adv_d_emBu[1][1]+Betau[2]*adv_d_emBu[2][1];
      emBu_rhs[2] = emBu_rhs[2]+Betau[0]*adv_d_emBu[0][2]+Betau[1]*
adv_d_emBu[1][2]+Betau[2]*adv_d_emBu[2][2];
      t1 = Betau[0];
      t4 = Betau[1];
      t7 = Betau[2];
      emPsi_rhs = emPsi_rhs+t1*adv_d_emPsi[0]+t4*adv_d_emPsi[1]+t7*
adv_d_emPsi[2];
      emPhi_rhs = emPhi_rhs+t1*adv_d_emPhi[0]+t4*adv_d_emPhi[1]+t7*
adv_d_emPhi[2];
