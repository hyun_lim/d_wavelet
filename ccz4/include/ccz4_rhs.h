      t1 = Alpha*Alpha;
      rho_ADM = t1*Tu[0][0];
      Ju_ADM[0] = Alpha*(Tu[0][1]+Betau[0]*Tu[0][0]);
      Ju_ADM[1] = Alpha*(Tu[0][2]+Betau[1]*Tu[0][0]);
      Ju_ADM[2] = Alpha*(Tu[0][3]+Betau[2]*Tu[0][0]);
      t4 = Betau[0]*gtd[0][0]+Betau[1]*gtd[0][1]+Betau[2]*gtd[0][2];
      t13 = gtd[0][0]*gtd[0][0];
      t15 = gtd[0][1]*gtd[0][1];
      t17 = gtd[0][2]*gtd[0][2];
      t31 = Betau[0]*gtd[0][1]+Betau[1]*gtd[1][1]+Betau[2]*gtd[1][2];
      t32 = t31*Tu[0][0];
      t33 = gtd[0][1]*Tu[0][1];
      t34 = gtd[1][1]*Tu[0][2];
      t35 = gtd[1][2]*Tu[0][3];
      t47 = gtd[1][2]*Tu[2][3];
      t56 = t4*(t32+t33+t34+t35)+gtd[0][0]*(t31*Tu[0][1]+gtd[0][1]*Tu[1][1]+gtd
[1][1]*Tu[1][2]+gtd[1][2]*Tu[1][3])+gtd[0][1]*(t31*Tu[0][2]+gtd[0][1]*Tu[1][2]+
gtd[1][1]*Tu[2][2]+t47)+gtd[0][2]*(t31*Tu[0][3]+gtd[0][1]*Tu[1][3]+gtd[1][1]*Tu
[2][3]+gtd[1][2]*Tu[3][3]);
      t60 = Betau[0]*gtd[0][2]+Betau[1]*gtd[1][2]+Betau[2]*gtd[2][2];
      t61 = t60*Tu[0][0];
      t62 = gtd[0][2]*Tu[0][1];
      t63 = gtd[1][2]*Tu[0][2];
      t64 = gtd[2][2]*Tu[0][3];
      t65 = t61+t62+t63+t64;
      t71 = t60*Tu[0][1]+gtd[0][2]*Tu[1][1]+gtd[1][2]*Tu[1][2]+gtd[2][2]*Tu[1]
[3];
      t77 = t60*Tu[0][2]+gtd[0][2]*Tu[1][2]+gtd[1][2]*Tu[2][2]+gtd[2][2]*Tu[2]
[3];
      t82 = t60*Tu[0][3]+gtd[0][2]*Tu[1][3]+t47+gtd[2][2]*Tu[3][3];
      t84 = t4*t65+gtd[0][0]*t71+gtd[0][1]*t77+gtd[0][2]*t82;
      t90 = gtd[1][1]*gtd[1][1];
      t92 = gtd[1][2]*gtd[1][2];
      t107 = t31*t65+gtd[0][1]*t71+gtd[1][1]*t77+gtd[1][2]*t82;
      t114 = gtd[2][2]*gtd[2][2];
      pTtd_ADM[0][0] = t4*(t4*Tu[0][0]+two*(gtd[0][0]*Tu[0][1]+gtd[0][1]*Tu[0]
[2]+gtd[0][2]*Tu[0][3]))+t13*Tu[1][1]+t15*Tu[2][2]+t17*Tu[3][3]+two*(gtd[0][0]*
gtd[0][1]*Tu[1][2]+gtd[0][0]*gtd[0][2]*Tu[1][3]+gtd[0][1]*gtd[0][2]*Tu[2][3]);
      pTtd_ADM[0][1] = t56;
      pTtd_ADM[0][2] = t84;
      pTtd_ADM[1][0] = t56;
      pTtd_ADM[1][1] = t31*(t32+two*(t33+t34+t35))+t15*Tu[1][1]+t90*Tu[2][2]+
t92*Tu[3][3]+two*(gtd[0][1]*gtd[1][1]*Tu[1][2]+gtd[0][1]*gtd[1][2]*Tu[1][3]+gtd
[1][1]*gtd[1][2]*Tu[2][3]);
      pTtd_ADM[1][2] = t107;
      pTtd_ADM[2][0] = t84;
      pTtd_ADM[2][1] = t107;
      pTtd_ADM[2][2] = t60*(t61+two*(t62+t63+t64))+t17*Tu[1][1]+t92*Tu[2][2]+
t114*Tu[3][3]+two*(gtd[0][2]*gtd[1][2]*Tu[1][2]+gtd[0][2]*gtd[2][2]*Tu[1][3]+
gtd[1][2]*gtd[2][2]*Tu[2][3]);
      inv_psi = 0.1E1/psi;
      psi_p = pow(psi,1.0*p_expo);
      inv_psi_p = 0.1E1/psi_p;
      //inv_Alpha = 0.1E1/Alpha;
      t1 = gtd[0][0];
      t2 = gtd[1][1];
      t4 = gtd[2][2];
      t6 = gtd[1][2];
      t7 = t6*t6;
      t9 = gtd[0][1];
      t10 = t9*t9;
      t12 = gtd[0][2];
      t16 = t12*t12;
      detgtd = t1*t2*t4-t1*t7-t10*t4+2.0*t9*t12*t6-t16*t2;
      idetgtd = 0.1E1/detgtd;
      t2 = gtd[1][2]*gtd[1][2];
      t8 = idetgtd*(-gtd[0][1]*gtd[2][2]+gtd[0][2]*gtd[1][2]);
      t12 = idetgtd*(gtd[0][1]*gtd[1][2]-gtd[0][2]*gtd[1][1]);
      t14 = gtd[0][2]*gtd[0][2];
      t20 = idetgtd*(-gtd[0][0]*gtd[1][2]+gtd[0][1]*gtd[0][2]);
      t22 = gtd[0][1]*gtd[0][1];
      gtu[0][0] = idetgtd*(gtd[1][1]*gtd[2][2]-t2);
      gtu[0][1] = t8;
      gtu[0][2] = t12;
      gtu[1][0] = t8;
      gtu[1][1] = idetgtd*(gtd[0][0]*gtd[2][2]-t14);
      gtu[1][2] = t20;
      gtu[2][0] = t12;
      gtu[2][1] = t20;
      gtu[2][2] = idetgtd*(gtd[0][0]*gtd[1][1]-t22);
      t2 = gtu[0][1]*Atd[0][1];
      t3 = gtu[0][2]*Atd[0][2];
      t18 = gtu[1][2]*Atd[1][2];
      Atud[0][0] = gtu[0][0]*Atd[0][0]+t2+t3;
      Atud[0][1] = gtu[0][0]*Atd[0][1]+gtu[0][1]*Atd[1][1]+gtu[0][2]*Atd[1][2];
      Atud[0][2] = gtu[0][0]*Atd[0][2]+gtu[0][1]*Atd[1][2]+gtu[0][2]*Atd[2][2];
      Atud[1][0] = gtu[0][1]*Atd[0][0]+gtu[1][1]*Atd[0][1]+gtu[1][2]*Atd[0][2];
      Atud[1][1] = t2+gtu[1][1]*Atd[1][1]+t18;
      Atud[1][2] = gtu[0][1]*Atd[0][2]+gtu[1][1]*Atd[1][2]+gtu[1][2]*Atd[2][2];
      Atud[2][0] = gtu[0][2]*Atd[0][0]+gtu[1][2]*Atd[0][1]+gtu[2][2]*Atd[0][2];
      Atud[2][1] = gtu[0][2]*Atd[0][1]+gtu[1][2]*Atd[1][1]+gtu[2][2]*Atd[1][2];
      Atud[2][2] = t3+t18+gtu[2][2]*Atd[2][2];
      t8 = Atud[0][0]*gtu[0][1]+Atud[0][1]*gtu[1][1]+Atud[0][2]*gtu[1][2];
      t12 = Atud[0][0]*gtu[0][2]+Atud[0][1]*gtu[1][2]+Atud[0][2]*gtu[2][2];
      t20 = Atud[1][0]*gtu[0][2]+Atud[1][1]*gtu[1][2]+Atud[1][2]*gtu[2][2];
      Atu[0][0] = Atud[0][0]*gtu[0][0]+Atud[0][1]*gtu[0][1]+Atud[0][2]*gtu[0]
[2];
      Atu[0][1] = t8;
      Atu[0][2] = t12;
      Atu[1][0] = t8;
      Atu[1][1] = Atud[1][0]*gtu[0][1]+Atud[1][1]*gtu[1][1]+Atud[1][2]*gtu[1]
[2];
      Atu[1][2] = t20;
      Atu[2][0] = t12;
      Atu[2][1] = t20;
      Atu[2][2] = Atud[2][0]*gtu[0][2]+Atud[2][1]*gtu[1][2]+Atud[2][2]*gtu[2]
[2];
      t3 = d_gtd[1][0][2]+d_gtd[2][0][1]-d_gtd[0][1][2];
      t8 = d_gtd[0][1][2]+d_gtd[2][0][1]-d_gtd[1][0][2];
      t13 = d_gtd[0][1][2]+d_gtd[1][0][2]-d_gtd[2][0][1];
      Ctd[0][0][0] = d_gtd[0][0][0];
      Ctd[0][0][1] = d_gtd[1][0][0];
      Ctd[0][0][2] = d_gtd[2][0][0];
      Ctd[0][1][0] = d_gtd[1][0][0];
      Ctd[0][1][1] = 2.0*d_gtd[1][0][1]-d_gtd[0][1][1];
      Ctd[0][1][2] = t3;
      Ctd[0][2][0] = d_gtd[2][0][0];
      Ctd[0][2][1] = t3;
      Ctd[0][2][2] = 2.0*d_gtd[2][0][2]-d_gtd[0][2][2];
      Ctd[1][0][0] = 2.0*d_gtd[0][0][1]-d_gtd[1][0][0];
      Ctd[1][0][1] = d_gtd[0][1][1];
      Ctd[1][0][2] = t8;
      Ctd[1][1][0] = d_gtd[0][1][1];
      Ctd[1][1][1] = d_gtd[1][1][1];
      Ctd[1][1][2] = d_gtd[2][1][1];
      Ctd[1][2][0] = t8;
      Ctd[1][2][1] = d_gtd[2][1][1];
      Ctd[1][2][2] = 2.0*d_gtd[2][1][2]-d_gtd[1][2][2];
      Ctd[2][0][0] = 2.0*d_gtd[0][0][2]-d_gtd[2][0][0];
      Ctd[2][0][1] = t13;
      Ctd[2][0][2] = d_gtd[0][2][2];
      Ctd[2][1][0] = t13;
      Ctd[2][1][1] = 2.0*d_gtd[1][1][2]-d_gtd[2][1][1];
      Ctd[2][1][2] = d_gtd[1][2][2];
      Ctd[2][2][0] = d_gtd[0][2][2];
      Ctd[2][2][1] = d_gtd[1][2][2];
      Ctd[2][2][2] = d_gtd[2][2][2];
      t8 = gtu[0][0]*Ctd[0][0][1]+gtu[0][1]*Ctd[1][0][1]+gtu[0][2]*Ctd[2][0][1]
;
      t12 = gtu[0][0]*Ctd[0][0][2]+gtu[0][1]*Ctd[1][0][2]+gtu[0][2]*Ctd[2][0]
[2];
      t20 = gtu[0][0]*Ctd[0][1][2]+gtu[0][1]*Ctd[1][1][2]+gtu[0][2]*Ctd[2][1]
[2];
      t32 = gtu[0][1]*Ctd[0][0][1]+gtu[1][1]*Ctd[1][0][1]+gtu[1][2]*Ctd[2][0]
[1];
      t36 = gtu[0][1]*Ctd[0][0][2]+gtu[1][1]*Ctd[1][0][2]+gtu[1][2]*Ctd[2][0]
[2];
      t44 = gtu[0][1]*Ctd[0][1][2]+gtu[1][1]*Ctd[1][1][2]+gtu[1][2]*Ctd[2][1]
[2];
      t56 = gtu[0][2]*Ctd[0][0][1]+gtu[1][2]*Ctd[1][0][1]+gtu[2][2]*Ctd[2][0]
[1];
      t60 = gtu[0][2]*Ctd[0][0][2]+gtu[1][2]*Ctd[1][0][2]+gtu[2][2]*Ctd[2][0]
[2];
      t68 = gtu[0][2]*Ctd[0][1][2]+gtu[1][2]*Ctd[1][1][2]+gtu[2][2]*Ctd[2][1]
[2];
      Ct[0][0][0] = gtu[0][0]*Ctd[0][0][0]+gtu[0][1]*Ctd[1][0][0]+gtu[0][2]*Ctd
[2][0][0];
      Ct[0][0][1] = t8;
      Ct[0][0][2] = t12;
      Ct[0][1][0] = t8;
      Ct[0][1][1] = gtu[0][0]*Ctd[0][1][1]+gtu[0][1]*Ctd[1][1][1]+gtu[0][2]*Ctd
[2][1][1];
      Ct[0][1][2] = t20;
      Ct[0][2][0] = t12;
      Ct[0][2][1] = t20;
      Ct[0][2][2] = gtu[0][0]*Ctd[0][2][2]+gtu[0][1]*Ctd[1][2][2]+gtu[0][2]*Ctd
[2][2][2];
      Ct[1][0][0] = gtu[0][1]*Ctd[0][0][0]+gtu[1][1]*Ctd[1][0][0]+gtu[1][2]*Ctd
[2][0][0];
      Ct[1][0][1] = t32;
      Ct[1][0][2] = t36;
      Ct[1][1][0] = t32;
      Ct[1][1][1] = gtu[0][1]*Ctd[0][1][1]+gtu[1][1]*Ctd[1][1][1]+gtu[1][2]*Ctd
[2][1][1];
      Ct[1][1][2] = t44;
      Ct[1][2][0] = t36;
      Ct[1][2][1] = t44;
      Ct[1][2][2] = gtu[0][1]*Ctd[0][2][2]+gtu[1][1]*Ctd[1][2][2]+gtu[1][2]*Ctd
[2][2][2];
      Ct[2][0][0] = gtu[0][2]*Ctd[0][0][0]+gtu[1][2]*Ctd[1][0][0]+gtu[2][2]*Ctd
[2][0][0];
      Ct[2][0][1] = t56;
      Ct[2][0][2] = t60;
      Ct[2][1][0] = t56;
      Ct[2][1][1] = gtu[0][2]*Ctd[0][1][1]+gtu[1][2]*Ctd[1][1][1]+gtu[2][2]*Ctd
[2][1][1];
      Ct[2][1][2] = t68;
      Ct[2][2][0] = t60;
      Ct[2][2][1] = t68;
      Ct[2][2][2] = gtu[0][2]*Ctd[0][2][2]+gtu[1][2]*Ctd[1][2][2]+gtu[2][2]*Ctd
[2][2][2];
      div_Beta = d_Betau[0][0]+d_Betau[1][1]+d_Betau[2][2];
      d_div_Beta[0] = dd_Betau[0][0][0]+dd_Betau[0][1][1]+dd_Betau[0][2][2];
      d_div_Beta[1] = dd_Betau[0][1][0]+dd_Betau[1][1][1]+dd_Betau[1][2][2];
      d_div_Beta[2] = dd_Betau[0][2][0]+dd_Betau[1][2][1]+dd_Betau[2][2][2];
      CalGamTil[0] = half*(gtu[0][0]*Ct[0][0][0]+gtu[1][1]*Ct[0][1][1]+gtu[2]
[2]*Ct[0][2][2])+gtu[0][1]*Ct[0][0][1]+gtu[0][2]*Ct[0][0][2]+gtu[1][2]*Ct[0][1]
[2];
      CalGamTil[1] = half*(gtu[0][0]*Ct[1][0][0]+gtu[1][1]*Ct[1][1][1]+gtu[2]
[2]*Ct[1][2][2])+gtu[0][1]*Ct[1][0][1]+gtu[0][2]*Ct[1][0][2]+gtu[1][2]*Ct[1][1]
[2];
      CalGamTil[2] = half*(gtu[0][0]*Ct[2][0][0]+gtu[1][1]*Ct[2][1][1]+gtu[2]
[2]*Ct[2][2][2])+gtu[0][1]*Ct[2][0][1]+gtu[0][2]*Ct[2][0][2]+gtu[1][2]*Ct[2][1]
[2];
      t1 = half*inv_psi_p;
      Zu[0] = t1*(Gamh[0]-CalGamTil[0]);
      Zu[1] = t1*(Gamh[1]-CalGamTil[1]);
      Zu[2] = t1*(Gamh[2]-CalGamTil[2]);
      t1 = p_expo*inv_psi;
      t3 = (p_expo+two)*inv_psi;
      t4 = d_psi[0]*d_psi[0];
      t12 = two*psi_p;
      t16 = gtd[0][0]*Zu[0]+gtd[0][1]*Zu[1]+gtd[0][2]*Zu[2];
      t32 = gtd[0][1]*Zu[0]+gtd[1][1]*Zu[1]+gtd[1][2]*Zu[2];
      t38 = t1*(fourth*(t3*d_psi[0]*d_psi[1]+Ct[0][0][1]*d_psi[0]+Ct[1][0][1]*
d_psi[1]+Ct[2][0][1]*d_psi[2])-half*dd_psi[0][1]-psi_p*(d_psi[0]*t32+d_psi[1]*
t16));
      t50 = gtd[0][2]*Zu[0]+gtd[1][2]*Zu[1]+gtd[2][2]*Zu[2];
      t56 = t1*(fourth*(t3*d_psi[0]*d_psi[2]+Ct[0][0][2]*d_psi[0]+Ct[1][0][2]*
d_psi[1]+Ct[2][0][2]*d_psi[2])-half*dd_psi[0][2]-psi_p*(d_psi[0]*t50+d_psi[2]*
t16));
      t57 = d_psi[1]*d_psi[1];
      t82 = t1*(fourth*(t3*d_psi[1]*d_psi[2]+Ct[0][1][2]*d_psi[0]+Ct[1][1][2]*
d_psi[1]+Ct[2][1][2]*d_psi[2])-half*dd_psi[1][2]-psi_p*(d_psi[1]*t50+d_psi[2]*
t32));
      t83 = d_psi[2]*d_psi[2];
      RDZpd_1[0][0] = t1*(fourth*(t3*t4+Ct[0][0][0]*d_psi[0]+Ct[1][0][0]*d_psi
[1]+Ct[2][0][0]*d_psi[2])-half*dd_psi[0][0]-t12*d_psi[0]*t16);
      RDZpd_1[0][1] = t38;
      RDZpd_1[0][2] = t56;
      RDZpd_1[1][0] = t38;
      RDZpd_1[1][1] = t1*(fourth*(t3*t57+Ct[0][1][1]*d_psi[0]+Ct[1][1][1]*d_psi
[1]+Ct[2][1][1]*d_psi[2])-half*dd_psi[1][1]-t12*d_psi[1]*t32);
      RDZpd_1[1][2] = t82;
      RDZpd_1[2][0] = t56;
      RDZpd_1[2][1] = t82;
      RDZpd_1[2][2] = t1*(fourth*(t3*t83+Ct[0][2][2]*d_psi[0]+Ct[1][2][2]*d_psi
[1]+Ct[2][2][2]*d_psi[2])-half*dd_psi[2][2]-t12*d_psi[2]*t50);
      t1 = p_expo*half;
      t6 = (t1-one)*inv_psi;
      t7 = d_psi[0]*d_psi[0];
      t11 = d_psi[1]*d_psi[1];
      t15 = d_psi[2]*d_psi[2];
      t34 = inv_psi*(Gamh[0]*d_psi[0]+Gamh[1]*d_psi[1]+Gamh[2]*d_psi[2]-gtu[0]
[0]*(t6*t7+dd_psi[0][0])-gtu[1][1]*(t6*t11+dd_psi[1][1])-gtu[2][2]*(t6*t15+
dd_psi[2][2])-two*(gtu[0][1]*(t6*d_psi[1]*d_psi[0]+dd_psi[0][1])+gtu[0][2]*(t6*
d_psi[2]*d_psi[0]+dd_psi[0][2])+gtu[1][2]*(t6*d_psi[2]*d_psi[1]+dd_psi[1][2])))
;
      t40 = RDZpd_1[0][1]+t1*t34*gtd[0][1];
      t43 = RDZpd_1[0][2]+t1*t34*gtd[0][2];
      t49 = RDZpd_1[1][2]+t1*t34*gtd[1][2];
      RDZpd[0][0] = RDZpd_1[0][0]+t1*t34*gtd[0][0];
      RDZpd[0][1] = t40;
      RDZpd[0][2] = t43;
      RDZpd[1][0] = t40;
      RDZpd[1][1] = RDZpd_1[1][1]+t1*t34*gtd[1][1];
      RDZpd[1][2] = t49;
      RDZpd[2][0] = t43;
      RDZpd[2][1] = t49;
      RDZpd[2][2] = RDZpd_1[2][2]+t1*t34*gtd[2][2];
      t7 = two*Ctd[0][0][0]+Ctd[0][0][0];
      t9 = two*Ctd[0][0][1];
      t10 = t9+Ctd[1][0][0];
      t12 = two*Ctd[0][0][2];
      t13 = t12+Ctd[2][0][0];
      t27 = t9+Ctd[0][0][1];
      t30 = two*Ctd[0][1][1]+Ctd[1][0][1];
      t32 = two*Ctd[0][1][2];
      t33 = t32+Ctd[2][0][1];
      t47 = t12+Ctd[0][0][2];
      t49 = t32+Ctd[1][0][2];
      t52 = two*Ctd[0][2][2]+Ctd[2][0][2];
      t82 = Ctd[0][0][1]+Ctd[1][0][0];
      t84 = Ctd[0][1][1]+Ctd[1][0][1];
      t86 = Ctd[0][1][2]+Ctd[1][0][2];
      t93 = Ctd[1][0][2]+Ctd[2][0][1];
      t108 = Ct[0][1][2]*Ctd[0][0][0];
      t109 = Ct[1][0][2]*Ctd[1][0][1];
      t111 = Ct[1][1][2]*Ctd[0][0][1];
      t113 = Ct[2][1][2]*Ctd[0][0][2];
      t121 = Ctd[1][1][2]+Ctd[2][1][1];
      t136 = Ct[0][1][2]*Ctd[0][0][1];
      t137 = Ct[1][0][2]*Ctd[1][1][1];
      t139 = Ct[1][1][2]*Ctd[0][1][1];
      t141 = Ct[2][1][2]*Ctd[0][1][2];
      t149 = Ctd[1][2][2]+Ctd[2][1][2];
      t164 = Ct[0][1][2]*Ctd[0][0][2];
      t165 = Ct[1][0][2]*Ctd[1][1][2];
      t167 = Ct[1][1][2]*Ctd[0][1][2];
      t169 = Ct[2][1][2]*Ctd[0][2][2];
      MapleGenVar1 = Gamh[0]*t82+Gamh[1]*t84+Gamh[2]*t86+gtu[0][0]*(Ct[0][0][0]
*t82+Ct[0][0][1]*Ctd[0][0][0]+2.0*Ct[1][0][0]*Ctd[1][0][1]+Ct[1][0][1]*Ctd[0]
[0][1]+Ct[2][0][0]*t93+Ct[2][0][1]*Ctd[0][0][2])+gtu[0][1]*(Ct[0][0][1]*t82+Ct
[0][1][1]*Ctd[0][0][0]+2.0*Ct[1][0][1]*Ctd[1][0][1]+Ct[1][1][1]*Ctd[0][0][1]+Ct
[2][0][1]*t93+Ct[2][1][1]*Ctd[0][0][2])+gtu[0][2]*(Ct[0][0][2]*t82+t108+2.0*
t109+t111+Ct[2][0][2]*t93+t113);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t84+Ct[0][0][1]*Ctd[0]
[0][1]+2.0*Ct[1][0][0]*Ctd[1][1][1]+Ct[1][0][1]*Ctd[0][1][1]+Ct[2][0][0]*t121+
Ct[2][0][1]*Ctd[0][1][2])+gtu[1][1]*(Ct[0][0][1]*t84+Ct[0][1][1]*Ctd[0][0][1]+
2.0*Ct[1][0][1]*Ctd[1][1][1]+Ct[1][1][1]*Ctd[0][1][1]+Ct[2][0][1]*t121+Ct[2][1]
[1]*Ctd[0][1][2]);
      t172 = MapleGenVar2+gtu[1][2]*(Ct[0][0][2]*t84+t136+2.0*t137+t139+Ct[2]
[0][2]*t121+t141)+gtu[0][2]*(Ct[0][0][0]*t86+Ct[0][0][1]*Ctd[0][0][2]+2.0*Ct[1]
[0][0]*Ctd[1][1][2]+Ct[1][0][1]*Ctd[0][1][2]+Ct[2][0][0]*t149+Ct[2][0][1]*Ctd
[0][2][2])+gtu[1][2]*(Ct[0][0][1]*t86+Ct[0][1][1]*Ctd[0][0][2]+2.0*Ct[1][0][1]*
Ctd[1][1][2]+Ct[1][1][1]*Ctd[0][1][2]+Ct[2][0][1]*t149+Ct[2][1][1]*Ctd[0][2][2]
)+gtu[2][2]*(Ct[0][0][2]*t86+t164+2.0*t165+t167+Ct[2][0][2]*t149+t169);
      t188 = fourth*t172-half*(gtu[0][0]*dd_gtd[0][0][0][1]+gtu[1][1]*dd_gtd[1]
[1][0][1]+gtu[2][2]*dd_gtd[2][2][0][1]-gtd[0][0]*d_Gamh[1][0]-gtd[0][1]*d_Gamh
[0][0]-gtd[0][1]*d_Gamh[1][1]-gtd[1][1]*d_Gamh[0][1]-gtd[0][2]*d_Gamh[1][2]-gtd
[1][2]*d_Gamh[0][2])-gtu[0][1]*dd_gtd[0][1][0][1]-gtu[0][2]*dd_gtd[0][2][0][1]-
gtu[1][2]*dd_gtd[1][2][0][1];
      t189 = Ctd[0][0][2]+Ctd[2][0][0];
      t191 = Ctd[0][1][2]+Ctd[2][0][1];
      t193 = Ctd[0][2][2]+Ctd[2][0][2];
      t204 = Ct[0][0][1]*t189;
      t205 = Ct[1][0][1]*t93;
      t207 = 2.0*Ct[2][0][1]*Ctd[2][0][2];
      t228 = Ct[0][0][1]*t191;
      t229 = Ct[1][0][1]*t121;
      t231 = 2.0*Ct[2][0][1]*Ctd[2][1][2];
      t252 = Ct[0][0][1]*t193;
      t253 = Ct[1][0][1]*t149;
      t255 = 2.0*Ct[2][0][1]*Ctd[2][2][2];
      MapleGenVar1 = Gamh[0]*t189+Gamh[1]*t191+Gamh[2]*t193+gtu[0][0]*(Ct[0][0]
[0]*t189+Ct[0][0][2]*Ctd[0][0][0]+Ct[1][0][0]*t93+Ct[1][0][2]*Ctd[0][0][1]+2.0*
Ct[2][0][0]*Ctd[2][0][2]+Ct[2][0][2]*Ctd[0][0][2])+gtu[0][1]*(t204+t108+t205+
t111+t207+t113)+gtu[0][2]*(Ct[0][0][2]*t189+Ct[0][2][2]*Ctd[0][0][0]+Ct[1][0]
[2]*t93+Ct[1][2][2]*Ctd[0][0][1]+2.0*Ct[2][0][2]*Ctd[2][0][2]+Ct[2][2][2]*Ctd
[0][0][2]);
      t267 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t191+Ct[0][0][2]*Ctd[0][0][1]+
Ct[1][0][0]*t121+Ct[1][0][2]*Ctd[0][1][1]+2.0*Ct[2][0][0]*Ctd[2][1][2]+Ct[2][0]
[2]*Ctd[0][1][2])+gtu[1][1]*(t228+t136+t229+t139+t231+t141)+gtu[1][2]*(Ct[0][0]
[2]*t191+Ct[0][2][2]*Ctd[0][0][1]+Ct[1][0][2]*t121+Ct[1][2][2]*Ctd[0][1][1]+2.0
*Ct[2][0][2]*Ctd[2][1][2]+Ct[2][2][2]*Ctd[0][1][2])+gtu[0][2]*(Ct[0][0][0]*t193
+Ct[0][0][2]*Ctd[0][0][2]+Ct[1][0][0]*t149+Ct[1][0][2]*Ctd[0][1][2]+2.0*Ct[2]
[0][0]*Ctd[2][2][2]+Ct[2][0][2]*Ctd[0][2][2])+gtu[1][2]*(t252+t164+t253+t167+
t255+t169)+gtu[2][2]*(Ct[0][0][2]*t193+Ct[0][2][2]*Ctd[0][0][2]+Ct[1][0][2]*
t149+Ct[1][2][2]*Ctd[0][1][2]+2.0*Ct[2][0][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[0]
[2][2]);
      t283 = fourth*t267-half*(gtu[0][0]*dd_gtd[0][0][0][2]+gtu[1][1]*dd_gtd[1]
[1][0][2]+gtu[2][2]*dd_gtd[2][2][0][2]-gtd[0][0]*d_Gamh[2][0]-gtd[0][2]*d_Gamh
[0][0]-gtd[0][1]*d_Gamh[2][1]-gtd[1][2]*d_Gamh[0][1]-gtd[0][2]*d_Gamh[2][2]-gtd
[2][2]*d_Gamh[0][2])-gtu[0][1]*dd_gtd[0][1][0][2]-gtu[0][2]*dd_gtd[0][2][0][2]-
gtu[1][2]*dd_gtd[1][2][0][2];
      t290 = two*Ctd[1][0][0]+Ctd[0][0][1];
      t292 = two*Ctd[1][0][1];
      t293 = t292+Ctd[1][0][1];
      t295 = two*Ctd[1][0][2];
      t296 = t295+Ctd[2][0][1];
      t310 = t292+Ctd[0][1][1];
      t313 = two*Ctd[1][1][1]+Ctd[1][1][1];
      t315 = two*Ctd[1][1][2];
      t316 = t315+Ctd[2][1][1];
      t330 = t295+Ctd[0][1][2];
      t332 = t315+Ctd[1][1][2];
      t335 = two*Ctd[1][2][2]+Ctd[2][1][2];
      MapleGenVar1 = Gamh[0]*t93+Gamh[1]*t121+Gamh[2]*t149+gtu[0][0]*(t204+Ct
[0][0][2]*Ctd[1][0][0]+t205+t109+t207+Ct[2][0][2]*Ctd[1][0][2])+gtu[0][1]*(Ct
[0][1][1]*t189+Ct[0][1][2]*Ctd[1][0][0]+Ct[1][1][1]*t93+Ct[1][1][2]*Ctd[1][0]
[1]+2.0*Ct[2][1][1]*Ctd[2][0][2]+Ct[2][1][2]*Ctd[1][0][2])+gtu[0][2]*(Ct[0][1]
[2]*t189+Ct[0][2][2]*Ctd[1][0][0]+Ct[1][1][2]*t93+Ct[1][2][2]*Ctd[1][0][1]+2.0*
Ct[2][1][2]*Ctd[2][0][2]+Ct[2][2][2]*Ctd[1][0][2]);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(t228+Ct[0][0][2]*Ctd[1][0][1]+t229
+t137+t231+Ct[2][0][2]*Ctd[1][1][2])+gtu[1][1]*(Ct[0][1][1]*t191+Ct[0][1][2]*
Ctd[1][0][1]+Ct[1][1][1]*t121+Ct[1][1][2]*Ctd[1][1][1]+2.0*Ct[2][1][1]*Ctd[2]
[1][2]+Ct[2][1][2]*Ctd[1][1][2]);
      t434 = MapleGenVar2+gtu[1][2]*(Ct[0][1][2]*t191+Ct[0][2][2]*Ctd[1][0][1]+
Ct[1][1][2]*t121+Ct[1][2][2]*Ctd[1][1][1]+2.0*Ct[2][1][2]*Ctd[2][1][2]+Ct[2][2]
[2]*Ctd[1][1][2])+gtu[0][2]*(t252+Ct[0][0][2]*Ctd[1][0][2]+t253+t165+t255+Ct[2]
[0][2]*Ctd[1][2][2])+gtu[1][2]*(Ct[0][1][1]*t193+Ct[0][1][2]*Ctd[1][0][2]+Ct[1]
[1][1]*t149+Ct[1][1][2]*Ctd[1][1][2]+2.0*Ct[2][1][1]*Ctd[2][2][2]+Ct[2][1][2]*
Ctd[1][2][2])+gtu[2][2]*(Ct[0][1][2]*t193+Ct[0][2][2]*Ctd[1][0][2]+Ct[1][1][2]*
t149+Ct[1][2][2]*Ctd[1][1][2]+2.0*Ct[2][1][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[1]
[2][2]);
      t450 = fourth*t434-half*(gtu[0][0]*dd_gtd[0][0][1][2]+gtu[1][1]*dd_gtd[1]
[1][1][2]+gtu[2][2]*dd_gtd[2][2][1][2]-gtd[0][1]*d_Gamh[2][0]-gtd[0][2]*d_Gamh
[1][0]-gtd[1][1]*d_Gamh[2][1]-gtd[1][2]*d_Gamh[1][1]-gtd[1][2]*d_Gamh[2][2]-gtd
[2][2]*d_Gamh[1][2])-gtu[0][1]*dd_gtd[0][1][1][2]-gtu[0][2]*dd_gtd[0][2][1][2]-
gtu[1][2]*dd_gtd[1][2][1][2];
      t457 = two*Ctd[2][0][0]+Ctd[0][0][2];
      t459 = two*Ctd[2][0][1];
      t460 = t459+Ctd[1][0][2];
      t462 = two*Ctd[2][0][2];
      t463 = t462+Ctd[2][0][2];
      t477 = t459+Ctd[0][1][2];
      t480 = two*Ctd[2][1][1]+Ctd[1][1][2];
      t482 = two*Ctd[2][1][2];
      t483 = t482+Ctd[2][1][2];
      t497 = t462+Ctd[0][2][2];
      t499 = t482+Ctd[1][2][2];
      t502 = two*Ctd[2][2][2]+Ctd[2][2][2];
      MapleGenVar2 = fourth*(two*(Gamh[0]*Ctd[0][0][0]+Gamh[1]*Ctd[0][0][1]+
Gamh[2]*Ctd[0][0][2])+gtu[0][0]*(Ct[0][0][0]*t7+Ct[1][0][0]*t10+Ct[2][0][0]*t13
)+gtu[0][1]*(Ct[0][0][1]*t7+Ct[1][0][1]*t10+Ct[2][0][1]*t13)+gtu[0][2]*(Ct[0]
[0][2]*t7+Ct[1][0][2]*t10+Ct[2][0][2]*t13)+gtu[0][1]*(Ct[0][0][0]*t27+Ct[1][0]
[0]*t30+Ct[2][0][0]*t33)+gtu[1][1]*(Ct[0][0][1]*t27+Ct[1][0][1]*t30+Ct[2][0][1]
*t33)+gtu[1][2]*(Ct[0][0][2]*t27+Ct[1][0][2]*t30+Ct[2][0][2]*t33)+gtu[0][2]*(Ct
[0][0][0]*t47+Ct[1][0][0]*t49+Ct[2][0][0]*t52)+gtu[1][2]*(Ct[0][0][1]*t47+Ct[1]
[0][1]*t49+Ct[2][0][1]*t52)+gtu[2][2]*(Ct[0][0][2]*t47+Ct[1][0][2]*t49+Ct[2][0]
[2]*t52));
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][0][0]+gtu[1][1]*dd_gtd[1][1]
[0][0]+gtu[2][2]*dd_gtd[2][2][0][0]-two*(gtd[0][0]*d_Gamh[0][0]+gtd[0][1]*
d_Gamh[0][1]+gtd[0][2]*d_Gamh[0][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[0][0] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][0][0]-gtu[0][2]*dd_gtd
[0][2][0][0]-gtu[1][2]*dd_gtd[1][2][0][0];
      RDZhd[0][1] = t188;
      RDZhd[0][2] = t283;
      RDZhd[1][0] = t188;
      MapleGenVar3 = fourth;
      MapleGenVar5 = two*(Gamh[0]*Ctd[1][0][1]+Gamh[1]*Ctd[1][1][1]+Gamh[2]*Ctd
[1][1][2])+gtu[0][0]*(Ct[0][0][1]*t290+Ct[1][0][1]*t293+Ct[2][0][1]*t296)+gtu
[0][1]*(Ct[0][1][1]*t290+Ct[1][1][1]*t293+Ct[2][1][1]*t296)+gtu[0][2]*(Ct[0][1]
[2]*t290+Ct[1][1][2]*t293+Ct[2][1][2]*t296)+gtu[0][1]*(Ct[0][0][1]*t310+Ct[1]
[0][1]*t313+Ct[2][0][1]*t316);
      MapleGenVar4 = MapleGenVar5+gtu[1][1]*(Ct[0][1][1]*t310+Ct[1][1][1]*t313+
Ct[2][1][1]*t316)+gtu[1][2]*(Ct[0][1][2]*t310+Ct[1][1][2]*t313+Ct[2][1][2]*t316
)+gtu[0][2]*(Ct[0][0][1]*t330+Ct[1][0][1]*t332+Ct[2][0][1]*t335)+gtu[1][2]*(Ct
[0][1][1]*t330+Ct[1][1][1]*t332+Ct[2][1][1]*t335)+gtu[2][2]*(Ct[0][1][2]*t330+
Ct[1][1][2]*t332+Ct[2][1][2]*t335);
      MapleGenVar2 = MapleGenVar3*MapleGenVar4;
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][1][1]+gtu[1][1]*dd_gtd[1][1]
[1][1]+gtu[2][2]*dd_gtd[2][2][1][1]-two*(gtd[0][1]*d_Gamh[1][0]+gtd[1][1]*
d_Gamh[1][1]+gtd[1][2]*d_Gamh[1][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[1][1] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][1][1]-gtu[0][2]*dd_gtd
[0][2][1][1]-gtu[1][2]*dd_gtd[1][2][1][1];
      RDZhd[1][2] = t450;
      RDZhd[2][0] = t283;
      RDZhd[2][1] = t450;
      MapleGenVar3 = fourth;
      MapleGenVar5 = two*(Gamh[0]*Ctd[2][0][2]+Gamh[1]*Ctd[2][1][2]+Gamh[2]*Ctd
[2][2][2])+gtu[0][0]*(Ct[0][0][2]*t457+Ct[1][0][2]*t460+Ct[2][0][2]*t463)+gtu
[0][1]*(Ct[0][1][2]*t457+Ct[1][1][2]*t460+Ct[2][1][2]*t463)+gtu[0][2]*(Ct[0][2]
[2]*t457+Ct[1][2][2]*t460+Ct[2][2][2]*t463)+gtu[0][1]*(Ct[0][0][2]*t477+Ct[1]
[0][2]*t480+Ct[2][0][2]*t483);
      MapleGenVar4 = MapleGenVar5+gtu[1][1]*(Ct[0][1][2]*t477+Ct[1][1][2]*t480+
Ct[2][1][2]*t483)+gtu[1][2]*(Ct[0][2][2]*t477+Ct[1][2][2]*t480+Ct[2][2][2]*t483
)+gtu[0][2]*(Ct[0][0][2]*t497+Ct[1][0][2]*t499+Ct[2][0][2]*t502)+gtu[1][2]*(Ct
[0][1][2]*t497+Ct[1][1][2]*t499+Ct[2][1][2]*t502)+gtu[2][2]*(Ct[0][2][2]*t497+
Ct[1][2][2]*t499+Ct[2][2][2]*t502);
      MapleGenVar2 = MapleGenVar3*MapleGenVar4;
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][2][2]+gtu[1][1]*dd_gtd[1][1]
[2][2]+gtu[2][2]*dd_gtd[2][2][2][2]-two*(gtd[0][2]*d_Gamh[2][0]+gtd[1][2]*
d_Gamh[2][1]+gtd[2][2]*d_Gamh[2][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[2][2] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][2][2]-gtu[0][2]*dd_gtd
[0][2][2][2]-gtu[1][2]*dd_gtd[1][2][2][2];
      t8 = p_expo*inv_psi;
      t13 = psi_p*eight_pi_G;
      t32 = inv_psi_p*(Alpha*(RDZhd[0][1]+RDZpd_1[0][1])-dd_Alpha[0][1]+half*(
Ct[0][0][1]*d_Alpha[0]+Ct[1][0][1]*d_Alpha[1]+Ct[2][0][1]*d_Alpha[2]+t8*(
d_Alpha[0]*d_psi[1]+d_Alpha[1]*d_psi[0])))-t13*Alpha*pTtd_ADM[0][1];
      t48 = inv_psi_p*(Alpha*(RDZhd[0][2]+RDZpd_1[0][2])-dd_Alpha[0][2]+half*(
Ct[0][0][2]*d_Alpha[0]+Ct[1][0][2]*d_Alpha[1]+Ct[2][0][2]*d_Alpha[2]+t8*(
d_Alpha[0]*d_psi[2]+d_Alpha[2]*d_psi[0])))-t13*Alpha*pTtd_ADM[0][2];
      t78 = inv_psi_p*(Alpha*(RDZhd[1][2]+RDZpd_1[1][2])-dd_Alpha[1][2]+half*(
Ct[0][1][2]*d_Alpha[0]+Ct[1][1][2]*d_Alpha[1]+Ct[2][1][2]*d_Alpha[2]+t8*(
d_Alpha[1]*d_psi[2]+d_Alpha[2]*d_psi[1])))-t13*Alpha*pTtd_ADM[1][2];
      Psi1[0][0] = inv_psi_p*(Alpha*(RDZhd[0][0]+RDZpd_1[0][0])-dd_Alpha[0][0]+
half*(Ct[0][0][0]*d_Alpha[0]+Ct[1][0][0]*d_Alpha[1]+Ct[2][0][0]*d_Alpha[2])+t8*
d_Alpha[0]*d_psi[0])-t13*Alpha*pTtd_ADM[0][0];
      Psi1[0][1] = t32;
      Psi1[0][2] = t48;
      Psi1[1][0] = t32;
      Psi1[1][1] = inv_psi_p*(Alpha*(RDZhd[1][1]+RDZpd_1[1][1])-dd_Alpha[1][1]+
half*(Ct[0][1][1]*d_Alpha[0]+Ct[1][1][1]*d_Alpha[1]+Ct[2][1][1]*d_Alpha[2])+t8*
d_Alpha[1]*d_psi[1])-t13*Alpha*pTtd_ADM[1][1];
      Psi1[1][2] = t78;
      Psi1[2][0] = t48;
      Psi1[2][1] = t78;
      Psi1[2][2] = inv_psi_p*(Alpha*(RDZhd[2][2]+RDZpd_1[2][2])-dd_Alpha[2][2]+
half*(Ct[0][2][2]*d_Alpha[0]+Ct[1][2][2]*d_Alpha[1]+Ct[2][2][2]*d_Alpha[2])+t8*
d_Alpha[2]*d_psi[2])-t13*Alpha*pTtd_ADM[2][2];
      third_trPsi1 = third*(Psi1[0][0]*gtu[0][0]+Psi1[1][1]*gtu[1][1]+Psi1[2]
[2]*gtu[2][2]+two*(Psi1[0][1]*gtu[0][1]+Psi1[0][2]*gtu[0][2]+Psi1[1][2]*gtu[1]
[2]));
      t4 = Psi1[0][1]-third_trPsi1*gtd[0][1];
      t6 = Psi1[0][2]-third_trPsi1*gtd[0][2];
      t10 = Psi1[1][2]-third_trPsi1*gtd[1][2];
      Psi1TF[0][0] = Psi1[0][0]-third_trPsi1*gtd[0][0];
      Psi1TF[0][1] = t4;
      Psi1TF[0][2] = t6;
      Psi1TF[1][0] = t4;
      Psi1TF[1][1] = Psi1[1][1]-third_trPsi1*gtd[1][1];
      Psi1TF[1][2] = t10;
      Psi1TF[2][0] = t6;
      Psi1TF[2][1] = t10;
      Psi1TF[2][2] = Psi1[2][2]-third_trPsi1*gtd[2][2];
      t4 = third*div_Beta;
      t19 = gtd[0][0]*d_Betau[1][0]+gtd[0][1]*d_Betau[0][0]+gtd[0][1]*d_Betau
[1][1]+gtd[1][1]*d_Betau[0][1]+gtd[0][2]*d_Betau[1][2]+gtd[1][2]*d_Betau[0][2]-
two*(t4*gtd[0][1]+Alpha*Atd[0][1]);
      t30 = gtd[0][0]*d_Betau[2][0]+gtd[0][2]*d_Betau[0][0]+gtd[0][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[0][1]+gtd[0][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[0][2]-
two*(t4*gtd[0][2]+Alpha*Atd[0][2]);
      t48 = gtd[0][1]*d_Betau[2][0]+gtd[0][2]*d_Betau[1][0]+gtd[1][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[1][1]+gtd[1][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[1][2]-
two*(t4*gtd[1][2]+Alpha*Atd[1][2]);
      gtd_rhs[0][0] = two*(gtd[0][0]*d_Betau[0][0]+gtd[0][1]*d_Betau[0][1]+gtd
[0][2]*d_Betau[0][2]-t4*gtd[0][0]-Alpha*Atd[0][0]);
      gtd_rhs[0][1] = t19;
      gtd_rhs[0][2] = t30;
      gtd_rhs[1][0] = t19;
      gtd_rhs[1][1] = two*(gtd[0][1]*d_Betau[1][0]+gtd[1][1]*d_Betau[1][1]+gtd
[1][2]*d_Betau[1][2]-t4*gtd[1][1]-Alpha*Atd[1][1]);
      gtd_rhs[1][2] = t48;
      gtd_rhs[2][0] = t30;
      gtd_rhs[2][1] = t48;
      gtd_rhs[2][2] = two*(gtd[0][2]*d_Betau[2][0]+gtd[1][2]*d_Betau[2][1]+gtd
[2][2]*d_Betau[2][2]-t4*gtd[2][2]-Alpha*Atd[2][2]);
      t16 = Alpha*(trK-two*Theta)-twothirds*div_Beta;
      t19 = two*Alpha;
      t33 = Atd[0][0]*(d_Betau[1][0]-t19*Atud[0][1])+Atd[0][1]*d_Betau[0][0]+
Atd[0][1]*(d_Betau[1][1]-t19*Atud[1][1])+Atd[1][1]*d_Betau[0][1]+Atd[0][2]*(
d_Betau[1][2]-t19*Atud[2][1])+Atd[1][2]*d_Betau[0][2]+Atd[0][1]*t16+Psi1TF[0]
[1];
      t35 = d_Betau[2][0]-t19*Atud[0][2];
      t39 = d_Betau[2][1]-t19*Atud[1][2];
      t43 = d_Betau[2][2]-t19*Atud[2][2];
      t47 = Atd[0][0]*t35+Atd[0][2]*d_Betau[0][0]+Atd[0][1]*t39+Atd[1][2]*
d_Betau[0][1]+Atd[0][2]*t43+Atd[2][2]*d_Betau[0][2]+Atd[0][2]*t16+Psi1TF[0][2];
      t68 = Atd[0][1]*t35+Atd[0][2]*d_Betau[1][0]+Atd[1][1]*t39+Atd[1][2]*
d_Betau[1][1]+Atd[1][2]*t43+Atd[2][2]*d_Betau[1][2]+Atd[1][2]*t16+Psi1TF[1][2];
      Atd_rhs[0][0] = two*(Atd[0][0]*(d_Betau[0][0]-Alpha*Atud[0][0])+Atd[0][1]
*(d_Betau[0][1]-Alpha*Atud[1][0])+Atd[0][2]*(d_Betau[0][2]-Alpha*Atud[2][0]))+
Atd[0][0]*t16+Psi1TF[0][0];
      Atd_rhs[0][1] = t33;
      Atd_rhs[0][2] = t47;
      Atd_rhs[1][0] = t33;
      Atd_rhs[1][1] = two*(Atd[0][1]*(d_Betau[1][0]-Alpha*Atud[0][1])+Atd[1][1]
*(d_Betau[1][1]-Alpha*Atud[1][1])+Atd[1][2]*(d_Betau[1][2]-Alpha*Atud[2][1]))+
Atd[1][1]*t16+Psi1TF[1][1];
      Atd_rhs[1][2] = t68;
      Atd_rhs[2][0] = t47;
      Atd_rhs[2][1] = t68;
      Atd_rhs[2][2] = two*(Atd[0][2]*(d_Betau[2][0]-Alpha*Atud[0][2])+Atd[1][2]
*(d_Betau[2][1]-Alpha*Atud[1][2])+Atd[2][2]*(d_Betau[2][2]-Alpha*Atud[2][2]))+
Atd[2][2]*t16+Psi1TF[2][2];
      t1 = twothirds*div_Beta;
      t35 = eight_pi_G*psi_p;
      t39 = sigma_1+twothirds*trK;
      t48 = threehalves*p_expo;
      t49 = Alpha*inv_psi;
      t52 = t48*t49*d_psi[0]-d_Alpha[0];
      t56 = t48*t49*d_psi[1]-d_Alpha[1];
      t60 = t48*t49*d_psi[2]-d_Alpha[2];
      MapleGenVar1 = t1*Gamh[0]-Gamh[0]*d_Betau[0][0]-Gamh[1]*d_Betau[1][0]-
Gamh[2]*d_Betau[2][0]+gtu[0][0]*dd_Betau[0][0][0];
      MapleGenVar2 = MapleGenVar1+gtu[1][1]*dd_Betau[1][1][0]+gtu[2][2]*
dd_Betau[2][2][0];
      Gamh_rhs[0] = MapleGenVar2+two*(gtu[0][1]*dd_Betau[0][1][0]+gtu[0][2]*
dd_Betau[0][2][0]+gtu[1][2]*dd_Betau[1][2][0])+third*(gtu[0][0]*d_div_Beta[0]+
gtu[0][1]*d_div_Beta[1]+gtu[0][2]*d_div_Beta[2])+two*(Alpha*(half*(Ct[0][0][0]*
Atu[0][0]+Ct[0][1][1]*Atu[1][1]+Ct[0][2][2]*Atu[2][2])+Ct[0][0][1]*Atu[0][1]+Ct
[0][0][2]*Atu[0][2]+Ct[0][1][2]*Atu[1][2]+gtu[0][0]*d_Theta[0]+gtu[0][1]*
d_Theta[1]+gtu[0][2]*d_Theta[2]-twothirds*(gtu[0][0]*d_trK[0]+gtu[0][1]*d_trK
[1]+gtu[0][2]*d_trK[2])-t35*Ju_ADM[0]-psi_p*Zu[0]*t39)-Theta*(gtu[0][0]*d_Alpha
[0]+gtu[0][1]*d_Alpha[1]+gtu[0][2]*d_Alpha[2])+Atu[0][0]*t52+Atu[0][1]*t56+Atu
[0][2]*t60);
      MapleGenVar1 = t1*Gamh[1]-Gamh[0]*d_Betau[0][1]-Gamh[1]*d_Betau[1][1]-
Gamh[2]*d_Betau[2][1]+gtu[0][0]*dd_Betau[0][0][1];
      MapleGenVar2 = MapleGenVar1+gtu[1][1]*dd_Betau[1][1][1]+gtu[2][2]*
dd_Betau[2][2][1];
      Gamh_rhs[1] = MapleGenVar2+two*(gtu[0][1]*dd_Betau[0][1][1]+gtu[0][2]*
dd_Betau[0][2][1]+gtu[1][2]*dd_Betau[1][2][1])+third*(gtu[0][1]*d_div_Beta[0]+
gtu[1][1]*d_div_Beta[1]+gtu[1][2]*d_div_Beta[2])+two*(Alpha*(half*(Ct[1][0][0]*
Atu[0][0]+Ct[1][1][1]*Atu[1][1]+Ct[1][2][2]*Atu[2][2])+Ct[1][0][1]*Atu[0][1]+Ct
[1][0][2]*Atu[0][2]+Ct[1][1][2]*Atu[1][2]+gtu[0][1]*d_Theta[0]+gtu[1][1]*
d_Theta[1]+gtu[1][2]*d_Theta[2]-twothirds*(gtu[0][1]*d_trK[0]+gtu[1][1]*d_trK
[1]+gtu[1][2]*d_trK[2])-t35*Ju_ADM[1]-psi_p*Zu[1]*t39)-Theta*(gtu[0][1]*d_Alpha
[0]+gtu[1][1]*d_Alpha[1]+gtu[1][2]*d_Alpha[2])+Atu[0][1]*t52+Atu[1][1]*t56+Atu
[1][2]*t60);
      MapleGenVar1 = t1*Gamh[2]-Gamh[0]*d_Betau[0][2]-Gamh[1]*d_Betau[1][2]-
Gamh[2]*d_Betau[2][2]+gtu[0][0]*dd_Betau[0][0][2];
      MapleGenVar2 = MapleGenVar1+gtu[1][1]*dd_Betau[1][1][2]+gtu[2][2]*
dd_Betau[2][2][2];
      Gamh_rhs[2] = MapleGenVar2+two*(gtu[0][1]*dd_Betau[0][1][2]+gtu[0][2]*
dd_Betau[0][2][2]+gtu[1][2]*dd_Betau[1][2][2])+third*(gtu[0][2]*d_div_Beta[0]+
gtu[1][2]*d_div_Beta[1]+gtu[2][2]*d_div_Beta[2])+two*(Alpha*(half*(Ct[2][0][0]*
Atu[0][0]+Ct[2][1][1]*Atu[1][1]+Ct[2][2][2]*Atu[2][2])+Ct[2][0][1]*Atu[0][1]+Ct
[2][0][2]*Atu[0][2]+Ct[2][1][2]*Atu[1][2]+gtu[0][2]*d_Theta[0]+gtu[1][2]*
d_Theta[1]+gtu[2][2]*d_Theta[2]-twothirds*(gtu[0][2]*d_trK[0]+gtu[1][2]*d_trK
[1]+gtu[2][2]*d_trK[2])-t35*Ju_ADM[2]-psi_p*Zu[2]*t39)-Theta*(gtu[0][2]*d_Alpha
[0]+gtu[1][2]*d_Alpha[1]+gtu[2][2]*d_Alpha[2])+Atu[0][2]*t52+Atu[1][2]*t56+Atu
[2][2]*t60);
      t3 = threefourths*(lambda_f0+lambda_f1*Alpha);
      Betau_rhs[0] = t3*Bu[0];
      Betau_rhs[1] = t3*Bu[1];
      Betau_rhs[2] = t3*Bu[2];
      Bu_rhs[0] = Gamh_rhs[0]-Bu[0]*feta;
      Bu_rhs[1] = Gamh_rhs[1]-Bu[1]*feta;
      Bu_rhs[2] = Gamh_rhs[2]-Bu[2]*feta;
      t2 = two*Theta;
      Alpha_rhs = -two*Alpha*(trK-t2-trK0);
      psi_rhs = twothirds*psi*(div_Beta-Alpha*trK)/p_expo;
      t10 = gtu[0][0];
      t13 = gtu[1][1];
      t16 = gtu[2][2];
      t19 = gtu[0][1];
      t22 = gtu[0][2];
      t25 = gtu[1][2];
      tr_pT = psi_p*(t10*pTtd_ADM[0][0]+t13*pTtd_ADM[1][1]+t16*pTtd_ADM[2][2]+
two*(t19*pTtd_ADM[0][1]+t22*pTtd_ADM[0][2]+t25*pTtd_ADM[1][2]));
      t58 = inv_psi_p*((RDZpd[0][0]+RDZhd[0][0])*t10+(RDZpd[1][1]+RDZhd[1][1])*
t13+(RDZpd[2][2]+RDZhd[2][2])*t16+two*((RDZpd[0][1]+RDZhd[0][1])*t19+(RDZpd[0]
[2]+RDZhd[0][2])*t22+(RDZpd[1][2]+RDZhd[1][2])*t25));
      t85 = d_Alpha[0];
      t88 = d_Alpha[1];
      t91 = d_Alpha[2];
      t95 = d_psi[0];
      t98 = d_psi[1];
      t101 = d_psi[2];
      trK_rhs = Alpha*(t58+trK*(trK-t2)-three*sigma_1*(one+sigma_2)*Theta+
four_pi_G*(tr_pT-three*rho_ADM))-inv_psi_p*(t10*dd_Alpha[0][0]+t13*dd_Alpha[1]
[1]+t16*dd_Alpha[2][2]+two*(t19*dd_Alpha[0][1]+t22*dd_Alpha[0][2]+t25*dd_Alpha
[1][2])-CalGamTil[0]*t85-CalGamTil[1]*t88-CalGamTil[2]*t91+half*p_expo*inv_psi*
(t10*t85*t95+t19*t85*t98+t22*t85*t101+t19*t88*t95+t13*t88*t98+t25*t88*t101+t22*
t91*t95+t25*t91*t98+t16*t91*t101));
      Theta_rhs = half*Alpha*(t58+two*trK*(third*trK-Theta)-Atd[0][0]*Atu[0][0]
-Atd[1][1]*Atu[1][1]-Atd[2][2]*Atu[2][2]-two*(Atd[0][1]*Atu[0][1]+Atd[0][2]*Atu
[0][2]+Atd[1][2]*Atu[1][2]))-Zu[0]*t85-Zu[1]*t88-Zu[2]*t91-Alpha*(sigma_1*(two+
sigma_2)*Theta+eight_pi_G*rho_ADM);
