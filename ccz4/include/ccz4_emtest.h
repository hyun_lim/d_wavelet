      inv_psi = 0.1E1/psi;
      psi_p = pow(psi,1.0*p_expo);
      inv_psi_p = 0.1E1/psi_p;
      inv_Alpha = 0.1E1/Alpha;
      t1 = gtd[0][0];
      t2 = gtd[1][1];
      t4 = gtd[2][2];
      t6 = gtd[1][2];
      t7 = t6*t6;
      t9 = gtd[0][1];
      t10 = t9*t9;
      t12 = gtd[0][2];
      t16 = t12*t12;
      detgtd = t1*t2*t4-t1*t7-t10*t4+2.0*t9*t12*t6-t16*t2;
      idetgtd = 0.1E1/detgtd;
      t2 = gtd[1][2]*gtd[1][2];
      t8 = idetgtd*(-gtd[0][1]*gtd[2][2]+gtd[0][2]*gtd[1][2]);
      t12 = idetgtd*(gtd[0][1]*gtd[1][2]-gtd[0][2]*gtd[1][1]);
      t14 = gtd[0][2]*gtd[0][2];
      t20 = idetgtd*(-gtd[0][0]*gtd[1][2]+gtd[0][1]*gtd[0][2]);
      t22 = gtd[0][1]*gtd[0][1];
      gtu[0][0] = idetgtd*(gtd[1][1]*gtd[2][2]-t2);
      gtu[0][1] = t8;
      gtu[0][2] = t12;
      gtu[1][0] = t8;
      gtu[1][1] = idetgtd*(gtd[0][0]*gtd[2][2]-t14);
      gtu[1][2] = t20;
      gtu[2][0] = t12;
      gtu[2][1] = t20;
      gtu[2][2] = idetgtd*(gtd[0][0]*gtd[1][1]-t22);
      t2 = gtu[0][1]*Atd[0][1];
      t3 = gtu[0][2]*Atd[0][2];
      t18 = gtu[1][2]*Atd[1][2];
      Atud[0][0] = gtu[0][0]*Atd[0][0]+t2+t3;
      Atud[0][1] = gtu[0][0]*Atd[0][1]+gtu[0][1]*Atd[1][1]+gtu[0][2]*Atd[1][2];
      Atud[0][2] = gtu[0][0]*Atd[0][2]+gtu[0][1]*Atd[1][2]+gtu[0][2]*Atd[2][2];
      Atud[1][0] = gtu[0][1]*Atd[0][0]+gtu[1][1]*Atd[0][1]+gtu[1][2]*Atd[0][2];
      Atud[1][1] = t2+gtu[1][1]*Atd[1][1]+t18;
      Atud[1][2] = gtu[0][1]*Atd[0][2]+gtu[1][1]*Atd[1][2]+gtu[1][2]*Atd[2][2];
      Atud[2][0] = gtu[0][2]*Atd[0][0]+gtu[1][2]*Atd[0][1]+gtu[2][2]*Atd[0][2];
      Atud[2][1] = gtu[0][2]*Atd[0][1]+gtu[1][2]*Atd[1][1]+gtu[2][2]*Atd[1][2];
      Atud[2][2] = t3+t18+gtu[2][2]*Atd[2][2];
      t8 = Atud[0][0]*gtu[0][1]+Atud[0][1]*gtu[1][1]+Atud[0][2]*gtu[1][2];
      t12 = Atud[0][0]*gtu[0][2]+Atud[0][1]*gtu[1][2]+Atud[0][2]*gtu[2][2];
      t20 = Atud[1][0]*gtu[0][2]+Atud[1][1]*gtu[1][2]+Atud[1][2]*gtu[2][2];
      Atu[0][0] = Atud[0][0]*gtu[0][0]+Atud[0][1]*gtu[0][1]+Atud[0][2]*gtu[0]
[2];
      Atu[0][1] = t8;
      Atu[0][2] = t12;
      Atu[1][0] = t8;
      Atu[1][1] = Atud[1][0]*gtu[0][1]+Atud[1][1]*gtu[1][1]+Atud[1][2]*gtu[1]
[2];
      Atu[1][2] = t20;
      Atu[2][0] = t12;
      Atu[2][1] = t20;
      Atu[2][2] = Atud[2][0]*gtu[0][2]+Atud[2][1]*gtu[1][2]+Atud[2][2]*gtu[2]
[2];
      t3 = d_gtd[1][0][2]+d_gtd[2][0][1]-d_gtd[0][1][2];
      t8 = d_gtd[0][1][2]+d_gtd[2][0][1]-d_gtd[1][0][2];
      t13 = d_gtd[0][1][2]+d_gtd[1][0][2]-d_gtd[2][0][1];
      Ctd[0][0][0] = d_gtd[0][0][0];
      Ctd[0][0][1] = d_gtd[1][0][0];
      Ctd[0][0][2] = d_gtd[2][0][0];
      Ctd[0][1][0] = d_gtd[1][0][0];
      Ctd[0][1][1] = 2.0*d_gtd[1][0][1]-d_gtd[0][1][1];
      Ctd[0][1][2] = t3;
      Ctd[0][2][0] = d_gtd[2][0][0];
      Ctd[0][2][1] = t3;
      Ctd[0][2][2] = 2.0*d_gtd[2][0][2]-d_gtd[0][2][2];
      Ctd[1][0][0] = 2.0*d_gtd[0][0][1]-d_gtd[1][0][0];
      Ctd[1][0][1] = d_gtd[0][1][1];
      Ctd[1][0][2] = t8;
      Ctd[1][1][0] = d_gtd[0][1][1];
      Ctd[1][1][1] = d_gtd[1][1][1];
      Ctd[1][1][2] = d_gtd[2][1][1];
      Ctd[1][2][0] = t8;
      Ctd[1][2][1] = d_gtd[2][1][1];
      Ctd[1][2][2] = 2.0*d_gtd[2][1][2]-d_gtd[1][2][2];
      Ctd[2][0][0] = 2.0*d_gtd[0][0][2]-d_gtd[2][0][0];
      Ctd[2][0][1] = t13;
      Ctd[2][0][2] = d_gtd[0][2][2];
      Ctd[2][1][0] = t13;
      Ctd[2][1][1] = 2.0*d_gtd[1][1][2]-d_gtd[2][1][1];
      Ctd[2][1][2] = d_gtd[1][2][2];
      Ctd[2][2][0] = d_gtd[0][2][2];
      Ctd[2][2][1] = d_gtd[1][2][2];
      Ctd[2][2][2] = d_gtd[2][2][2];
      t8 = gtu[0][0]*Ctd[0][0][1]+gtu[0][1]*Ctd[1][0][1]+gtu[0][2]*Ctd[2][0][1]
;
      t12 = gtu[0][0]*Ctd[0][0][2]+gtu[0][1]*Ctd[1][0][2]+gtu[0][2]*Ctd[2][0]
[2];
      t20 = gtu[0][0]*Ctd[0][1][2]+gtu[0][1]*Ctd[1][1][2]+gtu[0][2]*Ctd[2][1]
[2];
      t32 = gtu[0][1]*Ctd[0][0][1]+gtu[1][1]*Ctd[1][0][1]+gtu[1][2]*Ctd[2][0]
[1];
      t36 = gtu[0][1]*Ctd[0][0][2]+gtu[1][1]*Ctd[1][0][2]+gtu[1][2]*Ctd[2][0]
[2];
      t44 = gtu[0][1]*Ctd[0][1][2]+gtu[1][1]*Ctd[1][1][2]+gtu[1][2]*Ctd[2][1]
[2];
      t56 = gtu[0][2]*Ctd[0][0][1]+gtu[1][2]*Ctd[1][0][1]+gtu[2][2]*Ctd[2][0]
[1];
      t60 = gtu[0][2]*Ctd[0][0][2]+gtu[1][2]*Ctd[1][0][2]+gtu[2][2]*Ctd[2][0]
[2];
      t68 = gtu[0][2]*Ctd[0][1][2]+gtu[1][2]*Ctd[1][1][2]+gtu[2][2]*Ctd[2][1]
[2];
      Ct[0][0][0] = gtu[0][0]*Ctd[0][0][0]+gtu[0][1]*Ctd[1][0][0]+gtu[0][2]*Ctd
[2][0][0];
      Ct[0][0][1] = t8;
      Ct[0][0][2] = t12;
      Ct[0][1][0] = t8;
      Ct[0][1][1] = gtu[0][0]*Ctd[0][1][1]+gtu[0][1]*Ctd[1][1][1]+gtu[0][2]*Ctd
[2][1][1];
      Ct[0][1][2] = t20;
      Ct[0][2][0] = t12;
      Ct[0][2][1] = t20;
      Ct[0][2][2] = gtu[0][0]*Ctd[0][2][2]+gtu[0][1]*Ctd[1][2][2]+gtu[0][2]*Ctd
[2][2][2];
      Ct[1][0][0] = gtu[0][1]*Ctd[0][0][0]+gtu[1][1]*Ctd[1][0][0]+gtu[1][2]*Ctd
[2][0][0];
      Ct[1][0][1] = t32;
      Ct[1][0][2] = t36;
      Ct[1][1][0] = t32;
      Ct[1][1][1] = gtu[0][1]*Ctd[0][1][1]+gtu[1][1]*Ctd[1][1][1]+gtu[1][2]*Ctd
[2][1][1];
      Ct[1][1][2] = t44;
      Ct[1][2][0] = t36;
      Ct[1][2][1] = t44;
      Ct[1][2][2] = gtu[0][1]*Ctd[0][2][2]+gtu[1][1]*Ctd[1][2][2]+gtu[1][2]*Ctd
[2][2][2];
      Ct[2][0][0] = gtu[0][2]*Ctd[0][0][0]+gtu[1][2]*Ctd[1][0][0]+gtu[2][2]*Ctd
[2][0][0];
      Ct[2][0][1] = t56;
      Ct[2][0][2] = t60;
      Ct[2][1][0] = t56;
      Ct[2][1][1] = gtu[0][2]*Ctd[0][1][1]+gtu[1][2]*Ctd[1][1][1]+gtu[2][2]*Ctd
[2][1][1];
      Ct[2][1][2] = t68;
      Ct[2][2][0] = t60;
      Ct[2][2][1] = t68;
      Ct[2][2][2] = gtu[0][2]*Ctd[0][2][2]+gtu[1][2]*Ctd[1][2][2]+gtu[2][2]*Ctd
[2][2][2];
      div_Beta = d_Betau[0][0]+d_Betau[1][1]+d_Betau[2][2];
      d_div_Beta[0] = dd_Betau[0][0][0]+dd_Betau[0][1][1]+dd_Betau[0][2][2];
      d_div_Beta[1] = dd_Betau[0][1][0]+dd_Betau[1][1][1]+dd_Betau[1][2][2];
      d_div_Beta[2] = dd_Betau[0][2][0]+dd_Betau[1][2][1]+dd_Betau[2][2][2];
      CalGamTil[0] = half*(gtu[0][0]*Ct[0][0][0]+gtu[1][1]*Ct[0][1][1]+gtu[2]
[2]*Ct[0][2][2])+gtu[0][1]*Ct[0][0][1]+gtu[0][2]*Ct[0][0][2]+gtu[1][2]*Ct[0][1]
[2];
      CalGamTil[1] = half*(gtu[0][0]*Ct[1][0][0]+gtu[1][1]*Ct[1][1][1]+gtu[2]
[2]*Ct[1][2][2])+gtu[0][1]*Ct[1][0][1]+gtu[0][2]*Ct[1][0][2]+gtu[1][2]*Ct[1][1]
[2];
      CalGamTil[2] = half*(gtu[0][0]*Ct[2][0][0]+gtu[1][1]*Ct[2][1][1]+gtu[2]
[2]*Ct[2][2][2])+gtu[0][1]*Ct[2][0][1]+gtu[0][2]*Ct[2][0][2]+gtu[1][2]*Ct[2][1]
[2];
      t1 = half*inv_psi_p;
      Zu[0] = t1*(Gamh[0]-CalGamTil[0]);
      Zu[1] = t1*(Gamh[1]-CalGamTil[1]);
      Zu[2] = t1*(Gamh[2]-CalGamTil[2]);
      t1 = p_expo*inv_psi;
      t2 = p_expo+two;
      t3 = d_psi[0]*d_psi[0];
      t12 = two*psi_p;
      t16 = gtd[0][0]*Zu[0]+gtd[0][1]*Zu[1]+gtd[0][2]*Zu[2];
      t21 = t2*d_psi[0];
      t33 = gtd[0][1]*Zu[0]+gtd[1][1]*Zu[1]+gtd[1][2]*Zu[2];
      t39 = t1*(fourth*(t21*d_psi[1]*inv_psi+Ct[0][0][1]*d_psi[0]+Ct[1][0][1]*
d_psi[1]+Ct[2][0][1]*d_psi[2])-half*dd_psi[0][1]-psi_p*(d_psi[0]*t33+d_psi[1]*
t16));
      t40 = d_psi[2]*inv_psi;
      t51 = gtd[0][2]*Zu[0]+gtd[1][2]*Zu[1]+gtd[2][2]*Zu[2];
      t57 = t1*(fourth*(t21*t40+Ct[0][0][2]*d_psi[0]+Ct[1][0][2]*d_psi[1]+Ct[2]
[0][2]*d_psi[2])-half*dd_psi[0][2]-psi_p*(d_psi[0]*t51+d_psi[2]*t16));
      t58 = d_psi[1]*d_psi[1];
      t84 = t1*(fourth*(t2*d_psi[1]*t40+Ct[0][1][2]*d_psi[0]+Ct[1][1][2]*d_psi
[1]+Ct[2][1][2]*d_psi[2])-half*dd_psi[1][2]-psi_p*(d_psi[1]*t51+d_psi[2]*t33));
      t85 = d_psi[2]*d_psi[2];
      RDZpd_1[0][0] = t1*(fourth*(t2*t3*inv_psi+Ct[0][0][0]*d_psi[0]+Ct[1][0]
[0]*d_psi[1]+Ct[2][0][0]*d_psi[2])-half*dd_psi[0][0]-t12*d_psi[0]*t16);
      RDZpd_1[0][1] = t39;
      RDZpd_1[0][2] = t57;
      RDZpd_1[1][0] = t39;
      RDZpd_1[1][1] = t1*(fourth*(t2*t58*inv_psi+Ct[0][1][1]*d_psi[0]+Ct[1][1]
[1]*d_psi[1]+Ct[2][1][1]*d_psi[2])-half*dd_psi[1][1]-t12*d_psi[1]*t33);
      RDZpd_1[1][2] = t84;
      RDZpd_1[2][0] = t57;
      RDZpd_1[2][1] = t84;
      RDZpd_1[2][2] = t1*(fourth*(t2*t85*inv_psi+Ct[0][2][2]*d_psi[0]+Ct[1][2]
[2]*d_psi[1]+Ct[2][2][2]*d_psi[2])-half*dd_psi[2][2]-t12*d_psi[2]*t51);
      t1 = p_expo*inv_psi;
      t8 = (p_expo*half-1.0)*inv_psi;
      t9 = d_psi[0]*d_psi[0];
      t13 = d_psi[1]*d_psi[1];
      t17 = d_psi[2]*d_psi[2];
      t35 = Gamh[0]*d_psi[0]+Gamh[1]*d_psi[1]+Gamh[2]*d_psi[2]-gtu[0][0]*(t8*t9
+dd_psi[0][0])-gtu[1][1]*(t8*t13+dd_psi[1][1])-gtu[2][2]*(t8*t17+dd_psi[2][2])-
two*(gtu[0][1]*(t8*d_psi[1]*d_psi[0]+dd_psi[0][1])+gtu[0][2]*(t8*d_psi[2]*d_psi
[0]+dd_psi[0][2])+gtu[1][2]*(t8*d_psi[2]*d_psi[1]+dd_psi[1][2]));
      t42 = RDZpd_1[0][1]+t1*half*gtd[0][1]*t35;
      t46 = RDZpd_1[0][2]+t1*half*gtd[0][2]*t35;
      t54 = RDZpd_1[1][2]+t1*half*gtd[1][2]*t35;
      RDZpd[0][0] = RDZpd_1[0][0]+t1*half*gtd[0][0]*t35;
      RDZpd[0][1] = t42;
      RDZpd[0][2] = t46;
      RDZpd[1][0] = t42;
      RDZpd[1][1] = RDZpd_1[1][1]+t1*half*gtd[1][1]*t35;
      RDZpd[1][2] = t54;
      RDZpd[2][0] = t46;
      RDZpd[2][1] = t54;
      RDZpd[2][2] = RDZpd_1[2][2]+t1*half*gtd[2][2]*t35;
      t7 = two*Ctd[0][0][0]+Ctd[0][0][0];
      t9 = two*Ctd[0][0][1];
      t10 = t9+Ctd[1][0][0];
      t12 = two*Ctd[0][0][2];
      t13 = t12+Ctd[2][0][0];
      t27 = t9+Ctd[0][0][1];
      t30 = two*Ctd[0][1][1]+Ctd[1][0][1];
      t32 = two*Ctd[0][1][2];
      t33 = t32+Ctd[2][0][1];
      t47 = t12+Ctd[0][0][2];
      t49 = t32+Ctd[1][0][2];
      t52 = two*Ctd[0][2][2]+Ctd[2][0][2];
      t82 = Ctd[0][0][1]+Ctd[1][0][0];
      t84 = Ctd[0][1][1]+Ctd[1][0][1];
      t86 = Ctd[0][1][2]+Ctd[1][0][2];
      t93 = Ctd[1][0][2]+Ctd[2][0][1];
      t108 = Ct[0][1][2]*Ctd[0][0][0];
      t109 = Ct[1][0][2]*Ctd[1][0][1];
      t111 = Ct[1][1][2]*Ctd[0][0][1];
      t113 = Ct[2][1][2]*Ctd[0][0][2];
      t121 = Ctd[1][1][2]+Ctd[2][1][1];
      t136 = Ct[0][1][2]*Ctd[0][0][1];
      t137 = Ct[1][0][2]*Ctd[1][1][1];
      t139 = Ct[1][1][2]*Ctd[0][1][1];
      t141 = Ct[2][1][2]*Ctd[0][1][2];
      t149 = Ctd[1][2][2]+Ctd[2][1][2];
      t164 = Ct[0][1][2]*Ctd[0][0][2];
      t165 = Ct[1][0][2]*Ctd[1][1][2];
      t167 = Ct[1][1][2]*Ctd[0][1][2];
      t169 = Ct[2][1][2]*Ctd[0][2][2];
      MapleGenVar1 = Gamh[0]*t82+Gamh[1]*t84+Gamh[2]*t86+gtu[0][0]*(Ct[0][0][0]
*t82+Ct[0][0][1]*Ctd[0][0][0]+2.0*Ct[1][0][0]*Ctd[1][0][1]+Ct[1][0][1]*Ctd[0]
[0][1]+Ct[2][0][0]*t93+Ct[2][0][1]*Ctd[0][0][2])+gtu[0][1]*(Ct[0][0][1]*t82+Ct
[0][1][1]*Ctd[0][0][0]+2.0*Ct[1][0][1]*Ctd[1][0][1]+Ct[1][1][1]*Ctd[0][0][1]+Ct
[2][0][1]*t93+Ct[2][1][1]*Ctd[0][0][2])+gtu[0][2]*(Ct[0][0][2]*t82+t108+2.0*
t109+t111+Ct[2][0][2]*t93+t113);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t84+Ct[0][0][1]*Ctd[0]
[0][1]+2.0*Ct[1][0][0]*Ctd[1][1][1]+Ct[1][0][1]*Ctd[0][1][1]+Ct[2][0][0]*t121+
Ct[2][0][1]*Ctd[0][1][2])+gtu[1][1]*(Ct[0][0][1]*t84+Ct[0][1][1]*Ctd[0][0][1]+
2.0*Ct[1][0][1]*Ctd[1][1][1]+Ct[1][1][1]*Ctd[0][1][1]+Ct[2][0][1]*t121+Ct[2][1]
[1]*Ctd[0][1][2]);
      t172 = MapleGenVar2+gtu[1][2]*(Ct[0][0][2]*t84+t136+2.0*t137+t139+Ct[2]
[0][2]*t121+t141)+gtu[0][2]*(Ct[0][0][0]*t86+Ct[0][0][1]*Ctd[0][0][2]+2.0*Ct[1]
[0][0]*Ctd[1][1][2]+Ct[1][0][1]*Ctd[0][1][2]+Ct[2][0][0]*t149+Ct[2][0][1]*Ctd
[0][2][2])+gtu[1][2]*(Ct[0][0][1]*t86+Ct[0][1][1]*Ctd[0][0][2]+2.0*Ct[1][0][1]*
Ctd[1][1][2]+Ct[1][1][1]*Ctd[0][1][2]+Ct[2][0][1]*t149+Ct[2][1][1]*Ctd[0][2][2]
)+gtu[2][2]*(Ct[0][0][2]*t86+t164+2.0*t165+t167+Ct[2][0][2]*t149+t169);
      t188 = fourth*t172-half*(gtu[0][0]*dd_gtd[0][0][0][1]+gtu[1][1]*dd_gtd[1]
[1][0][1]+gtu[2][2]*dd_gtd[2][2][0][1]-gtd[0][0]*d_Gamh[1][0]-gtd[0][1]*d_Gamh
[0][0]-gtd[0][1]*d_Gamh[1][1]-gtd[1][1]*d_Gamh[0][1]-gtd[0][2]*d_Gamh[1][2]-gtd
[1][2]*d_Gamh[0][2])-gtu[0][1]*dd_gtd[0][1][0][1]-gtu[0][2]*dd_gtd[0][2][0][1]-
gtu[1][2]*dd_gtd[1][2][0][1];
      t189 = Ctd[0][0][2]+Ctd[2][0][0];
      t191 = Ctd[0][1][2]+Ctd[2][0][1];
      t193 = Ctd[0][2][2]+Ctd[2][0][2];
      t204 = Ct[0][0][1]*t189;
      t205 = Ct[1][0][1]*t93;
      t207 = 2.0*Ct[2][0][1]*Ctd[2][0][2];
      t228 = Ct[0][0][1]*t191;
      t229 = Ct[1][0][1]*t121;
      t231 = 2.0*Ct[2][0][1]*Ctd[2][1][2];
      t252 = Ct[0][0][1]*t193;
      t253 = Ct[1][0][1]*t149;
      t255 = 2.0*Ct[2][0][1]*Ctd[2][2][2];
      MapleGenVar1 = Gamh[0]*t189+Gamh[1]*t191+Gamh[2]*t193+gtu[0][0]*(Ct[0][0]
[0]*t189+Ct[0][0][2]*Ctd[0][0][0]+Ct[1][0][0]*t93+Ct[1][0][2]*Ctd[0][0][1]+2.0*
Ct[2][0][0]*Ctd[2][0][2]+Ct[2][0][2]*Ctd[0][0][2])+gtu[0][1]*(t204+t108+t205+
t111+t207+t113)+gtu[0][2]*(Ct[0][0][2]*t189+Ct[0][2][2]*Ctd[0][0][0]+Ct[1][0]
[2]*t93+Ct[1][2][2]*Ctd[0][0][1]+2.0*Ct[2][0][2]*Ctd[2][0][2]+Ct[2][2][2]*Ctd
[0][0][2]);
      t267 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t191+Ct[0][0][2]*Ctd[0][0][1]+
Ct[1][0][0]*t121+Ct[1][0][2]*Ctd[0][1][1]+2.0*Ct[2][0][0]*Ctd[2][1][2]+Ct[2][0]
[2]*Ctd[0][1][2])+gtu[1][1]*(t228+t136+t229+t139+t231+t141)+gtu[1][2]*(Ct[0][0]
[2]*t191+Ct[0][2][2]*Ctd[0][0][1]+Ct[1][0][2]*t121+Ct[1][2][2]*Ctd[0][1][1]+2.0
*Ct[2][0][2]*Ctd[2][1][2]+Ct[2][2][2]*Ctd[0][1][2])+gtu[0][2]*(Ct[0][0][0]*t193
+Ct[0][0][2]*Ctd[0][0][2]+Ct[1][0][0]*t149+Ct[1][0][2]*Ctd[0][1][2]+2.0*Ct[2]
[0][0]*Ctd[2][2][2]+Ct[2][0][2]*Ctd[0][2][2])+gtu[1][2]*(t252+t164+t253+t167+
t255+t169)+gtu[2][2]*(Ct[0][0][2]*t193+Ct[0][2][2]*Ctd[0][0][2]+Ct[1][0][2]*
t149+Ct[1][2][2]*Ctd[0][1][2]+2.0*Ct[2][0][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[0]
[2][2]);
      t283 = fourth*t267-half*(gtu[0][0]*dd_gtd[0][0][0][2]+gtu[1][1]*dd_gtd[1]
[1][0][2]+gtu[2][2]*dd_gtd[2][2][0][2]-gtd[0][0]*d_Gamh[2][0]-gtd[0][2]*d_Gamh
[0][0]-gtd[0][1]*d_Gamh[2][1]-gtd[1][2]*d_Gamh[0][1]-gtd[0][2]*d_Gamh[2][2]-gtd
[2][2]*d_Gamh[0][2])-gtu[0][1]*dd_gtd[0][1][0][2]-gtu[0][2]*dd_gtd[0][2][0][2]-
gtu[1][2]*dd_gtd[1][2][0][2];
      t290 = two*Ctd[1][0][0]+Ctd[0][0][1];
      t292 = two*Ctd[1][0][1];
      t293 = t292+Ctd[1][0][1];
      t295 = two*Ctd[1][0][2];
      t296 = t295+Ctd[2][0][1];
      t310 = t292+Ctd[0][1][1];
      t313 = two*Ctd[1][1][1]+Ctd[1][1][1];
      t315 = two*Ctd[1][1][2];
      t316 = t315+Ctd[2][1][1];
      t330 = t295+Ctd[0][1][2];
      t332 = t315+Ctd[1][1][2];
      t335 = two*Ctd[1][2][2]+Ctd[2][1][2];
      MapleGenVar1 = Gamh[0]*t93+Gamh[1]*t121+Gamh[2]*t149+gtu[0][0]*(t204+Ct
[0][0][2]*Ctd[1][0][0]+t205+t109+t207+Ct[2][0][2]*Ctd[1][0][2])+gtu[0][1]*(Ct
[0][1][1]*t189+Ct[0][1][2]*Ctd[1][0][0]+Ct[1][1][1]*t93+Ct[1][1][2]*Ctd[1][0]
[1]+2.0*Ct[2][1][1]*Ctd[2][0][2]+Ct[2][1][2]*Ctd[1][0][2])+gtu[0][2]*(Ct[0][1]
[2]*t189+Ct[0][2][2]*Ctd[1][0][0]+Ct[1][1][2]*t93+Ct[1][2][2]*Ctd[1][0][1]+2.0*
Ct[2][1][2]*Ctd[2][0][2]+Ct[2][2][2]*Ctd[1][0][2]);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(t228+Ct[0][0][2]*Ctd[1][0][1]+t229
+t137+t231+Ct[2][0][2]*Ctd[1][1][2])+gtu[1][1]*(Ct[0][1][1]*t191+Ct[0][1][2]*
Ctd[1][0][1]+Ct[1][1][1]*t121+Ct[1][1][2]*Ctd[1][1][1]+2.0*Ct[2][1][1]*Ctd[2]
[1][2]+Ct[2][1][2]*Ctd[1][1][2]);
      t434 = MapleGenVar2+gtu[1][2]*(Ct[0][1][2]*t191+Ct[0][2][2]*Ctd[1][0][1]+
Ct[1][1][2]*t121+Ct[1][2][2]*Ctd[1][1][1]+2.0*Ct[2][1][2]*Ctd[2][1][2]+Ct[2][2]
[2]*Ctd[1][1][2])+gtu[0][2]*(t252+Ct[0][0][2]*Ctd[1][0][2]+t253+t165+t255+Ct[2]
[0][2]*Ctd[1][2][2])+gtu[1][2]*(Ct[0][1][1]*t193+Ct[0][1][2]*Ctd[1][0][2]+Ct[1]
[1][1]*t149+Ct[1][1][2]*Ctd[1][1][2]+2.0*Ct[2][1][1]*Ctd[2][2][2]+Ct[2][1][2]*
Ctd[1][2][2])+gtu[2][2]*(Ct[0][1][2]*t193+Ct[0][2][2]*Ctd[1][0][2]+Ct[1][1][2]*
t149+Ct[1][2][2]*Ctd[1][1][2]+2.0*Ct[2][1][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[1]
[2][2]);
      t450 = fourth*t434-half*(gtu[0][0]*dd_gtd[0][0][1][2]+gtu[1][1]*dd_gtd[1]
[1][1][2]+gtu[2][2]*dd_gtd[2][2][1][2]-gtd[0][1]*d_Gamh[2][0]-gtd[0][2]*d_Gamh
[1][0]-gtd[1][1]*d_Gamh[2][1]-gtd[1][2]*d_Gamh[1][1]-gtd[1][2]*d_Gamh[2][2]-gtd
[2][2]*d_Gamh[1][2])-gtu[0][1]*dd_gtd[0][1][1][2]-gtu[0][2]*dd_gtd[0][2][1][2]-
gtu[1][2]*dd_gtd[1][2][1][2];
      t457 = two*Ctd[2][0][0]+Ctd[0][0][2];
      t459 = two*Ctd[2][0][1];
      t460 = t459+Ctd[1][0][2];
      t462 = two*Ctd[2][0][2];
      t463 = t462+Ctd[2][0][2];
      t477 = t459+Ctd[0][1][2];
      t480 = two*Ctd[2][1][1]+Ctd[1][1][2];
      t482 = two*Ctd[2][1][2];
      t483 = t482+Ctd[2][1][2];
      t497 = t462+Ctd[0][2][2];
      t499 = t482+Ctd[1][2][2];
      t502 = two*Ctd[2][2][2]+Ctd[2][2][2];
      MapleGenVar2 = fourth*(two*(Gamh[0]*Ctd[0][0][0]+Gamh[1]*Ctd[0][0][1]+
Gamh[2]*Ctd[0][0][2])+gtu[0][0]*(Ct[0][0][0]*t7+Ct[1][0][0]*t10+Ct[2][0][0]*t13
)+gtu[0][1]*(Ct[0][0][1]*t7+Ct[1][0][1]*t10+Ct[2][0][1]*t13)+gtu[0][2]*(Ct[0]
[0][2]*t7+Ct[1][0][2]*t10+Ct[2][0][2]*t13)+gtu[0][1]*(Ct[0][0][0]*t27+Ct[1][0]
[0]*t30+Ct[2][0][0]*t33)+gtu[1][1]*(Ct[0][0][1]*t27+Ct[1][0][1]*t30+Ct[2][0][1]
*t33)+gtu[1][2]*(Ct[0][0][2]*t27+Ct[1][0][2]*t30+Ct[2][0][2]*t33)+gtu[0][2]*(Ct
[0][0][0]*t47+Ct[1][0][0]*t49+Ct[2][0][0]*t52)+gtu[1][2]*(Ct[0][0][1]*t47+Ct[1]
[0][1]*t49+Ct[2][0][1]*t52)+gtu[2][2]*(Ct[0][0][2]*t47+Ct[1][0][2]*t49+Ct[2][0]
[2]*t52));
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][0][0]+gtu[1][1]*dd_gtd[1][1]
[0][0]+gtu[2][2]*dd_gtd[2][2][0][0]-two*(gtd[0][0]*d_Gamh[0][0]+gtd[0][1]*
d_Gamh[0][1]+gtd[0][2]*d_Gamh[0][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[0][0] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][0][0]-gtu[0][2]*dd_gtd
[0][2][0][0]-gtu[1][2]*dd_gtd[1][2][0][0];
      RDZhd[0][1] = t188;
      RDZhd[0][2] = t283;
      RDZhd[1][0] = t188;
      MapleGenVar3 = fourth;
      MapleGenVar5 = two*(Gamh[0]*Ctd[1][0][1]+Gamh[1]*Ctd[1][1][1]+Gamh[2]*Ctd
[1][1][2])+gtu[0][0]*(Ct[0][0][1]*t290+Ct[1][0][1]*t293+Ct[2][0][1]*t296)+gtu
[0][1]*(Ct[0][1][1]*t290+Ct[1][1][1]*t293+Ct[2][1][1]*t296)+gtu[0][2]*(Ct[0][1]
[2]*t290+Ct[1][1][2]*t293+Ct[2][1][2]*t296)+gtu[0][1]*(Ct[0][0][1]*t310+Ct[1]
[0][1]*t313+Ct[2][0][1]*t316);
      MapleGenVar4 = MapleGenVar5+gtu[1][1]*(Ct[0][1][1]*t310+Ct[1][1][1]*t313+
Ct[2][1][1]*t316)+gtu[1][2]*(Ct[0][1][2]*t310+Ct[1][1][2]*t313+Ct[2][1][2]*t316
)+gtu[0][2]*(Ct[0][0][1]*t330+Ct[1][0][1]*t332+Ct[2][0][1]*t335)+gtu[1][2]*(Ct
[0][1][1]*t330+Ct[1][1][1]*t332+Ct[2][1][1]*t335)+gtu[2][2]*(Ct[0][1][2]*t330+
Ct[1][1][2]*t332+Ct[2][1][2]*t335);
      MapleGenVar2 = MapleGenVar3*MapleGenVar4;
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][1][1]+gtu[1][1]*dd_gtd[1][1]
[1][1]+gtu[2][2]*dd_gtd[2][2][1][1]-two*(gtd[0][1]*d_Gamh[1][0]+gtd[1][1]*
d_Gamh[1][1]+gtd[1][2]*d_Gamh[1][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[1][1] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][1][1]-gtu[0][2]*dd_gtd
[0][2][1][1]-gtu[1][2]*dd_gtd[1][2][1][1];
      RDZhd[1][2] = t450;
      RDZhd[2][0] = t283;
      RDZhd[2][1] = t450;
      MapleGenVar3 = fourth;
      MapleGenVar5 = two*(Gamh[0]*Ctd[2][0][2]+Gamh[1]*Ctd[2][1][2]+Gamh[2]*Ctd
[2][2][2])+gtu[0][0]*(Ct[0][0][2]*t457+Ct[1][0][2]*t460+Ct[2][0][2]*t463)+gtu
[0][1]*(Ct[0][1][2]*t457+Ct[1][1][2]*t460+Ct[2][1][2]*t463)+gtu[0][2]*(Ct[0][2]
[2]*t457+Ct[1][2][2]*t460+Ct[2][2][2]*t463)+gtu[0][1]*(Ct[0][0][2]*t477+Ct[1]
[0][2]*t480+Ct[2][0][2]*t483);
      MapleGenVar4 = MapleGenVar5+gtu[1][1]*(Ct[0][1][2]*t477+Ct[1][1][2]*t480+
Ct[2][1][2]*t483)+gtu[1][2]*(Ct[0][2][2]*t477+Ct[1][2][2]*t480+Ct[2][2][2]*t483
)+gtu[0][2]*(Ct[0][0][2]*t497+Ct[1][0][2]*t499+Ct[2][0][2]*t502)+gtu[1][2]*(Ct
[0][1][2]*t497+Ct[1][1][2]*t499+Ct[2][1][2]*t502)+gtu[2][2]*(Ct[0][2][2]*t497+
Ct[1][2][2]*t499+Ct[2][2][2]*t502);
      MapleGenVar2 = MapleGenVar3*MapleGenVar4;
      MapleGenVar3 = -half*(gtu[0][0]*dd_gtd[0][0][2][2]+gtu[1][1]*dd_gtd[1][1]
[2][2]+gtu[2][2]*dd_gtd[2][2][2][2]-two*(gtd[0][2]*d_Gamh[2][0]+gtd[1][2]*
d_Gamh[2][1]+gtd[2][2]*d_Gamh[2][2]));
      MapleGenVar1 = MapleGenVar2+MapleGenVar3;
      RDZhd[2][2] = MapleGenVar1-gtu[0][1]*dd_gtd[0][1][2][2]-gtu[0][2]*dd_gtd
[0][2][2][2]-gtu[1][2]*dd_gtd[1][2][2][2];
      t13 = psi_p*eight_pi_G;
      t24 = p_expo*half;
      t34 = inv_psi_p*(Alpha*(RDZhd[0][1]+RDZpd_1[0][1])-dd_Alpha[0][1]+half*(
Ct[0][0][1]*d_Alpha[0]+Ct[1][0][1]*d_Alpha[1]+Ct[2][0][1]*d_Alpha[2])+t24*(
d_Alpha[0]*d_psi[1]+d_Alpha[1]*d_psi[0])*inv_psi)-t13*Alpha*pTtd_ADM[0][1];
      t51 = inv_psi_p*(Alpha*(RDZhd[0][2]+RDZpd_1[0][2])-dd_Alpha[0][2]+half*(
Ct[0][0][2]*d_Alpha[0]+Ct[1][0][2]*d_Alpha[1]+Ct[2][0][2]*d_Alpha[2])+t24*(
d_Alpha[0]*d_psi[2]+d_Alpha[2]*d_psi[0])*inv_psi)-t13*Alpha*pTtd_ADM[0][2];
      t83 = inv_psi_p*(Alpha*(RDZhd[1][2]+RDZpd_1[1][2])-dd_Alpha[1][2]+half*(
Ct[0][1][2]*d_Alpha[0]+Ct[1][1][2]*d_Alpha[1]+Ct[2][1][2]*d_Alpha[2])+t24*(
d_Alpha[1]*d_psi[2]+d_Alpha[2]*d_psi[1])*inv_psi)-t13*Alpha*pTtd_ADM[1][2];
      Psi1[0][0] = inv_psi_p*(Alpha*(RDZhd[0][0]+RDZpd_1[0][0])-dd_Alpha[0][0]+
half*(Ct[0][0][0]*d_Alpha[0]+Ct[1][0][0]*d_Alpha[1]+Ct[2][0][0]*d_Alpha[2])+
p_expo*d_Alpha[0]*d_psi[0]*inv_psi)-t13*Alpha*pTtd_ADM[0][0];
      Psi1[0][1] = t34;
      Psi1[0][2] = t51;
      Psi1[1][0] = t34;
      Psi1[1][1] = inv_psi_p*(Alpha*(RDZhd[1][1]+RDZpd_1[1][1])-dd_Alpha[1][1]+
half*(Ct[0][1][1]*d_Alpha[0]+Ct[1][1][1]*d_Alpha[1]+Ct[2][1][1]*d_Alpha[2])+
p_expo*d_Alpha[1]*d_psi[1]*inv_psi)-t13*Alpha*pTtd_ADM[1][1];
      Psi1[1][2] = t83;
      Psi1[2][0] = t51;
      Psi1[2][1] = t83;
      Psi1[2][2] = inv_psi_p*(Alpha*(RDZhd[2][2]+RDZpd_1[2][2])-dd_Alpha[2][2]+
half*(Ct[0][2][2]*d_Alpha[0]+Ct[1][2][2]*d_Alpha[1]+Ct[2][2][2]*d_Alpha[2])+
p_expo*d_Alpha[2]*d_psi[2]*inv_psi)-t13*Alpha*pTtd_ADM[2][2];
      third_trPsi1 = third*(Psi1[0][0]*gtu[0][0]+Psi1[1][1]*gtu[1][1]+Psi1[2]
[2]*gtu[2][2]+two*(Psi1[0][1]*gtu[0][1]+Psi1[0][2]*gtu[0][2]+Psi1[1][2]*gtu[1]
[2]));
      t4 = Psi1[0][1]-third_trPsi1*gtd[0][1];
      t6 = Psi1[0][2]-third_trPsi1*gtd[0][2];
      t10 = Psi1[1][2]-third_trPsi1*gtd[1][2];
      Psi1TF[0][0] = Psi1[0][0]-third_trPsi1*gtd[0][0];
      Psi1TF[0][1] = t4;
      Psi1TF[0][2] = t6;
      Psi1TF[1][0] = t4;
      Psi1TF[1][1] = Psi1[1][1]-third_trPsi1*gtd[1][1];
      Psi1TF[1][2] = t10;
      Psi1TF[2][0] = t6;
      Psi1TF[2][1] = t10;
      Psi1TF[2][2] = Psi1[2][2]-third_trPsi1*gtd[2][2];
      t20 = gtd[0][0]*d_Betau[1][0]+gtd[0][1]*d_Betau[0][0]+gtd[0][1]*d_Betau
[1][1]+gtd[1][1]*d_Betau[0][1]+gtd[0][2]*d_Betau[1][2]+gtd[1][2]*d_Betau[0][2]-
two*(third*gtd[0][1]*div_Beta+Alpha*Atd[0][1]);
      t32 = gtd[0][0]*d_Betau[2][0]+gtd[0][2]*d_Betau[0][0]+gtd[0][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[0][1]+gtd[0][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[0][2]-
two*(third*gtd[0][2]*div_Beta+Alpha*Atd[0][2]);
      t52 = gtd[0][1]*d_Betau[2][0]+gtd[0][2]*d_Betau[1][0]+gtd[1][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[1][1]+gtd[1][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[1][2]-
two*(third*gtd[1][2]*div_Beta+Alpha*Atd[1][2]);
      gtd_rhs[0][0] = two*(gtd[0][0]*d_Betau[0][0]+gtd[0][1]*d_Betau[0][1]+gtd
[0][2]*d_Betau[0][2]-third*gtd[0][0]*div_Beta-Alpha*Atd[0][0]);
      gtd_rhs[0][1] = t20;
      gtd_rhs[0][2] = t32;
      gtd_rhs[1][0] = t20;
      gtd_rhs[1][1] = two*(gtd[0][1]*d_Betau[1][0]+gtd[1][1]*d_Betau[1][1]+gtd
[1][2]*d_Betau[1][2]-third*gtd[1][1]*div_Beta-Alpha*Atd[1][1]);
      gtd_rhs[1][2] = t52;
      gtd_rhs[2][0] = t32;
      gtd_rhs[2][1] = t52;
      gtd_rhs[2][2] = two*(gtd[0][2]*d_Betau[2][0]+gtd[1][2]*d_Betau[2][1]+gtd
[2][2]*d_Betau[2][2]-third*gtd[2][2]*div_Beta-Alpha*Atd[2][2]);
      t16 = Alpha*(trK-two*Theta)-twothirds*div_Beta;
      t19 = two*Alpha;
      t33 = Atd[0][0]*(d_Betau[1][0]-t19*Atud[0][1])+Atd[0][1]*d_Betau[0][0]+
Atd[0][1]*(d_Betau[1][1]-t19*Atud[1][1])+Atd[1][1]*d_Betau[0][1]+Atd[0][2]*(
d_Betau[1][2]-t19*Atud[2][1])+Atd[1][2]*d_Betau[0][2]+Atd[0][1]*t16+Psi1TF[0]
[1];
      t35 = d_Betau[2][0]-t19*Atud[0][2];
      t39 = d_Betau[2][1]-t19*Atud[1][2];
      t43 = d_Betau[2][2]-t19*Atud[2][2];
      t47 = Atd[0][0]*t35+Atd[0][2]*d_Betau[0][0]+Atd[0][1]*t39+Atd[1][2]*
d_Betau[0][1]+Atd[0][2]*t43+Atd[2][2]*d_Betau[0][2]+Atd[0][2]*t16+Psi1TF[0][2];
      t68 = Atd[0][1]*t35+Atd[0][2]*d_Betau[1][0]+Atd[1][1]*t39+Atd[1][2]*
d_Betau[1][1]+Atd[1][2]*t43+Atd[2][2]*d_Betau[1][2]+Atd[1][2]*t16+Psi1TF[1][2];
      Atd_rhs[0][0] = two*(Atd[0][0]*(d_Betau[0][0]-Alpha*Atud[0][0])+Atd[0][1]
*(d_Betau[0][1]-Alpha*Atud[1][0])+Atd[0][2]*(d_Betau[0][2]-Alpha*Atud[2][0]))+
Atd[0][0]*t16+Psi1TF[0][0];
      Atd_rhs[0][1] = t33;
      Atd_rhs[0][2] = t47;
      Atd_rhs[1][0] = t33;
      Atd_rhs[1][1] = two*(Atd[0][1]*(d_Betau[1][0]-Alpha*Atud[0][1])+Atd[1][1]
*(d_Betau[1][1]-Alpha*Atud[1][1])+Atd[1][2]*(d_Betau[1][2]-Alpha*Atud[2][1]))+
Atd[1][1]*t16+Psi1TF[1][1];
      Atd_rhs[1][2] = t68;
      Atd_rhs[2][0] = t47;
      Atd_rhs[2][1] = t68;
      Atd_rhs[2][2] = two*(Atd[0][2]*(d_Betau[2][0]-Alpha*Atud[0][2])+Atd[1][2]
*(d_Betau[2][1]-Alpha*Atud[1][2])+Atd[2][2]*(d_Betau[2][2]-Alpha*Atud[2][2]))+
Atd[2][2]*t16+Psi1TF[2][2];
      t27 = Theta*inv_Alpha;
      t30 = eight_pi_G*psi_p;
      t32 = d_Theta[0]-t27*d_Alpha[0]-twothirds*d_trK[0]-t30*Jtd_ADM[0];
      t37 = d_Theta[1]-t27*d_Alpha[1]-twothirds*d_trK[1]-t30*Jtd_ADM[1];
      t42 = d_Theta[2]-t27*d_Alpha[2]-twothirds*d_trK[2]-t30*Jtd_ADM[2];
      t46 = sigma_1+twothirds*trK;
      t50 = threehalves*p_expo;
      t51 = Alpha*inv_psi;
      t54 = t50*t51*d_psi[0]-d_Alpha[0];
      t58 = t50*t51*d_psi[1]-d_Alpha[1];
      t62 = t50*t51*d_psi[2]-d_Alpha[2];
      Gamh_rhs[0] = twothirds*Gamh[0]*div_Beta-Gamh[0]*d_Betau[0][0]-Gamh[1]*
d_Betau[1][0]-Gamh[2]*d_Betau[2][0]+gtu[0][0]*dd_Betau[0][0][0]+gtu[1][1]*
dd_Betau[1][1][0]+gtu[2][2]*dd_Betau[2][2][0]+two*(gtu[0][1]*dd_Betau[0][1][0]+
gtu[0][2]*dd_Betau[0][2][0]+gtu[1][2]*dd_Betau[1][2][0])+third*(gtu[0][0]*
d_div_Beta[0]+gtu[0][1]*d_div_Beta[1]+gtu[0][2]*d_div_Beta[2])+two*(Alpha*(half
*(Ct[0][0][0]*Atu[0][0]+Ct[0][1][1]*Atu[1][1]+Ct[0][2][2]*Atu[2][2])+Ct[0][0]
[1]*Atu[0][1]+Ct[0][0][2]*Atu[0][2]+Ct[0][1][2]*Atu[1][2]+gtu[0][0]*t32+gtu[0]
[1]*t37+gtu[0][2]*t42-psi_p*Zu[0]*t46)+Atu[0][0]*t54+Atu[0][1]*t58+Atu[0][2]*
t62);
      Gamh_rhs[1] = twothirds*Gamh[1]*div_Beta-Gamh[0]*d_Betau[0][1]-Gamh[1]*
d_Betau[1][1]-Gamh[2]*d_Betau[2][1]+gtu[0][0]*dd_Betau[0][0][1]+gtu[1][1]*
dd_Betau[1][1][1]+gtu[2][2]*dd_Betau[2][2][1]+two*(gtu[0][1]*dd_Betau[0][1][1]+
gtu[0][2]*dd_Betau[0][2][1]+gtu[1][2]*dd_Betau[1][2][1])+third*(gtu[0][1]*
d_div_Beta[0]+gtu[1][1]*d_div_Beta[1]+gtu[1][2]*d_div_Beta[2])+two*(Alpha*(half
*(Ct[1][0][0]*Atu[0][0]+Ct[1][1][1]*Atu[1][1]+Ct[1][2][2]*Atu[2][2])+Ct[1][0]
[1]*Atu[0][1]+Ct[1][0][2]*Atu[0][2]+Ct[1][1][2]*Atu[1][2]+gtu[0][1]*t32+gtu[1]
[1]*t37+gtu[1][2]*t42-psi_p*Zu[1]*t46)+Atu[0][1]*t54+Atu[1][1]*t58+Atu[1][2]*
t62);
      Gamh_rhs[2] = twothirds*Gamh[2]*div_Beta-Gamh[0]*d_Betau[0][2]-Gamh[1]*
d_Betau[1][2]-Gamh[2]*d_Betau[2][2]+gtu[0][0]*dd_Betau[0][0][2]+gtu[1][1]*
dd_Betau[1][1][2]+gtu[2][2]*dd_Betau[2][2][2]+two*(gtu[0][1]*dd_Betau[0][1][2]+
gtu[0][2]*dd_Betau[0][2][2]+gtu[1][2]*dd_Betau[1][2][2])+third*(gtu[0][2]*
d_div_Beta[0]+gtu[1][2]*d_div_Beta[1]+gtu[2][2]*d_div_Beta[2])+two*(Alpha*(half
*(Ct[2][0][0]*Atu[0][0]+Ct[2][1][1]*Atu[1][1]+Ct[2][2][2]*Atu[2][2])+Ct[2][0]
[1]*Atu[0][1]+Ct[2][0][2]*Atu[0][2]+Ct[2][1][2]*Atu[1][2]+gtu[0][2]*t32+gtu[1]
[2]*t37+gtu[2][2]*t42-psi_p*Zu[2]*t46)+Atu[0][2]*t54+Atu[1][2]*t58+Atu[2][2]*
t62);
      t3 = threefourths*(lambda_f0+lambda_f1*Alpha);
      Betau_rhs[0] = t3*Bu[0];
      Betau_rhs[1] = t3*Bu[1];
      Betau_rhs[2] = t3*Bu[2];
      Bu_rhs[0] = Gamh_rhs[0]-Bu[0]*feta;
      Bu_rhs[1] = Gamh_rhs[1]-Bu[1]*feta;
      Bu_rhs[2] = Gamh_rhs[2]-Bu[2]*feta;
      t2 = two*Theta;
      Alpha_rhs = -two*Alpha*(trK-t2-trK0);
      psi_rhs = twothirds*psi*(div_Beta-Alpha*trK)/p_expo;
      t13 = gtu[0][0];
      t18 = gtu[1][1];
      t23 = gtu[2][2];
      t28 = gtu[0][1];
      t33 = gtu[0][2];
      t38 = gtu[1][2];
      t43 = inv_psi_p*((RDZpd[0][0]+RDZhd[0][0])*t13+(RDZpd[1][1]+RDZhd[1][1])*
t18+(RDZpd[2][2]+RDZhd[2][2])*t23+two*((RDZpd[0][1]+RDZhd[0][1])*t28+(RDZpd[0]
[2]+RDZhd[0][2])*t33+(RDZpd[1][2]+RDZhd[1][2])*t38));
      t70 = d_Alpha[0];
      t73 = d_Alpha[1];
      t76 = d_Alpha[2];
      t80 = d_psi[0];
      t83 = d_psi[1];
      t86 = d_psi[2];
      trK_rhs = Alpha*(t43+trK*(trK-t2)-three*sigma_1*(one+sigma_2)*Theta+
four_pi_G*(tr_pT-three*rho_ADM))-inv_psi_p*(t13*dd_Alpha[0][0]+t18*dd_Alpha[1]
[1]+t23*dd_Alpha[2][2]+two*(t28*dd_Alpha[0][1]+t33*dd_Alpha[0][2]+t38*dd_Alpha
[1][2])-CalGamTil[0]*t70-CalGamTil[1]*t73-CalGamTil[2]*t76+half*p_expo*inv_psi*
(t13*t70*t80+t28*t70*t83+t33*t70*t86+t28*t73*t80+t18*t73*t83+t38*t73*t86+t33*
t76*t80+t38*t76*t83+t23*t76*t86));
      Theta_rhs = half*Alpha*(t43+two*trK*(third*trK-Theta)-Atd[0][0]*Atu[0][0]
-Atd[1][1]*Atu[1][1]-Atd[2][2]*Atu[2][2]-two*(Atd[0][1]*Atu[0][1]+Atd[0][2]*Atu
[0][2]+Atd[1][2]*Atu[1][2]))-Zu[0]*t70-Zu[1]*t73-Zu[2]*t76-Alpha*(sigma_1*(2.0+
sigma_2)*Theta+eight_pi_G*rho_ADM);
      t5 = d_psi[0];
      t8 = d_psi[1];
      t11 = d_psi[2];
      divE = d_emEu[0][0]+d_emEu[1][1]+d_emEu[2][2]-threehalves*(emEu[0]*t5+
emEu[1]*t8+emEu[2]*t11)*inv_psi;
      t19 = emBu[0];
      t21 = emBu[1];
      t23 = emBu[2];
      divB = d_emBu[0][0]+d_emBu[1][1]+d_emBu[2][2]-threehalves*(t19*t5+t21*t8+
t23*t11)*inv_psi;
      t28 = t19*t19;
      t31 = t21*t21;
      t34 = t23*t23;
      Bsq = t28*gtd[0][0]+t31*gtd[1][1]+t34*gtd[2][2]+two*(t19*t21*gtd[0][1]+
t19*t23*gtd[0][2]+t21*t23*gtd[1][2]);
      inv_Bsq = 0.1E1/Bsq;
      t49 = fabs(psi);
      sqrt_psi = sqrt(t49);
      t1 = divE*sqrt_psi;
      t4 = -emEu[2]*emBu[1]+emEu[1]*emBu[2];
      t8 = emEu[2]*emBu[0]-emEu[0]*emBu[2];
      t12 = -emEu[1]*emBu[0]+emEu[0]*emBu[1];
      Ju[0] = t1*(gtu[0][0]*t4+gtu[0][1]*t8+gtu[0][2]*t12)*inv_Bsq;
      Ju[1] = t1*(gtu[0][1]*t4+gtu[1][1]*t8+gtu[1][2]*t12)*inv_Bsq;
      Ju[2] = t1*(gtu[0][2]*t4+gtu[1][2]*t8+gtu[2][2]*t12)*inv_Bsq;
      t7 = emBu[0]*gtd[0][1]+emBu[1]*gtd[1][1]+emBu[2]*gtd[1][2];
      t10 = d_Alpha[2]-Alpha*d_psi[2]*inv_psi;
      t23 = emBu[0]*gtd[0][2]+emBu[1]*gtd[1][2]+emBu[2]*gtd[2][2];
      t26 = d_Alpha[1]-Alpha*d_psi[1]*inv_psi;
      t53 = emBu[0]*gtd[0][0]+emBu[1]*gtd[0][1]+emBu[2]*gtd[0][2];
      t65 = d_Alpha[0]-Alpha*d_psi[0]*inv_psi;
      emEu_rhs[0] = -emEu[0]*d_Betau[0][0]-emEu[1]*d_Betau[1][0]-emEu[2]*
d_Betau[2][0]+sqrt_psi*(-t7*t10-Alpha*(d_emBu[2][0]*gtd[0][1]+emBu[0]*d_gtd[2]
[0][1]+d_emBu[2][1]*gtd[1][1]+emBu[1]*d_gtd[2][1][1]+d_emBu[2][2]*gtd[1][2]+
emBu[2]*d_gtd[2][1][2])+t23*t26+Alpha*(d_emBu[1][0]*gtd[0][2]+emBu[0]*d_gtd[1]
[0][2]+d_emBu[1][1]*gtd[1][2]+emBu[1]*d_gtd[1][1][2]+d_emBu[1][2]*gtd[2][2]+
emBu[2]*d_gtd[1][2][2]))+Alpha*(trK*emEu[0]-psi*(gtu[0][0]*d_emPsi[0]+gtu[0][1]
*d_emPsi[1]+gtu[0][2]*d_emPsi[2])-Ju[0]);
      emEu_rhs[1] = -emEu[0]*d_Betau[0][1]-emEu[1]*d_Betau[1][1]-emEu[2]*
d_Betau[2][1]+sqrt_psi*(t53*t10+Alpha*(d_emBu[2][0]*gtd[0][0]+emBu[0]*d_gtd[2]
[0][0]+d_emBu[2][1]*gtd[0][1]+emBu[1]*d_gtd[2][0][1]+d_emBu[2][2]*gtd[0][2]+
emBu[2]*d_gtd[2][0][2])-t23*t65-Alpha*(d_emBu[0][0]*gtd[0][2]+emBu[0]*d_gtd[0]
[0][2]+d_emBu[0][1]*gtd[1][2]+emBu[1]*d_gtd[0][1][2]+d_emBu[0][2]*gtd[2][2]+
emBu[2]*d_gtd[0][2][2]))+Alpha*(trK*emEu[1]-psi*(gtu[0][1]*d_emPsi[0]+gtu[1][1]
*d_emPsi[1]+gtu[1][2]*d_emPsi[2])-Ju[1]);
      emEu_rhs[2] = -emEu[0]*d_Betau[0][2]-emEu[1]*d_Betau[1][2]-emEu[2]*
d_Betau[2][2]+sqrt_psi*(-t53*t26-Alpha*(d_emBu[1][0]*gtd[0][0]+emBu[0]*d_gtd[1]
[0][0]+d_emBu[1][1]*gtd[0][1]+emBu[1]*d_gtd[1][0][1]+d_emBu[1][2]*gtd[0][2]+
emBu[2]*d_gtd[1][0][2])+t7*t65+Alpha*(d_emBu[0][0]*gtd[0][1]+emBu[0]*d_gtd[0]
[0][1]+d_emBu[0][1]*gtd[1][1]+emBu[1]*d_gtd[0][1][1]+d_emBu[0][2]*gtd[1][2]+
emBu[2]*d_gtd[0][1][2]))+Alpha*(trK*emEu[2]-psi*(gtu[0][2]*d_emPsi[0]+gtu[1][2]
*d_emPsi[1]+gtu[2][2]*d_emPsi[2])-Ju[2]);
      t7 = emEu[0]*gtd[0][1]+emEu[1]*gtd[1][1]+emEu[2]*gtd[1][2];
      t10 = d_Alpha[2]-Alpha*d_psi[2]*inv_psi;
      t23 = emEu[0]*gtd[0][2]+emEu[1]*gtd[1][2]+emEu[2]*gtd[2][2];
      t26 = d_Alpha[1]-Alpha*d_psi[1]*inv_psi;
      t53 = emEu[0]*gtd[0][0]+emEu[1]*gtd[0][1]+emEu[2]*gtd[0][2];
      t65 = d_Alpha[0]-Alpha*d_psi[0]*inv_psi;
      emBu_rhs[0] = -emBu[0]*d_Betau[0][0]-emBu[1]*d_Betau[1][0]-emBu[2]*
d_Betau[2][0]-sqrt_psi*(-t7*t10-Alpha*(d_emEu[2][0]*gtd[0][1]+emEu[0]*d_gtd[2]
[0][1]+d_emEu[2][1]*gtd[1][1]+emEu[1]*d_gtd[2][1][1]+d_emEu[2][2]*gtd[1][2]+
emEu[2]*d_gtd[2][1][2])+t23*t26+Alpha*(d_emEu[1][0]*gtd[0][2]+emEu[0]*d_gtd[1]
[0][2]+d_emEu[1][1]*gtd[1][2]+emEu[1]*d_gtd[1][1][2]+d_emEu[1][2]*gtd[2][2]+
emEu[2]*d_gtd[1][2][2]))+Alpha*(trK*emBu[0]+psi*(gtu[0][0]*d_emPhi[0]+gtu[0][1]
*d_emPhi[1]+gtu[0][2]*d_emPhi[2]));
      emBu_rhs[1] = -emBu[0]*d_Betau[0][1]-emBu[1]*d_Betau[1][1]-emBu[2]*
d_Betau[2][1]-sqrt_psi*(t53*t10+Alpha*(d_emEu[2][0]*gtd[0][0]+emEu[0]*d_gtd[2]
[0][0]+d_emEu[2][1]*gtd[0][1]+emEu[1]*d_gtd[2][0][1]+d_emEu[2][2]*gtd[0][2]+
emEu[2]*d_gtd[2][0][2])-t23*t65-Alpha*(d_emEu[0][0]*gtd[0][2]+emEu[0]*d_gtd[0]
[0][2]+d_emEu[0][1]*gtd[1][2]+emEu[1]*d_gtd[0][1][2]+d_emEu[0][2]*gtd[2][2]+
emEu[2]*d_gtd[0][2][2]))+Alpha*(trK*emBu[1]+psi*(gtu[0][1]*d_emPhi[0]+gtu[1][1]
*d_emPhi[1]+gtu[1][2]*d_emPhi[2]));
      emBu_rhs[2] = -emBu[0]*d_Betau[0][2]-emBu[1]*d_Betau[1][2]-emBu[2]*
d_Betau[2][2]-sqrt_psi*(-t53*t26-Alpha*(d_emEu[1][0]*gtd[0][0]+emEu[0]*d_gtd[1]
[0][0]+d_emEu[1][1]*gtd[0][1]+emEu[1]*d_gtd[1][0][1]+d_emEu[1][2]*gtd[0][2]+
emEu[2]*d_gtd[1][0][2])+t7*t65+Alpha*(d_emEu[0][0]*gtd[0][1]+emEu[0]*d_gtd[0]
[0][1]+d_emEu[0][1]*gtd[1][1]+emEu[1]*d_gtd[0][1][1]+d_emEu[0][2]*gtd[1][2]+
emEu[2]*d_gtd[0][1][2]))+Alpha*(trK*emBu[2]+psi*(gtu[0][2]*d_emPhi[0]+gtu[1][2]
*d_emPhi[1]+gtu[2][2]*d_emPhi[2]));
      emPsi_rhs = -kappa_1*Alpha*emPsi;
      emPhi_rhs = Alpha*(divB-kappa_2*emPhi);
