#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hawaii.h"

#include "bbhutil.h"

char *read_line(FILE *fin) {
    char *buffer;
    char *tmp;
    int read_chars = 0;
    int bufsize = 512;
    char *line = malloc(bufsize);

    if ( !line ) {
        return NULL;
    }

    buffer = line;

    while ( fgets(buffer, bufsize - read_chars, fin) ) {
        read_chars = strlen(line);

        if ( line[read_chars - 1] == '\n' ) {
            line[read_chars - 1] = '\0';
            return line;
        }

        else {
            bufsize = 2 * bufsize;
            tmp = realloc(line, bufsize);
            if ( tmp ) {
                line = tmp;
                buffer = line + read_chars;
            }
            else {
                free(line);
                return NULL;
            }
        }
    }
    return NULL;
}

int read_param_file(char *pfile)
{

  int ok = 1;
  const double huge  = 1.0e14;
  const double small = 1.0e-14;

  ok *= read_int_param(pfile, "max_iter", &pars.max_iter, 1, 1, 5000000); 
  ok *= read_int_param(pfile, "prune_frequency", &pars.prune_frequency,1,1,50); 
  ok *= read_real_param(pfile, "epsilon", &pars.epsilon, 1.0e-3, small, 0.5);

  pars.t0 = 0.0; // hard code the initial time.
  ok *= read_real_param(pfile, "tf", &pars.tf, 1.0, pars.t0, huge);
  ok *= read_real_param(pfile, "max_runtime", &pars.max_runtime, 3600.0, 1.0, huge);
 /* code will checkpoint (create restart file) by default every 4 hours, with
  * a minimum an 1 hour */
  ok *= read_real_param(pfile, "time_between_restarts", &pars.time_between_restarts, 14400.0, 3600.0, huge);
  ok *= read_real_param(pfile, "xmin", &pars.xmin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "xmax", &pars.xmax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "ymin", &pars.ymin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "ymax", &pars.ymax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "zmin", &pars.zmin, -100.0, -huge, huge);
  ok *= read_real_param(pfile, "zmax", &pars.zmax, 100.0, -huge, huge);
  ok *= read_real_param(pfile, "cfl", &pars.cfl, 0.25, small, 0.5);

  // CCZ4 parameters
  ok *= read_int_param(pfile, "id_type", &pars.id_type, 0, 0, 3); 
  ok *= read_int_param(pfile, "lambda_f0", &pars.lambda_f0, 1, 0, 1); 
  ok *= read_int_param(pfile, "lambda_f1", &pars.lambda_f1, 1, 0, 1); 
  ok *= read_real_param(pfile, "sigma_diss", &pars.sigma_diss, 0.0, 0.0, 1.0);
  ok *= read_int_param(pfile, "eta_damping_exp", &pars.eta_damping_exp,2, 0, 4);
  ok *= read_real_param(pfile, "eta", &pars.eta, 1.0, 0.0, 2.0);
  ok *= read_real_param(pfile, "R_0", &pars.R_0, 40.0, 0.0, huge);
  ok *= read_real_param(pfile, "trK0", &pars.trK0, 0.0, 0.0, 10.0);
  ok *= read_int_param(pfile, "lambda_1", &pars.lambda_1, 1, 0, 1); 
  ok *= read_int_param(pfile, "lambda_2", &pars.lambda_2, 1, 0, 1); 
  ok *= read_int_param(pfile, "lambda_3", &pars.lambda_3, 1, 0, 1); 
  ok *= read_int_param(pfile, "lambda_4", &pars.lambda_4, 1, 0, 1); 
  ok *= read_real_param(pfile, "psi_floor", &pars.psi_floor, 0.0001, 1.0e-10, 1.0);
  ok *= read_real_param(pfile, "alpha_floor", &pars.alpha_floor, 0.0001, 1.0e-10, 1.0);
  
  ok *= read_real_param(pfile, "sigma_1", &pars.sigma_1, 0.0001, 1.0e-10, 1.0);
  ok *= read_real_param(pfile, "sigma_2", &pars.sigma_2, 0.0001, 1.0e-10, 1.0);
  
  ok *= read_real_param(pfile, "mass1", &pars.mass1, 1.0, 0.0, huge);
  ok *= read_real_param(pfile, "bh1x", &pars.bh1x, -10.0, -huge, huge);
  ok *= read_real_param(pfile, "bh1y", &pars.bh1y, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "bh1z", &pars.bh1z, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "vx1", &pars.vx1, 0.0, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "vy1", &pars.vy1, 0.1, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "vz1", &pars.vz1, 0.0, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "spin1", &pars.spin1, 0.0, 0.0, 1.0);
  ok *= read_real_param(pfile, "spin1_th", &pars.spin1_th, 0.0, -huge, huge);
  ok *= read_real_param(pfile, "spin1_phi", &pars.spin1_th, 0.0, -huge, huge);

  ok *= read_real_param(pfile, "mass2", &pars.mass2, 1.0, 0.0, huge);
  ok *= read_real_param(pfile, "bh2x", &pars.bh2x, 10.0, -huge, huge);
  ok *= read_real_param(pfile, "bh2y", &pars.bh2y, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "bh2z", &pars.bh2z, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "vx2", &pars.vx2, 0.0, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "vy2", &pars.vy2, -0.1, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "vz2", &pars.vz2, 0.0, -0.9999999, 0.9999999);
  ok *= read_real_param(pfile, "spin2", &pars.spin2, 0.0, 0.0, 1.0);
  ok *= read_real_param(pfile, "spin2_th", &pars.spin2_th, 0.0, -huge, huge);
  ok *= read_real_param(pfile, "spin2_phi", &pars.spin2_th, 0.0, -huge, huge);

  // boundary conditions
  char **bcs;
  bcs = (char **) malloc(2*n_dim*sizeof(char *));
  for (int i = 0; i < 2*n_dim; i++) {
    bcs[i] = (char *) malloc(32*sizeof(char));
  }
  if (get_str_param(pfile, "boundaries", bcs, 2*n_dim)!= 1) {
    for(int i = 0; i < 2*n_dim; i++) {
      pars.bcs[i] = bc_outflow;
    }
  }
  else {
    for (int i = 0; i < 2*n_dim; i++) {
      if ( strcmp(bcs[i],"outflow") == 0 ) {
        pars.bcs[i] = bc_outflow;
        printf("## i=%d, bc_outflow\n",i);
      }
      else if ( strcmp(bcs[i],"wall") == 0) {
        pars.bcs[i] = bc_wall;
        printf("## i=%d, bc_wall\n",i);
      }
      else {
        pars.bcs[i] = bc_unknown;
        printf("## i=%d, bc_unknown\n",i);
      }
    }
  }
  for (int i = 0; i < 2*n_dim; i++) {
    free(bcs[i]);
  }
  free(bcs);

  ok *= read_int_param(pfile, "output_frequency", &pars.output_frequency,
                       0, 0, 1000000);
  ok *= read_int_param(pfile, "print_frequency", &pars.print_frequency,
                       0, 0, 1000000);

  // output fields
  // find out how many fields for which output is requested
  FILE *fp;
  char *line;
  fp = fopen(pfile,"r");
  int count = 0;
  if ( fp ) {
    while ( (line = read_line(fp))) {
      if ( strstr(line,"output_functions") ) {
        // count how many fields listed here
        const char *tmp = line;
        while( (tmp=strstr(tmp,"\"")) ) {
          count++;
          tmp++;
        }
      }
    }
    fclose(fp);
  }
  int num_req_fields = count/2;
  pars.num_req_fields = num_req_fields;

  // output fields
  // initialize output off
  for (int i=0;i<n_variab+n_aux;i++) {
    pars.output_fields[i] = no_output;
  }

  char **output_fields;
  output_fields = (char **) malloc((num_req_fields)*sizeof(char *));
  for (int i = 0; i < num_req_fields ; i++) {
    output_fields[i] = (char *) malloc(32*sizeof(char));
  }
  
  if (get_str_param(pfile, "output_functions", output_fields, num_req_fields)!= 1) {
    for(int i = 0; i < n_variab+n_aux; i++) {
      pars.output_fields[i] = no_output;
    }
  }
  else {
    for (int i = 0; i < num_req_fields; i++) {
      if ( strcmp(output_fields[i],"alpha") == 0 ) {
        pars.output_fields[U_ALPHA] = normal_output;
        printf(" alpha output selected\n");
      }
      if ( strcmp(output_fields[i],"theta") == 0 ) { //Output for theta
        pars.output_fields[U_THETA] = normal_output;
        printf(" Theta output selected\n");
      }
      if ( strcmp(output_fields[i],"shiftx") == 0 ) {
        pars.output_fields[U_SHIFTX] = normal_output;
        printf(" shiftx output selected\n");
      }
      if ( strcmp(output_fields[i],"shifty") == 0 ) {
        pars.output_fields[U_SHIFTY] = normal_output;
        printf(" shifty output selected\n");
      }
      if ( strcmp(output_fields[i],"shiftz") == 0 ) {
        pars.output_fields[U_SHIFTZ] = normal_output;
        printf(" shiftz output selected\n");
      }
      if ( strcmp(output_fields[i],"psi") == 0 ) {
        pars.output_fields[U_PSI] = normal_output;
        printf(" psi output selected\n");
      }
      if ( strcmp(output_fields[i],"trk") == 0 ) {
        pars.output_fields[U_TRK] = normal_output;
        printf(" trk output selected\n");
      }
      if ( strcmp(output_fields[i],"gamhx") == 0 ) {
        pars.output_fields[U_GAMHX] = normal_output;
        printf(" gamhx output selected\n");
      }
      if ( strcmp(output_fields[i],"gamhy") == 0 ) {
        pars.output_fields[U_GAMHY] = normal_output;
        printf(" gamhy output selected\n");
      }
      if ( strcmp(output_fields[i],"gamhz") == 0 ) {
        pars.output_fields[U_GAMHZ] = normal_output;
        printf(" gamhz output selected\n");
      }
      if ( strcmp(output_fields[i],"gbx") == 0 ) {
        pars.output_fields[U_GBX] = normal_output;
        printf(" gbx output selected\n");
      }
      if ( strcmp(output_fields[i],"gby") == 0 ) {
        pars.output_fields[U_GBY] = normal_output;
        printf(" gby output selected\n");
      }
      if ( strcmp(output_fields[i],"gbz") == 0 ) {
        pars.output_fields[U_GBZ] = normal_output;
        printf(" gbz output selected\n");
      }
      if ( strcmp(output_fields[i],"gtxx") == 0 ) {
        pars.output_fields[U_GTXX] = normal_output;
        printf(" gtxx output selected\n");
      }
      if ( strcmp(output_fields[i],"gtxy") == 0 ) {
        pars.output_fields[U_GTXY] = normal_output;
        printf(" gtxy output selected\n");
      }
      if ( strcmp(output_fields[i],"gtxz") == 0 ) {
        pars.output_fields[U_GTXZ] = normal_output;
        printf(" gtxz output selected\n");
      }
      if ( strcmp(output_fields[i],"gtyy") == 0 ) {
        pars.output_fields[U_GTYY] = normal_output;
        printf(" gtyy output selected\n");
      }
      if ( strcmp(output_fields[i],"gtyz") == 0 ) {
        pars.output_fields[U_GTYZ] = normal_output;
        printf(" gtyz output selected\n");
      }
      if ( strcmp(output_fields[i],"gtzz") == 0 ) {
        pars.output_fields[U_GTZZ] = normal_output;
        printf(" gtzz output selected\n");
      }
      if ( strcmp(output_fields[i],"atxx") == 0 ) {
        pars.output_fields[U_ATXX] = normal_output;
        printf(" atxx output selected\n");
      }
      if ( strcmp(output_fields[i],"atxy") == 0 ) {
        pars.output_fields[U_ATXY] = normal_output;
        printf(" atxy output selected\n");
      }
      if ( strcmp(output_fields[i],"atxz") == 0 ) {
        pars.output_fields[U_ATXZ] = normal_output;
        printf(" atxz output selected\n");
      }
      if ( strcmp(output_fields[i],"atyy") == 0 ) {
        pars.output_fields[U_ATYY] = normal_output;
        printf(" atyy output selected\n");
      }
      if ( strcmp(output_fields[i],"atyz") == 0 ) {
        pars.output_fields[U_ATYZ] = normal_output;
        printf(" atyz output selected\n");
      }
      if ( strcmp(output_fields[i],"atzz") == 0 ) {
        pars.output_fields[U_ATZZ] = normal_output;
        printf(" atzz output selected\n");
      }
    }
  }

  for (int i = 0; i < num_req_fields; i++) {
    free(output_fields[i]);
  }
  free(output_fields);

  if (ok != 1) {
    printf("There was an error reading the parameters.\n");
  }

  return ok;
}
 
int read_int_param(char* pfile, char *name, int *var, const int def_val,
                   const int min_val, const int max_val)
{
  if (get_param(pfile, name, "long", 1, var) != 1) {
    *var = def_val;
  }
  if (*var > max_val) {
    printf("%s = %d is out of range. max(%s) = %d\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %d is out of range. min(%s) = %d\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}

int read_real_param(char* pfile, char *name, double *var, const double def_val,
                   const double min_val, const double max_val)
{
  if (get_param(pfile, name, "double", 1, var) != 1) {
    *var = def_val;
  }
  if (*var > max_val) {
    printf("%s = %g is out of range. max(%s) = %g\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %g is out of range. min(%s) = %g\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}
int default_params()
{
  // no par file read-in supported; hard code parameters
  pars.max_iter = 10;
  pars.prune_frequency = 1;
  pars.epsilon = 1.e-3;

  pars.t0 = 0.0; // hard code the initial time.
  pars.tf = 1.0;
  pars.xmin = -100.0;
  pars.xmax = 100.0;
  pars.ymin = -100.0;
  pars.ymax = 100.0;
  pars.zmin = -100.0;
  pars.zmax = 100.0;

  pars.cfl = 0.25;

  pars.lambda_f0 = 1;
  pars.lambda_f1 = 1;
  pars.eta_damping_exp = 2;
  pars.eta = 1.0;
  pars.R_0 = 40.0;
  pars.trK0 = 0.0;
  pars.lambda_1 = 1;
  pars.lambda_2 = 1;
  pars.lambda_3 = 1;
  pars.lambda_4 = 1;
  pars.psi_floor = 0.0001;
  pars.alpha_floor = 0.0001;
  pars.sigma_diss  = 0.0;
  pars.sigma_1  = 0.001;
  pars.sigma_2  = 0.001;

  pars.mass1 = 1.0;
  pars.bh1x  = -10.0;
  pars.bh1y  =   0.0;
  pars.bh1z  =   0.0;
  pars.vx1   =   0.0;
  pars.vy1   =   0.1;
  pars.vz1   =   0.0;
  pars.spin1 =   0.0;
  pars.spin1_th = 0.0;
  pars.spin1_ph = 0.0;

  pars.mass2 = 1.0;
  pars.bh2x  =  10.0;
  pars.bh2y  =   0.0;
  pars.bh2z  =   0.0;
  pars.vx2   =   0.0;
  pars.vy2   =   -0.1;
  pars.vz2   =   0.0;
  pars.spin2 =   0.0;
  pars.spin2_th = 0.0;
  pars.spin2_ph = 0.0;

  return 1;
}
