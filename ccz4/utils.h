#ifndef HAWAII_UTILS_H
#define HAWAII_UTILS_H

#include <stdbool.h>
#include "types.h"
#include "wavelet.h"

#include "bbhutil.h"
#include <silo.h>


extern double eps[n_variab];
extern double eps_scale[n_variab];
extern double L_dim[n_dim];
extern const int deriv_offset[6];

typedef struct vizpoint {
  double x;
  double y;
  double z;
  double value[n_variab+n_aux];
} vizpoint;

void validate_parameters(void);

index_t linear_to_index(const int rank);

int index_to_linear(const index_t *index);

index_t add_index(const index_t *lhs, const index_t *rhs, const int scalar);

bool check_index(const index_t *index);

coord_t set_coordinate(const index_t *index);

double norm2(const coord_t *point1, const coord_t *point2);

coll_status_t set_point_status(const double *wavelet);

coll_status_t superior_status(const coll_status_t lhs,
                              const coll_status_t rhs);

coll_status_t inferior_status(const coll_status_t lhs,
                              const coll_status_t rhs);

bool has_superior_status(const coll_status_t lhs,
                         const coll_status_t rhs);

bool has_inferior_status(const coll_status_t lhs,
                         const coll_status_t rhs);

bool has_same_status(const coll_status_t lhs,
                     const coll_status_t rhs);

int get_level(const index_t *index);

void check_wavelet_stencil(coll_point_t *point, const int gen);

void check_derivative_stencil(coll_point_t *point, const int gen);

void check_extension_stencil(coll_point_t *point, const int gen,
                             const int dir, const char type);

void advance_time_stamp(coll_point_t *point, const int gen);

double get_global_dt(void);

void minmax_range_reducer(double *u, double *mins, double *maxs);

void set_eps_scale(double *mins, double *maxs);

void wavelet_adjust_grids_helper(coll_point_t *point);

void status_validate_down_helper(coll_point_t *point);

void complete_deriv_stencils();
void deriv_stencil_completion_helper(coll_point_t *point);

void visualize_grid(char *fname, int noness);

void advance_nonessen_stage(coll_point_t *nonessen_point, const int *mask,
                            const int gen, const int stage);

int try_closest_level(const coll_point_t *point, const int dir);
int get_closest_level(const coll_point_t *point, const int dir);

int64_t skip_at_level(int level);

int get_next_file_number();
void set_next_file_number(int snap);

void grid_sdf_output(unsigned int stamp,double t, int gen);

#endif
