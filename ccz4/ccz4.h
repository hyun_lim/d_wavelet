#ifndef HAWAII_BSSN_H
#define HAWAII_BSSN_H

#include "param.h"
#include "types.h"

#define NSTENCIL 7

/* conservative variables */
#define U_ALPHA 0
#define U_SHIFTX 1
#define U_SHIFTY 2
#define U_SHIFTZ 3
#define U_PSI 4 
#define U_TRK 5
#define U_GAMHX 6
#define U_GAMHY 7
#define U_GAMHZ 8
#define U_GBX 9
#define U_GBY 10
#define U_GBZ 11
#define U_GTXX 12
#define U_GTXY 13
#define U_GTXZ 14
#define U_GTYY 15
#define U_GTYZ 16
#define U_GTZZ 17
#define U_ATXX 18
#define U_ATXY 19
#define U_ATXZ 20
#define U_ATYY 21
#define U_ATYZ 22
#define U_ATZZ 23
#define U_THETA 24 

#endif 
