% The methods section.

%%{intuition}
Two desirable qualities of any partitioning strategy are load balancing, and minimization of overlap between the processor domains. SFC-based partitioning does a very good job in load balancing but do not permit an explicit control on the level of overlap.
Simple partitions, such as those producing relatively cuboidal partitions have a smaller overlap compared with more irregular partitions, as might be produced by a SFC-based partitioning algorithm.  
%%{block part}
In \cite{SundarSampathBiros08} we proposed a heuristic partitioning scheme based on the intuition that a coarse grid partition is more likely to have a smaller overlap between the processor domains as compared to a partition computed on the underlying fine grid. This algorithm first constructed and partitioned a complete linear octree based on the data (equivalent to a standard SFC-based partitioning). This was followed by coarsening of the ocree and a second weighted partitioning of the coarse octree to get {\em simpler} partitioning. There are a few shortcomings of this approach. Firstly, this is a heuristic and there are no guarantees that the partition produced will be better than using the standard SFC-based partition. This is mainly due to the reliance on an external sorting function and the bottom-up (coarsening) fashion in which it operates. 

\subsection{Distributed Memory TreeSort}
\label{sec:dsort}

Our modified SFC-ordering algorithm \tsort ~operates in a top-down fashion and we propose a distributed variant of \tsort ~that enables fine control over the load-balance and also enables reduction in communication costs in exchange for higher load-imbalance. The distributed algorithm proceeds as the sequential variant (\S\ref{sec:tsort}), bucketing and partially sorting the data. Unlike the sequential \tsort, we have to traverse the tree in a breadth first fashion, as the data needs to be distributed across processors. Note that at each level, we split each octant $8$ times (for $3$D), so in $\log_8 p$ steps we will have $p$ buckets. A reduction provides us with the global ranks of these $p$ buckets. Using the optimal ranks ($p_rn/p \pm tol$) at which the data needs to be partitioned, we selectively partition the buckets to obtain the correct partitioning of the local data. This is in principle similar to the approach used by algorithms such as histogramsort \cite{solomonik10} and hyksort\cite{hyksort}, except that no comparisons are needed for computing the ranks. The computational cost corresponds to the $\mathcal{O}(N/p)$ bucketing required for each of the $\log_8 p$ levels. We also need $p$ reductions and an all-to-all data exchange. Therefore the expected running time for the distributed algorithm is,
\[
T_p = t_c\frac{N}{p} + (t_s + t_wp) \log p + t_w \frac{N}{p}.
\]  
In our evaluation, we considered trees of depth $30$ (so that the coordinates can be represented using \texttt{unsigned int}). Note that upto $8^6 = 262,144$ buckets can be determined using six levels, so the cost of determining splitters to distribute the data across processors is significantly lower than other approaches. An equivalent analysis of complexities for popular distributed sorting algorithms can be found in \cite{hyksort}.    

While \tsort~ is performed in place, the distributed version requires $\mathcal{O}(p)$ additional storage to perform the reduction to compute the global splitter ranks. The additional storage as well as the cost of performing the reductions can be significant for large $p$, therefore we perform the splitter selection in a staged manner. We limit the maximum number of splitters to a user-specified parameter $k\leq p$. This also reduces the cost of the reduction to compute the global ranks of the splitters from $\mathcal{O}(p\log p)$ to $\mathcal{O}(k\log p)$. The expected running time for the staged distributed \tsort ~is,
\[
T_p = t_c\frac{N}{p} + (t_s + t_w k) \log p + t_w \frac{N}{p}.
\] 

Additionally, the all-to-all exchange is also performed in a staged manner similar to \cite{hyksort,bruck97}, avoiding potential network congestion. 
%The pseudocode for the staged distributed \tsort ~is given in Algorithm~\ref{alg:dsort}. This version assumes that the best tolerance corresponding to the problem and architecture is known. 
We will now develop the algorithm further to automatically determine the best tolerance. 

% Describe flexible partition here.
\subsection{Justification for Flexible Partitioning}
\label{sec:flexPart}

An advantage of the distributed \tsort~ algorithm is that we can specify a tolerance for the desired load-balance. While this is possible for samplesort variants \cite{kale93,hyksort,solomonik10}, 
%there are no guarantees on the quality of the resulting partition. 
the advantage is limited to reducing the cost of computing the partition or ordering at the cost of reduced load-balance. SFC-based partitioning algorithms are likely to partition using the \textit{finest} level octant, resulting is increased boundary surface, as the primary criterion is to equally divide the work (octants) amongst the processes. Our hypothesis is that, it should be possible to find a partition in close proximity to the optimally load-balanced partition, that has a lower inter-process boundary surface. This will enable users to get the partition with the minimum boundary surface, by specifying a tolerance on the load-balance. 
%As previously mentioned, such guarantees are not possible with traditional SFC-based partitioning algorithms. 
In case of \tsort, specifying a tolerance, in addition to making the ordering faster to compute, the induced partition also has reduced communication costs (for subsequent computations, like numerical simulations) in exchange for the increased load-imbalance. This makes \tsort ~attractive when the communication costs are high. 


\begin{figure}%
  \centering
  \subfloat[{$\scriptstyle l=1,~\lambda=2,~s=16$}]{
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.05,0) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(2,1); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
    }
  \subfloat[{$\scriptstyle l=2,~\lambda=1.2,~s=24$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0) rectangle +(0.5,0.525);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(1.025,0.5); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=3,~\lambda=1.05,~s=28$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.525,0.25); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=4,~\lambda=1.01,~s=30$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    \draw[blue!30,fill=blue!20,ultra thin] (1.7625,0) rectangle +(0.125,0.125);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0.1325) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.9,0) rectangle +(0.125,0.1325);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    \draw[green!30,fill=green!20,ultra thin] (1.775,1.775) rectangle +(0.25,0.125);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.25,0.25); 
    \draw[orange!30,fill=orange!50,ultra thin] (1.75,1.925) rectangle +(0.275,0.125); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \caption{\label{fig:flex} Illustration of the increase in communication costs with increasing levels of \tsort. Partitions for the case of $p=3$ are drawn with the boundary of the partition ($s$) and the load-imbalance ($\lambda$) given along with the level ($l$) at which the partition is defined. At each level, the orange partition (\textcolor{orange!50}{$\blacksquare$}) gets the extra load that is progressively reduced. The green partition (\textcolor{green!40}{$\blacksquare$}) gets the largest boundary that progressively increases.
  }
\end{figure}%


By design, for the \tsort ~algorithm the load-imbalance, $\lambda = \max_r(|A_r|)/\min_r(|A_r|)$ decreases with increasing $l$ getting closer to the optimal value of $\lambda=1$. However, as we increase levels, the boundary of the partition $s$ is non-decreasing. This is illustrated in Figure~\ref{fig:flex} using a simple 2D partition using 3 processors. This allows the user to specify a tolerance, say $1\%$, which when reached will prevent further refinement, potentially reducing the inter-process boundary and thereby the communication costs of subsequent operations. Note that the claim is not for reducing the data-exchange cost during the reordering, but for subsequent operations that might be performed using the partition.  

The example in Figure~\ref{fig:flex} considers uniform refinement, but the result is also true for meshes with adaptive refinement, as would be the case for most numerical simulations. This is explained in Figure~\ref{fig:flexProof}. Here we consider the partition between two processors, and the specific element that will be refined at the next level. It can be seen that for all cases except one, the surface area of the partition is non-decreasing. The case where the surface area decreases is a case of extreme refinement, that will only occur if the last child has a significantly higher refinement compared to the other siblings. While this case appears to limit the effectiveness of the approach, it is important to realize that other more-expensive approaches like spectral bisection \cite{pothen90} also fail for similar examples.  

\begin{figure}
  \centering  
  \begin{tikzpicture}[scale=0.18]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \draw[blue, ultra thick] (0,0) -- (0,8);
  %\draw[red, ultra thick] (0,0) -- (8,0);
  %\draw[red, ultra thick] (8,0) -- (8,8);
  %\draw[red, ultra thick] (0,8) -- (8,8);
  \node at (-1,4) {$2$};
  
  \begin{scope}[shift={(9,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \node at (2,2) {$4$};    
  \end{scope}
  
  \begin{scope}[shift={(18,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$4$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);        
  \end{scope}
  
  \begin{scope}[shift={(9,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  
  \node at (2,2) {$4$};
  \end{scope}    
  
  \begin{scope}[shift={(18,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$6$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);   
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \end{scope}
  
  \begin{scope}[shift={(9,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);  
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};           
  \end{scope}    
  
  \begin{scope}[shift={(18,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(27,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  %\draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,4) -- (8,8);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (2,6) {$4$};
  
  \draw[step=2] (4,4) grid +(4,4);
  \draw[step=1] (6,6) grid +(2,2);
  \draw[step=0.5] (7,7) grid +(1,1);
  \end{scope}
  
  \end{tikzpicture}
  \caption{\label{fig:flexProof} Demonstration that the communication costs are non-decreasing with increasing levels of \tsort ~for most refinements and identification of the pathological case where the surface area decreases (bottom-right). The left column corresponds to initial boundaries (blue line) sharing 1, 2, and 3 faces of the quadrant (rows) that will be refined at level $l$. The remaining columns illustrate the change in surface area for the cases where 1-3 child nodes get added to the blue partition. The numbers represent the surface area of each case.}
\end{figure}

%\begin{itemize}
 %\item {\bf Intuition:} Why do we want to use coarse partitions.  
 %\item {\bf BlockPart:} General idea has been done before, but not in a systematic manner. We expect it to work, but it was dictated by the local partition. Also no way to incorporate a performance model.
 %\item {\bf Distributed tsort:} as a better version of block part. Lead to that communication goes down with increasing levels. 
 %\item {\bf proof:} Arguments in favor of why the communication goes down.
 %\item {\bf Slack:} introduce the notion of the tolerance for the amount of load-imbalance we are willing to tolerate. Lead up to why this is sensitive to the architecture and problem and why it should be automatically determined. 
 %\item {\bf Performance model:} how much slack can be tolerated? Can this be simply predicted based on the memory and network bandwidth as well as the application. 
 %\item {\bf flexPart:} finally the distributed optiPart algorithm.  
%\end{itemize}


\subsection{Performance Model}

While having the user specify a tolerance for the load-balance in order to lower the communication costs allows for better performance, it does limit the portability of the method and makes it difficult when either the architecture or the data distribution changes. Ideally, we would like to automatically determine the optimum tolerance based on the data and the machine characteristics. As mentioned previously, the tradeoff is between the work load and the communication costs across all machines. Additionally, the time for either stage will be dominated by the processor that has the maximum load ($W_{max}$) or has to communicate the maximum ($C_{max}$) amount. We build a simple performance model to characterize the overall parallel runtime as combination of these two terms. We further note that these times can be estimated using the network bandwidth ($1/t_w$) and the memory bandwidth ($1/t_c$). In other words the total runtime ($T_p$) can be characterized by the following equation,
\begin{equation}
	\label{eq:model}
	T_p = \alpha t_c W_{max} + t_w C_{max}.
\end{equation} 
Here, $\alpha$ is indicative of the number of memory accesses performed per unit of work. For example, if the target application is a $7$-point stencil operation, then $\alpha$ will be $\sim 8$. In general, this can be computed using a simple sequential profiling of the main execution kernel. While this model ignores aspects such as overlapping computation and communication, it is simple and can help us easily determine if given a set of partitions with different $W_{max},C_{max}$ which partition is likely to be the most efficient.  

We would also like to briefly discuss the model from the energy perspective. For most modern cluster architectures, the overall energy will be strongly correlated with the overall runtime. Assuming an efficient processor architecture, the overall energy for the computation will not depend on the partitioning, as the sum of \emph{work} will remain the same. The overall energy cost of communication will however be lower for the lowest total data communicated. This is what we aim for, i.e., a partition that gives the best runtime by balancing $W_{max}$ and $C_{max}$ and minimizes the total energy required for the computation by additionally minimizing $C_{max}$. 

\subsection{\dsort: Architecture \& Data optimized partitioning}

%\begin{equation}
%E_{tot} = \alpha W_{max} + \beta C_{max} + \gamma \label{eq::performance_model}
%\end{equation}

Armed with our performance model, we can easily modify the distributed \tsort ~to compute the optimal partition without having to guess the appropriate tolerance. We will use the memory and network slowness ($t_c,t_w$) based on the machine and will expect the user to provide the parameter $\alpha$ that is representative of the subsequent computations. We call this algorithm \dsort. The algorithm proceeds the same as distributed \tsort ~, but instead relies on estimates of $W_{max}, C_{max}$ for the current and next refinements and proceeds only if the runtime estimates (\ref{eq:model}) for the next refinement are lower than the current estimate. This does not change the complexity of the partitioning algorithm, requiring only $\mathcal{O}(N/p)$ local work and a single reduction $\mathcal{O}(\log p)$. The pseudocode for the staged \dsort ~is given in Algorithm~\ref{alg:dsort}.

\begin{algorithm}[t]
  \caption{\dsort}\label{alg:dsort}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A distributed list of points or regions $W_r$, $\mathit{comm}$, $p$ (w.l.g., assume $p= mk$),
    $p_r$ of current task in $\mathit{comm}$,$\alpha$,$t_c$,$t_w$,%$tol$ load flexibility, 
    \Ensure globally sorted array $W$
    %\State $split\_count \leftarrow 1$
    \State $counts\_local \leftarrow [\ ], counts\_global \leftarrow [\ ]$    
    \State $s \leftarrow \tsort(W_r,l-\log(p),l)$ \Comment initial splitter computation
    \State $bdyOctants \leftarrow computeLocalBdyOctants(A_r,s)$
    \State $localSz \leftarrow size(A_r,s)$
    \State $\mathtt{MPI\_ReduceAll}(bdyOctants,C\_max,\mathtt{MPI\_MAX},comm)$
    \State $\mathtt{MPI\_ReduceAll}(localSz,W\_max,\mathtt{MPI\_MAX},comm)$ 
    \State $default \leftarrow \alpha t_c W\_max + t_w C\_max $ 
    %\While {$|s_r-\frac{rn}{p}|>tol$}  
    \State $current \leftarrow default$
    \While {$default \geq current$}   
    \State $counts \leftarrow 0$ \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    \State $counts\_local \leftarrow push(counts)$  
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor
    %\State $split\_count \leftarrow 2^{dim}\times split\_count$
    
    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,comm)$
    \State $s \leftarrow select(s,counts\_global)$
    
    \State $bdyOctants \leftarrow computeLocalBdyOctants(A_r,s)$
    \State $localSz \leftarrow size(A_r,s)$
    \State $\mathtt{MPI\_ReduceAll}(bdyOctants,C\_max,\mathtt{MPI\_MAX},comm)$
    \State $\mathtt{MPI\_ReduceAll}(localSz,W\_max,\mathtt{MPI\_MAX},comm)$ 
    \State $current \leftarrow \alpha t_c W\_max + t_w C\_max $
    \EndWhile
    %	  \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    % \Comment optimal splitter computation (with in the given tolerance)	  
    \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    \Comment Staged All2all
    %	  \While{$p > 1$}		\Comment Iters: $\mathcal{O}(\log p / \log k)$
    %      \State $N \leftarrow \mathtt{MPI\_AllReduce}(|B|, \mathit{comm})$
    %      \State $s \leftarrow \mathtt{ParallelSelect}\left(B, \left\{i \, N/k~|~i=1,...,k-1\right\} \right)$
    %      \State $d_{i+1} \leftarrow \mathtt{Rank}(s_i, B) ,~~ \forall i$ 				%\Comment $\bigO(p \log n)$
    %      \State $\left[d_0,~d_k\right] \leftarrow \left[ 0,~n \right]$
    %      \State $color \leftarrow \lfloor k \, p_r/p \rfloor $
    %      \ForAll{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $R_i \leftarrow \mathtt{MPI\_Irecv}(p_{recv}, \mathit{comm})$
    %      \EndFor
    %      \For{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $p_{send} \leftarrow m \, ((color+i)\bmod k) + (p_r \bmod m)$
    %      \State $\mathtt{MPI\_Issend} ( B[d_i,...,d_{i+1}-1] , p_{send}, \mathit{comm})$
    %      \State $j \leftarrow 2$
    %      \While {$ i>0 ~ and ~ i \bmod j = 0$}
    %      \State $R_{i-j} \leftarrow \mathtt{merge}(R_{i-j},R_{i-j/2})$
    %      \State $j \leftarrow 2 j$
    %      \EndWhile
    %      \State $\mathtt{MPI\_WaitRecv}(p_{recv})$
    %      \EndFor
    %      \State $\mathtt{MPI\_WaitAll}()$
    %      \State $B \leftarrow \mathtt{merge}(R_{0},R_{k/2})$
    %      \State $\mathit{comm} \leftarrow \mathtt{MPI\_Comm\_split}(color, \mathit{comm})$
    %      \State $p_r \leftarrow \mathtt{MPI\_Comm\_rank}(\mathit{comm})$
    %      \EndWhile
    \State \tsort$(W_r, 0, l)$ \Comment local sort
  \end{algorithmic}
\end{algorithm}


