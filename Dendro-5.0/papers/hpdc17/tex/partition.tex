% The methods section.

%%{intuition}
Two desirable qualities of any partitioning strategy are load balancing, and minimization of overlap between the processor domains. SFC-based partitioning does a very good job in load balancing but do not permit an explicit control on the level of overlap. Ideally, we would like to have a perfectly load-balanced partition that also minimizes the overall communication. But this is usually not possible, especially for non-uniform work distributions such as for adaptively refined meshes.  
Additionally, it might not be possible to minimize the load-imbalance and overall communication simultaneously. 
Since the cost of communication across inter-process boundaries depends both on the machine characteristics, say network bandwidth, as well as the application, i.e., the amount of data being exchanged per unit boundary, it is important to consider these aspects while partitioning data. Specifically, since the goal of most parallel codes is to minimize time-to-solution (and possibly energy-to-solution), it is important that the partition balances ``equal assignment of work'' with ``overall communication cost'' to achieve these goals. Clearly such a balance is dependent on both the machine-characteristics as well as the application's data-dependencies. In this section, we will incorporate these features into SFC-based partitioning.  


Simple partitions, such as those producing relatively cuboidal partitions have a smaller overlap compared with more irregular partitions, as might be produced by a SFC-based partitioning algorithm.  
%%{block part}
In \cite{SundarSampathBiros08} we proposed a heuristic partitioning scheme based on the intuition that a coarse grid partition is more likely to have a smaller overlap between the processor domains as compared to a partition computed on the underlying fine grid. This algorithm first constructed and partitioned a complete linear octree based on the data (equivalent to a standard SFC-based partitioning). This was followed by coarsening of the ocree and a second weighted partitioning of the coarse octree to get {\em simpler} partitioning. There are a few shortcomings of this approach. Firstly, this is a heuristic and there are no guarantees that the partition produced will be better than using the standard SFC-based partition. This is mainly due to the reliance on an external sorting function and the bottom-up (coarsening) fashion in which it operates. Secondly, the algorithm considers neither the machine-characteristics nor the application characteristics and will produce the same partition on different machines and for different applications\footnote{e.g. for the Poisson equation vs the wave Equation on the same mesh.}. OptiPart addresses these shortcomings. We will first present the distributed memory version of \tsort, then demonstrate that decreasing the load-imbalance via this algorithm results in a monotic increase in overall communication costs, allowing users to specify a tolerance and reduce overall communication costs. We then develop a performance model to estimate the optimal tolerance to obtain the best time-to-solution for the specific machine and application.    

\subsection{Distributed Memory TreeSort}
\label{sec:dtreesort}

Our modified SFC-ordering algorithm \tsort ~operates in a top-down fashion. In this section, 
we propose a distributed variant of \tsort ~that enables fine control over the
load-balance and also enables reduction in communication costs in exchange for
higher load-imbalance. The distributed algorithm proceeds as the sequential
variant (\S\ref{sec:tsort}), bucketing and partially sorting the data. Unlike
the sequential \tsort, we have to traverse the tree in a breadth first fashion,
as the data needs to be distributed across processors. Note that at each level,
we split each octant $8$ times (for $3$D), so in $\log_8 p$ steps we will have
$p$ buckets. A reduction provides us with the global ranks\footnote{The rank here referes to the position of a element in a sorted array.} of these $p$
buckets. Here $p$ is the desired number of partitions. Using the optimal ranks ($r\cdot N/p$) at which the data needs to
be partitioned for process $r$, we selectively partition the buckets to obtain the correct
partitioning of the local data. This is in principle similar to the approach
used by algorithms such as histogramsort \cite{solomonik10} and
hyksort\cite{hyksort}, except that no comparisons are needed for computing the
ranks. The computational cost corresponds to the $\mathcal{O}(N/p)$ bucketing
required for each of the $\log_8 p$ levels. We also need $p$ reductions and an
all-to-all data exchange. Therefore the expected parallel runtime, $T_p$ for the
distributed algorithm is,
\begin{equation}
T_p = t_c\frac{N}{p} + (t_s + t_wp) \log p + t_w \frac{N}{p},
\end{equation}
where $t_c, t_s$, and $t_w$ are the memory slowness (1/RAM bandwidth), the network latency and the network slowness (1/bandwidth), respectively. % \note{remove newline}
%
In our evaluation, we considered trees of depth $30$ (so that the coordinates
can be represented using \texttt{unsigned int}). Note that upto $8^6 = 262,144$
buckets can be determined using six levels, so the cost of determining
splitters to distribute the data across processors is significantly lower than
other approaches. An analysis of complexities for popular
distributed sorting algorithms can be found in \cite{hyksort}.    

While \tsort~ is performed in place, the distributed version requires $\mathcal{O}(p)$ additional storage to perform the reduction to compute the global splitter ranks. The additional storage as well as the cost of performing the reductions can be significant for large $p$, therefore we perform the splitter selection in a staged manner. We limit the maximum number of splitters to a user-specified parameter $k\leq p$. This also reduces the cost of the reduction to compute the global ranks of the splitters from $\mathcal{O}(p\log p)$ to $\mathcal{O}(k\log p)$. The expected running time for the staged distributed \tsort ~is,
\begin{equation} 
T_p = t_c\frac{N}{p} + (t_s + t_w k) \log p + t_w \frac{N}{p}.
\end{equation}
Additionally, the all-to-all exchange is also performed in a staged manner similar to \cite{hyksort,bruck97}, avoiding potential network congestion. 
%The pseudocode for the staged distributed \tsort ~is given in Algorithm~\ref{alg:dsort}. This version assumes that the best tolerance corresponding to the problem and architecture is known. 
We will now develop the algorithm further to automatically determine the best tolerance. 

\begin{remark}
 We will develop the distributed \tsort~ algorithm further to balance work and communication costs based on the machine-model in \S\ref{sec:dsort}. Therefore, for clarity of presentation, we are not presenting the pseudocode for distributed \tsort. The pseudocode for \dsort~ is presented in Algorithm~\ref{alg:dsort}.
\end{remark}

% Describe flexible partition here.
\subsection{Justification for Flexible Partitioning}
\label{sec:flexPart}

An advantage of the distributed \tsort~ algorithm is that we can specify a tolerance, $tol$, for the desired load-balance, i.e., stopping if the induced partitions are $r \cdot N/p \pm tolerance$ instead of the optimal $r\cdot N/p$. While it is possible to specify such a tolerance for samplesort variants \cite{kale93,hyksort,solomonik10}, 
%there are no guarantees on the quality of the resulting partition. 
the advantage is limited to reducing the cost of computing the partition or ordering at the cost of reduced load-balance and will not provide any reduction in communication costs. 
SFC-based partitioning algorithms are likely to partition using the \textit{finest} level octant, resulting is increased boundary surface, as the primary criterion is to equally divide the work (octants) amongst the processes. Our hypothesis is that, it should be possible to find a partition in close proximity to the optimally load-balanced partition, that has a lower inter-process boundary surface. This will enable users to get the partition with the minimum boundary surface, by specifying a tolerance on the equally-divided load. 
%As previously mentioned, such guarantees are not possible with traditional SFC-based partitioning algorithms. 
In case of \tsort, specifying a tolerance, in addition to making the ordering faster to compute, the induced partition also has reduced communication costs (for subsequent computations, like numerical simulations) in exchange for the increased load-imbalance. This makes \tsort ~attractive when the communication costs are high. If we consider the cost of communication to be \texttt{10x} that of performing the work on one unit of data, then an increase of $20$ units of work resulting in a reduction of $5$ units of data-exchange, would still provide savings of $5\times 10 - 20=30$ units. This is a contrived example, but the key point is that even small reductions in data-movement over the network provide large savings in overall runtime. 


\begin{figure}%
  \centering
  \subfloat[{$\scriptstyle l=1,~\lambda=2,~s=16$}]{
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.05,0) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(2,1); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
    }
  \subfloat[{$\scriptstyle l=2,~\lambda=1.2,~s=24$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0) rectangle +(0.5,0.525);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(1.025,0.5); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  } 
  \subfloat[{$\scriptstyle l=3,~\lambda=1.05,~s=28$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.525,0.25); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=4,~\lambda=1.01,~s=30$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    \draw[blue!30,fill=blue!20,ultra thin] (1.7625,0) rectangle +(0.125,0.125);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0.1325) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.9,0) rectangle +(0.125,0.1325);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    \draw[green!30,fill=green!20,ultra thin] (1.775,1.775) rectangle +(0.25,0.125);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.25,0.25); 
    \draw[orange!30,fill=orange!50,ultra thin] (1.75,1.925) rectangle +(0.275,0.125); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \caption{\label{fig:flex} Illustration of the increase in communication costs with increasing levels of \tsort. Partitions for the case of $p=3$ are drawn with the boundary of the partition ($s$) and the load-imbalance ($\lambda$) given along with the level ($l$) at which the partition is defined. At each level, the orange partition (\textcolor{orange!50}{$\blacksquare$}) gets the extra load that is progressively reduced. The green partition (\textcolor{green!40}{$\blacksquare$}) gets the largest boundary that progressively increases.
  }
\end{figure}%


By design, for the \tsort ~algorithm the load-imbalance, $\lambda = \max(|W_r|)/\min(|W_r|)$ decreases with increasing $l$ getting closer to the optimal value of $\lambda=1$. However, as we increase levels, the boundary of the partition $s$ is non-decreasing. This is illustrated in Figure~\ref{fig:flex} using a simple 2D partition using 3 processors. This allows the user to specify a tolerance, say $1\%$, which when reached will prevent further refinement, potentially reducing the inter-process boundary and thereby the communication costs of subsequent operations. Note that the claim is not for reducing the data-exchange cost during the reordering, but for subsequent operations that might be performed based on the partition.  

The example in Figure~\ref{fig:flex} considers uniform refinement, but the result is also true for meshes with adaptive refinement, as would be the case for most numerical simulations. This is demonstrated in Figure~\ref{fig:flexProof}. Here we consider the partition between two processors, and the specific element that will be refined at the next level. It can be seen that for all cases except one, the surface area of the partition is non-decreasing. The case where the surface area decreases is a case of extreme refinement, that will only occur if the last child has a significantly higher refinement compared to the other siblings. While this case appears to limit the effectiveness of the approach, it is important to realize that other more-expensive approaches like spectral bisection also fail for similar examples \cite{pothen90}.  

\begin{figure}
  \centering  
  \begin{tikzpicture}[scale=0.18]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \draw[blue, ultra thick] (0,0) -- (0,8);
  %\draw[red, ultra thick] (0,0) -- (8,0);
  %\draw[red, ultra thick] (8,0) -- (8,8);
  %\draw[red, ultra thick] (0,8) -- (8,8);
  \node at (-1,4) {$2$};
  
  \begin{scope}[shift={(9,0)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4); 
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \node at (2,2) {$4$};    
  \end{scope}
  
  \begin{scope}[shift={(18,0)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4);
  \draw[fill=sq_b4] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,0)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4); 
  \draw[fill=sq_b4] (0,4) rectangle +(4,4); 
  \draw[fill=sq_b4] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-9)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$4$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);        
  \end{scope}
  
  \begin{scope}[shift={(9,-9)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  
  \node at (2,2) {$4$};
  \end{scope}    
  
  \begin{scope}[shift={(18,-9)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4);
  \draw[fill=sq_b4] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,-9)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4); 
  \draw[fill=sq_b4] (0,4) rectangle +(4,4); 
  \draw[fill=sq_b4] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-18)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$6$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);   
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \end{scope}
  
  \begin{scope}[shift={(9,-18)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);  
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};           
  \end{scope}    
  
  \begin{scope}[shift={(18,-18)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4);
  \draw[fill=sq_b4] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(27,-18)}]
  \draw[fill=sq_r5] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=sq_b4] (0,0) rectangle +(4,4); 
  \draw[fill=sq_b4] (0,4) rectangle +(4,4); 
  \draw[fill=sq_b4] (4,0) rectangle +(4,4);
  
  %\draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,4) -- (8,8);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (2,6) {$4$};
  
  \draw[step=2] (4,4) grid +(4,4);
  \draw[step=1] (6,6) grid +(2,2);
  \draw[step=0.5] (7,7) grid +(1,1);
  \end{scope}
  
  \end{tikzpicture}
  \caption{\label{fig:flexProof} Demonstration that the communication costs are non-decreasing with increasing levels of \tsort ~for most refinements and identification of the pathological case where the surface area decreases (bottom-right). The left column corresponds to initial boundaries (blue line) sharing 1, 2, and 3 faces of the quadrant (rows) that will be refined at level $l$. The remaining columns illustrate the change in surface area for the cases where 1-3 child nodes get added to the blue partition. The numbers represent the surface area of each case.}
\end{figure}

%\begin{itemize}
 %\item {\bf Intuition:} Why do we want to use coarse partitions.  
 %\item {\bf BlockPart:} General idea has been done before, but not in a systematic manner. We expect it to work, but it was dictated by the local partition. Also no way to incorporate a performance model.
 %\item {\bf Distributed tsort:} as a better version of block part. Lead to that communication goes down with increasing levels. 
 %\item {\bf proof:} Arguments in favor of why the communication goes down.
 %\item {\bf Slack:} introduce the notion of the tolerance for the amount of load-imbalance we are willing to tolerate. Lead up to why this is sensitive to the architecture and problem and why it should be automatically determined. 
 %\item {\bf Performance model:} how much slack can be tolerated? Can this be simply predicted based on the memory and network bandwidth as well as the application. 
 %\item {\bf flexPart:} finally the distributed optiPart algorithm.  
%\end{itemize}


\subsection{Performance Model}
\label{sec:perf_model}

While having the user specify a tolerance for the load-balance in order to
lower the communication costs allows for better performance, it does limit the
portability of the method and makes it difficult when either the architecture
or the data distribution changes. Ideally, we would like to automatically
determine the optimum tolerance based on the application and the machine
characteristics. As mentioned previously, the tradeoff is between the load
and the communication costs across all machines. Additionally, the time for
either stage will be dominated by the processor that has the maximum load
($W_{max}$) or has to communicate the maximum ($C_{max}$) amount of data. We build a
simple performance model to characterize the overall parallel runtime as a
combination of these two terms. We further note that these times can be
estimated using the network bandwidth ($1/t_w$) and the memory bandwidth
($1/t_c$)--for memory-bound computations. In other words the total runtime ($T_p$) can be characterized by the
following equation,

\begin{equation}
	\label{eq:model}
	T_p = \alpha t_c W_{max} + t_w C_{max}.
\end{equation} 
Here, $\alpha$ is indicative of the number of memory accesses performed per unit of work. For example, if the target application is a $7$-point stencil operation, then $\alpha$ will be $\sim 8$. In general, this can be computed using a simple sequential profiling of the main execution kernel. While this model ignores aspects such as overlapping computation and communication, it is simple and can help us easily determine if given a set of partitions with different $W_{max},C_{max}$ which partition is likely to be the most efficient.  

\begin{remark}
  It is easy to modify (\ref{eq:model}) for a compute-bound application, and even use a simple profiling step to determine the right parameters for running a specific application on a specific architecture.
\end{remark}

We would also like to briefly discuss the model from the energy perspective. For most modern cluster architectures, the overall energy will be strongly correlated with the overall runtime. Assuming an efficient processor architecture, the overall energy for the computation will not depend on the partitioning, as the sum of \emph{work} will remain the same. The overall energy cost of communication will however be lower for the lowest total data communicated. This is what we aim for, i.e., a partition that gives the best runtime by balancing $W_{max}$ and $C_{max}$ and minimizes the total energy required for the computation by additionally minimizing $C_{max}$. However, since energy-to-solution is an increasingly important metric for current and future HPC systems, we will also analyze the energy-to-solution in addition to time-to-solution while evaluating our new partitioning algorithm.

\subsection{\dsort: Architecture \& Data optimized partitioning}
\label{sec:dsort}

%\begin{equation}
%E_{tot} = \alpha W_{max} + \beta C_{max} + \gamma \label{eq::performance_model}
%\end{equation}

Armed with our performance model, we can easily modify the distributed \tsort ~to compute the optimal partition without having to guess the appropriate tolerance. We will use the memory and network slowness ($t_c,t_w$) based on the machine and will expect the user to provide the parameter $\alpha$ that is representative of the core computations. We call this algorithm \dsort. The algorithm proceeds the same as distributed \tsort ~, but instead relies on estimates of $W_{max}, C_{max}$ for the current and next refinements and proceeds only if the runtime estimates (\ref{eq:model}) for the next refinement are lower than the current estimate. This does not change the complexity of the partitioning algorithm, requiring only $\mathcal{O}(N/p)$ local work and a single reduction $\mathcal{O}(\log p)$. \dsort~ relies on a helper routine that computes the quality of the current partition, by doing a linear pass over the elements to determine the size of the local boundary. The pseudocode for this routine is given in Algorithm~\ref{alg:pExecutionTime}. Finally, the pseudocode for \dsort ~is given in Algorithm~\ref{alg:dsort}. Note that the standard distributed \tsort~ can be recovered by iterating till the work is equally divided instead of using Algorithm~\ref{alg:pExecutionTime} to estimate the partition quality. 

\begin{algorithm}[t]
  \caption{PartitionQuality}\label{alg:pExecutionTime}
  \footnotesize
  \begin{algorithmic}[1]
	\Require A distributed list of elements or regions $A_r$, $\mathit{comm}$, splitters $s$,
	\Ensure  Predicted execution time $T_p$ for current splitters $s$
	\State $bdyOctants \leftarrow computeLocalBdyOctants(A_r,s)$
    \State $localSz \leftarrow size(A_r,s)$
    \State $\mathtt{MPI\_ReduceAll}(bdyOctants,C_{max},\mathtt{MPI\_MAX},comm)$
    \State $\mathtt{MPI\_ReduceAll}(localSz,W_{max},\mathtt{MPI\_MAX},comm)$ 
    \State $T_p \leftarrow \alpha t_c W_{max} + t_w C_{max}$
    \State \Return$T_p$
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}[t]
  \caption{\dsort}\label{alg:dsort}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A distributed list of elements or regions $A_r$, $\mathit{comm}$, $p$ (w.l.g., assume $p= mk$),
    $r$ of current task in $\mathit{comm}$,$\alpha$,$t_c$,$t_w$,%$tol$ load flexibility, 
    \Ensure globally sorted array $A$
    %\State $split\_count \leftarrow 1$
    \State $counts\_local [] \leftarrow 0, counts\_global [] \leftarrow 0$    
    \State $s \leftarrow \tsort(A_r,l-\log(p),l)$ \Comment initial splitter computation
    \State $default \leftarrow PartitionQuality(A_r, \mathit{comm},s)$ 
    %\While {$|s_r-\frac{rn}{p}|>tol$}  
    \State $current \leftarrow default$
    \While {$default \geq current$}   
    \State $counts [] \leftarrow 0$ \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$a \in A_r$}
    \State increment $counts[child\_num(a)]$
    \EndFor
    \State $counts\_local \leftarrow push(counts)$  
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \State $A^\prime [] \leftarrow empty$
		\For{$a \in A_r$}
		\State $i\leftarrow child\_num(a)$
		\State append $a$ to $A^\prime$ at offsets$[i]$
		\State increment offset$[i]$
		\EndFor 
%		\For{$w \in W$}
%		\State $i\leftarrow child\_num(w)$
%		\State append $w$ to $W_i$ at offsets$[i]$
%		\State increment offset$[i]$
%		\EndFor 
	\State $swap(A_r,A^\prime)$
    %\State $split\_count \leftarrow 2^{dim}\times split\_count$
    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,\mathtt{MPI\_SUM},comm)$
    \State $s \leftarrow select(s,counts\_global)$
    \State $default \leftarrow current$
    \State $current \leftarrow PartitionQuality(A_r, \mathit{comm},s)$
    \EndWhile
    %	  \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    % \Comment optimal splitter computation (with in the given tolerance)	  
    \State $\mathtt{MPI\_AlltoAllv}(W_r,s,comm)$
    \Comment Staged All2all
    %	  \While{$p > 1$}		\Comment Iters: $\mathcal{O}(\log p / \log k)$
    %      \State $N \leftarrow \mathtt{MPI\_AllReduce}(|B|, \mathit{comm})$
    %      \State $s \leftarrow \mathtt{ParallelSelect}\left(B, \left\{i \, N/k~|~i=1,...,k-1\right\} \right)$
    %      \State $d_{i+1} \leftarrow \mathtt{Rank}(s_i, B) ,~~ \forall i$ 				%\Comment $\bigO(p \log n)$
    %      \State $\left[d_0,~d_k\right] \leftarrow \left[ 0,~n \right]$
    %      \State $color \leftarrow \lfloor k \, p_r/p \rfloor $
    %      \ForAll{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $R_i \leftarrow \mathtt{MPI\_Irecv}(p_{recv}, \mathit{comm})$
    %      \EndFor
    %      \For{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $p_{send} \leftarrow m \, ((color+i)\bmod k) + (p_r \bmod m)$
    %      \State $\mathtt{MPI\_Issend} ( B[d_i,...,d_{i+1}-1] , p_{send}, \mathit{comm})$
    %      \State $j \leftarrow 2$
    %      \While {$ i>0 ~ and ~ i \bmod j = 0$}
    %      \State $R_{i-j} \leftarrow \mathtt{merge}(R_{i-j},R_{i-j/2})$
    %      \State $j \leftarrow 2 j$
    %      \EndWhile
    %      \State $\mathtt{MPI\_WaitRecv}(p_{recv})$
    %      \EndFor
    %      \State $\mathtt{MPI\_WaitAll}()$
    %      \State $B \leftarrow \mathtt{merge}(R_{0},R_{k/2})$
    %      \State $\mathit{comm} \leftarrow \mathtt{MPI\_Comm\_split}(color, \mathit{comm})$
    %      \State $p_r \leftarrow \mathtt{MPI\_Comm\_rank}(\mathit{comm})$
    %      \EndWhile
    \State \tsort$(A_r, 0, l)$ \Comment local sort
  \end{algorithmic}
\end{algorithm}


