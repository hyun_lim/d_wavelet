%\begin{itemize}
%  \item Computation sciences, FEM are an important application, where performance has progressively become smaller. 
%  \item SFC, Hilbert advantages
%  \item While the benefits of the Hilbert ordering have been known theoretically, it has not been used extensively across scales in an application. 
%  \item we present challenges, solutions and results from doing this. 
%  \item \textbf{contributions}
%  \item \textbf{limitations}
%  \item overview of the paper 
%\end{itemize}
%\begin{itemize}
%  \item million level parallelism
%  \item locality more important than ever
%  \item  cost of data movement, energy
%  \item important to limit the number of processors we communicate with
%  \item Speed of Meshing is important, especially for AMR
%  \item Theoretical advantages of Hilbert known, but adoption is limited because of the computational cost.
%\end{itemize}

As we scale up to exascale machines, the cost of data movement and
load-imbalances therein are a major bottleneck for achieving scalability\cite{doe10}
and energy and power efficiency \cite{shalf2011}. These are dictated largely by how
data (or tasks) are partitioned across processes.   
This motivates a need for better partitioning schemes for our most demanding 
applications. While we continue to build larger supercomputers, these come with increased parallelism at the node as well as the cluster level. In both cases, minimizing load and communication-costs as well as
minimizing data-movement are critical to ensuring scalability and performance on leadership architectures. 
These are also important for reducing the energy footprint of our algorithms and making extreme-scale computing economically viable \cite{shalf2011}. To be practical, such partitioning algorithms need to be optimal, i.e., with a $\mathcal{O}(N+\log p)$ parallel complexity for partitioning $N$ data across $p$ processes, ideally with low constants. 
Space Filling Curves (SFC) are commonly used by the HPC community for partitioning data\cite{campbell2003,devine2005,sundar2007} and for resource  allocations\cite{bender51,slurm}. By mapping high-dimensional spatial coordinates (or regions) onto a 1D curve, the task of partitioning is made trivial. Locality, i.e., ensuring that regions that are close in the higher dimensional space are also close along the 1D curve, is guaranteed by the \textit{ordering} of the curve, leading to different SFC, such as Morton, Hilbert, Peano, etc. The key challenge is to order the coordinates or regions according to the specified ordering, usually performed using an ordering function and sorting algorithm. Depending on the complexity of the ordering function---$\mathcal{O}(1)$ for Morton and $\mathcal{O}(\log(n))$ for Hilbert---the cost of ordering can be $\mathcal{O}(n\log(n))$ or $\mathcal{O}(n\log^2(n))$. This approaches is easily parallelized using efficient parallel sorting algorithms such as \ssort~\cite{samplesort}, leading to a parallel complexity of $N/p\log N/p + p\log^2 p$. This is the approach used by state-of-the-art packages \cite{campbell2003,devine2005,sundar2007}, such as Zoltan \cite{zoltan}. In this work, we present novel sequential and distributed ordering and partitioning algorithms for spatial data using Hilbert Curves.  

%Most PDE solvers struggle to achieve even 10\% of peak efficiency on modern supercomputers, largely due to the high cost of data movement.

%In this work we focus on improving standard Morton (or Z-order) space filling curve (SFC) based partitioning common in several Finite Element packages \cite{Burstedde10,BursteddeWilcoxGhattas11,BangerthBursteddeHeisterEtAl10b,BangerthBursteddeHeisterEtAl11,BangerthHartmannKanschat}.

\subsubsection*{Related Work} Load balancing and partitioning are critical when it comes to parallel
computations. Generally partitioning involves equally dividing the work and
data among the processors, reducing processor idle time and communication
costs. The standard approach is to model the problem using a
graph and partition the vertices of the graph into equal groups such
that the weight of the edges crossing across processes is minimized.
Graph partitioning is a NP-hard and most work focuses on heuristics to obtain
good approximations. Several graph partitioning packages exist \cite{chaco,metis,parmetis,scotch,jostle}
but performance and parallel scalability is challenging, especially for applications requiring repeated load-balancing, such as Adaptive Mesh Refinement (AMR).
In many such cases, SFC are used as a scalable and effective partitioning technique. 
One of the main advantage of SFC based partitioning is the preservation of
geometric locality of objects between processors. Depending on the SFC (i.e.
Morton, Moore, Hilbert) that used for partitioning, the amount of locality
preserved differs \cite{bader2012}.  
Most SFC based partitioning---especially for adaptive meshing---use the
Morton ordering that offers a good balance between the quality of partition and the 
efficiency of implementation.

There is a large literature of Space Filling Curve (SFC) based 
partitioning schemes. 
Several works have compared the clustering properties of space filling curves \cite{BonkiMoon,abel1990} and concluded the superiority of Hilbert curves over the Morton curve.
Gunther \emph{et~al}\cite{gunther2006} demonstrated that using SFCs to build hierarchical data structures such 
as octrees minimizes data access time. Algorithms have been proposed for computing the (inverse) mapping between one and $d$-dimensional spaces \cite{butz1971alternative, campbell2003, bader2012} including indexing schemes for for unequal dimension cardinalities \cite{Hamilton2008}, resulting in reduced communication costs. 
Other applications include multi-dimensional data reordering \cite{jin2005using}
and to speed up sparse matrix-vector multiplications \cite{yzelman2012} by improving cache-utilization.
Luitjens \emph{et~al.} \cite{luitjens2007parallel} analyze various parallel
sorting algorithms for ordering Hilbert curves for partitioning adaptively
refined meshes. However, they only consider comparison-based sorting algorithms and  
therefore $\mathcal{O}(N\log N)$ complexity algorithms. Several implementations of
SFC-based partitioning algorithms are also available including Zoltan \cite{zoltan}, Dendro \cite{sundar2007},
p4est \cite{p4est}. 
% Ocean Modeling \cite{dennis2007inverse}
A thorough review of SFCs and their applications can be found in \cite{bader2012}.

\subsubsection*{Contributions}

While SFCs have been used for partitioning data for a long time and several efficient implementations exist, our algorithms improve on the asymptotic complexity and demonstrate the scalability and efficiency experimentally. Our algorithms are based on well-known and well tested algorithmic techniques. \textit{Our main contribution is the design of optimal $\mathcal{O}(N)$ sequential and $\mathcal{O}(N/p + \log p)$ parallel SFC ordering algorithms with performance insensitive to the choice of SFC}. Our contributions in detail:  

\begin{itemize}
  \item \textbf{Method:} We present an algorithm that addresses the issue of efficient and scalable Hilbert ordering of spatial data, especially compared to the Morton ordering. Our main contribution is the following simple idea: Since the efficiency of Hilbert ordering compared to Morton is due to the overhead of computing rotations, let us factor it out and amortize the cost of computing rotations. This leads to a comparison-free ordering approach similar to the Radix sort \cite{knuth3} with added transformations between levels. We extend the algorithm to a scalable parallel variant and also develop efficient data-structures to enable $\mathcal{O}(1)$ traversal of compressed trees. Our distributed partitioning algorithm also allows users to minimize communication at the cost of a user-specified increase in load-imbalance.   
  \item \textbf{Experimental Evaluation:} We conduct experiments to compare the efficiency and scalability of our algorithm with state-of-the-art SFC-based partitioning codes Zoltan \cite{zoltan} and Dendro \cite{SampathSundarAdavaniEtAl08} on TACC's \Stampede ~and ORNL's \Titan ~upto 32,768 cores. While the theoretical clustering properties of Hilbert ordering are known, we experimentally demonstrate the reduction in communication related costs via three different metrics. We will also release our code on github. (currently withheld for review purposes.)  
\end{itemize}

%\begin{itemize}
%  \item top-down approach, avoids comparisons, like radix with the rotations between rearrangements
%  \item distributed version that allows control over load imbalance. contrast with samplesort and histogram sort.
%  \item Talk about minimizing communication costs for subsequent operations such as FEM calculations, even if there is slight load-imbalance.
%  \item Complexity as well as efficient implementations
%  \item Hilbert has better properties. Our work makes it comparable to Morton and faster than existing implementations, even of Morton. 
%  \item Also support $\mathcal{O}(1)$ traversal. important for subsequent operations using compressed representations.
%  \item By minimizing communications costs, number of procs data is exchanged with and flexible partitioning, we also demonstrate reduction in overall energy consumption. Important for future architectures.
%  \item Demonstrate excellent weak scalability on Titan. Also Strong. For both ordering as well as traversals.  
%\end{itemize}
% We demonstrate the efficacy of our algorithms using Morton and Hilbert Curves, for solving the Poisson equation using the Finite Element method on adaptively refined 3D Octree meshes. Also mention flexible partitioning and energy experiments. Abstract is likely limited, so give result highlights here. weak scaling plot on page 1?

%In a nutshell, our contributions are the following.
%\begin{itemize}
%  \item A new fast implementation of the Hilbert ordering
%  \item The first Hilbert-ordering based adaptive Finite Element code\footnote{While the Hilbert has been used in some cases for partitioning the data, we could not find cases where the traversals were performed in the Hilbert order.} (to the best of our knowledge)
%  \item Practical demonstration of the reduction in communication related costs, via three metrics, by using Hilbert-based partitioning, as opposed to Morton-based partitioning. 
%  \item Release of the fast Hilbert-based code for use with other codes. Code is available on github. (url withheld for review purposes.)   
%\end{itemize}

\subsubsection*{Limitations} 

One key disadvantage of SFC based partitioning schemes is, in environments where geometric locality is not proportional to communication cost, it is hard to encode communication patterns (or communication cost) in to SFC based partitioning schemes. 

% The main limitations of this work are the lack of scalability at larger levels of parallelism and the fact that the current Hilbert implementation is still slightly slower than the best Morton implementation. We are working on obtaining results using all nodes on Titan, as well as improving the efficiency of the Hilbert ordering.  

\subsubsection*{Organization of the paper}
The rest of the paper is organized as follows. In \S\ref{sec:bg}, we give a quick overview of Space filling curves. In \S\ref{sec:sort} we describe the new fast Hilbert ordering, as well as justifications for trading minor load-imbalance for reduced communication costs. In \S\ref{sec:getNext}, we 
discuss the modifications we had to make to the Octree meshing and Finite Element codes to support traversal based on the Hilbert ordering. In \S\ref{sec:results}, we present our experimental methodology and results demonstrating the superiority of the Hibert ordering. Finally, we conclude with directions for future work.