\begin{figure}
  \begin{tikzpicture}[scale=1.2, level distance=8mm,emph/.style={edge from parent/.style={red,->,shorten <=1pt,>=stealth',very thick,draw}},norm/.style={edge from parent/.style={black,->,shorten <=1pt,>=stealth',semithick,draw}}]
  \tikzstyle{edge from parent}=[black,->,shorten <=1pt,>=stealth',semithick,draw]
  \tikzstyle{level 1}=[sibling distance=8mm]
  \tikzstyle{level 2}=[sibling distance=6mm]
  \tikzstyle{level 3}=[sibling distance=4mm]
  
  \node[fill=red!30,rounded corners] {root}
  child[emph] { node[fill=blue!30,rounded corners] {nl}
    child[norm] { node[fill=green!30,rounded corners] {a}}
    child[emph] { node[fill=blue!30,rounded corners] {nl}
      child[norm] { node[fill=green!30,rounded corners] {b}}
      child[norm] { node[fill=green!30,rounded corners] {c}}
      child[emph] { node[draw=red,very thick,fill=green!30,rounded corners] {d}}
      child[norm] { node[fill=green!30,rounded corners] {e}}
    }
    child[norm] { node[fill=green!30,rounded corners] {f}}
    child[norm] { node[fill=green!30,rounded corners] {g}}
  }
  child { node[fill=green!30,rounded corners] {h}}
  child { node[fill=green!30,rounded corners] {i}}
  child { node[fill=blue!30,rounded corners] {nl}
    child { node[fill=green!30,rounded corners] {j}}
    child { node[fill=green!30,rounded corners] {k}}
    child { node[fill=green!30,rounded corners] {l}}
    child { node[fill=green!30,rounded corners] {m}}
  };
  
  %% Draw levels axis ...
  \draw[->] (-3,0) -- (-3,-3);		
  \draw[snake=ticks,segment length=0.95cm] (-3,0) -- (-3,-2.4);
  
  \draw (-2.8,0) node {$0$}
  (-2.8,-0.8) node {$1$}
  (-2.8,-1.6) node {$2$}
  (-2.8,-2.4) node {$3$};
  \draw (-3.3,-1.6) node [rotate=90] {$level$};		
  \draw (3,-2.9) node {$\mathcal{R}$};
  
  
  % stack 
  \begin{scope}[shift={(0.3,0)}]
  \draw (2.5,0.3) rectangle (3,-2.6);
  
  \draw (2.5,-0.45) -- (3,-0.45);  
  \draw[blue,very thick] (2.6,-0.2) -- (2.9,-0.2) -- (2.9,0.1) -- (2.6,0.1);
  
  \draw (2.5,-1.13) -- (3,-1.13);
  \draw[blue,very thick] (2.6,-0.65) -- (2.6,-0.95) -- (2.9,-0.95) -- (2.9,-0.65); 
  
  \draw (2.5,-1.9) -- (3,-1.9);
  \draw[blue,very thick] (2.9,-1.4) -- (2.6,-1.4) -- (2.6,-1.7) -- (2.9,-1.7);
  
  \draw[blue,very thick] (2.9,-2.45) -- (2.9,-2.15) -- (2.6,-2.15) -- (2.6,-2.45);
  \end{scope}  
  \end{tikzpicture}
  \caption{\label{fig:rStack} Illustration of the list of rotations being kept track of for a quadtree. Only the leaf nodes (\colorbox{green!30}{$a-m$}) are stored. During traversal, the rotation pattern of the current node (\fcolorbox{red}{green!30}{d}) as well as all its ancestors (path marked in red) is accessible from the rotation list, $\mathcal{R}$, indexed by the level of the current node or the ancestor. During traversal, $\mathcal{R}$ is updated only while moving to a deeper level, using the current node's rotation.}
\end{figure}

One of the most common applications of SFCs is for maintaining efficient data structures for octree-based adaptively refined meshes. Algorithms described in \S\ref{sec:sort} can be used to generate and maintain such data-structures. However, in order to perform operations such as Finite Element (FEM) calculations, we also need to develop efficient tree traversal methods. For example, we would need to traverse the elements of a FEM mesh in order to assemble the matrix, or evaluate a matrix-vector product (\mvec) for large-scale systems. Given the ever-increasing costs of data movement, it is also important to reduce the memory footprint associated with storing the mesh, or the Hilbert ordering. While efficient traversals for compressed adaptive meshes exist for the Morton ordering \cite{sundar2007,SampathAdavaniSundarEtAl08},  it does not extend directly to other SFCs such as the Hilbert curve. This is mainly due to the fact that unlike the Morton ordering, the  ordering of children within an octant is dependent on its level and the orderings of its ancestor. An easy solution is to store the rotation or ordering within each element, but this is not efficient as it would require $\mathcal{O}(n)$ additional storage. An alternative could be to re-compute the ordering for each octant as needed. While this is space efficient, this would require $\mathcal{O}(\log n)$ memory lookups for the fastest implementation. In this section, we propose a simple traversal algorithm for compressed Hilbert curves with $\mathcal{O}(1)$ amortized cost and storage.  

\subsection{Octree Meshing and Compression}
\label{sec-3-3}
One of the major problems with unstructured meshes is the
storage overhead. In the case of the octree, this amounts
to having to store both the octree and the lookup table. In
order to reduce the storage costs associated with the octree,
we compress the octree with minimal impact on the element traversal needed for finite element loops. 
The compression is essential for minimizing data movement.
The sorted, unique, linear octree can be easily compressed by
retaining only the 
%offset\footnote{The $(x,y,z)$ coordinates of the smallest corner.} 
anchor of the first element and the level of subsequent octants. Storing the offset for each octant requires a storage of three integers (12 bytes) and a byte for storing the level. Storing only the level represents a \texttt{12x} compression as opposed to storing the offset for every octant. While this is sufficient for the Morton encoding, for the Hilbert ordering the rotation patterns change across elements. In order to compute the next element, we need to know the rotation pattern of the current element or its ancestor(s) (this is a special case where the current element is the last child of its parent). As previously mentioned the tradeoff is between additional storage for the rotation or for additional computation. We propose to use a list, $\mathcal{R}$, of size $D_{max}$ to store the rotation patterns during traversal. The rotation for the current element as well as all its ancestors is available by indexing into $\mathcal{R}$ at the appropriate level. During traversal, we start at the \textit{root} level and keep updating $\mathcal{R}$ while visiting a new octant. This update is $\mathcal{O}(1)$ (amortized) and is performed using the parent's rotation patterns stored in $\mathcal{R}$. Note that the rotation for any element is only computed once during traversal using the parent's rotation pattern as opposed to starting from the root for each element. This makes it computationally as efficient as pre-computation while requiring only $D_{max}$ \texttt{byte}s of storage. For all the experiments reported in this paper, $D_{max}$ was $30$ and this is large enough for 3D meshes with potentially $2^{90}$ elements.

% Therefore, for the Hilbert ordering, we use one additional \texttt{byte} to store the rotation pattern of the element. 

\subsection{Finite Element Computations and MatVec}

Since our evaluation of the quality of the partition will be based on the evaluation of a \mvec~ with
the global finite element `stiffness' matrix, we will give a brief summary of the approach and the assumptions. Please refer to \cite{sundar2007,bader2012,p4est} for additional details on performing distributed finite element computations using octree meshes. The \mvec~ refers to a function that takes a vector and returns another vector, the result of applying the discretized PDE operator to the the input vector.
As the building block for most large-scale PDE solvers, the cost, efficiency and scalability of the \mvec~ operation will determine the efficiency and scalability of the overall system. 
The \mvec~ can be evaluated either by looping through the elements or by looping through the nodes. Due to the simplicity of the elemental loop, we only evaluate elemental loops in this work. We first loop over the elements in the interior of the processor domain since these elements do not share any nodes with the neighboring processors. While we perform this computation we communicate the values of the ghost nodes in the background. At the end of the first loop, we use the ghost values from other processors and loop over the remaining elements. These ghosts have to be exchanged after (or before) each \mvec ~and therefore characterizes the communication structure of finite element computations. It is important to note that the advantages of the Hilbert ordering are more pronounced in cases where the \mvec ~computation is not overlapped with the communication. But as this is fairly standard practice for large-scale codes, we have evaluated the ordering with overlapped computations and communications. 