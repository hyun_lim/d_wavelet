When we move towards the Exa-scale computing, one of the main challenges would be high data movement cost due to deep memory hierarchies. Most of the present Adaptive Mesh Refinement(AMR) implementations such
as Dendro \cite{SampathAdavaniSundarEtAl08}, p4est \cite{p4est} are written for early architectures where data movement cost would not be significant compared to Exa-scale architectures. Hence most of the early implementations of SFC based AMRs, use binary searches in order to enforce $2:1$ balancing constraint and generate mesh information. Use of binary searches (comparison operators) cause random memory accesses over the sorted array of octants, which leads to  lower cache performances. This would be a major bottleneck to achieve good performance in AMR based computations in exa-scale architectures.

%As we mentioned, low cache performance is not significant in current and previous architectures, but when we move towards exa-scale architectures low cache performance would be a major bottleneck to achieve good performance. 

In this section we describe how we can extend sequential and distributed implementation of \tsort~ to perform octree construction, balancing and mesh generation. Compared to traditional SFC based AMR algorithms \tsort~ does not use any binary searches (comparison operators), instead of searches we can perform fixed number of passes (i.e. for construction we need one pass while balancing and mesh generation we need two passes over the input octants.) over the input data for octree construction, $2:1$ balancing and mesh generation, which will minimize the random memory accesses and cache misses. 

%The idea of extending \tsort~ for octree construction, $2:1$ balancing and mesh generation, 


\subsection{Octree construction using \tsort~}

\begin{definition} \label{def::oct_construction}
    Given a set of points $\mathcal{X}$ on domain $\Omega$ and positive integer $K_{oct}$, we need to generate a hierarchical decomposition of domain $\Omega$ (by recursively splitting each dimension) such that number of points that each cell at leaf level contains at most $K_{oct}$ points. 
\end{definition}

In the sequential version of the \tsort~ (see algorithm \ref{alg:tsort}) we recurse  if the considering bucket (or region) has more than one point (i.e. in this case, $K_{oct}=1$). We can build the sequential version of the octree construction based on above approach by adding the corresponding octant (with the correct level) when number of points in the bucket is less than specified $K_{oct}$. This will generate all the leaf level nodes of complete linear octree for a given set of points $\mathcal{X}$.

Distributed memory version of the octree construction can be achieved by, replacing sequential \tsort~ in \dsort~ (see algorithm \ref{alg:dsort}) with the sequential octree construction (see algorithm \ref{alg:tsort_cons}). The key idea is that we use \dsort~ to partition input points with user specified tolerance and use \tsortcons~ to generate complete octree for each process. This will generate some duplicate octants (globally) due to the fact that we complete each octree locally. To remove duplicates and re\-partition the generated local octrees, we can call \dsort~ for the second time. Hence we can generate linear, globally sorted, \& complete octree using two passes of \dsort~ algorithm with minor modification to the \tsort~ (i.e. \tsortcons)

\begin{algorithm}[t]
  \caption{\tsortcons~: \tsort~ based octree construction (sequential)}\label{alg:tsort_cons}
  \footnotesize
  \begin{algorithmic}[1]
      \Require A list of points or regions $W$, the starting level $l_1$ and the ending level $l_2$, $K_{oct}$
    \Ensure $t_{cons}$ \- linear sorted complete octree (locally) based on $W$
    %\Function{TreeSort($W$, $l_1$, $l_2$)}{}
    \State $counts \leftarrow 0$
    \State $t_{bal} \leftarrow null $
    \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    % Bucket($W$, $l$) 
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor 
    \If{$l_1 > l_2$} 
    \For{$i:=1:2^{dim}$}
      \If{$|W_i|\geq K_{oct}$}
        \State \tsort ($W_i, l_1-1, l_2$) \Comment local sort
      \Else
        \State $t_{bal}.push(oct_i)$
      \EndIf  
    \EndFor 
    \EndIf
    %\EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{Octree balancing using \tsort~}
In many applications involving AMR, it is desirable to impose a restriction on the relative sizes of adjacent octants \textbf{!ref}. In this context we are focusing on enforcing $2:1$ balance constraint which can be derived from definition \ref{def::bal_constraint}.

\begin{definition}{\textbf{$K$ balanced octrees:}}\label{def::bal_constraint}
\textit{A linear $d-tree$ is $k-balanced$ if and only if, for any $l\in [1,\mathcal{L})$, no lead at level $l$ shares an $m-dimensional$ face ($m\in [k,d)$) with another leaf at level greater than $l+1$.}
\end{definition}

As a result of imposing $2:1$ balanced constraint no octant can be more than twice as coarser or finer as its adjacent octants. From now on by balancing, we mean enforcing $2:1$ balance constraint, unless stated otherwise. In this section, we will describe the \tsort~ based octree balancing, without using any binary searches (or comparison operator) on a constructed octree. For simplification purposes, we assume that the input to the balancing algorithm (see algorithm \ref{alg:tsort_balance}) is linear, globally sorted \& complete octree (Note that octree with above-specified properties can be generated via, algorithm \ref{alg:tsort_cons}).
    
The main motivation behind \tsort~ based octree balancing is that, what if we can figure out the set of new octants ($T_{aux}$) that need to be added to the constructed linear complete octree such that after the second pass of construction will result in a balanced, linear, globally sorted, \& complete octree.

\begin{theorem}[\textbf{Local balancing:}\label{thm:aux_bal_octants}]
   \textit{ Let $e \in T_{cons}$ , $P(e)$ denotes the parent of $e$ and $N(e)$ denotes all the neighbor of the octant $e$. Then the octree $T$ result in by construction of set of octants $U$, where $U={T_{cons} \bigcup \big\{\bigcup_{e\in T_{cons}} N(P(e))\big\}}$ satisfies the $2:1$ balanced constraint.  }
\end{theorem}

\textit{Proof: } \textbf{Proof goes here. }

According to the theorem \ref{thm:aux_bal_octants}, $e \in \T_{cons}$ is locally balanced after adding $N(P(e))$, the newly added octants (which are $N(P(e))$) might violate the balance constraint. Hence we need to add another layer of new octants to balance newly added octants and this goes on until we reach the root level. This effect known as the \textit{ripple effect}. Ripple effect causes high number of duplicate octants creation, which can be handled by maintaining an \textit{ordered set} (in our implementation we used \textit{std::set} ) with a simple lexicographical ordering of octants.

\textbf{\textcolor{red}{A figure need to explain the auxiliary balance octants.}}

\begin{algorithm}[t]
    \caption{CreateAuxOctants: creation of auxiliary octants for balancing (sequential)}\label{alg::bottom_up}
  \footnotesize
  \begin{algorithmic}[1]
    \Require $t_{cons}$ linear, sorted, completed octree
    \Ensure $t_{aux}$ \- unique set of auxiliary octants needed balance $t_{cons}$
	\State $t_{aux} \leftarrow t_{cons}$	
    \For  $~e$ in $t_{cons}$
      \State $t_{aux}.add\_unique(N(P(e)))$
    \EndFor
  \end{algorithmic}
\end{algorithm}

\begin{algorithm}[t]
  \caption{\tsortbal (sequential)}\label{alg:tsort_balance}
  \footnotesize
  \begin{algorithmic}[1]
    \Require linear, globally sorted, complete octree $T_{cons}$ ,starting level $l_1$ and the ending level $l_2$, $K_{oct}$
    \Ensure $t_{bal}$ \- linear sorted complete 2:1 balanced octree (locally)
    \State $t_{aux} \leftarrow CreateAuxOctants(t_{cons}) $
	\State $t_{bal} \leftarrow \tsortcons (t_{aux},l_1,l_2,K_{oct})$
    %\EndFunction
  \end{algorithmic}
\end{algorithm}

As we mentioned in previous sections we can make minor modifications to the \tsort algorithm to perform octree construction and balancing.  Hence we can use the key idea in the \tsort as the base and derive a single algorithm \dtsortmodified~ (see algorithm \ref{alg:dsort_modified} ) to perform distributed octree sorting, construction, and balancing. To switch between octree sorting, construction, and balancing we can use a \texttt{opt} flag. Note that in \dtsortmodified~ do not require comparison of octants, instead of that we perform fixed number of passes (two passes in octree construction \& three passes in octree balancing) over the input octants. 

\begin{algorithm}[t]
  \caption{\dtsortmodified}\label{alg:dsort_modified}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A distributed list of points or regions $W_r$, $\mathit{comm}$, $p$ (w.l.g., assume $p= mk$),
    $p_r$ of current task in $\mathit{comm}$, $tol$ load flexibility, option flag $opt$
    \Ensure globally sorted array $W$, globally sorted, complete, linear octree $T_{cons}$, globally sorted, complete, balanced linear octree $T_{bal}$
    %\State $split\_count \leftarrow 1$
    \State $counts\_local \leftarrow [\ ], counts\_global \leftarrow [\ ]$    
    \State $s \leftarrow \tsort(W_r,l-\log(p),l)$ \Comment initial splitter computation
    \While {$|s_r-\frac{rn}{p}|>tol$}     
    \State $counts \leftarrow 0$ \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    \State $counts\_local \leftarrow push(counts)$  
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor
    %\State $split\_count \leftarrow 2^{dim}\times split\_count$
    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,comm)$
    \State $s \leftarrow select(s,counts\_global)$
    \EndWhile
    %	  \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    % \Comment optimal splitter computation (with in the given tolerance)	  
    \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    \Comment Staged All2all
    \\
    \If{ $opt \& TS\_SORT$}
	    \State $W \leftarrow$\tsort$(W_r, 0, l)$ \Comment local sort
	\ElsIf {$opt \& TS\_CONS$}
		\State $t_{cons}\leftarrow$ \tsortcons$(W_r, 0, l)$ \Comment local construction
		\State $T_{cons} \leftarrow$ \dtsortmodified$(t_{cons},0,l,TS\_SORT)$ \Comment sort globally and remove duplicates
	\ElsIf {$opt \& TS\_BAL$ }
		\State $t_{bal}\leftarrow$ \tsortbal$(W_r, 0, l)$ \Comment local balancing
        \State $T_{bal}\leftarrow$ \dtsortmodified$(t_{bal},0,l,TS\_SORT)$ \Comment sort globally and remove duplicates
    \EndIf
  \end{algorithmic}
\end{algorithm}

\subsection{Octree mesh generation using \tsort~}
Adaptive octrees are widely used in computational sciences, for discretization of 3D geometric domains, and perform computations such as Finite Differencing and Finite Element, to solve Partial Differential Equations (PDEs). Both in, finite differences and finite element methods, the main computation is to apply a given stencil or construct local element matrices, which require a knowledge of neighbor information for a given octant. In this paper, mesh generation or meshing refers to the construction of $E2E$ mapping defined in the definition \ref{def::meshing}.

\begin{definition}[\textbf{Meshing}:]\label{def::meshing}
\textit{Given a globally sorted, complete, balanced linear octree, $T$ and let $I$ be the corresponding index family for $T$ we want to construct a mapping $E2E: I \rightarrow I^{8}$ where for a given index of an element $e \in T$ we get a vector of indices which points to the neighbor elements of current element $e$. The mapping $E2E$ is the element to elements mapping for the given octree $T$.}
\end{definition}

Generating search keys based on the neighbor information needed, and performing binary searches in a given octree is a common way of generating the E2E mapping. One of the main drawbacks of the above approach is, for each key generated, we need to perform a binary search (assuming the given octree is sorted)  which requires random accesses to the given linear octree, resulting in lower cache performances. In this paper, we suggest that with a minor modification to the \tsort~ algorithm, we can perform multiple key searches in a streaming manner without using any partial ordering operator. The modified \tsort~ algorithm for key searching is named as \tsearch~ (see algorithm \ref{alg:tsearch}). 

\begin{algorithm}[t]
  \caption{\tbucket~(Sequential)}\label{alg:bucket}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A list of points or regions $W$, current level $l$
    \Ensure $counts$ list of bucket counts, $W$ is reordered to be sorted at level l
    \State $counts [] \leftarrow 0$
    \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor 
  \end{algorithmic}
\end{algorithm}


\begin{algorithm}[t]
  \caption{\tsearch~ (Sequential)}\label{alg:tsearch}
  \footnotesize
  \begin{algorithmic}[1]
    \Require set of keys $K$, sorted octree $T$ the starting level $l_1$ and the ending level $l_2$
    \Ensure list of octant indices $I$ for given set of keys $K$ 
    \State $c\_keys[], keys [] \leftarrow \tbucket(K,l_1)$
    \State $c\_nodes[], nodes[]  \leftarrow \tbucket(T,l_1)$
    \Comment $|counts| = 2^{dim}$, 8 for 3D
    \State $nodeCount \leftarrow$ Scan$(c\_nodes)$
    \If {$nodeCount > 1$ and $l_1 \leq l_2$ }
		\For{$i < |c\_nodes|$}
    		\State \tsearch$(keys[i],nodes[i],l_1+1,l_2)$
    	\EndFor
    \Else
     	\If {$nodeCount=1$}
     	\State $\forall k \in K$,  $k.result \leftarrow Index(T[0])$
     	\EndIf
    \EndIf
    %\EndFunction
  \end{algorithmic}
\end{algorithm}






