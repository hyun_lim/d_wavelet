In this section we describe our sequential and parallel algorithms for fast ordering of Space Filling Curves. We will focus on the Hilbert Curve, although our approach is applicable to all SFCs. Unlike existing approaches based on a ordering operator and comparison-based sorting we propose a comparison-free algorithm for SFC ordering. The algorithm \tsort~ is similar to a top-down tree construction algorithm, bucketing the coordinates or regions according to the rules of the SFC at each level. This makes the ordering algorithm computationally insensitive to the specific ordering chosen. For example, existing approaches such as Zoltan \cite{zoltan} are significantly more expensive for Hilbert ordering as compared to Morton ordering. Our algorithm in contrast has the same cost of ordering for Hilbert as well as Morton. More importantly, due to the algorithm permitting efficient implementations, our ordering is faster than even the best Morton and distributed sorting implementations \cite{sundar2007,hyksort}. The structure of our parallel ordering and partitioning algorithm also enables us to easily specify the desired load-balance and more importantly achieve lower communication costs by specifying a higher tolerance for load-(im)balance. We first describe the sequential algorithm, followed by the distributed-memory variant and justification for the trade-off between lower communication costs and increased load imbalance. 


%In this section we present an algorithm based on tree structure (\textit{Tree Sort}) which can be used to compute ordering of SFCs efficiently compared to standard operator based sort methods. Even though we have implemented the tree sort algorithm for well known Morton (Z-curve) and Hilbert curve it can be easily extended to compute ordering of any SFC which obeys the \textit{vertex continuity} property.  In the following \ref{sec:sort:sequential} and \ref{sec:sort:parallel} sections we present detailed version of tree sort algorithm for Hilbert curve ordering. Morton ordering implementation can be achieved by minor changes (i.e. using the same rotation pattern instead of varying rotation patterns with depth) to the Hilbert ordering implementation. Section \ref{sec:sort:sequential} describes the purely sequential version of the tree sort implementation for Hilbert ordering and section \ref{sec:sort:parallel} provides parallelized version of the same algorithm.
%
%\par Even though Hilbert curve is more localized than the traditional Morton curve, computation of Hilbert ordering is not trivial as Morton ordering. This is mainly due to the fact that, varying rotation patterns associated with Hilbert curve while Morton curve is associated with a single fixed rotation pattern. In section \ref{sec:sort:rotation_table} we describe the implementation details of the \textit{Hilbert Rotation Table} which is used to speed up the rotation pattern computation. 

%\subsection{Fixed Size Hilbert Rotation Table}
%\label{sec:sort:rotation_table}
%\note{I am not sure this subsection is needed.}
%In order to implement tree sort, we need to compute the rotations at varying depths of the curve. A further speed up can be obtained via the use of look-up tables for the rotation patterns, wherein we store a small table with the relationship between the current octant's rotation and the resulting rotations of its eight children. \\
%\noindent\textit{\textbf{Lemma 1}}
%The three dimensional Hilbert curve has 24 different rotation patterns.\\
%\textit{\textbf{Proof:}}
%3-D Hilbert curve is associated with an unit cube domain. If we consider a particular rotation pattern in Hilbert curve, it has the property that its entrance (point where the curve enters the cube) and exit (point where the curve leaves the cube) lies on the same edge of the cube. Hence if we consider an particular face, it has 4 possible combinations for entering and exiting points. Since a cube has 6 faces, total number of possible Hilbert rotation patterns are 6 times 4 which gives us 24 rotation pattens. \hfill$\square$
%
%\par The rotation pattern within an octant depends only on its parent's rotation and its position inside the parent (i.e., its child number). Thus as previously mentioned, there are 8 possible transformations. Hence we can store all the rotation permutations that we need to compute Hilbert ordering in a fixed sized table which contains $24\times8$ entries. Please note that, Hilbert table size is independent of the input points and the depth of the curve that we consider.  By using this table, we can compute the rotation pattern of a sub octant using a single look-up (assuming that we have its parents rotation pattern) instead of performing multiple swaps and permutations.   

\begin{algorithm}[t]
  \caption{\tsort~(Sequential)}\label{alg:tsort}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A list of points or regions $W$, the starting level $l_1$ and the ending level $l_2$
    \Ensure $W$ is reordered according to the SFC.
    %\Function{TreeSort($W$, $l_1$, $l_2$)}{}
    \State $counts [] \leftarrow 0$
    \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    % Bucket($W$, $l$) 
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor 
    \If{$l_1 > l_2$} 
    \For{$i:=1:2^{dim}$}
    \State \tsort ($W_i, l_1-1, l_2$)
    \Comment local sort
    \EndFor 
    \EndIf
    %\EndFunction
  \end{algorithmic}
\end{algorithm}

\subsection{Sequential TreeSort}
\label{sec:tsort}

%Main drawback in operator based sorting techniques is redundant computation of rotation patterns. For example let $oct_1$ and $oct_2$ be two spatial points (octants) which needed to be compared in the sorting procedure, we need $d$ rotation table look ups (which might contain the same look up which is needed for ordering another two octants.) where $d$ is the level of \textit{Nearest Common Ancestor (NCA)} of $oct_1$ and $oct_2$. In order to avoid redundant table look ups, the tree sort algorithm is developed. The main idea in the tree sort algorithm is to put the given octants to correct sub regions (we call them \textit{buckets}, Note that in 3D Hilbert curve each octant has 8 sub octants) at the same time traversing the linear octree in, depth first order, starting from the root octant. For example we start the algorithm by traversing the root node, where we iterate through all the octants and put them to the correct bucket which will make the given points sorted based on order 1 Hilbert curve. We perform the same operation to other octants in depth first order, until no further bucketing needed. Note that since we are traversing the linear octree, in the depth first order, we only need one table look up to get the rotation pattern of one level deeper, because we have the rotation pattern of the parent node.  Pseudo code for the tree sort algorithm is given in algorithm \ref{seq:tree_sort}. 

The Morton ordering and the resulting partition is popular in HPC  due to the efficiency of computing the ordering. The Morton encoding for any region is generated by interleaving the binary representations ($D_{max}$ bits each) of the three coordinates of the region's anchor, and then appending the binary representation ($(\lfloor(\log_2{D_{max}})\rfloor+ 1)$ bits) of the region's size to this sequence of bits \cite{bern99,tropf81}. In practice it is usually easier to define the comparison operator using the multicomponent representation of the region, i.e. using the $x,y,$ and $z$ components along with the level. Scalable distributed sorting algorithms \cite{solomonik10,hyksort} can then be used to efficiently order and partition data. The Hilbert ordering has significantly better clustering properties compared to the Morton, but the ordering is not fixed at all levels. The Hilbert ordering within an octant depends on the rotations and child numbers of all of its ancestors, requiring us to track rotations across levels. While the efficiency of the Hilbert comparison can be improved using look-up tables, this still involves multiple memory lookups per comparison.This makes the Hilbert ordering unattractive for performance critical applications like AMR. 

\begin{algorithm}[t]
  \caption{\dsort}\label{alg:dsort}
  \footnotesize
  \begin{algorithmic}[1]
    \Require A distributed list of points or regions $W_r$, $\mathit{comm}$, $p$ (w.l.g., assume $p= mk$),
    $p_r$ of current task in $\mathit{comm}$, $tol$ load flexibility, 
    \Ensure globally sorted array $W$
    %\State $split\_count \leftarrow 1$
    \State $counts\_local \leftarrow [\ ], counts\_global \leftarrow [\ ]$    
    \State $s \leftarrow \tsort(W_r,l-\log(p),l)$ \Comment initial splitter computation
    
    \While {$|s_r-\frac{rn}{p}|>tol$}     
    \State $counts \leftarrow 0$ \Comment $|counts| = 2^{dim}$, 8 for 3D
    \For{$w \in W$}
    \State increment $counts[child\_num(w)]$
    \EndFor
    \State $counts\_local \leftarrow push(counts)$  
    \State $counts \leftarrow R_h(counts)$ 
    \Comment Permute counts using SFC ordering
    \State offsets $\leftarrow scan(counts)$
    \For{$w \in W$}
    \State $i\leftarrow child\_num(w)$
    \State append $w$ to $W_i$ at offsets$[i]$
    \State increment offset$[i]$
    \EndFor
    %\State $split\_count \leftarrow 2^{dim}\times split\_count$

    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,comm)$
    \State $s \leftarrow select(s,counts\_global)$
    \EndWhile
    %	  \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    % \Comment optimal splitter computation (with in the given tolerance)	  
    \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
    \Comment Staged All2all
    %	  \While{$p > 1$}		\Comment Iters: $\mathcal{O}(\log p / \log k)$
    %      \State $N \leftarrow \mathtt{MPI\_AllReduce}(|B|, \mathit{comm})$
    %      \State $s \leftarrow \mathtt{ParallelSelect}\left(B, \left\{i \, N/k~|~i=1,...,k-1\right\} \right)$
    %      \State $d_{i+1} \leftarrow \mathtt{Rank}(s_i, B) ,~~ \forall i$ 				%\Comment $\bigO(p \log n)$
    %      \State $\left[d_0,~d_k\right] \leftarrow \left[ 0,~n \right]$
    %      \State $color \leftarrow \lfloor k \, p_r/p \rfloor $
    %      \ForAll{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $R_i \leftarrow \mathtt{MPI\_Irecv}(p_{recv}, \mathit{comm})$
    %      \EndFor
    %      \For{$i \in {0,...,k-1}$}
    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
    %      \State $p_{send} \leftarrow m \, ((color+i)\bmod k) + (p_r \bmod m)$
    %      \State $\mathtt{MPI\_Issend} ( B[d_i,...,d_{i+1}-1] , p_{send}, \mathit{comm})$
    %      \State $j \leftarrow 2$
    %      \While {$ i>0 ~ and ~ i \bmod j = 0$}
    %      \State $R_{i-j} \leftarrow \mathtt{merge}(R_{i-j},R_{i-j/2})$
    %      \State $j \leftarrow 2 j$
    %      \EndWhile
    %      \State $\mathtt{MPI\_WaitRecv}(p_{recv})$
    %      \EndFor
    %      \State $\mathtt{MPI\_WaitAll}()$
    %      \State $B \leftarrow \mathtt{merge}(R_{0},R_{k/2})$
    %      \State $\mathit{comm} \leftarrow \mathtt{MPI\_Comm\_split}(color, \mathit{comm})$
    %      \State $p_r \leftarrow \mathtt{MPI\_Comm\_rank}(\mathit{comm})$
    %      \EndWhile
    \State \tsort$(W_r, 0, l)$ \Comment local sort
  \end{algorithmic}
\end{algorithm}


Our main intuition is to factor out the Hilbert rotations, amortizing the cost of computing the rotations and  leading to a comparison-free algorithm.
Given that we are interested in generating a space-traversal, more so of application specific coordinates or regions, it is efficient to consider this problem as one of generating a quadtree or an octree, in 2D and 3D respectively. Specifically, we construct the tree in a \textit{top-down} fashion, one level at a time, arranging the octants based on the recurrence rules for the specific SFC, say Hilbert. We call this algorithm \tsort ~(Algorithm~\ref{alg:tsort}).  
 There are two advantages to this approach. Firstly, if the SFC can dictate the ordering based on the level, as in the case of Hilbert, then these are applied to the ordering at this level with an $\mathcal{O}(1)$ cost. Specifically, the cost of applying the transformation is $\Theta(2^{dim})$. Secondly, the ordering of the octants into one of $2^{dim}$ buckets can be determined rather cheaply using only the $l^{th}$ bit of the coordinates, where $l$ is the current level during tree generation. At this point, the algorithm should look suspiciously similar to the Radix sort \cite{knuth3}. Indeed, the algorithm is similar to the Most Significant Digit (MSD) Radix sort, with an additional transformation at every level and it is surprising that this is not the preferred method of generating spatial trees or ordering SFCs. Another advantage of this approach is that it traverses the tree in a depth-first fashion leading to good locality and cache utilization. It is important to note that while simple SFCs like Morton can be generated using the least significant digit (LSD) Radix sort, other SFCs involving transformations like the Hilbert Curve, are not amenable to efficient generation using LSD Radix. 
 
 \subsubsection*{Complexity:} The complexity of \tsort~ is $\mathcal{O}(n)$ with a constant factor of $D_{max}$. A comparison-sorting based SFC ordering approach will have a complexity of $\mathcal{O}(n\log n)$, with an additional constant of $D_{max}$ for Hilbert ordering. While it can be argued that for Morton curves, the $\log n$ term is comparable to the constant $D_{max}$, note that for comparison-sorting, the expected depth is $\log_2 n$, whereas for \tsort~ it is $\min(\log_8 n, D_{max})$. On \Stampede, ordering $10^8$ octants according to the Hilbert Curve using \tsort~ is \textsc{11.3x} faster compared to using \texttt{std::sort} along with the most efficient Hilbert comparison. Even for the equivalent Morton ordering, \tsort ~is \textsc{2.2x} faster. 

%\begin{itemize}
%  \item Morton comparison is efficient. Can be implemented without creating interleaved key.
%  \item Hilbert comparison, or rotated SFCs in general are expensive, requiring us to track rotations across levels. Usually made efficient using lookup tables for rotations, still require multiple memory lookups per comparison, leading to inefficient implementations. 
%  \item Our main intuition is to factor out the hilbert rotations leading to a comparison-free algorithm
%  \item Similar in structure to a MSD Radix sort.
%  \item LSD Radix can only support simple SFCs like Morton. 
%  \item Similar to Octree construction, starting from the root, keep dividing as long as octants have more than 1 point. Main difference is that the ordering of the children at each level is defined based on the recurrence rules for the specific SFC, say Hilbert.
%  \item Cost per point per level is trivially simple to compute, even easier than efficient Morton computations. Easy to implement on other architectures, such as GPUs, for example using Thrust. 
%  \item Cost of rotation lookup/computation is amortized.
%  \item Highlight single-core results.
%\end{itemize}


\subsection{Distributed Memory TreeSort}
\label{sec:dsort}

%\begin{algorithm}[t]
%  \caption{\dsort}\label{alg:dsort}
%  \footnotesize
%  \begin{algorithmic}[1]
%    \Require A distributed list of points or regions $W_r$, $\mathit{comm}$, $p$ (w.l.g., assume $p= mk$),
%    $p_r$ of current task in $\mathit{comm}$, $tol$ load flexibility, 
%    \Ensure globally sorted array $W$
%    \State $split\_count \leftarrow 1$
%    \State $counts\_local \leftarrow [\ ], counts\_global \leftarrow [\ ]$    
%    \State $splitters[p-1] \leftarrow NULL$
%    \Comment initial splitter computation
%    \While {$split\_count \geq  p$}     
%    \State $counts \leftarrow 0$ \Comment $|counts| = 2^{dim}$, 8 for 3D
%    \For{$w \in W$}
%    \State increment $counts[child\_num(w)]$
%    \EndFor
%    \State $counts\_local \leftarrow push(counts)$  
%    \State $counts \leftarrow R_h(counts)$ 
%    \Comment Permute counts using SFC ordering
%    \State offsets $\leftarrow scan(counts)$
%    \For{$w \in W$}
%    \State $i\leftarrow child\_num(w)$
%    \State append $w$ to $W_i$ at offsets$[i]$
%    \State increment offset$[i]$
%    \EndFor
%    \State $split\_count \leftarrow 2^{dim}\times split\_count$
%    \EndWhile
%    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,comm)$
%    % \Comment optimal splitter computation (with in the given tolerance)	  
%    \While{$|splitters-optimal|>tol$}
%    \State Shift(splitters)  \Comment left or right
%    \State $\mathtt{MPI\_ReduceAll}(counts\_local,counts\_global,comm)$
%    \EndWhile
%    \State $\mathtt{MPI\_AlltoAllv}$(A,splitters,comm)
%    %	  \While{$p > 1$}		\Comment Iters: $\mathcal{O}(\log p / \log k)$
%    %      \State $N \leftarrow \mathtt{MPI\_AllReduce}(|B|, \mathit{comm})$
%    %      \State $s \leftarrow \mathtt{ParallelSelect}\left(B, \left\{i \, N/k~|~i=1,...,k-1\right\} \right)$
%    %      \State $d_{i+1} \leftarrow \mathtt{Rank}(s_i, B) ,~~ \forall i$ 				%\Comment $\bigO(p \log n)$
%    %      \State $\left[d_0,~d_k\right] \leftarrow \left[ 0,~n \right]$
%    %      \State $color \leftarrow \lfloor k \, p_r/p \rfloor $
%    %      \ForAll{$i \in {0,...,k-1}$}
%    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
%    %      \State $R_i \leftarrow \mathtt{MPI\_Irecv}(p_{recv}, \mathit{comm})$
%    %      \EndFor
%    %      \For{$i \in {0,...,k-1}$}
%    %      \State $p_{recv} \leftarrow m \, ((color-i)\bmod k) + (p_r \bmod m)$
%    %      \State $p_{send} \leftarrow m \, ((color+i)\bmod k) + (p_r \bmod m)$
%    %      \State $\mathtt{MPI\_Issend} ( B[d_i,...,d_{i+1}-1] , p_{send}, \mathit{comm})$
%    %      \State $j \leftarrow 2$
%    %      \While {$ i>0 ~ and ~ i \bmod j = 0$}
%    %      \State $R_{i-j} \leftarrow \mathtt{merge}(R_{i-j},R_{i-j/2})$
%    %      \State $j \leftarrow 2 j$
%    %      \EndWhile
%    %      \State $\mathtt{MPI\_WaitRecv}(p_{recv})$
%    %      \EndFor
%    %      \State $\mathtt{MPI\_WaitAll}()$
%    %      \State $B \leftarrow \mathtt{merge}(R_{0},R_{k/2})$
%    %      \State $\mathit{comm} \leftarrow \mathtt{MPI\_Comm\_split}(color, \mathit{comm})$
%    %      \State $p_r \leftarrow \mathtt{MPI\_Comm\_rank}(\mathit{comm})$
%    %      \EndWhile
%    \State \tsort$(A, l, 0)$ \Comment local sort
%  \end{algorithmic}
%\end{algorithm}

While it would be straightforward to use \tsort ~instead of the local sort in distributed sorting algorithms like \ssort ~\cite{samplesort} to achieve overall speedup, we propose a distributed variant of \tsort ~that enables fine control over the load-balance (unlike samplesort) and also enables reduction is communication costs in exchange for higher load-imbalance. The distributed algorithm proceeds as the sequential variant (\S\ref{sec:tsort}), bucketing and partially sorting the data. Unlike the sequential \tsort, we have to traverse the tree in a breadth first fashion, as the data needs to be distributed across processors. 
Note that at each level, we split each octant $8$ times (for $3$D), so in $\log_8 p$ steps we will have $p$ buckets. A reduction provides us with the global ranks of these $p$ buckets. Using the optimal ranks ($p_rn/p \pm tol$) at which the data needs to be partitioned, we selectively partition the buckets to obtain the correct partitioning of the local data. This is in principle similar to the approach used by algorithms such as histogramsort \cite{solomonik10} and hyksort\cite{hyksort}, except that no comparisons are needed for computing the ranks. The computational cost is for the $\mathcal{O}(N/p)$ bucketing required for each of the $\log_8 p$ levels. We also need a $p$ reductions and an all-to-all data exchange. Therefore the expected running time for the distributed algorithm is,
\[
T_p = t_c\frac{N}{p} + (t_s + t_wp) \log p + t_w \frac{N}{p}.
\]  
In our evaluation, we considered trees of depth $30$ (so that the coordinates can be represented using \texttt{unsigned int}). Note that upto $8^6 = 262,144$ buckets can be determined using six levels, so the cost of determining splitters to distribute the data across processors is significantly lower than other approaches. An equivalent analysis of complexities for popular distributed sorting algorithms can be found in \cite{hyksort}.    

\begin{figure}%
  \centering
  \subfloat[{$\scriptstyle l=1,~\lambda=2,~s=16$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.05,0) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(2,1); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=2,~\lambda=1.2,~s=24$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0) rectangle +(0.5,0.525);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(1.025,0.5); 
    \end{scope}   
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=3,~\lambda=1.05,~s=28$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.525,0.25); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \subfloat[{$\scriptstyle l=4,~\lambda=1.01,~s=30$}] {
    \begin{tikzpicture}
    \begin{scope}[shift={(-0.03125,-0.03125)}]
    \draw[blue!30,fill=blue!20,ultra thin] (0,0) rectangle +(1,1); 
    \draw[blue!30,fill=blue!20,ultra thin] (1.0,0) rectangle +(0.5,0.5);
    \draw[blue!30,fill=blue!20,ultra thin] (1.5,0) rectangle +(0.2625,0.25);
    \draw[blue!30,fill=blue!20,ultra thin] (1.7625,0) rectangle +(0.125,0.125);
    
    \draw[green!30,fill=green!20,ultra thin] (1.025,0.525) rectangle +(1,1); 
    \draw[green!30,fill=green!20,ultra thin] (1.525,0.275) rectangle +(0.25,0.25);
    \draw[green!30,fill=green!20,ultra thin] (1.775,0.1325) rectangle +(0.25,0.525);
    \draw[green!30,fill=green!20,ultra thin] (1.9,0) rectangle +(0.125,0.1325);
    \draw[green!30,fill=green!20,ultra thin] (1.525,1.5) rectangle +(0.5,0.275);
    \draw[green!30,fill=green!20,ultra thin] (1.775,1.775) rectangle +(0.25,0.125);
    
    \draw[orange!30,fill=orange!50,ultra thin] (0,1.05) rectangle +(1,1); 
    \draw[orange!30,fill=orange!50,ultra thin] (1,1.55) rectangle +(0.5,0.5);
    \draw[orange!30,fill=orange!50,ultra thin] (1.5,1.8) rectangle +(0.25,0.25); 
    \draw[orange!30,fill=orange!50,ultra thin] (1.75,1.925) rectangle +(0.275,0.125); 
    \end{scope}
    \tikz[scale=1.3, thin] \hilbert((0mm,0mm),4);
    \end{tikzpicture}
  }
  \caption{\label{fig:flex} Illustration of the increase in communication costs with increasing levels of \tsort. Partitions for the case of $p=3$ are drawn with the boundary of the partition ($s$) and the load-imbalance ($\lambda$) given along with the level ($l$) at which the partition is defined. At each level, the orange partition (\textcolor{orange!50}{$\blacksquare$}) gets the extra load that is progressively reduced. The green partition (\textcolor{green!40}{$\blacksquare$}) gets the largest boundary that progressively increases.
  }
\end{figure}%

While \tsort~ is performed in place, the distributed version requires $\mathcal{O}(p)$ additional storage to perform the reduction to compute the global splitter ranks. The additional storage as well as the cost of performing the reductions can be significant for large $p$, therefore we perform the splitter selection in a staged manner. We limit the maximum number of splitters to a user-specified parameter $k\leq p$. This also reduces the cost of the reduction to compute the global ranks of the splitters from $\mathcal{O}(p\log p)$ to $\mathcal{O}(k\log p)$. The expected running time for the staged distributed \tsort ~is,
\[
T_p = t_c\frac{N}{p} + (t_s + t_w k) \log p + t_w \frac{N}{p}.
\] 

Additionally, the all-to-all exchange is also performed in a staged manner similar to \cite{hyksort,bruck97}, avoiding potential network congestion. The pseudocode for the staged distributed \tsort ~is given in Algorithm~\ref{alg:dsort}.


\subsection{Flexible Partitioning}
\label{sec:flexPart}

\begin{figure}
  \centering  
  \begin{tikzpicture}[scale=0.2]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \draw[blue, ultra thick] (0,0) -- (0,8);
  %\draw[red, ultra thick] (0,0) -- (8,0);
  %\draw[red, ultra thick] (8,0) -- (8,8);
  %\draw[red, ultra thick] (0,8) -- (8,8);
  \node at (-1,4) {$2$};
  
  \begin{scope}[shift={(9,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \node at (2,2) {$4$};    
  \end{scope}
  
  \begin{scope}[shift={(18,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (0,0) -- (4,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,0)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
 
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$4$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);        
  \end{scope}
  
  \begin{scope}[shift={(9,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);
  
  \node at (2,2) {$4$};
  \end{scope}    
  
  \begin{scope}[shift={(18,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \node at (2,2) {$4$};
  \end{scope}
  
  \begin{scope}[shift={(27,-9)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  \draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,4);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (6,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(0,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  \node at (-1,4) {$6$};
  \draw[blue, ultra thick] (0,0) -- (0,8);
  \draw[blue, ultra thick] (0,0) -- (8,0);   
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \end{scope}
  
  \begin{scope}[shift={(9,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[blue, ultra thick] (0,4) -- (0,8);
  \draw[blue, ultra thick] (4,0) -- (8,0); 
  \draw[blue, ultra thick] (0,4) -- (4,4);
  \draw[blue, ultra thick] (4,0) -- (4,4);  
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};           
  \end{scope}    
  
  \begin{scope}[shift={(18,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4);
  \draw[fill=blue!40] (0,4) rectangle +(4,4);  
  \draw[blue, ultra thick] (4,0) -- (4,8);
  \draw[blue, ultra thick] (4,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,0) -- (8,8);
  \node at (2,2) {$6$};
  \end{scope}
  
  \begin{scope}[shift={(27,-18)}]
  \draw[fill=red!20] (0,0) rectangle +(8,8);
  \draw[step=4] (0,0) grid +(8,8);
  
  \draw[fill=blue!40] (0,0) rectangle +(4,4); 
  \draw[fill=blue!40] (0,4) rectangle +(4,4); 
  \draw[fill=blue!40] (4,0) rectangle +(4,4);
  
  %\draw[blue, ultra thick] (0,0) -- (8,0);
  \draw[blue, ultra thick] (0,8) -- (4,8);
  \draw[blue, ultra thick] (8,4) -- (8,8);
  \draw[blue, ultra thick] (4,4) -- (4,8);
  \draw[blue, ultra thick] (4,4) -- (8,4);
  \node at (2,6) {$4$};
  
  \draw[step=2] (4,4) grid +(4,4);
  \draw[step=1] (6,6) grid +(2,2);
  \draw[step=0.5] (7,7) grid +(1,1);
  \end{scope}
  
  
  \end{tikzpicture}
  \caption{\label{fig:flexProof} Demonstration that the communication costs are non-decreasing with increasing levels of \tsort ~for most refinements and identification of the pathological case where the surface area decreases (bottom-right). The left column corresponds to initial boundaries (blue line) sharing 1, 2, and 3 faces of the quadrant (rows) that will be refined at level $l$. The remaining columns illustrate the change in surface area for the cases where 1-3 child nodes get added to the blue partition. The numbers represent the surface area of each case.}
\end{figure}

An advantage of the distributed \tsort~ algorithm is that we can specify a tolerance for the desired load-balance. While this is possible for samplesort variants \cite{kale93,hyksort,solomonik10}, 
%there are no guarantees on the quality of the resulting partition. 
the advantage is limited to reducing the cost of computing the partition or ordering at the cost of reduced load-balance. SFC-based partitioning algorithms are likely to partition using the \textit{finest} level octant, resulting is increased boundary surface, as the primary criterion is to equally divide the work (octants) amongst the processes. Our hypothesis is that, it should be possible to find a partition in close proximity to the optimally load-balanced partition, that has a lower inter-process boundary surface. This will enable users to get the partition with the minimum boundary surface, by specifying a tolerance on the load-balance. 
%As previously mentioned, such guarantees are not possible with traditional SFC-based partitioning algorithms. 
In case of \tsort, specifying a tolerance, in addition to making the ordering faster to compute, the induced partition also has reduced communication costs (for subsequent computations, like numerical simulations) in exchange for the increased load-imbalance. This makes \tsort ~attractive when the communication costs are high. 

By design, for the \tsort ~algorithm the load-imbalance, $\lambda = \max_r(|A_r|)/\min_r(|A_r|)$ decreases with increasing $l$ getting closer to the optimal value of $\lambda=1$. However, as we increase levels, the boundary of the partition $s$ is non-decreasing. This is illustrated in Figure~\ref{fig:flex} using a simple 2D partition using 3 processors. This allows the user to specify a tolerance, say $1\%$, which when reached will prevent further refinement, potentially reducing the inter-process boundary and thereby the communication costs of subsequent operations. Note that the claim is not for reducing the data-exchange cost during the reordering, but for subsequent operations that might be performed using the partition.  

The example in Figure~\ref{fig:flex} considers uniform refinement, but the result is also true for meshes with moderate adaptive refinement, as would be the case for most numerical simulations. This is explained in Figure~\ref{fig:flexProof}. Here we consider the partition between two processors, and the specific element that will be refined at the next level. It can be seen that for all cases except one, the surface area of the partition is non-decreasing. The case where the surface area decreases is a case of extreme refinement, that will only occur if the last child has a significantly higher refinement compared to the other siblings. While this case appears to limit the effectiveness of the approach, it is important to realize that other more-expensive approaches like spectral bisection \cite{pothen90} also fail for similar examples.  

