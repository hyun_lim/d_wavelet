//
// Created by milinda on 12/26/16.
//

/**
 * @author Milinda Fernando
 * @author Hari Sundar
 * @breif Contains data structures to store the reference element information.
 *
 * @refference: Based of HOMG code written in matlab.
 * */

#include "refel.h"


RefElement::RefElement()// default constructor
{
    Vr=NULL;
    Vr_0=NULL;
    Vr_1=NULL;
    ip_1D_0=NULL;
    ip_1D_1=NULL;
    ipT_1D_0=NULL;
    ipT_1D_1=NULL;


}

RefElement::RefElement(unsigned int dim, unsigned int order)
{

    /*
     * Reference element domain is from  (-1,1)
     * */


   m_uiDimension=dim;
   m_uiOrder=order;

   m_uiNp  = (m_uiOrder + 1) * (m_uiOrder + 1) * (m_uiOrder + 1);
   m_uiNfp = (m_uiOrder + 1) * (m_uiOrder + 1);
   m_uiNrp = (m_uiOrder + 1);




   r =new double [m_uiNrp];
   Vr=new double [m_uiNrp*m_uiNrp];

   Vr_0=new double [m_uiNrp*m_uiNrp];
   Vr_1=new double [m_uiNrp*m_uiNrp];

   double * r_child_0=new double [m_uiNrp];
   double * r_child_1=new double [m_uiNrp];

   double * xEval=new double[m_uiNrp];
   double * xCh_0=new double[m_uiNrp];
   double * xCh_1=new double[m_uiNrp];

   ip_1D_0=new double [m_uiNrp*m_uiNrp];
   ip_1D_1=new double [m_uiNrp*m_uiNrp];

   ipT_1D_0=new double [m_uiNrp*m_uiNrp];
   ipT_1D_1=new double [m_uiNrp*m_uiNrp];

    unsigned int info;

   double x_min=-1.0;
   double x_max= 1.0;

   for(unsigned int k=0;k<m_uiNrp;k++){
       r[k]=x_min+(x_max-x_min)*k/m_uiOrder;
   }

   for(unsigned int k=0;k<m_uiNrp;k++)
   { // parent to child points.
       r_child_0[k]=0.5*(r[k]-1);
       r_child_1[k]=0.5*(r[k]+1);
   }

  /*std::cout<<" r: ";printArray_1D(r,m_uiNrp);
  std::cout<<" r0: ";printArray_1D(r_child_0,m_uiNrp);
  std::cout<<" r1: ";printArray_1D(r_child_1,m_uiNrp);*/



   for(unsigned int i=0;i<m_uiNrp;i++) {

       basis::jacobip(0, 0, i, r, xEval, m_uiNrp);
       basis::jacobip(0, 0, i, r_child_0, xCh_0, m_uiNrp);
       basis::jacobip(0, 0, i, r_child_1, xCh_1, m_uiNrp);

       /*std::cout<<i<<" r eval : ";printArray_1D(xEval,m_uiNrp);
       std::cout<<i<<" r0 eval : ";printArray_1D(xCh_0,m_uiNrp);
       std::cout<<i<<" r1:eval  ";printArray_1D(xCh_1,m_uiNrp);*/

       for (unsigned int j = 0; j < m_uiNrp; j++) {
           Vr[i * m_uiNrp + j] = xEval[j];
           Vr_0[i * m_uiNrp + j] = xCh_0[j];
           Vr_1[i * m_uiNrp + j] = xCh_1[j];
       }
   }

    /*std::cout<<"Vr: "<<std::endl;
    printArray_2D(Vr,m_uiNrp,m_uiNrp);

    std::cout<<"Vr0: "<<std::endl;
    printArray_2D(Vr_0,m_uiNrp,m_uiNrp);

    std::cout<<"Vr1: "<<std::endl;
    printArray_2D(Vr_1,m_uiNrp,m_uiNrp);*/


    lapack::lapack_DGESV(m_uiNrp,m_uiNrp,Vr,m_uiNrp,Vr_0,ip_1D_0,m_uiNrp,info);
    lapack::lapack_DGESV(m_uiNrp,m_uiNrp,Vr,m_uiNrp,Vr_1,ip_1D_1,m_uiNrp,info);


    for(unsigned int i=0;i<m_uiNrp;i++) {
        for (unsigned int j = 0; j < m_uiNrp; j++){
            ipT_1D_0[i*m_uiNrp+j]=ip_1D_0[j*m_uiNrp+i];
            ipT_1D_1[i*m_uiNrp+j]=ip_1D_1[j*m_uiNrp+i];
        }
    }


   /* std::cout<<"ip_1D_0: "<<std::endl;
    printArray_2D(ip_1D_0,m_uiNrp,m_uiNrp);

    std::cout<<"ip_1D_1: "<<std::endl;
    printArray_2D(ip_1D_1,m_uiNrp,m_uiNrp);*/


    delete [] r_child_0;
    delete [] r_child_1;
    delete [] xEval;
    delete [] xCh_0;
    delete [] xCh_1;



}

RefElement::~RefElement() {

    if(Vr!=NULL) delete [] Vr;
    if(Vr_0!=NULL) delete [] Vr_0;
    if(Vr_1!=NULL) delete [] Vr_1;
    if(ip_1D_0!=NULL) delete [] ip_1D_0;
    if(ip_1D_1!=NULL) delete [] ip_1D_1;
    /*if(im1!=NULL) delete [] im1;
    if(im2!=NULL) delete [] im1;
*/

    Vr=NULL;
    Vr_0=NULL;
    Vr_1=NULL;
    ip_1D_0=NULL;
    ip_1D_1=NULL;
  /*  im1=NULL;
    im2=NULL;*/

}