//
// Created by milinda on 1/15/17.
//


/**
 * @author Milinda Fernando
 * @breif This file contains the precomputed interpolation matrices for a given order, computed based on Hari's HOMG refele.m code.
 * @Note: For wavelet based finite differencing we use uniform points instead of gll points.
 *
 * This is a temporary implementation later we will move to compute the interpolation
 * matrices for a given order on the fly. (See the jacobiPolynomials.h, dendroMatrix.h, and referenceElement.h)
 *
 * */



#ifndef SFCSORTBENCH_INTERPOLATIONMATRICES_H
#define SFCSORTBENCH_INTERPOLATIONMATRICES_H

/*
 *
 *  % Prolongation
        Ph      % interpolation from this element to its 4/8 children
        Pp      % interpolation from this element to its 2p version
 *
 *
 * */


static int _ORDER1_NROWS=3;
static int _ORDER1_NCOLS=2;

static double* PH_1={1.0 , 0, \
                     0.5, 0.5, \
                     0 , 1.0 };


static double* PH_1={1.0 , 0, \
                     0.5, 0.5, \
                     0 , 1.0 };



static int _ORDER2_NROWS=5;
static int _ORDER2_NCOLS=3;

static double* PH_2={1.0 , 0, 0, \
                    0.3750,    0.7500,   -0.1250, \
                    0,   1.0000,         0,\
                    -0.1250,    0.7500,    0.3750,\
                     0,         0,    1.0000};


static double* PP_2={1.0 , 0, 0, \
                    0.3750,    0.7500,   -0.1250, \
                    0,   1.0000,         0,\
                    -0.1250,    0.7500,    0.3750,\
                     0,         0,    1.0000};



static int _ORDER3_NROWS=7;
static int _ORDER3_NCOLS=4;

static double* PH_3={  1.0000,         0,         0,         0,\
                        0.3125,    0.9375,   -0.3125,    0.0625,\
                            0,    1.0000,    0.0000,   -0.0000,\
                        -0.0625,    0.5625,    0.5625,   -0.0625,\
                            0,         0,    1.0000,         0, \
                            0.0625,   -0.3125,    0.9375,    0.3125,\
                            0,         0,         0,    1.0000};


static double* PP_3={  1.0000,         0,         0,         0,\
                        0.3125,    0.9375,   -0.3125,    0.0625,\
                            0,    1.0000,    0.0000,   -0.0000,\
                        -0.0625,    0.5625,    0.5625,   -0.0625,\
                            0,         0,    1.0000,         0, \
                            0.0625,   -0.3125,    0.9375,    0.3125,\
                            0,         0,         0,    1.0000};





static int _ORDER4_NROWS=9;
static int _ORDER4_NCOLS=5;

static double* PH_4=\
{ 1.0000,   -0.0000,    0.0000,   -0.0000,   -0.0000,\
0.2734,    1.0938,   -0.5469,    0.2188,   -0.0391,\
-0.0000,    1.0000,   -0.0000,    0.0000,    0.0000,\
-0.0391,    0.4688,    0.7031,   -0.1563,    0.0234,\
0.0000,         0,    1.0000,   0.0000,    0.0000,\
0.0234,   -0.1562,    0.7031,    0.4688,   -0.0391,\
0,   -0.0000,   -0.0000,    1.0000,         0,\
-0.0391,    0.2187,   -0.5469,    1.0938,    0.2734,\
0,         0,         0,         0,    1.0000,\
};


static double* PP_4=\
{ 1.0000,   -0.0000,    0.0000,   -0.0000,   -0.0000,\
0.2734,    1.0938,   -0.5469,    0.2188,   -0.0391,\
-0.0000,    1.0000,   -0.0000,    0.0000,    0.0000,\
-0.0391,    0.4688,    0.7031,   -0.1563,    0.0234,\
0.0000,         0,    1.0000,   0.0000,    0.0000,\
0.0234,   -0.1562,    0.7031,    0.4688,   -0.0391,\
0,   -0.0000,   -0.0000,    1.0000,         0,\
-0.0391,    0.2187,   -0.5469,    1.0938,    0.2734,\
0,         0,         0,         0,    1.0000,\
};




static int _ORDER5_NROWS=11;
static int _ORDER5_NCOLS=6;

static double* PH_5=\
{ 1.0000,         0,         0,         0,         0,         0,\
0.2461,    1.2305,   -0.8203,    0.4922,   -0.1758,    0.0273,\
0,    1.0000,         0,         0,         0,         0,\
-0.0273,    0.4102,    0.8203,   -0.2734,    0.0820,   -0.0117,\
-0.0000,    0.0000,    1.0000,   -0.0000,    0.0000,    0.0000,\
0.0117,   -0.0977,    0.5859,    0.5859,   -0.0977,    0.0117,\
-0.0000,   -0.0000,   -0.0000,    1.0000,    0.0000,   -0.0000,\
-0.0117,    0.0820,   -0.2734,    0.8203,    0.4102,   -0.0273,\
0.0000,         0,         0,         0,   1.0000,    0.0000,\
0.0273,   -0.1758,    0.4922,   -0.8203,    1.2305,    0.2461,\
0,         0,         0,         0,         0,   1.0000,\
};


static double* PP_5=\
{ 1.0000,         0,         0,         0,         0,         0,\
0.2461,    1.2305,   -0.8203,    0.4922,   -0.1758,    0.0273,\
0,    1.0000,         0,         0,         0,         0,\
-0.0273,    0.4102,    0.8203,   -0.2734,    0.0820,   -0.0117,\
-0.0000,    0.0000,    1.0000,   -0.0000,    0.0000,    0.0000,\
0.0117,   -0.0977,    0.5859,    0.5859,   -0.0977,    0.0117,\
-0.0000,   -0.0000,   -0.0000,    1.0000,    0.0000,   -0.0000,\
-0.0117,    0.0820,   -0.2734,    0.8203,    0.4102,   -0.0273,\
0.0000,         0,         0,         0,   1.0000,    0.0000,\
0.0273,   -0.1758,    0.4922,   -0.8203,    1.2305,    0.2461,\
0,         0,         0,         0,         0,   1.0000,\
};




#endif //SFCSORTBENCH_INTERPOLATIONMATRICES_H
