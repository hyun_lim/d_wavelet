//
// Created by milinda on 12/25/16.
//

/**
 * @author Milinda Fernando
 * @author Hari Sundar
 * @breif Contains data structures to store the reference element information.
 *
 * @refference: Based of HOMG code written in matlab.
 * */
#ifndef SFCSORTBENCH_REFERENCEELEMENT_H
#define SFCSORTBENCH_REFERENCEELEMENT_H

#include "basis.h"
#include "tensor.h"
class RefElement{


private :
    /** Dimension */
    int                 m_uiDimension;
    /** Polynomial Order */
    int                 m_uiOrder;
    /** Number of 3D interpolation points on the element */
    int                 m_uiNp;
    /** Number of 2D face interpolation points */
    int                 m_uiNfp;
    /** Number of 1D interpolation points */
    int                 m_uiNrp;


    /** 1D reference coordinates of the interpolation nodes
     *  Size ( 1 x \c Nrp )
     */
    double *       r;

    /** Gauss points */
    //double *       rg;

    /** 1D interpolation matrix for child 0 */
    double * ip_1D_0;

    /** 1D interpolation matrix for child 1*/
    double * ip_1D_1;


    /** 1D interpolation matrix for child 0 (transpose) */
    double * ipT_1D_0;

    /** 1D interpolation matrix for child 1 (transpose)*/
    double * ipT_1D_1;

    /**Vandermonde matrix for interpolation points.   */
    double * Vr;

    /**Vandermonde matrix for interpolation points of child 0   */
    double * Vr_0;

    /**Vandermonde matrix for interpolation points of child 1   */
    double * Vr_1;



public:
    RefElement();
    RefElement(unsigned int dim, unsigned int order);
    ~RefElement();
    // some getter methods to access required data.
    inline int getOrder(){return m_uiOrder;}
    inline int getDim(){return m_uiDimension;}
    inline int get1DNumInterpolationPoints(){return m_uiNrp;}

    inline double * getIMChild0(){return ip_1D_0;}
    inline double * getIMChild1(){return ip_1D_1;}

    /**
     * @param[in] in: input function values.
     * @param[in] childNum: Morton ID of the child number where the interpolation is needed.
     * @param[out] out: interpolated values.
     *
     * @brief This is computed in way that 3d coordinates changes in the order of z, y, x
     * Which means first we need to fill all the z values in plane(x=0,y=0) then all the z values in plane (x=0,y=0+h) and so forth.
     *
     */
    inline void I3D_Parent2Child(double * in, double* out, unsigned int childNum )
    {

        double * im1=new double[m_uiNp];
        double * im2=new double[m_uiNp];

        switch (childNum)
        {
            case 0:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_0,in,im1); // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im2,out); // along z
                break;
            case 1:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_1,in,im1); // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im2,out); // along z

                break;
            case 2:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_0,in,im1);  // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im2,out); // along z
                break;
            case 3:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_1,in,im1);  // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im2,out); // along z
                break;
            case 4:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_0,in,im1);  // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im2,out); // along z
                break;
            case 5:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_1,in,im1); // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_0,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im2,out); // along z
                break;
            case 6:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_0,in,im1); // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im2,out); // along z
                break;
            case 7:
                DENDRO_TENSOR_IIAX_APPLY_ELEM(m_uiNrp,ip_1D_1,in,im1); // along x
                DENDRO_TENSOR_IAIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im1,im2); // along y
                DENDRO_TENSOR_AIIX_APPLY_ELEM(m_uiNrp,ip_1D_1,im2,out); // along z
                break;
            default:
            std::cout<<"[refel][error]: invalid child number specified for the interpolation."<<std::endl;
                break;

        }

        delete [] im1;
        delete [] im2;

    }




};

#endif //SFCSORTBENCH_REFERENCEELEMENT_H
