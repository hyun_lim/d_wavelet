//
// Created by milinda on 9/2/16.
//

/**
 * @author: Milinda Shayamal Fernando
 * School of Computing , University of Utah
 *
 * @breif Contains the functions to generate the mesh data structure from the 2:1 balanced linear octree.
 *
 * Assumptions:
 * 1). Assumes that octree is balanced and sorted.
 * 2). Assumes that there is no duplicate nodes.
 *
 *
 * */


#include "mesh.h"

namespace ot {



    Mesh::Mesh(std::vector<ot::TreeNode> &in,unsigned int k_s,unsigned int pOrder) {

        m_uiStensilSz=k_s;
        m_uiElementOrder=pOrder;

        m_uiRank=0;
        m_uiNpes=1;


        m_uiEL_i=0; // initilize the counter this is not mandatory.
        m_uiNumDirections=(1u << m_uiDim) - 2 * (m_uiDim - 2);

        if(m_uiDim==2)
            m_uiNpE=(m_uiElementOrder+1)*(m_uiElementOrder+1);
        else if(m_uiDim==3)
            m_uiNpE=(m_uiElementOrder+1)*(m_uiElementOrder+1)*(m_uiElementOrder+1);

        buildE2EMap(in);
        buildE2NMap();

    }


    Mesh::Mesh(std::vector<ot::TreeNode> &in, unsigned int k_s, unsigned int pOrder,MPI_Comm comm)  //!!! Note: k_s <= 3
    {

        m_uiComm=comm;
        MPI_Comm_rank(m_uiComm, &m_uiRank);
        MPI_Comm_size(m_uiComm, &m_uiNpes);


        /*m_uiElementOrder=pOrder;
        m_uiRefEl=RefElement(1,m_uiElementOrder);*/

        m_uiStensilSz=k_s;
        m_uiElementOrder=pOrder;
        m_uiEL_i=0;

        if(m_uiDim==2)
            m_uiNpE=(m_uiElementOrder+1)*(m_uiElementOrder+1);
        else if(m_uiDim==3)
            m_uiNpE=(m_uiElementOrder+1)*(m_uiElementOrder+1)*(m_uiElementOrder+1);

        m_uiNumDirections=(1u << m_uiDim) - 2 * (m_uiDim - 2);

        buildE2EMap(in,comm);
        buildE2NMap();
        computeNodeScatterMaps(comm);




    }

    Mesh::~Mesh() {

        if(m_uiNpes>1) {
            delete[] m_uiSplitter;
            delete[] m_uiSplitterNodes;
            delete[] m_uiSendKeyCount;
            delete[] m_uiSendKeyOffset;
            delete[] m_uiSendOctCountRound1;
            delete[] m_uiSendOctOffsetRound1;
            delete[] m_uiSendOctCountRound2;
            delete[] m_uiSendOctOffsetRound2;


            delete[] m_uiRecvKeyCount;
            delete[] m_uiRecvKeyOffset;
            delete[] m_uiRecvOctCountRound1;
            delete[] m_uiRecvOctOffsetRound1;
            delete[] m_uiRecvOctCountRound2;
            delete[] m_uiRecvOctOffsetRound2;


        }


        m_uiPreGhostOctants.clear();
        m_uiPostGhostOctants.clear();

        m_uiEmbeddedOctree.clear();
        m_uiGhostOctants.clear();
        m_uiAllElements.clear();

        m_uiE2NMapping_CG.clear();
        m_uiE2NMapping_DG.clear();
        m_uiE2EMapping.clear();

        m_uiSendBufferElement.clear();
        m_uiScatterMapElementRound1.clear();
        m_uiSendBufferNodes.clear();
        m_uiScatterMapActualNodeSend.clear();
        m_uiScatterMapActualNodeRecv.clear();
    }


    void Mesh::generateSearchKeys()
    {

        std::set<Key, OctreeComp<Key> > keys;
        ot::TreeNode *inPtr = (&(*(m_uiEmbeddedOctree.begin())));
        std::pair<std::set<Key, OctreeComp<Key> >::iterator, bool> hint;
        unsigned int domain_max = (1u<<(m_uiMaxDepth-1))+1;
        Key tmpKey;

        unsigned int K=1;
        for (int i = 0; i < m_uiEmbeddedOctree.size(); i++) {//$
            register unsigned int myLev = m_uiEmbeddedOctree[i].getLevel();
            if ((myLev == 1)) {
                continue;
            }

            register unsigned int mySz = (1u << (m_uiMaxDepth - myLev));
            register unsigned int myX = inPtr[i].getX();
            register unsigned int myY = inPtr[i].getY();
            register unsigned int myZ = inPtr[i].getZ();
            //domain_max=(1u<<my)
            // We can skip the morton index -0  because that key is mapped to the current element. So No need to search for that.
            // Note: we do not need to perform any boundary checks when generating the keys because, we are skipping all the level one octatns.

            //for (unsigned int K = 1; K <= m_uiStensilSz; K++) {


            /** *
             * Below orientation is used when generating keys.
             *
             * [up]
             * Y
             * |     Z [front]
             * |    /
             * |   /
             * |  /
             * | /
             * -------------> X [right]
             */

            // Key generation along X axis.
            if ((myX + K * mySz) < domain_max) {
                hint = keys.emplace(Key((myX + K * mySz), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_RIGHT);
                if (hint.second) m_uiKeys.push_back(*(hint.first));

            }
            if (myX >0) {
                hint = keys.emplace(Key((myX - 1), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_LEFT);
                if (hint.second) m_uiKeys.push_back(*(hint.first));

            }



         /*   // some additional keys to recover E2N send/recv mismatch for some wiered partitions.
            if (((myX + K * mySz) < domain_max) && ((myY + K * mySz) < domain_max) ) {

                hint = keys.emplace( Key((myX+K*mySz), (myY + K * mySz), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                //hint.first->addOwner(i);
                if (hint.second) m_uiKeys.push_back(*(hint.first));
            }*/


                // Key generation along Y axis.
            if ((myY + K * mySz) < domain_max) {
                hint = keys.emplace( Key(myX, (myY + K * mySz), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_UP);
                if (hint.second) m_uiKeys.push_back(*(hint.first));
            }
            if (myY >0) {
                hint = keys.emplace(Key(myX, (myY - 1), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_DOWN);
                if (hint.second) m_uiKeys.push_back(*(hint.first));

            }

            if (m_uiDim == 3) {

                if ((myZ + K * mySz) < domain_max) {
                    hint = keys.emplace(Key(myX, myY, (myZ + K * mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_FRONT);
                    if (hint.second) m_uiKeys.push_back(*(hint.first));

                }


                /*// some additional keys to recover E2N send/recv mismatch for some wiered partitions.
                if (((myX + K * mySz) < domain_max) && ((myZ + K * mySz) < domain_max) ) {

                    hint = keys.emplace( Key((myX+K*mySz), myY, (myZ+mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    //hint.first->addOwner(i);
                    if (hint.second) m_uiKeys.push_back(*(hint.first));
                }

                // some additional keys to recover E2N send/recv mismatch for some wiered partitions.
                if (((myY + K * mySz) < domain_max) && ((myZ + K * mySz) < domain_max) ) {

                    hint = keys.emplace( Key(myX, (myY+K*mySz), (myZ+K*mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    //hint.first->addOwner(i);
                    if (hint.second) m_uiKeys.push_back(*(hint.first));
                }

                // some additional keys to recover E2N send/recv mismatch for some wiered partitions.
                if (((myX + K * mySz) < domain_max) && ((myY + K * mySz) < domain_max)  && ((myZ + K * mySz) < domain_max) ) {

                    hint = keys.emplace( Key((myX+K*mySz), (myY+K*mySz), (myZ+K*mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    //hint.first->addOwner(i);
                    if (hint.second) m_uiKeys.push_back(*(hint.first));
                }*/


                if (myZ >0) {
                    hint = keys.emplace(Key(myX, myY, (myZ - 1), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_BACK);
                    if (hint.second) m_uiKeys.push_back(*(hint.first));
                }

            }

            //}
        }//end for i

       /* std::cout << "Key Gen time: " << (t2 - t1) << " Keys generated: " << keys.size() << " m_uiEmbeddedOCtree Size: "
                  << m_uiEmbeddedOctree.size() << std::endl;
*/

 if(m_uiNpes>1) {
     for (unsigned int i = 0; i < m_uiNpes; i++) {
         tmpKey = Key(m_uiSplitter[i].getX(), m_uiSplitter[i].getY(), m_uiSplitter[i].getZ(), m_uiMaxDepth,
                      m_uiSplitter[i].getDim(), m_uiMaxDepth);
         hint = keys.emplace(tmpKey);
         if (hint.second) m_uiKeys.push_back(tmpKey);

     }
 }

        keys.clear();


    }


    void Mesh::generateGhostElementSearchKeys()
    {


        std::set<Key, OctreeComp<Key> > keys;
        ot::TreeNode *inPtr = (&(*(m_uiAllElements.begin())));
        std::pair<std::set<Key, OctreeComp<Key> >::iterator, bool> hint;
        unsigned int domain_max = (1u<<(m_uiMaxDepth-1))+1;
        Key tmpKey;

        unsigned int K=1;
        for (unsigned int i = m_uiElementPreGhostBegin; i < m_uiElementPreGhostEnd; i++) {//$

            register unsigned int myLev = inPtr[i].getLevel();
            if ((myLev == 1)) {
                continue;
            }

            register unsigned int mySz = (1u << (m_uiMaxDepth - myLev));
            register unsigned int myX = inPtr[i].getX();
            register unsigned int myY = inPtr[i].getY();
            register unsigned int myZ = inPtr[i].getZ();
            //domain_max=(1u<<my)
            // We can skip the morton index -0  because that key is mapped to the current element. So No need to search for that.
            // Note: we do not need to perform any boundary checks when generating the keys because, we are skipping all the level one octatns.

            //for (unsigned int K = 1; K <= m_uiStensilSz; K++) {


            /** *
             * Below orientation is used when generating keys.
             *
             * [up]
             * Y
             * |     Z [front]
             * |    /
             * |   /
             * |  /
             * | /
             * -------------> X [right]
             */

            // Key generation along X axis.
            if ((myX + K * mySz) < domain_max) {
                hint = keys.emplace(Key((myX + K * mySz), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_RIGHT);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }
            if (myX >0) {
                hint = keys.emplace(Key((myX - 1), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_LEFT);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }
            // Key generation along Y axis.
            if ((myY + K * mySz) < domain_max) {
                hint = keys.emplace( Key(myX, (myY + K * mySz), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_UP);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
            }
            if (myY >0) {
                hint = keys.emplace(Key(myX, (myY - 1), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_DOWN);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }

            if (m_uiDim == 3) {

                if ((myZ + K * mySz) < domain_max) {
                    hint = keys.emplace(Key(myX, myY, (myZ + K * mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_FRONT);
                    if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
                }
                if (myZ >0) {
                    hint = keys.emplace(Key(myX, myY, (myZ - 1), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_BACK);
                    if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
                }

            }



            //}
        }//end for i

        for (unsigned int i = m_uiElementPostGhostBegin; i < m_uiElementPostGhostEnd; i++) {//$

            register unsigned int myLev = inPtr[i].getLevel();
            if ((myLev == 1)) {
                continue;
            }

            register unsigned int mySz = (1u << (m_uiMaxDepth - myLev));
            register unsigned int myX = inPtr[i].getX();
            register unsigned int myY = inPtr[i].getY();
            register unsigned int myZ = inPtr[i].getZ();
            //domain_max=(1u<<my)
            // We can skip the morton index -0  because that key is mapped to the current element. So No need to search for that.
            // Note: we do not need to perform any boundary checks when generating the keys because, we are skipping all the level one octatns.

            //for (unsigned int K = 1; K <= m_uiStensilSz; K++) {


            /** *
             * Below orientation is used when generating keys.
             *
             * [up]
             * Y
             * |     Z [front]
             * |    /
             * |   /
             * |  /
             * | /
             * -------------> X [right]
             */

            // Key generation along X axis.
            if ((myX + K * mySz) < domain_max) {
                hint = keys.emplace(Key((myX + K * mySz), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_RIGHT);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }
            if (myX >0) {
                hint = keys.emplace(Key((myX - 1), myY, myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_LEFT);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }
            // Key generation along Y axis.
            if ((myY + K * mySz) < domain_max) {
                hint = keys.emplace( Key(myX, (myY + K * mySz), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_UP);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
            }
            if (myY >0) {
                hint = keys.emplace(Key(myX, (myY - 1), myZ, m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hint.first->addOwner(i);
                hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_DOWN);
                if (hint.second) m_uiGhostKeys.push_back(*(hint.first));

            }

            if (m_uiDim == 3) {

                if ((myZ + K * mySz) < domain_max) {
                    hint = keys.emplace(Key(myX, myY, (myZ + K * mySz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_FRONT);
                    if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
                }
                if (myZ >0) {
                    hint = keys.emplace(Key(myX, myY, (myZ - 1), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                    hint.first->addOwner(i);
                    hint.first->addStencilIndexAndDirection(K - 1, OCT_DIR_BACK);
                    if (hint.second) m_uiGhostKeys.push_back(*(hint.first));
                }

            }



            //}
        }


        keys.clear();

    }


    void Mesh::buildE2EMap(std::vector<ot::TreeNode> &in,MPI_Comm comm)
    {

        int rank, npes;
        MPI_Comm_rank(comm, &rank);
        MPI_Comm_size(comm, &npes);



        // 1a - Embed the octree one level deeper.
#ifdef DEBUG_MESH_GENERATION
        treeNodesTovtk(in,rank,"balOct");
#endif

        addBoundaryNodesType1(in, m_uiPositiveBoundaryOctants, m_uiDim, m_uiMaxDepth);
        m_uiMaxDepth = m_uiMaxDepth + 1;

#ifdef DEBUG_MESH_GENERATION
        //treeNodesTovtk(m_uiPositiveBoundaryOctants,rank,"positiveBoundary");
#endif

        std::swap(m_uiEmbeddedOctree, in);
        m_uiEmbeddedOctree.insert(m_uiEmbeddedOctree.end(), m_uiPositiveBoundaryOctants.begin(),
                                  m_uiPositiveBoundaryOctants.end());


        // clear the in and positive boundary octants;
        m_uiPositiveBoundaryOctants.clear();
        in.clear();

        /*
         * Below Key vector and ot::TreeNode vector is being use for sorting Keys and treeNodes repeatedly. So make sure you clear them after using them in SFC_TreeSort.
         *
         * */
        std::vector<Key> tmpKeys;
        std::vector<ot::TreeNode> tmpNodes;

        Key rootKey(0, 0, 0, (OCT_KEY_NONE | 0), m_uiDim, m_uiMaxDepth);
        ot::TreeNode rootNode(0,0,0,0,m_uiDim,m_uiMaxDepth);

        //std::cout<<"rank: "<<rank<<" par::sort begin: "<<m_uiMaxDepth<<" "<<m_uiDim<<" embedded octreeSize: "<<m_uiEmbeddedOctree.size()<<std::endl;
        SFC::parSort::SFC_treeSort(m_uiEmbeddedOctree, tmpNodes, tmpNodes, tmpNodes, 0.1, m_uiMaxDepth, rootNode, 0, 1,TS_REMOVE_DUPLICATES, 2, comm);
        std::swap(tmpNodes, m_uiEmbeddedOctree);
        tmpNodes.clear();

        assert(par::test::isUniqueAndSorted(m_uiEmbeddedOctree, comm));

        unsigned int localSz = m_uiEmbeddedOctree.size();

#ifdef DEBUG_MESH_GENERATION
        treeNodesTovtk(m_uiEmbeddedOctree,rank,"m_uiEmbeddedOctree");
#endif

        //1a- embedding END


        // AllGather of the the max local octant. This will be used to split the keys among the processors.

        m_uiSplitter = new ot::TreeNode[npes];
        par::Mpi_Allgather(&m_uiEmbeddedOctree[localSz - 1], m_uiSplitter, 1, comm);
#ifdef DEBUG_MESH_GENERATION
        if(!rank)
        {
            std::vector<ot::TreeNode> splitter_vec;
            for(int i=0;i<npes;i++)
            {
                splitter_vec.push_back(m_uiSplitter[i]);
            }
            treeNodesTovtk(splitter_vec,rank,"splitters");
        }
#endif
        //scan value will be needed for computing the global index of octants.
        // par::Mpi_Scan(&localSz,&localSz_scan,1, MPI_SUM,comm);

        unsigned int localOctreeSz = m_uiEmbeddedOctree.size();




        // 2- generate the keys.

        generateSearchKeys();

        // 2a - Generate Keys END

#ifdef DEBUG_MESH_GENERATION
        if(!rank) std::cout<<"Key generation completed "<<std::endl;
#endif

        //std::cout<<"maxDepth: "<<m_uiMaxDepth<<std::endl;

        //3 - Exchange Keys



        //==================
#ifdef DEBUG_MESH_GENERATION
        unsigned int localKeySz=keys_vec.size();
        unsigned int globalKeySz=0;
        par::Mpi_Reduce(&localKeySz,&globalKeySz,1,MPI_SUM,0,comm);
        if(!rank) std::cout<<" Total number of keys generated: "<<globalKeySz<<std::endl;
#endif

        //==================

        SFC::seqSort::SFC_treeSort(&(*(m_uiKeys.begin())), m_uiKeys.size(), tmpKeys, tmpKeys, tmpKeys, m_uiMaxDepth,m_uiMaxDepth, rootKey, 0, 1, TS_SORT_ONLY);
        tmpKeys.clear();
        assert(seq::test::isUniqueAndSorted(m_uiKeys));



#ifdef DEBUG_MESH_GENERATION
        MPI_Barrier(comm);
          std::vector<ot::TreeNode> splitters;
        for(unsigned int p=0;p<npes;p++)
            splitters.push_back(m_uiSplitter[p]);

        treeNodesTovtk(splitters,rank,"m_uiSplitter");
        if(!rank) std::cout<<"SeqSort cmpleted for generated Keys. "<<std::endl;
        treeNodesTovtk(m_uiKeys,rank,"m_uiKeys",false);
#endif


        m_uiSendKeyCount = new unsigned int[npes];
        m_uiRecvKeyCount = new unsigned int[npes];

        m_uiSendKeyOffset = new unsigned int[npes];
        m_uiRecvKeyOffset = new unsigned int[npes];


        m_uiSendOctCountRound1 = new unsigned int[npes];
        m_uiRecvOctCountRound1 = new unsigned int[npes];

        m_uiSendOctOffsetRound1 = new unsigned int[npes];
        m_uiRecvOctOffsetRound1 = new unsigned int[npes];


        unsigned int count = 0, keyCount = 0;
        Key * m_uiKeysPtr=&(*(m_uiKeys.begin()));
        for (unsigned int i = 0; i < m_uiKeys.size(); i++) {

            if(count!=rank) {
                for (unsigned int w = 0; w < m_uiKeysPtr[i].getOwnerList()->size(); w++)
                    m_uiGhostElementIDsToBeSent.push_back((*(m_uiKeysPtr[i].getOwnerList()))[w]);

                if ((m_uiKeysPtr[i] == m_uiSplitter[count]) || m_uiSplitter[count].isAncestor(m_uiKeysPtr[i])) {

                    if (i < (m_uiKeys.size() - 1) && ((!(m_uiKeysPtr[i + 1] == m_uiSplitter[count])) && (!(m_uiSplitter[count].isAncestor(m_uiKeysPtr[i + 1]))))) {
                        m_uiSendKeyCount[count] = keyCount + m_uiKeysPtr[i].getOwnerList()->size();
                        count++;
                        keyCount = 0;
                        continue;
                    }

                }
                keyCount += m_uiKeysPtr[i].getOwnerList()->size();
                m_uiSendKeyCount[count] = keyCount;

            }else
            {
                while (!((m_uiKeysPtr[i] == m_uiSplitter[count]) || m_uiSplitter[count].isAncestor(m_uiKeysPtr[i]))) i++;
                while ( (i < (m_uiKeys.size() - 1)) && (((m_uiKeysPtr[i + 1] == m_uiSplitter[count])) ||((m_uiSplitter[count].isAncestor(m_uiKeysPtr[i + 1]))))) i++;
                if(i) i--;

                m_uiSendKeyCount[count]=0;
                count++;
                keyCount=0;

            }
        }

        par::Mpi_Alltoall(m_uiSendKeyCount,m_uiRecvKeyCount,1,comm);

        m_uiSendKeyOffset[0]=0;
        m_uiRecvKeyOffset[0]=0;
        omp_par::scan(m_uiSendKeyCount,m_uiSendKeyOffset,npes);
        omp_par::scan(m_uiRecvKeyCount,m_uiRecvKeyOffset,npes);

        m_uiGhostElementIDsToBeRecv.resize(m_uiRecvKeyOffset[npes-1]+m_uiRecvKeyCount[npes-1]);
        par::Mpi_Alltoallv(&(*(m_uiGhostElementIDsToBeSent.begin())),(int *)m_uiSendKeyCount,(int *)m_uiSendKeyOffset,&(*(m_uiGhostElementIDsToBeRecv.begin())),(int *)m_uiRecvKeyCount,(int *) m_uiRecvKeyOffset,comm);

        //std::cout<<rank <<" ghostEleSz: "<<m_uiGhostElementIDsToBeSent.size()<<" computed Size: "<<(m_uiSendKeyOffset[npes-1]+m_uiSendKeyCount[npes-1])<<std::endl;
        assert(m_uiGhostElementIDsToBeSent.size()==(m_uiSendKeyOffset[npes-1]+m_uiSendKeyCount[npes-1]));

        std::set<unsigned  int > tmpSendElementIds;
        for(unsigned int p=0;p<npes;p++)
        {
            //if(!rank) std::cout<<" P: "<<p<<"begin: "<<m_uiSendKeyOffset[p]<<" end : "<<(m_uiSendKeyCount[p]+m_uiSendKeyOffset[p])<<std::endl;
            tmpSendElementIds.insert((m_uiGhostElementIDsToBeSent.begin()+m_uiSendKeyOffset[p]),(m_uiGhostElementIDsToBeSent.begin()+m_uiSendKeyCount[p]+m_uiSendKeyOffset[p]));
            m_uiSendOctCountRound1[p]=tmpSendElementIds.size();
            m_uiScatterMapElementRound1.insert(m_uiScatterMapElementRound1.end(),tmpSendElementIds.begin(),tmpSendElementIds.end());
            tmpSendElementIds.clear();
        }

#ifdef DEBUG_MESH_GENERATION
        MPI_Barrier(comm);
        if(!rank) std::cout<<"Ghost Element Size: "<<m_uiScatterMapElementRound1.size()<<" without removing duplicates: "<<m_uiGhostElementIDsToBeSent.size()<<std::endl;
#endif
        m_uiGhostElementIDsToBeSent.clear();


#ifdef DEBUG_MESH_GENERATION
        /* if(!rank)
         for(unsigned int k=0;k<npes;k++)
         {
             std::cout<<"Processor "<<k<<" sendCnt: "<<m_uiSendKeyCount[k]<<" recvCnt: "<<m_uiRecvKeyCount[k]<<std::endl;
         }*/
#endif

        par::Mpi_Alltoall(m_uiSendOctCountRound1,m_uiRecvOctCountRound1,1,comm);

        m_uiSendOctOffsetRound1[0]=0;
        m_uiRecvOctOffsetRound1[0]=0;

        omp_par::scan(m_uiSendOctCountRound1,m_uiSendOctOffsetRound1,npes);
        omp_par::scan(m_uiRecvOctCountRound1,m_uiRecvOctOffsetRound1,npes);

        m_uiSendBufferElement.resize(m_uiSendOctOffsetRound1[npes-1]+m_uiSendOctCountRound1[npes-1]);

        for(unsigned int k=0;k<m_uiSendBufferElement.size();k++)
            m_uiSendBufferElement[k]=m_uiEmbeddedOctree[m_uiScatterMapElementRound1[k]];

        m_uiGhostOctants.resize(m_uiRecvOctOffsetRound1[npes - 1] + m_uiRecvOctCountRound1[npes - 1]);
        par::Mpi_Alltoallv(&(*(m_uiSendBufferElement.begin())), (int *) m_uiSendOctCountRound1, (int *) m_uiSendOctOffsetRound1,
                           &(*(m_uiGhostOctants.begin())), (int *) m_uiRecvOctCountRound1, (int *) m_uiRecvOctOffsetRound1, comm);



//#ifdef DEBUG_MESH_GENERATION
        treeNodesTovtk(m_uiEmbeddedOctree,rank,"embeddedOctree");
        treeNodesTovtk(m_uiGhostOctants,rank,"ghostR1");

// #endif


        ot::TreeNode localOct_min = m_uiEmbeddedOctree.front();
        ot::TreeNode localOct_max = m_uiEmbeddedOctree.back();

        std::swap(m_uiAllElements, m_uiEmbeddedOctree);
        m_uiEmbeddedOctree.clear();
        m_uiAllElements.insert(m_uiAllElements.end(), m_uiGhostOctants.begin(), m_uiGhostOctants.end());

        SFC::seqSort::SFC_treeSort(&(*(m_uiAllElements.begin())), m_uiAllElements.size(), tmpNodes, tmpNodes, tmpNodes, m_uiMaxDepth,m_uiMaxDepth, rootNode, 0, 1, TS_REMOVE_DUPLICATES);
        std::swap(m_uiAllElements, tmpNodes);
        tmpNodes.clear();

        assert(seq::test::isUniqueAndSorted(m_uiAllElements));

        std::vector<ot::TreeNode> ghostElementsRound1;
        std::swap(m_uiGhostOctants,ghostElementsRound1);

        tmpNodes.clear();
        SFC::seqSort::SFC_treeSort(&(*(ghostElementsRound1.begin())), ghostElementsRound1.size(), tmpNodes, tmpNodes, tmpNodes, m_uiMaxDepth,m_uiMaxDepth, rootNode, 0, 1, TS_REMOVE_DUPLICATES);
        std::swap(ghostElementsRound1,tmpNodes);
        tmpNodes.clear();

        m_uiGhostOctants.clear();

        m_uiElementPreGhostBegin = 0;
        m_uiElementPreGhostEnd = 0;
        m_uiElementLocalBegin = 0;
        m_uiElementLocalEnd = 0;
        m_uiElementPostGhostBegin = 0;
        m_uiElementPostGhostEnd = 0;

        for (unsigned int k = 0; k < m_uiAllElements.size(); k++) {
            if (m_uiAllElements[k] == localOct_min) {
                m_uiElementLocalBegin = k;
                break;
            }
        }

        for (unsigned int k = (m_uiAllElements.size() - 1); k > 0; k--) {
            if (m_uiAllElements[k] == localOct_max)
                m_uiElementLocalEnd = k + 1;
        }

        m_uiElementPreGhostEnd = m_uiElementLocalBegin;
        m_uiElementPostGhostBegin = m_uiElementLocalEnd;
        m_uiElementPostGhostEnd = m_uiAllElements.size();

        m_uiNumLocalElements=(m_uiElementLocalEnd-m_uiElementLocalBegin);
        m_uiNumPreGhostElements=(m_uiElementPreGhostEnd-m_uiElementPreGhostBegin);
        m_uiNumPostGhostElements=(m_uiElementPostGhostEnd-m_uiElementPostGhostBegin);

        m_uiNumTotalElements=m_uiNumPreGhostElements+m_uiNumLocalElements+m_uiNumPostGhostElements;



        // E2E Mapping for the Round 1 Ghost Exchange. Later we will perform another round of ghost exchange, (for the ghost elements that are hanging ) and build the correct complete E2E mapping.
        SFC::seqSearch::SFC_treeSearch(&(*(m_uiKeys.begin())), &(*(m_uiAllElements.begin())), 0, m_uiKeys.size(), 0,m_uiAllElements.size(), m_uiMaxDepth, m_uiMaxDepth, 0);

        std::vector<unsigned int> *ownerList;
        std::vector<uint8_t> *stencilIndexDirection;
        unsigned int result;
        unsigned int dir;
        //unsigned int stencilIndex;
        m_uiKeysPtr=&(*(m_uiKeys.begin()));

        m_uiE2EMapping.resize(m_uiAllElements.size()*m_uiNumDirections,LOOK_UP_TABLE_DEFAULT);

        /**Neighbour list is made from
        *  first x axis. (left to right)
        *  second y axis. (down to up)
        *  third z axis. (back to front)**/

        for (unsigned int k = 0; k < m_uiKeys.size(); k++) {
            ownerList = m_uiKeysPtr[k].getOwnerList();
            if(ownerList->size()) assert((OCT_FOUND & m_uiKeysPtr[k].getFlag()));// Note that all the keys should be found locally due to the fact that we have exchanged ghost elements.
            stencilIndexDirection = m_uiKeysPtr[k].getStencilIndexDirectionList();
            result = m_uiKeysPtr[k].getSearchResult();
            if(ownerList->size()) assert(m_uiAllElements[result].isAncestor(m_uiKeys[k]) || m_uiAllElements[result]==m_uiKeys[k]); // To check the result found in the treeSearch is correct or not.
            for (unsigned int w = 0; w < ownerList->size(); w++) {
                dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                //stencilIndex = (((*stencilIndexDirection)[w]) & (KS_MAX << 3u)) >> 3u;
                m_uiE2EMapping[(((*ownerList)[w]) + m_uiElementLocalBegin)*m_uiNumDirections+dir] = result;
                // Note : Following is done to enforce that the local elements that points to ghost elements has the inverse mapping.
                /*if(result<m_uiElementLocalBegin || result>=m_uiElementLocalEnd)
                    m_uiE2EMapping[result*m_uiNumDirections+ (1u^dir)]=(((*ownerList)[w]) + m_uiElementLocalBegin); // Note This depends on the OCT_DIR numbering.*/


            }

        }


        m_uiGhostKeys.clear();
        generateGhostElementSearchKeys();

        SFC::seqSearch::SFC_treeSearch(&(*(m_uiGhostKeys.begin())),&(*(m_uiAllElements.begin())),0,m_uiGhostKeys.size(),0,m_uiAllElements.size(),m_uiMaxDepth,m_uiMaxDepth,ROOT_ROTATION);

//#ifdef DEBUG_MESH_GENERATION
        std::vector<ot::TreeNode> notFoundKeys;
        std::vector<ot::TreeNode> foundResult;
//#endif

        m_uiKeysPtr=&(*(m_uiGhostKeys.begin()));
        // Note : Since the ghost elements are not complete it is not required to find all the keys in the ghost.
        for (unsigned int k = 0; k < m_uiGhostKeys.size(); k++) {
            ownerList = m_uiKeysPtr[k].getOwnerList();
            if((OCT_FOUND & m_uiKeysPtr[k].getFlag())) {
                stencilIndexDirection = m_uiKeysPtr[k].getStencilIndexDirectionList();
                result = m_uiKeysPtr[k].getSearchResult();
                if (ownerList->size()) assert(m_uiAllElements[result].isAncestor(m_uiGhostKeys[k]) || m_uiAllElements[result] ==  m_uiGhostKeys[k]); // To check the result found in the treeSearch is correct or not.
                foundResult.push_back(m_uiAllElements[result]);
                for (unsigned int w = 0; w < ownerList->size(); w++) {
                    dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                    m_uiE2EMapping[(((*ownerList)[w])) * m_uiNumDirections + dir] = result;
                    // Note : Following is done to enforce that the local elements that points to ghost elements has the inverse mapping.
                }

            }else
            {
                notFoundKeys.push_back(m_uiGhostKeys[k]);

            }

        }



        m_uiSendOctCountRound2 = new unsigned int[npes];
        m_uiRecvOctCountRound2 = new unsigned int[npes];

        m_uiSendOctOffsetRound2 = new unsigned int[npes];
        m_uiRecvOctOffsetRound2 = new unsigned int[npes];

        tmpSendElementIds.clear();
        m_uiSendBufferElement.clear();
        unsigned int elementLookup;
        std::set<unsigned int > * tmpSendEleIdR2=new std::set<unsigned int>[npes];
        std::pair<std::set<unsigned int>::iterator,bool> setHintUint;


        for(unsigned int p=0;p<npes;p++)
        {
            m_uiSendOctCountRound2[p]=0;
            for(unsigned int ele=m_uiSendOctOffsetRound1[p];ele<(m_uiSendOctOffsetRound1[p]+m_uiSendOctCountRound1[p]);ele++)
            {
               /* if(m_uiRank==25 && (m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)==432)
                {
                    std::vector<ot::TreeNode> cusE2ECheck;
                    cusE2ECheck.push_back(m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)]);

                    elementLookup=m_uiE2EMapping[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)*m_uiNumDirections+OCT_DIR_LEFT];
                    unsigned int elementLookup2=m_uiE2EMapping[elementLookup*m_uiNumDirections+OCT_DIR_BACK];
                    if((elementLookup!=LOOK_UP_TABLE_DEFAULT*//* (elementLookup>=m_uiElementLocalBegin) &&(elementLookup< m_uiElementLocalEnd)*//*) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel()))
                    {

                        cusE2ECheck.push_back(m_uiAllElements[elementLookup]);
                        if(elementLookup2!=LOOK_UP_TABLE_DEFAULT) cusE2ECheck.push_back(m_uiAllElements[elementLookup2]);
                    }

                    treeNodesTovtk(cusE2ECheck,(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),"cusE2ECheck432");
                }*/

                for(unsigned int dir=0;dir<m_uiNumDirections;dir++)
                {
                    elementLookup=m_uiE2EMapping[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)*m_uiNumDirections+dir];

                    if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                        setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                        if(setHintUint.second)
                        {
                            for(unsigned int pj=0;pj<npes;pj++)
                            {
                                if(pj==p) continue;
                                if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                                {
                                    tmpSendEleIdR2[pj].emplace(elementLookup);
                                }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                                {
                                    tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                                }
                            }
                        }
                    }

                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_LEFT,OCT_DIR_DOWN,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_LEFT,OCT_DIR_UP,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_LEFT,OCT_DIR_BACK,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_LEFT,OCT_DIR_FRONT,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }


                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_RIGHT,OCT_DIR_DOWN,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_RIGHT,OCT_DIR_UP,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }


                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_RIGHT,OCT_DIR_BACK,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }


                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_RIGHT,OCT_DIR_FRONT,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }


                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_DOWN,OCT_DIR_BACK,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_DOWN,OCT_DIR_FRONT,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_UP,OCT_DIR_BACK,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }
                }

                OCT_DIR_DIAGONAL_E2E((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin),OCT_DIR_UP,OCT_DIR_FRONT,elementLookup);
                if((elementLookup!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[elementLookup].getLevel()<=m_uiAllElements[(m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin)].getLevel())) {
                    setHintUint = tmpSendEleIdR2[p].emplace(elementLookup);
                    if(setHintUint.second)
                    {
                        for(unsigned int pj=0;pj<npes;pj++)
                        {
                            if(pj==p) continue;
                            if(tmpSendEleIdR2[pj].find((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin))!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace(elementLookup);
                            }else if(tmpSendEleIdR2[pj].find(elementLookup)!=tmpSendEleIdR2[pj].end())
                            {
                                tmpSendEleIdR2[pj].emplace((m_uiScatterMapElementRound1[ele]+m_uiElementLocalBegin));
                            }
                        }
                    }

                }


            }



        }

        m_uiSendBufferElement.clear();
        std::vector<unsigned int> common_data;
        for(unsigned int pi=0;pi<npes;pi++)
        {
            tmpSendElementIds.clear();
            tmpSendElementIds.insert(tmpSendEleIdR2[pi].begin(),tmpSendEleIdR2[pi].end());
            /*for(unsigned int pj=0;pj<npes;pj++)
            {

                if(pi==pj){
                    tmpSendElementIds.insert(tmpSendEleIdR2[pj].begin(),tmpSendEleIdR2[pj].end());
                }else
                {
                    set_intersection(tmpSendEleIdR2[pi].begin(),tmpSendEleIdR2[pi].end(),tmpSendEleIdR2[pj].begin(),tmpSendEleIdR2[pj].end(),std::back_inserter(common_data));
                    if(common_data.size())
                    {
                        tmpSendElementIds.insert(tmpSendEleIdR2[pj].begin(),tmpSendEleIdR2[pj].end());
                    }
                    common_data.clear();

                }

            }*/

            for(auto it=tmpSendElementIds.begin();it!=tmpSendElementIds.end();++it)
            {
                m_uiSendBufferElement.push_back(m_uiAllElements[*it]);
            }

            m_uiSendOctCountRound2[pi]=tmpSendElementIds.size();



        }

        delete[] tmpSendEleIdR2;

        //if(m_uiScatterMapElementRound2.size()!=0) {
            par::Mpi_Alltoall(m_uiSendOctCountRound2, m_uiRecvOctCountRound2, 1, comm);
            m_uiSendOctOffsetRound2[0] = 0;
            m_uiRecvOctOffsetRound2[0] = 0;

            omp_par::scan(m_uiSendOctCountRound2,m_uiSendOctOffsetRound2,npes);
            omp_par::scan(m_uiRecvOctCountRound2,m_uiRecvOctOffsetRound2,npes);


            m_uiGhostOctants.clear();
            m_uiGhostOctants.resize(m_uiRecvOctOffsetRound2[npes-1]+m_uiRecvOctCountRound2[npes-1]);
            assert(m_uiSendBufferElement.size()==(m_uiSendOctOffsetRound2[npes-1]+m_uiSendOctCountRound2[npes-1]));
            par::Mpi_Alltoallv(&(*(m_uiSendBufferElement.begin())),(int *)m_uiSendOctCountRound2,(int *)m_uiSendOctOffsetRound2,&(*(m_uiGhostOctants.begin())),(int *)m_uiRecvOctCountRound2,(int *)m_uiRecvOctOffsetRound2,comm);



//#ifdef DEBUG_MESH_GENERATION
            treeNodesTovtk(m_uiGhostOctants,rank,"ghostR2");

//#endif


            m_uiAllElements.insert(m_uiAllElements.end(),m_uiGhostOctants.begin(),m_uiGhostOctants.end());

            tmpNodes.clear();
            SFC::seqSort::SFC_treeSort(&(*(m_uiAllElements.begin())), m_uiAllElements.size(), tmpNodes, tmpNodes, tmpNodes, m_uiMaxDepth,m_uiMaxDepth, rootNode, 0, 1, TS_REMOVE_DUPLICATES);
            std::swap(m_uiAllElements, tmpNodes);
            tmpNodes.clear();

            assert(seq::test::isUniqueAndSorted(m_uiAllElements));
            m_uiGhostOctants.clear();

            m_uiElementPreGhostBegin = 0;
            m_uiElementPreGhostEnd = 0;
            m_uiElementLocalBegin = 0;
            m_uiElementLocalEnd = 0;
            m_uiElementPostGhostBegin = 0;
            m_uiElementPostGhostEnd = 0;

            for (unsigned int k = 0; k < m_uiAllElements.size(); k++) {
                if (m_uiAllElements[k] == localOct_min) {
                    m_uiElementLocalBegin = k;
                    break;
                }
            }

            for (unsigned int k = (m_uiAllElements.size() - 1); k > 0; k--) {
                if (m_uiAllElements[k] == localOct_max)
                    m_uiElementLocalEnd = k + 1;
            }

            m_uiElementPreGhostEnd = m_uiElementLocalBegin;
            m_uiElementPostGhostBegin = m_uiElementLocalEnd;
            m_uiElementPostGhostEnd = m_uiAllElements.size();

            m_uiNumLocalElements=(m_uiElementLocalEnd-m_uiElementLocalBegin);
            m_uiNumPreGhostElements=(m_uiElementPreGhostEnd-m_uiElementPreGhostBegin);
            m_uiNumPostGhostElements=(m_uiElementPostGhostEnd-m_uiElementPostGhostBegin);

            m_uiNumTotalElements=m_uiNumPreGhostElements+m_uiNumLocalElements+m_uiNumPostGhostElements;


            unsigned int round1GhostCount=0;


            m_uiGhostElementRound1Index.resize(ghostElementsRound1.size());

            for(unsigned int ele=m_uiElementPreGhostBegin;ele<m_uiElementPreGhostEnd;ele++)
            {
                if(m_uiAllElements[ele]==ghostElementsRound1[round1GhostCount])
                {
                    m_uiGhostElementRound1Index[round1GhostCount]=ele;
                    round1GhostCount++;
                }


            }

            for(unsigned int ele=m_uiElementPostGhostBegin;ele<m_uiElementPostGhostEnd;ele++)
            {
                if(m_uiAllElements[ele]==ghostElementsRound1[round1GhostCount])
                {
                    m_uiGhostElementRound1Index[round1GhostCount]=ele;
                    round1GhostCount++;
                }

            }

            ghostElementsRound1.clear();



            // E2E Mapping for the Round 2 Ghost Exchange.
            SFC::seqSearch::SFC_treeSearch(&(*(m_uiKeys.begin())), &(*(m_uiAllElements.begin())), 0, m_uiKeys.size(), 0,m_uiAllElements.size(), m_uiMaxDepth, m_uiMaxDepth, 0);

            m_uiKeysPtr=&(*(m_uiKeys.begin()));

            m_uiE2EMapping.clear();
            m_uiE2EMapping.resize(m_uiAllElements.size()*m_uiNumDirections,LOOK_UP_TABLE_DEFAULT);

            //*Neighbour list is made from
           // *  first x axis. (left to right)
           // *  second y axis. (down to up)
           // *  third z axis. (back to front)

            for (unsigned int k = 0; k < m_uiKeys.size(); k++) {
                ownerList = m_uiKeysPtr[k].getOwnerList();
                if(ownerList->size()) assert((OCT_FOUND & m_uiKeysPtr[k].getFlag()));// Note that all the keys should be found locally due to the fact that we have exchanged ghost elements.
                stencilIndexDirection = m_uiKeysPtr[k].getStencilIndexDirectionList();
                result = m_uiKeysPtr[k].getSearchResult();
                if(ownerList->size()) assert(m_uiAllElements[result].isAncestor(m_uiKeys[k]) || m_uiAllElements[result]==m_uiKeys[k]); // To check the result found in the treeSearch is correct or not.
                for (unsigned int w = 0; w < ownerList->size(); w++) {
                    dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                    //stencilIndex = (((*stencilIndexDirection)[w]) & (KS_MAX << 3u)) >> 3u;
                    m_uiE2EMapping[(((*ownerList)[w]) + m_uiElementLocalBegin)*m_uiNumDirections+dir] = result;
                    // Note : Following is done to enforce that the local elements that points to ghost elements has the inverse mapping.
                    if(result<m_uiElementLocalBegin || result>=m_uiElementLocalEnd)
                        m_uiE2EMapping[result*m_uiNumDirections+ (1u^dir)]=(((*ownerList)[w]) + m_uiElementLocalBegin); // Note This depends on the OCT_DIR numbering.


                }

            }

        //}



        assert(seq::test::checkE2EMapping(m_uiE2EMapping,m_uiAllElements,m_uiElementLocalBegin,m_uiElementLocalEnd,1,m_uiNumDirections));


#ifdef DEBUG_MESH_GENERATION
        /* if(!m_uiRank)
        {
            for(unsigned int ele=0;ele<m_uiE2HangingFaces.size();ele++)
            {
                printf("ele: %d value: %d \n",ele,m_uiE2HangingFaces[ele]);
            }
        }*/
        treeNodesTovtk(m_uiAllElements,rank,"m_uiAllElements");
        //std::cout<<"rank: "<<rank<<"pre begin: "<<m_uiElementPreGhostBegin<<" pre end: "<<m_uiElementPreGhostEnd<<" local begin: "<<m_uiElementLocalBegin<<" local end: "<<m_uiElementLocalEnd<<" post begin: "<<m_uiElementPostGhostBegin<<" post end: "<<m_uiElementPostGhostEnd<<std::endl;
#endif

        // Note: Note that m_uiAlllNodes need not to be sorted and contains duplicates globally.


#ifdef DEBUG_MESH_GENERATION
        std::vector<ot::TreeNode> missedKeys;
            for (unsigned int k = 0; k < m_uiKeys.size(); k++) {
                if (!(m_uiAllElements[result].isAncestor(m_uiKeys[k]) || m_uiAllElements[result]==m_uiKeys[k])) {
                    /*std::cout << "rank: " << rank << " key : " << m_uiKeys[k] << " not found!" << " Search index count: "
                              << searchResults.size() << std::endl;*/
                    missedKeys.push_back(m_uiKeys[k]);
                }
            }

        treeNodesTovtk(missedKeys,rank,"missedKeys");
#endif

        m_uiGhostKeys.clear();
        generateGhostElementSearchKeys();

        SFC::seqSearch::SFC_treeSearch(&(*(m_uiGhostKeys.begin())),&(*(m_uiAllElements.begin())),0,m_uiGhostKeys.size(),0,m_uiAllElements.size(),m_uiMaxDepth,m_uiMaxDepth,ROOT_ROTATION);

//#ifdef DEBUG_MESH_GENERATION
        //std::vector<ot::TreeNode> notFoundKeys;
        //std::vector<ot::TreeNode> foundResult;
        notFoundKeys.clear();
        foundResult.clear();
//#endif

        m_uiKeysPtr=&(*(m_uiGhostKeys.begin()));
        // Note : Since the ghost elements are not complete it is not required to find all the keys in the ghost.
        for (unsigned int k = 0; k < m_uiGhostKeys.size(); k++) {
            ownerList = m_uiKeysPtr[k].getOwnerList();
            if((OCT_FOUND & m_uiKeysPtr[k].getFlag())) {
                stencilIndexDirection = m_uiKeysPtr[k].getStencilIndexDirectionList();
                result = m_uiKeysPtr[k].getSearchResult();
                if (ownerList->size()) assert(m_uiAllElements[result].isAncestor(m_uiGhostKeys[k]) || m_uiAllElements[result] ==  m_uiGhostKeys[k]); // To check the result found in the treeSearch is correct or not.
                 foundResult.push_back(m_uiAllElements[result]);
                 for (unsigned int w = 0; w < ownerList->size(); w++) {
                    dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                    m_uiE2EMapping[(((*ownerList)[w])) * m_uiNumDirections + dir] = result;
                    // Note : Following is done to enforce that the local elements that points to ghost elements has the inverse mapping.
                }

            }else
            {
                   notFoundKeys.push_back(m_uiGhostKeys[k]);

            }

        }

#ifdef DEBUG_MESH_GENERATION
        treeNodesTovtk(notFoundKeys,rank,"notFoundKeys");
        treeNodesTovtk(m_uiGhostKeys,rank,"m_uiGhostKeys");
        treeNodesTovtk(foundResult,rank,"foundResult");
#endif
        if (!rank)  std::cout << "E2E mapping Ended" << std::endl;


    }



    void Mesh::buildE2EMap(std::vector<ot::TreeNode> &in)
    {


        addBoundaryNodesType1(in, m_uiPositiveBoundaryOctants, m_uiDim, m_uiMaxDepth);
        m_uiMaxDepth = m_uiMaxDepth + 1;

        std::swap(m_uiEmbeddedOctree, in);
        m_uiEmbeddedOctree.insert(m_uiEmbeddedOctree.end(), m_uiPositiveBoundaryOctants.begin(),m_uiPositiveBoundaryOctants.end());

        std::vector<ot::TreeNode> tmpNodes;
        ot::TreeNode rootNode(0, 0, 0, 0, m_uiDim, m_uiMaxDepth);
        // clear the in and positive boundary octants;
        m_uiPositiveBoundaryOctants.clear();
        in.clear();

        //std::cout<<"rank: "<<m_uiRank<<" par::sort begin: "<<m_uiMaxDepth<<" "<<m_uiDim<<" embedded octreeSize: "<<m_uiEmbeddedOctree.size()<<std::endl;
        SFC::seqSort::SFC_treeSort(&(*(m_uiEmbeddedOctree.begin())), m_uiEmbeddedOctree.size(), tmpNodes, tmpNodes, tmpNodes,m_uiMaxDepth, m_uiMaxDepth, rootNode, 0, 1, TS_REMOVE_DUPLICATES);

        std::swap(tmpNodes, m_uiEmbeddedOctree);
        tmpNodes.clear();

        generateSearchKeys(); // generates keys for sequential case.

        std::swap(m_uiAllElements,m_uiEmbeddedOctree);
        m_uiEmbeddedOctree.clear();

        //update the loop counters.
        m_uiElementPreGhostBegin=0;
        m_uiElementPreGhostEnd=0;
        m_uiElementLocalBegin=0;
        m_uiElementLocalEnd=m_uiAllElements.size();
        m_uiElementPostGhostBegin=m_uiElementLocalEnd;
        m_uiElementPostGhostEnd=m_uiElementLocalEnd;

        m_uiNumLocalElements=(m_uiElementLocalEnd-m_uiElementLocalBegin);
        m_uiNumPreGhostElements=(m_uiElementPreGhostEnd-m_uiElementPreGhostBegin);
        m_uiNumPostGhostElements=(m_uiElementPostGhostEnd-m_uiElementPostGhostBegin);

        m_uiNumTotalElements=m_uiNumPreGhostElements+m_uiNumLocalElements+m_uiNumPostGhostElements;



        //1b - allocate  & initialize E2E mapping.
        m_uiE2EMapping.resize(m_uiAllElements.size()*m_uiNumDirections,LOOK_UP_TABLE_DEFAULT);



        SFC::seqSearch::SFC_treeSearch(&(*(m_uiKeys.begin())), &(*(m_uiAllElements.begin())), 0, m_uiKeys.size(), 0, m_uiAllElements.size(), m_uiMaxDepth, m_uiMaxDepth, 0);

        std::vector<unsigned int> *ownerList;
        std::vector<uint8_t> *stencilIndexDirection;
        unsigned int result;
        unsigned int dir;
        //unsigned int stencilIndex;
        Key *m_uiKeysPtr=&(*(m_uiKeys.begin()));

        m_uiE2EMapping.resize(m_uiAllElements.size()*m_uiNumDirections,LOOK_UP_TABLE_DEFAULT);

        /**Neighbour list is made from
        *  first x axis. (left to right)
        *  second y axis. (down to up)
        *  third z axis. (back to front)**/

        for (unsigned int k = 0; k < m_uiKeys.size(); k++) {
            assert((OCT_FOUND & m_uiKeysPtr[k].getFlag()));// Note that all the keys should be found locally due to the fact that we have exchanged ghost elements.
            ownerList = m_uiKeysPtr[k].getOwnerList();
            stencilIndexDirection = m_uiKeysPtr[k].getStencilIndexDirectionList();
            result = m_uiKeysPtr[k].getSearchResult();
            assert(m_uiAllElements[result].isAncestor(m_uiKeys[k]) || m_uiAllElements[result]==m_uiKeys[k]); // To check the result found in the treeSearch is correct or not.
            for (unsigned int w = 0; w < ownerList->size(); w++) {
                dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                //stencilIndex = (((*stencilIndexDirection)[w]) & (KS_MAX << 3u)) >> 3u;
                //std::cout<<"dir: "<<dir<<" stencil Index: "<<stencilIndex<<"owner index: "<<(*ownerList)[w]<<std::endl;
                m_uiE2EMapping[(((*ownerList)[w]) + m_uiElementLocalBegin)*m_uiNumDirections+dir] = result;
                // Note : Following is done to enforce that the local elements that points to ghost elements has the inverse mapping.
                if(result<m_uiElementLocalBegin || result>=m_uiElementLocalEnd) {
                    m_uiE2EMapping[result * m_uiNumDirections + (1u ^ dir)] = (((*ownerList)[w]) +
                                                                               m_uiElementLocalBegin); // Note This depends on the OCT_DIR numbering.
                    assert(false);// for sequential case this cannot be true.
                }


            }

        }

        assert(seq::test::checkE2EMapping(m_uiE2EMapping,m_uiAllElements,m_uiElementPreGhostBegin,m_uiElementPostGhostEnd,1,m_uiNumDirections));
#ifdef DEBUG_MESH_GENERATION
        treeNodesTovtk(m_uiAllElements,rank,"m_uiAllElements");
        //std::cout<<"rank: "<<rank<<"pre begin: "<<m_uiElementPreGhostBegin<<" pre end: "<<m_uiElementPreGhostEnd<<" local begin: "<<m_uiElementLocalBegin<<" local end: "<<m_uiElementLocalEnd<<" post begin: "<<m_uiElementPostGhostBegin<<" post end: "<<m_uiElementPostGhostEnd<<std::endl;
#endif

        // Note: Note that m_uiAlllNodes need not to be sorted and contains duplicates globally.


#ifdef DEBUG_MESH_GENERATION
        std::vector<ot::TreeNode> missedKeys;
            for (unsigned int k = 0; k < m_uiKeys.size(); k++) {
                if (!(OCT_FOUND & m_uiKeys[k].getFlag())) {
                    std::cout << "rank: " << rank << " key : " << m_uiKeys[k] << " not found!" << " Search index count: "
                              << searchResults.size() << std::endl;
                    missedKeys.push_back(m_uiKeys[k]);
                }
            }

        treeNodesTovtk(missedKeys,rank,"missedKeys");
#endif

        std::cout << "Seq: E2E mapping Ended" << std::endl;

    }

    void Mesh::buildE2NMap() {


#ifdef DEBUG_E2N_MAPPING
        std::vector<ot::TreeNode> invalidatedPreGhost;
        std::vector<ot::TreeNode> invalidatedPostGhost;
        //if(!m_uiRank)  std::cout<<"E2E  rank : "<<m_uiRank<<std::endl;
        //if(!m_uiRank)
            for(unsigned int e=0;e<m_uiAllElements.size();e++)
            {
                //if(m_uiAllElements[e].getLevel()!=1) {
                //std::cout << "Element : "<<e<<" " << m_uiAllElements[e] << " : Node List :";
                for (unsigned int k = 0; k < m_uiNumDirections; k++) {

                  //  std::cout << " " << m_uiE2EMapping[e * m_uiNumDirections + k];
                   if(m_uiE2EMapping[e * m_uiNumDirections + k]!=LOOK_UP_TABLE_DEFAULT) assert(m_uiE2EMapping[e * m_uiNumDirections + k]<m_uiAllElements.size());

                }

                //std::cout << std::endl;
                //}

            }
#endif

        // update the E2E mapping with fake elements.
        unsigned int lookUp = 0;
        unsigned int lev1 = 0;
        unsigned int lev2 = 0;

        unsigned int child;
        unsigned int parent;

#ifdef DEBUG_E2N_MAPPING
        for(unsigned int ge=m_uiElementPreGhostBegin;ge<m_uiElementPreGhostEnd;ge++)
        {
            for(unsigned int dir=0;dir<m_uiNumDirections;dir++)
              if(m_uiE2EMapping[ge*m_uiNumDirections+dir]!=LOOK_UP_TABLE_DEFAULT)
                  assert((m_uiE2EMapping[ge*m_uiNumDirections+dir]>=m_uiElementLocalBegin) && (m_uiE2EMapping[ge*m_uiNumDirections+dir]<m_uiElementLocalEnd) );
        }

        for(unsigned int ge=m_uiElementPostGhostBegin;ge<m_uiElementPostGhostEnd;ge++)
        {
            for(unsigned int dir=0;dir<m_uiNumDirections;dir++)
                if(m_uiE2EMapping[ge*m_uiNumDirections+dir]!=LOOK_UP_TABLE_DEFAULT)
                    assert((m_uiE2EMapping[ge*m_uiNumDirections+dir]>=m_uiElementLocalBegin) && (m_uiE2EMapping[ge*m_uiNumDirections+dir]<m_uiElementLocalEnd) );
        }

#endif
        assert(m_uiNumTotalElements == m_uiAllElements.size());
        assert((m_uiElementPostGhostEnd - m_uiElementPreGhostBegin) > 0);
        assert(m_uiNumTotalElements == ((m_uiElementPostGhostEnd - m_uiElementPreGhostBegin)));

        m_uiE2NMapping_CG.resize(m_uiNumTotalElements * m_uiNpE);
        m_uiE2NMapping_DG.resize(m_uiNumTotalElements * m_uiNpE);

        // initialize the DG mapping. // this order is mandotory.
        for (unsigned int e = 0; e < (m_uiNumTotalElements); e++)
            for (unsigned int k = 0; k < (m_uiElementOrder + 1); k++) //z coordinate
                for (unsigned int j = 0; j < (m_uiElementOrder + 1); j++) // y coordinate
                    for (unsigned int i = 0; i < (m_uiElementOrder + 1); i++) // x coordinate
                        m_uiE2NMapping_CG[e * m_uiNpE + k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                          j * (m_uiElementOrder + 1) + i] =
                                e * m_uiNpE + k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                j * (m_uiElementOrder + 1) + i;


#ifdef DEBUG_E2N_MAPPING
        MPI_Barrier(MPI_COMM_WORLD);
        if (!m_uiRank) std::cout << "Invalid nodes removed " << std::endl;
        //treeNodesTovtk(invalidatedPreGhost,m_uiRank,"invalidPre");
        //treeNodesTovtk(invalidatedPostGhost,m_uiRank,"invalidPost");
#endif

        // 2. Removing the duplicate nodes from the mapping.
        unsigned int ownerIndexChild;
        unsigned int ownerIndexParent;

        unsigned int child_i,child_j,child_k;
        unsigned int parent_i,parent_j,parent_k;


#ifdef DEBUG_E2N_MAPPING
        std::vector<ot::TreeNode> cusE2ECheck;

        if(!m_uiRank && m_uiNpes>1){

            unsigned int eleID=2;
            //cusE2ECheck.push_back(m_uiAllElements[eleID]);
            for(unsigned int dir=0;dir<(m_uiNumDirections);dir++){
                if(m_uiE2EMapping[eleID*m_uiNumDirections+dir]!=LOOK_UP_TABLE_DEFAULT)
                    cusE2ECheck.push_back(m_uiAllElements[m_uiE2EMapping[eleID*m_uiNumDirections+dir]]);
            }

            treeNodesTovtk(cusE2ECheck,m_uiRank,"cusE2ECheck");
        }

        unsigned int eleVtk=2;
        unsigned int eleVtkAt=1;

        unsigned  int lookUp1,lookUp2,lookUp3,lookUp4;
        unsigned int edgeOwner;
        unsigned int cornerNodeOwner;

        unsigned int cornerNodeOwnerIndex;
        unsigned int cornerNodeChildIndex;
#endif

        bool parentChildLevEqual=false;
        std::vector<unsigned int> faceChildIndex; // indices that are being updated for a face
        std::vector<unsigned int> faceOwnerIndex; // indices that are being used for updates. for a face

        std::vector<unsigned int> edgeChildIndex; // indices that are being updated for an edge
        std::vector<unsigned int> edgeOwnerIndex; // indices that are being used for updates. for an edge

        for (unsigned int e = m_uiElementPreGhostBegin; e < (m_uiElementPostGhostEnd); e++) {



            // 1. All local nodes for a given element is automatically mapped for a given element by construction of the DG indexing.

            //2. Map internal nodes on each face,  with the corresponding face.
            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_LEFT];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    parentChildLevEqual=true;
                    lev1 = e;
                    lev2 = lookUp;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

               if(child==e){

                   assert(parent==lookUp);

                   faceNodesIndex(child,OCT_DIR_LEFT,faceChildIndex,true);
                   faceNodesIndex(parent,OCT_DIR_RIGHT,faceOwnerIndex,true);

                   assert(faceChildIndex.size()==faceOwnerIndex.size());

                   for(unsigned int index=0;index<faceChildIndex.size();index++)
                       m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];


                   OCT_DIR_LEFT_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the left face


               }


            }


            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_RIGHT];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    lev1 = e;
                    lev2 = lookUp;
                    parentChildLevEqual=true;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

               if(child==e)
                {
                    assert(parent==lookUp);
                    faceNodesIndex(child,OCT_DIR_RIGHT,faceChildIndex,true);
                    faceNodesIndex(parent,OCT_DIR_LEFT,faceOwnerIndex,true);

                    assert(faceChildIndex.size()==faceOwnerIndex.size());

                    for(unsigned int index=0;index<faceChildIndex.size();index++)
                        m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];


                    OCT_DIR_RIGHT_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the right face



                }


            }

            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_DOWN];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    lev1 = e;
                    lev2 = lookUp;
                    parentChildLevEqual=true;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

                if(child==e)
                {
                    assert(parent==lookUp);
                    faceNodesIndex(child,OCT_DIR_DOWN,faceChildIndex,true);
                    faceNodesIndex(parent,OCT_DIR_UP,faceOwnerIndex,true);

                    assert(faceChildIndex.size()==faceOwnerIndex.size());

                    for(unsigned int index=0;index<faceChildIndex.size();index++)
                        m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];



                    OCT_DIR_DOWN_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the DOWN face

                }


            }

            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_UP];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    lev1 = e;
                    lev2 = lookUp;
                    parentChildLevEqual=true;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

                if(child==e)
                {
                    assert(parent==lookUp);
                    faceNodesIndex(child,OCT_DIR_UP,faceChildIndex,true);
                    faceNodesIndex(parent,OCT_DIR_DOWN,faceOwnerIndex,true);

                    assert(faceChildIndex.size()==faceOwnerIndex.size());

                    for(unsigned int index=0;index<faceChildIndex.size();index++)
                        m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];


                    OCT_DIR_UP_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the UP face



                }



            }


            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_BACK];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    lev1 = e;
                    lev2 = lookUp;
                    parentChildLevEqual=true;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

                if(child==e)
                {
                    assert(parent==lookUp);
                    faceNodesIndex(child,OCT_DIR_BACK,faceChildIndex,true);
                    faceNodesIndex(parent,OCT_DIR_FRONT,faceOwnerIndex,true);

                    assert(faceChildIndex.size()==faceOwnerIndex.size());

                    for(unsigned int index=0;index<faceChildIndex.size();index++)
                        m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];



                    OCT_DIR_BACK_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the back face


                }


            }


            parentChildLevEqual=false;
            lookUp = m_uiE2EMapping[e * m_uiNumDirections + OCT_DIR_FRONT];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                lev1 = m_uiAllElements[e].getLevel();
                lev2 = m_uiAllElements[lookUp].getLevel();
                if (lev1 == lev2) {
                    lev1 = e;
                    lev2 = lookUp;
                    parentChildLevEqual=true;
                    assert(e != lookUp);
                }

                child = ((lev1 > lev2) ? e : lookUp);
                parent = ((lev1 < lev2) ? e : lookUp);

                if(child==e)
                {

                    assert(parent==lookUp);
                    faceNodesIndex(child,OCT_DIR_FRONT,faceChildIndex,true);
                    faceNodesIndex(parent,OCT_DIR_BACK,faceOwnerIndex,true);

                    assert(faceChildIndex.size()==faceOwnerIndex.size());

                    for(unsigned int index=0;index<faceChildIndex.size();index++)
                        m_uiE2NMapping_CG[faceChildIndex[index]]=m_uiE2NMapping_CG[faceOwnerIndex[index]];


                    OCT_DIR_FRONT_INTERNAL_EDGE_MAP(child,parent,parentChildLevEqual,edgeChildIndex,edgeOwnerIndex); // maps all the edges in the front face



                }


            }


            CORNER_NODE_MAP(e);

           /* if(m_uiRank==1 && e==30)
            {
                std::cout<<"m_uiRank: "<<m_uiRank<<" : e: "<<m_uiAllElements[e]<<" E2N: ";
                for(unsigned int node=0;node<m_uiNpE;node++)
                {
                 std::cout <<" " <<m_uiE2NMapping_CG[e*m_uiNpE+node];
                }
                std::cout<<std::endl;

            }*/



        }


#ifdef DEBUG_E2N_MAPPING
         MPI_Barrier(MPI_COMM_WORLD);
         unsigned int eleIndex;
         if(m_uiRank==0)  std::cout<<"E2N  rank : "<<m_uiRank<<std::endl;
         if(m_uiRank==0)
         for(unsigned int e=0;e<m_uiAllElements.size();e++)
         {
             //if(m_uiAllElements[e].getLevel()!=1) {
                 std::cout << "Element : "<<e<<" " << m_uiAllElements[e] << " : Node List :";
                 for (unsigned int k = 0; k < m_uiNpE; k++) {

                     std::cout << " " << m_uiE2NMapping_CG[e * m_uiNpE + k];
          }

                 std::cout << std::endl;
             //}

         }

#endif


        //assert(seq::test::checkE2NMapping(m_uiE2EMapping, m_uiE2NMapping_CG,m_uiAllElements,m_uiNumDirections,m_uiElementOrder));
        std::vector<unsigned int > E2N_DG_Sorted(m_uiE2NMapping_CG);


// 3. Update DG indexing with CG indexing.
        std::sort( E2N_DG_Sorted.begin(), E2N_DG_Sorted.end() );
        E2N_DG_Sorted.erase( std::unique( E2N_DG_Sorted.begin(), E2N_DG_Sorted.end() ), E2N_DG_Sorted.end() );

        unsigned int owner1,ii_x1,jj_y1,kk_z1;
        unsigned int nsz;

        std::vector<unsigned int> E2N_DG_Unique;
        //std::set<ot::TreeNode,OctreeComp<ot::TreeNode>> dg_nodes;
        std::map<ot::TreeNode, unsigned int > dg_rmDupMap;
        //std::map<unsigned int , unsigned int > dg_rmDupIndexMap;
        std::pair<std::map<ot::TreeNode, unsigned int >::iterator,bool> dgNodeHint;
        //std::map<unsigned  int, unsigned int >::iterator tmpIter;

        for(unsigned int index=0;index<E2N_DG_Sorted.size();index++)
        {
            dg2eijk(E2N_DG_Sorted[index],owner1,ii_x1,jj_y1,kk_z1);
            assert(owner1<m_uiAllElements.size());
            nsz=1u<<(m_uiMaxDepth-m_uiAllElements[owner1].getLevel());
            dgNodeHint=dg_rmDupMap.emplace(ot::TreeNode((m_uiAllElements[owner1].getX()+ii_x1*nsz/m_uiElementOrder),(m_uiAllElements[owner1].getY()+jj_y1*nsz/m_uiElementOrder),(m_uiAllElements[owner1].getZ()+kk_z1*nsz/m_uiElementOrder),m_uiMaxDepth,m_uiDim,m_uiMaxDepth),E2N_DG_Sorted[index]);
            if((!dgNodeHint.second) && (E2N_DG_Sorted[index]!=(*(dgNodeHint.first)).second))
            {
                std::replace(m_uiE2NMapping_CG.begin(),m_uiE2NMapping_CG.end(),E2N_DG_Sorted[index],(*(dgNodeHint.first)).second);
                //dg_rmDupIndexMap.emplace(E2N_DG_Sorted[index],(*(dgNodeHint.first)).second);
                //E2N_DG_Sorted.erase(E2N_DG_Sorted.begin()+index);
            }else if(dgNodeHint.second)
            {
                E2N_DG_Unique.push_back(E2N_DG_Sorted[index]);

            }

        }
        E2N_DG_Sorted.clear();
        std::swap(E2N_DG_Sorted,E2N_DG_Unique);
        E2N_DG_Unique.clear();

       /* if(m_uiRank==1 )
        {
            std::cout<<"m_uiRank: "<<m_uiRank<<" : e: "<<m_uiAllElements[30]<<" E2N after RD: ";
            for(unsigned int node=0;node<m_uiNpE;node++)
            {
                std::cout <<" " <<m_uiE2NMapping_CG[30*m_uiNpE+node];
            }
            std::cout<<std::endl;

        }*/




#ifdef DEBUG_E2N_MAPPING
        //MPI_Barrier(MPI_COMM_WORLD);
        if(!m_uiRank) std::cout<<"m_uiRank: "<<m_uiRank<<"Number of actual nodes: "<<(E2N_DG_Sorted.size())<<std::endl;
#endif

        m_uiNodePreGhostBegin=UINT_MAX;
        m_uiNodeLocalBegin=UINT_MAX;
        m_uiNodePostGhostBegin=UINT_MAX;

        unsigned int preOwner=UINT_MAX;
        unsigned int localOwner=UINT_MAX;
        unsigned int postOwner=UINT_MAX;




        for(unsigned int e=0;e<((m_uiElementPostGhostEnd-m_uiElementPreGhostBegin));e++)
        {
            unsigned int tmpIndex;
            for(unsigned int k=0;k<m_uiNpE;k++)
            {

                tmpIndex = (m_uiE2NMapping_CG[e * m_uiNpE + k]/m_uiNpE);
                assert(tmpIndex==(((m_uiE2NMapping_CG[e * m_uiNpE + k]) / (m_uiElementOrder + 1)) /
                                                                              (m_uiElementOrder + 1)) / (m_uiElementOrder + 1));
                if ((tmpIndex >= m_uiElementPreGhostBegin) && (tmpIndex < m_uiElementPreGhostEnd) && /*(preOwner>=(m_uiE2NMapping_CG[e * m_uiNpE + k])/m_uiNpE) &&*/ (m_uiNodePreGhostBegin>m_uiE2NMapping_CG[e * m_uiNpE + k])){
                    preOwner = m_uiE2NMapping_CG[e * m_uiNpE + k]/m_uiNpE;
                    m_uiNodePreGhostBegin = m_uiE2NMapping_CG[e * m_uiNpE + k];
                }

                if ((tmpIndex >= m_uiElementLocalBegin) && (tmpIndex < m_uiElementLocalEnd) && /*(localOwner >=(m_uiE2NMapping_CG[e * m_uiNpE + k])/m_uiNpE) &&*/ (m_uiNodeLocalBegin > m_uiE2NMapping_CG[e * m_uiNpE + k])) {
                    localOwner = m_uiE2NMapping_CG[e * m_uiNpE + k]/m_uiNpE;
                    m_uiNodeLocalBegin = m_uiE2NMapping_CG[e * m_uiNpE + k];
                }

                if ((tmpIndex >= m_uiElementPostGhostBegin) && (tmpIndex < m_uiElementPostGhostEnd) && /*(postOwner >=(m_uiE2NMapping_CG[e * m_uiNpE + k])/m_uiNpE) &&*/ (m_uiNodePostGhostBegin>m_uiE2NMapping_CG[e * m_uiNpE + k])) {
                    postOwner = m_uiE2NMapping_CG[e * m_uiNpE + k]/m_uiNpE;
                    m_uiNodePostGhostBegin = m_uiE2NMapping_CG[e * m_uiNpE + k];
                }

            }

        }


        assert(m_uiNodeLocalBegin!=UINT_MAX); // local node begin should be found.
        m_uiNodeLocalBegin=(std::lower_bound(E2N_DG_Sorted.begin(),E2N_DG_Sorted.end(),m_uiNodeLocalBegin)-E2N_DG_Sorted.begin());
        if(m_uiNodePreGhostBegin==UINT_MAX) {
            m_uiNodePreGhostBegin=0;
            m_uiNodePreGhostEnd=0;
            assert(m_uiNodeLocalBegin==0);
        }
        else{
            m_uiNodePreGhostBegin=(std::lower_bound(E2N_DG_Sorted.begin(),E2N_DG_Sorted.end(),m_uiNodePreGhostBegin)-E2N_DG_Sorted.begin());
            m_uiNodePreGhostEnd=m_uiNodeLocalBegin;
        }

        if(m_uiNodePostGhostBegin==UINT_MAX) {
            m_uiNodeLocalEnd=E2N_DG_Sorted.size(); //E2N_DG_Sorted.back();
            m_uiNodePostGhostBegin=m_uiNodeLocalEnd;
            m_uiNodePostGhostEnd=m_uiNodeLocalEnd;
        }
        else
        {
            m_uiNodePostGhostBegin=(std::lower_bound(E2N_DG_Sorted.begin(),E2N_DG_Sorted.end(),m_uiNodePostGhostBegin)-E2N_DG_Sorted.begin());
            m_uiNodeLocalEnd=m_uiNodePostGhostBegin;
            m_uiNodePostGhostEnd=E2N_DG_Sorted.size(); //E2N_DG_Sorted.back();

        }







        //std::cout<<"Total valid Nodes: "<<E2N_DG_Sorted.size()<<std::endl;
        m_uiNumActualNodes=E2N_DG_Sorted.size();
        //m_uiE2NMapping_CG=new unsigned int [m_uiE2NMapping_CG.size()];
        unsigned int dgIndex=0;
        unsigned int cgIndex=0;
        for(unsigned int e=0;e<((m_uiElementPostGhostEnd-m_uiElementPreGhostBegin));e++)
        {
           for(unsigned int k=0;k<(m_uiElementOrder+1);k++)
               for(unsigned int j=0;j<(m_uiElementOrder+1);j++)
                   for(unsigned int i=0;i<(m_uiElementOrder+1);i++)
            {

                    dgIndex = m_uiE2NMapping_CG[e * m_uiNpE + k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i];
                    cgIndex = (std::lower_bound(E2N_DG_Sorted.begin(), E2N_DG_Sorted.end(), dgIndex) -
                               E2N_DG_Sorted.begin());
                    assert(cgIndex < E2N_DG_Sorted.size());
                    m_uiE2NMapping_CG[e * m_uiNpE + k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                      j * (m_uiElementOrder + 1) + i] = cgIndex;

                    m_uiE2NMapping_DG[e * m_uiNpE + k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                  j * (m_uiElementOrder + 1) + i]=E2N_DG_Sorted[cgIndex];


            }
        }

        E2N_DG_Sorted.clear();




#ifdef DEBUG_E2N_MAPPING
        MPI_Barrier(MPI_COMM_WORLD);
        if(m_uiRank) std::cout<<" DG to CG index updated "<<std::endl;
#endif




      /*  unsigned int eleIndex;
        //if(!m_uiRank)  std::cout<<"E2N  rank : "<<m_uiRank<<std::endl;
        //if(!m_uiRank)
            for(unsigned int e=0;e<m_uiAllElements.size();e++)
            {
                //if(m_uiAllElements[e].getLevel()!=1) {
                std::cout << "rank: "<<m_uiRank<<" Element : "<<e<<" " << m_uiAllElements[e] << " : Node List :";
                for (unsigned int k = 0; k < m_uiNpE; k++) {

                    std::cout << " " << m_uiE2NMapping_CG[e * m_uiNpE + k];

                }

                std::cout << std::endl;
                //}

            }*/
//--------------------------------------------------------PRINT THE E2N MAP------------------------------------------------------------------------------------
        /*for(unsigned int w=0;w<m_uiE2NMapping_CG.size();w++)
            std::cout<<"w: "<<w<<" -> : "<<m_uiE2NMapping_CG[w]<<std::endl;*/

//--------------------------------------------------------PRINT THE E2N MAP------------------------------------------------------------------------------------


// 4. Generate Fake elments for the wavelet code.

     /*   std::set<ot::TreeNode, OctreeComp<ot::TreeNode> > fakeElements;
        std::pair<std::set<ot::TreeNode, OctreeComp<ot::TreeNode> >::iterator, bool> hint;
        std::pair<std::set<Key, OctreeComp<Key> >::iterator, bool> hintKey;
        std::set<Key,OctreeComp<Key>> fakeKeys;

        unsigned int eleSz;
        unsigned int dmax=(1u<<(m_uiMaxDepth-1))+1;
        for(unsigned int e=0;e<m_uiNumTotalElements;e++)
        {
           lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_LEFT];
           if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
               eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
               assert(m_uiAllElements[e].getX()>=eleSz);
               hint = fakeElements.emplace(ot::TreeNode((m_uiAllElements[e].getX()-eleSz),m_uiAllElements[e].getY(),m_uiAllElements[e].getZ(),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));
               hintKey = fakeKeys.emplace(Key((m_uiAllElements[e].getX() - eleSz), m_uiAllElements[e].getY(), m_uiAllElements[e].getZ(),m_uiMaxDepth , m_uiDim, m_uiMaxDepth));
               hintKey.first->addOwner(e);
               hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_LEFT);
           }

            lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_RIGHT];
            if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
                eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
                assert((m_uiAllElements[e].getX()+eleSz) <dmax);
                hint = fakeElements.emplace(ot::TreeNode((m_uiAllElements[e].getX()+eleSz),m_uiAllElements[e].getY(),m_uiAllElements[e].getZ(),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));
                hintKey = fakeKeys.emplace(Key((m_uiAllElements[e].getX() +eleSz), m_uiAllElements[e].getY(), m_uiAllElements[e].getZ(), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hintKey.first->addOwner(e);
                hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_RIGHT);
            }


            lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_DOWN];
            if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
                eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
                assert(m_uiAllElements[e].getY()>=eleSz);
                hint = fakeElements.emplace(ot::TreeNode(m_uiAllElements[e].getX(),(m_uiAllElements[e].getY()-eleSz),m_uiAllElements[e].getZ(),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));

                hintKey = fakeKeys.emplace(Key(m_uiAllElements[e].getX(), (m_uiAllElements[e].getY()-eleSz), m_uiAllElements[e].getZ(), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hintKey.first->addOwner(e);
                hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_DOWN);

            }

            lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_UP];
            if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
                eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
                assert((m_uiAllElements[e].getY()+eleSz) <dmax);
                hint = fakeElements.emplace(ot::TreeNode(m_uiAllElements[e].getX(),(m_uiAllElements[e].getY()+eleSz),m_uiAllElements[e].getZ(),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));
                hintKey = fakeKeys.emplace(Key(m_uiAllElements[e].getX(), (m_uiAllElements[e].getY()+eleSz), m_uiAllElements[e].getZ(), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hintKey.first->addOwner(e);
                hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_UP);

            }

            lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_BACK];
            if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
                eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
                assert(m_uiAllElements[e].getZ()>=eleSz);
                hint = fakeElements.emplace(ot::TreeNode(m_uiAllElements[e].getX(),m_uiAllElements[e].getY(),(m_uiAllElements[e].getZ()-eleSz),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));
                hintKey = fakeKeys.emplace(Key(m_uiAllElements[e].getX(), m_uiAllElements[e].getY(), (m_uiAllElements[e].getZ()-eleSz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hintKey.first->addOwner(e);
                hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_BACK);

            }

            lookUp=m_uiE2EMapping[e*m_uiNumDirections+OCT_DIR_FRONT];
            if((lookUp!=LOOK_UP_TABLE_DEFAULT) && (m_uiAllElements[lookUp].getLevel()>m_uiAllElements[e].getLevel())) {
                eleSz=1u<<(m_uiMaxDepth-m_uiAllElements[e].getLevel());
                assert((m_uiAllElements[e].getZ()+eleSz) <dmax);
                hint = fakeElements.emplace(ot::TreeNode(m_uiAllElements[e].getX(),m_uiAllElements[e].getY(),(m_uiAllElements[e].getZ()+eleSz),m_uiAllElements[e].getLevel(),m_uiDim,m_uiMaxDepth));
                hintKey = fakeKeys.emplace(Key(m_uiAllElements[e].getX(), m_uiAllElements[e].getY(), (m_uiAllElements[e].getZ()+eleSz), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
                hintKey.first->addOwner(e);
                hintKey.first->addStencilIndexAndDirection(0,OCT_DIR_FRONT);

            }


        }*/


#ifdef DEBUG_E2N_MAPPING
        /*Note: @milinda if number of fake elements is zero program grashes. Fix this later. */
        MPI_Barrier(MPI_COMM_WORLD);
        if(m_uiRank) std::cout<<" Fake element generation complete. "<<std::endl;
        if(m_uiRank)std::cout<<"fake element size: "<< fakeElements.size()<<std::endl;
#endif
        /*std::vector<ot::TreeNode> fakeElements_vec(fakeElements.begin(),fakeElements.end());
        std::vector<Key> fakeKeys_vec(fakeKeys.begin(),fakeKeys.end());
        fakeKeys.clear();
        fakeElements.clear();

        std::vector<ot::TreeNode> fakeElementsSorted;
        ot::TreeNode root(m_uiDim,m_uiMaxDepth);
        // Note: You should NOT remove duplicates here. because fake elements that are generated can have embedded elements.
        SFC::seqSort::SFC_treeSort(&(*(fakeElements_vec.begin())),fakeElements_vec.size(),fakeElementsSorted,fakeElementsSorted,fakeElementsSorted,m_uiMaxDepth,m_uiMaxDepth,root,ROOT_ROTATION,1,TS_SORT_ONLY);
        SFC::seqSearch::SFC_treeSearch(&(*(fakeKeys_vec.begin())),&(*(fakeElements_vec.begin())),0,fakeKeys_vec.size(),0,fakeElements_vec.size(),m_uiMaxDepth,m_uiMaxDepth,ROOT_ROTATION);

        // 5. Merge fake elements at the end of the m_uiAllElements.
        m_uiAllElements.resize(m_uiNumTotalElements+fakeElements_vec.size());
        m_uiNumTotalFakeElements=fakeElements_vec.size();

        for(unsigned int fe=0;fe<fakeElements_vec.size();fe++)
            m_uiAllElements[fe+m_uiElementPostGhostEnd]=fakeElements_vec[fe];

        std::vector<unsigned  int > fakeElement2Node_CG(fakeElements_vec.size()*m_uiNpE);
        for(unsigned int fe=0;fe<fakeElements_vec.size();fe++)
        for(unsigned int k=0;k<(m_uiElementOrder+1);k++)
            for(unsigned int j=0;j<(m_uiElementOrder+1);j++)
                for(unsigned int i=0;i<(m_uiElementOrder+1);i++)
                    fakeElement2Node_CG[fe*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i]=k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i+m_uiNumActualNodes;


        //6. Update the old E2E mapping with fake element indices.
        std::vector<unsigned int> *ownerList;
        std::vector<uint8_t> *stencilIndexDirection;
        unsigned int result;
        unsigned int dir;
        //Note that fake elment indexing will go after end on m_uiPostGhostElements.
        for (unsigned int fk = 0; fk < fakeKeys_vec.size(); fk++) {// $
            assert((OCT_FOUND & fakeKeys_vec[fk].getFlag())); // all keys should be found.

            if(!((OCT_FOUND & fakeKeys_vec[fk].getFlag())))
                std::cout<<" key not found. : "<<fk<<" :: "<<fakeKeys_vec[fk]<<std::endl;
            ownerList = fakeKeys_vec[fk].getOwnerList();
            stencilIndexDirection = fakeKeys_vec[fk].getStencilIndexDirectionList();
            result = fakeKeys_vec[fk].getSearchResult();
            for (unsigned int w = 0; w < ownerList->size(); w++) {
                dir = ((*stencilIndexDirection)[w]) & KEY_DIR_OFFSET;
                //stencilIndex = (((*stencilIndexDirection)[w]) & (KS_MAX << 3u)) >> 3u;
                //std::cout<<"dir: "<<dir<<" stencil Index: "<<stencilIndex<<std::endl;
                m_uiE2EMapping[((*ownerList)[w])*m_uiNumDirections + dir ] = (result + m_uiElementPostGhostEnd);
                //std::cout<<"updating fakeElement e2N index : "<<result<<" direction : "<<dir<<" from owner : "<<((*ownerList)[w])<<std::endl;
                // updating the E2N mapping of fake elements at the common face shared with the actual element.
            }

            //std::cout<<"key:"<<fakeKeys_vec[fk]<<" found: "<<fakeElements_vec[result]<<std::endl;
            assert(fakeElements_vec[result].isAncestor(fakeKeys_vec[fk]) || fakeElements_vec[result]==fakeKeys_vec[fk]);
        }*/

        // initialize fake element counters.
        m_uiFElementPreGhostBegin=m_uiElementPostGhostEnd;
        m_uiFElementPreGhostEnd=m_uiFElementPreGhostBegin;

        m_uiFElementLocalBegin=m_uiFElementPreGhostEnd;
        m_uiFElementLocalEnd=m_uiFElementLocalBegin;

        m_uiFElementPostGhostBegin=m_uiFElementLocalEnd;
        m_uiFElementPostGhostEnd=m_uiFElementPostGhostBegin;


       /* if(fakeElements_vec.size()) {

            ot::TreeNode *fakeElement_ptr = &(*(fakeElements_vec.begin()));
            ot::TreeNode *actualElement_ptr = &(*(m_uiAllElements.begin()));
            unsigned int fakeElCount = 0;
            unsigned int actualElCount = m_uiElementPreGhostBegin;
            unsigned int nodeFactor;

            int xIndex[(m_uiElementOrder + 1)];
            int yIndex[(m_uiElementOrder + 1)];
            int zIndex[(m_uiElementOrder + 1)];
            unsigned int actualReset = 0;
            // 7. Find acutal elements inside fake elements and perform the E2N update.
            while ((actualElCount < m_uiElementPostGhostEnd) && fakeElCount < fakeElements_vec.size()) {
                //actualReset=actualElCount;
                while ((actualElCount < m_uiElementPostGhostEnd) &&
                       (!fakeElement_ptr[fakeElCount].isAncestor(actualElement_ptr[actualElCount])))
                    actualElCount++;
                //std::cout<<"fakeElement e2N index : "<<fakeElCount<<" is an ancesstor to : "<<actualElCount<<std::endl;
                // actualElCont is an a desendent from fakeElCount.
                if (computeOveralppingNodes(fakeElement_ptr[fakeElCount], actualElement_ptr[actualElCount], xIndex, yIndex,
                                            zIndex)) {
                    //std::cout<<"updating fakeElement e2N index : "<<fakeElCount<<" from owner : "<<actualElCount<<std::endl;
                    for (unsigned int k = 0; k < (m_uiElementOrder + 1); k++)
                        for (unsigned int j = 0; j < (m_uiElementOrder + 1); j++)
                            for (unsigned int i = 0; i < (m_uiElementOrder + 1); i++) {
//                                  std::cout<<" xIndex["<<i<<"]: "<<xIndex[i]<<" "<<std::endl;
//                                    std::cout<<" yIndex["<<j<<"]: "<<xIndex[j]<<" "<<std::endl;
//                                    std::cout<<" zIndex["<<k<<"]: "<<xIndex[k]<<" "<<std::endl;
                                if ((xIndex[i] >= 0 && yIndex[j] >= 0 && zIndex[k] >= 0) &&
                                    (xIndex[i] < (m_uiElementOrder + 1) && yIndex[j] < (m_uiElementOrder + 1) &&
                                     zIndex[k] < (m_uiElementOrder + 1))) {
                                    if (m_uiE2NMapping_CG[actualElCount * m_uiNpE +
                                                          k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                                          j * (m_uiElementOrder + 1) + i] !=
                                        LOOK_UP_TABLE_DEFAULT) { // to check whether FE2N is not updated from a invalidated ghost node.
                                        fakeElement2Node_CG[fakeElCount * m_uiNpE +
                                                            zIndex[k] * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                                            yIndex[j] * (m_uiElementOrder + 1) +
                                                            xIndex[i]] = m_uiE2NMapping_CG[
                                                actualElCount * m_uiNpE +
                                                k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                                j * (m_uiElementOrder + 1) + i];
                                    }
                                }
                            }


                }


                actualElCount++;
                if ((actualElCount < m_uiElementPostGhostEnd) &&
                    (!fakeElement_ptr[fakeElCount].isAncestor(actualElement_ptr[actualElCount]))) {
                    if ((fakeElCount + 1) < fakeElements_vec.size() &&
                        fakeElement_ptr[fakeElCount].isAncestor(fakeElement_ptr[fakeElCount + 1]))
                        actualElCount = m_uiElementPreGhostBegin; // need to reset the counter.

                    fakeElCount++;

                }

            }


            bool localFakeEl=true;
            for(unsigned int fe=m_uiFElementPreGhostBegin;fe<m_uiAllElements.size();fe++)
            {
                localFakeEl=true;
                for(unsigned int w=0;w<m_uiNpE;w++)
                {
                    if(fakeElement2Node_CG[(fe-m_uiFElementPreGhostBegin)*m_uiNpE+w]!=LOOK_UP_TABLE_DEFAULT && (fakeElement2Node_CG[(fe-m_uiFElementPreGhostBegin)*m_uiNpE+w]>=m_uiNumActualNodes))
                        localFakeEl=false;
                }
                //std::cout<<"rank: "<<m_uiRank<<" fe: "<<fe<<" Local Element : "<<localFakeEl<<std::endl;
                if(localFakeEl)
                {
                    m_uiFElementLocalBegin=fe;
                    m_uiFElementPreGhostEnd=m_uiFElementLocalBegin;
                    break;
                }

            }

            bool postFakeEl=false;
            for(unsigned int fe=(m_uiFElementLocalBegin+1);fe<m_uiAllElements.size();fe++)
            {
                postFakeEl=false;
                for(unsigned int w=0;w<m_uiNpE;w++)
                {
                    if(fakeElement2Node_CG[(fe-m_uiFElementPreGhostBegin)*m_uiNpE+w]!=LOOK_UP_TABLE_DEFAULT && (fakeElement2Node_CG[(fe-m_uiFElementPreGhostBegin)*m_uiNpE+w]>=m_uiNumActualNodes)) {
                        postFakeEl = true;
                        break;
                    }
                }
                //std::cout<<"rank: "<<m_uiRank<<" fe: "<<fe<<" post Element : "<<postFakeEl<<std::endl;
                if(postFakeEl)
                {
                    m_uiFElementPostGhostBegin=fe;
                    m_uiFElementLocalEnd=m_uiFElementPostGhostBegin;
                    m_uiFElementPostGhostEnd=m_uiAllElements.size();
                    break;
                }

            }
            if(!postFakeEl) // this implies that no post fake element was found.
            {
                m_uiFElementLocalEnd=m_uiAllElements.size();
                m_uiFElementPostGhostBegin=m_uiFElementLocalEnd;
                m_uiFElementPostGhostEnd=m_uiFElementPostGhostBegin;
            }


        }

        assert(m_uiNumTotalFakeElements==((m_uiFElementPreGhostEnd-m_uiFElementPreGhostBegin)+(m_uiFElementLocalEnd-m_uiFElementLocalBegin)+(m_uiFElementPostGhostEnd-m_uiFElementPostGhostBegin)));


        m_uiNumFakeNodes=0;
        for(unsigned int fe=0;fe<fakeElements_vec.size();fe++)
            for(unsigned int w=0;w<m_uiNpE;w++)
                if(fakeElement2Node_CG[fe*m_uiNpE+w]>=m_uiNumActualNodes) m_uiNumFakeNodes++;*/

//#ifdef DEBUG_E2N_MAPPING
        //MPI_Barrier(MPI_COMM_WORLD);
        if(!m_uiRank) std::cout<<"[NODE] rank:  "<<m_uiRank<<" pre ( "<<m_uiNodePreGhostBegin<<", "<<m_uiNodePreGhostEnd<<") local ( "<<m_uiNodeLocalBegin<<", "<<m_uiNodeLocalEnd<<")"<<" post ("<<m_uiNodePostGhostBegin<<" , "<<m_uiNodePostGhostEnd<<")"<<std::endl;
        if(!m_uiRank) std::cout<<"[ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiElementPreGhostBegin<<", "<<m_uiElementPreGhostEnd<<") local ( "<<m_uiElementLocalBegin<<", "<<m_uiElementLocalEnd<<")"<<" post ("<<m_uiElementPostGhostBegin<<" , "<<m_uiElementPostGhostEnd<<")"<<std::endl;
        //if(!m_uiRank)std::cout<<"number of Fake Elements : "<<fakeElements_vec.size()<<std::endl;
        if(!m_uiRank)std::cout<<"number of FakeElement Nodes: "<<m_uiNumFakeNodes<<std::endl;
        if(!m_uiRank)std::cout<<"[Fake ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiFElementPreGhostBegin<<", "<<m_uiFElementPreGhostEnd<<") local ( "<<m_uiFElementLocalBegin<<", "<<m_uiFElementLocalEnd<<")"<<" post ("<<m_uiFElementPostGhostBegin<<" , "<<m_uiFElementPostGhostEnd<<")"<<std::endl;
        //MPI_Barrier(MPI_COMM_WORLD);
        if(m_uiRank) std::cout<<"[NODE] rank:  "<<m_uiRank<<" pre ( "<<m_uiNodePreGhostBegin<<", "<<m_uiNodePreGhostEnd<<") local ( "<<m_uiNodeLocalBegin<<", "<<m_uiNodeLocalEnd<<")"<<" post ("<<m_uiNodePostGhostBegin<<" , "<<m_uiNodePostGhostEnd<<")"<<std::endl;
        if(m_uiRank) std::cout<<"[ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiElementPreGhostBegin<<", "<<m_uiElementPreGhostEnd<<") local ( "<<m_uiElementLocalBegin<<", "<<m_uiElementLocalEnd<<")"<<" post ("<<m_uiElementPostGhostBegin<<" , "<<m_uiElementPostGhostEnd<<")"<<std::endl;
        //if(m_uiRank)std::cout<<"number of Fake Elements : "<<fakeElements_vec.size()<<std::endl;
        if(m_uiRank)std::cout<<"number of FakeElement Nodes: "<<m_uiNumFakeNodes<<std::endl;
        if(m_uiRank)std::cout<<"[Fake ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiFElementPreGhostBegin<<", "<<m_uiFElementPreGhostEnd<<") local ( "<<m_uiFElementLocalBegin<<", "<<m_uiFElementLocalEnd<<")"<<" post ("<<m_uiFElementPostGhostBegin<<" , "<<m_uiFElementPostGhostEnd<<")"<<std::endl;

//#endif

//---------------------------------------print out the E2N mapping of fake elements. (This is done for only the fake elements. ) ---------------------------------------------------------------------------------


       /* MPI_Barrier(MPI_COMM_WORLD);
        if(!m_uiRank){
            std::cout<<"rank: "<<m_uiRank<<"fake element e2n mapping. "<<std::endl;
            std::cout<<"number of Fake Elements : "<<fakeElements_vec.size()<<std::endl;
            std::cout<<"number of FakeElement Nodes: "<<m_uiNumFakeNodes<<std::endl;
            std::cout<<"[Fake ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiFElementPreGhostBegin<<", "<<m_uiFElementPreGhostEnd<<") local ( "<<m_uiFElementLocalBegin<<", "<<m_uiFElementLocalEnd<<")"<<" post ("<<m_uiFElementPostGhostBegin<<" , "<<m_uiFElementPostGhostEnd<<")"<<std::endl;
          for(unsigned int e=0;e<fakeElements_vec.size();e++)
          {
                  std::cout << "Element : "<<e<<" " << fakeElements_vec[e] << " : Node List :";
                  for (unsigned int k = 0; k < m_uiNpE; k++) {

                      std::cout << " " << fakeElement2Node_CG[e * m_uiNpE + k];

                  }

                  std::cout << std::endl;


          }
        }
        MPI_Barrier(MPI_COMM_WORLD);
        if(m_uiRank==1){
            std::cout<<"rank: "<<m_uiRank<<"fake element e2n mapping. "<<std::endl;
            std::cout<<"number of Fake Elements : "<<fakeElements_vec.size()<<std::endl;
            std::cout<<"number of FakeElement Nodes: "<<m_uiNumFakeNodes<<std::endl;
            std::cout<<"[Fake ELEMENT] rank:  "<<m_uiRank<<" pre ( "<<m_uiFElementPreGhostBegin<<", "<<m_uiFElementPreGhostEnd<<") local ( "<<m_uiFElementLocalBegin<<", "<<m_uiFElementLocalEnd<<")"<<" post ("<<m_uiFElementPostGhostBegin<<" , "<<m_uiFElementPostGhostEnd<<")"<<std::endl;
            for(unsigned int e=0;e<fakeElements_vec.size();e++)
            {
                std::cout << "Element : "<<e<<" " << fakeElements_vec[e] << " : Node List :";
                for (unsigned int k = 0; k < m_uiNpE; k++) {

                    std::cout << " " << fakeElement2Node_CG[e * m_uiNpE + k];

                }

                std::cout << std::endl;


            }
        }
        MPI_Barrier(MPI_COMM_WORLD);*/

//-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        // 8. Change the size of the original E2N mapping and copy fake element to node mapping at the end of the actual element to node mapping.
        //m_uiE2NMapping_CG.resize(m_uiNumTotalElements*m_uiNpE+(m_uiFElementPostGhostEnd-m_uiFElementPreGhostBegin)*m_uiNpE);
        //memcpy(&(*(m_uiE2NMapping_CG.begin()+(m_uiNumTotalElements*m_uiNpE))),&(*(fakeElement2Node_CG.begin())),sizeof(unsigned int )*(m_uiFElementPostGhostEnd-m_uiFElementPreGhostBegin)*m_uiNpE);

//---------------------------------------print out the final e2n mapping of all, actual and fake element to node mapping.--------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

        if(!m_uiRank) std::cout<<"E2N Mapping ended"<<std::endl;





    }




    void Mesh::computeNodeScatterMaps(MPI_Comm comm)
    {

        int rank,npes;
        MPI_Comm_rank(comm,&rank);
        MPI_Comm_size(comm,&npes);


        ot::TreeNode minMaxLocalNode[2];

        ot::TreeNode rootNode(0,0,0,0,m_uiDim,m_uiMaxDepth);
        std::vector<ot::TreeNode>tmpNode;

        unsigned int x,y,z,sz; // x y z and size of an octant.
        unsigned int ownerID,ii_x,jj_y,kk_z; // DG index to ownerID and ijk decomposition variable.
        std::set<unsigned int > nodeIndexVisited; // To keep track of the nodes already include in the sendNode maps.
        std::pair<std::set<unsigned int >::iterator,bool> setHintUint;
        unsigned int nodeIndex;
        unsigned int nodeIndex_DG;
        unsigned int elementLookUp;


        // 1. compute the local nodes.
        for(unsigned int ele=m_uiElementLocalBegin;ele<m_uiElementLocalEnd;ele++)
        {

            for(unsigned int k=0;k<(m_uiElementOrder+1);k++)
                for(unsigned int j=0;j<(m_uiElementOrder+1);j++)
                    for(unsigned int i=0;i<(m_uiElementOrder+1);i++) {
                        if(m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i]>=m_uiNodeLocalBegin  &&  m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i]<m_uiNodeLocalEnd) {
                            setHintUint=nodeIndexVisited.emplace(m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i]);
                            if(setHintUint.second) {
                                dg2eijk(m_uiE2NMapping_DG[ele * m_uiNpE +
                                                          k * (m_uiElementOrder + 1) * (m_uiElementOrder + 1) +
                                                          j * (m_uiElementOrder + 1) + i], ownerID, ii_x, jj_y, kk_z);
                                x = m_uiAllElements[ownerID].getX();
                                y = m_uiAllElements[ownerID].getY();
                                z = m_uiAllElements[ownerID].getZ();
                                sz = 1u << (m_uiMaxDepth - m_uiAllElements[ownerID].getLevel());
                                m_uiAllLocalNode.push_back(ot::TreeNode((x + ii_x * sz/m_uiElementOrder), (y + jj_y * sz/m_uiElementOrder), (z + kk_z * sz/m_uiElementOrder), m_uiMaxDepth,m_uiDim, m_uiMaxDepth));

                            }

                        }

                    }


        }


        // 2. Find the local max of the local nodes to compute local splitters.
        //SFC::seqSort::SFC_treeSortLocalOptimal(&(*(m_uiAllLocalNode.begin())),m_uiAllLocalNode.size(),m_uiMaxDepth,m_uiMaxDepth,rootNode,ROOT_ROTATION,false,maxLocalNode);
        SFC::seqSort::SFC_treeSort(&(*(m_uiAllLocalNode.begin())),m_uiAllLocalNode.size(),tmpNode,tmpNode,tmpNode,m_uiMaxDepth,m_uiMaxDepth,rootNode,ROOT_ROTATION,1,TS_SORT_ONLY);
        minMaxLocalNode[0] = m_uiAllLocalNode.front();
        minMaxLocalNode[1] = m_uiAllLocalNode.back();

        treeNodesTovtk(m_uiAllLocalNode,m_uiRank,"m_uiAllLocalNode");


        // 3. Gather the splitter max
        m_uiSplitterNodes=new ot::TreeNode[2*npes]; // both min and max
        std::vector<unsigned int > minMaxIDs;
        minMaxIDs.resize(2*npes);

        par::Mpi_Allgather(minMaxLocalNode,m_uiSplitterNodes,2,comm);

        std::vector<ot::TreeNode> splitterNodes;
        std::vector<ot::TreeNode> splitterElements;

        for(unsigned int p=0;p<npes;p++)
        {
            splitterNodes.push_back(m_uiSplitterNodes[p]);
            splitterElements.push_back(m_uiSplitter[p]);
        }

        if(!rank) treeNodesTovtk(splitterNodes,rank,"splitterNodes");
        if(!rank) treeNodesTovtk(splitterElements,rank,"splitterElements");

        assert(seq::test::isUniqueAndSorted(splitterElements));

        m_uiScatterMapActualNodeSend.clear();
        m_uiSendNodeCount=new unsigned int [npes];
        m_uiRecvNodeCount=new unsigned int [npes];
        m_uiSendNodeOffset=new unsigned int [npes];
        m_uiRecvNodeOffset=new unsigned int [npes];

        std::set<unsigned int >* scatterMapNodeSet=new std::set<unsigned int > [npes]; // To keep track of the nodes to send to each processor.
        std::set<unsigned int >* scatterMapElementSetR1=new std::set<unsigned int >[npes];  // Round 1 ghost element scatter map.


        //assert((m_uiNumPreGhostElements+m_uiNumPostGhostElements)==(m_uiRecvOctOffsetRound1[npes-1]+m_uiRecvOctCountRound1[npes-1]));


//#ifdef DEBUG_E2N_MAPPING
        std::vector<ot::TreeNode> customElements;
        std::vector<ot::TreeNode> sendNodes[npes];

        /*if(!m_uiRank) customElements.push_back(m_uiAllElements[206]);
        if(!m_uiRank) customElements.push_back(m_uiAllElements[57]);
        if(!m_uiRank) treeNodesTovtk(customElements,m_uiRank,"customElement");*/

        std::vector<ot::TreeNode> allocatedGNodes;




//#endif

        // 3a. Compute send nodes based on the send elements that processor p has sent to other processors in the round 1 ghost communication.
        for(unsigned int p=0;p<npes;p++)
        {
            m_uiSendNodeCount[p]=0;
            scatterMapNodeSet[p]=std::set<unsigned int >();
            scatterMapElementSetR1[p]=std::set<unsigned int >(m_uiScatterMapElementRound1.begin()+m_uiSendOctOffsetRound1[p],m_uiScatterMapElementRound1.begin()+m_uiSendOctOffsetRound1[p]+m_uiSendOctCountRound1[p]);

        }



        std::vector<ot::TreeNode> neighbourElement;
        std::set<Key, OctreeComp<Key>> * ghostElementChainedSet=new std::set<Key,OctreeComp<Key>>[npes];
        std::pair< std::set<Key,OctreeComp<Key>>::iterator,bool > setHintKey;
        std::set<Key,OctreeComp<Key>>::iterator  setItKey;
        std::vector<Key> ghostElementChained;
        std::vector<Key> tmpKeys;
        Key rootKey(0,0,0,0,m_uiDim,m_uiMaxDepth);
        unsigned int nodeFlag=0;
        ot::TreeNode tmpElement;
        unsigned int tmpEleLev=0;
        Key tmpKey;
        unsigned int* sendChainedGCount=new unsigned int [npes];
        unsigned int* recvChainedGCount=new unsigned int [npes];
        unsigned int* sendChainedGOffset=new unsigned int [npes];
        unsigned int* recvChainedGOffset=new unsigned int [npes];
        unsigned int * recvChainedKeyCount=new unsigned int [npes];
        unsigned int * recvChainedKeyOffset=new unsigned int [npes];

        std::vector<unsigned int > sendChainedGBuffer;
        std::set<unsigned  int>* sendChainedGhostIDSet=new std::set<unsigned int>[npes];





        // 4. Allocation of the node corresponding to pre ghost elements.

        nodeIndexVisited.clear();
        bool ghostLev1=false;
        unsigned int round1GhostCount=0;

        for(unsigned int ele=m_uiElementPreGhostBegin;ele<m_uiElementPreGhostEnd;ele++)
        {

            ghostLev1=false;
            if((round1GhostCount<m_uiGhostElementRound1Index.size()) && m_uiGhostElementRound1Index[round1GhostCount]==ele)
            {
                ghostLev1=true;
                round1GhostCount++;
            }



            for(unsigned int k=0;k<(m_uiElementOrder+1);k++)
                for(unsigned int j=0;j<(m_uiElementOrder+1);j++)
                    for(unsigned int i=0;i<(m_uiElementOrder+1);i++) {

                        nodeIndex=m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i];
                        nodeIndex_DG=m_uiE2NMapping_DG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i];

                        if((ghostLev1) && !(nodeIndex>=m_uiNodeLocalBegin  &&  nodeIndex<m_uiNodeLocalEnd)) {

                            setHintUint=nodeIndexVisited.emplace(nodeIndex);
                            if(setHintUint.second) {

                                m_uiScatterMapActualNodeRecv.push_back((*(setHintUint.first)));
                                dg2eijk(nodeIndex_DG, ownerID, ii_x, jj_y, kk_z);





//#ifdef DEBUG_E2N_MAPPING

                                x = m_uiAllElements[ownerID].getX();
                                y = m_uiAllElements[ownerID].getY();
                                z = m_uiAllElements[ownerID].getZ();
                                sz = 1u << (m_uiMaxDepth - m_uiAllElements[ownerID].getLevel());
                                allocatedGNodes.push_back(
                                        ot::TreeNode((x + ii_x * sz/m_uiElementOrder), (y + jj_y * sz/m_uiElementOrder), (z + kk_z * sz/m_uiElementOrder), m_uiMaxDepth,
                                                     m_uiDim, m_uiMaxDepth));
//#endif




                                assert(ownerID<m_uiAllElements.size());
                                tmpElement=m_uiAllElements[ownerID];
                                assert(tmpElement.getLevel()>=m_uiAllElements[ownerID].getLevel());
                                nodeFlag=getDIROfANode(ii_x,jj_y,kk_z);
                                //if(!m_uiRank) std::cout<<" nodeFlag: "<<nodeFlag<<std::endl;
                                assert(nodeFlag!=OCT_DIR_INTERNAL);
                                tmpEleLev=tmpElement.getLevel();
                                tmpElement.setFlag((tmpEleLev) | (1u<<(nodeFlag+CHAINED_GHOST_OFFSET)));
                                assert(tmpElement.getFlag()>>(CHAINED_GHOST_OFFSET) & (1u<<nodeFlag));
                                setHintKey=ghostElementChainedSet[m_uiRank].emplace(Key(tmpElement));
                                if(!setHintKey.second)
                                {

                                    setItKey=setHintKey.first;
                                    tmpKey=(*setItKey);
                                    tmpKey.setFlag((tmpKey.getFlag()| (1u<<(nodeFlag+CHAINED_GHOST_OFFSET))));
                                    setItKey++;
                                    ghostElementChainedSet[m_uiRank].erase(setHintKey.first);
                                    ghostElementChainedSet[m_uiRank].insert(setItKey,tmpKey);
                                    //std::cout<<"tmpKey: "<<tmpKey<<" owner Size: "<<tmpKey.getOwnerList()->size()<<std::endl;
                                }else
                                {
                                    setHintKey.first->addOwner(m_uiRank);
                                }



                                //std::cout<<"m_uiRank: "<<m_uiRank<<"owner ID: "<<ownerID<<" allocated node: "<<allocatedGNodes.back()<<std::endl;
                            }

                        }

                    }


        }

       /* if(m_uiRank) treeNodesTovtk(customElements,m_uiRank,"customEle");*/

        // 5. Allocation of the node corresponding to post ghost elements.

        for(unsigned int ele=m_uiElementPostGhostBegin;ele<m_uiElementPostGhostEnd;ele++)
        {

            ghostLev1=false;
            if((round1GhostCount<m_uiGhostElementRound1Index.size()) && m_uiGhostElementRound1Index[round1GhostCount]==ele)
            {
                ghostLev1=true;
                round1GhostCount++;
            }

            for(unsigned int k=0;k<(m_uiElementOrder+1);k++)
                for(unsigned int j=0;j<(m_uiElementOrder+1);j++)
                    for(unsigned int i=0;i<(m_uiElementOrder+1);i++) {

                        nodeIndex=m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i];
                        nodeIndex_DG=m_uiE2NMapping_DG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i];

                        if((ghostLev1) && !(nodeIndex>=m_uiNodeLocalBegin  &&  nodeIndex<m_uiNodeLocalEnd)){

                            setHintUint=nodeIndexVisited.emplace(m_uiE2NMapping_CG[ele*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i]);

                            if(setHintUint.second) {

                                m_uiScatterMapActualNodeRecv.push_back((*(setHintUint.first)));
                                dg2eijk(nodeIndex_DG, ownerID, ii_x, jj_y, kk_z);

//#ifdef DEBUG_E2N_MAPPING

                                x = m_uiAllElements[ownerID].getX();
                                y = m_uiAllElements[ownerID].getY();
                                z = m_uiAllElements[ownerID].getZ();
                                sz = 1u << (m_uiMaxDepth - m_uiAllElements[ownerID].getLevel());
                                allocatedGNodes.push_back(ot::TreeNode((x + ii_x * sz/m_uiElementOrder), (y + jj_y * sz/m_uiElementOrder), (z + kk_z * sz/m_uiElementOrder), m_uiMaxDepth,m_uiDim, m_uiMaxDepth));

//#endif
                                assert((ownerID!=LOOK_UP_TABLE_DEFAULT) && (!(ownerID>=m_uiElementLocalBegin && ownerID<m_uiElementLocalEnd)));
                                assert(ownerID<m_uiAllElements.size());
                                tmpElement=m_uiAllElements[ownerID];
                                assert(tmpElement.getLevel()>=m_uiAllElements[ownerID].getLevel());
                                nodeFlag=getDIROfANode(ii_x,jj_y,kk_z);
                                //if(!m_uiRank) std::cout<<" nodeFlag: "<<nodeFlag<<std::endl;
                                assert(nodeFlag!=OCT_DIR_INTERNAL);
                                tmpEleLev=tmpElement.getLevel();
                                tmpElement.setFlag((tmpEleLev) | (1u<<(nodeFlag+CHAINED_GHOST_OFFSET)));
                                assert(tmpElement.getFlag()>>(CHAINED_GHOST_OFFSET) & (1u<<nodeFlag));
                                setHintKey=ghostElementChainedSet[m_uiRank].emplace(Key(tmpElement));
                                if(!setHintKey.second)
                                {

                                    setItKey=setHintKey.first;
                                    tmpKey=(*setItKey);
                                    tmpKey.setFlag((tmpKey.getFlag()| (1u<<(nodeFlag+CHAINED_GHOST_OFFSET))));
                                    setItKey++;
                                    ghostElementChainedSet[m_uiRank].erase(setHintKey.first);
                                    ghostElementChainedSet[m_uiRank].insert(setItKey,tmpKey);
                                    //std::cout<<"tmpKey: "<<tmpKey<<" owner Size: "<<tmpKey.getOwnerList()->size()<<std::endl;
                                }else
                                {
                                    setHintKey.first->addOwner(m_uiRank);
                                }


                                //if(!m_uiRank) std::cout<<"m_uiRank: "<<m_uiRank<<"ele: "<<ele<<" : "<<m_uiAllElements[ele]<<"(i,j,k): "<<i<<" ,"<<" ,"<<j<<" ,"<<k<<"  allocated: "<<allocatedGNodes.back()<<" Index: "<<(*(hint.first))<<" owner,i,j,k:" <<ownerID<<" ,"<<ii_x<<" ,"<<jj_y<<" ,"<<kk_z<<" owner : "<<m_uiAllElements[ownerID]<<std::endl;
                                //std::cout<<"m_uiRank: "<<m_uiRank<<"owner ID: "<<ownerID<<" allocated node: "<<allocatedGNodes.back()<<std::endl;

                            }

                        }

                    }

        }



//#ifdef DEBUG_E2N_MAPPING
        std::vector<ot::TreeNode> ownerElement1;
        std::vector<ot::TreeNode> ownerElement2;


        std::vector<ot::TreeNode> nonLocalNodes;
        std::vector<ot::TreeNode> LocalNodes;

        treeNodesTovtk(allocatedGNodes,rank,"allocatedGhostNodes");
//#endif



        ghostElementChained.clear();
        for(unsigned int p=0;p<npes;p++)
        {
            ghostElementChained.insert(ghostElementChained.end(),ghostElementChainedSet[p].begin(),ghostElementChainedSet[p].end());

        }
        std::cout<<"m_uiRank "<<m_uiRank<<" gElementChained.size(): "<<ghostElementChained.size()<<std::endl;
        treeNodesTovtk(ghostElementChained,m_uiRank,"ghostElementChained");

        std::vector<unsigned int> recvGhostChainedFlag;
        std::vector<Key> recvGhostChained;
        std::vector<unsigned int > recvChainedGBuffer;
        std::vector<Key> recvGhostChainedSearchKeys;
        std::vector<unsigned int> * ownerList;
        unsigned int result;

        unsigned int tmpCornerIndex;
        std::vector<unsigned int> tmpCornerIndexVec;

        std::vector<Key> missingNodes;
        std::set<Key,OctreeComp<Key> > missingNodesSet;


        while(ghostElementChained.size())
        {

            sendChainedGBuffer.clear();
            recvGhostChainedFlag.clear();
            recvGhostChained.clear();
            recvChainedGBuffer.clear();
            recvGhostChainedSearchKeys.clear();
            missingNodes.clear();
            missingNodesSet.clear();

            for(unsigned int p=0;p<npes;p++)
            {
                ghostElementChainedSet[p].clear();
                sendChainedGhostIDSet[p].clear();
                sendChainedGCount[p]=0;
                recvChainedKeyCount[p]=0;
            }


            unsigned int myX,myY,myZ,mySz;

            for(unsigned int e=0;e<ghostElementChained.size();e++)
            {
                /*if(m_uiRank==11 && (ghostElementChained[e]==m_uiAllElements[225]))
                {
                    std::cout<<" m_uiRank:  "<<m_uiRank<<" e(vec): "<<e<<" val: "<<ghostElementChained[e]<<" flag_val: "<<ghostElementChained[e].getFlag()<<std::endl;
                }*/

                myX=ghostElementChained[e].getX();
                myY=ghostElementChained[e].getY();
                myZ=ghostElementChained[e].getZ();
                mySz=1u<<(m_uiMaxDepth-ghostElementChained[e].getLevel());

                nodeFlag=ghostElementChained[e].getFlag();
                nodeFlag=nodeFlag>>(CHAINED_GHOST_OFFSET);

                // Corner nodes.
                if((nodeFlag & (1u<<(OCT_DIR_LEFT_DOWN_BACK))))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_RIGHT_DOWN_BACK)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_LEFT_UP_BACK)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_RIGHT_UP_BACK)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_LEFT_DOWN_FRONT)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_RIGHT_DOWN_FRONT)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_LEFT_UP_FRONT)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }

                if((nodeFlag & (1u<<OCT_DIR_RIGHT_UP_FRONT)))
                {
                    setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                    setHintKey.first->addOwner(e);
                }


                if(m_uiElementOrder>1)
                {

                    // internal edges.  can happen if only order is >1
                    if((nodeFlag & (1u<<OCT_DIR_LEFT_DOWN)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_LEFT_UP)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    } else if((nodeFlag & (1u<<OCT_DIR_LEFT_BACK)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_LEFT_FRONT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }if((nodeFlag & (1u<<OCT_DIR_RIGHT_DOWN)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_RIGHT_UP)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    } else if((nodeFlag & (1u<<OCT_DIR_RIGHT_BACK)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_RIGHT_FRONT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_DOWN_BACK)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_DOWN_FRONT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_UP_BACK)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_UP_FRONT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }


                    // internal faces.

                    if((nodeFlag & (1u<<OCT_DIR_LEFT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+0*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_RIGHT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+m_uiElementOrder*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_DOWN)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+0*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_UP)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+m_uiElementOrder*(mySz/m_uiElementOrder)),(myZ+1*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);


                    }else if((nodeFlag & (1u<<OCT_DIR_BACK)))
                    {

                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+0*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);

                    }else if((nodeFlag & (1u<<OCT_DIR_FRONT)))
                    {
                        setHintKey= missingNodesSet.emplace(Key((myX+1*(mySz/m_uiElementOrder)),(myY+1*(mySz/m_uiElementOrder)),(myZ+m_uiElementOrder*(mySz/m_uiElementOrder)),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));
                        setHintKey.first->addOwner(e);
                    }


                }




            }


            for(unsigned int p=0;p<2*npes;p++) {
                missingNodesSet.emplace(Key(m_uiSplitterNodes[p]));
            }




            missingNodes.clear();
            missingNodes.insert(missingNodes.end(),missingNodesSet.begin(),missingNodesSet.end());

            std::cout<<"m_uiRank: "<<m_uiRank<<" missing node size: "<<missingNodes.size()<<std::endl;

            treeNodesTovtk(missingNodes,m_uiRank,"missingNodes");
            treeNodesTovtk(nonLocalNodes,rank,"nonLocal");

            SFC::seqSort::SFC_treeSort(&(*(missingNodes.begin())),missingNodes.size(),tmpKeys,tmpKeys,tmpKeys,m_uiMaxDepth,m_uiMaxDepth,rootKey,ROOT_ROTATION,1,TS_SORT_ONLY);

            for(unsigned int p=0;p<2*npes;p++)
            {
                minMaxIDs[p]=(std::find(missingNodes.begin(),missingNodes.end(),m_uiSplitterNodes[p])-missingNodes.begin());
                assert(minMaxIDs[p]<missingNodes.size());
            }

            Key * missingNodesPtr=&(*(missingNodes.begin()));
            for(unsigned int e=0;e<missingNodes.size();e++)
            {

                for(unsigned int p=0;p<npes;p++)
                {

                    if( ( p!=m_uiRank ) &&  (e>=minMaxIDs[2*p] && e<=minMaxIDs[2*p+1]))
                    { // need to send this node to processor p.
                        for (unsigned int w = 0; w < missingNodesPtr[e].getOwnerList()->size(); w++)
                        {
                            setHintUint=sendChainedGhostIDSet[p].emplace((*(missingNodesPtr[e].getOwnerList()))[w]);
                        }
                    }

                }

            }

            // std::cout<<"m_uiRank: "<<m_uiRank<<" splitterNodeCount: "<<splitterNodeCount<<std::endl;
            for(unsigned int p=0;p<npes;p++)
            {
                for(auto it=sendChainedGhostIDSet[p].begin();it!=sendChainedGhostIDSet[p].end();++it)
                {

                    sendChainedGBuffer.push_back(ghostElementChained[*it].getX());
                    sendChainedGBuffer.push_back(ghostElementChained[*it].getY());
                    sendChainedGBuffer.push_back(ghostElementChained[*it].getZ());
                    sendChainedGBuffer.push_back(ghostElementChained[*it].getFlag());
                    sendChainedGBuffer.push_back(ghostElementChained[*it].getOwnerList()->size());
                    sendChainedGCount[p]+=5+ghostElementChained[*it].getOwnerList()->size();
                    for (unsigned int w = 0; w < ghostElementChained[*it].getOwnerList()->size(); w++)
                        sendChainedGBuffer.push_back((*(ghostElementChained[*it].getOwnerList()))[w]);

                }

            }

            par::Mpi_Alltoall(sendChainedGCount,recvChainedGCount,1,comm);


            sendChainedGOffset[0]=0;
            recvChainedGOffset[0]=0;

            omp_par::scan(sendChainedGCount,sendChainedGOffset,npes);
            omp_par::scan(recvChainedGCount,recvChainedGOffset,npes);

            //std::cout<<" m_uiRank: "<<m_uiRank<<" sendBuf ID size: "<<sendChainedGBuffer.size()<<" sendCount Size: "<<(sendChainedGOffset[npes-1]+sendChainedGCount[npes-1])<<std::endl;
            assert(sendChainedGBuffer.size()==(sendChainedGOffset[npes-1]+sendChainedGCount[npes-1]));


            recvChainedGBuffer.resize(recvChainedGOffset[npes-1]+recvChainedGCount[npes-1]);
            par::Mpi_Alltoallv(&(*(sendChainedGBuffer.begin())),(int *) sendChainedGCount,(int *) sendChainedGOffset,&(*(recvChainedGBuffer.begin())),(int *) recvChainedGCount,(int *) recvChainedGOffset,comm);



            unsigned int recvGindex=0;
            unsigned int pCount=0;
            unsigned int recvKeyCount=0;

            for(unsigned int p=0;p<npes;p++)
            {   recvKeyCount=0;
                while(recvGindex<(recvChainedGOffset[p]+recvChainedGCount[p]))
                {
                    tmpKey=Key(recvChainedGBuffer[recvGindex],recvChainedGBuffer[recvGindex+1],recvChainedGBuffer[recvGindex+2],(recvChainedGBuffer[recvGindex+3] & ot::TreeNode::MAX_LEVEL),m_uiDim,m_uiMaxDepth);
                    recvGhostChainedFlag.push_back(recvChainedGBuffer[recvGindex+3]);
                    tmpKey.getOwnerList()->resize(recvChainedGBuffer[recvGindex+4]);
                    tmpKey.getOwnerList()->assign((recvChainedGBuffer.begin()+(recvGindex+5)),(recvChainedGBuffer.begin()+(recvGindex+5)+recvChainedGBuffer[recvGindex+4]));
                    recvGindex=recvGindex+5+recvChainedGBuffer[recvGindex+4];
                    recvGhostChained.push_back(tmpKey);
                    recvKeyCount++;

                }

                recvChainedKeyCount[p]=recvKeyCount;

            }

            recvChainedKeyOffset[0]=0;
            omp_par::scan(recvChainedKeyCount,recvChainedKeyOffset,npes);
            treeNodesTovtk(recvGhostChained,m_uiRank,"recvGChained");


            recvGhostChainedSearchKeys.resize(recvGhostChained.size());
            for(unsigned int ele=0;ele<recvGhostChained.size();ele++)
            {
                recvGhostChainedSearchKeys[ele]=Key(recvGhostChained[ele].getX(),recvGhostChained[ele].getY(),recvGhostChained[ele].getZ(),recvGhostChained[ele].getLevel(),m_uiDim,m_uiMaxDepth);
                recvGhostChainedSearchKeys[ele].addOwner(ele);
                recvGhostChained[ele].setSearchResult(LOOK_UP_TABLE_DEFAULT);

            }


            /*std::cout<<"m_uiRank "<<m_uiRank<<" recvKeySize: "<<recvGhostChained.size()<<std::endl;*/
            /* if(m_uiRank==12)
             for(unsigned int p=0;p<npes;p++)
             {   //std::cout<<"recvKeyGCount p : "<<p<<" : "<< (recvChainedGOffset[p]+recvChainedGCount[p])<<std::endl;
                 std::cout<<"recvKeyCount p: "<<p<<" : "<<recvChainedKeyCount[p]<<std::endl;
             }*/

            SFC::seqSearch::SFC_treeSearch(&(*(recvGhostChainedSearchKeys.begin())),&(*(m_uiAllElements.begin())),0,recvGhostChainedSearchKeys.size(),0,m_uiAllElements.size(),m_uiMaxDepth,m_uiMaxDepth,ROOT_ROTATION);
            for(unsigned int ele=0;ele<recvGhostChained.size();ele++)
            {
                assert(recvGhostChained[ele].getOwnerList()->size()==1);
                if(recvGhostChainedSearchKeys[ele].getFlag() & OCT_FOUND)
                {
                    recvGhostChained[(*(recvGhostChainedSearchKeys[ele].getOwnerList()))[0]].setSearchResult(recvGhostChainedSearchKeys[ele].getSearchResult());
                }

            }


            ghostElementChained.clear();

            for(unsigned int ele=0;ele<recvGhostChained.size();ele++)
            {

                if(recvGhostChained[ele].getSearchResult()!=LOOK_UP_TABLE_DEFAULT)
                { // implies that current chainedGhost is one of my local node.

                    tmpCornerIndexVec.clear();
                    ownerList=recvGhostChained[ele].getOwnerList();
                    result=recvGhostChained[ele].getSearchResult();
                    nodeFlag=recvGhostChainedFlag[ele];
                    nodeFlag=nodeFlag>>(CHAINED_GHOST_OFFSET);

                    /*if(*//*m_uiRank==12*//*(m_uiRank==12 && ele==615) || ( m_uiRank==12 && (recvGhostChained[ele]==recvGhostChained[615])))
                {

                    std::cout<<"ele: "<<ele<<" Key: "<<recvGhostChained[ele]<<" found: "<<m_uiAllElements[result]<<" nodeFlag: "<<recvGhostChainedFlag[ele]<<std::endl;
                    for(unsigned int node=0;node<m_uiNpE;node++)
                    {
                        nodeIndex=m_uiE2NMapping_CG[result*m_uiNpE+node];
                        if(nodeIndex>=m_uiNodeLocalBegin && nodeIndex<m_uiNodeLocalEnd)
                        {
                            std::cout<<"E2N: "<<ele<<" found: "<<result<<" node: "<<"node: "<<node;
                        }
                        std::cout<<std::endl;


                    }
                    std::vector<ot::TreeNode> cusElement;
                    cusElement.push_back(recvGhostChained[ele]);
                    treeNodesTovtk(cusElement,ele,"cusElement");
                    cusElement.clear();


                }
*/

                    if(nodeFlag & (1u<<OCT_DIR_LEFT_DOWN_BACK))
                    {
                        cornerNodeIndex(result,0,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_RIGHT_DOWN_BACK))
                    {
                        cornerNodeIndex(result,1,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_LEFT_UP_BACK))
                    {
                        cornerNodeIndex(result,2,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_RIGHT_UP_BACK))
                    {
                        cornerNodeIndex(result,3,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }


                    if(nodeFlag & (1u<<OCT_DIR_LEFT_DOWN_FRONT))
                    {
                        cornerNodeIndex(result,4,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_RIGHT_DOWN_FRONT))
                    {
                        cornerNodeIndex(result,5,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_LEFT_UP_FRONT))
                    {
                        cornerNodeIndex(result,6,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }

                    if(nodeFlag & (1u<<OCT_DIR_RIGHT_UP_FRONT))
                    {
                        cornerNodeIndex(result,7,tmpCornerIndex);
                        tmpCornerIndexVec.push_back(tmpCornerIndex);
                    }


                    for(unsigned int cornerNodeIndex=0;cornerNodeIndex<tmpCornerIndexVec.size();cornerNodeIndex++)
                    {

                        nodeIndex=m_uiE2NMapping_CG[tmpCornerIndexVec[cornerNodeIndex]];
                        nodeIndex_DG=m_uiE2NMapping_DG[tmpCornerIndexVec[cornerNodeIndex]];
                        dg2eijk(nodeIndex_DG,ownerID,ii_x,jj_y,kk_z);

                        for(unsigned int w=0;w<ownerList->size();w++)
                        {
                            if(nodeIndex>=m_uiNodeLocalBegin && nodeIndex<m_uiNodeLocalEnd)
                            {

                                assert((*ownerList)[w]!=m_uiRank);

                                setHintUint=scatterMapNodeSet[(*ownerList)[w]].emplace(nodeIndex);
                                if(setHintUint.second)
                                {
                                    dg2eijk(nodeIndex_DG,ownerID,ii_x,jj_y,kk_z);
                                    x=m_uiAllElements[ownerID].getX();
                                    y=m_uiAllElements[ownerID].getY();
                                    z=m_uiAllElements[ownerID].getZ();
                                    sz=1u<<(m_uiMaxDepth-m_uiAllElements[ownerID].getLevel());
                                    sendNodes[(*ownerList)[w]].push_back(ot::TreeNode((x+ii_x*sz/m_uiElementOrder),(y+jj_y*sz/m_uiElementOrder),(z+kk_z*sz/m_uiElementOrder),m_uiMaxDepth,m_uiDim,m_uiMaxDepth));

                                    m_uiSendNodeCount[(*ownerList)[w]]++;
                                    //std::cout<<"m_uiRank: "<<m_uiRank<<" R2 SendNodes Executed. "<<std::endl;

                                }
                            }else
                            {

                                assert(!(nodeIndex>=m_uiNodeLocalBegin && nodeIndex<m_uiNodeLocalEnd));
                                assert(ownerID<m_uiAllElements.size());
                                tmpElement=m_uiAllElements[ownerID];
                                assert(tmpElement.getLevel()>=m_uiAllElements[ownerID].getLevel());
                                nodeFlag=getDIROfANode(ii_x,jj_y,kk_z);
                                //if(!m_uiRank) std::cout<<" nodeFlag: "<<nodeFlag<<std::endl;
                                assert(nodeFlag!=OCT_DIR_INTERNAL);
                                tmpEleLev=tmpElement.getLevel();
                                tmpElement.setFlag((tmpEleLev) | (1u<<(nodeFlag+CHAINED_GHOST_OFFSET)));
                                assert(tmpElement.getFlag()>>(CHAINED_GHOST_OFFSET) & (1u<<nodeFlag));

                                if((ownerID>=m_uiElementLocalBegin && ownerID<m_uiElementLocalEnd))
                                {

                                    setHintKey=ghostElementChainedSet[(*ownerList)[w]].emplace(Key(tmpElement));
                                    if(!setHintKey.second)
                                    {

                                        setItKey=setHintKey.first;
                                        tmpKey=(*setItKey);
                                        tmpKey.setFlag((tmpKey.getFlag()| (1u<<(nodeFlag+CHAINED_GHOST_OFFSET))));
                                        setItKey++;
                                        ghostElementChainedSet[(*ownerList)[w]].erase(setHintKey.first);
                                        ghostElementChainedSet[(*ownerList)[w]].insert(setItKey,tmpKey);
                                        //std::cout<<"tmpKey: "<<tmpKey<<" owner Size: "<<tmpKey.getOwnerList()->size()<<std::endl;
                                    }else
                                    {
                                        setHintKey.first->addOwner((*ownerList)[w]);
                                    }

                                } else
                                {
                                    assert(ownerID!=LOOK_UP_TABLE_DEFAULT);
                                    /*if(m_uiRank==23)
                                    {
                                        std::vector<ot::TreeNode> cusECheck;
                                        cusECheck.push_back(m_uiAllElements[ownerID]);
                                        treeNodesTovtk(cusECheck,ownerID,"cusECheck");
                                    }*/


                                }



                            }


                        }

                    }

                }

            }


            for(unsigned int p=0;p<npes;p++)
            {
                ghostElementChained.insert(ghostElementChained.end(),ghostElementChainedSet[p].begin(),ghostElementChainedSet[p].end());
                ghostElementChainedSet[p].clear();

            }


        }




        for(unsigned int p=0;p<npes;p++) {
            char filename[256];
            sprintf(filename,"sendNode_R2%d",p);
            treeNodesTovtk(sendNodes[p],rank, filename);
            sendNodes[p].clear();
        }

        delete [] sendChainedGCount;
        delete [] recvChainedGCount;
        delete [] sendChainedGOffset;
        delete [] recvChainedGOffset;
        delete [] recvChainedKeyCount;
        delete [] recvChainedKeyOffset;





        treeNodesTovtk(neighbourElement,m_uiRank,"neighBourElement");
//#ifdef DEBUG_E2N_MAPPING
        std::cout<<"======== m_uiRank: "<<m_uiRank<<" : "<<allocatedGNodes.size()<<std::endl;


        treeNodesTovtk(ownerElement1,rank,"owner1Elements");
        treeNodesTovtk(ownerElement2,rank,"owner2Elements");
        treeNodesTovtk(nonLocalNodes,rank,"nonLocal");
        treeNodesTovtk(LocalNodes,rank,"Local");
//#endif


        for(unsigned int p=0;p<npes;p++) {
            m_uiScatterMapActualNodeSend.insert(m_uiScatterMapActualNodeSend.end(), scatterMapNodeSet[p].begin(),
                                            scatterMapNodeSet[p].end());
            scatterMapNodeSet[p].clear();
        }

        delete [] scatterMapNodeSet;
        delete [] scatterMapElementSetR1;
        delete [] sendChainedGhostIDSet;



        par::Mpi_Alltoall(m_uiSendNodeCount,m_uiRecvNodeCount,1,comm);

        m_uiSendNodeOffset[0]=0;
        m_uiRecvNodeOffset[0]=0;

        omp_par::scan(m_uiSendNodeCount,m_uiSendNodeOffset,npes);
        omp_par::scan(m_uiRecvNodeCount,m_uiRecvNodeOffset,npes);
/*
        MPI_Barrier(comm);
        if(!rank)
            for(unsigned int p=0;p<npes;p++)
                std::cout<<"rank: "<<rank<<" m_uiSendNodeCount["<<p<<"]: "<<m_uiSendNodeCount[p]<<" m_uiRecvNodeCount["<<p<<"]: "<<m_uiRecvNodeCount[p]<<std::endl;

        MPI_Barrier(comm);
        if(rank==1)
            for(unsigned int p=0;p<npes;p++)
                std::cout<<"rank: "<<rank<<" m_uiSendNodeCount["<<p<<"]: "<<m_uiSendNodeCount[p]<<" m_uiRecvNodeCount["<<p<<"]: "<<m_uiRecvNodeCount[p]<<std::endl;

        MPI_Barrier(comm);*/


        if((m_uiRecvNodeOffset[npes-1]+m_uiRecvNodeCount[npes-1])!=m_uiScatterMapActualNodeRecv.size())
        {
            std::cout<<" Rank: "<<m_uiRank<<" Total Ghost Elements allocated: "<<m_uiScatterMapActualNodeRecv.size()<<" Number of elements will get recieved: "<<(m_uiRecvNodeOffset[npes-1]+m_uiRecvNodeCount[npes-1])<<std::endl;
            assert(false);
        }
        std::cout<<" Rank: "<<m_uiRank<<" Total Ghost Elements allocated: "<<m_uiScatterMapActualNodeRecv.size()<<" Number of elements will get recieved: "<<(m_uiRecvNodeOffset[npes-1]+m_uiRecvNodeCount[npes-1])<<std::endl;

#ifdef DEBUG_E2N_MAPPING
        MPI_Barrier(comm);
        if(!rank)
            for(unsigned int p=0;p<npes;p++)
                std::cout<<"rank: "<<rank<<" recv nodes from : ["<<p<<"]: begin:  "<<m_uiRecvNodeOffset[p]<<" end: "<<(m_uiRecvNodeOffset[p]+m_uiRecvNodeCount[p])<<std::endl;


        MPI_Barrier(comm);
        if(rank==1)
            for(unsigned int p=0;p<npes;p++)
                std::cout<<"rank: "<<rank<<" recv nodes from : ["<<p<<"]: begin:  "<<m_uiRecvNodeOffset[p]<<" end: "<<(m_uiRecvNodeOffset[p]+m_uiRecvNodeCount[p])<<std::endl;


        MPI_Barrier(comm);
#endif


    }


}