//
// Created by milinda on 9/2/16.

/**
 * @author: Milinda Shayamal Fernando
 *
 * School of Computing , University of Utah
 *
 * @breif Contains the functions to generate the mesh data structure from the 2:1 balanced linear octree.
 * @details
 * Assumptions:
 * 1). Assumes that octree is balanced and sorted.
 * 2). Assumes that there is no duplicate nodes.
 * 3). Inorder to make the implementation much simpler we use the Morton ordering to number the each vertex of a given particular element. Note that, this doesn't cause any dependency between SFC curve used and the mesh
 * generation part.
 *
 * */


//

#ifndef SFCSORTBENCH_MESH_H
#define SFCSORTBENCH_MESH_H


#include <vector>
#include "mpi.h"
#include "octUtils.h"
#include "treenode2vtk.h"
#include "TreeNode.h"
#include "sfcSort.h"
#include "sfcSearch.h"
#include "testUtils.h"
#include <climits>
#include <memory>
#include "refel.h"

/**
 * How the oct flags are used in the mesh generation part.
 *
 * bit 0 - 4 : Used to store the level.
 * bit 5: 1- if the key was found in the tree 0- otherwise.
 * bit 6: Key type NONE
 * bit 7: Key type SPLITTER
 * bit 8: Key type UP
 * bit 9: Key type DOWN
 * bit 10: Key type FRONT
 * bit 11: Key type BACK
 * bit 12: Key type LEFT
 * bit 13: key type RIGHT
 *
 * */

enum KeyType{NONE,SPLITTER,UP,DOWN,FRONT,BACK,LEFT,RIGHT};




//#define DEBUG_MESH_GENERATION

#define LOOK_UP_TABLE_DEFAULT UINT_MAX
#define KS_MAX 31 // maximum number of points in any direction that wavelet DA supports.
#define KEY_DIR_OFFSET 7
#define CHAINED_GHOST_OFFSET 5


namespace ot
{

    namespace WaveletDA
    {
        enum LoopType{ALL,INDEPENDENT,DEPENDENT};
    }


}



namespace ot {



    /**
     * @breif Key class to store some additional info to build nodelist.
     * */
    class Key : public ot::TreeNode
    {

    protected:
        std::shared_ptr< std::vector<unsigned int > > m_uiOwnerList;
        std::shared_ptr< std::vector<uint8_t > > m_uiStencilIndexWithDirection;  // first 3 bits contains the direction (OCT_DIR_LEFT,OCT_DIR_RIGHT etc) and the other 5 bit contains the stencil index.  Hence k_s max is 31.
        unsigned int m_uiSearchResult;


    public:
        Key()
        {
            m_uiX=0;
            m_uiY=0;
            m_uiZ=0;
            m_uiLevel=0;
            m_uiSearchResult=0;

            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();

            m_uiOwnerList= std::shared_ptr< std::vector<unsigned int > >(new std::vector<unsigned int>());
            m_uiStencilIndexWithDirection=std::shared_ptr< std::vector<uint8_t > >(new std::vector<uint8_t >());


            m_uiOwnerList.get()->clear();
            m_uiStencilIndexWithDirection.get()->clear();


        }

        Key(unsigned int px, unsigned int py, unsigned int pz, unsigned int plevel,unsigned int pDim,unsigned int pMaxDepth)/*: ot::TreeNode(px,py,pz,plevel,pDim,pMaxDepth)*/
        {
            m_uiX=px;
            m_uiY=py;
            m_uiZ=pz;
            m_uiLevel=plevel;

            m_uiSearchResult=0;

            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();

            m_uiOwnerList= std::shared_ptr< std::vector<unsigned int > >(new std::vector<unsigned int>());
            m_uiStencilIndexWithDirection=std::shared_ptr< std::vector<uint8_t > >(new std::vector<uint8_t >());


            m_uiOwnerList.get()->clear();
            m_uiStencilIndexWithDirection.get()->clear();


        }

        Key(unsigned int pLevel, unsigned int pMaxDepth)
        {
            m_uiX=0;
            m_uiY=0;
            m_uiZ=0;
            m_uiSearchResult=0;
            m_uiLevel=pLevel;

            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();

            m_uiOwnerList= std::shared_ptr< std::vector<unsigned int > >(new std::vector<unsigned int>());
            m_uiStencilIndexWithDirection=std::shared_ptr< std::vector<uint8_t > >(new std::vector<uint8_t >());


            m_uiOwnerList.get()->clear();
            m_uiStencilIndexWithDirection.get()->clear();

        }

        Key(const ot::TreeNode node)
        {
            m_uiX=node.getX();
            m_uiY=node.getY();
            m_uiZ=node.getZ();
            m_uiLevel=node.getFlag();
            m_uiSearchResult=0;
            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();

            m_uiOwnerList= std::shared_ptr< std::vector<unsigned int > >(new std::vector<unsigned int>());
            m_uiStencilIndexWithDirection=std::shared_ptr< std::vector<uint8_t > >(new std::vector<uint8_t >());


            m_uiOwnerList.get()->clear();
            m_uiStencilIndexWithDirection.get()->clear();

        }

        ~Key()
        {
            // need to delete the allocated variables
            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();



        }

        inline void operator= ( const Key & node )

        {

            //std::cout<<"shared_ptr_ copy begin0 : "<<*this<< " = "<<node<<std::endl;
            //std::cout<<"shared_ptr_ copy begin1 : "<<*this<<" owner Size: "<<m_uiOwnerList.get()->size()<< " = "<<node<<" node ownerSize : "<<node.m_uiOwnerList.get()->size()<<std::endl;
            m_uiX=node.getX();
            m_uiY=node.getY();
            m_uiZ=node.getZ();
            m_uiLevel=node.getFlag();
            m_uiSearchResult=node.getSearchResult();
            //std::cout<<"shared_ptr_ copy begin2 : "<<*this<<" owner Size: "<<m_uiOwnerList.get()->size()<< " = "<<node<<" node ownerSize : "<<node.m_uiOwnerList.get()->size()<<std::endl;
            m_uiOwnerList.reset();
            m_uiStencilIndexWithDirection.reset();

           /* m_uiOwnerList=std::shared_ptr< std::vector<unsigned int > >(node.m_uiOwnerList);
            m_uiStencilIndexWithDirection=std::shared_ptr< std::vector<uint8_t > >(node.m_uiStencilIndexWithDirection);*/
            m_uiOwnerList=node.m_uiOwnerList;
            m_uiStencilIndexWithDirection=node.m_uiStencilIndexWithDirection;

            //std::cout<<"shared_ptr_ copy end1 : "<<*this<<" owner Size: "<<m_uiOwnerList.get()->size()<< " = "<<node<<" node ownerSize : "<<node.m_uiOwnerList.get()->size()<<std::endl;
            /*return *this;*/
        }

        inline void operator= ( const ot::TreeNode & node )

        {

            //std::cout<<"shared_ptr_ copy begin0 : "<<*this<< " = "<<node<<std::endl;
            //std::cout<<"shared_ptr_ copy begin1 : "<<*this<<" owner Size: "<<m_uiOwnerList.get()->size()<< " = "<<node<<" node ownerSize : "<<node.m_uiOwnerList.get()->size()<<std::endl;
            m_uiX=node.getX();
            m_uiY=node.getY();
            m_uiZ=node.getZ();
            m_uiLevel=node.getFlag();
            //std::cout<<"shared_ptr_ copy end1 : "<<*this<<" owner Size: "<<m_uiOwnerList.get()->size()<< " = "<<node<<" node ownerSize : "<<node.m_uiOwnerList.get()->size()<<std::endl;
            //return *this;
        }


        inline void setSearchResult(unsigned int pIndex)
        {
            m_uiSearchResult=pIndex;
        }

        inline void addOwner(unsigned int ownerLocalID) const{
            m_uiOwnerList.get()->push_back(ownerLocalID);
        }

        inline void changeOwnerOffset(unsigned int localBegin) const
        {
            std::vector<unsigned int >* tmp=m_uiOwnerList.get();
            for(unsigned int k=0;k<tmp->size();k++)
                (*tmp)[k]=(*tmp)[k]+localBegin;
        }

        inline void addStencilIndexAndDirection(unsigned int index, unsigned int direction) const
        {
            uint8_t temp= (index<<3) | direction;
            m_uiStencilIndexWithDirection.get()->push_back(temp);
        }



        inline std::vector<unsigned int> * getOwnerList(){return m_uiOwnerList.get(); }
        inline std::vector<uint8_t > * getStencilIndexDirectionList(){return m_uiStencilIndexWithDirection.get();}


        inline unsigned int getSearchResult()const {return m_uiSearchResult;}


    };

    /**
     * @breif Contains all the information needed to build neighbourhood information for the balanced octree based on treeSearch.
     * */
    class Mesh {

    private :
        //std::vector<ot::TreeNode> m_uiOctree;                         // Stores the 2:1 balanced linear sorted octree.
        std::vector<ot::TreeNode> m_uiPositiveBoundaryOctants;        // Stores the positive boundary nodes.
        /** Element to Element mapping data. Array size: [m_uiAllNodes.size()*m_uiStensilSz*m_uiNumDirections];  But this is done for m_uiStencilSz=1*/
        std::vector<unsigned int> m_uiE2EMapping;
        /** Element to Node (interpolative) mapping data for continous Galerkin methods. Array size: [m_uiAllNodes.size()*m_uiNpE]*/
        //unsigned int *m_uiE2NMapping_CG;
        /** Element ot Node mapping data for discontinous Galerkin methods. Array size: [m_uiAllNodes.size()*m_uiNpE];*/
        std::vector<unsigned int> m_uiE2NMapping_CG;
        /** Element to Node mapping with DG indexing after removing duplicates. This is used for debugging. */
        std::vector<unsigned int> m_uiE2NMapping_DG;

        /**maps each ghost element to it's processor ID.*/
        std::vector<unsigned int > m_uiGhostElement2OwnerProcID;


        /** splitter element for each processor. */
        ot::TreeNode *m_uiSplitter;                                 // used to spit the keys to the correct nodes.

        /**Splitter Node for each processor*/
        ot::TreeNode *m_uiSplitterNodes;

        // The number of nodes owned by the current processor.
        unsigned int m_uiNodeSize;
        unsigned int m_uiBoundaryNodeSize;

        // Pre  and Post ghost octants.

        /** pre ghost elements (will be freed after use)*/
        std::vector<ot::TreeNode> m_uiPreGhostOctants;
        /**post ghost elements (will be freed after use)*/
        std::vector<ot::TreeNode> m_uiPostGhostOctants;


        // Keys Related attributes
        /**search keys generated for local elements. */
        std::vector<Key> m_uiKeys;

        /**search keys generated for ghost elements*/
        std::vector<Key>m_uiGhostKeys;

        /**input to the mesh generation (will be freed after use)*/
        std::vector<ot::TreeNode> m_uiEmbeddedOctree;
        /** Ghost Elements. (will be freed after use)*/
        std::vector<ot::TreeNode> m_uiGhostOctants;

        /** store the indices of round 1 communication.
         *
         * Note: !! You cannot build a m_uiGhostElementRound2Index, because at the round 2 exchange of ghost elements we might
         * need to send a round1 ghost element to another processor.
         *
         * */
        std::vector<unsigned int > m_uiGhostElementRound1Index;


        /** stores all the pre + local + post ELEMENTS. */
        std::vector<ot::TreeNode> m_uiAllElements;
        std::vector<ot::TreeNode> m_uiAllLocalNode;

        /**begin of the pre ghost elements*/
        unsigned int m_uiElementPreGhostBegin = 0; // Not mandatory to store this.
        /** end of pre ghost elements*/
        unsigned int m_uiElementPreGhostEnd;
        /** begin of locat octants (INDEPENDENT)*/
        unsigned int m_uiElementLocalBegin;
        /** end of the local octants (INDEPENDET)*/
        unsigned int m_uiElementLocalEnd;
        /**begin location for the post ghost octants*/
        unsigned int m_uiElementPostGhostBegin;
        /** end location for the post ghost octants*/
        unsigned int m_uiElementPostGhostEnd;
        /**begin of the pre ghost elements of  fake elements*/
        unsigned int m_uiFElementPreGhostBegin;
        /**end of the pre ghost elements of fake elements. */
        unsigned int m_uiFElementPreGhostEnd;
        /**begin of the local fake elements*/
        unsigned int m_uiFElementLocalBegin;
        /**end of the local fake elements*/
        unsigned int m_uiFElementLocalEnd;
        /**begin of the fake post ghost elements*/
        unsigned int m_uiFElementPostGhostBegin;
        /**end of the fake post ghost elements*/
        unsigned int m_uiFElementPostGhostEnd;




        /**Number of local elements. */
        unsigned int m_uiNumLocalElements;
        /**Number of pre ghost elements*/
        unsigned int m_uiNumPreGhostElements;
        /**Number of post ghost elements*/
        unsigned int m_uiNumPostGhostElements;

        /*** Total number of actual elements in the mesh (preG+ local + postG ) */
        unsigned int m_uiNumTotalElements;

        /** Total number of fake elements in the mesh */
        unsigned int m_uiNumTotalFakeElements;


        /** Total number of actual nodes. Defn: Actual nodes are the ones that are owned by actual elememts. */
        unsigned int m_uiNumActualNodes;

        /** Total number of fake nodes. Defn: these nodes are owned my the fake elements. */
        unsigned int m_uiNumFakeNodes;


        /** begin location  of the pre ghost nodes in CG indexing*/
        unsigned int m_uiNodePreGhostBegin;
        /** end location of the pre ghost nodes in CG indexing*/
        unsigned int m_uiNodePreGhostEnd;
        /** begin location of the local nodes in CG indexing*/
        unsigned int m_uiNodeLocalBegin;
        /** end location of the local nodes in CG indexing*/
        unsigned int m_uiNodeLocalEnd;
        /** begin location of the post ghost nodes in CG indexing*/
        unsigned int m_uiNodePostGhostBegin;
        /** end location of the post ghost nodes in CG indexing*/
        unsigned int m_uiNodePostGhostEnd;


        /** store whether each face of a element is hanging or not.
         *
         * Size: Number of total elements.
         * Each char (1 byte) represents that each face of an element is hanning or not.
         *
         * element -> char value
         *
         * value.bit[0]: 1 if left face is hanging
         * value.bit[1]: 1 if right face is hanging
         * value.bit[2]: 1 if down face is hanging
         * value.bit[3]: 1 if up face is hanging
         * value.bit[4]: 1 if back face is hanging
         * value.bit[5]: 1 if front face is hanging.
         * */
        std::vector<char> m_uiE2HangingFaces;

        // Stuff for communication and synchronization ...
        //-------------------------------------------------------------------------

        /**Rank of the current process*/
        int m_uiRank;
        /** size of the Comm */
        int m_uiNpes;

        /** MPI communicator. */
        MPI_Comm m_uiComm;

        /**Number of keys should be sent to each processor. (Note that instead of keys we are sending the owners of keys to the required processor)*/
        unsigned int *m_uiSendKeyCount;
        unsigned int *m_uiRecvKeyCount;

        unsigned int *m_uiSendKeyOffset;
        unsigned int *m_uiRecvKeyOffset;


        /**SendOct counts related to round 1 of ghost exchange. */
        /**Number of ghost octants(elements) that needed to be sent to each processor */
        unsigned int *m_uiSendOctCountRound1;
        /**Number of ghost elemets recieved from each processor. */
        unsigned int *m_uiRecvOctCountRound1;
        /**Ghost send count offset, used in all2allv*/
        unsigned int *m_uiSendOctOffsetRound1;
        /**Ghost recieve count offset, used in all2allv*/
        unsigned int *m_uiRecvOctOffsetRound1;



        /**SendOct counts related to round 2 of ghost exchange. */
        /**Number of ghost octants(elements) that needed to be sent to each processor */
        unsigned int *m_uiSendOctCountRound2;
        /**Number of ghost elemets recieved from each processor. */
        unsigned int *m_uiRecvOctCountRound2;
        /**Ghost send count offset, used in all2allv*/
        unsigned int *m_uiSendOctOffsetRound2;
        /**Ghost recieve count offset, used in all2allv*/
        unsigned int *m_uiRecvOctOffsetRound2;




        /**Number of nodes that needed to be sent to each processor*/
        unsigned int * m_uiSendNodeCount;
        /**Number of nodes that recieved from each processor*/
        unsigned int * m_uiRecvNodeCount;
        /**Send node count offset*/
        unsigned int * m_uiSendNodeOffset;
        /**Recv node count offset*/
        unsigned int * m_uiRecvNodeOffset;


        /** local element ids that needs to be send for other processors. */
        std::vector<unsigned int> m_uiGhostElementIDsToBeSent;
        /** local element ids that recieved from other processors after ghost element exchange*/
        std::vector<unsigned int> m_uiGhostElementIDsToBeRecv;

        /**stores the CG indecies of pre ghost nodes which are hanging, */
        std::vector<unsigned int > m_uiPreGhostHangingNodeCGID;

        /**stores the CG indices of post ghost nodes which are hanging.*/
        std::vector<unsigned int > m_uiPostGhostHangingNodeCGID;



        /**Send buffer of ghost elements (octants)*/
        std::vector<ot::TreeNode> m_uiSendBufferElement;
        /**Actual exchange of node values. This is the actual exhange that happens in element looping*/
        std::vector<double> m_uiSendBufferNodes;
        /**Scatter map for the elements. Keeps track of which local elements need to be sent to which processor. (This has to be derived with m_uiSendOctCount values)
         * Element ID's that is been exchanged at round 1 of ghost exchange.
         * Property: For a given processor rank p the scatter map elements that is set to processor p is sorted and unique.
         * */
        std::vector<unsigned int> m_uiScatterMapElementRound1;

        /**
         * Scatter Map for the elements that is being exchanged at round 2.
         * Property: For a given processor rank p the scatter map elements that is set to processor p is sorted and unique.
         * */
        std::vector<unsigned int > m_uiScatterMapElementRound2;




        /**Scatter map for the actual nodes. Keeps track of which local node needs to be sent to which processor. */
        std::vector<unsigned int > m_uiScatterMapActualNodeSend;

        /** Scatter map for the actual nodes, recieving from other processors. */
        std::vector<unsigned int > m_uiScatterMapActualNodeRecv;

        /**Scatter map for fake nodes, Keeps track which local node which is needed by another fake element resides in a another processor. */
        std::vector<unsigned int >m_uiScatterMapFakeNodes;

        // variables to manage loop access over elements.
        /**counter for the current element*/
        unsigned int m_uiEL_i;

        /**order of the octant elements. */
        unsigned int m_uiElementOrder;

        /**Number of nodes per element*/
        unsigned int m_uiNpE;

        /**Number of neighbours that need to be searched in each direction. */
        unsigned int m_uiStensilSz;

        /**number of total directions that the search is performed*/
        unsigned int m_uiNumDirections;



        /**Reference element to perform interpolations. */
        //RefElement m_uiRefEl;


    private:


        /**
         * @author Milinda Fernando
         * @brief generates search key elements for local elements.
         *
         *
         * */

        void generateSearchKeys();

        /**
         * @author Milinda Fernando
         * @brief generates search keys for ghost elements, inorder to build the E2E mapping between ghost elements.
         * */

        void generateGhostElementSearchKeys();



        /**
         * @author Milinda Fernando
         * @brief Builds the E2E mapping, in the mesh
         * @param [in] in: 2:1 balanced octree
         * @param [in] k_s : Stencil size, how many elements to search in each direction
         * @param [in] comm: MPI Communicator.
         * */
        void buildE2EMap(std::vector<ot::TreeNode> &in,MPI_Comm comm );

        /**
         * @author Milinda Fernando
         * @brief Builds the E2E mapping, in the mesh (sequential case. No ghost nodes. )
         * @param [in] in: 2:1 balanced octree
         * @param [in] k_s : Stencil size, how many elements to search in each direction
         * */
         void buildE2EMap(std::vector<ot::TreeNode> &in);

        /**
         *
         * @author Milinda Fernando
         * @brief Builds the Element to Node (E2N) mapping to enforce the continuity of the solution. (Needed in continous Galerkin methods. )
         * This function assumes that E2E mapping is already built.
         *
         * */

        void buildE2NMap();


        /**
         * @author  Milinda Fernando
         * @brief Computes the overlapping nodes between given two elements. Note that idx ,idy, idz should be integer array size of (m_uiElementOrder + 1).
         * @param [in] Parent : denotes the larger(ancestor) element of the two considering elements.
         * @param [in] child : denotes the desendent from the parent element.
         * @param [out] idx: mapping between x nodes indecies of parent element to x node indecies for the child element.
         * @param [out] idy: mapping between y nodes indecies of parent element to y node indecies for the child element.
         * @param [out] idz: mapping between z nodes indecies of parent element to z node indecies for the child element.
         * @example idx[0]= 2 implies that the parent element x index 0 is given from the child x index 2.
         *
         * */
        inline bool computeOveralppingNodes(const ot::TreeNode & parent, const ot::TreeNode & child, int * idx,int * idy, int * idz);

        /**
         *  @breif Computes the ScatterMap and the send node counts for ghost node exchage.
         *  We do the scatter map exchange in two different steps. Hence we need to compute 2 different scatter maps.
         *      1. Scatter map for actual nodes.
         *      2. Scatter map for fake nodes.
         *  @param[in] MPI_Communicator for scatter map node exchange.
         *
         *
         */
        void computeNodeScatterMaps(MPI_Comm comm);


        /**
         * @brief computes number of hanging nodes, in ghost nodes. In order to call this
         * E2E and E2N and E2HanginFaces mappings should be complete.
         *
         * */

        void computeGhostHanginNodes();

        /**@brief  Removes hanging ghost nodes from E2N mapping.
         *
         * Assumption: Both E2E and E2N mapping should be completed before calling this method.
         *
         * .*/
        void removeHangingGhostNodes();



        /**
         *
         *
         * childf1 and childf2 denotes the edge that happens ar the intersection of those two planes.
         * */

        /**
         * @breif This function is to map internal edge nodes in the LEFT face.
         * @param[in] child element in cordieration for mapping.
         * @param[in] parent or the LEFT neighbour of child
         * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
         * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
         * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
         * */
        inline void OCT_DIR_LEFT_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);
        /**
        * @breif This function is to map internal edge nodes in the RIGHT face.
        * @param[in] child element in cordieration for mapping.
        * @param[in] parent or the RIGHT neighbour of child
        * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
        * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * */
        inline void OCT_DIR_RIGHT_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);
        /**
        * @breif This function is to map internal edge nodes in the DOWN face.
        * @param[in] child element in cordieration for mapping.
        * @param[in] parent or the DOWN neighbour of child
        * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
        * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * */
        inline void OCT_DIR_DOWN_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);
        /**
        * @breif This function is to map internal edge nodes in the UP face.
        * @param[in] child element in cordieration for mapping.
        * @param[in] parent or the UP neighbour of child
        * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
        * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * */
        inline void OCT_DIR_UP_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);
        /**
        * @breif This function is to map internal edge nodes in the FRONT face.
        * @param[in] child element in cordieration for mapping.
        * @param[in] parent or the FRONT neighbour of child
        * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
        * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
        * */
        inline void OCT_DIR_FRONT_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);
        /**
         * @breif This function is to map internal edge nodes in the BACK face.
         * @param[in] child element in cordieration for mapping.
         * @param[in] parent or the BACK neighbour of child
         * @param[in] parentChildLevEqual specifies whether the parent child levels are equal (true) if their equal.
         * @param[in] edgeChildIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
         * @param[in] edgeOwnerIndex std::vector (pre-allocated) for the size of the internal nodes of the edge.
         * */
        inline void OCT_DIR_BACK_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex);


        /**
         * @brief maps the corner nodes of an elements to a  corresponsing elements.
         * @param[in] child element id in corsideration.
         *  */
        inline void CORNER_NODE_MAP(unsigned int child);


        /**
         * @brief Returns the diagonal element ID if considering two faces are simillar in size of elementID.
         * */
        inline void OCT_DIR_DIAGONAL_E2E(unsigned int elementID, unsigned int face1,unsigned int face2,unsigned int& lookUp);





    public:
        /**
         * @author Milinda Fernando
         * @breif Main Construction for the Mesh data structure construction
         * */

        Mesh(std::vector<ot::TreeNode> &in, unsigned int k_s, unsigned int pOrder);
        Mesh(std::vector<ot::TreeNode> &in, unsigned int k_s, unsigned int pOrder,MPI_Comm comm);

        ~Mesh();


        // Setters and getters.
        /** @breif Returns the number of local elements in the grid. (local to the current considering processor)*/
        inline unsigned int getNumLocalMeshElements(){return m_uiNumLocalElements;}
        /** @breif Returns the number of pre-ghost elements */
        inline unsigned int getNumPreGhostElements(){return m_uiNumPreGhostElements;}
        /** @brief Returns the number of post-ghost elements */
        inline unsigned int getNumPostGhostElements(){return m_uiNumPostGhostElements;}
        // get nodal information.

        /**@brief return the number of nodes local to the mesh*/
        inline unsigned int getNumLocalMeshNodes(){return (m_uiNodeLocalEnd-m_uiNodeLocalBegin);}
        /**@brief return the number of pre ghost mesh nodes*/
        inline unsigned int getNumPreMeshNodes(){return (m_uiNodePreGhostEnd-m_uiNodePreGhostBegin);}
        /**@brief return the number of post ghost mesh nodes*/
        inline unsigned int getNumPostMeshNodes(){return (m_uiNodePostGhostEnd-m_uiNodePostGhostBegin);}
        /**@brief return the begin location of element pre ghost*/
        inline unsigned int getElementPreGhostBegin(){return m_uiElementPreGhostBegin;}
        /**@brief return the end location of element pre ghost*/
        inline unsigned int getElementPreGhostEnd(){return m_uiElementPreGhostEnd;}
        /**@brief return the begin location of element local*/
        inline unsigned int getElementLocalBegin(){return m_uiElementLocalBegin;}
        /**@brief return the end location of element local*/
        inline unsigned int getElementLocalEnd(){return m_uiElementLocalEnd;}
        /**@brief return the begin location of element post ghost */
        inline unsigned int getElementPostGhostBegin(){return m_uiElementPostGhostBegin;}
        /**@brief return the end location of element post ghost*/
        inline unsigned int getElementPostGhostEnd(){return m_uiElementPostGhostEnd;}

        /**@brief return the begin location of pre ghost nodes*/
        inline unsigned int getNodePreGhostBegin(){return m_uiNodePreGhostBegin;}
        /**@brief return the end location of pre ghost nodes*/
        inline unsigned int getNodePreGhostEnd(){return m_uiNodePreGhostEnd;}
        /**@brief return the location of local node begin*/
        inline unsigned int getNodeLocalBegin(){return m_uiNodeLocalBegin;}
        /**@brief return the location of local node end*/
        inline unsigned int getNodeLocalEnd(){return m_uiNodeLocalEnd;}
        /**@brief return the location of post node begin*/
        inline unsigned int getNodePostGhostBegin(){return m_uiNodePostGhostBegin;}
        /**@brief return the location of post node end*/
        inline unsigned int getNodePostGhostEnd(){ return m_uiNodePostGhostEnd;}

        /**@brief returns the pointer to All elements array. */
        inline const ot::TreeNode* getPtrAllElements(){return &(*(m_uiAllElements.begin()));}

        /**@brief return the number of directions in the E2E mapping. */
        inline unsigned int getNumDirections(){return m_uiNumDirections;}

        /**@brief return the number of nodes per element.*/
        inline unsigned int getNumNodesPerElement(){return m_uiNpE;}

        inline void dg2eijk(unsigned int dg_index,unsigned int& e, unsigned int & i,unsigned int & j, unsigned int &k)
        {
            e=dg_index/m_uiNpE;
            k=0;j=0;i=0;

            //std::cout<<"e: "<<e<<std::endl;
            if(dg_index>e*m_uiNpE) k=(dg_index-e*m_uiNpE)/((m_uiElementOrder+1)*(m_uiElementOrder+1));
            //std::cout<<"k: "<<k<<std::endl;
            if((dg_index + k*((m_uiElementOrder+1)*(m_uiElementOrder+1)))> (e*m_uiNpE)) j=(dg_index-e*m_uiNpE-k*((m_uiElementOrder+1)*(m_uiElementOrder+1)))/(m_uiElementOrder+1);
            //std::cout<<"j: "<<j<<std::endl;
            if((dg_index+ k*((m_uiElementOrder+1)*(m_uiElementOrder+1))+j*(m_uiElementOrder+1)) >(e*m_uiNpE) ) i=(dg_index-e*m_uiNpE-k*((m_uiElementOrder+1)*(m_uiElementOrder+1))-j*(m_uiElementOrder+1));
            //std::cout<<"i: "<<i<<std::endl;

        }


        /**
         * @brief Returns the direction of the node when i, j, k index of the node given.
         * */
        inline unsigned int getDIROfANode(unsigned int ii_x,unsigned int jj_y,unsigned int kk_z);

        /**
         * @brief decompose the direction to it's ijk values.
         * */

        inline void directionToIJK(unsigned int direction,std::vector<unsigned int > &ii_x, std::vector<unsigned int >& jj_y, std::vector<unsigned int > &kk_z);

        // functions to access, faces and edges of a given element.

        /**
         * @brief Returns the index of m_uiE2NMapping (CG or DG) for a specified face.
         * @param [in] elementID: element ID of the face, that the face belongs to .
         * @param [in] face : Face that you need the indexing,
         * @param [in] isInternal: return only the internal nodes if true hence the index size would be ((m_uiElementOrder-1)*(m_uiElementOrder-1))
         * @param [out] index: returns the indecies of the requested face.
         * */

        inline void faceNodesIndex(unsigned int elementID, unsigned int face, std::vector<unsigned int>& index,
                                   bool isInternal);

        /**
         * @brief Returns the index of m_uiE2NMapping (CG or DG) for a specified Edge.
         * @param [in] elementID: element ID of the edge, that the face belongs to .
         * @param [in] face1 : one of the face, that an edge belongs to
         * @param [in] face2: second face that an edge belongs to . Hence the edge in cosideration will be intersection of the two faces, face1 and face2.
         * @param [in] isInternal: return only the internal nodes if true hence the index size would be ((m_uiElementOrder-1)*(m_uiElementOrder-1))
         * @param [out] index: returns the indecies of the requested face.
         * */

        inline void edgeNodeIndex(unsigned int elementID,unsigned int face1,unsigned int face2, std::vector<unsigned int >& index, bool isInternal);


        /**
        * @brief Returns the index of m_uiE2NMapping (CG or DG) for a specified Edge.
        * @param [in] elementID: element ID of the edge, that the face belongs to .
        * @param [in] mortonIndex: morton ID of the corner node in the cordinate change in the order of x y z.
        * @param [out] index: returns the indecies of the requested face.
        * */

        inline void cornerNodeIndex(unsigned int elementID,unsigned int mortonIndex, unsigned int &index);


        // Wavelet Init functions
        /**
         * @brief Initilizes the wavelet DA loop depending on the WaveletDA flags specified
         * */
        template <ot::WaveletDA::LoopType type>
        void init();

        /**
         * @brief Check whether the next element is available.
         * */

        template <ot::WaveletDA::LoopType type>
        bool nextAvailable();

        /**
         * @brief Increment the counters to access the next element in the mesh.
         * */

        template <ot::WaveletDA::LoopType  type>
        void next();

        /**
         * @brief Return the current element as an octant.
         * */
        inline const ot::TreeNode& currentOctant();

        /**
         * @brief Returns the current element index.
         * */
        inline unsigned  int currentIndex();

        /**
         * @brief Returns the current neighbour list (Element) information. Note that for 3D it is 8 neighbours for each octant and for 2D it is 4.
         * */

        inline void currentElementNeighbourIndexList(unsigned int *neighList);

        /**
         * @brief: Returns the node index list belongs to current element.
         * NodeList size should be m_uiNpE;
         * */

        inline void currentElementNodeList(unsigned int * nodeList);


        /**
         * @brief Returns the node index list belogns to the currentl element in DG indexing.
         * NodeList size should be m_uiNpE;
         *
         * */
        inline void currentElementNodeList_DG(unsigned int * nodeList);


        // Methods needed for PDE & ODE and other solvers.
        template <typename T>
        void createVector(T * vec);

        template <typename T>
        void createVector(std::vector<T> &vec);

        /**
         *
         * @brief Performs all parent to child interpolations for the m_uiEl_i element in order to apply the stencil.
         * @param[in] parent function values
         * @param[out] interpolated values.
         * */
        inline void parent2ChildInterpolation(double * in ,double * out);





    };


    template<>
    inline void Mesh::init<WaveletDA::LoopType ::ALL>(){
        m_uiEL_i=m_uiElementPreGhostBegin;
    }


    template <>
    inline void Mesh::init<WaveletDA::INDEPENDENT>(){
        m_uiEL_i=m_uiElementLocalBegin;
    }

    template <>
    inline void Mesh::init<WaveletDA::DEPENDENT>() {
        m_uiEL_i=m_uiElementPreGhostBegin;
    }

    template <>
    inline bool Mesh::nextAvailable<WaveletDA::ALL>()
    {
        return (m_uiEL_i<m_uiElementPostGhostEnd);
    }


    template <>
    inline bool Mesh::nextAvailable<WaveletDA::INDEPENDENT>()
    {
        return (m_uiEL_i<m_uiElementLocalEnd);
    }
    template <>

    inline bool Mesh::nextAvailable<WaveletDA::DEPENDENT>()
    {
        return (m_uiEL_i<m_uiElementPreGhostEnd) || ((m_uiEL_i>m_uiElementLocalEnd) && m_uiEL_i<m_uiElementPostGhostEnd);
    }

    template <>
    inline void Mesh::next<WaveletDA::ALL>()
    {
        // interpolation should come here.
        m_uiEL_i++;
    }

    template <>
    inline void Mesh::next<WaveletDA::INDEPENDENT>()
    {
        // interpolation should come here.
        m_uiEL_i++;
    }

    template <>
    inline void Mesh::next<WaveletDA::DEPENDENT>()
    {
        // interpolation should come here.
        m_uiEL_i++;
        if(m_uiEL_i==m_uiElementPreGhostEnd)
            m_uiEL_i=m_uiElementPostGhostBegin;
    }


    inline const ot::TreeNode & Mesh::currentOctant()
    {
        return m_uiAllElements[m_uiEL_i];
    }

    inline unsigned int Mesh::currentIndex() {return m_uiEL_i;}

    inline void Mesh::currentElementNeighbourIndexList(unsigned int *neighList)
    {
        for(unsigned int k=0;k<m_uiNumDirections;k++)
            neighList[k]=m_uiE2EMapping[m_uiEL_i*m_uiNumDirections+k];
    }


    inline void Mesh::currentElementNodeList(unsigned int * nodeList)
    {
        for(unsigned int k=0;k<m_uiNpE;k++)
        {
            nodeList[k]=m_uiE2NMapping_CG[m_uiEL_i*m_uiNpE+k];
        }
    }

    inline void Mesh::currentElementNodeList_DG(unsigned int * nodeList)
    {
        for(unsigned int k=0;k<m_uiNpE;k++)
        {
            nodeList[k]=m_uiE2NMapping_DG[m_uiEL_i*m_uiNpE+k];
        }
    }



    template <typename T>
    void Mesh::createVector(T * vec)
    {
       vec=new T[m_uiNumActualNodes];

    }


    template <typename T>
    void Mesh::createVector(std::vector<T> &vec)
    {
        vec.resize(m_uiNumActualNodes);

    }


    inline void Mesh::parent2ChildInterpolation(double * in ,double * out)
    {
        unsigned int cnum=0;
        // to get the morton child number.
        unsigned int mid_bit = m_uiMaxDepth - m_uiAllElements[m_uiEL_i].getLevel();
        cnum = ( (((m_uiAllElements[m_uiEL_i].getZ() >> mid_bit) & 1u) << 2u) | (((m_uiAllElements[m_uiEL_i].getY() >> mid_bit) & 1u) << 1u) | ((m_uiAllElements[m_uiEL_i].getX() >>mid_bit) & 1u));


        if(cnum==0)
        {

        }else if(cnum==1)
        {

        }


    }




    inline bool Mesh::computeOveralppingNodes(const ot::TreeNode & parent, const ot::TreeNode & child, int * idx,int *idy, int * idz)
    {

        unsigned int  Lp =1u<<(m_uiMaxDepth-parent.getLevel());
        unsigned int  Lc =1u<<(m_uiMaxDepth-child.getLevel());
        //intilize the mapping to -1. -1 denotes that mapping is not defined for given k value.

        for (unsigned int k = 0; k < (m_uiElementOrder + 1); k++) {
            idx[k] = -1;
            idy[k] = -1;
            idz[k] = -1;

        }
        bool state=false;
        bool stateX=false;
        bool stateY=false;
        bool stateZ=false;
        if(parent==child) {
            for (unsigned int k = 0; k < (m_uiElementOrder + 1); k++) {
                idx[k] = k;
                idy[k] = k;
                idz[k] = k;

            }
            return true;
        }else if(parent.isAncestor(child))
        {

            /*if((((child.getX()-parent.getX())*m_uiElementOrder)%Lp) || (((child.getY()-parent.getY())*m_uiElementOrder)%Lp) || (((child.getZ()-parent.getZ())*m_uiElementOrder)%Lp)) return false;
            else*/
            {
                unsigned int index[3];
                for (unsigned int k = 0; k < (m_uiElementOrder + 1); k++) {

                    index[0]=(m_uiElementOrder+1);
                    index[1]=(m_uiElementOrder+1);
                    index[2]=(m_uiElementOrder+1);

                    if(!(((child.getX()-parent.getX())*m_uiElementOrder + k*Lc)%Lp))
                        index[0] = ((child.getX()-parent.getX())*m_uiElementOrder + k*Lc)/Lp;

                    if(!(((child.getY()-parent.getY())*m_uiElementOrder + k*Lc)%Lp))
                        index[1] = ((child.getY()-parent.getY())*m_uiElementOrder + k*Lc)/Lp;

                    if(!(((child.getZ()-parent.getZ())*m_uiElementOrder + k*Lc)%Lp))
                        index[2] = ((child.getZ()-parent.getZ())*m_uiElementOrder + k*Lc)/Lp;

                    if(!stateX && index[0]<(m_uiElementOrder+1))
                        stateX=true;

                    if(!stateY && index[1]<(m_uiElementOrder+1))
                        stateY=true;

                    if(!stateZ && index[2]<(m_uiElementOrder+1))
                        stateZ=true;


                    if(index[0]<(m_uiElementOrder+1)) {idx[k]=index[0]; assert((parent.getX()+idx[k]*Lp/m_uiElementOrder)==(child.getX()+k*Lc/m_uiElementOrder));}
                    if(index[1]<(m_uiElementOrder+1)) {idy[k]=index[1]; assert((parent.getY()+idy[k]*Lp/m_uiElementOrder)==(child.getY()+k*Lc/m_uiElementOrder));}
                    if(index[2]<(m_uiElementOrder+1)) {idz[k]=index[2]; assert((parent.getZ()+idz[k]*Lp/m_uiElementOrder)==(child.getZ()+k*Lc/m_uiElementOrder));}

                }
                state=stateX & stateY & stateZ;
                return state;

            }

        }else
        {
            return false;
        }




    }


    inline void Mesh::faceNodesIndex(unsigned int elementID, unsigned int face, std::vector<unsigned int>& index, bool isInternal){


        unsigned int begin=0;
        unsigned int end=(m_uiElementOrder+1);
        unsigned int numPts1D;

        if(isInternal) {
            begin = 1;
            end = (m_uiElementOrder);
        }
        numPts1D=(end-begin);
        if(numPts1D==0)
        {
            index.clear();
            return;
        }

        index.resize(numPts1D*numPts1D);


        if(face ==OCT_DIR_LEFT)
        {
            for(unsigned int k=begin;k<end;k++)
            {
                for(unsigned int j=begin;j<end;j++)
                {
                    index[k*(numPts1D)*j]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+0;
                }
            }

        }else if(face==OCT_DIR_RIGHT)
        {
            for(unsigned int k=begin;k<end;k++)
            {
                for(unsigned int j=begin;j<end;j++)
                {
                    index[k*(numPts1D)*j]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+m_uiElementOrder;
                }
            }


        }else if(face==OCT_DIR_DOWN)
        {

            for(unsigned int k=begin;k<end;k++)
            {
                for(unsigned int i=begin;i<end;i++)
                {
                    index[k*(numPts1D)*i]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+i;
                }
            }

        }else if(face==OCT_DIR_UP)
        {

            for(unsigned int k=begin;k<end;k++)
            {
                for(unsigned int i=begin;i<end;i++)
                {
                    index[k*(numPts1D)*i]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+i;
                }
            }

        }else if(face==OCT_DIR_FRONT)
        {

            for(unsigned int j=begin;j<end;j++)
            {
                for(unsigned int i=begin;i<end;i++)
                {
                    index[j*(numPts1D)*i]=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i;
                }
            }

        }else if(face==OCT_DIR_BACK)
        {

            for(unsigned int j=begin;j<end;j++)
            {
                for(unsigned int i=begin;i<end;i++)
                {
                    index[j*(numPts1D)*i]=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+i;
                }
            }

        }


    }


    inline void Mesh::edgeNodeIndex(unsigned int elementID,unsigned int face1,unsigned int face2, std::vector<unsigned int >& index, bool isInternal)
    {
        unsigned int begin=0;
        unsigned int end=(m_uiElementOrder+1);
        unsigned int numPts1D;

        if(isInternal) {
            begin = 1;
            end = (m_uiElementOrder);
        }
        numPts1D=(end-begin);
        if(numPts1D==0)
        {
            index.clear();
            return;
        }

        index.resize(numPts1D);

        if(((face1==OCT_DIR_LEFT)|| (face1==OCT_DIR_UP)) && ( (face2==OCT_DIR_UP) || (face2==OCT_DIR_LEFT) ))
        {
            for(unsigned int k=begin;k<end;k++)
                index[k]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+0;

        }else if(((face1==OCT_DIR_LEFT)|| (face1==OCT_DIR_DOWN)) && ( (face2==OCT_DIR_DOWN) || (face2==OCT_DIR_LEFT) ))
        {
            for(unsigned int k=begin;k<end;k++)
                index[k]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+0;

        }else if(((face1==OCT_DIR_LEFT)|| (face1==OCT_DIR_FRONT)) && ( (face2==OCT_DIR_FRONT) || (face2==OCT_DIR_LEFT) ))
        {
            for(unsigned int j=begin;j<end;j++)
                index[j]=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+0;

        }else if(((face1==OCT_DIR_LEFT)|| (face1==OCT_DIR_BACK)) && ( (face2==OCT_DIR_BACK) || (face2==OCT_DIR_LEFT) ))
        {
            for(unsigned int j=begin;j<end;j++)
                index[j]=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+0;

        }else if(((face1==OCT_DIR_RIGHT)|| (face1==OCT_DIR_UP)) && ( (face2==OCT_DIR_UP) || (face2==OCT_DIR_RIGHT) )) {

            for(unsigned int k=begin;k<end;k++)
            index[k]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(((face1==OCT_DIR_RIGHT)|| (face1==OCT_DIR_DOWN)) && ( (face2==OCT_DIR_DOWN) || (face2==OCT_DIR_RIGHT) ))
        {

            for(unsigned int k=begin;k<end;k++)
                index[k]=elementID*m_uiNpE+k*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(((face1==OCT_DIR_RIGHT)|| (face1==OCT_DIR_FRONT)) && ( (face2==OCT_DIR_FRONT) || (face2==OCT_DIR_RIGHT) ))
        {

            for(unsigned int j=begin;j<end;j++)
                index[j]=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(((face1==OCT_DIR_RIGHT)|| (face1==OCT_DIR_BACK)) && ( (face2==OCT_DIR_BACK) || (face2==OCT_DIR_RIGHT) ))
        {

            for(unsigned int j=begin;j<end;j++)
                index[j]=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+j*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(((face1==OCT_DIR_UP)|| (face1==OCT_DIR_FRONT)) && ( (face2==OCT_DIR_FRONT) || (face2==OCT_DIR_UP) ))
        {
            for(unsigned int i=begin;i<end;i++)
                index[i]=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+ i;

        }else if(((face1==OCT_DIR_UP)|| (face1==OCT_DIR_BACK)) && ( (face2==OCT_DIR_BACK) || (face2==OCT_DIR_UP) ))
        {
            for(unsigned int i=begin;i<end;i++)
                index[i]=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+ i;

        }else if(((face1==OCT_DIR_DOWN)|| (face1==OCT_DIR_FRONT)) && ( (face2==OCT_DIR_FRONT) || (face2==OCT_DIR_DOWN) ))
        {

            for(unsigned int i=begin;i<end;i++)
                index[i]=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+ i;

        }else if(((face1==OCT_DIR_DOWN)|| (face1==OCT_DIR_BACK)) && ( (face2==OCT_DIR_BACK) || (face2==OCT_DIR_DOWN) ))
        {
            for(unsigned int i=begin;i<end;i++)
                index[i]=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+ i;
        }





    }

    inline void Mesh::cornerNodeIndex(unsigned int elementID,unsigned int mortonIndex, unsigned int &index)
    {
        if(mortonIndex==0)
        {
            index=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+0;
        }else if(mortonIndex==1)
        {
            index=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+m_uiElementOrder;
        }else if(mortonIndex==2)
        {
            index=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+0;

        }else if(mortonIndex==3)
        {
            index=elementID*m_uiNpE+0*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(mortonIndex==4)
        {
            index=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+0;

        }else if(mortonIndex==5)
        {
            index=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+0*(m_uiElementOrder+1)+m_uiElementOrder;

        }else if(mortonIndex==6)
        {
            index=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+0;

        }else if(mortonIndex==7)
        {
            index=elementID*m_uiNpE+m_uiElementOrder*(m_uiElementOrder+1)*(m_uiElementOrder+1)+m_uiElementOrder*(m_uiElementOrder+1)+m_uiElementOrder;
        }


    }



    inline void Mesh::OCT_DIR_DIAGONAL_E2E(unsigned int elementID, unsigned int face1,unsigned int face2,unsigned int& lookUp)
    {

        assert(elementID<m_uiAllElements.size());
        unsigned int myLev=m_uiAllElements[elementID].getLevel();
       /* unsigned int lookUpF1;
        unsigned int lookUpF2;*/

        lookUp=m_uiE2EMapping[elementID*m_uiNumDirections+face1];

     /*   if(m_uiRank==0 && elementID==65 && face1==OCT_DIR_RIGHT && face2==OCT_DIR_BACK)
        {
            lookUp=m_uiE2EMapping[elementID*m_uiNumDirections+OCT_DIR_BACK];
            std::cout<<RED<<" element: "<<m_uiAllElements[elementID]<<" RB :"<<m_uiAllElements[lookUp]<<NRM<<std::endl;
        }*/


       /* if(face1==OCT_DIR_LEFT  && face2==OCT_DIR_BACK)
        {
            if(lookUpF1!=LOOK_UP_TABLE_DEFAULT)
                lookUp=m_uiE2EMapping[lookUpF1*m_uiNumDirections+OCT_DIR_BACK];

        }else if(face1==OCT_DIR_BACK && face2==OCT_DIR_LEFT)
        {
            if(lookUpF1!=LOOK_UP_TABLE_DEFAULT)
                lookUp=m_uiE2EMapping[lookUpF1*m_uiNumDirections+OCT_DIR_LEFT];
        }else if(face1==OCT_DIR_LEFT && face2==OCT_DIR_FRONT)
        {
            if(lookUpF1!=LOOK_UP_TABLE_DEFAULT && myLev<=m_uiAllElements[lookUpF1].getLevel())
            {
                lookUp=m_uiE2EMapping[lookUpF1*m_uiNumDirections+OCT_DIR_FRONT];
            }else
            {
                assert(abs(myLev-m_uiAllElements[lookUpF1].getLevel())==1);


            }
        }*/




        if(lookUp!=LOOK_UP_TABLE_DEFAULT)
        {
            if(myLev>=m_uiAllElements[lookUp].getLevel())
                lookUp=m_uiE2EMapping[lookUp*m_uiNumDirections+face2];
            else
                lookUp=LOOK_UP_TABLE_DEFAULT;

        }else
        {
            lookUp=m_uiE2EMapping[elementID*m_uiNumDirections+face2];
            if(lookUp!=LOOK_UP_TABLE_DEFAULT)
            {
                if(myLev>=m_uiAllElements[lookUp].getLevel())
                    lookUp=m_uiE2EMapping[lookUp*m_uiNumDirections+face1];
                else
                    lookUp=LOOK_UP_TABLE_DEFAULT;
            }

        }



    }


    inline void Mesh::OCT_DIR_LEFT_INTERNAL_EDGE_MAP(unsigned int child,unsigned int parent,bool parentChildLevEqual,std::vector<unsigned int >& edgeChildIndex,std::vector<unsigned int>& edgeOwnerIndex)
    {
        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;
        // -- EDGE OF Element e , (OCT_DIR_LEFT, OCT_DIR_UP)
        edgeOwner=parent;

        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_UP];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_LEFT,OCT_DIR_UP,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_RIGHT,OCT_DIR_UP,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_RIGHT,OCT_DIR_DOWN,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_LEFT,OCT_DIR_DOWN,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];




        // -- EDGE OF Element e , (OCT_DIR_LEFT,OCT_DIR_DOWN)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_DOWN];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }


        edgeNodeIndex(child,OCT_DIR_LEFT,OCT_DIR_DOWN,edgeChildIndex,true);


        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_RIGHT,OCT_DIR_DOWN,edgeOwnerIndex,true);
        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_RIGHT,OCT_DIR_UP,edgeOwnerIndex,true);
        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_LEFT,OCT_DIR_UP,edgeChildIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        //-----------------------------------------------------


        // EDGE of Element e (OCT_DIR_LEFT, OCT_DIR_FRONT)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_FRONT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_LEFT,OCT_DIR_FRONT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_RIGHT,OCT_DIR_FRONT,edgeOwnerIndex,true);
        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_RIGHT,OCT_DIR_BACK,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_LEFT,OCT_DIR_BACK,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];




        // EDGE of Element e (OCT_DIR_LEFT, OCT_DIR_BACK)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_BACK];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_LEFT,OCT_DIR_BACK,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_RIGHT,OCT_DIR_BACK,edgeOwnerIndex,true);
        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_RIGHT,OCT_DIR_FRONT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_LEFT,OCT_DIR_FRONT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];
    }

    inline void Mesh::OCT_DIR_RIGHT_INTERNAL_EDGE_MAP(unsigned int child, unsigned int parent, bool parentChildLevEqual, std::vector<unsigned int> &edgeChildIndex, std::vector<unsigned int> &edgeOwnerIndex)
    {
        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;
        // Edge of element e (OCT_DIR_RIGHT,OCT_DIR_UP)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_UP];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_RIGHT,OCT_DIR_UP,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_LEFT,OCT_DIR_UP,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_LEFT,OCT_DIR_DOWN,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_RIGHT,OCT_DIR_DOWN,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of element e (OCT_DIR_RIGHT,OCT_DIR_DOWN)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_DOWN];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_RIGHT,OCT_DIR_DOWN,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_LEFT,OCT_DIR_DOWN,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_LEFT,OCT_DIR_UP,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_RIGHT,OCT_DIR_UP,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of element e (OCT_DIR_RIGHT,OCT_DIR_FRONT)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_FRONT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_RIGHT,OCT_DIR_FRONT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_LEFT,OCT_DIR_FRONT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_LEFT,OCT_DIR_BACK,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_RIGHT,OCT_DIR_BACK,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of element e (OCT_DIR_RIGHT,OCT_DIR_BACK)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_BACK];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_RIGHT,OCT_DIR_BACK,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_LEFT,OCT_DIR_BACK,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_LEFT,OCT_DIR_FRONT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_RIGHT,OCT_DIR_FRONT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];

    }


    inline void Mesh::OCT_DIR_DOWN_INTERNAL_EDGE_MAP(unsigned int child, unsigned int parent, bool parentChildLevEqual, std::vector<unsigned int> &edgeChildIndex, std::vector<unsigned int> &edgeOwnerIndex)
    {

        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;

        // Edge of element e (OCT_DIR_DOWN,OCT_DIR_RIGHT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_RIGHT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_DOWN,OCT_DIR_RIGHT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_UP,OCT_DIR_RIGHT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_UP,OCT_DIR_LEFT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_DOWN,OCT_DIR_LEFT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];


        // Edge of element e (OCT_DIR_DOWN,OCT_DIR LEFT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_LEFT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_DOWN,OCT_DIR_LEFT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_UP,OCT_DIR_LEFT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_UP,OCT_DIR_RIGHT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_DOWN,OCT_DIR_RIGHT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];




        // Edge of element e (OCT_DIR_DOWN,OCT_DIR_FRONT)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_FRONT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_DOWN,OCT_DIR_FRONT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_UP,OCT_DIR_FRONT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_UP,OCT_DIR_BACK,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_DOWN,OCT_DIR_BACK,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of element e (OCT_DIR_DOWN,OCT_DIR_BACK)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_BACK];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_DOWN,OCT_DIR_BACK,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_UP,OCT_DIR_BACK,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_UP,OCT_DIR_FRONT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_DOWN,OCT_DIR_FRONT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];
    }

    inline void Mesh::OCT_DIR_UP_INTERNAL_EDGE_MAP(unsigned int child, unsigned int parent, bool parentChildLevEqual, std::vector<unsigned int> &edgeChildIndex, std::vector<unsigned int> &edgeOwnerIndex)
    {

        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;


        // Edge of element e (OCT_DIR_UP,OCT_DIR_LEFT)


        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_LEFT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_UP,OCT_DIR_LEFT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_DOWN,OCT_DIR_LEFT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_DOWN,OCT_DIR_RIGHT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_UP,OCT_DIR_RIGHT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of Element e (OCT_DIR_UP, OCT_DIR_RIGHT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_RIGHT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_UP,OCT_DIR_RIGHT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_DOWN,OCT_DIR_RIGHT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_DOWN,OCT_DIR_LEFT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_UP,OCT_DIR_LEFT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];





        // Edge of element e (OCT_DIR_UP,OCT_DIR_FRONT)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_FRONT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_UP,OCT_DIR_FRONT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_DOWN,OCT_DIR_FRONT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_DOWN,OCT_DIR_BACK,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_UP,OCT_DIR_BACK,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // Edge of element e (OCT_DIR_UP,OCT_DIR_BACK)
        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_BACK];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_UP,OCT_DIR_BACK,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_DOWN,OCT_DIR_BACK,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_DOWN,OCT_DIR_FRONT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_UP,OCT_DIR_FRONT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];

    }

    inline void Mesh::OCT_DIR_BACK_INTERNAL_EDGE_MAP(unsigned int child, unsigned int parent, bool parentChildLevEqual, std::vector<unsigned int> &edgeChildIndex, std::vector<unsigned int> &edgeOwnerIndex)
    {

        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;

        // edge of the element e (OCT_DIR_BACK, OCT_DIR_LEFT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_LEFT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_BACK,OCT_DIR_LEFT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_FRONT,OCT_DIR_LEFT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_FRONT,OCT_DIR_RIGHT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_BACK,OCT_DIR_RIGHT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // edge of the element e (OCT_DIR_BACK, OCT_DIR_RIGHT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_RIGHT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_BACK,OCT_DIR_RIGHT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_FRONT,OCT_DIR_RIGHT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_FRONT,OCT_DIR_LEFT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_BACK,OCT_DIR_LEFT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];


        // edge of the element e (OCT_DIR_BACK, OCT_DIR_DOWN)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_DOWN];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_BACK,OCT_DIR_DOWN,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_FRONT,OCT_DIR_DOWN,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_FRONT,OCT_DIR_UP,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_BACK,OCT_DIR_UP,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];



        // edge of the element e (OCT_DIR_BACK, OCT_DIR_UP)


        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_UP];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_BACK,OCT_DIR_UP,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_FRONT,OCT_DIR_UP,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_FRONT,OCT_DIR_DOWN,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_BACK,OCT_DIR_DOWN,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];





    }

    inline void Mesh::OCT_DIR_FRONT_INTERNAL_EDGE_MAP(unsigned int child, unsigned int parent, bool parentChildLevEqual, std::vector<unsigned int> &edgeChildIndex, std::vector<unsigned int> &edgeOwnerIndex)
    {

        unsigned int edgeOwner;
        unsigned int lookUp1,lookUp2;

        // edge of the element e (OCT_DIR_FRONT, OCT_DIR_LEFT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_LEFT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_FRONT,OCT_DIR_LEFT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_BACK,OCT_DIR_LEFT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_BACK,OCT_DIR_RIGHT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_FRONT,OCT_DIR_RIGHT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];

        // edge of the element e (OCT_DIR_FRONT, OCT_DIR_RIGHT)

        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_RIGHT];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_FRONT,OCT_DIR_RIGHT,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_BACK,OCT_DIR_RIGHT,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_BACK,OCT_DIR_LEFT,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_FRONT,OCT_DIR_LEFT,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];

        // edge of the element e (OCT_DIR_FRONT, OCT_DIR_DOWN)


        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_DOWN];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_FRONT,OCT_DIR_DOWN,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_BACK,OCT_DIR_DOWN,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_BACK,OCT_DIR_UP,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_FRONT,OCT_DIR_UP,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];


        // edge of the element e (OCT_DIR_FRONT, OCT_DIR_UP)


        edgeOwner=parent;
        lookUp1=m_uiE2EMapping[parent*m_uiNumDirections+OCT_DIR_UP];
        if(lookUp1!=LOOK_UP_TABLE_DEFAULT)
        {
            if((m_uiAllElements[lookUp1].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp1].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp1<edgeOwner)))
                edgeOwner=lookUp1;
        }

        if(parentChildLevEqual)
        {
            lookUp2=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
            if(lookUp2!=LOOK_UP_TABLE_DEFAULT)
            {
                if((m_uiAllElements[lookUp2].getLevel()< m_uiAllElements[edgeOwner].getLevel()) || ((m_uiAllElements[lookUp2].getLevel()==m_uiAllElements[edgeOwner].getLevel()) && (lookUp2<edgeOwner)))
                    edgeOwner=lookUp2;
            }
        }

        edgeNodeIndex(child,OCT_DIR_FRONT,OCT_DIR_UP,edgeChildIndex,true);

        if(edgeOwner==parent)
        {
            edgeNodeIndex(parent,OCT_DIR_BACK,OCT_DIR_UP,edgeOwnerIndex,true);


        }else if(edgeOwner==lookUp1)
        {
            edgeNodeIndex(lookUp1,OCT_DIR_BACK,OCT_DIR_DOWN,edgeOwnerIndex,true);

        }else
        {
            assert(edgeOwner==lookUp2);
            edgeNodeIndex(lookUp2,OCT_DIR_FRONT,OCT_DIR_DOWN,edgeOwnerIndex,true);
        }

        assert(edgeChildIndex.size()==edgeOwnerIndex.size());
        for(unsigned int index=0;index<edgeChildIndex.size();index++)
            m_uiE2NMapping_CG[edgeChildIndex[index]]=m_uiE2NMapping_CG[edgeOwnerIndex[index]];


    }


    inline void Mesh::CORNER_NODE_MAP(unsigned int child)
    {


        // DEBUG HELPER CODE TO figure out Nodal mismatches. Be really carefull when changing this function :)
        /*if(m_uiRank==1) {
            std::vector<ot::TreeNode> cusEleCheck;
            cusEleCheck.push_back(m_uiAllElements[child]);
            unsigned int ownerID, ii_x, jj_y, kk_z, x, y, z, sz;
            dg2eijk(cornerOwnerIndex,ownerID,ii_x,jj_y,kk_z);
            x = m_uiAllElements[ownerID].getX();
            y = m_uiAllElements[ownerID].getY();
            z = m_uiAllElements[ownerID].getZ();
            sz = 1u << (m_uiMaxDepth - m_uiAllElements[ownerID].getLevel());
            cusEleCheck.push_back(
                    ot::TreeNode((x + ii_x * sz / m_uiElementOrder), (y + jj_y * sz / m_uiElementOrder),
                                 (z + kk_z * sz / m_uiElementOrder), m_uiMaxDepth, m_uiDim, m_uiMaxDepth));
            treeNodesTovtk(cusEleCheck,child,"cusEleCheck");
        }*/


        unsigned int cornerOwner;
        unsigned int cornerOwnerIndex;
        unsigned int cornerChildIndex;
        unsigned int numLookUp=6;
        unsigned int lookUp[numLookUp];

        // MORTON INDEX 0 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,0,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_LEFT,OCT_DIR_BACK,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_DOWN,OCT_DIR_LEFT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_BACK,OCT_DIR_DOWN,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }

        assert(cornerOwner<m_uiAllElements.size());

        if(cornerOwner==lookUp[0])
        {
           cornerNodeIndex(lookUp[0],1,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],2,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],4,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],5,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],3,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],6,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];


        // MORTON INDEX 1 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,1,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_RIGHT,OCT_DIR_BACK,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_DOWN,OCT_DIR_RIGHT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_BACK,OCT_DIR_DOWN,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],0,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],3,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],5,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],4,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],2,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],7,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];


        // MORTON INDEX 2 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,2,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_LEFT,OCT_DIR_BACK,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_UP,OCT_DIR_LEFT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_BACK,OCT_DIR_UP,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],3,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],0,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],6,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],7,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],1,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],4,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];



        // MORTON INDEX 3 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,3,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_BACK];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_RIGHT,OCT_DIR_BACK,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_UP,OCT_DIR_RIGHT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_BACK,OCT_DIR_UP,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],2,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],1,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],7,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],6,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],0,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],5,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];


        // MORTON INDEX 4 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,4,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_LEFT,OCT_DIR_FRONT,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_DOWN,OCT_DIR_LEFT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_FRONT,OCT_DIR_DOWN,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],5,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],6,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],0,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],1,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],7,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],2,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];


        // MORTON INDEX 5 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,5,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_DOWN];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_RIGHT,OCT_DIR_FRONT,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_DOWN,OCT_DIR_RIGHT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_FRONT,OCT_DIR_DOWN,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],4,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],7,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],1,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],0,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],6,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],3,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];


        // MORTON INDEX 6 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,6,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_LEFT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_LEFT,OCT_DIR_FRONT,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_UP,OCT_DIR_LEFT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_FRONT,OCT_DIR_UP,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],7,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {

            cornerNodeIndex(lookUp[1],4,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],2,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],3,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],5,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],0,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];



        // MORTON INDEX 7 NODE.
        cornerOwner=child;
        cornerNodeIndex(child,7,cornerChildIndex);

        lookUp[0]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_RIGHT];
        lookUp[1]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_UP];
        lookUp[2]=m_uiE2EMapping[child*m_uiNumDirections+OCT_DIR_FRONT];

        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_RIGHT,OCT_DIR_FRONT,lookUp[3]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_UP,OCT_DIR_RIGHT,lookUp[4]);
        OCT_DIR_DIAGONAL_E2E(child,OCT_DIR_FRONT,OCT_DIR_UP,lookUp[5]);


        for(unsigned int w=0;w<numLookUp;w++)
        {
            if( (lookUp[w]!=LOOK_UP_TABLE_DEFAULT ) && ((m_uiAllElements[lookUp[w]].getLevel()<m_uiAllElements[cornerOwner].getLevel()) || ((m_uiAllElements[lookUp[w]].getLevel()==m_uiAllElements[cornerOwner].getLevel()) && (lookUp[w]<cornerOwner))))
                cornerOwner=lookUp[w];
        }


        if(cornerOwner==lookUp[0])
        {
            cornerNodeIndex(lookUp[0],6,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[1])
        {
            cornerNodeIndex(lookUp[1],5,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[2])
        {
            cornerNodeIndex(lookUp[2],3,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[3])
        {
            cornerNodeIndex(lookUp[3],2,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[4])
        {
            cornerNodeIndex(lookUp[4],4,cornerOwnerIndex);

        }else if(cornerOwner==lookUp[5])
        {
            cornerNodeIndex(lookUp[5],1,cornerOwnerIndex);
        }

        if(cornerOwner!=child) m_uiE2NMapping_CG[cornerChildIndex]=m_uiE2NMapping_CG[cornerOwnerIndex];



    }



    inline unsigned int Mesh::getDIROfANode(unsigned int ii_x,unsigned int jj_y,unsigned int kk_z)
    {
        // stateX (0-> internal 1-> corner)
        bool stateX[3];
        bool stateY[3];
        bool stateZ[3];

        stateX[0]= ((ii_x>0) && (ii_x<m_uiElementOrder));
        stateX[1]= (ii_x==0);
        stateX[2]= (ii_x==m_uiElementOrder);

        stateY[0]= ((jj_y>0) && (jj_y<m_uiElementOrder));
        stateY[1]= (jj_y==0);
        stateY[2]= (jj_y==m_uiElementOrder);

        stateZ[0]= ((kk_z>0) && (kk_z<m_uiElementOrder));
        stateZ[1]= (kk_z==0);
        stateZ[2]= (kk_z==m_uiElementOrder);

        if(stateX[0] && stateY[0] && stateZ[0])
            return OCT_DIR_INTERNAL;

        if(stateX[1] && stateY[1] && stateZ[1]) // morton index 0
            return OCT_DIR_LEFT_DOWN_BACK;

        if(stateX[2] && stateY[1] && stateZ[1]) // morton index 1
            return OCT_DIR_RIGHT_DOWN_BACK;

        if(stateX[1] && stateY[2] && stateZ[1]) // morton index 2
            return OCT_DIR_LEFT_UP_BACK;

        if(stateX[2] && stateY[2] && stateZ[1]) // morton index 3
            return OCT_DIR_RIGHT_UP_BACK;

        if(stateX[1] && stateY[1] && stateZ[2]) // morton index 4
            return OCT_DIR_LEFT_DOWN_FRONT;

        if(stateX[2] && stateY[1] && stateZ[2]) // morton index 5
            return OCT_DIR_RIGHT_DOWN_FRONT;

        if(stateX[1] && stateY[2] && stateZ[2]) // morton index 6
            return OCT_DIR_LEFT_UP_FRONT;

        if(stateX[2] && stateY[2] && stateZ[2]) // morton index 7
            return OCT_DIR_RIGHT_UP_FRONT;

        // internal faces.

        if(stateZ[0] && stateY[0] && stateX[1])
            return OCT_DIR_LEFT;

        if(stateZ[0] && stateY[0] && stateX[2])
            return OCT_DIR_RIGHT;

        if(stateZ[0] && stateX[0] && stateY[1])
            return OCT_DIR_DOWN;

        if(stateZ[0] && stateZ[0] && stateY[2])
            return OCT_DIR_UP;

        if(stateX[0] && stateY[0] && stateZ[1])
            return OCT_DIR_BACK;

        if(stateX[0] && stateY[0] && stateZ[2])
            return OCT_DIR_FRONT;

        // internal corners.

        if(stateZ[0] && stateX[1] && stateY[1])
            return OCT_DIR_LEFT_DOWN;

        if(stateZ[0] && stateX[1] && stateY[2])
            return OCT_DIR_LEFT_UP;

        if(stateY[0] && stateX[1] && stateZ[1])
            return OCT_DIR_LEFT_BACK;

        if(stateY[0] && stateX[1] && stateZ[2])
            return OCT_DIR_LEFT_FRONT;


        if(stateZ[0] && stateX[2] && stateY[1])
            return OCT_DIR_RIGHT_DOWN;

        if(stateZ[0] && stateX[2] && stateY[2])
            return OCT_DIR_RIGHT_UP;

        if(stateY[0] && stateX[2] && stateZ[1])
            return OCT_DIR_RIGHT_BACK;

        if(stateY[0] && stateX[2] && stateZ[2])
            return OCT_DIR_RIGHT_FRONT;



        if(stateX[0] && stateZ[1] && stateY[1])
            return OCT_DIR_DOWN_BACK;

        if(stateX[0] && stateZ[2] && stateY[1])
            return OCT_DIR_DOWN_FRONT;


        if(stateX[0] && stateZ[1] && stateY[2])
            return OCT_DIR_UP_BACK;

        if(stateX[0] && stateZ[2] && stateY[2])
            return OCT_DIR_UP_FRONT;



    }




}









#endif //SFCSORTBENCH_MESH_H
