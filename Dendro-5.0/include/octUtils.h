//
// Created by milinda on 9/6/16.
//

#ifndef SFCSORTBENCH_OCTUTILS_H
#define SFCSORTBENCH_OCTUTILS_H
/**
  @brief A collection of simple functions for manipulating octrees.
Examples: Regular Refinements, Linearizing an octree, I/O,
Nearest Common Ancestor, adding positive boundaries, marking hanging nodes
@author Rahul S. Sampath, rahul.sampath@gmail.com
@author Hari Sundar, hsundar@gmail.com
@author Milinda Fernando ,milinda@cs.utah.edu

 @remarks Most of the functions used for the mesh generation. Most of the implementations are based on the previous implementation of dendro version 4.0

*/


#include "TreeNode.h"
#include <vector>
#include <assert.h>
#include "mpi.h"
#include "parUtils.h"


/**
 * @author Rahul Sampath
 * @author Hari Sundar
 *
 * @breif add the positive boundary octants for the given octree input.
 * */
void addBoundaryNodesType1(std::vector<ot::TreeNode> &in,
                           std::vector<ot::TreeNode>& bdy,
                           unsigned int dim, unsigned int maxDepth);




int refineOctree(const std::vector<ot::TreeNode> & inp,
                 std::vector<ot::TreeNode> &out);

int refineAndPartitionOctree(const std::vector<ot::TreeNode> & inp,
                             std::vector<ot::TreeNode> &out, MPI_Comm comm);


int createRegularOctree(std::vector<ot::TreeNode>& out, unsigned int lev,
                        unsigned int dim, unsigned int maxDepth, MPI_Comm comm);

#endif //SFCSORTBENCH_OCTUTILS_H
