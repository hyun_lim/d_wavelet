echo 'upload all cookbooks from /chef-repo/cookbooks to Chef server (also on head)'
knife cookbook upload -a 
echo 'upload all roles from /chef-repo/roles to Chef server (also on head)'
knife role from file /chef-repo/roles/*.rb
echo 'assign cookbooks emulab-shiny to head'
knife node run_list add head "recipe[emulab-shiny]"

echo 'Running chef-client'
chef-client
