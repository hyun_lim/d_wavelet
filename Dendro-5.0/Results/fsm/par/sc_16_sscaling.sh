#!/bin/bash

export OMP_NUM_THREADS=1

dim=3
md=30
numPts=1000000000
grain=$numPts

numProcs=1

for i in `seq 1 6`;
        do
	numProcs=$((numProcs * 2))
	grain=$(($numPts / $numProcs))
        echo 'executing strong scaling for morton : '  $numProcs $grain
	mpirun -np $numProcs build_m/./SFCSortBench $grain $dim $md 0.0001 0
	sleep 2
	echo 'executing strong scaling for hilbert : '  $numProcs $grain
        mpirun -np $numProcs build_h/./SFCSortBench $grain $dim $md 0.0001 0
        sleep 2
done   


