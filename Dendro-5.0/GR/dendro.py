##########################################################################
# module: dendro
# author: Hari Sundar
# email:  hari@cs.utah.edu
#
# python module to generate efficient code for General Relativity.
#
# ©2016 University of Utah, All rights reserved.
##########################################################################

from sympy import *
from sympy.tensor.array import *
from sympy.functions.special.tensor_functions import KroneckerDelta

## internal variables
undef = symbols('undefined')

metric = undef
inv_metric = undef
C1 = undef
C2 = undef
C3 = undef # C2_spatial

d  = undef # first derivative
d2 = undef # second derivative

l1, l2, l3, l4, eta = symbols('lambda[0] lambda[1] lambda[2] lambda[3] eta')

Ricci = undef

##########################################################################
## variable initialization functions
##########################################################################


def scalar(name):
    """
    Create a scalar variable with the corresponding name. The 'name' will be during code generation, so should match the
    variable name used in the C++ code.
    """
    return symbols(name)


def vec3(name):
    """
    Create a 3D vector variable with the corresponding name. The 'name' will be during code generation, so should match
    the variable name used in the C++ code. The returned variable can be indexed(0,1,2), i.e.,

    b = dendro.vec3("beta")
    b[1] = x^2
    """
    vname = ' '.join([name + repr(i) for i in [0, 1, 2]])
    return symbols(vname)


def sym_3x3(name):
    """
    Create a symmetric 3x3 matrix variables with the corresponding name. The 'name' will be during code generation, so
    should match the variable name used in the C++ code. The returned variable can be indexed(0,1,2)^2, i.e.,

    gt = dendro.sym_3x3("gt")
    gt[0,2] = x^2
    """

    vname = ' '.join([name + repr(i) for i in range(6)])
    m1, m2, m3, m4, m5, m6 = symbols(vname)

    return Matrix([[m1, m2, m3], [m2, m4, m5], [m3, m5, m6]])


##########################################################################
## derivative related functions
##########################################################################


def set_first_derivative(g):
    """
    Set how the stencil for the first derivative will be called. Here g is a string

    Typically,

    d_i u =  g(i, u)
    """
    global d
    d = Function(g)
    return d


def set_second_derivative(g):
    """
    Set how the stencil for the second derivative will be called. Here g is a string

    Typically,

    d_ij u =  g(i, j, u)
    """
    global d2
    d2 = Function(g)
    return d2


def lie(b, a, weight=0):
    """
    Computes the Lie derivative of field a wrt vector field b. Assumes the metric has been set. Optional weight for the
    field can be specified.

    b must be of type dendro.vec3
    a can be scalar, vec3 or sym_3x3

    Computes L_b(v)
    """
    global d

    e_i = [0, 1, 2]
    e_ij = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]

    if type(b) != tuple:
        raise ValueError('Dendro: The field wrt which the Lie derivative is calculated needs to be vec3.')

    return {
        Symbol: sum([b[i] * d(i, a) for i in e_i]) - weight*a*sum([d(i, a[i]) for i in e_i]),
        tuple: [sum([b[j] * d(j, a[i]) - a[j] * d(j, b[i]) + weight*a[i]*d(j, b[j]) for j in e_i]) for i in e_i],
        Matrix: [sum([b[k]*d(k, a[i, j]) + a[i, k]*d(j, b[k]) + a[k, j]*d(i, b[k]) - weight*a[i, j]*d(k, b[k])
                      for k in e_i]) for i, j in e_ij]
    }[type(a)]


def laplacian(a):
    """
    Computes the laplacian of the matrix. Assumes metric is set.
    """
    global d, d2, inv_metric, C2

    if inv_metric == undef:
        inv_metric = simplify(metric.inv())

    e_i = [0, 1, 2]
    e_ij = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]


    # check - loop over 6 or 9 ?
    return sum([(inv_metric[i, j] * d2(i, j, a) - sum([C2[l, i, j] * d(l, a) for l in e_i])) for i, j in e_ij])


def sqr(a):
    """
    Computes the square of the matrix. Assumes metric is set.
    """
    global inv_metric

    e_i = [0, 1, 2]
    e_ij = [(0, 0), (0, 1), (0, 2), (1, 1), (1, 2), (2, 2)]

    return [a[i, j]*sum([inv_metric[i, k] * inv_metric[j, l] * a[k, l] for k in e_i for l in e_i]) for i, j in e_ij]


def trace_free(x):
    """
    makes the operator trace-free
    """

##########################################################################
## metric related functions
##########################################################################


def set_metric(m):
    """
    sets the metric variable, so that dendro knows how to compute the derived variables. This should be done fairly
    early on. e.g.,

    gt = dendro.sym_3x3("gt")
    dendro.set_metric(gt)
    """
    global metric

    metric = gt


def get_inverse_metric():
    """
    Computes and returns the inverse metric. The variables need for be defined in advance. e.g.,

    gt = dendro.sym_3x3("gt")
    dendro.set_metric(gt)
    igt = dendro.get_inverse_metric()
    """
    global metric, inv_metric, undef

    if metric == undef:
        raise ValueError('Dendro: Metric not defined.')

    if inv_metric == undef:
        inv_metric = simplify(metric.inv())

    return inv_metric


def get_first_christoffel():
    """
    Computes and returns the first Christoffel Symbols. Assumes the metric has been set. e.g.,

    dendro.set_metric(gt);

    C1 = dendro.get_first_christoffel();
    """
    global metric, inv_metric, undef, C1, d

    e_i = [0, 1, 2]

    if inv_metric == undef:
        get_inverse_metric()

    if C1 == undef:
        C1 = MutableDenseNDimArray(range(27), (3, 3, 3))

        for k in e_i:
            for j in e_i:
                for i in e_i:
                    C1[k, i, j] = 1 / 2 * (d(j, metric[k, i]) + d(i, metric[k, j]) - d(k, metric[i, j]))

    return C1


def get_second_christoffel():
    """
    Computes and returns the second Christoffel Symbols. Assumes the metric has been set. Will compute the first
    Christoffel if not already computed. e.g.,

    dendro.set_metric(gt);

    C2 = dendro.get_second_christoffel();
    """
    global C2, C1, inv_metric

    if C2 == undef:
        if C1 == undef:
            get_first_christoffel()

        igt_t = Array(inv_metric, (3, 3))
        C2 = tensorcontraction(tensorproduct(igt_t, C1), (1, 2))

    return C2


def get_complete_christoffel(chi):
    """
    Computes and returns the second Christoffel Symbols. Assumes the metric has been set. Will compute the first/second
    Christoffel if not already computed. e.g.,

    dendro.set_metric(gt);

    C2_spatial = dendro.get_complete_christoffel();
    """
    global metric, inv_metric, undef, C1, C2, C3, d

    e_i = [0, 1, 2]

    if C3 == undef:
        C3 = MutableDenseNDimArray(range(27), (3, 3, 3))

        if C2 == undef:
            get_second_christoffel()

        for k in e_i:
            for j in e_i:
                for i in e_i:
                    C3[i, j, k] = C2[i, j, k] - 1/(2*chi)*(KroneckerDelta(i, j) * d(k, chi) +
                                                           KroneckerDelta(i, k) * d(j, chi) -
                                                           metric[j, k]*sum([inv_metric[i, m]*d(m, chi) for m in e_i])
                                                           )

    return C3


def compute_ricci(Gt, chi):
    """
    Computes the Ricci tensor. e.g.,

    dendro.set_metric(gt)

    R = dendro.compute_ricci(Gt, chi)

    or

    dendro.compute_ricci(Gt, chi)

    and use

    dendro.ricci

    The conformal connection coefficient and the conformal variable needs to be supplied.
    """
    global metric, inv_metric, C1, C2, d2

    e_i = [0, 1, 2]
    e_ij = [(0,0), (0,1), (0,2), (1,0), (1,1), (1,2), (2,0), (2,1), (2,2)]

    Rt = [-1/2*sum([inv_metric[l, m]*d2(l, m, metric[i, j]) for l, m in e_ij]) +
              1/2*sum([metric[k,i]*d(j, Gt[k]) + metric[k,j]*d(i, Gt[k]) for k in e_i]) +
              1/2*sum([Gt[k]*(C1[i,j,k] + C1[i,i,k]) for k in e_i]) +
              sum([inv_metric[l,m]*(C2[k,l,i]*C1[j,k,m] + C2[j,l,i]*C1[i,k,m] + C2[k,i,m]*C1[k,l,j])
                   for k in e_i for l,m in e_ij]) for i,j in e_ij]

    Rphi = [-2*cov_didj(chi) - sum([2*metric[i, j]*dtilde_up_i_down_i(k, chi) - 4*d(i, chi)*d(j, chi) -
                                    4*metric[i, j] * dtilde_up_i_dtilde_i(k, chi) for k in e_i]) for i, j in e_ij]

