import dendro

###################################################################
# initialize
###################################################################

# declare variables
a   = dendro.scalar("alpha")
chi = dendro.scalar("chi")
K   = dendro.scalar("K")

Gt  = dendro.vec3("Gt")
b   = dendro.vec3("beta")
B   = dendro.vec3("B")

gt  = dendro.sym_3x3("gt")
At  = dendro.sym_3x3("At")

# Lie derivative weight
weight = -2/3

# specify the functions for computing first and second derivatives
d = dendro.set_first_derivative('grad')    # first argument is direction
d2 = dendro.set_second_derivative('grad2')  # first 2 arguments are directions

# generate metric related quantities 
dendro.set_metric(gt)
igt = dendro.get_inverse_metric()

C1 = dendro.get_first_christoffel()
C2 = dendro.get_second_christoffel()
C2_spatial = dendro.get_complete_christoffel(chi)

###################################################################
# evolution equations
###################################################################

a_rhs = l1*dendro.lie(b, a) - 2*a*K

b_rhs = [ 3/4 * f(a) * B[i] +
         l2 * vec_j_del_j(b, b[i]) 
         for i in e_i ]

B_rhs = [Gt_rhs[i] - eta * B[i] + 
         l3 * vec_j_del_j(b, B[i]) - 
         l4 * vec_j_del_j(b, Gt[i]) 
         for i in e_i]

gt_rhs =  dendro.lie(b, gt, weight) - 2*a*At

chi_rhs = dendro.lie(b, chi, weight) + 2 / 3 * chi * ( a*K )

At_rhs = At_rhs = dendro.lie(b, At, weight) + chi * dendro.TF(-dendro.Laplacian(a) + a*dendro.Ricci) + a*(K*At -2*dendro.Sqr(At))

K_rhs = vec_k_del_k(K) - dtilde_up_i_down_i(a) + a*(1/3*K*K + a*dendro.Sqr(At))

Gt_rhs = dendro.Lie(b, Gt) + [sum([igt[j,k] * d2(j,k,b[i]) + 1/3*igt[i,j] * d2(j,k,b[k]) for j in e_i for k in e_i]) - sum([2*up_up(i,j,At)*d(j,a) for j in e_i]) for i in e_i] + 2*a*([C1[i,j,k]*up_up(j,k, At) - sum( [3/(2*chi)*up_up(i,j,At)*d(j,chi) + 2/3*igt[i,j]*d(j,K) for j in e_i])  for i in e_i])

###################################################################
# generate code
###################################################################

outs = [a_rhs, b_rhs, B_rhs, gt_rhs, chi_rhs, At_rhs, K_rhs, Gt_rhs]

dendro.generate(outs)

