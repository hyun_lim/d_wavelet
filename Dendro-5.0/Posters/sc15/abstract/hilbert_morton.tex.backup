\documentclass{article}
\usepackage[utf8]{inputenc}

\usepackage{graphicx} % Allows including images
\usepackage{booktabs} % Allows the use of \toprule, \midrule and \bottomrule in tables
%\usepackage{hyperref} % Allows to put hyperlinks.
\usepackage{color}
\usepackage{listings}
\usepackage[english]{babel}
\usepackage{subcaption}
\usepackage{float}
\usepackage{amsmath}
\usepackage{pgfplots}
\usepackage{authblk}
\usepackage[hidelinks]{hyperref}
\usepackage{multicol}



\begin{document}
 
  Space Filling Curves (SFC) are commonly used by the HPC community for
  partitioning data\cite{campbell2003,devine2005,sundar2007} and for resource
  allocations\cite{bender51,slurm}. Amongst the various SFCs, Hilbert curves
  have been found to be in general superior to the more ubiquitous Morton or
  Z-Curve \cite{campbell2003}. However, their adoption in large-scale HPC
  codes, especially for partitioning applications, has not been as common as
  Morton curves due to the computational complexity associated with the Hilbert
  ordering.
 

 
 \bibliographystyle{abbrv}
\bibliography{hilbert} 


 
 
\end{document}
