#ifndef HAVE_DERIVS_H
#define HAVE_DERIVS_H

void apply_KOdiss(coll_point_t *point, const int gen,
                  const int mask[n_variab + n_aux]);

void compute_fd_derivative(coll_point_t *point, const int gen, const int dir,
                           const int mask[n_variab + n_aux]);
void compute_KOdiss(coll_point_t *point, const int gen, const int dir,
                    const int mask[n_variab + n_aux]);
double deriv42_x(double *u, double dx, int indx, int n);
double deriv42adv_x(double *u, double dx, double betax, int indx, int n);
double deriv42_xx(double *u, double dx, int indx, int n);


#endif
