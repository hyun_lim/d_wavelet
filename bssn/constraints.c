#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"

/*---------------------------------------------------------------------------*
 *
 *
 *
 *---------------------------------------------------------------------------*/
void bssn_analysis(coll_point_t *point, const int gen)
{
// FIXME LATER
#if 0

  double rho_ADM = 0.0;
  double Jtd_ADM[3] = {0.0, 0.0, 0.0};
  double Tu[4][4] = { {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0}, 
                      {0.0, 0.0, 0.0, 0.0}, {0.0, 0.0, 0.0, 0.0} };

  double Alpha, chi, trK, inv_chi;
  double Betau[3];
  double gtd[3][3], gtu[3][3], Kd[3][3], Atd[3][3], Atu[3][3], Atud[3][3];
  double Bu[3];
  double Rtd[3][3], Rpd[3][3];
  double Ct[3][3][3], Ctd[3][3][3];
  double CalGamt[3], Gamt[3];

  double idetgtd, detgtd;
  double dd_chi[3][3];
  double dd_gtd[3][3][3][3];

  const double eight_pi_G  = 8.0*acos(-1.0);
  const double sixteen_pi_G  = 16.0*acos(-1.0);
  const double fourth = 0.25;
  const double third = 1.0/3.0;
  const double half = 0.5;
  const double twothirds = 2.0/3.0;
  const double threehalves = 1.5;
  const double two = 2.0;

  double MapleGenVar1, MapleGenVar2;

#if 0
  printf(" time_stamp=%d, gen=%d\n",point->time_stamp,gen);
  printf(" idx = (%d, %d, %d), (x,y,z)=( %g, %g, %g)\n",
           point->index.idx[0], point->index.idx[1], point->index.idx[2],
           point->coords.pos[0],point->coords.pos[1],point->coords.pos[2]);
#endif
 

  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = point->index.idx[i];
  }

  //int closest_level = point->closest_level;
  int closest_level = 0;
  double  dx = L_dim[0] / ns[0];
  for (int dir = x_dir; dir < n_dim; dir++) {
    int cl = get_closest_level(point, dir);
    assert( cl >= 1 );
    double tdx = L_dim[dir] / ns[dir] / (1 << cl);
//    printf("cl=%d, closest_level=%d, dx=%g, tdx=%g\n",cl,closest_level,dx,tdx);
    if (tdx < dx) {
      closest_level = cl;
      dx = tdx;
    }
  }
  assert( closest_level >= 1 );

  // get the grid spacing
  int h = 1 << (JJ - closest_level);

 /* 
  * Build a uniform stencil, which may include non-essential points.
  * This is the original choice
  */
  double *u[3][7], *du[3][7], *pos[3][7];
  coll_point_t *stcl[3][7];

  int nnull = 0;
  for (int dir = 0; dir < n_dim; dir++) {
    for (int qq = x_dir; qq < n_dim; qq++) {
      id.idx[qq] = point->index.idx[qq];
    }
    for (int ll = 0; ll < 7; ll++) {
      if ( ll == 3 ) {
        u[dir][ll] = point->u[gen];
        du[dir][ll] = point->du[gen];
        pos[dir][ll] = point->coords.pos;
        stcl[dir][ll] = point;
      } else {
        coll_point_t *cpm=NULL;
        id.idx[dir] = point->index.idx[dir] + (ll-3)*h;
        if (check_index(&id)) {
          cpm = get_coll_point(&id);
          //Stage 1 should have assured that all points exist, so...
          assert(cpm != NULL);
  
          u[dir][ll] = cpm->u[gen];
          du[dir][ll] = cpm->du[gen];
          stcl[dir][ll] = cpm;
          //TODO, this should actually be the closest position to the center;
          // this will be corrected below
          pos[dir][ll] = cpm->coords.pos;
        } else {
          u[dir][ll] = NULL;
          du[dir][ll] = NULL;
          pos[dir][ll] = NULL;
          stcl[dir][ll] = NULL;
          nnull++;
        }
      }
    }
  }

  int bi[3] = {0, 0, 0};
  int npts[3] = {7, 7, 7}; 
  int indx[3] = {3, 3, 3};

  if (nnull > 0) {
   /* Apply boundary conditions and return */
    for (int d = 0; d < 3; d++) {
     /* check to see if neighbors don't exist. If so, then we are on boundary */
      if (u[d][2] == NULL || u[d][4]== NULL) {
        sommerfeld_boundary(point, gen);
        return;
      }

      if (u[d][1] == NULL) {
        bi[d]   = 2;
        npts[d] = 5;
        indx[d] = 1;
      }
      else if (u[d][0] == NULL) {
        bi[d]   = 1;
        npts[d] = 6; 
        indx[d] = 2;
      }
      else if (u[d][5] == NULL) {
        bi[d]   = 0;
        npts[d] = 5;
        indx[d] = 3;
      }
      else if (u[d][6] == NULL) {
        bi[d]   = 0;
        npts[d] = 6;
        indx[d] = 3;
      }
    } 
}
if (nnull > 0){
    //* check to see if neighbors don't exist. If so, then we are on boundary *
     for(int dd = 0; dd < 3; dd++) {
      if (du[dd][2] == NULL || du[dd][4]== NULL) {
        sommerfeld_boundary(point, gen);
        return;
      }
/*
      if (du[dd][1] == NULL) {
        bi[dd]   = 2;
        npts[dd] = 5;
        indx[dd] = 1;
      }
      else if (du[dd][0] == NULL) {
        bi[dd]   = 1;
        npts[dd] = 6; 
        indx[dd] = 2;
      }
      else if (du[dd][5] == NULL) {
        bi[dd]   = 0;
        npts[dd] = 5;
        indx[dd] = 3;
      }
      else if (du[dd][6] == NULL) {
        bi[dd]   = 0;
        npts[dd] = 6;
        indx[dd] = 3;
      } */
    } 
  }

  double *upt = point->u[gen];
  double *dupt = point->du[gen];

  Alpha = upt[U_ALPHA];
  Betau[0] = upt[U_SHIFTX];
  Betau[1] = upt[U_SHIFTY];
  Betau[2] = upt[U_SHIFTZ];
  Bu[0] = upt[U_GBX];
  Bu[1] = upt[U_GBY];
  Bu[2] = upt[U_GBZ];

  chi = upt[U_CHI];
  trK = upt[U_TRK];
  gtd[0][0] = upt[U_GTXX];
  gtd[0][1] = upt[U_GTXY];
  gtd[0][2] = upt[U_GTXZ];
  gtd[1][0] = upt[U_GTXY];
  gtd[1][1] = upt[U_GTYY];
  gtd[1][2] = upt[U_GTYZ];
  gtd[2][0] = upt[U_GTXZ];
  gtd[2][1] = upt[U_GTYZ];
  gtd[2][2] = upt[U_GTZZ];

  Atd[0][0] = upt[U_ATXX];
  Atd[0][1] = upt[U_ATXY];
  Atd[0][2] = upt[U_ATXZ];
  Atd[1][0] = upt[U_ATXY];
  Atd[1][1] = upt[U_ATYY];
  Atd[1][2] = upt[U_ATYZ];
  Atd[2][0] = upt[U_ATXZ];
  Atd[2][1] = upt[U_ATYZ];
  Atd[2][2] = upt[U_ATZZ];

 /* define derivatives first */
 /* metric first derivatives */
  double d_chi[3];
  double d_gtd[3][3][3];
  double d_Atd[3][3][3];

  d_chi[0] = dupt[U_CHI*n_dim + 0];
  d_chi[1] = dupt[U_CHI*n_dim + 1];
  d_chi[2] = dupt[U_CHI*n_dim + 2];

  d_gtd[0][0][0] = dupt[U_GTXX*n_dim + 0];
  d_gtd[1][0][0] = dupt[U_GTXX*n_dim + 1];
  d_gtd[2][0][0] = dupt[U_GTXX*n_dim + 2];

  d_gtd[0][0][1] = dupt[U_GTXY*n_dim + 0];
  d_gtd[1][0][1] = dupt[U_GTXY*n_dim + 1];
  d_gtd[2][0][1] = dupt[U_GTXY*n_dim + 2];

  d_gtd[0][0][2] = dupt[U_GTXZ*n_dim + 0];
  d_gtd[1][0][2] = dupt[U_GTXZ*n_dim + 1];
  d_gtd[2][0][2] = dupt[U_GTXZ*n_dim + 2];

  d_gtd[0][1][0] = d_gtd[0][0][1];
  d_gtd[1][1][0] = d_gtd[1][0][1];
  d_gtd[2][1][0] = d_gtd[2][0][1];

  d_gtd[0][1][1] = dupt[U_GTYY*n_dim + 0];
  d_gtd[1][1][1] = dupt[U_GTYY*n_dim + 1];
  d_gtd[2][1][1] = dupt[U_GTYY*n_dim + 2];

  d_gtd[0][1][2] = dupt[U_GTYZ*n_dim + 0];
  d_gtd[1][1][2] = dupt[U_GTYZ*n_dim + 1];
  d_gtd[2][1][2] = dupt[U_GTYZ*n_dim + 2];

  d_gtd[0][2][0] = d_gtd[0][0][2];
  d_gtd[1][2][0] = d_gtd[1][0][2];
  d_gtd[2][2][0] = d_gtd[2][0][2];

  d_gtd[0][2][1] = d_gtd[0][1][2];
  d_gtd[1][2][1] = d_gtd[1][1][2];
  d_gtd[2][2][1] = d_gtd[2][1][2];

  d_gtd[0][2][2] = dupt[U_GTZZ*n_dim + 0];
  d_gtd[1][2][2] = dupt[U_GTZZ*n_dim + 1];
  d_gtd[2][2][2] = dupt[U_GTZZ*n_dim + 2];

 /* trK first derivatives */
  double d_trK[3];
  d_trK[0] = dupt[U_TRK*n_dim + 0];
  d_trK[1] = dupt[U_TRK*n_dim + 1];
  d_trK[2] = dupt[U_TRK*n_dim + 2];

 /* Gamt first derivatives */
  double d_Gamt[3][3];
  d_Gamt[0][0] = dupt[U_GAMTX*n_dim + 0];
  d_Gamt[1][0] = dupt[U_GAMTX*n_dim + 1];
  d_Gamt[2][0] = dupt[U_GAMTX*n_dim + 2];

  d_Gamt[0][1] = dupt[U_GAMTY*n_dim + 0];
  d_Gamt[1][1] = dupt[U_GAMTY*n_dim + 1];
  d_Gamt[2][1] = dupt[U_GAMTY*n_dim + 2];

  d_Gamt[0][2] = dupt[U_GAMTZ*n_dim + 0];
  d_Gamt[1][2] = dupt[U_GAMTZ*n_dim + 1];
  d_Gamt[2][2] = dupt[U_GAMTZ*n_dim + 2];


 /*
  *  2d arrays are 
          ( a00, a01, a02 )
          ( a10, a11, a12 )
          ( a20, a21, a22 )
     But for symmetric 2D tensors, we only store the unique components on the
     grid. The grid functions are arranged in a 1D array
          ( a00 , a01, a02, a11, a12, a22 )

     To map the 2D index notation to the sparse storage 1d storage, we use
     func_map. Given 2 indices, func_map returns the location of the component
     in the 1d array.

  *  Alternative:  2*i + j -i*j/4
  */

  double u1d[7];

  // second derivatives -- xx, yy, and zz second derivatives
  double xd_chi[3];

  for (int d = x_dir; d < 3; d++) {

    for (int i = 0; i < 3; i++) {  
      for (int j = i; j < 3; j++) {  
        for (int p = 0; p < npts[d]; p++) {
          u1d[p] = u[d][p+bi[d]][U_GTXX + 2*i + j - i*j/4];
        }
        dd_gtd[d][d][i][j] = deriv42_xx(u1d, dx, indx[d], npts[d]);
        dd_gtd[d][d][j][i] = dd_gtd[d][d][i][j];
      }
    }

    for (int p = 0; p < npts[d]; p++) {
      u1d[p] = u[d][p+bi[d]][U_CHI];
    }
    dd_chi[d][d] = deriv42_xx(u1d, dx, indx[d], npts[d]);
    xd_chi[d] = deriv42_x(u1d, dx, indx[d], npts[d]);

/*
    if ( point->coords.pos[0] > -7.21 &&  point->coords.pos[0] < -7.19 &&
       point->coords.pos[1] > -7.21 &&  point->coords.pos[1] < -7.19 &&
       point->coords.pos[2] > -7.21 &&  point->coords.pos[2] < -7.19) {

      printf("d = %d",d);
    }
*/
 
    double rdchi = xd_chi[d] - d_chi[d];
    if (fabs(rdchi) > 1.0e-5) {
      printf("  >>> DCHI d=%d:  rdchi=%g, indx=%d, index=%d, d_chi=%g, xd_chi=%g, \n",d,rdchi,indx[d], point->index.idx[d], d_chi[d], xd_chi[d]);
    }
  }

  // mixed second derivatives -- xy, xz, and yz second derivatives
  for (int m = 0; m < 2; m++) {
    for (int p = m+1; p < 3; p++) {
      int k;
      for (int i = 0; i < 3; i++) {
        for (int j = i; j < 3; j++) {
          k = (U_GTXX + 2*i + j - i*j/4)*n_dim + m;

          for (int q = 0; q < npts[p]; q++) {
            u1d[q] = du[p][q+bi[p]][k];
          }
          dd_gtd[p][m][i][j] = deriv42_x(u1d, dx, indx[p], npts[p]);
          dd_gtd[m][p][i][j] = dd_gtd[p][m][i][j];
          dd_gtd[m][p][j][i] = dd_gtd[m][p][i][j];
          dd_gtd[p][m][j][i] = dd_gtd[m][p][i][j];
        }
      }

      k = U_CHI * n_dim + m;
      for (int q = 0; q < npts[p]; q++) {
        u1d[q] = du[p][q+bi[p]][k];
      }
      dd_chi[p][m] = deriv42_x(u1d, dx, indx[p], npts[p]);
      dd_chi[m][p] = dd_chi[p][m];

    }
  }

#include "include/constraint_vars.h"
  double HamCon_geo1, HamCon_geo2, HamCon_geo3, HamCon_fld, HamCon;
  double Rscalar, trA;
  double MomCon_geo[3], MomCon_fld[3], MomCon[3];
  double cconfun[3], calcconfun[3];
  double detgtm1;
  double d_Kd[3][3][3], Del_Kd[3][3][3], Chr[3][3][3];
  double xpsi4R, xpsi4I, EWeyl[3][3], BWeyl[3][3], Tm_R[3], Tm_I[3];
  double rad, xpt[3], gu[3][3];
  double M_ADM_surf;
  double J_ADM_surf[3];

#include "include/constraints.h"

#endif
}
