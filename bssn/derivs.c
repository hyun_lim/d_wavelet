#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"


/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void compute_fd_derivative(coll_point_t *point, const int gen, const int dir,
                           const int mask[n_variab + n_aux])
{

  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = point->index.idx[i];
  }

  int closest_level = get_closest_level(point, dir);
  assert( closest_level >= 1 );
  double dx = L_dim[dir] / ns[dir] / (1 << closest_level);

/*
 //int closest_level = point->closest_level;
  int closest_level = 0;
  double  dx = L_dim[0] / ns[0];
  for (int dir = x_dir; dir < n_dim; dir++) {
    int cl = get_closest_level(point, dir);
    assert( cl >= 1 );
    double tdx = L_dim[dir] / ns[dir] / (1 << cl);
    printf("cl=%d, closest_level=%d, dx=%g, tdx=%g\n",cl,closest_level,dx,tdx);
    if (tdx < dx) {
      closest_level = cl;
      dx = tdx;
    }
  }

  assert( closest_level >= 1 );
*/

  // get the grid spacing
  int h = 1 << (JJ - closest_level);
  
  int bi, ci, npts;

  if (point->index.idx[dir] - 2*h >= 0 && point->index.idx[dir] + 2*h <= max_index[dir]) {
    bi = point->index.idx[dir] - 2*h;
    ci = 2;
    npts = 5;
  }
  else if (point->index.idx[dir] - h == 0) {
    bi = point->index.idx[dir] - h;
    ci = 1;
    npts = 3;
  }
  else if (point->index.idx[dir] == 0) {
    bi = point->index.idx[dir];
    ci = 0;
    npts = 3;
  }
  else if (point->index.idx[dir] + h == max_index[dir]) {
    bi = point->index.idx[dir] - h;
    ci = 3;
    npts = 3;
  }
  else if (point->index.idx[dir] == max_index[dir]) {
    bi = point->index.idx[dir] - 2*h;
    ci = 4;
    npts = 3;
  }
  else {
    printf("compute_fd_derivative: Stencil problem. dir=%d, idx=%d, max=%d, h=%d\n",
            dir,point->index.idx[dir],max_index[dir],h);
    exit(2);
  }

  double *u[5], *du;
  du = point->du[gen];
  for (int ll = 0; ll < npts; ll++) {
    if (ll == ci) {
      u[ll]  = point->u[gen];
    }
    else {
      coll_point_t *cpm=NULL;
      id.idx[dir] = bi + ll*h;
      cpm = get_coll_point(&id);
      //check_deriv_stencil should have assured that all points exist, so...
      if (cpm == NULL) {
        printf("CFD: point is (%d, %d, %d)\n",point->index.idx[0], point->index.idx[1],point->index.idx[2]);
        printf("CFD: id.idx   (%d, %d, %d)\n",id.idx[0], id.idx[1],id.idx[2]);
      }
      assert(cpm != NULL);
  
      u[ll] = cpm->u[gen];
    }
  }

  const double idx = 1.0/dx;
  const double idx_by_2 = idx / 2.0;
  const double idx_by_12 = idx / 12.0;

  if (ci == 2) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = ( u[0][m] - 8.0 * u[1][m] 
                                    + 8.0 * u[3][m] - u[4][m] ) * idx_by_12;
    }
  }
  else if (ci == 0) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = ( -3.0*u[0][m] + 4.0*u[1][m] - u[2][m] ) * idx_by_2;
    }
  }
  else if (ci == 1) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = ( -u[0][m] + u[2][m] ) * idx_by_2; 
    }
  }
  else if (ci == 3) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = ( - u[0][m] + u[2][m] ) * idx_by_2; 
    }
  }
  else if (ci == 4) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = ( u[0][m] - 4.0 * u[1][m] 
                                         + 3.0 * u[2][m]) * idx_by_2; 
    }
  }
  else {
    printf("compute_fd_derivative: Unexpected value of ci=%d.\n",ci);
    exit(2);
  }

}

/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void compute_KOdiss(coll_point_t *point, const int gen, const int dir,
                    const int mask[n_variab + n_aux])
{

  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = point->index.idx[i];
  }

  int closest_level = get_closest_level(point, dir);
  assert( closest_level >= 1 );
  double dx = L_dim[dir] / ns[dir] / (1 << closest_level);

  // get the grid spacing
  int h = 1 << (JJ - closest_level);
  
  int bi, ci, npts;

  if (point->index.idx[dir] - 3*h >= 0 && point->index.idx[dir] + 3*h <= max_index[dir]) {
    bi = point->index.idx[dir] - 3*h;
    ci = 3;
    npts = 7;
  }
  else if (point->index.idx[dir] - 2*h == 0) {
    bi = point->index.idx[dir] - 2*h;
    ci = 2;
    npts = 6;
  }
  else if (point->index.idx[dir] - h == 0) {
    bi = point->index.idx[dir] - h;
    ci = 1;
    npts = 5;
  }
  else if (point->index.idx[dir] == 0) {
    bi = point->index.idx[dir];
    ci = 0;
    npts = 4;
  }
  else if (point->index.idx[dir] + 2*h == max_index[dir]) {
    bi = point->index.idx[dir] - 3*h;
    ci = 4;
    npts = 6;
  }
  else if (point->index.idx[dir] + h == max_index[dir]) {
    bi = point->index.idx[dir] - 3*h;
    ci = 5;
    npts = 5;
  }
  else if (point->index.idx[dir] == max_index[dir]) {
    bi = point->index.idx[dir] - 3*h;
    ci = 6;
    npts = 4;
  }
  else {
    printf("compute_KOdiss: Stencil problem.\n");
    exit(2);
  }

  double *u[7], *du;
  du = point->du[gen];
  for (int ll = 0; ll < npts; ll++) {
    if (ll == ci) {
      u[ll]  = point->u[gen];
    }
    else {
      coll_point_t *cpm=NULL;
      id.idx[dir] = bi + ll*h;
      cpm = get_coll_point(&id);
      //Point should exist, so...
      assert(cpm != NULL);
  
      u[ll] = cpm->u[gen];
    }
  }

  const double s0   = -1.0 / (64.0 * dx);
  const double smr3 =  1.0 / (59.0 / 48.0 * 64.0 * dx);
  const double smr2 =  1.0 / (43.0 / 48.0 * 64.0 * dx);
  const double smr1 =  1.0 / (49.0 / 48.0 * 64.0 * dx);
  const double spr3 =  smr3;
  const double spr2 =  smr2;
  const double spr1 =  smr1;

  if (ci == 0) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = (u[3][m] - 3.0*u[2][m] + 3.0*u[1][m] - u[0][m])*smr3;
    }
  }
  else if (ci == 1) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] =  (u[4][m] - 6.0*u[3][m] + 12.0*u[2][m] 
                                    - 10.0*u[1][m] + 3.0*u[0][m]) * smr2;
    }
  }
  else if (ci == 2) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = (u[5][m] - 6.0*u[4][m]  + 15.0*u[3][m] - 19.0*u[2][m] 
                                   + 12.0*u[1][m] - 3.0*u[0][m]) * smr1;
    }
  }
  else if (ci == 3) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = s0  * ( - u[0][m] +  6.0*u[1][m]
                             - 15.0*u[2][m] + 20.0*u[3][m] - 15.0*u[4][m] 
                             +  6.0*u[5][m] -      u[6][m]   );
    }
  }
  else if (ci == 4) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = (u[0][m] - 6.0*u[1][m] + 15.0*u[2][m] - 19.0*u[3][m] 
                                   + 12.0*u[4][m] - 3.0*u[5][m]) * spr1;

    }
  }
  else if (ci == 5) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = (u[0][m] - 6.0*u[1][m] + 12.0*u[2][m] 
                                   - 10.0*u[3][m] + 3.0*u[4][m]) * spr2;
    }
  }
  else if (ci == 6) {
    for (int m = 0; m < n_variab; m++) {
      du[n_dim*m + dir] = (u[0][m] - 3.0*u[1][m] + 3.0*u[2][m] - u[3][m])*spr3;
    }
  }
  else {
    printf("compute_KOdiss: Unexpected value of ci=%d.\n",ci);
    exit(2);
  }

}

/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
void apply_KOdiss(coll_point_t *point, const int gen, 
                  const int mask[n_variab + n_aux])
{
  double * const du = point->du[gen];
  double * const rhs = point->rhs[gen];

  const double sigma_diss = pars.sigma_diss;

  for (int m = 0; m < n_variab; m++) {
    rhs[m] += sigma_diss * (du[m*n_dim] + du[m*n_dim + 1] + du[m*n_dim + 2]);
  }

}


/*---------------------------------------------------------------------
 *     
 *        deriv42_x  calculates the first derivative with respect to 
 *                   x using a centered fourth order finite difference 
 *                   operator at all points except the boundaries (i=1
 *                   and i=nx) and the penultimate boundary points 
 *                   (i=2 and i=nx-1).  At the penultimate boundary 
 *                   points, we use a centered second order operator 
 *                   rather than an up- or downwind fourth order  
 *                   operator.  At the boundary points (i=1,nx) we 
 *                   spoil the spirit of this effort of using centered
 *                   difference operators and use shifted operators as
 *                   appropriate. 
 *     
 *---------------------------------------------------------------------*/
double deriv42_x(double *u, double dx, int indx, int n)
{

    double idx = 1.0/dx;
    double idx_by_2 = 0.5 * idx; 
    double idx_by_12 = idx / 12.0;
    
    double dxu;
    
    if (indx == 0) {
      dxu = ( -3.0 * u[0] + 4.0 * u[1] - u[2] ) * idx_by_2;
    }
    else if (indx == 1) {      
      dxu = ( -u[0] + u[2] ) * idx_by_2; 
    }
    else if (indx > 1 && indx < n-2) {
      dxu = ( u[indx-2] - 8.0 * u[indx-1] + 8.0 * u[indx+1] 
                      - u[indx+2] ) * idx_by_12;  
    }
    else if ( indx == n-2 ) {
      dxu = ( - u[indx-1] + u[indx+1] ) * idx_by_2; 
    }
    else if ( indx == n-1 ) {
      dxu = ( u[indx-2] - 4.0 * u[indx-1] + 3.0 * u[indx]) * idx_by_2; 
    }    
    else {
      dxu = -1.0;
      printf("Index error in deriv42_x. indx=%d, n=%d",indx,n);
    }

    return dxu;
}

/*---------------------------------------------------------------------
 *     
 *        deriv44_x  calculates the first derivative with respect to 
 *                   x using a centered fourth order finite difference 
 *                   operator at all points except the boundaries (i=1
 *                   and i=nx) and the penultimate boundary points 
 *                   (i=2 and i=nx-1).  At these latter points, we use
 *                   the appropriate up- or downwind fourth order 
 *                   operators.  Note that each of these shifted 
 *                   operators use five point stencils.  
 *     
 *--------------------------------------------------------------------- */   
double deriv44_x(double *u, double dx, int indx, int n)
{

    double idx = 1.0/dx;
    double idx_by_12 = idx / 12.0;
    
    double dxu;

  if (indx == 0) {
    dxu = ( - 25.0 * u[0] + 48.0 * u[1] - 36.0 * u[2] + 16.0 * u[3]
            -  3.0 * u[4] ) * idx_by_12;
  }
  else if (indx == 1) {
    dxu = (  -  3.0 * u[0] - 10.0 * u[1] + 18.0 * u[2] -  6.0 * u[3] 
             + u[4] ) * idx_by_12;
  }
  else if (indx == 2) {
    dxu = ( u[0] - 8.0 * u[1] + 8.0 * u[3] - u[4] ) * idx_by_12;
  }
  else if (indx == 3) {
    dxu = (  - u[0] +  6.0 * u[1] - 18.0 * u[2] + 10.0 * u[3] 
             +  3.0 * u[4] ) * idx_by_12;
  }
  else if (indx == 4) {
    dxu = (     3.0 * u[0] - 16.0 * u[1] + 36.0 * u[2] - 48.0 * u[3] 
             + 25.0 * u[4] ) * idx_by_12;
  }
  else {
    dxu = -1.0;
    printf("Index error in deriv44_x. indx=%d, n=%d",indx,n);
  }

  return dxu;
}


 
/*----------------------------------------------------------------
  *    
  *      deriv42adv_x  calculates the first derivative with respect to 
  *                    x using an "advective" fourth order finite 
  *                    difference operator at all points on the 
  *                    interval [4,nx-3].  This derivative, if used, 
  *                    will only be applied on "Lie" terms in the equations 
  *                    such as beta^x \partial_x in the equations.  
  *                    Further, it will depend on the sign of beta^x.  
  *                    Namely, if beta^x is positive, we will use a 
  *                    fourth order upwind operator while if beta^x 
  *                    is negative, we use a fourth order downwind 
  *                    operator.  At other points, things>= more 
  *                    complicated.   
  *                     
  *                    At i=3, if beta^x is positive, we continue to  
  *                    use the fourth order upwind operator but if  
  *                    beta^x is negative, we use a second order downwind  
  *                    operator.   
  *                      
  *                    We reverse this at i=nx-2 such that if beta^x is  
  *                    positive, we use a second order upwind operator 
  *                    while if beta^x is negative we continue to use  
  *                    the fourth order downwind operator.   
  *                     
  *                    At i=2, if beta^x is positive, we use the   
  *                    second order upwind operator and if beta^x is  
  *                    negative, we use a second order centered operator. 
  *                     
  *                    At i=nx-1, if beta^x is positive, we use the   
  *                    second order centered operator and if beta^x is  
  *                    negative, we use a second order downwind operator. 
  *                     
  *                    At i=1, we use the second order upwind operator 
  *                    for both positive and negative values of beta^x.
  *                    
  *                    At i=nx, we use the second order downwind operator 
  *                    for both positive and negative values of beta^x.
  *                     
  *--------------------------------------------------------------------*/

double deriv42adv_x(double *u, double dx, double betax, int indx, int n)
{

  double idx = 1.0/dx;
  double idx_by_2 = 0.5 * idx;
  double idx_by_12 = idx / 12.0;

  double Dxu;

  if (indx == 0) {
    Dxu = ( -3.0 * u[0] + 4.0 * u[1] - u[2]) * idx_by_2;
  }
  else if (indx == 1) {
    if (betax > 0.0 ) {
      Dxu = ( -  3.0 * u[1] + 4.0 * u[2] - u[3]) * idx_by_2;
    }
    else {
      Dxu = ( -u[0] + u[2]) * idx_by_2;
    }
  }
  else if ( indx == 2 ) {
    if ( betax >= 0.0 ) {
      Dxu = ( -  3.0 * u[1] - 10.0 * u[2] + 18.0 * u[3] 
              -  6.0 * u[4] + u[5]) * idx_by_12;
    }
    else {
      Dxu = (u[0] - 4.0 * u[1] +  3.0 * u[2]) * idx_by_2;
    }
  }
  else if (indx > 2 && indx < n-3 ) {
    if ( betax >= 0.0 ) {
      Dxu = ( -  3.0 * u[indx-1] - 10.0 * u[indx] + 18.0 * u[indx+1]
              -  6.0 * u[indx+2] + u[indx+3] ) * idx_by_12;
    }
    else {
      Dxu = ( - u[indx-3] + 6.0 * u[indx-2] - 18.0 * u[indx-1]
                     + 10.0 * u[indx] + 3.0 * u[indx+1]) * idx_by_12;
    }
  }
  else if (indx == n-3) { 
    if ( betax >= 0.0 ) {
      Dxu = (  - 3.0 * u[indx] + 4.0 * u[indx+1] - u[indx+2] ) * idx_by_2;
    }
    else {
      Dxu = ( - u[indx-3] +  6.0 * u[indx-2] - 18.0 * u[indx-1] 
              + 10.0 * u[indx] +  3.0 * u[indx+1]) * idx_by_12;
    }
  }
  else if (indx == n-2) { 
    if ( betax >= 0.0 ) {
      Dxu = ( - u[indx-1] + u[indx+1]) * idx_by_2;
    }
    else {
      Dxu = (u[indx-2] - 4.0 * u[indx-1] + 3.0 * u[indx]) * idx_by_2; 
    }
  }
  else if (indx == n-1) { 
     Dxu = (u[indx-2] - 4.0 * u[indx-1] + 3.0 * u[indx]) * idx_by_2; 
  }     
  else {
    Dxu = -1.0;
    printf("Index error in deriv42adv_x. indx=%d, n=%d",indx,n);
  }

  return Dxu;
}



/*---------------------------------------------------------------------
 *     
 *       deriv42_xx  calculates the second derivative with respect to  
 *                   x using a centered fourth order finite difference 
 *                   operator at all points except the boundaries (i=1
 *                   and i=nx) and the penultimate boundary points 
 *                   (i=2 and i=nx-1).  At the penultimate boundary 
 *                   points, we use a centered second order operator 
 *                   rather than an up- or downwind fourth order  
 *                   operator.  
 *      
 *                   At the boundary points (i=1 and i=nx) we can not,  
 *                   of course, define centered difference operators.  
 *                   As a result, we have to spoil our effort in this  
 *                   routine to use only centered difference operators.  
 *                   Instead, at the boundary points, we choose to use 
 *                   appropriately shifted second order operators.  In
 *                   particular, at i=1, we use a forward (upwind)  
 *                   operator with a four point stencil.  At i=nx, we  
 *                   use a backward (downwind) operator, again with 
 *                   a four point stencil. 
 *
 *---------------------------------------------------------------------*/
double deriv42_xx(double *u, double dx, int indx, int n) {

    double idx_sqrd = 1.0/(dx*dx);
    double idx_sqrd_by_12 = idx_sqrd / 12.0;
    
    double DxDxu;

    if ( indx == 0 ) {
      DxDxu = ( 2.0 * u[0] - 5.0 * u[1] + 4.0 * u[2]- u[3] ) * idx_sqrd;
    }
    else if ( indx == 1 ) {
      DxDxu = ( u[0] - 2.0 * u[1] + u[2] ) * idx_sqrd;
    }
    else if (indx > 1 && indx < n-2) {
      DxDxu = ( - u[indx-2] + 16.0 * u[indx-1] - 30.0 * u[indx]
                + 16.0 * u[indx+1] - u[indx+2] ) * idx_sqrd_by_12;  
    }      
    else if ( indx == n-2 ) { 
      DxDxu = ( u[indx-1] - 2.0 * u[indx] + u[indx+1] ) * idx_sqrd; 
    }
    else if ( indx == n-1) {
      DxDxu = ( - u[indx-3] + 4.0 * u[indx-2] 
                - 5.0 * u[indx-1] + 2.0 * u[indx] ) * idx_sqrd;
    }
    else {
      DxDxu = -1.0;
      printf("Index error in deriv42adv_xx. indx=%d, n=%d",indx,n);
    }

    return DxDxu;
}
