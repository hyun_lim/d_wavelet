      t1 = Alpha*Alpha;
      rho_ADM = t1*Tu[0][0];
      t2 = Tu[0][1]+Betau[0]*Tu[0][0];
      t5 = Tu[0][2]+Betau[1]*Tu[0][0];
      t8 = Tu[0][3]+Betau[2]*Tu[0][0];
      Jtd_ADM[0] = Alpha*(t2*gtd[0][0]+t5*gtd[0][1]+t8*gtd[0][2]);
      Jtd_ADM[1] = Alpha*(t2*gtd[0][1]+t5*gtd[1][1]+t8*gtd[1][2]);
      Jtd_ADM[2] = Alpha*(t2*gtd[0][2]+t5*gtd[1][2]+t8*gtd[2][2]);
      t4 = Betau[0]*gtd[0][0]+Betau[1]*gtd[0][1]+Betau[2]*gtd[0][2];
      t5 = t4*t4;
      t11 = gtd[0][0]*Tu[0][1]+gtd[0][1]*Tu[0][2]+gtd[0][2]*Tu[0][3];
      t13 = gtd[0][0]*gtd[0][0];
      t15 = gtd[0][1]*gtd[0][1];
      t17 = gtd[0][2]*gtd[0][2];
      t19 = gtd[0][0]*gtd[0][1];
      t21 = gtd[0][0]*gtd[0][2];
      t23 = gtd[0][1]*gtd[0][2];
      t31 = Betau[0]*gtd[0][1]+Betau[1]*gtd[1][1]+Betau[2]*gtd[1][2];
      t37 = gtd[0][1]*Tu[0][1]+gtd[1][1]*Tu[0][2]+gtd[1][2]*Tu[0][3];
      t43 = gtd[0][0]*gtd[1][2];
      t46 = gtd[0][1]*gtd[1][1];
      t48 = gtd[0][1]*gtd[1][2];
      t51 = gtd[0][2]*gtd[1][1];
      t53 = gtd[0][2]*gtd[1][2];
      t55 = t4*t31*Tu[0][0]+t4*t37+t31*t11+t19*Tu[1][1]+gtd[0][0]*gtd[1][1]*Tu
[1][2]+t43*Tu[1][3]+t15*Tu[2][1]+t46*Tu[2][2]+t48*Tu[2][3]+t23*Tu[3][1]+t51*Tu
[3][2]+t53*Tu[3][3];
      t59 = Betau[0]*gtd[0][2]+Betau[1]*gtd[1][2]+Betau[2]*gtd[2][2];
      t65 = gtd[0][2]*Tu[0][1]+gtd[1][2]*Tu[0][2]+gtd[2][2]*Tu[0][3];
      t74 = gtd[0][1]*gtd[2][2];
      t78 = gtd[0][2]*gtd[2][2];
      t80 = t4*t59*Tu[0][0]+t4*t65+t59*t11+t21*Tu[1][1]+t43*Tu[1][2]+gtd[0][0]*
gtd[2][2]*Tu[1][3]+t23*Tu[2][1]+t48*Tu[2][2]+t74*Tu[2][3]+t17*Tu[3][1]+t53*Tu
[3][2]+t78*Tu[3][3];
      t81 = t31*t31;
      t86 = gtd[1][1]*gtd[1][1];
      t88 = gtd[1][2]*gtd[1][2];
      t92 = gtd[1][1]*gtd[1][2];
      t110 = gtd[1][2]*gtd[2][2];
      t112 = t31*t59*Tu[0][0]+t31*t65+t59*t37+t23*Tu[1][1]+t48*Tu[1][2]+t74*Tu
[1][3]+t51*Tu[2][1]+t92*Tu[2][2]+gtd[1][1]*gtd[2][2]*Tu[2][3]+t53*Tu[3][1]+t88*
Tu[3][2]+t110*Tu[3][3];
      t113 = t59*t59;
      t119 = gtd[2][2]*gtd[2][2];
      pTtd_ADM[0][0] = t5*Tu[0][0]+two*t4*t11+t13*Tu[1][1]+t15*Tu[2][2]+t17*Tu
[3][3]+two*(t19*Tu[1][2]+t21*Tu[1][3]+t23*Tu[2][3]);
      pTtd_ADM[0][1] = t55;
      pTtd_ADM[0][2] = t80;
      pTtd_ADM[1][0] = t55;
      pTtd_ADM[1][1] = t81*Tu[0][0]+two*t31*t37+t15*Tu[1][1]+t86*Tu[2][2]+t88*
Tu[3][3]+two*(t46*Tu[1][2]+t48*Tu[1][3]+t92*Tu[2][3]);
      pTtd_ADM[1][2] = t112;
      pTtd_ADM[2][0] = t80;
      pTtd_ADM[2][1] = t112;
      pTtd_ADM[2][2] = t113*Tu[0][0]+two*t59*t65+t17*Tu[1][1]+t88*Tu[2][2]+t119
*Tu[3][3]+two*(t53*Tu[1][2]+t78*Tu[1][3]+t110*Tu[2][3]);
      t1 = fabs(chi);
      inv_chi = 0.1E1/t1;
      t1 = gtd[0][0];
      t2 = gtd[1][1];
      t4 = gtd[2][2];
      t6 = gtd[1][2];
      t7 = t6*t6;
      t9 = gtd[0][1];
      t10 = t9*t9;
      t12 = gtd[0][2];
      t16 = t12*t12;
      detgtd = t1*t2*t4-t1*t7-t10*t4+2.0*t9*t12*t6-t16*t2;
      idetgtd = 0.1E1/detgtd;
      t2 = gtd[1][2]*gtd[1][2];
      t8 = idetgtd*(-gtd[0][1]*gtd[2][2]+gtd[0][2]*gtd[1][2]);
      t12 = idetgtd*(gtd[0][1]*gtd[1][2]-gtd[0][2]*gtd[1][1]);
      t14 = gtd[0][2]*gtd[0][2];
      t20 = idetgtd*(-gtd[0][0]*gtd[1][2]+gtd[0][1]*gtd[0][2]);
      t22 = gtd[0][1]*gtd[0][1];
      gtu[0][0] = idetgtd*(gtd[1][1]*gtd[2][2]-t2);
      gtu[0][1] = t8;
      gtu[0][2] = t12;
      gtu[1][0] = t8;
      gtu[1][1] = idetgtd*(gtd[0][0]*gtd[2][2]-t14);
      gtu[1][2] = t20;
      gtu[2][0] = t12;
      gtu[2][1] = t20;
      gtu[2][2] = idetgtd*(gtd[0][0]*gtd[1][1]-t22);
      t2 = gtu[0][1]*Atd[0][1];
      t3 = gtu[0][2]*Atd[0][2];
      t18 = gtu[1][2]*Atd[1][2];
      Atud[0][0] = gtu[0][0]*Atd[0][0]+t2+t3;
      Atud[0][1] = gtu[0][0]*Atd[0][1]+gtu[0][1]*Atd[1][1]+gtu[0][2]*Atd[1][2];
      Atud[0][2] = gtu[0][0]*Atd[0][2]+gtu[0][1]*Atd[1][2]+gtu[0][2]*Atd[2][2];
      Atud[1][0] = gtu[0][1]*Atd[0][0]+gtu[1][1]*Atd[0][1]+gtu[1][2]*Atd[0][2];
      Atud[1][1] = t2+gtu[1][1]*Atd[1][1]+t18;
      Atud[1][2] = gtu[0][1]*Atd[0][2]+gtu[1][1]*Atd[1][2]+gtu[1][2]*Atd[2][2];
      Atud[2][0] = gtu[0][2]*Atd[0][0]+gtu[1][2]*Atd[0][1]+gtu[2][2]*Atd[0][2];
      Atud[2][1] = gtu[0][2]*Atd[0][1]+gtu[1][2]*Atd[1][1]+gtu[2][2]*Atd[1][2];
      Atud[2][2] = t3+t18+gtu[2][2]*Atd[2][2];
      t8 = Atud[0][0]*gtu[0][1]+Atud[0][1]*gtu[1][1]+Atud[0][2]*gtu[1][2];
      t12 = Atud[0][0]*gtu[0][2]+Atud[0][1]*gtu[1][2]+Atud[0][2]*gtu[2][2];
      t20 = Atud[1][0]*gtu[0][2]+Atud[1][1]*gtu[1][2]+Atud[1][2]*gtu[2][2];
      Atu[0][0] = Atud[0][0]*gtu[0][0]+Atud[0][1]*gtu[0][1]+Atud[0][2]*gtu[0]
[2];
      Atu[0][1] = t8;
      Atu[0][2] = t12;
      Atu[1][0] = t8;
      Atu[1][1] = Atud[1][0]*gtu[0][1]+Atud[1][1]*gtu[1][1]+Atud[1][2]*gtu[1]
[2];
      Atu[1][2] = t20;
      Atu[2][0] = t12;
      Atu[2][1] = t20;
      Atu[2][2] = Atud[2][0]*gtu[0][2]+Atud[2][1]*gtu[1][2]+Atud[2][2]*gtu[2]
[2];
      t3 = d_gtd[1][0][2]+d_gtd[2][0][1]-d_gtd[0][1][2];
      t8 = d_gtd[0][1][2]+d_gtd[2][0][1]-d_gtd[1][0][2];
      t13 = d_gtd[0][1][2]+d_gtd[1][0][2]-d_gtd[2][0][1];
      Ctd[0][0][0] = d_gtd[0][0][0];
      Ctd[0][0][1] = d_gtd[1][0][0];
      Ctd[0][0][2] = d_gtd[2][0][0];
      Ctd[0][1][0] = d_gtd[1][0][0];
      Ctd[0][1][1] = 2.0*d_gtd[1][0][1]-d_gtd[0][1][1];
      Ctd[0][1][2] = t3;
      Ctd[0][2][0] = d_gtd[2][0][0];
      Ctd[0][2][1] = t3;
      Ctd[0][2][2] = 2.0*d_gtd[2][0][2]-d_gtd[0][2][2];
      Ctd[1][0][0] = 2.0*d_gtd[0][0][1]-d_gtd[1][0][0];
      Ctd[1][0][1] = d_gtd[0][1][1];
      Ctd[1][0][2] = t8;
      Ctd[1][1][0] = d_gtd[0][1][1];
      Ctd[1][1][1] = d_gtd[1][1][1];
      Ctd[1][1][2] = d_gtd[2][1][1];
      Ctd[1][2][0] = t8;
      Ctd[1][2][1] = d_gtd[2][1][1];
      Ctd[1][2][2] = 2.0*d_gtd[2][1][2]-d_gtd[1][2][2];
      Ctd[2][0][0] = 2.0*d_gtd[0][0][2]-d_gtd[2][0][0];
      Ctd[2][0][1] = t13;
      Ctd[2][0][2] = d_gtd[0][2][2];
      Ctd[2][1][0] = t13;
      Ctd[2][1][1] = 2.0*d_gtd[1][1][2]-d_gtd[2][1][1];
      Ctd[2][1][2] = d_gtd[1][2][2];
      Ctd[2][2][0] = d_gtd[0][2][2];
      Ctd[2][2][1] = d_gtd[1][2][2];
      Ctd[2][2][2] = d_gtd[2][2][2];
      t8 = gtu[0][0]*Ctd[0][0][1]+gtu[0][1]*Ctd[1][0][1]+gtu[0][2]*Ctd[2][0][1]
;
      t12 = gtu[0][0]*Ctd[0][0][2]+gtu[0][1]*Ctd[1][0][2]+gtu[0][2]*Ctd[2][0]
[2];
      t20 = gtu[0][0]*Ctd[0][1][2]+gtu[0][1]*Ctd[1][1][2]+gtu[0][2]*Ctd[2][1]
[2];
      t32 = gtu[0][1]*Ctd[0][0][1]+gtu[1][1]*Ctd[1][0][1]+gtu[1][2]*Ctd[2][0]
[1];
      t36 = gtu[0][1]*Ctd[0][0][2]+gtu[1][1]*Ctd[1][0][2]+gtu[1][2]*Ctd[2][0]
[2];
      t44 = gtu[0][1]*Ctd[0][1][2]+gtu[1][1]*Ctd[1][1][2]+gtu[1][2]*Ctd[2][1]
[2];
      t56 = gtu[0][2]*Ctd[0][0][1]+gtu[1][2]*Ctd[1][0][1]+gtu[2][2]*Ctd[2][0]
[1];
      t60 = gtu[0][2]*Ctd[0][0][2]+gtu[1][2]*Ctd[1][0][2]+gtu[2][2]*Ctd[2][0]
[2];
      t68 = gtu[0][2]*Ctd[0][1][2]+gtu[1][2]*Ctd[1][1][2]+gtu[2][2]*Ctd[2][1]
[2];
      Ct[0][0][0] = gtu[0][0]*Ctd[0][0][0]+gtu[0][1]*Ctd[1][0][0]+gtu[0][2]*Ctd
[2][0][0];
      Ct[0][0][1] = t8;
      Ct[0][0][2] = t12;
      Ct[0][1][0] = t8;
      Ct[0][1][1] = gtu[0][0]*Ctd[0][1][1]+gtu[0][1]*Ctd[1][1][1]+gtu[0][2]*Ctd
[2][1][1];
      Ct[0][1][2] = t20;
      Ct[0][2][0] = t12;
      Ct[0][2][1] = t20;
      Ct[0][2][2] = gtu[0][0]*Ctd[0][2][2]+gtu[0][1]*Ctd[1][2][2]+gtu[0][2]*Ctd
[2][2][2];
      Ct[1][0][0] = gtu[0][1]*Ctd[0][0][0]+gtu[1][1]*Ctd[1][0][0]+gtu[1][2]*Ctd
[2][0][0];
      Ct[1][0][1] = t32;
      Ct[1][0][2] = t36;
      Ct[1][1][0] = t32;
      Ct[1][1][1] = gtu[0][1]*Ctd[0][1][1]+gtu[1][1]*Ctd[1][1][1]+gtu[1][2]*Ctd
[2][1][1];
      Ct[1][1][2] = t44;
      Ct[1][2][0] = t36;
      Ct[1][2][1] = t44;
      Ct[1][2][2] = gtu[0][1]*Ctd[0][2][2]+gtu[1][1]*Ctd[1][2][2]+gtu[1][2]*Ctd
[2][2][2];
      Ct[2][0][0] = gtu[0][2]*Ctd[0][0][0]+gtu[1][2]*Ctd[1][0][0]+gtu[2][2]*Ctd
[2][0][0];
      Ct[2][0][1] = t56;
      Ct[2][0][2] = t60;
      Ct[2][1][0] = t56;
      Ct[2][1][1] = gtu[0][2]*Ctd[0][1][1]+gtu[1][2]*Ctd[1][1][1]+gtu[2][2]*Ctd
[2][1][1];
      Ct[2][1][2] = t68;
      Ct[2][2][0] = t60;
      Ct[2][2][1] = t68;
      Ct[2][2][2] = gtu[0][2]*Ctd[0][2][2]+gtu[1][2]*Ctd[1][2][2]+gtu[2][2]*Ctd
[2][2][2];
      div_Beta = d_Betau[0][0]+d_Betau[1][1]+d_Betau[2][2];
      d_div_Beta[0] = dd_Betau[0][0][0]+dd_Betau[0][1][1]+dd_Betau[0][2][2];
      d_div_Beta[1] = dd_Betau[0][1][0]+dd_Betau[1][1][1]+dd_Betau[1][2][2];
      d_div_Beta[2] = dd_Betau[0][2][0]+dd_Betau[1][2][1]+dd_Betau[2][2][2];
      CalGamt[0] = half*(gtu[0][0]*Ct[0][0][0]+gtu[1][1]*Ct[0][1][1]+gtu[2][2]*
Ct[0][2][2])+gtu[0][1]*Ct[0][0][1]+gtu[0][2]*Ct[0][0][2]+gtu[1][2]*Ct[0][1][2];
      CalGamt[1] = half*(gtu[0][0]*Ct[1][0][0]+gtu[1][1]*Ct[1][1][1]+gtu[2][2]*
Ct[1][2][2])+gtu[0][1]*Ct[1][0][1]+gtu[0][2]*Ct[1][0][2]+gtu[1][2]*Ct[1][1][2];
      CalGamt[2] = half*(gtu[0][0]*Ct[2][0][0]+gtu[1][1]*Ct[2][1][1]+gtu[2][2]*
Ct[2][2][2])+gtu[0][1]*Ct[2][0][1]+gtu[0][2]*Ct[2][0][2]+gtu[1][2]*Ct[2][1][2];
      t2 = d_chi[0]*d_chi[0];
      t18 = half*dd_chi[0][1]-fourth*(d_chi[0]*d_chi[1]*inv_chi+Ct[0][0][1]*
d_chi[0]+Ct[1][0][1]*d_chi[1]+Ct[2][0][1]*d_chi[2]);
      t27 = half*dd_chi[0][2]-fourth*(d_chi[0]*d_chi[2]*inv_chi+Ct[0][0][2]*
d_chi[0]+Ct[1][0][2]*d_chi[1]+Ct[2][0][2]*d_chi[2]);
      t29 = d_chi[1]*d_chi[1];
      t45 = half*dd_chi[1][2]-fourth*(d_chi[1]*d_chi[2]*inv_chi+Ct[0][1][2]*
d_chi[0]+Ct[1][1][2]*d_chi[1]+Ct[2][1][2]*d_chi[2]);
      t47 = d_chi[2]*d_chi[2];
      Rpd_1[0][0] = half*dd_chi[0][0]-fourth*(t2*inv_chi+Ct[0][0][0]*d_chi[0]+
Ct[1][0][0]*d_chi[1]+Ct[2][0][0]*d_chi[2]);
      Rpd_1[0][1] = t18;
      Rpd_1[0][2] = t27;
      Rpd_1[1][0] = t18;
      Rpd_1[1][1] = half*dd_chi[1][1]-fourth*(t29*inv_chi+Ct[0][1][1]*d_chi[0]+
Ct[1][1][1]*d_chi[1]+Ct[2][1][1]*d_chi[2]);
      Rpd_1[1][2] = t45;
      Rpd_1[2][0] = t27;
      Rpd_1[2][1] = t45;
      Rpd_1[2][2] = half*dd_chi[2][2]-fourth*(t47*inv_chi+Ct[0][2][2]*d_chi[0]+
Ct[1][2][2]*d_chi[1]+Ct[2][2][2]*d_chi[2]);
      t8 = two*Ctd[0][0][0]+Ctd[0][0][0];
      t10 = two*Ctd[0][0][1];
      t11 = t10+Ctd[1][0][0];
      t13 = two*Ctd[0][0][2];
      t14 = t13+Ctd[2][0][0];
      t28 = t10+Ctd[0][0][1];
      t31 = two*Ctd[0][1][1]+Ctd[1][0][1];
      t33 = two*Ctd[0][1][2];
      t34 = t33+Ctd[2][0][1];
      t48 = t13+Ctd[0][0][2];
      t50 = t33+Ctd[1][0][2];
      t53 = two*Ctd[0][2][2]+Ctd[2][0][2];
      t67 = 2.0*CalGamt[0]*Ctd[0][0][0]+2.0*CalGamt[1]*Ctd[0][0][1]+2.0*CalGamt
[2]*Ctd[0][0][2]+gtu[0][0]*(Ct[0][0][0]*t8+Ct[1][0][0]*t11+Ct[2][0][0]*t14)+gtu
[0][1]*(Ct[0][0][1]*t8+Ct[1][0][1]*t11+Ct[2][0][1]*t14)+gtu[0][2]*(Ct[0][0][2]*
t8+Ct[1][0][2]*t11+Ct[2][0][2]*t14)+gtu[0][1]*(Ct[0][0][0]*t28+Ct[1][0][0]*t31+
Ct[2][0][0]*t34)+gtu[1][1]*(Ct[0][0][1]*t28+Ct[1][0][1]*t31+Ct[2][0][1]*t34)+
gtu[1][2]*(Ct[0][0][2]*t28+Ct[1][0][2]*t31+Ct[2][0][2]*t34)+gtu[0][2]*(Ct[0][0]
[0]*t48+Ct[1][0][0]*t50+Ct[2][0][0]*t53)+gtu[1][2]*(Ct[0][0][1]*t48+Ct[1][0][1]
*t50+Ct[2][0][1]*t53)+gtu[2][2]*(Ct[0][0][2]*t48+Ct[1][0][2]*t50+Ct[2][0][2]*
t53);
      t84 = Ctd[0][0][1]+Ctd[1][0][0];
      t86 = Ctd[0][1][1]+Ctd[1][0][1];
      t88 = Ctd[0][1][2]+Ctd[1][0][2];
      t95 = Ctd[1][0][2]+Ctd[2][0][1];
      t110 = Ct[0][1][2]*Ctd[0][0][0];
      t111 = Ct[1][0][2]*Ctd[1][0][1];
      t113 = Ct[1][1][2]*Ctd[0][0][1];
      t115 = Ct[2][1][2]*Ctd[0][0][2];
      t123 = Ctd[1][1][2]+Ctd[2][1][1];
      t138 = Ct[0][1][2]*Ctd[0][0][1];
      t139 = Ct[1][0][2]*Ctd[1][1][1];
      t141 = Ct[1][1][2]*Ctd[0][1][1];
      t143 = Ct[2][1][2]*Ctd[0][1][2];
      t151 = Ctd[1][2][2]+Ctd[2][1][2];
      t166 = Ct[0][1][2]*Ctd[0][0][2];
      t167 = Ct[1][0][2]*Ctd[1][1][2];
      t169 = Ct[1][1][2]*Ctd[0][1][2];
      t171 = Ct[2][1][2]*Ctd[0][2][2];
      MapleGenVar1 = CalGamt[0]*t84+CalGamt[1]*t86+CalGamt[2]*t88+gtu[0][0]*(Ct
[0][0][0]*t84+Ct[0][0][1]*Ctd[0][0][0]+2.0*Ct[1][0][0]*Ctd[1][0][1]+Ct[1][0][1]
*Ctd[0][0][1]+Ct[2][0][0]*t95+Ct[2][0][1]*Ctd[0][0][2])+gtu[0][1]*(Ct[0][0][1]*
t84+Ct[0][1][1]*Ctd[0][0][0]+2.0*Ct[1][0][1]*Ctd[1][0][1]+Ct[1][1][1]*Ctd[0][0]
[1]+Ct[2][0][1]*t95+Ct[2][1][1]*Ctd[0][0][2])+gtu[0][2]*(Ct[0][0][2]*t84+t110+
2.0*t111+t113+Ct[2][0][2]*t95+t115);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t86+Ct[0][0][1]*Ctd[0]
[0][1]+2.0*Ct[1][0][0]*Ctd[1][1][1]+Ct[1][0][1]*Ctd[0][1][1]+Ct[2][0][0]*t123+
Ct[2][0][1]*Ctd[0][1][2])+gtu[1][1]*(Ct[0][0][1]*t86+Ct[0][1][1]*Ctd[0][0][1]+
2.0*Ct[1][0][1]*Ctd[1][1][1]+Ct[1][1][1]*Ctd[0][1][1]+Ct[2][0][1]*t123+Ct[2][1]
[1]*Ctd[0][1][2]);
      t174 = MapleGenVar2+gtu[1][2]*(Ct[0][0][2]*t86+t138+2.0*t139+t141+Ct[2]
[0][2]*t123+t143)+gtu[0][2]*(Ct[0][0][0]*t88+Ct[0][0][1]*Ctd[0][0][2]+2.0*Ct[1]
[0][0]*Ctd[1][1][2]+Ct[1][0][1]*Ctd[0][1][2]+Ct[2][0][0]*t151+Ct[2][0][1]*Ctd
[0][2][2])+gtu[1][2]*(Ct[0][0][1]*t88+Ct[0][1][1]*Ctd[0][0][2]+2.0*Ct[1][0][1]*
Ctd[1][1][2]+Ct[1][1][1]*Ctd[0][1][2]+Ct[2][0][1]*t151+Ct[2][1][1]*Ctd[0][2][2]
)+gtu[2][2]*(Ct[0][0][2]*t88+t166+2.0*t167+t169+Ct[2][0][2]*t151+t171);
      t192 = fourth*t174+half*(gtd[0][0]*d_Gamt[1][0]+gtd[0][1]*d_Gamt[0][0]+
gtd[0][1]*d_Gamt[1][1]+gtd[1][1]*d_Gamt[0][1]+gtd[0][2]*d_Gamt[1][2]+gtd[1][2]*
d_Gamt[0][2]-gtu[0][0]*dd_gtd[0][0][0][1]-gtu[1][1]*dd_gtd[1][1][0][1]-gtu[2]
[2]*dd_gtd[2][2][0][1]-two*(gtu[0][1]*dd_gtd[0][1][0][1]+gtu[0][2]*dd_gtd[0][2]
[0][1]+gtu[1][2]*dd_gtd[1][2][0][1]));
      t193 = Ctd[0][0][2]+Ctd[2][0][0];
      t195 = Ctd[0][1][2]+Ctd[2][0][1];
      t197 = Ctd[0][2][2]+Ctd[2][0][2];
      t208 = Ct[0][0][1]*t193;
      t209 = Ct[1][0][1]*t95;
      t211 = 2.0*Ct[2][0][1]*Ctd[2][0][2];
      t232 = Ct[0][0][1]*t195;
      t233 = Ct[1][0][1]*t123;
      t235 = 2.0*Ct[2][0][1]*Ctd[2][1][2];
      t256 = Ct[0][0][1]*t197;
      t257 = Ct[1][0][1]*t151;
      t259 = 2.0*Ct[2][0][1]*Ctd[2][2][2];
      MapleGenVar1 = CalGamt[0]*t193+CalGamt[1]*t195+CalGamt[2]*t197+gtu[0][0]*
(Ct[0][0][0]*t193+Ct[0][0][2]*Ctd[0][0][0]+Ct[1][0][0]*t95+Ct[1][0][2]*Ctd[0]
[0][1]+2.0*Ct[2][0][0]*Ctd[2][0][2]+Ct[2][0][2]*Ctd[0][0][2])+gtu[0][1]*(t208+
t110+t209+t113+t211+t115)+gtu[0][2]*(Ct[0][0][2]*t193+Ct[0][2][2]*Ctd[0][0][0]+
Ct[1][0][2]*t95+Ct[1][2][2]*Ctd[0][0][1]+2.0*Ct[2][0][2]*Ctd[2][0][2]+Ct[2][2]
[2]*Ctd[0][0][2]);
      t271 = MapleGenVar1+gtu[0][1]*(Ct[0][0][0]*t195+Ct[0][0][2]*Ctd[0][0][1]+
Ct[1][0][0]*t123+Ct[1][0][2]*Ctd[0][1][1]+2.0*Ct[2][0][0]*Ctd[2][1][2]+Ct[2][0]
[2]*Ctd[0][1][2])+gtu[1][1]*(t232+t138+t233+t141+t235+t143)+gtu[1][2]*(Ct[0][0]
[2]*t195+Ct[0][2][2]*Ctd[0][0][1]+Ct[1][0][2]*t123+Ct[1][2][2]*Ctd[0][1][1]+2.0
*Ct[2][0][2]*Ctd[2][1][2]+Ct[2][2][2]*Ctd[0][1][2])+gtu[0][2]*(Ct[0][0][0]*t197
+Ct[0][0][2]*Ctd[0][0][2]+Ct[1][0][0]*t151+Ct[1][0][2]*Ctd[0][1][2]+2.0*Ct[2]
[0][0]*Ctd[2][2][2]+Ct[2][0][2]*Ctd[0][2][2])+gtu[1][2]*(t256+t166+t257+t169+
t259+t171)+gtu[2][2]*(Ct[0][0][2]*t197+Ct[0][2][2]*Ctd[0][0][2]+Ct[1][0][2]*
t151+Ct[1][2][2]*Ctd[0][1][2]+2.0*Ct[2][0][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[0]
[2][2]);
      t289 = fourth*t271+half*(gtd[0][0]*d_Gamt[2][0]+gtd[0][2]*d_Gamt[0][0]+
gtd[0][1]*d_Gamt[2][1]+gtd[1][2]*d_Gamt[0][1]+gtd[0][2]*d_Gamt[2][2]+gtd[2][2]*
d_Gamt[0][2]-gtu[0][0]*dd_gtd[0][0][0][2]-gtu[1][1]*dd_gtd[1][1][0][2]-gtu[2]
[2]*dd_gtd[2][2][0][2]-two*(gtu[0][1]*dd_gtd[0][1][0][2]+gtu[0][2]*dd_gtd[0][2]
[0][2]+gtu[1][2]*dd_gtd[1][2][0][2]));
      t297 = two*Ctd[1][0][0]+Ctd[0][0][1];
      t299 = two*Ctd[1][0][1];
      t300 = t299+Ctd[1][0][1];
      t302 = two*Ctd[1][0][2];
      t303 = t302+Ctd[2][0][1];
      t317 = t299+Ctd[0][1][1];
      t320 = two*Ctd[1][1][1]+Ctd[1][1][1];
      t322 = two*Ctd[1][1][2];
      t323 = t322+Ctd[2][1][1];
      t337 = t302+Ctd[0][1][2];
      t339 = t322+Ctd[1][1][2];
      t342 = two*Ctd[1][2][2]+Ctd[2][1][2];
      t356 = 2.0*CalGamt[0]*Ctd[1][0][1]+2.0*CalGamt[1]*Ctd[1][1][1]+2.0*
CalGamt[2]*Ctd[1][1][2]+gtu[0][0]*(Ct[0][0][1]*t297+Ct[1][0][1]*t300+Ct[2][0]
[1]*t303)+gtu[0][1]*(Ct[0][1][1]*t297+Ct[1][1][1]*t300+Ct[2][1][1]*t303)+gtu[0]
[2]*(Ct[0][1][2]*t297+Ct[1][1][2]*t300+Ct[2][1][2]*t303)+gtu[0][1]*(Ct[0][0][1]
*t317+Ct[1][0][1]*t320+Ct[2][0][1]*t323)+gtu[1][1]*(Ct[0][1][1]*t317+Ct[1][1]
[1]*t320+Ct[2][1][1]*t323)+gtu[1][2]*(Ct[0][1][2]*t317+Ct[1][1][2]*t320+Ct[2]
[1][2]*t323)+gtu[0][2]*(Ct[0][0][1]*t337+Ct[1][0][1]*t339+Ct[2][0][1]*t342)+gtu
[1][2]*(Ct[0][1][1]*t337+Ct[1][1][1]*t339+Ct[2][1][1]*t342)+gtu[2][2]*(Ct[0][1]
[2]*t337+Ct[1][1][2]*t339+Ct[2][1][2]*t342);
      MapleGenVar1 = CalGamt[0]*t95+CalGamt[1]*t123+CalGamt[2]*t151+gtu[0][0]*(
t208+Ct[0][0][2]*Ctd[1][0][0]+t209+t111+t211+Ct[2][0][2]*Ctd[1][0][2])+gtu[0]
[1]*(Ct[0][1][1]*t193+Ct[0][1][2]*Ctd[1][0][0]+Ct[1][1][1]*t95+Ct[1][1][2]*Ctd
[1][0][1]+2.0*Ct[2][1][1]*Ctd[2][0][2]+Ct[2][1][2]*Ctd[1][0][2])+gtu[0][2]*(Ct
[0][1][2]*t193+Ct[0][2][2]*Ctd[1][0][0]+Ct[1][1][2]*t95+Ct[1][2][2]*Ctd[1][0]
[1]+2.0*Ct[2][1][2]*Ctd[2][0][2]+Ct[2][2][2]*Ctd[1][0][2]);
      MapleGenVar2 = MapleGenVar1+gtu[0][1]*(t232+Ct[0][0][2]*Ctd[1][0][1]+t233
+t139+t235+Ct[2][0][2]*Ctd[1][1][2])+gtu[1][1]*(Ct[0][1][1]*t195+Ct[0][1][2]*
Ctd[1][0][1]+Ct[1][1][1]*t123+Ct[1][1][2]*Ctd[1][1][1]+2.0*Ct[2][1][1]*Ctd[2]
[1][2]+Ct[2][1][2]*Ctd[1][1][2]);
      t442 = MapleGenVar2+gtu[1][2]*(Ct[0][1][2]*t195+Ct[0][2][2]*Ctd[1][0][1]+
Ct[1][1][2]*t123+Ct[1][2][2]*Ctd[1][1][1]+2.0*Ct[2][1][2]*Ctd[2][1][2]+Ct[2][2]
[2]*Ctd[1][1][2])+gtu[0][2]*(t256+Ct[0][0][2]*Ctd[1][0][2]+t257+t167+t259+Ct[2]
[0][2]*Ctd[1][2][2])+gtu[1][2]*(Ct[0][1][1]*t197+Ct[0][1][2]*Ctd[1][0][2]+Ct[1]
[1][1]*t151+Ct[1][1][2]*Ctd[1][1][2]+2.0*Ct[2][1][1]*Ctd[2][2][2]+Ct[2][1][2]*
Ctd[1][2][2])+gtu[2][2]*(Ct[0][1][2]*t197+Ct[0][2][2]*Ctd[1][0][2]+Ct[1][1][2]*
t151+Ct[1][2][2]*Ctd[1][1][2]+2.0*Ct[2][1][2]*Ctd[2][2][2]+Ct[2][2][2]*Ctd[1]
[2][2]);
      t460 = fourth*t442+half*(gtd[0][1]*d_Gamt[2][0]+gtd[0][2]*d_Gamt[1][0]+
gtd[1][1]*d_Gamt[2][1]+gtd[1][2]*d_Gamt[1][1]+gtd[1][2]*d_Gamt[2][2]+gtd[2][2]*
d_Gamt[1][2]-gtu[0][0]*dd_gtd[0][0][1][2]-gtu[1][1]*dd_gtd[1][1][1][2]-gtu[2]
[2]*dd_gtd[2][2][1][2]-two*(gtu[0][1]*dd_gtd[0][1][1][2]+gtu[0][2]*dd_gtd[0][2]
[1][2]+gtu[1][2]*dd_gtd[1][2][1][2]));
      t468 = two*Ctd[2][0][0]+Ctd[0][0][2];
      t470 = two*Ctd[2][0][1];
      t471 = t470+Ctd[1][0][2];
      t473 = two*Ctd[2][0][2];
      t474 = t473+Ctd[2][0][2];
      t488 = t470+Ctd[0][1][2];
      t491 = two*Ctd[2][1][1]+Ctd[1][1][2];
      t493 = two*Ctd[2][1][2];
      t494 = t493+Ctd[2][1][2];
      t508 = t473+Ctd[0][2][2];
      t510 = t493+Ctd[1][2][2];
      t513 = two*Ctd[2][2][2]+Ctd[2][2][2];
      t527 = 2.0*CalGamt[0]*Ctd[2][0][2]+2.0*CalGamt[1]*Ctd[2][1][2]+2.0*
CalGamt[2]*Ctd[2][2][2]+gtu[0][0]*(Ct[0][0][2]*t468+Ct[1][0][2]*t471+Ct[2][0]
[2]*t474)+gtu[0][1]*(Ct[0][1][2]*t468+Ct[1][1][2]*t471+Ct[2][1][2]*t474)+gtu[0]
[2]*(Ct[0][2][2]*t468+Ct[1][2][2]*t471+Ct[2][2][2]*t474)+gtu[0][1]*(Ct[0][0][2]
*t488+Ct[1][0][2]*t491+Ct[2][0][2]*t494)+gtu[1][1]*(Ct[0][1][2]*t488+Ct[1][1]
[2]*t491+Ct[2][1][2]*t494)+gtu[1][2]*(Ct[0][2][2]*t488+Ct[1][2][2]*t491+Ct[2]
[2][2]*t494)+gtu[0][2]*(Ct[0][0][2]*t508+Ct[1][0][2]*t510+Ct[2][0][2]*t513)+gtu
[1][2]*(Ct[0][1][2]*t508+Ct[1][1][2]*t510+Ct[2][1][2]*t513)+gtu[2][2]*(Ct[0][2]
[2]*t508+Ct[1][2][2]*t510+Ct[2][2][2]*t513);
      Rtd[0][0] = fourth*t67-half*(gtu[0][0]*dd_gtd[0][0][0][0]+2.0*gtu[0][1]*
dd_gtd[0][1][0][0]+2.0*gtu[0][2]*dd_gtd[0][2][0][0]+gtu[1][1]*dd_gtd[1][1][0]
[0]+2.0*gtu[1][2]*dd_gtd[1][2][0][0]+gtu[2][2]*dd_gtd[2][2][0][0])+gtd[0][0]*
d_Gamt[0][0]+gtd[0][1]*d_Gamt[0][1]+gtd[0][2]*d_Gamt[0][2];
      Rtd[0][1] = t192;
      Rtd[0][2] = t289;
      Rtd[1][0] = t192;
      Rtd[1][1] = fourth*t356-half*(gtu[0][0]*dd_gtd[0][0][1][1]+2.0*gtu[0][1]*
dd_gtd[0][1][1][1]+2.0*gtu[0][2]*dd_gtd[0][2][1][1]+gtu[1][1]*dd_gtd[1][1][1]
[1]+2.0*gtu[1][2]*dd_gtd[1][2][1][1]+gtu[2][2]*dd_gtd[2][2][1][1])+gtd[0][1]*
d_Gamt[1][0]+gtd[1][1]*d_Gamt[1][1]+gtd[1][2]*d_Gamt[1][2];
      Rtd[1][2] = t460;
      Rtd[2][0] = t289;
      Rtd[2][1] = t460;
      Rtd[2][2] = fourth*t527-half*(gtu[0][0]*dd_gtd[0][0][2][2]+2.0*gtu[0][1]*
dd_gtd[0][1][2][2]+2.0*gtu[0][2]*dd_gtd[0][2][2][2]+gtu[1][1]*dd_gtd[1][1][2]
[2]+2.0*gtu[1][2]*dd_gtd[1][2][2][2]+gtu[2][2]*dd_gtd[2][2][2][2])+gtd[0][2]*
d_Gamt[2][0]+gtd[1][2]*d_Gamt[2][1]+gtd[2][2]*d_Gamt[2][2];
      t9 = inv_chi*eight_pi_G;
      t30 = chi*(Alpha*Rtd[0][1]-dd_Alpha[0][1]+half*(Ct[0][0][1]*d_Alpha[0]+Ct
[1][0][1]*d_Alpha[1]+Ct[2][0][1]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[0][1]+Alpha*
Rpd_1[0][1]-half*(d_Alpha[0]*d_chi[1]+d_Alpha[1]*d_chi[0]);
      t46 = chi*(Alpha*Rtd[0][2]-dd_Alpha[0][2]+half*(Ct[0][0][2]*d_Alpha[0]+Ct
[1][0][2]*d_Alpha[1]+Ct[2][0][2]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[0][2]+Alpha*
Rpd_1[0][2]-half*(d_Alpha[0]*d_chi[2]+d_Alpha[2]*d_chi[0]);
      t75 = chi*(Alpha*Rtd[1][2]-dd_Alpha[1][2]+half*(Ct[0][1][2]*d_Alpha[0]+Ct
[1][1][2]*d_Alpha[1]+Ct[2][1][2]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[1][2]+Alpha*
Rpd_1[1][2]-half*(d_Alpha[1]*d_chi[2]+d_Alpha[2]*d_chi[1]);
      Psi1[0][0] = chi*(Alpha*Rtd[0][0]-dd_Alpha[0][0]+half*(Ct[0][0][0]*
d_Alpha[0]+Ct[1][0][0]*d_Alpha[1]+Ct[2][0][0]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[0]
[0]+Alpha*Rpd_1[0][0]-d_Alpha[0]*d_chi[0];
      Psi1[0][1] = t30;
      Psi1[0][2] = t46;
      Psi1[1][0] = t30;
      Psi1[1][1] = chi*(Alpha*Rtd[1][1]-dd_Alpha[1][1]+half*(Ct[0][1][1]*
d_Alpha[0]+Ct[1][1][1]*d_Alpha[1]+Ct[2][1][1]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[1]
[1]+Alpha*Rpd_1[1][1]-d_Alpha[1]*d_chi[1];
      Psi1[1][2] = t75;
      Psi1[2][0] = t46;
      Psi1[2][1] = t75;
      Psi1[2][2] = chi*(Alpha*Rtd[2][2]-dd_Alpha[2][2]+half*(Ct[0][2][2]*
d_Alpha[0]+Ct[1][2][2]*d_Alpha[1]+Ct[2][2][2]*d_Alpha[2]))-t9*Alpha*pTtd_ADM[2]
[2]+Alpha*Rpd_1[2][2]-d_Alpha[2]*d_chi[2];
      third_trPsi1 = third*(Psi1[0][0]*gtu[0][0]+Psi1[1][1]*gtu[1][1]+Psi1[2]
[2]*gtu[2][2]+two*(Psi1[0][1]*gtu[0][1]+Psi1[0][2]*gtu[0][2]+Psi1[1][2]*gtu[1]
[2]));
      t4 = Psi1[0][1]-third_trPsi1*gtd[0][1];
      t6 = Psi1[0][2]-third_trPsi1*gtd[0][2];
      t10 = Psi1[1][2]-third_trPsi1*gtd[1][2];
      Psi1TF[0][0] = Psi1[0][0]-third_trPsi1*gtd[0][0];
      Psi1TF[0][1] = t4;
      Psi1TF[0][2] = t6;
      Psi1TF[1][0] = t4;
      Psi1TF[1][1] = Psi1[1][1]-third_trPsi1*gtd[1][1];
      Psi1TF[1][2] = t10;
      Psi1TF[2][0] = t6;
      Psi1TF[2][1] = t10;
      Psi1TF[2][2] = Psi1[2][2]-third_trPsi1*gtd[2][2];
      t20 = gtd[0][0]*d_Betau[1][0]+gtd[0][1]*d_Betau[0][0]+gtd[0][1]*d_Betau
[1][1]+gtd[1][1]*d_Betau[0][1]+gtd[0][2]*d_Betau[1][2]+gtd[1][2]*d_Betau[0][2]-
two*(third*gtd[0][1]*div_Beta+Alpha*Atd[0][1]);
      t32 = gtd[0][0]*d_Betau[2][0]+gtd[0][2]*d_Betau[0][0]+gtd[0][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[0][1]+gtd[0][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[0][2]-
two*(third*gtd[0][2]*div_Beta+Alpha*Atd[0][2]);
      t52 = gtd[0][1]*d_Betau[2][0]+gtd[0][2]*d_Betau[1][0]+gtd[1][1]*d_Betau
[2][1]+gtd[1][2]*d_Betau[1][1]+gtd[1][2]*d_Betau[2][2]+gtd[2][2]*d_Betau[1][2]-
two*(third*gtd[1][2]*div_Beta+Alpha*Atd[1][2]);
      gtd_rhs[0][0] = two*(gtd[0][0]*d_Betau[0][0]+gtd[0][1]*d_Betau[0][1]+gtd
[0][2]*d_Betau[0][2]-third*gtd[0][0]*div_Beta-Alpha*Atd[0][0]);
      gtd_rhs[0][1] = t20;
      gtd_rhs[0][2] = t32;
      gtd_rhs[1][0] = t20;
      gtd_rhs[1][1] = two*(gtd[0][1]*d_Betau[1][0]+gtd[1][1]*d_Betau[1][1]+gtd
[1][2]*d_Betau[1][2]-third*gtd[1][1]*div_Beta-Alpha*Atd[1][1]);
      gtd_rhs[1][2] = t52;
      gtd_rhs[2][0] = t32;
      gtd_rhs[2][1] = t52;
      gtd_rhs[2][2] = two*(gtd[0][2]*d_Betau[2][0]+gtd[1][2]*d_Betau[2][1]+gtd
[2][2]*d_Betau[2][2]-third*gtd[2][2]*div_Beta-Alpha*Atd[2][2]);
      t14 = Alpha*trK-twothirds*div_Beta;
      t24 = two*Alpha;
      t30 = Atd[0][0]*d_Betau[1][0]+Atd[0][1]*d_Betau[0][0]+Atd[0][1]*d_Betau
[1][1]+Atd[1][1]*d_Betau[0][1]+Atd[0][2]*d_Betau[1][2]+Atd[1][2]*d_Betau[0][2]+
Atd[0][1]*t14+Psi1TF[0][1]-t24*(Atd[0][0]*Atud[0][1]+Atd[0][1]*Atud[1][1]+Atd
[0][2]*Atud[2][1]);
      t43 = Atd[0][0]*d_Betau[2][0]+Atd[0][2]*d_Betau[0][0]+Atd[0][1]*d_Betau
[2][1]+Atd[1][2]*d_Betau[0][1]+Atd[0][2]*d_Betau[2][2]+Atd[2][2]*d_Betau[0][2]+
Atd[0][2]*t14+Psi1TF[0][2]-t24*(Atd[0][0]*Atud[0][2]+Atd[0][1]*Atud[1][2]+Atd
[0][2]*Atud[2][2]);
      t69 = Atd[0][1]*d_Betau[2][0]+Atd[0][2]*d_Betau[1][0]+Atd[1][1]*d_Betau
[2][1]+Atd[1][2]*d_Betau[1][1]+Atd[1][2]*d_Betau[2][2]+Atd[2][2]*d_Betau[1][2]+
Atd[1][2]*t14+Psi1TF[1][2]-t24*(Atd[0][1]*Atud[0][2]+Atd[1][1]*Atud[1][2]+Atd
[1][2]*Atud[2][2]);
      Atd_rhs[0][0] = two*(Atd[0][0]*(d_Betau[0][0]-Alpha*Atud[0][0])+Atd[0][1]
*(d_Betau[0][1]-Alpha*Atud[1][0])+Atd[0][2]*(d_Betau[0][2]-Alpha*Atud[2][0]))+
Atd[0][0]*t14+Psi1TF[0][0];
      Atd_rhs[0][1] = t30;
      Atd_rhs[0][2] = t43;
      Atd_rhs[1][0] = t30;
      Atd_rhs[1][1] = two*(Atd[0][1]*(d_Betau[1][0]-Alpha*Atud[0][1])+Atd[1][1]
*(d_Betau[1][1]-Alpha*Atud[1][1])+Atd[1][2]*(d_Betau[1][2]-Alpha*Atud[2][1]))+
Atd[1][1]*t14+Psi1TF[1][1];
      Atd_rhs[1][2] = t69;
      Atd_rhs[2][0] = t43;
      Atd_rhs[2][1] = t69;
      Atd_rhs[2][2] = two*(Atd[0][2]*(d_Betau[2][0]-Alpha*Atud[0][2])+Atd[1][2]
*(d_Betau[2][1]-Alpha*Atud[1][2])+Atd[2][2]*(d_Betau[2][2]-Alpha*Atud[2][2]))+
Atd[2][2]*t14+Psi1TF[2][2];
      t28 = eight_pi_G*inv_chi;
      t30 = twothirds*d_trK[0]+t28*Jtd_ADM[0];
      t34 = twothirds*d_trK[1]+t28*Jtd_ADM[1];
      t38 = twothirds*d_trK[2]+t28*Jtd_ADM[2];
      t42 = threehalves*Alpha;
      t45 = -t42*d_chi[0]*inv_chi-d_Alpha[0];
      t49 = -t42*d_chi[1]*inv_chi-d_Alpha[1];
      t53 = -t42*d_chi[2]*inv_chi-d_Alpha[2];
      Gamt_rhs[0] = twothirds*CalGamt[0]*div_Beta-CalGamt[0]*d_Betau[0][0]-
CalGamt[1]*d_Betau[1][0]-CalGamt[2]*d_Betau[2][0]+gtu[0][0]*dd_Betau[0][0][0]+
gtu[1][1]*dd_Betau[1][1][0]+gtu[2][2]*dd_Betau[2][2][0]+two*(gtu[0][1]*dd_Betau
[0][1][0]+gtu[0][2]*dd_Betau[0][2][0]+gtu[1][2]*dd_Betau[1][2][0])+third*(gtu
[0][0]*d_div_Beta[0]+gtu[0][1]*d_div_Beta[1]+gtu[0][2]*d_div_Beta[2])+two*(
Alpha*(half*(Ct[0][0][0]*Atu[0][0]+Ct[0][1][1]*Atu[1][1]+Ct[0][2][2]*Atu[2][2])
+Ct[0][0][1]*Atu[0][1]+Ct[0][0][2]*Atu[0][2]+Ct[0][1][2]*Atu[1][2]-gtu[0][0]*
t30-gtu[0][1]*t34-gtu[0][2]*t38)+Atu[0][0]*t45+Atu[0][1]*t49+Atu[0][2]*t53);
      Gamt_rhs[1] = twothirds*CalGamt[1]*div_Beta-CalGamt[0]*d_Betau[0][1]-
CalGamt[1]*d_Betau[1][1]-CalGamt[2]*d_Betau[2][1]+gtu[0][0]*dd_Betau[0][0][1]+
gtu[1][1]*dd_Betau[1][1][1]+gtu[2][2]*dd_Betau[2][2][1]+two*(gtu[0][1]*dd_Betau
[0][1][1]+gtu[0][2]*dd_Betau[0][2][1]+gtu[1][2]*dd_Betau[1][2][1])+third*(gtu
[0][1]*d_div_Beta[0]+gtu[1][1]*d_div_Beta[1]+gtu[1][2]*d_div_Beta[2])+two*(
Alpha*(half*(Ct[1][0][0]*Atu[0][0]+Ct[1][1][1]*Atu[1][1]+Ct[1][2][2]*Atu[2][2])
+Ct[1][0][1]*Atu[0][1]+Ct[1][0][2]*Atu[0][2]+Ct[1][1][2]*Atu[1][2]-gtu[0][1]*
t30-gtu[1][1]*t34-gtu[1][2]*t38)+Atu[0][1]*t45+Atu[1][1]*t49+Atu[1][2]*t53);
      Gamt_rhs[2] = twothirds*CalGamt[2]*div_Beta-CalGamt[0]*d_Betau[0][2]-
CalGamt[1]*d_Betau[1][2]-CalGamt[2]*d_Betau[2][2]+gtu[0][0]*dd_Betau[0][0][2]+
gtu[1][1]*dd_Betau[1][1][2]+gtu[2][2]*dd_Betau[2][2][2]+two*(gtu[0][1]*dd_Betau
[0][1][2]+gtu[0][2]*dd_Betau[0][2][2]+gtu[1][2]*dd_Betau[1][2][2])+third*(gtu
[0][2]*d_div_Beta[0]+gtu[1][2]*d_div_Beta[1]+gtu[2][2]*d_div_Beta[2])+two*(
Alpha*(half*(Ct[2][0][0]*Atu[0][0]+Ct[2][1][1]*Atu[1][1]+Ct[2][2][2]*Atu[2][2])
+Ct[2][0][1]*Atu[0][1]+Ct[2][0][2]*Atu[0][2]+Ct[2][1][2]*Atu[1][2]-gtu[0][2]*
t30-gtu[1][2]*t34-gtu[2][2]*t38)+Atu[0][2]*t45+Atu[1][2]*t49+Atu[2][2]*t53);
      t3 = threefourths*(lambda_f0+lambda_f1*Alpha);
      Betau_rhs[0] = t3*Bu[0];
      Betau_rhs[1] = t3*Bu[1];
      Betau_rhs[2] = t3*Bu[2];
      Bu_rhs[0] = Gamt_rhs[0]-Bu[0]*feta;
      Bu_rhs[1] = Gamt_rhs[1]-Bu[1]*feta;
      Bu_rhs[2] = Gamt_rhs[2]-Bu[2]*feta;
      Alpha_rhs = -two*Alpha*(trK-trK0);
      chi_rhs = -twothirds*chi*(div_Beta-Alpha*trK);
      t8 = gtu[0][0];
      t11 = gtu[1][1];
      t14 = gtu[2][2];
      t17 = gtu[0][1];
      t20 = gtu[0][2];
      t23 = gtu[1][2];
      tr_pT = inv_chi*(t8*pTtd_ADM[0][0]+t11*pTtd_ADM[1][1]+t14*pTtd_ADM[2][2]+
two*(t17*pTtd_ADM[0][1]+t20*pTtd_ADM[0][2]+t23*pTtd_ADM[1][2]));
      t49 = trK*trK;
      t71 = d_Alpha[0];
      t74 = d_Alpha[1];
      t77 = d_Alpha[2];
      t82 = d_chi[0];
      t85 = d_chi[1];
      t88 = d_chi[2];
      trK_rhs = Alpha*(Atd[0][0]*Atu[0][0]+Atd[1][1]*Atu[1][1]+Atd[2][2]*Atu[2]
[2]+two*(Atd[0][1]*Atu[0][1]+Atd[0][2]*Atu[0][2]+Atd[1][2]*Atu[1][2])+third*t49
+four_pi_G*(rho_ADM+tr_pT))-chi*(t8*dd_Alpha[0][0]+2.0*t17*dd_Alpha[0][1]+2.0*
t20*dd_Alpha[0][2]+t11*dd_Alpha[1][1]+2.0*t23*dd_Alpha[1][2]+t14*dd_Alpha[2][2]
-CalGamt[0]*t71-CalGamt[1]*t74-CalGamt[2]*t77)+half*(t8*t71*t82+t17*t71*t85+t20
*t71*t88+t17*t74*t82+t11*t74*t85+t23*t74*t88+t20*t77*t82+t23*t77*t85+t14*t77*
t88);
