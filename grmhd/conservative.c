#include "conservative.h"

#include <stdio.h>
#include <stdbool.h>

#include "hawaii.h"


//These give the integral under the basis functions (in 1d) near the edges.
// Note that these are specific to the 4 point stencil case. To get the
// total in 2d or 3d, just multiple these. The full value with the volume
// factor includes a \Delta x / 2^j in each direction, where \Delta x is
// the spacing at the base level.
#ifndef PERIODIC
static double total_weight_[5] = {
  121.0 / 360.0, 462.0 / 360.0, 303.0 / 360.0, 374.0 / 360.0, 1.0
};
#endif


static FILE *cons_file_;


//TODO: Figure out what changes in CYL


void test_fluid_conservation(double tcurr, unsigned stamp) {
  double vals[n_cons];
  for (size_t i = 0; i < n_cons; ++i) {
    vals[i] = 0.0;
  }

  //loop over the grid, adding it all up; first the array
  for (int i = 0; i < npts_in_array; ++i) {
    coll_point_t *point = &coll_points->array[i];
    wavelet_trans(point, auxiliary_set_wavelet, 0, conservative_mask);
    compute_point_contribution(vals, point);
  }
  //then the hash table
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      //if (ptr->point->time_stamp == stamp) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        wavelet_trans(ptr->point, auxiliary_set_wavelet, 0, conservative_mask);
        compute_point_contribution(vals, ptr->point);
      }
      ptr = ptr->next;
    }
  }

  //Compute the base level volume factor
  double dVol = 1.0;
  for (dir_t dir = x_dir; dir < n_dim; ++dir) {
    dVol *= L_dim[dir] / ns[dir];
  }
  for (int i = 0; i < n_cons; ++i) {
    vals[i] *= dVol;
  }

  //write a line to the file
  fprintf(cons_file_, "%.12lg %u ", tcurr, stamp);
  for (int i = 0; i < n_cons; ++i) {
    fprintf(cons_file_, "%.12lg ", vals[i]);
  }
  fprintf(cons_file_, "\n");
}


void compute_point_contribution(double *vals, const coll_point_t *point) {
  double factor = 1.0;

#ifndef PERIODIC
  //the index location factors
  int step = 1 << (JJ - point->level);
  for (dir_t dir = x_dir; dir < n_dim; ++dir) {
    int direction_index = point->index.idx[dir] / step;
    int max_direction_index = max_index[dir] / step - direction_index;
    if (max_direction_index < 4) {
      factor *= total_weight_[max_direction_index];
    } else if (direction_index < 4) {
      factor *= total_weight_[direction_index];
    } // else just use a factor of 1.0
  }
#endif

  //the level factors
  if (point->level) {
    factor /= (double)(1 << (n_dim * point->level));
    for (int i = 0; i < n_cons; ++i) {
      vals[i] += factor * point->wavelet[0][U_D + i];
    }
  } else {
    for (int i = 0; i < n_cons; ++i) {
      vals[i] += factor * point->u[0][U_D + i];
    }
  }
}


bool open_cons_file(int freq, int restart_flag) {
  if (freq == 0) {
    return true;
  }

  cons_file_ = fopen("run_conservation.txt", (restart_flag ? "a" : "w"));
  if (cons_file_ == NULL) {
    perror("Error opening run conservation file:");
    return false;
  }

  return true;
}


void close_cons_file() {
  if (cons_file_ != NULL) {
    fclose(cons_file_);
  }
}
