#ifndef HAWAII_H
#define HAWAII_H

#include "hawaii-param.h"
#include "hawaii-storage.h"
#include "hawaii-utils.h"
#include "hawaii-integr.h"
#include "hawaii-prob.h"
#include "hawaii-types.h"
#include "hawaii-wavelet.h"

#ifdef RMHD
#include "hawaii-rmhd-param.h"
#endif

#ifdef CILK
#include <cilk/cilk.h>
#include <cilk/cilk_api.h>
#endif


//Make some convenience macros to hide all the CILK checks
#ifdef CILK
  #define FOR cilk_for
  #define LOCK(x) (sync_tatas_acquire(&_table_locks[morton_key(&x) % CILK_LOCK_TBL_SIZE]))
  #define UNLOCK(x) (sync_tatas_release(&_table_locks[morton_key(&x) % CILK_LOCK_TBL_SIZE]))
#else
  #define FOR for
  #define LOCK(x)
  #define UNLOCK(x)
#endif


#endif
