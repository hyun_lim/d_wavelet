#ifndef HAWAII_RMHD_H
#define HAWAII_RMHD_H

#include "hawaii-param.h"
#include "hawaii-types.h"

#define NSTENCIL 7
#define STENCIL_CENTER (NSTENCIL-1)/2

#define ANSI_COLOR_RED     "\x1b[31m"
#define ANSI_COLOR_GREEN   "\x1b[32m"
#define ANSI_COLOR_YELLOW  "\x1b[33m"
#define ANSI_COLOR_BLUE    "\x1b[34m"
#define ANSI_COLOR_MAGENTA "\x1b[35m"
#define ANSI_COLOR_CYAN    "\x1b[36m"
#define ANSI_COLOR_RESET   "\x1b[0m"

/* conservative variables */
#define n_cons 9
#define U_D 9
#define U_SX 10
#define U_SY 11
#define U_SZ 12
#define U_TAU 13
#define U_BX 14
#define U_BY 15
#define U_BZ 16
#define U_PSI 17

/* primitive variables */
#define n_prims 9
#define V_RHO 0
#define V_VX 1
#define V_VY 2
#define V_VZ 3
#define V_P  4
#define V_BX 5
#define V_BY 6
#define V_BZ 7
#define V_PSI 8

/* fluxes */
#define F_D 18
#define F_SX 19
#define F_SY 20
#define F_SZ 21
#define F_TAU 22
#define F_BX 23
#define F_BY 24
#define F_BZ 25
#define F_PSI 26

/* root solving parameter definitions */
#define NFPAR     24

#define FP_SSQ 0
#define FP_BSQ 1
#define FP_BS 2
#define FP_D 3
#define FP_TAU 4
#define FP_GAMMA 5
#define FP_VSQ 6
#define FP_COORD_X 7
#define FP_COORD_Y 8
#define FP_COORD_Z 9
#define FP_CHR 10
#define FP_INDEX_X 11
#define FP_INDEX_Y 12
#define FP_INDEX_Z 13
#define FP_KAPPA 14
#define FP_TRACE 15
#define FP_CAL_PRESS 16
#define FP_Q 17
#define FP_C2PWARN 18
#define FP_VACUUM 19

double get_local_dt(const coll_point_t *point); 
void flux_x(double *u, double *pos);
void flux_y(double *u, double *pos);
void flux_z(double *u, double *pos);
void recon_mp5_(double ul[NSTENCIL], double ur[NSTENCIL], double u[NSTENCIL], int *n);
void recon_func_ppm(double ul[NSTENCIL], double ur[NSTENCIL], 
                    double u[NSTENCIL], double s[NSTENCIL], int n);
void plm_reconstruction(double ul[NSTENCIL][n_variab + n_aux], 
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim], const int dir);
void mp5_reconstruction(double ul[NSTENCIL][n_variab+n_aux], 
                        double ur[NSTENCIL][n_variab+n_aux], 
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim]);
void ppm_reconstruction(double ul[NSTENCIL][n_variab + n_aux], 
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim]);
void GRHydro_MP5Reconstruct1d (const int nx,
                         const double* const restrict a,
                         double* const restrict aminus,
                         double* const restrict aplus);
void GRHydro_WENOReconstruct1d ( const int nx, 
                         const double* const restrict a,
                         double* const restrict aminus, 
                         double* const restrict aplus);
//static inline double MP5(const double am2, 
//                            const double am1, 
//                            const double a, 
//                            const double ap1, 
//                            const double ap2,
//                            const double anorm,
//                            const double mp5_eps,
//                            const double mp5_alpha
//                            );
void grhmp5_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim]);
void weno5_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim]);

void recon_mp5(double *ul, double *ur, double *u, int n);
double square_vector(double vu[3], double gd[3][3]);
double square_form(double vd[3], double gu[3][3]);
void vector_lower_index(double vd[3], double vu[3], double gd[3][3]);
void form_raise_index(double vu[3], double vd[3], double gu[3][3]);
void load_metric(double gd[3][3], double *u, const double * pos);
void load_gauge(double *alpha, double Betau[3], double *u);
int metric_vars(double gu[3][3], double *detg, double gd[3][3], double *u, 
                const double * pos);
void prim_to_con(double *u, const double *pos);
void eigen_values(double *Lp, double *Lm, double *u, const int direc, 
                  double *pos);
void PFEigenVals(double *Lp, double *Lm, double *u,
                 double gd[3][3], double gu[3][3], const int direc);
void MHDEigenVals(double *Lp, double *Lm, double *u,
                 double gd[3][3], double gu[3][3], const int direc);
double cal_cs(double rho, double p, double gamma);
void DefaultEigenVals(double *Lp, double *Lm, double *u, double gu[3][3],
                      const int direc);
void apply_floor(double *u, double *pos);
int con_to_prim(double *du, double *du_old, double *pos);
int con_to_prim_axis(coll_point_t *point, const int gen);
int MHD_Solver_1(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
void MHDWorkVars(double *fpar, double *u, double gd[3][3], 
                 double gu[3][3], double *xpt, double gamma);
int MHD_Solver_Isentropic(double *u, double *xpt, double gd[3][3], 
                          double gu[3][3], double *fpar);
int func_dis(double *f, double q, double *fpar);
int func_dis_d(double *f, double *df, double q, double *fpar);
int func_p(double *f, double q, double *fpar);
int func_p_d(double *f, double *df, double q, double *fpar);
int func_p_isentropic(double *f, double rho0, double *fpar);
int func_p_d_isentropic(double *f, double *df, double rho0, double *fpar);
int rbrac(int (*func)(double *, double , double *), double *x1, double *x2, 
           double *par, int trace);
int bisec(int (*func)(double *, double , double *),
          double *rtb, double x1, double x2, 
          double tol, double *fpar, int trace);
int FindFluidBounds(double *ql, double *fpar);
int rtsafe(int (*func)(double *, double *, double , double *), 
              double *rts, double x1, double x2, double tol, 
              double *fpar,int trace);
int zbrent(int(*func)(double *, double , double *), 
              double *rts, double x1, double x2, double tol, double *fpar);
int func_p_isentropic(double *F, double y, double *fpar);
int SolvePrimVarsIsentropic(double *xi, double *u, double *fpar);
void num_flux_hlle(coll_point_t *point, const int gen, const int dir);
void MapleDump(double *fpar);
void MathDump(double *fpar);
int func_ideal_gas(double *ff, double xx, double *fpar);
int MHD_Solver_2(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int MHD_Solver_3(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int MHD_Solver_4(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int MHD_Solver_5(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int MHD_Solver_5_vparal0(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int MHD_Solver_5_vperp0(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar);
int func4_p(double *f, double q, double *fpar);
int func4_p_d(double *f, double *df, double q, double *fpar);
int func5_p(double *f, double q, double *fpar);
int func5_p_d(double *f, double *df, double q, double *fpar);
int func5g_p(double *f, double q, double *fpar);
int func5g_p_d(double *f, double *df, double q, double *fpar);
int func5f_p(double *f, double q, double *fpar);
int func5f_p_d(double *f, double *df, double q, double *fpar);
int func5_vperp0_p(double *f, double q, double *fpar);
int func5_vperp0_p_d(double *f, double *df, double q, double *fpar);
int func5f_vparal0_p(double *f, double q, double *fpar);
int func5f_vparal0_p_d(double *f, double *df, double q, double *fpar);
int func5_vparal0_p(double *f, double q, double *fpar);
int func5_vparal0_p_d(double *f, double *df, double q, double *fpar);
void checkfornans(double *x, char *name, char *routine);
void cal_source(double *rhs, const double *u, const double *pos, const double gf);
void cal_source_v2(double *rhs, const double *u, const double *pos, const double gf);
void cal_source_v3(double *rhs, const double *u, const double *pos, const double gf);
double fmin3( double x, double y, double z);
double fmax3( double x, double y, double z);
double minmod(double x, double y );
double minmod4(double w, double x, double y, double z);


//void fill_point_closest_level(coll_point_t *point);
//void check_hlle_stencil(coll_point_t *point);
//void check_all_hlle_stencils(void);
void hlle_rhs_stage1(coll_point_t *point, const int gen);
void hlle_rhs_stage1cyl(coll_point_t *point, const int gen);
void hlle_rhs_stage2(coll_point_t *point, const int gen);
void wd_rhs_stage1(coll_point_t *point, const int gen);
void wd_rhs_stage2(coll_point_t *point, const int gen);
void recon_wv(double *wv, double *u, double *pos);
void calv(double *u, double *wv, double *pos);
void good_guess_con_to_prim(coll_point_t *point, int gen, int stage);

// initial data
void gaussian_data(const coord_t *coord, double *u);
void gaussian_data2(const coord_t *coord, double *u);
void gaussian_data_psitest(const coord_t *coord, double *u);
void riemann_problem(const coord_t *coord, double *u, const int dir);
void riemann_problem1(const coord_t *coord, double *u, const int dir);
void riemann_problem2(const coord_t *coord, double *u, const int dir);
void riemann_problem3(const coord_t *coord, double *u, const int dir);
void riemann_problem_2D(const coord_t *coord, double *u);
void kelvin_helmholtz(const coord_t *coord, double *u);
void kelvin_helmholtz_RadRez(const coord_t *coord, double *u);
void weak_field(const coord_t *coord, double *u);
void duffel_macfadyen_rt1(const coord_t *coord, double *u);
void duffel_macfadyen_rt1_mag(const coord_t *coord, double *u);
void DZBL_cyl_blast(const coord_t *coord, double *u);
void rel_rotor(const coord_t *coord, double *u);



#endif 
