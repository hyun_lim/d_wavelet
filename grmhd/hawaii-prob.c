#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "hawaii.h"
#include "hawaii-rmhd.h"

#define HLLE

double t0;
double tf;

// rmhd {{{
#ifdef RMHD
/// ---------------------------------------------------------------------------
/// The equation to solve is the relativistic MHD equations
/// ---------------------------------------------------------------------------

void problem_init(void) {
  t0 = 0.0; // Simulation starting time
  tf = 1;   // Simulation stopping time
  int i;

  double ranges[n_variab];
  initial_ranges(ranges);

  for (i=0;i<n_variab;i++) {
    eps[i] = pars.epsilon; // Error tolerance for grid refinement
    eps_scale[i] = eps[i] * ranges[i];
  }

#if n_dim == 1
    L_dim[x_dir] = pars.xmax - pars.xmin;
#elif n_dim == 2
    L_dim[x_dir] = pars.xmax - pars.xmin;
    L_dim[y_dir] = pars.ymax - pars.ymin;
#elif n_dim == 3
    L_dim[x_dir] = pars.xmax - pars.xmin;
    L_dim[y_dir] = pars.ymax - pars.ymin;
    L_dim[z_dir] = pars.zmax - pars.zmin;
#endif

  for (i=0;i<F_D;i++) {
    deriv_mapping[i] = -1;
  }
  for (i=F_D;i<=F_PSI;i++) {
    deriv_mapping[i] = i-F_D;
  }

  validate_parameters();
  time_stamp = 0; // Initialize global time stamp
}


//WARNING: This currently only works for the RMHD case with no B field and
// no initial velocity. This routine should be strengthened.
void initial_ranges(double *ranges) {

  //stationary gas initially, so W = 1
  for (int i = 0; i < n_variab; ++i) {
    ranges[i] = 1.0;
  }

#ifndef NO_SCALED_EPS
  // By default we scale epsilon by the values of D and TAU.
  //compute the initial ranges of the primary fields
  double rho_max = pars.id_gauss_floor + pars.id_amp;
  double rho_min = pars.id_gauss_floor;
  double P_max = pars.id_kappa * pow(rho_max, pars.gamma);
  double P_min = pars.id_kappa * pow(rho_min, pars.gamma);
  // [ewh] commenting these out as they don't seem to get used and their presence only seems to throw warnings at compile:
  //double inte_max = P_max / (rho_max * (pars.gamma - 1.0));
  //double inte_min = P_min / (rho_min * (pars.gamma - 1.0));
  //double he_max = rho_max * (1.0 + inte_max) + P_max;
  //double he_min = rho_min * (1.0 + inte_min) + P_min;
  //double D_max = rho_max;
  //double D_min = rho_min;
  //double tau_max = he_max - P_max - rho_max;
  //double tau_min = he_min - P_min - rho_min;

  ranges[V_RHO] = rho_max - rho_min;
  ranges[V_P] = P_max - P_min;

#ifdef CYL
  ranges[U_PSI] =   (rho_max * rho_max + P_max * P_max)
                  - (rho_min * rho_min + P_min * P_min);
#endif

#endif

}

void initial_condition(const coord_t *coord, double *u) {
  if (pars.id_type == 1) {
    int dir = pars.id_indep_coord;
    riemann_problem(coord, u, dir);
  }
  else if (pars.id_type == 2) {
    gaussian_data2(coord, u);
  }
  else if (pars.id_type == 3) {
    duffel_macfadyen_rt1(coord, u);
  }
  else if (pars.id_type == 4) {
    DZBL_cyl_blast(coord, u);
  }
  else if (pars.id_type == 5) {
    rel_rotor(coord, u);
  }
  else if (pars.id_type == 6) {
    int dir = pars.id_indep_coord;
    riemann_problem3(coord, u, dir);
  }
  else if (pars.id_type == 7) {
    riemann_problem_2D(coord, u);
  }
  else if (pars.id_type == 8) {
    kelvin_helmholtz(coord, u);
  }
  else if (pars.id_type == 9) {
    weak_field(coord, u);
  }
  else if (pars.id_type == 10) {
    kelvin_helmholtz_RadRez(coord, u);
  }
  else if (pars.id_type == 11) {
    duffel_macfadyen_rt1_mag(coord, u);
  }
  else if (pars.id_type == 12) {
    gaussian_data_psitest(coord, u);
  }
  else {
    printf("initial_condition: unknown id_type = %d\n",pars.id_type);
    exit(-2);
  }

}


void compute_rhs(const double t, const int gen) {
#ifdef HLLE
  //This stage will guarantee that the nonessential points in any
  // HLLE stencil are up to date
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    hlle_rhs_stage1(point, gen);
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        hlle_rhs_stage1(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }

  //This stage performs prim_to_con for any up to date nonessential point
  FOR (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] == nonessential
                      && ptr->point->time_stamp == time_stamp) {
        prim_to_con(ptr->point->u[gen], ptr->point->coords.pos);
      }
      ptr = ptr->next;
    }
  }

#ifdef CYL
  int skip_at_max = skip_at_level(max_level);
  index_t c2pa_index;
  c2pa_index.idx[0] = 0;

  FOR (int idx = 0; idx <= max_index[1]; idx += skip_at_max) {
    c2pa_index.idx[1] = idx;
    coll_point_t *axis_point = get_coll_point(&c2pa_index);
    if (axis_point != NULL &&
                axis_point->status[CURRENT_STATUS] > nonessential) {
      con_to_prim_axis(axis_point, gen);
    }
  }
#endif //-- end ifdef CYL

  FOR (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    hlle_rhs_stage2(point, gen);
  }
  FOR (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->hash_table[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        hlle_rhs_stage2(ptr->point, gen);
      }
      ptr = ptr->next;
    }
  }
#endif // HLLE
}

#endif //RMHD
// }}}
