#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <sys/time.h>
#include "hawaii.h"
#include "restart.h"
#include "stats.h"
#include "conservative.h"

#ifdef TRAP_FPE
#include <fenv.h>
#endif

#if defined(RMHD)
Pars pars;
#endif


//#define POORMANSOUTPUT

static inline double getticks(void) {
  struct timeval tv;
  gettimeofday(&tv, NULL);
  return (double) (tv.tv_sec * 1e6 + tv.tv_usec);
}

static inline double elapsed(double t1, double t0) {
  return (double) (t1 - t0) / 1.0e6;
}

int primary_mask[n_variab + n_aux];
int primitive_mask[n_variab + n_aux];
int auxiliary_mask[n_variab + n_aux];
int allvars_mask[n_variab + n_aux];
int conservative_mask[n_variab + n_aux];

void setup_default_masks(void) {
  for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
    if (ivar < n_variab) {
      primary_mask[ivar] = 1;
    } else {
      primary_mask[ivar] = 0;
    }
    if (ivar >= n_prims) {
      auxiliary_mask[ivar] = 1;
    } else {
      auxiliary_mask[ivar] = 0;
    }
    if (ivar < n_prims) {
      primitive_mask[ivar] = 1;
    }
    if (ivar >= n_prims && ivar < n_prims + n_cons) {
      conservative_mask[ivar] = 1;
    } else {
      conservative_mask[ivar] = 0;
    }
    allvars_mask[ivar] = 1;
  }
}


#ifdef CILK
tatas_lock_t *_table_locks;
#endif


int main(int argc, char *argv[]) {
  double startup = getticks();

#if defined(CYL) && defined(PERIODIC)
  assert(0 && "CYL and PERIODIC are incompatible. Please choose only one.")
#endif

#ifdef TRAP_FPE
  feenableexcept(FE_DIVBYZERO | FE_INVALID | FE_OVERFLOW);
#endif

#ifdef RNPL
  // read parameter file
  char pfile[128];
  if ( argc < 2 ) {
    printf(" Usage: %s <parameter file> [restart_flag]\n",argv[0]);
    exit(-1);
  }
  strcpy(pfile, argv[1]);
  if (read_param_file(pfile) != 1) {
    printf("Error reading parameters. Die here.\n");
    exit(-1);
  }
#else
  default_params();
#endif

  //Detect if we want a restart
  // Currently, anything after the parameter file will trigger a restart
  int restart_flag = (argc > 2);

  setup_default_masks();

  //the time statistics we are collecting
  time_stats_t timing;
  memset(&timing, 0, sizeof(timing));
  //This will be reset in the case of a restart; see below
  double previous_total = sum_time_stats(&timing);
  if (!open_stats_file(pars.stats_output_frequency, restart_flag)) {
    return -1;
  }
  if (!open_cons_file(pars.cons_test_frequency, restart_flag)) {
    return -1;
  }

  // Simulation time
  double t = t0; // Current simulation real time

  // Initialize problem parameters
  problem_init();

  // Generate adptive grid from initial profile
  storage_init();

  if (restart_flag) {
    int insnap;
    if (read_restart_file(&t, &time_stamp, &timing, &insnap)) {
      return -1;
    }
    previous_total = sum_time_stats(&timing);
    set_next_file_number(insnap);
    complete_wavelet_stencils();
  } else {
    //normal initialization
    create_full_grids();
    create_adap_grids();
  }
  complete_hlle_stencils();
#ifdef CYL
  cyl_axis_complete_c2p_axis();
#endif

  printf("Max level at start of run is %d\n", max_level);




#ifdef CILK
  _table_locks = malloc(sizeof(tatas_lock_t) * CILK_LOCK_TBL_SIZE);
  assert(_table_locks != NULL);
  for (int itab = 0; itab < CILK_LOCK_TBL_SIZE; ++itab) {
    _table_locks[itab].lock = 0;
  }
#endif

#if defined(RNPL) || defined(SILO)
  if ( !restart_flag && pars.output_frequency != 0 ) {
    grid_sdf_output(time_stamp,t,0);
  }
#endif

  timing.t_startup += elapsed(getticks(), startup);
  double t_total = sum_time_stats(&timing) - previous_total;
  double t_last_restart = 0.0;

  //The main simulation loop
  while (time_stamp/n_gen < pars.max_iter && t < pars.tf
                  /*&& t_total < pars.max_runtime*/) {
    if (t_total > pars.max_runtime) {
      fprintf(stdout, "Reached maximum runtime: %lg (limit is %lg)\n",
               t_total, pars.max_runtime);
      break;
    }
    double dt = get_global_dt();
    dt = fmin(dt, pars.tf - t);

    //Output to terminal
    double s_misc = getticks();
    if ( pars.print_frequency != 0 ) {
      if (time_stamp/n_gen % pars.print_frequency == 0 ) {
        int numpoints = count_points(neighboring, time_stamp);
        printf("step %d: t_next = %14.7e, dt = %10.3e, npts = %d\n",
                time_stamp / n_gen, t + dt, dt, numpoints);
      }
    }

    //Output statistics to 'run_statistics.txt'
    if (pars.stats_output_frequency &&
                  (time_stamp / n_gen % pars.stats_output_frequency == 0)) {
      int numpoints = count_points(neighboring, time_stamp);
      int numpoints_noness = count_points(nonessential, time_stamp);
      write_to_stats(t, time_stamp, &timing, numpoints,
                      numpoints_noness - numpoints);
    }

    //Output conservation testing to 'run_conservation.txt'
    if (pars.cons_test_frequency &&
                  (time_stamp / n_gen % pars.cons_test_frequency == 0)) {
      test_fluid_conservation(t, time_stamp);
    }

    timing.t_io += elapsed(getticks(), s_misc);

    double s1 = getticks();
    apply_time_integrator(t, dt);
    timing.t_integrate += elapsed(getticks(), s1);
    t += dt;

    double s2 = getticks();
    adjust_grids();
    timing.t_adjust += elapsed(getticks(), s2);
    double s4 = getticks();
    if (time_stamp % (n_gen * pars.prune_frequency) == 0)
      prune_grids();
    timing.t_prune += elapsed(getticks(),s4);

    double s3 = getticks();
#if defined(RNPL) || defined(SILO)
    if ( pars.output_frequency != 0 ) {
      if (time_stamp % (n_gen * pars.output_frequency) == 0 ) {
        grid_sdf_output(time_stamp,t,0);
      }
    }
#endif

    t_total = sum_time_stats(&timing) - previous_total;
    if (t_total - t_last_restart > pars.time_between_restarts) {
      fprintf(stdout, "Making restart: %lg %lg %lg\n",
                        t_total, previous_total, pars.time_between_restarts);
      t_last_restart = t_total;
      assert(write_restart_file(t, time_stamp,
                                &timing, get_next_file_number()) == 0);
    }
    timing.t_io += elapsed(getticks(), s3);
  }

  //Write restart file right at the end no matter what
  write_restart_file(t, time_stamp, &timing, get_next_file_number());

  printf(" Time integrator loop: %f, Adjust grid: %f  "
         "\n startup: %f output: %f prune grids: %f seconds\n",
         timing.t_integrate, timing.t_adjust,
         timing.t_startup, timing.t_io, timing.t_prune);

  close_stats_file();
  close_cons_file();
  storage_cleanup();

  return 0;
}
