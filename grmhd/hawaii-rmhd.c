#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"
#include "hawaii-rmhd.h"


#define DIE_ON_C2P_ERRORS 1

#ifdef RMHD

// get_local_dt {{{
double get_local_dt (const coll_point_t *point) {
  double dt = 1.e34;
  for (dir_t dir = x_dir; dir < n_dim; dir++) {
    int closest_level = get_closest_level(point, dir);
    double dx0 = L_dim[dir] / max_index[dir];
    int64_t h = (int64_t)1 << (JJ - closest_level);
    double dx = dx0 * h;
    dt = fmin(dt, pars.cfl * dx);
  }

  if (fabs(dt) > 100.0) {
    fprintf(stdout, ".");
  }

  return dt;
}
// }}}

// fluxes {{{
/*-------------------------------------------------------------------------
 *
 *  rhs_cal_flux: Calculate flux functions
 *
 *-------------------------------------------------------------------------*/
void flux_x(double *u, double *pos)
{
  double D, Tau, P;
  double Su[3], Bu[3], vu[3], vd[3], Sd[3], Bd[3];
  double gd[3][3], gu[3][3];
  double vsq, Wsq, Bv;
  double Alpha, Beta[3], detg, sdetg;
  double psi;

  load_gauge(&Alpha, Beta, u);
  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("flux_x: Error calculating metric determinant. Die here.\n");
    exit(-1);
  }

  sdetg = sqrt(detg);

  if (pars.densitized_vars == 1) 
  {
    double isdetg = 1.0 / sdetg;
    D     = isdetg * u[U_D];
    Sd[0] = isdetg * u[U_SX];
    Sd[1] = isdetg * u[U_SY];
    Sd[2] = isdetg * u[U_SZ];
    Tau   = isdetg * u[U_TAU];
    Bu[0] = isdetg * u[U_BX];
    Bu[1] = isdetg * u[U_BY];
    Bu[2] = isdetg * u[U_BZ];
    psi   = isdetg * u[U_PSI];
  }
  else 
  {
    D     = u[U_D];
    Sd[0] = u[U_SX];
    Sd[1] = u[U_SY];
    Sd[2] = u[U_SZ];
    Tau   = u[U_TAU];
    Bu[0] = u[U_BX];
    Bu[1] = u[U_BY];
    Bu[2] = u[U_BZ];
    psi   = u[U_PSI];
  }

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  P     = u[V_P];

  vsq = square_vector(vu, gd);
  vector_lower_index(vd, vu, gd);
  form_raise_index(Su, Sd, gu);
  vector_lower_index(Bd, Bu, gd);

  Wsq  = 1.0/(1.0 - vsq);

  Bv = Bd[0]*vu[0] + Bd[1]*vu[1] + Bd[2]*vu[2];

  u[F_D]   = Alpha*sdetg*D*(vu[0] - Beta[0]/Alpha);
  u[F_SX]  = Alpha*sdetg*(Sd[0]*vu[0] + P
              - 0.5/Wsq*(-Bu[1]*Bd[1] + Bu[0]*Bd[0] - Bu[2]*Bd[2])
              - 0.5*Bv* (-Bu[1]*vd[1] + Bu[0]*vd[0] - Bu[2]*vd[2])
                      - Beta[0]/Alpha*Sd[0]);
  u[F_SY]  = Alpha*sdetg*(Sd[1]*vu[0] - Bu[0]*Bd[1]/Wsq - Bv*Bu[0]*vd[1]
                      - Beta[0]/Alpha*Sd[1]);
  u[F_SZ]  = Alpha*sdetg*(Sd[2]*vu[0] - Bu[0]*Bd[2]/Wsq - Bv*Bu[0]*vd[2]
                      - Beta[0]/Alpha*Sd[2]);
  u[F_TAU] = Alpha*sdetg*(Su[0] - D*vu[0]) - sdetg*Beta[0]*Tau;

  //u[F_BX]  = 0.0;
  //u[F_BX]  = Alpha * sdetg * psi * gu[0][0];
  u[F_BX]  = 0.0 ;

  u[F_BY]  = Alpha*sdetg
            //* (Bu[1]*(vu[0] - Beta[0]/Alpha) - Bu[0]*(vu[1] - Beta[1]/Alpha) + psi * gu[0][1] );
            * (Bu[1]*(vu[0] - Beta[0]/Alpha) - Bu[0]*(vu[1] - Beta[1]/Alpha) );
  u[F_BZ]  = Alpha*sdetg
            //* (Bu[2]*(vu[0] - Beta[0]/Alpha) - Bu[0]*(vu[2] - Beta[2]/Alpha) + psi * gu[0][2] );
            * (Bu[2]*(vu[0] - Beta[0]/Alpha) - Bu[0]*(vu[2] - Beta[2]/Alpha) );
  //u[F_PSI] = 0.0;
  //u[F_PSI] = sdetg * ( Alpha * Bu[0] - Beta[0] * psi ) ;
  u[F_PSI] = psi ;

}

/*-------------------------------------------------------------------------
 *
 *  flux_y
 *
 *-------------------------------------------------------------------------*/
void flux_y(double *u, double *pos)
{
  double D, Tau, P;
  double Su[3], Bu[3], vu[3], vd[3], Sd[3], Bd[3];
  double gd[3][3], gu[3][3];
  double vsq, Wsq, Bv;
  double Alpha, Beta[3], detg, sdetg;
  double psi;

  load_gauge(&Alpha, Beta, u);
  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("flux_y: Error calculating metric determinant. Die here.\n");
    exit(-1);
  }

  sdetg = sqrt(detg);

  if (pars.densitized_vars == 1) {
    double isdetg = 1.0 / sdetg;
    D     = isdetg * u[U_D];
    Sd[0] = isdetg * u[U_SX];
    Sd[1] = isdetg * u[U_SY];
    Sd[2] = isdetg * u[U_SZ];
    Tau   = isdetg * u[U_TAU];
    Bu[0] = isdetg * u[U_BX];
    Bu[1] = isdetg * u[U_BY];
    Bu[2] = isdetg * u[U_BZ];
    psi   = isdetg * u[U_PSI];
  }
  else {
    D     = u[U_D];
    Sd[0] = u[U_SX];
    Sd[1] = u[U_SY];
    Sd[2] = u[U_SZ];
    Tau   = u[U_TAU];
    Bu[0] = u[U_BX];
    Bu[1] = u[U_BY];
    Bu[2] = u[U_BZ];
    psi   = u[U_PSI];
  }

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  P     = u[V_P];

  vsq = square_vector(vu, gd);
  vector_lower_index(vd, vu, gd);
  form_raise_index(Su, Sd, gu);
  vector_lower_index(Bd, Bu, gd);

  Wsq  = 1.0/(1.0 - vsq);

  Bv = Bd[0]*vu[0] + Bd[1]*vu[1] + Bd[2]*vu[2];

  u[F_D]   = Alpha*sdetg*D*(vu[1] - Beta[1]/Alpha);
  u[F_SX]  = Alpha*sdetg*(Sd[0]*vu[1] - Bu[1]*Bd[0]/Wsq - Bv*Bu[1]*vd[0]
                           - Beta[1]/Alpha*Sd[0]);
  u[F_SY]  = Alpha*sdetg*(Sd[1]*vu[1] + P
              - 0.5/Wsq*(-Bu[0]*Bd[0] + Bu[1]*Bd[1] - Bu[2]*Bd[2])
              - 0.5*Bv* (-Bu[0]*vd[0] + Bu[1]*vd[1] - Bu[2]*vd[2])
                           - Beta[1]/Alpha*Sd[1]);
  u[F_SZ]  = Alpha*sdetg*(Sd[2]*vu[1] - Bu[1]*Bd[2]/Wsq - Bv*Bu[1]*vd[2]
                           - Beta[1]/Alpha*Sd[2]);
  u[F_TAU] = Alpha*sdetg*(Su[1] - D*vu[1]) - sdetg*Beta[1]*Tau;

  u[F_BX]  = Alpha*sdetg
            //* (Bu[0]*(vu[1] - Beta[1]/Alpha) - Bu[1]*(vu[0] - Beta[0]/Alpha) + psi * gu[0][1] );
            * (Bu[0]*(vu[1] - Beta[1]/Alpha) - Bu[1]*(vu[0] - Beta[0]/Alpha) );
  //u[F_BY]  = 0.0;
  //u[F_BY]  = Alpha * sdetg * psi * gu[1][1];
  u[F_BY]  = 0.0 ; 
  u[F_BZ]  = Alpha*sdetg
            //* (Bu[2]*(vu[1] - Beta[1]/Alpha) - Bu[1]*(vu[2] - Beta[2]/Alpha) + psi * gu[1][2] );
            * (Bu[2]*(vu[1] - Beta[1]/Alpha) - Bu[1]*(vu[2] - Beta[2]/Alpha) );
  u[F_PSI] = 0.0 ;
  //u[F_PSI] = sdetg * ( Alpha * Bu[1] - Beta[1] * psi ) ;

}

/*-------------------------------------------------------------------------
 *
 *  flux_z
 *
 *-------------------------------------------------------------------------*/
void flux_z(double *u, double *pos)
{
  double D, Tau, P;
  double Su[3], Bu[3], vu[3], vd[3], Sd[3], Bd[3];
  double gd[3][3], gu[3][3];
  double vsq, Wsq, Bv;
  double Alpha, Beta[3], detg, sdetg;
  double psi;

  load_gauge(&Alpha, Beta, u);
  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("rhs_cal_flux: Error calculating metric determinant. Die here.\n");
    exit(-1);
  }

  sdetg = sqrt(detg);

  if (pars.densitized_vars == 1) {
    double isdetg = 1.0 / sdetg;
    D     = isdetg * u[U_D];
    Sd[0] = isdetg * u[U_SX];
    Sd[1] = isdetg * u[U_SY];
    Sd[2] = isdetg * u[U_SZ];
    Tau   = isdetg * u[U_TAU];
    Bu[0] = isdetg * u[U_BX];
    Bu[1] = isdetg * u[U_BY];
    Bu[2] = isdetg * u[U_BZ];
    psi   = isdetg * u[U_PSI];
  }
  else {
    D     = u[U_D];
    Sd[0] = u[U_SX];
    Sd[1] = u[U_SY];
    Sd[2] = u[U_SZ];
    Tau   = u[U_TAU];
    Bu[0] = u[U_BX];
    Bu[1] = u[U_BY];
    Bu[2] = u[U_BZ];
    psi   = u[U_PSI];
  }

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  P     = u[V_P];

  vsq = square_vector(vu, gd);
  vector_lower_index(vd, vu, gd);
  form_raise_index(Su, Sd, gu);
  vector_lower_index(Bd, Bu, gd);

  Wsq  = 1.0/(1.0 - vsq);

  Bv = Bd[0]*vu[0] + Bd[1]*vu[1] + Bd[2]*vu[2];

  u[F_D]   = Alpha*sdetg*D*(vu[2] - Beta[2]/Alpha);
  u[F_SX]  = Alpha*sdetg*(Sd[0]*vu[2] - Bu[2]*Bd[0]/Wsq - Bv*Bu[2]*vd[0]
                 - Beta[2]/Alpha*Sd[0]);
  u[F_SY]  = Alpha*sdetg*(Sd[1]*vu[2] - Bu[2]*Bd[1]/Wsq - Bv*Bu[2]*vd[1]
                 - Beta[2]/Alpha*Sd[1]);
  u[F_SZ]  = Alpha*sdetg*(Sd[2]*vu[2] + P
              - 0.5/Wsq*(-Bu[0]*Bd[0] - Bu[1]*Bd[1] + Bu[2]*Bd[2])
              - 0.5*Bv* (-Bu[0]*vd[0] - Bu[1]*vd[1] + Bu[2]*vd[2])
                  - Beta[2]/Alpha*Sd[2]);
  u[F_TAU] = Alpha*sdetg*(Su[2] - D*vu[2]) - sdetg*Beta[2]*Tau;

  u[F_BX]  = Alpha*sdetg
            //* (Bu[0]*(vu[2] - Beta[2]/Alpha) - Bu[2]*(vu[0] - Beta[0]/Alpha) + psi * gu[0][2] );
            * (Bu[0]*(vu[2] - Beta[2]/Alpha) - Bu[2]*(vu[0] - Beta[0]/Alpha) );
  u[F_BY]  =  Alpha*sdetg
            //* (Bu[1]*(vu[2] - Beta[2]/Alpha) - Bu[2]*(vu[1] - Beta[1]/Alpha) + psi * gu[1][2] );
            * (Bu[1]*(vu[2] - Beta[2]/Alpha) - Bu[2]*(vu[1] - Beta[1]/Alpha) );
  //u[F_BZ]  = 0.0;
  //u[F_BZ]  = Alpha * sdetg * psi * gu[2][2] ;
  u[F_BZ]  = 0.0 ;
  //u[F_PSI] = 0.0;
  //u[F_PSI] = sdetg * ( Alpha * Bu[2] - Beta[2] * psi ) ;
  u[F_PSI] = 0.0 ;

}
// }}}

// HLLE numerical flux {{{
/*-------------------------------------------------------------------------
 *
 *  This is my first attempt at an HLLE update. This is not finished
 *
 *-------------------------------------------------------------------------*/
void num_flux_hlle(coll_point_t *point, const int gen, const int dir)
{
  index_t index = point->index;
  double *u[NSTENCIL], *pos[NSTENCIL];
  coll_point_t *stcl[NSTENCIL];


#ifdef CYL
  /* if on axis, simply set rhs = 0 and return */
  if (index.idx[0] == 0) {
    for (int m = 0; m < n_variab; m++) {
      point->rhs[gen][m] = 0.0;
    }
    return;
  }
#endif

  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = index.idx[i];
  }

  int closest_level = get_closest_level(point, dir);
  assert( closest_level >= 1 );

  // get the grid spacing
  double dx;
  dx = L_dim[dir] / ns[dir] / (1 << closest_level);
  int h = 1 << (JJ - closest_level);

  for (int ll = 0; ll < NSTENCIL; ll++) {
    if ( ll == STENCIL_CENTER ) {
      u[ll] = point->u[gen];
      pos[ll] = point->coords.pos;
      stcl[ll] = point;
    } else {
      coll_point_t *cpm = NULL;
      id.idx[dir] = index.idx[dir] + (ll - STENCIL_CENTER) * h;
#ifdef PERIODIC
      id = map_index_into_grid(&id);
#endif
      if (check_index(&id)) {
        cpm = get_coll_point(&id);
        //Stage 1 should have assured that all points exist, so...
        assert(cpm != NULL);

#ifdef CYL
        //NOTE: This will repeat work on occasion
        if (cpm->status[CURRENT_STATUS] == nonessential &&
                                        cpm->index.idx[0] == 0) {
          for (int creset = 0; creset < n_variab; ++creset) {
            cpm->u[gen][creset] = 0.0;
          }
          //call con_to_prim_axis on the point
          con_to_prim_axis(cpm, gen);
        }
#endif

        u[ll] = cpm->u[gen];
        stcl[ll] = cpm;
        //TODO, this should actually be the closest position to the center;
        // this will be corrected below
        pos[ll] = cpm->coords.pos;
      } else {
        u[ll] = NULL;
        pos[ll] = NULL;
        stcl[ll] = NULL;
      }
    }
  }

  int nn = 0; // number of null points in initial stencil
  int bi;
  for (int ll = 0; ll < NSTENCIL; ll++) {
    if (pos[ll] == NULL) {
      nn++;
      bi = ll-STENCIL_CENTER;
    }
  }
#ifdef PERIODIC
  //The periodic stencil should never be missing anything
  assert(nn == 0);
#endif

  double pos1[n_dim], pos2[n_dim], pos3[n_dim];
  if (nn > 0) {
   /* set initial coords the coords of the center point. correct later. */
    for (int ll = 0; ll < n_dim; ll++) {
      pos1[ll] = pos[3][ll];
      pos2[ll] = pos[3][ll];
      pos3[ll] = pos[3][ll];
    }
    if (bi < 0) {
     /* left boundary */
      if (pos[2] == NULL) {
        pos[2] = pos1;
        pos[1] = pos2;
        pos[0] = pos3;
        pos[2][dir] = pos[3][dir] - dx;
        pos[1][dir] = pos[3][dir] - 2.0*dx;
        pos[0][dir] = pos[3][dir] - 3.0*dx;
      }
      else if (pos[1] ==NULL) {
        pos[1] = pos2;
        pos[0] = pos3;
        pos[1][dir] = pos[2][dir] - dx;
        pos[0][dir] = pos[2][dir] - 2.0*dx;
      }
      else if (pos[0] == NULL) {
        pos[0] = pos3;
        pos[0][dir] = pos[1][dir] - dx;
      }
    }
    else {
     /* right boundary */
      if (pos[4] ==NULL) {
        pos[4] = pos1;
        pos[5] = pos2;
        pos[6] = pos3;
        pos[4][dir] = pos[3][dir] + dx;
        pos[5][dir] = pos[3][dir] + 2.0*dx;
        pos[6][dir] = pos[3][dir] + 3.0*dx;
      }
      else if (pos[5] == NULL) {
        pos[5] = pos1;
        pos[6] = pos2;
        pos[5][dir] = pos[4][dir] + dx;
        pos[6][dir] = pos[4][dir] + 2.0*dx;
      }
      else if (pos[6] == NULL) {
        pos[6] = pos1;
        pos[6][dir] = pos[5][dir] + dx;
      }
    }
  }
#ifdef PERIODIC
  //NOTE: STENCIL_CENTER is the index of the center, and it is also the
  // number of points on either side.
  double wrappos[STENCIL_CENTER][n_dim];
  int wrapcount = 0;
  for (int ll = 0; ll < STENCIL_CENTER; ++ll) {
    if (pos[ll][dir] > pos[STENCIL_CENTER][dir]) {
      //If we get here, the points to the left of the center have a position
      // to the right; i.e., the point has been wrapped around
      for (int i = 0; i < n_dim; ++i) {
        wrappos[wrapcount][i] = pos[ll][i];
      }
      wrappos[wrapcount][dir] -= L_dim[dir];
      pos[ll] = wrappos[wrapcount];
      ++wrapcount;
    }
  }
  for (int ll = STENCIL_CENTER + 1; ll < NSTENCIL; ++ll) {
    if (pos[ll][dir] < pos[STENCIL_CENTER][dir]) {
      assert(wrapcount < STENCIL_CENTER);
      //If we get here, the points to the right of the center have a position
      // to the left; i.e., the point has been wrapped around
      for (int i = 0; i < n_dim; ++i) {
        wrappos[wrapcount][i] = pos[ll][i];
      }
      wrappos[wrapcount][dir] += L_dim[dir];
      pos[ll] = wrappos[wrapcount];
      ++wrapcount;
    }
  }
#endif

 /* get metric info for the current point. This is needed to calculate
 *   * sources in non-Cartesian geometries with undensitized vars.
 *     */
  double Alpha, Beta[3], gd[3][3], gu[3][3], detg;
  load_gauge(&Alpha, Beta, u[2]);
  load_metric(gd, u[2], pos[2]);
  int rc = metric_vars(gu, &detg, gd, u[2], pos[2]);
  if (rc != 1) {
    printf("num_flux_hlle: Error calculating metric determinant. Die here.\n");
    exit(-1);
  }
  double sdetg = sqrt(detg);

/* storage for boundary conditions */
  double ubx0[n_variab + n_aux];
  double ubx1[n_variab + n_aux];
  double ubx2[n_variab + n_aux];
  double *ub[3];
  ub[0] = ubx0;
  ub[1] = ubx1;
  ub[2] = ubx2;

 /* LEFT boundary conditions */
  if ( u[0] == NULL || u[1] == NULL || u[2] == NULL) {
    int nbpts, ib;
    if (u[2] == NULL) {
      nbpts = 3;
      ib = 3;
    }
    else if (u[1] == NULL) {
      nbpts = 2;
      ib = 2;
    }
    else {
      nbpts = 1;
      ib = 1;
    }
//  printf("bc = %d, %d\n",pars.bcs[2*dir],bc_wall);
    if (pars.bcs[2*dir] == bc_wall) {
     /* left boundary is a wall */
      for (int pp = 0; pp < nbpts; pp++) {
        for (int m = 0; m < n_variab + n_aux; m++) {
          ub[pp][m] = u[ib+pp+1][m];
        }
/*      printf("nbpts=%d, ib=%d, setting (%d) = (%d)\n",
 *                                       nbpts,ib,ib-pp-1,ib+pp+1); */
        ub[pp][U_SX+dir] *= -1.0;
        ub[pp][V_VX+dir] *= -1.0;
        u[ib-pp-1] = ub[pp];
      }
    }
#ifdef CYL
    else if (pars.bcs[2*dir] == bc_axis) {
     /* left boundary is an axis */
      assert(u[2] != NULL); /* just an idiot check */
      for (int pp = 0; pp < nbpts; pp++) {
        for (int m = 0; m < n_variab + n_aux; m++) {
          ub[pp][m] = u[ib+pp+1][m];
        }
/*      printf("nbpts=%d, ib=%d, setting (%d) = (%d)\n",
 *                                       nbpts,ib,ib-pp-1,ib+pp+1); */
        ub[pp][U_SX] *= -1.0;
        ub[pp][V_VX] *= -1.0;
        u[ib-pp-1] = ub[pp];
      }
    }
#endif
    else {
     /* left boundary is outflow */
      u[0] = u[ib];
      if (u[1] == NULL) u[1] = u[ib];
      if (u[2] == NULL) u[2] = u[ib];
    }
  }

 /* RIGHT boundary conditions */
  if ( u[4] == NULL || u[5] == NULL || u[6] == NULL) {
    int ib, nbpts;
    if (u[4] == NULL) {
      nbpts = 3;
      ib = 3;
    }
    else if (u[5] == NULL) {
      nbpts = 2;
      ib = 4;
    }
    else {
      nbpts = 1;
      ib = 5;
    }
    if (pars.bcs[2*dir+1] == bc_wall) {
     /* right boundary is a wall */
      for (int pp = 0; pp < nbpts; pp++) {
        for (int m = 0; m < n_variab + n_aux; m++) {
          ub[pp][m] = u[ib-pp-1][m];
        }
        ub[pp][U_SX+dir] *= -1.0;
        ub[pp][V_VX+dir] *= -1.0;
        u[ib+pp+1] = ub[pp];
      }
    }
    else {
     /* right boundary is outflow */
      u[6] = u[ib];
      if (u[5] == NULL) u[5] = u[ib];
      if (u[4] == NULL) u[4] = u[ib];
    }
  }

  // FIXME -----------------------------------------------------------
  for (int i = 0; i < NSTENCIL; i++) {
    if (u[i][V_RHO] < 0.0) {
      printf("TEST HLLE: rho < 0. i=%d, rho=%g, status=%d\n",i,u[i][V_RHO],stcl[i]->status[CURRENT_STATUS]);
    }
    if (u[i][V_P] < 0.0) {
      printf("TEST HLLE: P < 0. i=%d, P=%g, status=%d\n",i,u[i][V_P],stcl[i]->status[CURRENT_STATUS]);
    }
  }
  // end FIXME -----------------------------------------------------------

  double ul[NSTENCIL][n_variab + n_aux];
  double ur[NSTENCIL][n_variab + n_aux];
  double f[NSTENCIL][n_variab];
  double pos_h[NSTENCIL][n_dim];

  // coordinates at the midpoints, i+1/2.
  for (int i = 1; i < NSTENCIL-1; i++) {
    for (int m = 0; m < n_dim; m++) {
      pos_h[i][m] = 0.5*(pos[i-1][m] + pos[i][m]);
    }
  }


  for (int i = 0; i < NSTENCIL; i++)
  {
    if ( u[i][V_P] < 0.0 )
    { printf("-0.5neg again u[i=%3d][V_P] = %15.10e \n", i,u[i][V_P]);
      printf("         pos_h[i-1][0] = %15.10e \n", pos_h[i-1][0] );
      printf("         pos_h[i  ][0] = %15.10e \n", pos_h[i  ][0] );
      printf("         pos_h[i+1][0] = %15.10e \n", pos_h[i+1][0] );
    }
  }




  if (pars.reconstruction == recon_type_mp5) {
    mp5_reconstruction(ul, ur, u, pos, pos_h);
  }
  else if (pars.reconstruction == recon_type_grhmp5) {
    grhmp5_reconstruction(ul, ur, u, pos, pos_h);
  }
  else if (pars.reconstruction == recon_type_weno5) {
    weno5_reconstruction(ul, ur, u, pos, pos_h);
  }
  else if (pars.reconstruction == recon_type_ppm) {
   /* piece-wise parabolic reconstruction */
    ppm_reconstruction(ul, ur, u, pos, pos_h);
  }
  else {
   /* piece-wise linear reconstruction with minmod limiter */
    plm_reconstruction(ul, ur, u, pos, pos_h, dir);
  }

  // Apply floor to the primitive variables -- applied to ul and ur
  for (int i = 1; i < NSTENCIL-1; i++) {

    if (ul[i][V_RHO] < 0.0 )
    {printf("7negative prim vars ul[i][V_RHO] = %15.10e, r=%g\n",ul[i][V_RHO],pos[i][0]);}
    if (ul[i][V_P] < 0.0 )
    {printf("7negative prim vars ul[i][V_P] = %15.10e, r=%g \n",ul[i][V_P],pos[i][0]);}
    if (ul[i+1][V_RHO] < 0.0 )
    {printf("7 negative prim vars ul[i+1][V_RHO] =%15.10e, r=%g\n",ul[i+1][V_RHO],pos[i+1][0]);}
    if (ul[i+1][V_P] < 0.0 )
    {printf("7negative prim vars ul[i+1][V_P] = %15.10e, r=%g \n",ul[i+1][V_P],pos[i+1][0]);}


    if (ul[i+1][V_RHO] < pars.vacuum ) ul[i+1][V_RHO] = pars.vacuum;
    if (ul[i+1][V_P] < pars.vacuum) ul[i+1][V_P] = pars.vacuum;
    if (ur[i][V_RHO] < pars.vacuum ) ur[i][V_RHO] = pars.vacuum;
    if (ur[i][V_P] < pars.vacuum) ur[i][V_P] = pars.vacuum;

  }

  // Call primtocon for reconstructed conserved vars
  for (int i = 1; i < NSTENCIL-1; i++) {
    prim_to_con(ul[i+1], pos_h[i+1]);
    prim_to_con(ur[i], pos_h[i]);
  }

  // HLLE flux
  for (int i = STENCIL_CENTER; i < STENCIL_CENTER+2; i++) {
    if (dir == x_dir) {
      flux_x(ul[i], pos_h[i]);
      flux_x(ur[i], pos_h[i]);
    }
    else if (dir == y_dir) {
      flux_y(ul[i], pos_h[i]);
      flux_y(ur[i], pos_h[i]);
    }
    else {
      flux_z(ul[i], pos_h[i]);
      flux_z(ur[i], pos_h[i]);
    }
    double Lpl, Lml, Lpr, Lmr;
    eigen_values(&Lpl, &Lml, ul[i], dir, pos_h[i]);
    eigen_values(&Lpr, &Lmr, ur[i], dir, pos_h[i]);
    double bp = fmax(fmax(0.0, Lpl), Lpr);
    double bm = fmin(fmin(0.0, Lml), Lmr);
    for (int m = 0; m < n_variab; m++) {
      f[i][m] = (bp*ul[i][m+F_D] - bm*ur[i][m+F_D]
                     + bp*bm*(ur[i][U_D+m] - ul[i][U_D+m])) / (bp - bm);
    }
  }

#if 0
  if (point->index.idx[0] == 45) {
    printf("...before\n");
    printf(">>>  x = %g\n",pos[3][0]);
    printf(">>>  u = %g, %g, %g, %g, %g\n",
           u[0][V_RHO],u[1][V_RHO],u[2][V_RHO],u[3][V_RHO],u[4][V_RHO]);
    printf(">>>  ul = %g, %g, %g, %g, %g\n",
           ul[0][V_RHO],ul[1][V_RHO],ul[2][V_RHO],ul[3][V_RHO],ul[4][V_RHO]);
    printf(">>>  ur = %g, %g, %g, %g, %g\n",
           ur[0][V_RHO],ur[1][V_RHO],ur[2][V_RHO],ur[3][V_RHO],ur[4][V_RHO]);
  }
#endif

  double geom_factor = (pars.densitized_vars == 1) ? 1.0 : 1.0/sdetg;
#ifdef CYL
 /* only call the source routine once per update. enforce this
  * by checking that dir==0.
  */
  if (dir == 0)
  {
    //cal_source(point->rhs[gen], point->u[gen], pos[STENCIL_CENTER], geom_factor);
    cal_source_v2(point->rhs[gen], point->u[gen], pos[STENCIL_CENTER], geom_factor);
  }
#else
  if (dir == 0)
  {
    cal_source_v3(point->rhs[gen], point->u[gen], pos[STENCIL_CENTER], geom_factor);
  }
#endif

#if 0
   if (point->index.idx[0] == 45) {
    printf("...after\n");
    for (int m = 0; m < n_variab; m++) {
      printf("      pp=3, rhs[%d] = %g, fl = %g, fr = %g\n",
                   m,stcl[3]->rhs[gen][m],f[3][m],f[4][m]);
    }
  }
#endif

  for (int m = 0; m < n_variab; m++) {
    point->rhs[gen][m] -= geom_factor
         * ( f[STENCIL_CENTER+1][m] - f[STENCIL_CENTER][m] ) /
           (pos_h[STENCIL_CENTER+1][dir] - pos_h[STENCIL_CENTER][dir]);
  }

#if 0
  if (point->coords.pos[0] < 0.03) {
    printf(" r=%g, z=%g, indx=%d, %d\n",
            point->coords.pos[0],point->coords.pos[1],point->index.idx[0],
            point->index.idx[1]);
  }
#endif

#if 0
  if (point->index.idx[0] == 1 && point->index.idx[1] == 80) {
    printf("------------------------------------------------------------------------\n");
    printf("    indx = (%d, %d)  x = (%g, %g), dir = %d\n",
             point->index.idx[0], point->index.idx[1],
             point->coords.pos[0], point->coords.pos[1], dir);
    for (int pp = 0; pp < 5; pp++) {
      if (stcl[pp] != NULL) {
      printf(">> Stencil point pp=%d, x = %g, status = %d\n",
                        pp,stcl[pp]->coords.pos[0],stcl[pp]->status[0]);
      for (int m = 0; m < n_variab; m++) {
        printf(ANSI_COLOR_GREEN "      pp=%d, prim[%d] = %g, con = %g, wlt = %g\n" ANSI_COLOR_RESET,pp,m,stcl[pp]->u[gen][m],stcl[pp]->u[gen][U_D+m],stcl[pp]->wavelet[gen][m] );
      }
      if (pp == 2) {
        for (int m = 0; m < n_variab; m++) {
          printf("      pp=%d, rhs[%d] = %g, fl = %g, fr = %g, gf = %g, dx = %g\n",
                       pp,m,stcl[pp]->rhs[gen][m],f[2][m],f[3][m], geom_factor,
                       (pos_h[3][dir] - pos_h[2][dir]) );
        }
      }
      }
    }
  }
#endif

#if 0
   if (point->index.idx[0]==45) 
   {
   //if (point->coords.pos[0] > 0.27 && point->coords.pos[0] < 0.31) 
    printf("-------------------------\n");
    printf("    indx = %d,  x = %g\n",point->index.idx[0],point->coords.pos[0]);

    for (int pp = 0; pp < NSTENCIL; pp++) {
      printf(ANSI_COLOR_RED "Stencil point pp=%d, x = %g, status = %d\n" ANSI_COLOR_RESET,
                        pp,stcl[pp]->coords.pos[0],stcl[pp]->status[0]);
      for (int m = 0; m < n_variab; m++) {
        printf(ANSI_COLOR_GREEN "  pp=%d, prim[%d] = %g, vl = %g, vr = %g, ul = %g, ur = %g\n" ANSI_COLOR_RESET ,pp,m,stcl[pp]->u[gen][m],ul[pp][V_RHO+m],ur[pp][V_RHO+m],ul[pp][U_D+m],ur[pp][U_D+m]);
      }
      if (pp == 3) {
        for (int m = 0; m < n_variab; m++) {
          printf("  pp=%d, rhs[%d] = %g, f = %g\n",pp,m,stcl[pp]->rhs[gen][m],f[pp][m]);
        }
      }
      else if (pp == 4) {
        for (int m = 0; m < n_variab; m++) {
          printf("  pp=%d, f[%d] = %g\n",pp,m,f[pp][m]);
        }
      }
    }
    exit(0);
  }
#endif

}
//}}}

//{{{  cal_source 
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void cal_source(double *rhs, const double *u, const double *pos,
                const double geom_factor)
{

#ifdef CYL
  assert(pos[0] > 0.0);

  if (pars.densitized_vars == 1) {
    rhs[U_SX-U_D] += geom_factor * (u[U_SZ]*u[V_VZ]/pos[0] + u[V_P]);
    rhs[U_SZ-U_D] -= geom_factor * (pos[0]*u[U_SX]*u[V_VZ]);
  }
#endif

}
// }}}

//{{{  cal_source_v2 
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void cal_source_v2(double *rhs, const double *u, const double *pos,
                   const double geom_factor)
{

#ifdef CYL
  assert(pos[0] > 0.0);

  double  gd[3][3], gu[3][3] ;
  double  Su[3], Bu[3], vu[3] ;
  double  Sd[3], Bd[3], vd[3] ;
  //double  D, Tau, psi, P ; 
  double  psi, P ;
  double  Bv, Bsq ;
  double  vsq, invWsq ;
  double  detg, sdetg, invsdetg ;

  // this will assume cylindrical coordinates in flat space with 
  // orientiation (x^0,x^1,x^2) = (rho,z,phi) 
  //load_metric(gd, u, pos); 
  gd[0][0] = 1.0 ;
  gd[0][1] = 0.0 ;
  gd[0][2] = 0.0 ;
  gd[1][0] = 0.0 ;
  gd[1][1] = 1.0 ;
  gd[1][2] = 0.0 ;
  gd[2][0] = 0.0 ;
  gd[2][1] = 0.0 ;
  gd[2][2] = pos[0] * pos[0] ;

  const double  epsilon = 1.e-14 ;

  gu[0][0] = 1.0 ;
  gu[0][1] = 0.0 ;
  gu[0][2] = 0.0 ;
  gu[1][0] = 0.0 ;
  gu[1][1] = 1.0 ;
  gu[1][2] = 0.0 ;
  gu[2][0] = 0.0 ;
  gu[2][1] = 0.0 ;
  gu[2][2] = 1.0 / ( epsilon + pos[0] * pos[0] ) ;

  detg = pos[0] * pos[0] ;

  //int rc = metric_vars(gu, &detg, gd, u, pos) ; 
  //if ( rc != 1 )
  //{
    //printf("calc_source_v2:  Error calculating the metric determinant.\n");
    //printf("calc_source_v2:  We will die here.   \n"); 
    //exit (-1) ;  
  //} 

  sdetg = sqrt( detg ) ;
  invsdetg = 1.0 / sdetg ;

  if (pars.densitized_vars == 1)
  {
    //D     = invsdetg * u[U_D] ;
    Sd[0] = invsdetg * u[U_SX] ;
    Sd[1] = invsdetg * u[U_SY] ;
    Sd[2] = invsdetg * u[U_SZ] ;
    //Tau   = invsdetg * u[U_TAU] ;  
    Bu[0] = invsdetg * u[U_BX] ;
    Bu[1] = invsdetg * u[U_BY] ;
    Bu[2] = invsdetg * u[U_BZ] ;
    psi   = invsdetg * u[U_PSI] ;
  }
  else
  {
    //D     = u[U_D] ;   
    Sd[0] = u[U_SX] ;
    Sd[1] = u[U_SY] ;
    Sd[2] = u[U_SZ] ;
    //Tau   = u[U_TAU] ;  
    Bu[0] = u[U_BX] ;
    Bu[1] = u[U_BY] ;
    Bu[2] = u[U_BZ] ;
    psi   = u[U_PSI] ;
  }

  vu[0] = u[V_VX] ;
  vu[1] = u[V_VY] ;
  vu[2] = u[V_VZ] ;
  P     = u[V_P] ;

  vsq = square_vector( vu, gd ) ;
  Bsq = square_vector( Bu, gd ) ;
  vector_lower_index( vd, vu, gd ) ;
  form_raise_index( Su, Sd, gu ) ;
  vector_lower_index( Bd, Bu, gd ) ;
  invWsq = 1.0 - vsq ;

  Bv = Bd[0]*vu[0] + Bd[1]*vu[1] + Bd[2]*vu[2] ;

  rhs[U_SX-U_D] +=   vu[2] * Sd[2]
                   + P
                   - invWsq * ( Bd[2] * Bu[2] - 0.5 * Bsq )
                   -   Bv   * ( Bd[2] * vu[2] - 0.5 * Bv  ) ;

  rhs[U_SZ-U_D] -=   pos[0]*pos[0] * (            Sd[0] * vu[2]
                                       - invWsq * Bd[0] * Bu[2]  )
                   - Bv * vd[0] * Bu[2] ;

  rhs[U_BX-U_D] += psi ;

  rhs[U_PSI-U_D] -= pars.divB_damping * pos[0] * psi ;
#else
  // this assumes cartesian, flat space 
  rhs[U_PSI-U_D] -= pars.divB_damping * u[U_PSI] ;
#endif

}
// }}}

//{{{  cal_source_v3 
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void cal_source_v3(double *rhs, const double *u, const double *pos,
                const double geom_factor)
{
  double psi ;

  psi = u[U_PSI] ;
  //rhs[U_PSI-U_D] -= pars.divB_damping * psi ;
  rhs[U_PSI-U_D] -= psi ;

  //printf(" yo dude, psi=%12.7e rhs=%15.10e \n", psi, rhs[U_PSI-U_D] ); 



}
// }}}

// MP5 reconstruction {{{
/*----------------------------------------------------------------------
 *
 *    MP5 reconstruction:
 *
 *----------------------------------------------------------------------*/
void mp5_reconstruction(double ul[NSTENCIL][n_variab+n_aux],
                        double ur[NSTENCIL][n_variab+n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim])
{
  double ulf[NSTENCIL], urf[NSTENCIL], uf[NSTENCIL];
  int n = NSTENCIL;

  // reconstruct the primitive variables. Including each component of v^i
  for (int m = 0; m < n_prims; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = u[i][m];
    }

    recon_mp5(ulf, urf, uf, n);

    for (int i = 0; i < NSTENCIL; i++) {
      ul[i][m] = ulf[i];
      ur[i][m] = urf[i];
    }
  }

  // enforce the floor
  for (int i = 0; i < NSTENCIL; i++) {
    ul[i][V_RHO] = fmax(pars.vacuum, ul[i][V_RHO]);
    ur[i][V_RHO] = fmax(pars.vacuum, ur[i][V_RHO]);
    ul[i][V_P] = fmax(pars.vacuum, ul[i][V_P]);
    ur[i][V_P] = fmax(pars.vacuum, ur[i][V_P]);
  }

  // reconstruct Wv, and overwrite components of v^i
  double vec[3], wv[NSTENCIL][3], wvl[NSTENCIL][3], wvr[NSTENCIL][3];

  for (int i = 0; i < NSTENCIL; i++) {
    recon_wv(vec, u[i], pos[i]);
    wv[i][0] = vec[0];
    wv[i][1] = vec[1];
    wv[i][2] = vec[2];
  }
  for (int m = 0; m < 3; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = wv[i][m];
    }

    recon_mp5(ulf, urf, uf, n);

    for (int i = 0; i < NSTENCIL; i++) {
      wvl[i][m] = ulf[i];
      wvr[i][m] = urf[i];
    }
  }

  for (int i = 1; i < NSTENCIL-1; i++) {
    double gd[3][3];
    load_metric(gd, ul[i], pos_h[i]);
    double wvl3[3], wvr3[3];
    for (int m = 0; m < 3; m++) {
      wvl3[m] = wvl[i][m];
      wvr3[m] = wvr[i][m];
    }
    double wl = sqrt(1.0 + square_vector(wvl3, gd));
    double wr = sqrt(1.0 + square_vector(wvr3, gd));
    for (int m = 0; m < 3; m++) {
      ul[i][V_VX+m] = wvl3[m]/ wl;
      ur[i][V_VX+m] = wvr3[m]/ wr;
    }
  }

}

// recon_mp5 and helper routines {{{
/*----------------------------------------------------------------------
 *
 *    recon_mp5. Performs MP5 reconstruction on a single function.
 *
 *----------------------------------------------------------------------*/
void recon_mp5(double *ul, double *ur, double *u, int n)
{

  double b1 = (1.0/60.0);
  double b2 = (4.0/3.0);
  double alpha = 4.0;
  double epsm  = 1.0e-10;

  for (int j = 2; j < n-2; j++) {

    double unorm = sqrt( u[j-2]*u[j-2] + u[j-1]*u[j-1] + u[j]*u[j]
                       + u[j+1]*u[j+1] + u[j+2]*u[j+2] );

   /* The left state at j+1/2 */
    double uor = b1*( 2.0*u[j-2] - 13.0*u[j-1] + 47.0*u[j]
                   + 27.0*u[j+1] - 3.0*u[j+2] );
    double ump = u[j] + minmod( u[j+1] - u[j], alpha*(u[j] - u[j-1]) );
    if ( (uor-u[j])*(uor-ump) > epsm*unorm ) {
      double djm1 = u[j-2] - 2.0*u[j-1] + u[j];
      double dj   = u[j-1] - 2.0*u[j]   + u[j+1];
      double djp1 = u[j]   - 2.0*u[j+1] + u[j+2];

      double dm4jph = minmod4(4.0*dj-djp1, 4.0*djp1-dj, dj, djp1);
      double dm4jmh = minmod4(4.0*dj-djm1, 4.0*djm1-dj, dj, djm1);
      double uul = u[j] + alpha*(u[j] - u[j-1]);
      double uav = 0.5*(u[j] + u[j+1]);
      double umd = uav - 0.5*dm4jph;
      double ulc = u[j] + 0.5*(u[j] - u[j-1]) + b2*dm4jmh;
      double umin = fmax( fmin3( u[j], u[j+1], umd ), fmin3( u[j], uul, ulc) );
      double umax = fmin( fmax3( u[j], u[j+1], umd ), fmax3( u[j], uul, ulc) );
      ul[j+1] = uor + minmod( umin - uor, umax - uor );
    }
    else {
      ul[j+1] = uor;
    }

   /* The right state at j-1/2
         change u[j-2]--> u[j+2]
                u[j-1]--> u[j+1]
                u[j+1]--> u[j-1]
                u[j+2]--> u[j-2]
    */
    uor = b1*(  2.0*u[j+2]  - 13.0*u[j+1]  + 47.0*u[j]
             + 27.0*u[j-1] - 3.0*u[j-2] );
    ump = u[j] + minmod( u[j-1] - u[j], alpha*(u[j] - u[j+1]) );
    if ( (uor-u[j])*(uor-ump) > epsm*unorm ) {
      double djm1 = u[j+2] - 2.0*u[j+1] + u[j];
      double dj   = u[j+1] - 2.0*u[j]   + u[j-1];
      double djp1 = u[j]   - 2.0*u[j-1] + u[j-2];

      double dm4jph = minmod4(4.0*dj-djp1, 4.0*djp1-dj, dj, djp1);
      double dm4jmh = minmod4(4.0*dj-djm1, 4.0*djm1-dj, dj, djm1);
      double uul = u[j] + alpha*(u[j] - u[j+1]);
      double uav = 0.50*(u[j] + u[j-1]);
      double umd = uav - 0.50*dm4jph;
      double ulc = u[j] + 0.50*(u[j] - u[j+1]) + b2*dm4jmh;
      double umin = fmax( fmin3( u[j], u[j-1], umd), fmin3( u[j], uul, ulc) );
      double umax = fmin( fmax3( u[j], u[j-1], umd), fmax3( u[j], uul, ulc) );
      ur[j] = uor + minmod(umin - uor, umax - uor);
    }
    else {
      ur[j] = uor;
    }

  }

 /* boundary conditions */
  ul[0] = u[0];
  ul[1] = u[0];
  ul[2] = u[1];
  ur[0] = u[0];
  ur[1] = u[1];

  ur[n-2] = u[n-2];
  ur[n-1] = u[n-1];
  ul[n-1] = u[n-2];

}

/*----------------------------------------------------------------------
 *
 *    minmod
 *
 *----------------------------------------------------------------------*/
double minmod(double x, double y)
{
  double sgnx = (x < 0.0) ? -1.0 : 1.0;
  double sgny = (y < 0.0) ? -1.0 : 1.0;
  return 0.5*(sgnx + sgny)*fmin(fabs(x),fabs(y));
}

/*----------------------------------------------------------------------
 *
 *    minmod4
 *
 *----------------------------------------------------------------------*/
double minmod4(double w, double x, double y, double z)
{

  double sgnw = (w < 0.0) ? -1.0 : 1.0;
  double sgnx = (x < 0.0) ? -1.0 : 1.0;
  double sgny = (y < 0.0) ? -1.0 : 1.0;
  double sgnz = (z < 0.0) ? -1.0 : 1.0;

  return 0.125*(sgnw + sgnx) * fabs((sgnw + sgny) * (sgnw + sgnz))
              * fmin( fmin(fabs(w), fabs(x)), fmin(fabs(y), fabs(z)) );
}

/*----------------------------------------------------------------------
 *
 *    fmin3
 *
 *----------------------------------------------------------------------*/
double fmin3( double x, double y, double z)
{
  double a = fmin(x, y);
  return (z < a) ? z : a;
}

/*----------------------------------------------------------------------
 *
 *    fmax3
 *
 *----------------------------------------------------------------------*/
double fmax3( double x, double y, double z)
{
  double a = fmax(x, y);
  return (z > a) ? z : a;
}
// }}}


// }}}

// PPM reconstruction {{{
/*----------------------------------------------------------------------
 *
 *    PPM reconstruction: Piece-wise Parabolic Method
 *
 *----------------------------------------------------------------------*/
void ppm_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim])
{
  double ulf[NSTENCIL], urf[NSTENCIL], uf[NSTENCIL], slp[NSTENCIL];

  // reconstruct the primitive variables. Including each component of v^i
  for (int m = 0; m < n_prims; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = u[i][m];
    }
    recon_func_ppm(ulf, urf, uf, slp, NSTENCIL);
    for (int i = 0; i < NSTENCIL; i++) {
      ul[i][m] = ulf[i];
      ur[i][m] = urf[i];
    }
  }

  // enforce the floor
  for (int i = 0; i < NSTENCIL; i++) {
    ul[i][V_RHO] = fmax(pars.vacuum, ul[i][V_RHO]);
    ur[i][V_RHO] = fmax(pars.vacuum, ur[i][V_RHO]);
    ul[i][V_P] = fmax(pars.vacuum, ul[i][V_P]);
    ur[i][V_P] = fmax(pars.vacuum, ur[i][V_P]);
  }

  // reconstruct Wv, and overwrite components of v^i
  double vec[3], wv[NSTENCIL][3], wvl[NSTENCIL][3], wvr[NSTENCIL][3];

  for (int i = 0; i < NSTENCIL; i++) {
    recon_wv(vec, u[i], pos[i]);
    wv[i][0] = vec[0];
    wv[i][1] = vec[1];
    wv[i][2] = vec[2];
  }
  for (int m = 0; m < 3; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = wv[i][m];
    }
    recon_func_ppm(ulf, urf, uf, slp, NSTENCIL);
    for (int i = 0; i < NSTENCIL; i++) {
      wvl[i][m] = ulf[i];
      wvr[i][m] = urf[i];
    }
  }

  for (int i = 1; i < NSTENCIL-1; i++) {
    double gd[3][3];
    load_metric(gd, ul[i], pos_h[i]);
    double wvl3[3], wvr3[3];
    for (int m = 0; m < 3; m++) {
      wvl3[m] = wvl[i][m];
      wvr3[m] = wvr[i][m];
    }
    double wl = sqrt(1.0 + square_vector(wvl3, gd));
    double wr = sqrt(1.0 + square_vector(wvr3, gd));
    for (int m = 0; m < 3; m++) {
      ul[i][V_VX+m] = wvl3[m]/ wl;
      ur[i][V_VX+m] = wvr3[m]/ wr;
    }
  }

}

/*----------------------------------------------------------------------
 *
 *    recon_func_ppm:
 *      PPM reconstruction of a single function.
 *
 *----------------------------------------------------------------------*/
void recon_func_ppm(double ul[NSTENCIL], double ur[NSTENCIL], double u[NSTENCIL],
                    double slope[NSTENCIL], const int n)
{
  const double one_sixth = 1.0/6.0;

  slope[0] = 0.0;
  slope[n-1] = 0.0;

  for (int i = 1; i < n-1; i++) {
    double slc = 0.5 * (u[i+1] - u[i-1]);
    double slp = u[i+1] - u[i];
    double slm = u[i] - u[i-1];
    if ( slp * slm > 0.0 ) {
      double sgn = (slc < 0.0) ? -1.0 : 1.0;
      double slc1 = fabs(slc);
      double slp1 = 2.0*fabs(slp);
      double slm1 = 2.0*fabs(slm);
      double min1 = (slc1 < slp1) ? slc1 : slp1;
      double min2 = (min1 < slm1) ? min1 : slm1;
      slope[i] = sgn * min2;
    }
    else {
      slope[i] = 0.0;
    }
  }

//initialize the left and right states
//right <==> minus
//left <==> plus
  ul[0] = u[0];
  ur[0] = u[0];
  for (int i = 1; i < n; i++) {
    ul[i] = 0.5*(u[i] + u[i-1]) + one_sixth*( slope[i-1] - slope[i] );
    ur[i] = ul[i];
  }


//monotonicity constraints
  for (int i = 0; i < n-1; i++) {
    double temp1 = (ul[i+1] - ur[i])*(u[i] - 0.5*(ur[i] + ul[i+1]));
    double temp2 = one_sixth*(ul[i+1] - ur[i])*(ul[i+1] - ur[i]);
    if ( (ul[i+1]-u[i])*(u[i]-ur[i]) <= 0.0 ) {
      ur[i] = u[i];
      ul[i+1] = u[i];
    }
    else if ( temp1 > temp2 ) {
      ur[i] = 3.0*u[i] - 2.0*ul[i+1];
    }
    else if ( temp1 < -temp2 ) {
      ul[i+1] = 3.0*u[i] - 2.0*ur[i];
    }
  }

// boundary conditions
  ur[n-1] = u[0];

}

// }}}

// PLM reconstruction {{{
/*----------------------------------------------------------------------
 *
 *    PLM reconstruction: Piece-wise Linear Method with Minmod limiter
 *
 *----------------------------------------------------------------------*/
void plm_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim],
                        const int dir)
{

  // Reconstruct the primitive variables.
  for (int i = 1; i < NSTENCIL-1; i++) {
    // Loop over primitive vars.
    for (int p = 0; p < n_prims ; p++) {
      double df1, df2, sdf1, sdf2, slp;
      df1 = (u[i][p] - u[i-1][p])/(pos[i][dir] - pos[i-1][dir]);
      df2 = (u[i+1][p] - u[i][p])/(pos[i+1][dir] - pos[i][dir]);
// FIXME-make more efficient
      sdf1 = (df1 > 0.0) ? 1.0 : ((df1 < 0.0) ? -1.0 : 0.0);
      sdf2 = (df2 > 0.0) ? 1.0 : ((df2 < 0.0) ? -1.0 : 0.0);
// -end FIXME
      slp = 0.5*(sdf1 + sdf2)*fmin(fabs(df1), fabs(df2));
      ul[i+1][p] = u[i][p] + slp * (pos_h[i+1][dir] - pos[i][dir]);
      ur[i][p]   = u[i][p] + slp * (pos_h[i][dir] - pos[i][dir]);
    }
  }

  if (pars.reconstruct_Wv == 1) {
    double wv[NSTENCIL][3], wvl[3], wvr[3];
    for (int i = 0; i < NSTENCIL; i++) {
      recon_wv(wv[i], u[i], pos[i]);
    }
    // Reconstruct Wv
    for (int i = 1; i < NSTENCIL-1; i++) {
      // Loop over primitive vars.
      for (int p = 0; p < 3; p++) {
        double df1, df2, sdf1, sdf2, slp;
        df1 = (wv[i][p] - wv[i-1][p])/(pos[i][dir] - pos[i-1][dir]);
        df2 = (wv[i+1][p] - wv[i][p])/(pos[i+1][dir] - pos[i][dir]);
  // FIXME-make more efficient
        sdf1 = (df1 > 0.0) ? 1.0 : ((df1 < 0.0) ? -1.0 : 0.0);
        sdf2 = (df2 > 0.0) ? 1.0 : ((df2 < 0.0) ? -1.0 : 0.0);
  // -end FIXME
        slp = 0.5*(sdf1 + sdf2)*fmin(fabs(df1), fabs(df2));
        wvl[p] = wv[i][p] + slp * (pos_h[i+1][dir] - pos[i][dir]);
        wvr[p] = wv[i][p] + slp * (pos_h[i][dir] - pos[i][dir]);
      }
      double gdl[3][3], gdr[3][3];
      load_metric(gdl, ul[i+1], pos_h[i+1]);
      load_metric(gdr, ur[i], pos_h[i]);
      double wl = sqrt(1.0 + square_vector(wvl, gdl));
      double wr = sqrt(1.0 + square_vector(wvr, gdr));
      for (int p = 0; p < 3; p++) {
        ul[i+1][V_VX+p] = wvl[p]/ wl;
        ur[i][V_VX+p] = wvr[p]/ wr;
      }
    }
  }
}

// }}}

// recon Wv (old) {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void recon_wv_old(double *wv, double *u, double *pos)
{
  double gd[3][3];
  load_metric(gd, u, pos);

  double vu[3];
  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  double vsq = square_vector(vu, gd);

  if ( vsq > 1.0 ) {
    printf("## recon_wv: before reconstruction, vsq > 1. vsq = %g\n",vsq);
    exit(-1);
  }

  double W = fmax(1.0 / sqrt(1.0 - vsq), 1.0);

  wv[0] = W*vu[0];
  wv[1] = W*vu[1];
  wv[2] = W*vu[2];

//printf("@1 u=(%g,%g,%g) Wv=(%g,%g,%g)\n",vu[0],vu[1],vu[2],wv[0],wv[1],wv[2]);

}
//}}}

// recon Wv {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void recon_wv(double *wv, double *u, double *pos)
{
  double gd[3][3];
  load_metric(gd, u, pos);

  double vu[3];
  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  double vsq = square_vector(vu, gd);

#if 0
  if ( vsq > 1.0 ) {
    printf("## recon_wv: before reconstruction, vsq > 1.0 vsq = %g\n",vsq);
    exit(-1);
  }
#endif

  if ( u[V_RHO] < 0.0 )
  { printf("3.0negative prim values:  rho = %15.10e \n", u[V_RHO] ); }
  if ( u[V_P] < 0.0 )
  { printf("3.0negative prim values:   P  = %15.10e \n", u[V_P]); }




#if 1
  //  If vsq is only a little bit (roundoff?) greater than 1, decrease
  //  components of v by a little.
  if ( vsq-1.0 >= 0.0  &&  vsq-1.0 < 1.e-15 )
  {
      for (int ii=0; ii<3; ii++)
      {
             if ( vu[ii] >= 0.0 ) { vu[ii] -= 5.e-16 ; }
        else if ( vu[ii] <  0.0 ) { vu[ii] += 5.e-16 ; }
      }
      vsq = square_vector(vu, gd) ;

      // if its still not good enough, do it again ...
      if ( (vsq-1.0) >= 0.0 && vsq-1.0 < 1.0e-15 )
      {
          for (int ii=0; ii<3; ii++)
          {
                 if ( vu[ii] >= 0.0 ) { vu[ii] -= 5.e-16 ; }
            else if ( vu[ii] <  0.0 ) { vu[ii] += 5.e-16 ; }
          }
          vsq = square_vector(vu, gd) ;
      }
  }

  if ( vsq >= 1.0 ) {
    printf("## recon_wv: before reconstruction, vsq >= 1.0 vsq = %25.20e\n",vsq);
    printf("  recon_wv: u[V_VX] = %25.20e\n", u[V_VX]);
    printf("  recon_wv: u[V_VY] = %25.20e\n", u[V_VY]);
    printf("  recon_wv: u[V_VZ] = %25.20e\n", u[V_VZ]);
    printf("  recon_wv: u[V_P]  = %25.20e\n", u[V_P]);
    printf("  recon_wv: u[V_RHO]= %25.20e\n", u[V_RHO]);
    printf("  recon_wv: u[U_SX] = %25.20e\n", u[U_SX]);
    printf("  recon_wv: u[U_SY] = %25.20e\n", u[U_SY]);
    printf("  recon_wv: u[U_SZ] = %25.20e\n", u[U_SZ]);

    double  tmp = sqrt( 1.0/vsq - 6.e-16 ) ;
    for ( int ii=0; ii<3; ii++ )
    {
      vu[ii] *= tmp ;
    }

    //exit(-1);
  }

  // If we subtracted from the components of v to try and bring down vsq
  // below 1, we should save the resulting values.
  u[V_VX] = vu[0] ;
  u[V_VY] = vu[1] ;
  u[V_VZ] = vu[2] ;

#endif

  double W = fmax(1.0 / sqrt(1.0 - vsq), 1.0);

  wv[0] = W*vu[0];
  wv[1] = W*vu[1];
  wv[2] = W*vu[2];

}
//}}}

//{{{  calv
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void calv(double *u, double *wv, double *pos)
{
  double gd[3][3];
  load_metric(gd, u, pos);

  double Wvsq = square_vector(wv, gd);
  double W = sqrt(1.0 + Wvsq);

/*
  double u1 = u[V_VX];
  double u2 = u[V_VY];
  double u3 = u[V_VZ];
*/

  u[V_VX] = wv[0]/W;
  u[V_VY] = wv[1]/W;
  u[V_VZ] = wv[2]/W;

/*
  printf("@2 u=(%g,%g,%g) Wv=(%g,%g,%g)\n",u1,u2,u3,wv[0],wv[1],wv[2]);
  if (fabs(u1-u[V_VX]) > 1.0e-12) {
    printf("@@@ u1=%g, ux=%g, diff=%g\n",u1,u[V_VX],fabs(u1-u[V_VX]));
  }
  printf("@3 u=(%g,%g,%g) Wv=(%g,%g,%g)\n",u[V_VX],u[V_VY],u[V_VZ],
                                           wv[0],wv[1],wv[2]);
*/

}
// }}}

// prim_to_con {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void prim_to_con(double *u, const double *pos)
{
  double rho, p;
  double vsq, Bsq, Bv, h, Wsq, hWsq;
  double vu[3], vd[3], Bu[3], Bd[3];
  double gd[3][3], gu[3][3], detg;

  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("prim_to_con: problem with metric. detg = %g\n",detg);
    exit(-1);
  }
  double sdetg = sqrt(detg);

  if (u[V_RHO] < 0.0) {
    printf("prim_to_con: rho < 0. rho=%g\n",u[V_RHO]);
    u[V_RHO] = pars.vacuum;
  }
  if (u[V_P] < 0.0) {
    printf("prim_to_con: P < 0. rho=%g\n",u[V_P]);
    u[V_P] = pars.vacuum;
  }


  rho    = u[V_RHO];
  vu[0]  = u[V_VX];
  vu[1]  = u[V_VY];
  vu[2]  = u[V_VZ];
  p      = u[V_P];
  Bu[0]  = u[V_BX];  // FIXME be careful with factors of sdetg & B.
  Bu[1]  = u[V_BY];
  Bu[2]  = u[V_BZ];
  
  double psi = u[V_PSI];

  vsq = square_vector(vu, gd);
  Bsq = square_vector(Bu, gd);
  vector_lower_index(vd, vu, gd);
  vector_lower_index(Bd, Bu, gd);

  Wsq = 1.0/(1.0-vsq);
  Bv = vd[0]*Bu[0] + vd[1]*Bu[1] + vd[2]*Bu[2];
  h = rho + pars.gamma*p / (pars.gamma - 1.0);
  hWsq = h*Wsq;


  if ( u[V_VX] < -1.0 )
  {
    printf("  prim_to_con:  vx = %25.20e \n", u[V_VX] );
    printf("  prim_to_con:   P = %25.20e \n", u[V_P] );
    printf("  prim_to_con: rho = %25.20e \n", u[V_RHO] );
    printf("  prim_to_con:   X = %25.20e \n", pos[0] );
  }


  if (Wsq < 1.0) {
    printf("Wsq < 1. vsq=%f. v=(%f, %f, %f)\n",vsq,vu[0],vu[1],vu[2]);
    printf(" metric: %f, %f, %f, %f, %f, %f\n",gd[0][0],gd[0][1],gd[0][2],
            gd[1][1],gd[1][2],gd[2][2]);
  }
  else {
    if (pars.densitized_vars == 1) {
      u[U_D]   = sdetg*(rho * sqrt(Wsq));
      u[U_TAU] = sdetg*(hWsq + Bsq - p - 0.5*(Bv*Bv + Bsq/Wsq)) - u[U_D];
      u[U_SX]  = sdetg*((hWsq + Bsq)*vd[0] - Bv*Bd[0]);
      u[U_SY]  = sdetg*((hWsq + Bsq)*vd[1] - Bv*Bd[1]);
      u[U_SZ]  = sdetg*((hWsq + Bsq)*vd[2] - Bv*Bd[2]);
      u[U_BX]  = sdetg*Bu[0];
      u[U_BY]  = sdetg*Bu[1];
      u[U_BZ]  = sdetg*Bu[2];
      u[U_PSI] = sdetg*psi;
    }
    else {
      u[U_D]   = (rho * sqrt(Wsq));
      u[U_TAU] = (hWsq + Bsq - p - 0.5*(Bv*Bv + Bsq/Wsq)) - u[U_D];
      u[U_SX]  = ((hWsq + Bsq)*vd[0] - Bv*Bd[0]);
      u[U_SY]  = ((hWsq + Bsq)*vd[1] - Bv*Bd[1]);
      u[U_SZ]  = ((hWsq + Bsq)*vd[2] - Bv*Bd[2]);
      u[U_BX]  = Bu[0];
      u[U_BY]  = Bu[1];
      u[U_BZ]  = Bu[2];
      u[U_PSI] = u[V_PSI];
    }
  }

}
// }}}

// eigenvalues {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void eigen_values(double *Lp, double *Lm, double *u, const int dir, double *pos)
{

  double gd[3][3], gu[3][3];
  double detg;
  const int ltrace = 0;

  if (ltrace) {
    printf("CalMaxSpeeds:  begin\n");
  }

  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("eigen_values: problem with metric. detg = %g\n",detg);
    exit(-1);
  }

  if (pars.use_wave_speeds == 1) {
    MHDEigenVals(Lp, Lm, u, gd, gu, dir);
    //PFEigenVals(Lp, Lm, u, gd, gu, dir);
  }
  else {
    DefaultEigenVals(Lp, Lm, u, gu, dir);
  }

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void PFEigenVals(double *Lp, double *Lm, double *u,
                 double gd[3][3], double gu[3][3], const int dir)
{
  double Alpha, Beta[3];
  double gamma = pars.gamma;
  double rho   = u[V_RHO];
  double p     = u[V_P];

  double vu[3];
  double vsq, csq, vk, hkk, dis, betak;

  double cs = cal_cs(rho, p, gamma);

  load_gauge(&Alpha, Beta, u);

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];

  hkk    = gu[dir][dir];
  betak  = Beta[dir];
  vk     = vu[dir];

  vsq = square_vector(vu, gd);
  csq = cs*cs;

  dis = csq*(1.0-vsq)*(hkk*(1.0-vsq*csq) - vk*vk*(1.0-csq));
  *Lp = Alpha/(1.0-vsq*csq)*(vk*(1.0-csq) + sqrt(dis)) - betak;
  *Lm = Alpha/(1.0-vsq*csq)*(vk*(1.0-csq) - sqrt(dis)) - betak;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void MHDEigenVals(double *Lp, double *Lm, double *u,
                 double gd[3][3], double gu[3][3], const int dir)
{

  double Alpha, Beta[3], vu[3], Bu[3], Bd[3];
  double vk, betak, hkk;

  double gamma = pars.gamma;

  double P   = u[V_P];
  double rho = u[V_RHO];
  double h   = (rho + gamma/(gamma-1.0)*P);

  load_gauge(&Alpha, Beta, u);

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];
  Bu[0] = u[U_BX];
  Bu[1] = u[U_BY];
  Bu[2] = u[U_BZ];

  double vsq = square_vector(vu, gd);
  vector_lower_index(Bd, Bu, gd);

  double Bv   = Bd[0]*vu[0] + Bd[1]*vu[1] + Bd[2]*vu[2];
  double Bsq  = Bd[0]*Bu[0] + Bd[1]*Bu[1] + Bd[2]*Bu[2];
  double Wsq  = 1.0/(1.0 - vsq);

  vk = vu[dir];
  betak = Beta[dir];
  hkk = gu[dir][dir];

  double cs    = cal_cs(rho, P, gamma);
  double csq   = cs*cs;

 /* Careful here.  Bsq is the square of the 3-magnetic field.  bsq is the
  * square of the co-moving magnetic four-vector.
  * casq is the Alfven speed squared.
  */
  double bsq = Bsq/Wsq + Bv*Bv;
  double casq = bsq/(h + bsq);
  double asq = csq + casq - csq*casq;

  double dis = asq*(1.0-vsq)*(hkk*(1.0-vsq*asq) - vk*vk*(1.0-asq));
  *Lp = Alpha/(1.0-vsq*asq)*(vk*(1.0-asq) + sqrt(dis)) - betak;
  *Lm = Alpha/(1.0-vsq*asq)*(vk*(1.0-asq) - sqrt(dis)) - betak;

 /* Calculate the light cone speeds for comparison.  These are
  * the default max and min speeds

     *Lpd =  Alpha*sqrt(hkk) - betak;
     *Lmd = -Alpha*sqrt(hkk) - betak;
  */

}

/*-------------------------------------------------------------------------
 *
 *  Calculate the sound speed.
 *
 *-------------------------------------------------------------------------*/
double cal_cs(double rho, double p, double gamma)
{

  const double factor = 1.0e3;
  double       y, cs;

  if (rho*factor < p) {
    y  = (gamma - 1.0)*rho/(gamma*p);
    cs = sqrt(gamma - 1.0)*(1.0+(-0.5+(0.375+(-0.3125+0.2734375*y)*y)*y)*y);
  }
  else {
    cs = sqrt(gamma*(gamma - 1.0)*p / ((gamma - 1.0)*rho + gamma*p));
  }

  return cs;


}

/*-------------------------------------------------------------------------
 *
 * Calculate local light cone speeds
 *
 *-------------------------------------------------------------------------*/
void DefaultEigenVals(double *Lp, double *Lm, double *u, double gu[3][3],
                      const int dir)
{
  double Alpha, Beta[3];

  load_gauge(&Alpha, Beta, u);

  *Lp =  Alpha*sqrt(gu[dir][dir]) - Beta[dir];
  *Lm = -Alpha*sqrt(gu[dir][dir]) - Beta[dir];
}

// }}}

// apply_floor {{{
/*-------------------------------------------------------------------------
 *
 * Floor D and rescale the momentum to satisfy S^2 < (tau + D)^2 if necessary.
 *
 *-------------------------------------------------------------------------*/
void apply_floor(double *u, double *pos)
{
  double detg;
  double gd[3][3], gu[3][3];

  load_metric(gd, u, pos);
  int rc = metric_vars(gu, &detg, gd, u, pos);
  if (rc != 1) {
    printf("apply_floor: Error calculating detg = %g. Die here.\n",detg);
    exit(-1);
  }

  double Sd[3];
  if ( u[U_D] < pars.vacuum) {
    u[U_D]  = pars.vacuum;
    u[U_SX] = 0.0;
    u[U_SY] = 0.0;
    u[U_SZ] = 0.0;
    u[U_TAU] = pars.vacuum;
  }
  u[U_TAU] = fmax(pars.vacuum,u[U_TAU]);
  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];
  double Ssq = square_form(Sd, gu);
  double S2max = (2.0*u[U_D] + u[U_TAU])*u[U_TAU];
  if (S2max <= Ssq) {
//FIXME  decide if we want to use this
//#define VELOCITY_FACTOR 0.9999
    //double t_min = VELOCITY_FACTOR * sqrt(S2max/Ssq);
    double t_min = sqrt(S2max/Ssq);
    u[U_SX] *= t_min;
    u[U_SY] *= t_min;
    u[U_SZ] *= t_min;
  }

  if ( u[V_VX] < -1.0  ||  u[V_VX] > 1.0 )
  {
    printf("  apply_floor:  v is superluminal \n") ;
    printf("  apply_floor:       vx = %25.20e \n", u[V_VX] ) ;
  }

}
// }}}

// con_to_prim {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int con_to_prim(double *du, double *du_old, double *pos)
{
  int       rc;
  double    fpar[NFPAR];
  double    gamma = pars.gamma;
  double    u[n_variab + n_aux];

  double    gd[3][3], gu[3][3];
  double    detg;
  double    xpt[4];

  xpt[0] = 0.0;

  xpt[1] = pos[0];
  xpt[2] = 0.0;
  xpt[3] = 0.0;
#if n_dim == 2
  xpt[2] = pos[1];
#elif n_dim == 3
  xpt[2] = pos[1];
  xpt[3] = pos[2];
#endif

  load_metric(gd, du, pos);
  rc = metric_vars(gu, &detg, gd, du, pos);
  if (rc != 1) {
    printf("con_to_prim: Error calculating metric determinant. Die here.\n");
    exit(-1);
  }
  double sdetg = sqrt(detg);

  if (pars.densitized_vars == 1) {
    double isdetg = 1.0/sdetg;
    for (int ii = n_prims; ii < n_prims + n_cons; ii++) {
      u[ii] = isdetg * du[ii];
    }
  }
  else {
    for (int ii = n_prims; ii < n_prims + n_cons; ii++) {
      u[ii] = du[ii];
    }
  }

 /* Floor the primitive variables.  When running with AMR, there have been
  * cases where interpolation has resulted in the variables D, tau, rho, or
  * P having negative values.  D and tau are floored in contoprim.  rho and
  * P are used here for the initial guess.  Floor them just to be sure. */
  if (du_old[V_RHO] < pars.vacuum) du_old[V_RHO] = pars.vacuum;
  if (du_old[V_P]   < pars.vacuum) du_old[V_P]   = pars.vacuum;

 /* copy the prim vars (only) into the solution at the advanced time.
  * this serves as an initial guess for the primitive solver. Note, primitive
  * variables are not densitized, so no need to divide by sdetg.
  */
  for (int ii = 0; ii < n_prims; ii++) {
    u[ii] = du_old[ii];
  }

 /* apply the floor here. */
  apply_floor(u, pos);
//FIXME  in an earlier code, we were testing several versions of apply_floor, including for just mhd.

  MHDWorkVars(fpar, u, gd, gu, xpt, gamma);

       if (pars.eos_solver == 1) {
    rc = MHD_Solver_1(u, xpt, gd, gu, fpar);
  }
  else if (pars.eos_solver == 2) {
    rc = MHD_Solver_2(u, xpt, gd, gu, fpar);
  }
  else if (pars.eos_solver == 4) {
    rc = MHD_Solver_4(u, xpt, gd, gu, fpar);
  }
  else if (pars.eos_solver == 3) {
    rc = MHD_Solver_3(u, xpt, gd, gu, fpar);
  }
  else if (pars.eos_solver == 5) {
    rc = MHD_Solver_5(u, xpt, gd, gu, fpar);
  }


//FIXME  this needs changing since we do not usually consider calling the isentropic solver at all now ...
  /* Call the isentropic solver when the default solver MHD_Solver_1 fails */
  if (rc < 0) {
    if (1) {      //pars.contoprimwarn == 1 ||) { FIXME
      printf("MHD_Solver failed\n");
      exit(-2); // FIXME
    }
    rc = MHD_Solver_Isentropic(u, xpt, gd, gu, fpar);
    if (rc == 0) {
      if (pars.contoprimwarn == 1)
            printf("MHD_Solver_Isentropic to the rescue\n");
    }
  }

//FIXME   this too ...
  if (rc < 0) {
    if (DIE_ON_C2P_ERRORS) {
      printf("### MHD_Solver_1 did not work.  Die here.\n");
      fpar[FP_TRACE] = 1.0;
      rc = MHD_Solver_1(u, xpt, gd, gu, fpar);
      /* exit(2); */
      printf("ConToPrimPT: MHD_Solver 1 did not work\n");
      MathDump(fpar);
      exit(-1);
    }
    return -1;
  }

 /* Copy the conserved variables back into du. This is done in case the
  * conserved variables were modified by apply_floor or elsewhere.
  * These variables need to be densitized. */
  if (pars.densitized_vars == 1) {
    for (int ii = n_prims; ii < n_prims + n_cons; ii++) {
      du[ii] = sdetg * u[ii];
    }
  }
  else {
    for (int ii = n_prims; ii < n_prims + n_cons; ii++) {
      du[ii] = u[ii];
    }
  }


 /* copy the primitive variables into du */
  for (int ii = 0; ii < n_prims; ii++) {
    du[ii] = u[ii];
  }

  return 0;

}

/*-------------------------------------------------------------------------
 *
 * This is the original solver from had. It works with MHD (B != 0).
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_1(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar)
{

  int    rc;
  double q, qi, ql, qh, vsq, F, eq1;
  double Sd[3], Su[3], sol[4];

  double gamma = pars.gamma;

  double Ssq = fpar[FP_SSQ];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];

  double D     = u[U_D];
  double tau   = u[U_TAU];
  double Bx    = u[U_BX];
  double By    = u[U_BY];
  double Bz    = u[U_BZ];

  int trace = fpar[FP_TRACE];
  int   fail;

  if (trace) {
    printf("MHD_Solver_1 begin...\n");
  }

  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];
  form_raise_index(Su, Sd, gu);

  /* An initial guess for hW^2 */
  vsq = fpar[FP_VSQ];
  qi = (u[V_RHO] + gamma/(gamma-1.0)*u[V_P])/(1.0-vsq);
  ql = 0.95*qi;
  qh = 1.05*qi;

  if (trace) {
    printf("initial guess ql=%g, qh=%g. rho=%g, gamma=%g, p=%g, vsq=%g\n",ql,qh,u[V_RHO],gamma,u[V_P],vsq);
  }

  rc = FindFluidBounds(&ql, fpar);

  if (rc < 0) {
    if ( pars.contoprimwarn == 1 ) {
      printf("MHD_Solver_1:  Could not find a lower bound\n");
      printf("  x  := %g\n",xpt[1]);
      printf("  y  := %g\n",xpt[2]);
      printf("  z  := %g\n",xpt[3]);
      printf("  Su := (%g, %g, %g)\n",Su[0], Su[1], Su[2]);
      printf("  Bu := (%g, %g, %g)\n",Bx, By, Bz);
      printf("  vu := (%g, %g, %g)\n",u[V_VX],u[V_VY],u[V_VZ]);
      printf("----- Maple Output-------------\n\n");
      printf("  Ssq:=%g; Bsq:=%g; BS:=%g;\n",fpar[FP_SSQ],
                     fpar[FP_BSQ],fpar[FP_BS]);
      printf("  d:=1-((2*q+Bsq)*BS^2+q^2*Ssq)/(q^2*(q+Bsq)^2);\n");
    }
  /*  return -2; */
    fail = 1;
  }

  if (trace) {
    printf("after FindFluidBounds ql=%g, qh=%g\n",ql,qh);
  }

  if (qi < ql) {
    qi = 1.1*ql;
  }
  if (qh < ql) qh = 2.0*ql;

  sol[0] = qi;
  sol[1] = ql;
  sol[2] = qh;

  int xrc;

  double xq  = sol[0];
  double xrl = sol[1];
  double xrh = sol[2];
  const double xtol = 1.0e-12;

  /* Bracket the root */
  int xtrace = (int) fpar[FP_TRACE];

  xrc = rbrac(func_p, &xrl, &xrh, fpar, xtrace);

  if (xrc < 0) {
    return -4;
  }

  /* All set up...should now be able to get the root. */
  xrc = rtsafe(func_p_d, &xq, xrl, xrh, xtol, fpar, xtrace);

  if (xrc < 0) {
    rc = -4;
    sol[3] = 0.0;
  }
  else {
    sol[3] = xq;
    rc = 0;
  }

  fail = 0;
  if (rc != 0) {
    if ( pars.contoprimwarn == 1 ) {
      printf("PrimSolver_1:  ### Solver failed ###");
    }
    fail = 1;
  }

  q = sol[3];
  if (q < 1.e-14) {
    if ( pars.contoprimwarn == 1 ) {
      printf("...q=%e, qi=%e\n",q,qi);
      printf("  x  := %e\n",xpt[1]);
      printf("  y  := %e\n",xpt[2]);
      printf("  z  := %e\n",xpt[3]);
      printf("  Su := (%e, %e, %e)\n",Su[0], Su[1], Su[2]);
      printf("  Bu := (%e, %e, %e)\n",Bx, By, Bz);
      printf("  vu := (%e, %e, %e)\n",u[V_VX],u[V_VY],u[V_VZ]);
      printf("----- Maple Output-------------\n\n");
      printf("  Ssq:=%e; Bsq:=%e; BS:=%e;\n",fpar[FP_SSQ],
                    fpar[FP_BSQ],fpar[FP_BS]);
      printf("  d:=1-((2*q+Bsq)*BS^2+q^2*Ssq)/(q^2*(q+Bsq)^2);\n");
    }
    /* fail = 1; */
    return -1;  /* DN: Simply return if q is too small.
                   Should we apply the floor here instead? */
  }

  vsq = ((2.0*q+Bsq)*BS*BS + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq));

  if (fabs(vsq) >= 1.0) {
    eq1 = q*q*(q+Bsq)*(q+Bsq) - q*q*Ssq - BS*BS*(2.0*q+Bsq);
    if ( pars.contoprimwarn == 1 ) {
      printf("### vsq is unphysical, vsq = %15.12f\n",vsq);
      printf("### q                      = %15.12f\n",q);
      printf("### BS                     = %15.12f\n",BS);
      printf("### BSq                    = %15.12f\n",Bsq);
      printf("### SSq                    = %15.12f\n",Ssq);
      printf("### tau                    = %15.12f\n",tau);
      printf("### D                      = %15.12f\n",D);
      printf("### eq1                    = %15.12f\n",eq1);
      rc = func_p(&F,q,fpar);
      printf("###  f                     = %e\n",F);
    }
    fail = 1;
  }

/*
  double xx = q/D;
  double ff;
  func_ideal_gas(&ff, xx, fpar);
*/

  if ( fail ) {
    return -4;
  } else {
    u[V_RHO] = D*sqrt(1.0 - vsq);
    u[V_P]   = (gamma-1.0)/gamma*(q*(1.0-vsq) - u[V_RHO]);
    u[V_VX]  = (Su[0] + Bx*BS/q)/(Bsq+q);
    u[V_VY]  = (Su[1] + By*BS/q)/(Bsq+q);
    u[V_VZ]  = (Su[2] + Bz*BS/q)/(Bsq+q);
    u[V_BX]  = u[U_BX];
    u[V_BY]  = u[U_BY];
    u[V_BZ]  = u[U_BZ];
    u[V_PSI] = u[U_PSI];

  }

  if (trace) {
    printf("vsq = %20.16e\n",vsq);
    printf("q   = %20.16e\n",q);
    printf("rho = %20.16e\n",u[V_RHO]);
    printf("p   = %20.16e\n",u[V_P]);
    printf("vx  = %20.16e\n",u[V_VX]);
    printf("vy  = %20.16e\n",u[V_VY]);
    printf("vz  = %20.16e\n",u[V_VZ]);
    printf("Bx  = %20.16e, %20.16e\n",u[U_BX],Bx);
    printf("By  = %20.16e, %20.16e\n",u[U_BY],By);
    printf("Bz  = %20.16e, %20.16e\n\n",u[U_BZ],Bz);
    printf("  Su := (%e, %e, %e)\n",Su[0], Su[1], Su[2]);
    printf("### BS                     = %20.16e\n",BS);
    printf("### BSq                    = %20.16e\n",Bsq);
    printf("### SSq                    = %20.16e\n",Ssq);
  }

  return 0;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void MHDWorkVars(double *fpar, double *u, double gd[3][3],
                 double gu[3][3], double *xpt, double gamma)
{

  double kappa;
  double Sd[3], Bu[3], vu[3];
  double vsq, Ssq, Bsq, BS;
  double P     = u[V_P];
  double rho_0 = u[V_RHO];

  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];

  Bu[0] = u[U_BX];
  Bu[1] = u[U_BY];
  Bu[2] = u[U_BZ];

  vu[0] = u[V_VX];
  vu[1] = u[V_VY];
  vu[2] = u[V_VZ];

  Ssq = square_form(Sd, gu);
  Bsq = square_vector(Bu, gd);
  vsq = square_vector(vu, gd);
  //if (vsq >= 1.0) vsq = 0.99999999;
  if (vsq >= 1.0)
  {
    printf("  MHDWorkVars:  The velocity is superluminal.  Generally \n");
    printf("  MHDWorkVars:  we consider this incorrect.   \n");
    printf("  MHDWorkVars:  Currently, we simply set it to 0.99999999 \n");
    printf("  MHDWorkVars:  which seems sort of stupid. \n");
    printf("  MHDWorkVars:       vsq = %25.20e \n", vsq );
    vsq = 0.99999999;
  }


        if ( fpar[FP_COORD_X] == 0.0 && vsq > 0.8 )
        {
          //printf(" MHDWrkV: X=%7.2e, vsq=%15.10e, vx=%15.10e \n",fpar[FP_COORD_X],vsq,vu[0] ) ;
        }


  BS = Sd[0]*Bu[0] + Sd[1]*Bu[1] + Sd[2]*Bu[2];

  fpar[FP_SSQ]   = Ssq;
  fpar[FP_BSQ]   = Bsq;
  fpar[FP_BS]    = BS;
  fpar[FP_D]     = u[U_D];
  fpar[FP_TAU]   = u[U_TAU];
  fpar[FP_GAMMA] = gamma;
  fpar[FP_VSQ]   = vsq;
  fpar[FP_COORD_X] = xpt[1];
  fpar[FP_COORD_Y] = xpt[2];
  fpar[FP_COORD_Z] = xpt[3];
  kappa            = P/pow(rho_0,gamma);
  fpar[FP_KAPPA]   = ( kappa < 1000.0 ) ? kappa : 1000.0;

  /* TRACE is reserved for internal use in the specific solvers */
  fpar[FP_TRACE]   = 0.0;

  fpar[FP_C2PWARN] = pars.contoprimwarn;
  fpar[FP_VACUUM]  = pars.vacuum;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_Isentropic(double *u, double *xpt, double gd[3][3],
                          double gu[3][3], double *fpar)
{

  int    rc;
  double y, yi, vsq, F, hW2;
  double xi;

  double Sd[3], Su[3], Bu[3];

  double gamma = pars.gamma;

  double Ssq = fpar[FP_SSQ];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];
  double kappa = fpar[FP_KAPPA];

  double D     = u[U_D];

  const int ltrace2 = 0;
  int   fail;

  if (ltrace2) {
    printf("MHD_Solver_Isentropic begin...\n");
  }

  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];

  Bu[0] = u[U_BX];
  Bu[1] = u[U_BY];
  Bu[2] = u[U_BZ];

  form_raise_index(  Su, Sd, gu);

  /* An initial guess for rho0 */
  vsq = fpar[FP_VSQ];
  yi = D*sqrt(1.0-vsq);
  //yl = 0.75*D;
  //yh = 2.0*yi;

  xi = yi;

  rc = SolvePrimVarsIsentropic(&xi, u, fpar);

  fail = 0;
  if (rc < 0) {
    if (pars.contoprimwarn == 1)
      printf("Isentropic Solver Failed.  Die here.\n");
    fail = 1;
    return -4;
  }

  y = xi;

  if (y <= 0.0) {
    if (pars.contoprimwarn == 1)
      printf("### Isentropic Solver has unphysical root, y=%20.14e\n",
         y);
  }

  if (y <= 0.0) {
    if ( pars.contoprimwarn == 1 ) {
      printf("### y is unphysical, y= %15.12f\n",y);
      printf("### BS                     = %15.12f\n",BS);
      printf("### BSq                    = %15.12f\n",Bsq);
      printf("### SSq                    = %15.12f\n",Ssq);
      rc = func_p_isentropic(&F, y, fpar);
      printf("###  f                     = %e\n",F);
    }
    fail = 1;
  }

  if ( fail ) {
    if (DIE_ON_C2P_ERRORS) {
      MathDump(fpar);
      exit(-1);
    }
    return -4;
  }
  else {
    if ( y < pars.vacuum ) {
      y = pars.vacuum;
      u[V_P] = pars.vacuum;
    }
    else {
      u[V_RHO] = y;
      u[V_P]   = kappa*pow(y,gamma);
    }

    hW2 = (y + gamma/(gamma-1.0)*u[V_P])*D*D/(y*y);

    /* Note:  The momentum S is stored with index down.  The velocity
     *        components are index up.
     */
    u[V_VX]  = (Su[0] + BS*Bu[0]/hW2)/(hW2 + Bsq);
    u[V_VY]  = (Su[1] + BS*Bu[1]/hW2)/(hW2 + Bsq);
    u[V_VZ]  = (Su[2] + BS*Bu[2]/hW2)/(hW2 + Bsq);

    if (ltrace2) {
      printf(">>> Isentropic solution >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n");
      printf(">>>    rho          = %g\n",u[V_RHO]);
      printf(">>>    vx           = %g\n",u[V_VX]);
      printf(">>>    vy           = %g\n",u[V_VY]);
      printf(">>>    vz           = %g\n",u[V_VZ]);
      printf(">>>    P            = %g\n",u[V_P]);
    }
  }

  return 0;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int SolvePrimVarsIsentropic(double *qa, double *upt, double *fpar)
{

  int rc;
  double q  = *qa;
  const double tol = 1.0e-10;
  const int ltrace = 1;

  double D   = fpar[FP_D];


  /* Bracket the root */
  int trace = 0;
  double rl = 0.1*D;
  double rh = 2.0*(fpar[FP_D] + fpar[FP_TAU]);
  if (ltrace) {
    fprintf(stdout,"@@ SolvePrimVarsIsentropic: ... rl=%g, rh=%g\n",rl,rh);
  }
  rc = rbrac(func_p_isentropic, &rl, &rh, fpar, trace);
  if (rc < 0) {
    if ( pars.contoprimwarn == 1 ) {
      fprintf(stdout,"SolvePrimVarsIsentropicIsentropic: Could not bracket the root for hW^2\n");
      fprintf(stdout,"      rl = %g, rh = %g\n",qa[1],qa[2]);
    }
    rl = 0.1*D;
    rh = 2.0*(fpar[FP_D] + fpar[FP_TAU]);
    trace = 1;
    rc = rbrac(func_p_isentropic, &rl, &rh, fpar, trace);
    return -1;
  }

  /* All set up...should now be able to get the root. */
  rc = rtsafe(func_p_d_isentropic, &q, rl, rh, tol, fpar, trace);
  if (rc < 0) {
    if ( pars.contoprimwarn == 1 ) {
      fprintf(stdout,"SolvePrimVarsIsentropic:  Error in rtsafe\n");
    }
    return -1;
  }

  *qa = q;
  return 0;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func_dis(double *f, double q, double *fpar) {
  double Ssq = fpar[FP_SSQ];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];

  *f = 1.0 - ((2.0*q + Bsq)*BS*BS + q*q*Ssq) / (q*q*(q + Bsq)*(q + Bsq));
  return 1;
}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func_dis_d(double *f, double *df, double q, double *fpar) {
  double Ssq = fpar[FP_SSQ];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];

  *f  = 1.0 - ((2.0*q + Bsq)*BS*BS + q*q*Ssq) / (q*q*(q + Bsq)*(q + Bsq));
  *df = 2.0*(3.0*BS*BS*q*q + 3.0*q*BS*BS*Bsq
       + q*q*q*Ssq + BS*BS*Bsq*Bsq)/pow(q*(q + Bsq),3);

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *  This routine calculates only f, used for bracketing the root
 *   This version of func_p evaluates the entire expression at once
 *
 *-------------------------------------------------------------------------*/
int func_p(double *f, double q, double *fpar) {

  double Ssq   = fpar[FP_SSQ];
  double Bsq   = fpar[FP_BSQ];
  double BS    = fpar[FP_BS];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];

  double dis;

  dis = 1.0 - ((2.0*q + Bsq)*BS*BS + q*q*Ssq) / (q*q*(q + Bsq)*(q + Bsq));
  dis = fabs(dis);

  *f  = -0.5*BS*BS/(q*q)
        + (0.5*Bsq - tau - D + q/gamma)
        + ((gamma-1.0)/gamma*q
        + 0.5*Bsq)*((2.0*q+Bsq)*BS*BS
        + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq))
        + (gamma-1.0)/gamma*D*sqrt(dis);

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func_p_d(double *f, double *df, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double Bsq   = fpar[FP_BSQ];
  double BS    = fpar[FP_BS];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];

  double dis;

  dis = 1.0 - ((2.0*q + Bsq)*BS*BS + q*q*Ssq) / (q*q*(q + Bsq)*(q + Bsq));
  dis = fabs(dis);

  if ( dis < 1.e-14 ) {
     dis = 1.e-14;
/*
   //  FIXME !!!  This solution needs to be investigated.
   // printf(" FUNC_P_DP: dis < 0. q=%g dis=%g\n",q,dis);
   // return -1;
*/
  }

  *f  = -0.5*BS*BS/(q*q)
        + (0.5*Bsq - tau - D + q/gamma)
        + ((gamma-1.0)/gamma*q
        + 0.5*Bsq)*((2.0*q+Bsq)*BS*BS
        + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq))
        + (gamma-1.0)/gamma*D*sqrt(dis);

  *df = BS*BS/(q*q*q) + 1.0/gamma
        + (gamma-1.0)/gamma*((2.0*q+Bsq)*BS*BS + q*q*Ssq)
                          /(q*q*(q+Bsq)*(q+Bsq))
        + 2.0*((gamma-1.0)/gamma*q + 0.5*Bsq
                    - 0.5*(gamma-1.0)/gamma*D/sqrt(dis))
        *(BS*BS + q*Ssq
          - ((2.0*q+Bsq)*BS*BS + q*q*Ssq)
          *(1.0/q + 1.0/(q+Bsq)))/(q*q*(q+Bsq)*(q+Bsq));

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func_p_isentropic(double *f, double rho0, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double Bsq   = fpar[FP_BSQ];
  double BS    = fpar[FP_BS];
  double D     = fpar[FP_D];
  double Gamma = fpar[FP_GAMMA];
  double kappa = fpar[FP_KAPPA];

  double h = rho0 + Gamma/(Gamma - 1.0)*kappa*pow(rho0,Gamma);
  double W = D/rho0;

  *f  = (h*h*W*W + 2.0*h*Bsq + Bsq*Bsq/(W*W))*(W*W - 1.0)
      - (BS*BS*Bsq)/(h*h*W*W*W*W) - Ssq;

  return 1;

}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func_p_d_isentropic(double *f, double *df, double rho0, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double Bsq   = fpar[FP_BSQ];
  double BS    = fpar[FP_BS];
  double D     = fpar[FP_D];
  double g     = fpar[FP_GAMMA];
  double kappa = fpar[FP_KAPPA];

  double P = kappa*pow(rho0,g);
  double h = rho0 + g/(g - 1.0)*P;
  double W = D/rho0;

  /* Maple vars */
  double t2, t5, t7, t8, t10, t11, t12, t17, t21, t22, t23, t24;
  double t29, t31, t46, t47, t51, t52, t53;

  *f  = (h*h*W*W + 2.0*h*Bsq + Bsq*Bsq/(W*W))*(W*W - 1.0)
      - (BS*BS*Bsq)/(h*h*W*W*W*W) - Ssq;

      t2 = 1.0/(g-1.0);
      t5 = P;
      t7 = rho0+g*t2*t5;
      t8 = D*D;
      t10 = rho0*rho0;
      t11 = 1/t10;
      t12 = g*g;
      t17 = 1.0+t12*t2*t5/rho0;
      t21 = t7*t7;
      t22 = t21*t8;
      t23 = t10*rho0;
      t24 = 1/t23;
      t29 = Bsq*Bsq;
      t31 = t29/t8;
      t46 = BS*BS;
      t47 = t46*Bsq;
      t51 = t8*t8;
      t52 = 1/t51;
      t53 = t10*t10;
     *df = (2.0*t7*t8*t11*t17-2.0*t22*t24+0.2E1*t17*Bsq+2.0*t31*rho0)*(t8*t11
-0.1E1)-2.0*(t22*t11+0.2E1*t7*Bsq+t31*t10)*t8*t24+2.0*t47/t21/t7*t52*t53*t17
-4.0*t47/t21*t52*t23;


  return 1;
}

/*-------------------------------------------------------------------------
 *
 * This is based on a solver from hyperXMHD. It uses rescaled variables.
 * It does work with MHD (B!=0), however it does not reproduce simulation
 * results when using other, independent solvers. It should not be used.
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_2(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar)
{
  const double gamma = pars.gamma;

  double Ssq = fpar[FP_SSQ];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];

  double D     = u[U_D];
  double tau   = u[U_TAU];
  double Bx    = u[U_BX];
  double By    = u[U_BY];
  double Bz    = u[U_BZ];

  double q = tau / D;
  double r = Ssq / D;
  double s = Bsq / D;
  double t = BS / pow(D,1.5);

  int trace = fpar[FP_TRACE];

  if (trace) {
    printf("MHD_Solver_2 begin...\n");
  }

  double Sd[3], Su[3];
  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];
  form_raise_index(Su, Sd, gu);

 /* An initial guess for x = hW^2/D = (1 + epsilon + P/rho)*W.  */
  double vsq = fpar[FP_VSQ];
  double W = 1.0/sqrt(1.0 - vsq);
  double x = (1.0 + gamma/(gamma-1.0)*u[V_P]/u[V_RHO])*W;

 /* physical bounds on x */
  double xmin = 0.9*(1.0 + q - s);
  double xmax = 1.1*(2.0 + 2.0*q - s);

 /* reset the initial guess if xi is out of range. */
  if (x < xmin || x > xmax) {
    x = 0.5 * ( xmin + xmax );
  }

  if (trace) {
    printf("initial guess x=%g, rho=%g, gamma=%g, p=%g, vsq=%g\n",x,u[V_RHO],gamma,u[V_P],vsq);
  }

  /* All set up...should now be able to get the root. */
  double xtol = 1.0e-14;
  //printf(">>> calling zbrent\n");
  int xrc = zbrent(func_ideal_gas, &x, xmin, xmax, xtol, fpar);

  if (xrc < 0 || !isfinite(x)) {
    if ( pars.contoprimwarn == 1 ) {
      printf("MHD_Solver_2:  ### ZBRENT solver failed: xrc=%d, x =%g  ###\n",xrc,x);
      printf("   x = %21.15e\n",x);
      printf("   xmin = %g,  xmax = %g\n",xmin,xmax);
      printf("   q = %g\n",x*D);
      printf("   qmin = %g,  qmax = %g\n",xmin*D,xmax*D);
      double ff;
      func_ideal_gas(&ff, x, fpar);
      printf("   f = %g\n",ff);
      MathDump(fpar);
    }
    return -4;
  }

  double iWsq = 1.0 - (x*x*r + (2.0*x+s)*t*t)/(x*x*(x+s)*(x+s));

  if (iWsq < 0.0) {
    printf("MHD_Solver_2:  ### 1/W^2 < 0.  iWsq = %g ###\n",iWsq);
    printf("   x = %21.15e\n",x);
    printf("   xmin = %g,  xmax = %g\n",xmin,xmax);
    printf("   q = %g\n",x*D);
    printf("   qmin = %g,  qmax = %g\n",xmin*D,xmax*D);
    double ff;
    func_ideal_gas(&ff, x, fpar);
    printf("   f = %g\n",ff);
    MathDump(fpar);
    return -4;
  }

  double Wsq  = 1.0/iWsq;
  W = sqrt(Wsq);
  double rho = D/W;
  double eps = (W - 1.0) + x*(1.0 - Wsq)/W + W*(q - s)
             + 0.5*W*((t*t/(x*x)) + s*iWsq);
  double P = (gamma - 1.0)*rho*eps;

  if (rho < pars.vacuum) {
    printf("MHD_Solver_2: ### resetting state to vacuum. x = %g\n",x);
   /* reset the state to vacuum */
    u[V_RHO] = pars.vacuum;
    u[V_VX] = 0.0;
    u[V_VY] = 0.0;
    u[V_VZ] = 0.0;
    u[V_P] = pars.vacuum * pars.vacuum;
    prim_to_con(u, xpt);
    return 0;
  }

  if (P < 0.0) {
    printf("MHD_Solver_2: ### resetting P. P = %g\n",P);
    P = pars.vacuum * pars.vacuum;
  }

  double qq = (rho * (1.0 + eps) + P)*Wsq;

  double vu[3];
  vu[0]  = (Su[0] + Bx*BS/qq)/(Bsq + qq);
  vu[1]  = (Su[1] + By*BS/qq)/(Bsq + qq);
  vu[2]  = (Su[2] + Bz*BS/qq)/(Bsq + qq);
  vsq = square_vector(vu, gd);
  if (vsq >= 1.0) {
    double xvsq = 1.0 - iWsq;
    if (xvsq >= 1.0) {
      printf("MHD_SOLVER_2: problem here. xvsq >= 1. xvsq = %g\n",xvsq);
      exit(2);
    }
   /* rescale the velocity */
    printf("MHD_Solver_2: rescaling the velocity...\n");
    double c = xvsq/vsq;
    vu[0] *= c;
    vu[1] *= c;
    vu[2] *= c;
   /* FIXME: need to recalculate the conserved to be consistent. */
  }

  u[V_RHO] = rho;
  u[V_P]   = P;
  u[V_VX]  = vu[0];
  u[V_VY]  = vu[1];
  u[V_VZ]  = vu[2];
  u[V_BX]  = u[U_BX]; /* note that V_BX is undensitized, which isn't obvious here. */
  u[V_BY]  = u[U_BY];
  u[V_BZ]  = u[U_BZ];
  u[V_PSI] = u[U_PSI];


  if (trace) {
    printf("vsq = %20.16e\n",vsq);
    printf("q   = %20.16e\n",q);
    printf("rho = %20.16e\n",u[V_RHO]);
    printf("p   = %20.16e\n",u[V_P]);
    printf("vx  = %20.16e\n",u[V_VX]);
    printf("vy  = %20.16e\n",u[V_VY]);
    printf("vz  = %20.16e\n",u[V_VZ]);
    printf("Bx  = %20.16e, %20.16e\n",u[U_BX],Bx);
    printf("By  = %20.16e, %20.16e\n",u[U_BY],By);
    printf("Bz  = %20.16e, %20.16e\n\n",u[U_BZ],Bz);
    printf("  Su := (%e, %e, %e)\n",Su[0], Su[1], Su[2]);
    printf("### BS                     = %20.16e\n",BS);
    printf("### BSq                    = %20.16e\n",Bsq);
    printf("### SSq                    = %20.16e\n",Ssq);
  }

  return 0;

}

/*-------------------------------------------------------------------------
 *
 *  Equations for MHD_Solver_2
 *
 *-------------------------------------------------------------------------*/
int func_ideal_gas(double *f, double x, double *fpar)
{
  double D = fpar[FP_D];
  double q = fpar[FP_TAU]/D;
  double r = fpar[FP_SSQ]/D;
  double s = fpar[FP_BSQ]/D;
  double t = fpar[FP_BS]/pow(D,1.5);
  double gamma = fpar[FP_GAMMA];

  //double Wm2 = 1.0 - (r + (2.0/x + s/(x*x))*t*t)/((x+s)*(x+s));
  //double Wm2 = 1.0 - (r*x*x + (2.0*x + s)*t*t)/(x*x*(x+s)*(x+s));

  double iWsq = 1.0 - (x*x*r + (2.0*x + s)*t*t)/(x*x*(x + s)*(x + s));

  if (iWsq < 1.0e-10) {
    *f = -1.0;
    return 0;
  }
  double Wsq  = 1.0/iWsq;
  double W = sqrt(Wsq);
  double rho = D/W;
  double eps = (W - 1.0) + x*(1.0 - Wsq)/W + W*(q - s)
             + 0.5*W*((t*t/(x*x)) + s*iWsq);
  double P = (gamma - 1.0)*rho*eps;

  *f = x - (1.0 + eps + P/rho)*W;



/*
  double W;
  if (Wm2 < 0.0) {
    W = -1.0/sqrt(-Wm2);
  }
  else {
    W = 1.0/sqrt(Wm2);
  }

  double PDm1 = (gamma - 1.0)*(x/W - 1.0) / (gamma * W);

  *f = x - q - 1.0 - PDm1 + s - 0.5*(t*t*D*D + s/(W*W));

*/
  //printf("x=%g, Wm2=%g, W=%g, f=%g\n",x,Wm2,W,*f);

  return 0;
}


#if 1
// MHD_Solver_3 {{{
/*-------------------------------------------------------------------------
 *
 *  This is the most robust and accurate solver. However, it does not
 *  include a magnetic field.  So B = 0 only for this solver.
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_3(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar)
{

  double gamma = pars.gamma;

  double Ssq   = fpar[FP_SSQ];
  double D     = u[U_D];
  double tau   = u[U_TAU];


  double a = tau + D;
  double ql = sqrt(Ssq)/a;
  ql = fmax(D/a, ql);
  double t1 = 0.5 * gamma;
  double t2 = t1*t1 - (gamma - 1.0)*Ssq/(a*a);
  if (t2 < 0.0) {
    printf("MHD_Solver_4:  problem with upper bound. t2 < 0. t2 = %g\n",t2);
    exit(2);
  }
  double qh = t1 + sqrt(t2);
  double qi = 0.5*(ql + qh);

  double q = qi;

  double ql0 = ql;
  double qh0 = qh;
  double q0 = q;

  int xtrace = 0;
  double xtol = 1.0e-14;

/*
  if (!isfinite(qh)) {
    printf("  qh is not finite.\n");
    printf("    a  = %g, Ssq = %g\n",a, Ssq);
    printf("    t1 = %g, t2  = %g\n",t1, t2);
  }
*/
  assert(isfinite(ql)==1);
  assert(isfinite(qh)==1);

  int xrc = rtsafe(func4_p_d, &q, ql, qh, xtol, fpar, xtrace);

  if (xrc < 0) {
    ql = ql0;
    qh = qh0;
    q = q0;
    xtrace = 1;
    xrc = rtsafe(func4_p_d, &q, ql, qh, xtol, fpar, xtrace);
    exit(2);
    return -4;
  }

  q *= a;
  double vsq = Ssq /(q*q);

  double Sd[3], Su[3];
  Sd[0] = u[U_SX];
  Sd[1] = u[U_SY];
  Sd[2] = u[U_SZ];
  form_raise_index(Su, Sd, gu);

  u[V_RHO] = D*sqrt(1.0 - vsq);
  u[V_P]   = (gamma-1.0)/gamma*(q*(1.0-vsq) - u[V_RHO]);
  u[V_VX]  = Su[0] / q;
  u[V_VY]  = Su[1] / q;
  u[V_VZ]  = Su[2] / q;

  return 0;
}
//}}}
#endif


/*-------------------------------------------------------------------------
 *
 *  This is the most robust and accurate solver. However, it does not
 *  include a magnetic field.  So B = 0 only for this solver.
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_4(double *u, double *xpt, double gd[3][3], double gu[3][3],
                 double *fpar)
{
  const int ltrace = 0;

  double gamma = pars.gamma;

  double Ssq   = fpar[FP_SSQ];
  double D     = u[U_D];
  double tau   = u[U_TAU];
  double Sd[3] ;

  Sd[0] = u[U_SX] ;
  Sd[1] = u[U_SY] ;
  Sd[2] = u[U_SZ] ;

  int j ;
  double qf_1, qf_2, qg_2, qf_max ;
  double ql, qh, f_at_qh, f_at_qh_v2;
  double f_at_ql = 0.0 ;
  double rescale_factor1 ;
  double a = tau + D;
  double t1 = 0.5 * gamma;
  double beta_sq = Ssq/(a*a) ;
  double t2 = t1*t1 - (gamma-1.0)*beta_sq ;
  // t2 shows up under a square root, so we check to see if it is < 0.
  if (t2 < 0.0)
  {
    printf("MHD_Solver_4:  problem with upper bound. t2 < 0. t2 = %g\n",t2);
    exit(2);
  }
  double sqrt_t2 = sqrt(t2) ;
  qf_1 = t1 - sqrt_t2 ; // the smaller root of f
  qf_2 = t1 + sqrt_t2 ; // the larger root of f
  qf_max = t1 ;         // the location of the max of f
  qg_2 = sqrt(beta_sq); // the larger root of g

  if ( fabs( (qg_2 - qf_1) / (qg_2 + qf_1 ) ) < 1.e-6 )
  {
    if (ltrace) {
      printf("  MHD_Solver_4:  Roots are colliding near q=0. \n");
      printf("  MHD_Solver_4:  Adding a 1.e-5 piece to the larger \n");
      printf("  MHD_Solver_4:  hoping this resolves the problem. \n");
    }
    qg_2 += 1.e-5 ;
  }

  // Set the lower bound to qg_2 unless qg_2 is smaller than the location of
  // the maximum of f.
  ql = fmax( qg_2, qf_max ) ;


  //  This temporarily initializes the upper bound to a possible value.
  //  The following code checks this to make sure it works.
  qh = qf_2 ;


  /* {{{ ... documentation on the cases that need to be considered ...
  We consider various possibilities for the relative location of qg_2 and
  qf_2 as they should be the lower and upper bounds, respectively, for our
  root, q, of F(q).

  Case 1 is that qg_2 < qf_2 and a physical inequality is obeyed.  All is well
  in this case and the Newton solve should proceed without issue.

  Case 2 considers the violation of another physical inequality and adjusts
  (rescales) the components of S in order to enforce the inequality.

  Case 3 considers the numerical "vanishing" of D/a relative to beta_sq which
  can also numerically violate physical inequalities.

  Case 4 considers the (seemingly remote but nonetheless realized) possibility
  that qg_2 and qf_2 are exactly the same (to machine precision).

  No claim is made that these cases are exhaustive.  They were simply ones
  that arose in debugging the code and trying to make this primitive solver as
  robust as possible.  There is a sort of "ad hoc-ness" to some of these.
  Could they be improved?  Probably.  But for now they seem to work.
  }}}  */
  // CASE 1
  if ( qg_2 < qf_2  &&  beta_sq < 1.0 )
  {
    qh = qf_2 ;
  }
  // CASE 2
  else if ( beta_sq + (D/a)*(D/a) - 1.0 >= 0.0 )
  {
    if (ltrace) 
    {
      printf("  MHD_Solver_4:  Our bounds are colliding near q=1.  \n");
      printf("  MHD_Solver_4:  In particular, we have an inequality \n");
      printf("  MHD_Solver_4:  violation.  In particular, Ssq/a^2 + D^2/a^2\n");
      printf("  MHD_Solver_4:  should be less than 1.  Instead it is \n") ;
      printf("  MHD_Solver_4:   Ssq/a^2+D^2/a^2=%35.30e\n",beta_sq+(D/a)*(D/a));
//{{{
#if 0
    printf("  MHD_Solver_4:  where:     \n" );
    printf("  MHD_Solver_4:      Ssq = %25.20e \n", Ssq );
    printf("  MHD_Solver_4:      a^2 = %25.20e \n", a*a );
    printf("  MHD_Solver_4:      D^2 = %25.20e \n", D*D );
    printf("  MHD_Solver_4:      tau = %25.20e \n", tau );
    printf("  MHD_Solver_4:       D  = %25.20e \n", D );
    printf("     \n");
    printf("  MHD_Solver_4:  We will rescale S_i and Ssq in hopes this \n");
    printf("  MHD_Solver_4:  resolves the problem. \n");
#endif
//}}}
    }

    /*
    We rescale the components of Sd (and hence Ssq) in order that
    the inequality is enforced.

    FIXME:  Note that this scaling factor is a bit arbitrary.  It would
    FIXME:  be well to experiment with the value added in.
    */
    //double  eps0 = 1.e-14 ;
    //rescale_factor1 = 1.0 / (sqrt(beta_sq + (D/a)*(D/a)) + eps0) ;
    rescale_factor1 = 1.0 / (sqrt(beta_sq + (D/a)*(D/a)) + 1.e-14) ;

    // Now multiply Sd by the scaling factor.
    for ( j=0; j<3; j++ )
    { Sd[j] *= rescale_factor1 ; }

    // Recalculate Ssq.
    Ssq = square_form(Sd, gu);

    // Now we have to recalculate the roots of f and g with this new Ssq.
    beta_sq = Ssq/(a*a);
    t2 = t1*t1 - (gamma-1.0)*beta_sq ;
    if (t2 < 0.0) { printf("MHD_Solver_4: t2 < 0, i.e. t2=%25.20e\n",t2); }
    qf_2 = t1 + sqrt(t2) ; // new root of f; upper bound on q
    qg_2 = sqrt(beta_sq) ; // new root of g; lower bound on q

    // Check that these new roots are indeed good upper and lower bounds.
    if ( qg_2 < qf_2 )
    {
      ql = qg_2 ;
      qh = qf_2 ;
    }
    else
//{{{
    {
      if (ltrace) {
        printf("  MHD_Solver_4:  Even after rescaling, we still have a \n");
        printf("  MHD_Solver_4:  problem with the relative values of the \n");
        printf("  MHD_Solver_4:  bounds qg_2 and qf_2.  In particular, we n");
        printf("  MHD_Solver_4:  should have that  qg_2 < qf_2. Instead, we\n");
        printf("  MHD_Solver_4:  have:  \n");
        printf("  MHD_Solver_4:      qg_2 = %25.20e \n", qg_2 );
        printf("  MHD_Solver_4:      qf_2 = %25.20e \n", qf_2 );
      }
    }
//}}}

    // Because we made a rescaling of the components of S and of Ssq, we need
    // to save these.
    u[U_SX] = Sd[0] ;
    u[U_SY] = Sd[1] ;
    u[U_SZ] = Sd[2] ;
    fpar[FP_SSQ] = Ssq;

    //{{{ Having rescaled, check again that the inequality that failed before is now satisfied.
    if ( beta_sq + (D/a)*(D/a) >= 1.0 )
    {
      if (ltrace) {
        printf("  MHD_Solver_4:  Even after rescaling S_i the physical \n");
        printf("  MHD_Solver_4:  inequality that should be satisfied is not\n");
        printf("  MHD_Solver_4:  In particular, \n");
        printf("  MHD_Solver_4:   (Ssq+D^2)/a^2=%35.30e\n",beta_sq+(D/a)*(D/a));
      }
    }
    else
    {
      if (ltrace) {
        printf("  MHD_Solver_4:  After what appears to be a successful \n");
        printf("  MHD_Solver_4:  rescaling, we have  \n");
        printf("  MHD_Solver_4:   (Ssq+D^2)/a^2=%35.30e \n",beta_sq+(D/a)*(D/a));
      }
    }
//}}}
  }

  // CASE 3
  else if ( fabs( (beta_sq + (D/a)*(D/a)) - beta_sq ) <= 6.0e-16 )
  {
 //{{{
    printf("  MHD_Solver_4:  SURPRISE! I deleted the treatment of this \n");
    printf("  MHD_Solver_4:  possibility as I didn't think it ever arose. \n");
    printf("  MHD_Solver_4:  I guess it does after all. \n");
/*
    printf("  MHD_Solver_4:  D/a is so small as to be lost in roundoff.\n");
    printf("  MHD_Solver_4:  We will adjust Ssq in hopes this \n");
    printf("  MHD_Solver_4:  resolves the problem. \n");

    //Instead of a true (multiplicative) rescaling, in this case, we have
    //found that simply subtracting a small amount from each of the
    //components of S can bring Ssq back into the physical regime.
    //  FIXME:  Again, the value 1.e-15 seems a bit arbitrary.
    //  FIXME:  It might be well to play with this some.
    //  FIXME:  Actually, this could be a real problem.  If Sd is already negative, we could be making the magnitude, Ssq, in fact larger.  this is definitely a potential problem and should be addressed ...
    for ( j=0; j<3; j++ ) { Sd[j] -= 1.e-15 * a ; }

    // Recalculate Ssq.
    Ssq = square_form(Sd, gu);

    // Now we have to recalculate the roots of f and g with this new Ssq.
    beta_sq = Ssq/(a*a);
    t2 = 1.0 - (gamma-1.0)*beta_sq/(t1*t1) ;
    if (t2 < 0.0) { printf("MHD_Solver_4: t2 < 0, i.e. t2=%25.20e\n",t2); }
    qf_2 = t1*( 1.0 + sqrt(t2)) ; // new root of f; upper bound on q
    qg_2 = sqrt(beta_sq) ;        // new root of g; lower bound on q

    // Check that these new roots are indeed good upper and lower bounds.
    if ( qg_2 < qf_2 )
    {
      ql = qg_2 ;
      qh = qf_2 ;
    }
    else
    {
      printf("  MHD_Solver_4:  Even after adjusting (via subtracting) \n") ;
      printf("  MHD_Solver_4:  the components of S_i, we still have a \n");
      printf("  MHD_Solver_4:  problem with the relative values of the \n");
      printf("  MHD_Solver_4:  bounds  qg_2  and  qf_2.  In particular,\n");
      printf("  MHD_Solver_4:  we should have  qg_2 < qf_2.  Instead, we \n");
      printf("  MHD_Solver_4:  have \n");
      printf("  MHD_Solver_4:      qg_2 = %25.20e \n", qg_2 );
      printf("  MHD_Solver_4:      qf_2 = %25.20e \n", qf_2 );
    }

    // Because we made a change to the S components and Ssq, we need to save
    // them.
    u[U_SX] = Sd[0] ;
    u[U_SY] = Sd[1] ;
    u[U_SZ] = Sd[2] ;
    fpar[FP_SSQ] = Ssq;

    // Having modified the conserved variables to force the inequality
    // to be satisfied, let's check that that actually happened.
    if ( fabs( (beta_sq + (D/a)*(D/a)) - beta_sq ) <= 6.0e-16 )
    {
      printf("  MHD_Solver_4:  On adjusting componets of S, we still \n");
      printf("  MHD_Solver_4:  have a problem with D being too small: \n");
      printf("  MHD_Solver_4:      fabs( (beta_sq + (D/a)*(D/a)) - beta_sq ) = %25.20e \n", fabs((beta_sq+(D/a)*(D/a)) - beta_sq) );
      printf("  MHD_Solver_4:      Ssq/a^2 + D^2/a^2 = %35.30e \n",beta_sq+(D/a)*(D/a));
    }
*/

//}}}
  }

  // CASE 4
  else if ( qg_2 == qf_2 )
  {
    qg_2 -= 1.e-16 ;
    qf_2 += 1.e-16 ;

    ql = qg_2 ;
    qh = qf_2 ;
  }
  else
  { //{{{
  // FIXME:
  // This else is effectively a garbage dump for all the other possibilities
  // not delineated in the if/else above.  It is a sort of "none of the
  // above".  As a result, if we do get here, we essentially don't know why ...
    printf("  MHD_Solver_4:   something unexpected (1) \n");
    printf("        qg_2 = %25.20e \n", qg_2 );
    printf("        qf_2 = %25.20e \n", qf_2 );
    printf("     beta_sq = %25.20e \n", beta_sq );
    printf("        D    = %25.20e \n", D );
    printf("        tau  = %25.20e \n", tau );
    printf("        Ssq  = %25.20e \n", Ssq );
    printf("    beta_sq + (D/a)^2 = %25.20e \n", beta_sq+(D/a)*(D/a) );
    printf("   dump fpar  \n");
    printf("      fpar[FP_SSQ]     = %25.20e \n", fpar[FP_SSQ] );
    printf("      fpar[FP_BSQ]     = %25.20e \n", fpar[FP_BSQ] );
    printf("      fpar[FP_BS ]     = %25.20e \n", fpar[FP_BS ] );
    printf("      fpar[FP_D  ]     = %25.20e \n", fpar[FP_D  ] );
    printf("      fpar[FP_TAU]     = %25.20e \n", fpar[FP_TAU] );
    printf("      fpar[FP_GAMMA]   = %25.20e \n", fpar[FP_GAMMA] );
    printf("      fpar[FP_VSQ]     = %25.20e \n", fpar[FP_VSQ] );
    printf("      fpar[FP_COORD_X] = %25.20e \n", fpar[FP_COORD_X] );
    printf("      fpar[FP_COORD_Y] = %25.20e \n", fpar[FP_COORD_Y] );
    printf("      fpar[FP_COORD_Z] = %25.20e \n", fpar[FP_COORD_Z] );
    printf("      fpar[FP_INDEX_X] = %25.20e \n", fpar[FP_INDEX_X] );
  }  //}}}


  // At this point, we should have the two values, namely qg_2 and qf_2,
  // that should form a legitimate bracket for finding the root of F(q).
  // Let's check.
  //
  // We want to check that at ql, the function F(q) := f(q)-(const)*sqrt(g(q))
  // is positive.  First just calculate F(ql).
  int rc=0;
  rc = func4_p( &f_at_ql, ql, fpar );
  if (rc < 0)
  {
    printf(" MHD_Solver_4:  Something's amiss with ql  \n");
    printf(" MHD_Solver_4:  We couldn't calculate f(ql). \n");
  }

  // Now check that at qh, the function F(q) := f(q)-(const)*sqrt(g(q))
  // is negative.  For now, just calculate F(qh).
  rc = 0 ;
  rc = func4_p( &f_at_qh, qh, fpar );
  if (rc < 0)
  {
    printf(" MHD_Solver_4:  Something's amiss with qh \n");
    printf(" MHD_Solver_4:  We couldn't calculate f(qh). \n");
  }

  // If, indeed, F(ql) > 0 and F(qh) < 0, then we should have a
  // bracket for the root.  We will check for the failure of this and do
  // what we can.
  if ( f_at_ql < 0.0 || f_at_qh > 0.0 )
  {
    if (ltrace) {
      printf("  MHD_Solver_4:  There is a problem with the bracketing of \n");
      printf("  MHD_Solver_4:  the root of F(q).    \n");
      printf("  MHD_Solver_4:         ql = %25.20e \n",ql);
      printf("  MHD_Solver_4:    f_at_ql = %25.20e should be > 0.0\n",f_at_ql);
      printf("  MHD_Solver_4:         qh = %25.20e \n",qh);
      printf("  MHD_Solver_4:    f_at_qh = %25.20e should be < 0.0\n",f_at_qh);
      printf("  MHD_Solver_4:  We are sort of at a loss at this point.  We \n");
      printf("  MHD_Solver_4: will go on, but without much hope of success.\n");
    }
  }

  if ( f_at_ql > 0.0  &&  f_at_qh > 0.0 )
  {
    if (ltrace) {
      printf("  MHD_Solver_4:  At the lower and upper bounds, the \n");
      printf("  MHD_Solver_4:  function is positive so we fail to \n");
      printf("  MHD_Solver_4:  get a bracket at all.  Needless to \n");
      printf("  MHD_Solver_4:  say, this is bad and may mean no   \n");
      printf("  MHD_Solver_4:  solution exists.                   \n");
      printf("  MHD_Solver_4:  Optimistically, it may meant that  \n");
      printf("  MHD_Solver_4:  the upper bound is not large enough. \n");
      printf("  MHD_Solver_4:  We will try to shift the current value \n");
      printf("  MHD_Solver_4:  of qh (which is %25.20e) up by 6.e-16, i.e.\n",qh);
    }

    qh += 6.e-16 ;

    if (ltrace) {
      printf("  MHD_Solver_4:      qh = %25.20e \n",qh );
      printf("  MHD_Solver_4:  Checking to see if that worked ... \n");
    }

    rc = func4_p( &f_at_qh_v2, qh, fpar ) ;

    if (ltrace) {
      printf("  MHD_Solver_4:  ... well, among other things, the new \n");
      printf("  MHD_Solver_4:  value of f (which should now be negative) \n");
      printf("  MHD_Solver_4:  at the new value of qh is: \n");
      printf("  MHD_Solver_4:    old      qh = %25.20e \n",qh-6.e-16);
      printf("  MHD_Solver_4:    old f_at_qh = %25.20e \n",f_at_qh);
      printf("  MHD_Solver_4:    new      qh = %25.20e \n",qh);
      printf("  MHD_Solver_4:    new f_at_qh = %25.20e (should be neg) \n", f_at_qh_v2);
    }
  }


  /*
  At this point, we should have a legitimate bracket for our root, namely
  [ql, qh] := [qg_2, qf_2].  We should now be able to proceed with the
  Newton solve.

  As our initial guess for the root, take the midpoint of the bracket.
  */
  double qi = 0.5*(ql + qh);
  double q = qi;

  //  Incorporate a little redundancy in the event things go bad.
  double ql0 = ql;
  double qh0 = qh;
  double q0 = q;

  //  Set trace parameter for rtsafe to 0 (no tracing), set the tolerance for
  //  the Newton solve, and call rtsafe to get the root, q.
  int xtrace = 0;
  double xtol = 1.0e-14;
  int xrc = rtsafe(func4_p_d, &q, ql, qh, xtol, fpar, xtrace);

  // In the event of failure, a postmortem ...
  if (xrc < 0)
  {
    printf("  MHD_Solver_4:  The Newton solve (rtsafe) failed.\n");
    printf("  MHD_Solver_4:  Current values include  \n");
    printf("    ql = %25.20e \n",ql);
    printf("    qh = %25.20e \n",qh);
    printf("    qi = %25.20e \n",qi);
    printf("    q   =%25.20e \n",q);
    printf("    ql0 = %25.20e \n",ql0);
    printf("    qh0 = %25.20e \n",qh0);
    printf("    q0   =%25.20e \n",q0);
    printf("  MHD_Solver_4:  Some conserved quantities and  \n");
    printf("  MHD_Solver_4:  inequalities,  \n");
    printf("    D               = %25.20e \n", D);
    printf("    tau+D           = %25.20e \n", a);
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));
    printf("  MHD_Solver_4:  After rtsafe failure ... redoing ... \n");

    // Retrying rtsafe but with a different trace parameter so as to get
    // some more info out of rtsafe.
    ql = ql0;
    qh = qh0;
    q = q0;

    xtrace = 1;
    xrc = rtsafe(func4_p_d, &q, ql, qh, xtol, fpar, xtrace);
    exit(2);
    return -4;
  }


  //  If we get to this point we may actually have a solution.
  //  We have to rescale back by a = tau+D.
  q *= a;

  // Knowing the value for q (:= hW^2, i.e. related to the enthalpy), we
  // can now get the remaining primitive variables.  Start with the velocity
  // squared:  v^2.
  double vsq = Ssq / (q*q);
  //  We have the "down" components of S; now get the "up" components
  double Su[3];
  form_raise_index(Su, Sd, gu);

  //  Now calculate rho, pressure, and the "up" components of v^i.
  u[V_RHO] = D*sqrt(1.0 - vsq);
  u[V_P]   = (gamma-1.0)/gamma*(q*(1.0-vsq) - u[V_RHO]);
  u[V_VX]  = Su[0] / q;
  u[V_VY]  = Su[1] / q;
  u[V_VZ]  = Su[2] / q;


/*
  if (u[V_RHO] < 0.0) {
    printf("vsq=%g, Ssq=%g, q=%g, rho=%g, P=%g\n",vsq,Ssq,q,u[V_RHO],u[V_P]);
    exit(2);
  }
  if (u[V_P] < 0.0) {
    printf("vsq=%g, Ssq=%g, q=%g, rho=%g, P=%g\n",vsq,Ssq,q,u[V_RHO],u[V_P]);
    exit(2);
  }
*/
  


  if ( u[V_RHO] < 0.0 )
  {
    if (ltrace)
      printf("negative prim values:  rho = %15.10e \n", u[V_RHO] );

    u[V_RHO] = pars.vacuum;
  }
  if ( u[V_P] < 0.0 )
  {
    if (ltrace)
      printf("negative prim values:   P  = %15.10e \n", u[V_P]);

    u[V_P] = pars.vacuum;
  }
  assert( D > 0.0 );

  return 0;

}



/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func4_p(double *f, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];

  double dis;

  double a = tau+D;
  dis = q*q - Ssq/(a*a);
  if ( dis < 1.e-18 ) {
     dis = 1.e-18;
  }

  *f  = -(q - 0.5*gamma)*(q - 0.5*gamma) + gamma*gamma/4.0
      - (gamma-1.0)*Ssq/(a*a) - (gamma-1.0)*D/a*sqrt(dis);

  return 1;
}



/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func4_p_d(double *f, double *df, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];

  double dis;

  double a = tau+D;
  dis = q*q - Ssq/(a*a);
  if ( dis < 1.e-18 ) {
     dis = 1.e-18;
  }

  *f  = - (q - 0.5*gamma)*(q - 0.5*gamma) + gamma*gamma/4.0
        - (gamma-1.0)*Ssq/(a*a) - (gamma-1.0)*D/a*sqrt(dis);

  *df = -2.0*(q - 0.5*gamma) - (gamma-1.0)*D/a*q/sqrt(dis);

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
int func4_p_d_trial(double *f, double *df, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];

  double dis;

  double a = tau+D;
  dis = q*q - Ssq/(a*a);


/*
  double  dis2=dis;
  if ( dis < 0.0  &&  dis > -6.e-16 )
  {
    printf("  func4_p_d:  The argument of a square root is negative. \n");
    printf("  func4_p_d:      dis = %25.20e \n", dis );
    dis2 = dis;
  }
*/

  if ( dis < 1.e-18 ) {
     dis = 1.e-18;
  }

  *f  = -(q - 0.5*gamma)*(q - 0.5*gamma) + gamma*gamma/4.0
      - (gamma-1.0)*Ssq/(a*a) - (gamma-1.0)*D/a*sqrt(dis);

  *df = -2.0*(q - 0.5*gamma) - (gamma-1.0)*D/a*q/sqrt(dis);



/*
  if ( dis2 < 0.0  &&  dis2 > -6.e-16 )
  {
    printf("  func4_p_d:  Since had a proglem, on correction, get \n");
    printf("  func4_p_d:      q = %25.20e \n", q );
    printf("  func4_p_d:      a = %25.20e \n", a );
    printf("  func4_p_d:    Ssq = %25.20e \n", Ssq );
    printf("  func4_p_d:      D = %25.20e \n", D );
    printf("  func4_p_d:  gamma = %25.20e \n", gamma );
    printf("  func4_p_d:      X = %25.20e \n", fpar[FP_COORD_X]);
    printf("  func4_p_d:      f = %25.20e \n", *f );
    printf("  func4_p_d:     df = %25.20e \n", *df );
  }
*/

  return 1;
}







/*-------------------------------------------------------------------------
 *
 *  This aspires to be a more robust and accurate solver for the primitive 
 *  variables that includes a magnetic field.  So B != 0 for this solver. 
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_5(  double *u        , 
                   double *xpt      , 
                   double  gd[3][3] , 
                   double  gu[3][3] ,
                   double *fpar        )
{
  const int ltrace = 1;

  double  gamma = pars.gamma;

  double  D    = u[U_D];
  double  tau  = u[U_TAU];
  double  Bx   = u[U_BX];
  double  By   = u[U_BY];
  double  Bz   = u[U_BZ];
 
  double Sd[3] ; 
  Sd[0] = u[U_SX] ; 
  Sd[1] = u[U_SY] ; 
  Sd[2] = u[U_SZ] ; 
 
  double  Ssq  = fpar[FP_SSQ];
  double  Bsq  = fpar[FP_BSQ];
  double  BS   = fpar[FP_BS ];
  
  double  qg ; 
  double  qg_l  =0.0, qg_h  =0.0, qg_i  =0.0 ;
  double  qg_l_0=0.0, qg_h_0=0.0, qg_i_0=0.0 ; 
  
  double  qf ; 
  double  qf_l  , qf_h  , qf_i   ; 
  double  qf_l_0, qf_h_0, qf_i_0 ; 
  
  double  ql, qh, qi, q ;
  
  double  qg_2, qf_2 ; 

  int rc = 0 ;

  int gtrace; 
  int grc = 0 ; 
  double  gtol = 1.0e-14; //FIXME these tolerances seem large compared to the 6.e-16  used elsewhere in the code  
  
  int ftrace ; 
  int frc = 0 ; 
  double  ftol = 1.0e-14;

  double  F_at_ql, F_at_qh;
  
  
  /* {{{  Some documentation ... 
  The equation whose root we are to solve is of the form  
         f(q) = c0*(q+c1) * sqrt(g(q)).    
  where f(q) is a quartic polynomial in q, the c's are constants wrt q and 
  g(q) is another quartic polynomial in q.  An alternative form for the 
  equation to which we will refer is simply
         F(q) := f(q) - c0*(q+c1)*sqrt(g(q))  
  for which we find the root F(q) = 0.

  Our task is complicated by the relatively high order polynomials we have
  and the presence of the square root.  It would be nice to be able to 
  prove that there is a solution that exists and is unique.  In the B=0 (no
  magnetic field) case, we can do that.  Unfortunately, there doesn't seem 
  to be a proof of that for the B != 0 case though there is some numerical 
  evidence that there is a solution most of the time.  
  
  There are some things one can say about the system.  One can show that 
  there is a single, positive, first order root of g(q) (except in a special
  case that we will have to handle specially).  There could, of course, be 
  other roots, but these will be negative roots and we are not interested in 
  them as they are smaller than the single, positive root.  We will call 
  this root qg_2 (as we do in MHD_Solver_4 for the no-B 
  field case).  It can possibly serve as a lower bound on the bracket for 
  the root of F(q).  
  
  The upper bound on the bracket for the root of F(q) will be given by the 
  largest of the two positive roots of the quartic f(q).  One can show that 
  f(q) will have at most two positive roots.  (It could be the case that 
  these degenerate to a single double root or a complex pair, but we will 
  hope this doesn't happen too often in practice.  --famous last words.)  
  
  To find these bounds (or a bracket) on the root of F(q), we will have to 
  solve for largest possible root of the quartic g(q) and for the two  
  positive roots of the quartic f(q).  We will do both numerically.  The two 
  positive roots fo f(q) we will call qf_1 and qf_2 (as in the no-B field 
  case in MHD_Solver_4).      
  
  Our expectation will be that qf_1 < qf_2 and that together, 
  qf_1 < qg_2 < qf_2.  However, we will also be aware that we have no 
  analytic proof that this will be true for all values of the parameters.  
   
  Thus we must solve two root finding problems to set the bracket for 
  the actual root of the root finding problem we actually want to do, namely 
  F(q)=0.   
 
  We will assume that we have good (i.e. physical) conserved variables
  coming into this routine.  But we will have to check this, sometimes more
  than once.  To that end, we do have several inequalities that must be 
  satisfied.  These we will check to ensure that the conserved variables
  take values in the physically allowable regime.  

  Another challenge with respect to this system is that there are several 
  "parameters" which enter the equation and solution.  These are, of course,
  the conserved variables.  Sometimes, such as in the vacuum region of 
  a star, some of these variables vanish, e.g. the velocity.  In such 
  cases the equation can change form and the order of the polynomials can
  be reduced (a simple example is that both quartics become effective 
  quadratics when B=0).  We have to handle these cases separately.  
  
  The FIRST such case is when the velocity vector is the zero vector:  
  v^2=0.  This, in turn implies S^2=0 and BS=0.  In this case,
  there is an analytic solution to the entire problem, namely 
       hW^2 = gamma*(tau - B^2/2) + D
 
  The SECOND case is when BS=0 but the velocity is nonzero.  This happens
  if Bv=0 or v_parallel vanishes, i.e. the component of the velocity 
  parallel to the magnetic field vanishes and the velocity is strictly normal 
  to the magnetic field.  This is equivalent to S_parallel (=BS/|B|) 
  vanishing.  In terms of our parameters (rescaled conserved variables) 
  this says that beta_sq = xi_sq.  In this case, the (originally quartic) 
  polynomial g(q) takes the form 
      g(q) = q^2 [ (q+2*alpha^2)^2 - xi^2 ]
  and the polynomial f(q) develops a zero at q=0 as well.  Hence this zero 
  at q=0 can be canceled out of F(q) and we can think of f as being a cubic 
  and g a quadratic.  
 
  The THIRD case is if v^2 is nonzero but v_perp vanishes, i.e. the 
  velocity and magnetic field are parallel and the velocity lies completely
  along the direction of the magnetic field.  This implies that S_perp
  vanishes and xi_sq = 0.  In this case, the problem simplifies to a form
  almost identical to the pure fluid case, namely f and g are both quadratics,
  but now with slightly changed coefficients (to accommodate the B field).
  }}}  */ 
  
  /*
  We define some quantities and check two basic inequalities the conserved
  quantities must satisfy. 
  */
  double  a = tau + D ; 
  double  a_sq = a*a ; 
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq = Ssq / a_sq ; 
  double  epsilon_sq = (BS*BS) / (Bsq * a_sq) ; 
  double  xi_sq = beta_sq - epsilon_sq ; 

  /* 
  Check the inequality tau+D-0.5*Bsq > 0 [i.e. alpha_sq = 0.5*Bsq/(tau+D) < 1]
  */
  if ( alpha_sq > 1.0 ) 
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5:  The magnetic field is unphysical wrt the \n");
    printf("  MHD_Solver_5:  ideal MHD approx.  \n");
//{{{  if inequality not satisfied, dump state and do a maple dump    
#if 1
    printf("  MHD_Solver_5:             Bsq = %25.20e \n", Bsq ); 
    printf("  MHD_Solver_5:             tau = %25.20e \n", tau ); 
    printf("  MHD_Solver_5:              D  = %25.20e \n", D ); 
    printf("  MHD_Solver_5:     tau+D-Bsq/2 = %25.20e \n", a-0.5*Bsq ); 
    printf("  MHD_Solver_5:        COORD_X  = %25.20e \n", fpar[FP_COORD_X]); 
    printf("  MHD_Solver_5:        COORD_Y  = %25.20e \n", fpar[FP_COORD_Y]); 
    printf("  MHD_Solver_5:        COORD_Z  = %25.20e \n", fpar[FP_COORD_Z]); 
    printf("  MHD_Solver_5:          Ssq = %25.20e \n", Ssq); 
    printf("  MHD_Solver_5:          Bsq = %25.20e \n", Bsq); 
    printf("  MHD_Solver_5:           BS = %25.20e \n", BS); 
    printf("  MHD_Solver_5:     alpha_sq = %25.20e \n", alpha_sq); 
    printf("  MHD_Solver_5:      beta_sq = %25.20e \n", beta_sq); 
    printf("  MHD_Solver_5:        xi_sq = %25.20e \n", xi_sq); 
    printf("  MHD_Solver_5:  tau+D-Bsq/2 = %25.20e \n", tau+D-0.5*Bsq); 
    printf("  MHD_Solver_5:===== MAPLE DUMP ===============================\n");
    printf("   restart: with(plots): \n");  
    printf("   alpha_sq := %25.20e ; \n", alpha_sq );  
    printf("   beta_sq  := %25.20e ; \n", beta_sq  );  
    printf("     xi_sq  := %25.20e ; \n", xi_sq    );  
    printf("   gamma1   := %25.20e ; \n", gamma    );  
    printf("   delta1   := %25.20e ; \n", D/a      );  
    printf("   f(q) := (q+2.0*alpha_sq)                                  \n"); 
    printf("    * ( (q+2.0*alpha_sq) * ( -  (q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("                               *(q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("                             +  (0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("                               *(0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("                             -  (gamma1-1.0)*beta_sq         \n");
    printf("                           )                                 \n");
    printf("      + (3.0*gamma1-4.0)*alpha_sq*xi_sq  )                   \n");
    printf("    - 2.0*(gamma1-2.0)*alpha_sq*alpha_sq*xi_sq      ;        \n");
    printf("   plot(f(q), q=-0.5..1) ; \n");  
    printf("   g(q) :=  q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq)          \n");
    printf("         - q*q * xi_sq                                      \n");
    printf("         - (q+2.0*alpha_sq)*(q+2.0*alpha_sq)*(beta_sq-xi_sq);\n");
    printf("   g_1(q) :=  (gamma1-1.)*delta1*(q+2.0*alpha_sq)*sqrt(g(q));\n");
    printf("   plot(g(q), q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g(q)}, q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g_1(q)}, q=-0.5..1) ; \n");  
    printf("===============================================================\n");
    printf("  MHD_Solver_5:  and the latter should be > 0.  \n" ); 
    printf("  MHD_Solver_5:  Said another way, the ratio Bsq/(2(tau+D)) \n");
    printf("  MHD_Solver_5:  should be positive and less than one.  It is\n");
    printf("  MHD_Solver_5:        alpha_sq = %25.20e \n", alpha_sq ); 
#endif
///}}}
    }
  }

  /* 
  Check inequality  S^2 < (tau+D)^2  (dominant energy condition) 
  */
  if ( beta_sq > 1.0 ) 
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5:  The identity S^2 < (tau+D)^2 is violated. \n");
//{{{  if inequality not satisfied, dump some variables 
#if 1
    printf("  MHD_Solver_5:                tau = %25.20e \n", tau ); 
    printf("  MHD_Solver_5:                 D  = %25.20e \n", D ); 
    printf("  MHD_Solver_5:                Ssq = %25.20e \n", Bsq ); 
    printf("  MHD_Solver_5:    (tau+D)^2 - S^2 = %25.20e \n", a_sq-Ssq ); 
    printf("  MHD_Solver_5:           alpha_sq = %25.20e \n", alpha_sq ); 
#endif
//}}}
    }
  }


  /*
  CASE 0:  B^2 = 0; shouldn't happen but if it does we should go back to 
    MHD_Solver_4.  
  */ 
  if ( sqrt( Bsq ) < 6.e-16 ) 
  {
    //{{{     
    //printf("  MHD_Solver_5:  We are coming in with a magnetic field \n");
    //printf("  MHD_Solver_5:  that is (possibly machine) zero.       \n");
    //printf("  MHD_Solver_5:      |B| = %25.20e          \n", sqrt(Bsq) );
    //printf("  MHD_Solver_5:  We will use MHD_Solver_4 which does the\n");
    //printf("  MHD_Solver_5:  pure fluid case. \n");
    //}}}     
    rc = MHD_Solver_4(u, xpt, gd, gu, fpar) ;   
    return 1; 
  }


  /* {{{ CASE 1:  v^2 = 0; analytic solution 
  
  If the velocity vector is the zero vector, the form of the equation whose 
  root we are to find changes.  In this case, the momentum vector, S_i, 
  is also identically zero.  The general solution to our problem is then 
  straightforwardly found to be (in rescaled variables) 
     q = gamma * (1 - alpha^2) - (gamma - 1) * delta 
  or, in usual notation 
     q = gamma * (tau + D - B^2/2) - (gamma - 1) * D 
  or, 
     hW^2 = gamma*(tau - B^2/2) + D
  
  We use this solution in the following for this possibility.  
  
  }}} */ 
  if ( beta_sq < 6.e-16  &&  epsilon_sq < 6.e-16 ) 
  { 
//{{{ 
#if 0   
    printf("  MHD_Solver_5:  Encountered Case 1 for which v^2=0 \n");
    printf("  MHD_Solver_5:  It would appear that the velocity\n");
    printf("    vector is essentially the zero vector.  We are \n");
    printf("    calling this Case 1 and noting that the solution \n");
    printf("    is better handled analytically.                 \n");
    printf("       Sd[0] = %25.20e \n", Sd[0] ) ; 
    printf("       Sd[1] = %25.20e \n", Sd[1] ) ; 
    printf("       Sd[2] = %25.20e \n", Sd[2] ) ; 
    printf("        Ssq  = %25.20e \n", Ssq ) ; 
    printf("        BS   = %25.20e \n", BS ) ; 
#endif     
//}}} 
    
    q = gamma * (tau - 0.5*Bsq) + D ; 
    
    u[V_RHO] = D ; 
    u[V_P]   = (gamma-1.0)/gamma*(q - u[V_RHO]);
    u[V_VX]  = 0.0 ; 
    u[V_VY]  = 0.0 ; 
    u[V_VZ]  = 0.0 ; 
    u[V_BX]  = u[U_BX] ; 
    u[V_BY]  = u[U_BY] ; 
    u[V_BZ]  = u[U_BZ] ; 
    u[V_PSI] = u[U_PSI] ; 

    return 1 ; 
  }
  /* {{{ CASE 2:  (v_paral)^2 = 0; f cubic, g quadratic   
  
  The second special case mentioned above is if BS=0 but S^2 is not zero.
  This is equivalent to v_parallel = 0 or beta_sq=xi_sq or that the 
  coordinate velocity, v^i, is normal to the magnetic field, B^i.  
  }}} */
  else if ( (beta_sq > 6.e-16  &&  epsilon_sq < 6.e-16)  ||  xi_sq == beta_sq ) 
  { 
//{{{
#if 0
    printf("  MHD_Solver_5:  Encountered Case 2 for which (v_paral)^2=0 \n");
    printf("  MHD_Solver_5:  but there is a nonzero velocity and it is  \n");
    printf("  MHD_Solver_5:  strictly perpendicular to the B field.     \n");
    printf("  MHD_Solver_5:  We also have that BS vanishes and that     \n");
    printf("  MHD_Solver_5:  beta_sq and xi_sq are equal.    \n");
    printf("  MHD_Solver_5:         BS = %25.20e \n", BS ); 
    printf("  MHD_Solver_5:        Ssq = %25.20e \n", Ssq ); 
    printf("  MHD_Solver_5:      xi_sq = %25.20e \n", xi_sq ); 
    printf("  MHD_Solver_5:    beta_sq = %25.20e \n", beta_sq ); 
    printf("  MHD_Solver_5: epsilon_sq = %25.20e \n", epsilon_sq ); 
#endif    
//}}}
    
    double rc_vparal0 = MHD_Solver_5_vparal0(u, xpt, gd, gu, fpar);  
    if (rc_vparal0 < 0) 
    { 
      printf("  MHD_Solver_5:  The solver failed in the special case \n"); 
      printf("  MHD_Solver_5:  for which v_paral vanishes.\n"); 
      exit(-1); 
    }
    
    return 1; 
  }
  /* {{{  CASE 3:  (v_perp)^2 = 0; f and g quadratics 

  The third special case mentioned above is for v_perp = 0 which is 
  equivalent to xi_sq = 0 or to S_perp^2 = 0.  In this case, the solution
  method is almost exactly the same as MHD_Solver_4.  
  }}} */ 
  else if ( (xi_sq < 1.e-15  &&  beta_sq > 6.e-16)  ||  beta_sq == epsilon_sq )
  { 
//{{{
#if 0    
    printf("  MHD_Solver_5:  Encountered Case 3 for which (v_perp)^2=0 \n");
    printf("  MHD_Solver_5:  but there is a nonzero velocity and it lies\n");
    printf("  MHD_Solver_5:  completely along the B field.  We also have\n");
    printf("  MHD_Solver_5:  that xi_sq and S_perp^2 vanish.     \n");
    printf("  MHD_Solver_5:      xi_sq = %25.20e \n", xi_sq); 
    printf("  MHD_Solver_5:         BS = %25.20e \n", BS   ); 
    printf("  MHD_Solver_5:        Ssq = %25.20e \n", Ssq  ); 
    printf("  MHD_Solver_5:    beta_sq = %25.20e \n", beta_sq); 
#endif
//}}}

    double rc_vperp0 = MHD_Solver_5_vperp0(u, xpt, gd, gu, fpar);  
    if (rc_vperp0 < 0) 
    { 
      printf("  MHD_Solver_5:  The solver failed in the special case \n"); 
      printf("  MHD_Solver_5:  for which v_perp vanishes.\n"); 
      exit(-1); 
    }
    
    return 1; 
  }

  double  g_at_qg_l = 0.0 ; 
  double  g_at_qg_h = 0.0 ; 
    
  /* {{{ 
  If we have gotten to this point and we we are still here, then we have 
  passed through the gauntlet of special cases of vanishing velocity 
  components and are firmly planted in the land of the general problem.

  We begin by trying to find the largest zero of the quartic g(q). 
  We will call this root  qg_2  (as in the B=0 primitive solver, 
  MHD_Solver_4).  It might serve as the lower bound on the bracket 
  for solving for the root of F(q). 
   
  To do this, we need bounds on the root  qg_2.  We can show analytically 
  that a lower bound is q=sqrt(beta_sq - xi_sq) and an upper bound is 
     q=tau+D (or 1 in rescaled variables).  
  }}}  */ 
  if (     beta_sq > 6.e-16  &&     
        epsilon_sq > 6.e-16  &&    
             xi_sq > 6.e-16       ) 
  { 
    /* 
    The following value for qg_l is just the intersection of the 
    "Mexican hat" part of g with the positive axis.  It is the lower 
    bound for finding qg_2, the positive root of g.  It can also be 
    written as 
                 qg_l = sqrt( BS*BS/Bsq ) / (tau+D)   
    */
    qg_l = sqrt( beta_sq - xi_sq ) ;  
    // just to be safe, divide by 2 
    qg_l *= 0.5 ; 

    /* 
    Check that the lower bound on the root of g(q) is not too small. 
    */
    if ( qg_l < 6.e-16 )
    { 
      //{{{
      if (ltrace) { 
      printf("  MHD_Solver_5:  Trying to calculate the lower bound for \n");
      printf("  MHD_Solver_5:  the bracket for the root of g(q).  Indeed, \n");
      printf("  MHD_Solver_5:  qg_l is so small that it looks like we have\n");
      printf("  MHD_Solver_5:  the case that (v_paral)^2 = 0.  \n"); 
      printf("  MHD_Solver_5:      qg_l = %25.20e \n", qg_l); 
      printf("  MHD_Solver_5:       Ssq = %25.20e \n", Ssq); 
      printf("  MHD_Solver_5:        BS = %25.20e \n", BS); 
      printf("  MHD_Solver_5:     xi_sq = %25.20e \n", xi_sq); 
      } 
      //}}}
    } 
    
    /*
    Set an upper bound for finding the root of g(q) (where we call that root 
    qg_2).  It is easy to show that  g(1) > 0  and we can set qg_h to 1.0.  
    */ 
    qg_h = 1.0 ; 
  

    /*
    Double check that these two values provide a good bracket for the root 
    of g.  Analytically, we can show that g(qg_l) < 0 and q(qg_h) > 0, but 
    the question is whether we also get these results numerically. 
    */
    func5g_p( &g_at_qg_l, qg_l, fpar) ;  
    func5g_p( &g_at_qg_h, qg_h, fpar) ;  
    if ( g_at_qg_l >= 0  ||  g_at_qg_h <= 0.0 ) 
    { 
      //{{{  dump state and do a maple dump    
      if (ltrace) { 
      printf("  MHD_Solver_5:  Problem, bracket on g is wrong \n") ; 
      printf("  MHD_Solver_5:    g_at_qg_l = %25.20e \n", g_at_qg_l) ; 
      printf("  MHD_Solver_5:    g_at_qg_h = %25.20e \n", g_at_qg_h) ; 
      printf("  MHD_Solver_5:         qg_l = %25.20e \n", qg_l); 
      printf("  MHD_Solver_5:         qg_h = %25.20e \n", qg_h); 
      printf("  MHD_Solver_5:          tau = %25.20e \n", tau); 
      printf("  MHD_Solver_5:           D  = %25.20e \n",  D ); 
      printf("  MHD_Solver_5:          Ssq = %25.20e \n", Ssq); 
      printf("  MHD_Solver_5:          Bsq = %25.20e \n", Bsq); 
      printf("  MHD_Solver_5:           BS = %25.20e \n", BS); 
      printf("  MHD_Solver_5:     alpha_sq = %25.20e \n", alpha_sq); 
      printf("  MHD_Solver_5:      beta_sq = %25.20e \n", beta_sq); 
      printf("  MHD_Solver_5:        xi_sq = %25.20e \n", xi_sq); 
      printf("  MHD_Solver_5:  tau+D-Bsq/2 = %25.20e \n", tau+D-0.5*Bsq); 
      printf("  MHD_Solver_5:===== MAPLE DUMP =============================\n");
      printf("   restart: with(plots): \n");  
      printf("   alpha_sq := %25.20e ; \n", alpha_sq );  
      printf("   beta_sq  := %25.20e ; \n", beta_sq  );  
      printf("     xi_sq  := %25.20e ; \n", xi_sq    );  
      printf("   gamma1   := %25.20e ; \n", gamma    );  
      printf("   delta1   := %25.20e ; \n", D/a      );  
      printf("   f(q) := (q+2.0*alpha_sq)                                  \n");
      printf("    * ( (q+2.0*alpha_sq) * ( -  (q-0.5*gamma1*(1.0-alpha_sq))\n");
      printf("                               *(q-0.5*gamma1*(1.0-alpha_sq))\n");
      printf("                             +  (0.5*gamma1*(1.0-alpha_sq))  \n");
      printf("                               *(0.5*gamma1*(1.0-alpha_sq))  \n");
      printf("                             -  (gamma1-1.0)*beta_sq         \n");
      printf("                           )                                 \n");
      printf("      + (3.0*gamma1-4.0)*alpha_sq*xi_sq  )                   \n");
      printf("    - 2.0*(gamma1-2.0)*alpha_sq*alpha_sq*xi_sq      ;        \n");
      printf("   plot(f(q), q=-0.5..1) ;                                   \n");
      printf("   g(q) :=  q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq)          \n");
      printf("         - q*q * xi_sq                                       \n");
      printf("         - (q+2.0*alpha_sq)*(q+2.0*alpha_sq)*(beta_sq-xi_sq);\n");
      printf("   g_1(q) :=  (gamma1-1.)*delta1*(q+2.0*alpha_sq)*sqrt(g(q));\n");
      printf("   plot(g(q), q=-0.5..1) ;                                   \n");
      printf("   plot( {f(q), g(q)}, q=-0.5..1) ;                          \n");
      printf("   plot( {f(q), g_1(q)}, q=-0.5..1) ;                        \n");
      printf("=============================================================\n");
      } 
      //}}}  
        
      //  despite this presumed failure, I think we are actually okay.  
      //  let's check ... 
      qg_i_0 = 0.5 * (qg_h+qg_l); 
      gtrace=0;
      grc = rtsafe(func5g_p_d, &qg_i_0, qg_l, qg_h, gtol, fpar, gtrace);
      if (ltrace) 
      {
       printf(" MHD_Solver_5:  In a check, we find the root of g  \n"); 
       printf(" MHD_Solver_5:  despite the problems with the brackets ... \n"); 
       printf(" MHD_Solver_5:    qg_l = %25.20e \n", qg_l ); 
       printf(" MHD_Solver_5:    qg_h = %25.20e \n", qg_h ); 
       printf(" MHD_Solver_5:    qg   = %25.20e \n", qg_i_0 ); 
      }
      qg_i_0 = 0.5 * (qg_h+(0.5*qg_l)); 
      gtrace=0;
      grc = rtsafe(func5g_p_d, &qg_i_0, qg_l, qg_h, gtol, fpar, gtrace);
      if (ltrace) 
      {
       printf(" MHD_Solver_5:  In a second check, we find the root of g e \n");
       printf(" MHD_Solver_5:  despite the problems with the brackets ... \n");
       printf(" MHD_Solver_5:    qg_l = %25.20e \n", qg_l*0.5 ); 
       printf(" MHD_Solver_5:    qg_h = %25.20e \n", qg_h ); 
       printf(" MHD_Solver_5:    qg   = %25.20e \n", qg_i_0 ); 
      } 

    } 
    // We make an initial guess for the root of g as the midpoint of 
    // the bracket. 
    qg_i = 0.5 * ( qg_h + qg_l ) ; 
    qg = qg_i ;

    // Incorporate a little redundancy if things go wrong ...  
    qg_l_0 = qg_l ; 
    qg_h_0 = qg_h ;
    qg_i_0 = qg_i ;

    // Now use rtsafe to find the root of g (called qg) bracketed between the
    // bounds qg_l and qg_h  
    gtrace = 0;
    grc = rtsafe(func5g_p_d, &qg, qg_l, qg_h, gtol, fpar, gtrace);

    // in the event of failure, a postmortem:  
    if (grc < 0)
    {
      qg_l = qg_l_0;
      qg_h = qg_h_0;
      qg   = qg_i_0;
      
      gtrace = 1;
      grc = rtsafe(func5g_p_d, &qg, qg_l, qg_h, gtol, fpar, gtrace);
      exit(2);
      return -4;
    }

  }
  else if (  beta_sq < 6.e-16  &&  epsilon_sq < 6.e-16 ) 
  //{{{  
  {
    if (ltrace)
    {
      printf("  MHD_Solver_5:  Something strange happened.  We seem \n");
      printf("  MHD_Solver_5:  to have come back to Case 1 after  \n");
      printf("  MHD_Solver_5:  originally skipping it.  \n");
    }
  }
  //}}}
  else if ( (beta_sq > 6.e-16  &&  epsilon_sq < 6.e-16)  ||  beta_sq == xi_sq )
  //{{{  
  {
    if (ltrace)
    {
      printf("  MHD_Solver_5:  Something strange happened.  We seem \n");
      printf("  MHD_Solver_5:  to have come back to Case 2 after  \n");
      printf("  MHD_Solver_5:  originally skipping it.  \n");
    }
  }
  //}}}
  else if ( (beta_sq > 6.e-16  &&  xi_sq < 6.e-16)  ||  beta_sq == epsilon_sq ) 
  //{{{  
  {
    if (ltrace)
    {
      printf("  MHD_Solver_5:  Something strange happened.  We seem \n");
      printf("  MHD_Solver_5:  to have come back to Case 3 after  \n");
      printf("  MHD_Solver_5:  originally skipping it.  \n");
    }
  }
  //}}}
  else 
  //{{{  
  {
    if (ltrace)
    {
      printf("  MHD_Solver_5:  Now we're onto something strange ...  \n") ; 
      printf("  MHD_Solver_5:  We're not in any known case ... \n");
      printf("  MHD_Solver_5:        Ssq = %25.20e \n", Ssq); 
      printf("  MHD_Solver_5:         BS = %25.20e \n", BS); 
      printf("  MHD_Solver_5:       |BS| = %25.20e \n", fabs(BS)); 
      printf("  MHD_Solver_5:   alpha_sq = %25.20e \n", alpha_sq); 
      printf("  MHD_Solver_5:    beta_sq = %25.20e \n", beta_sq); 
      printf("  MHD_Solver_5:      xi_sq = %25.20e \n", xi_sq); 
      printf("  MHD_Solver_5: epsilon_sq = %25.20e \n", epsilon_sq); 
    }
  }
  //}}}

  
  //  We have the root of g that we wanted, namely the single positive root:   
  qg_2 = qg ; 

  
  /* {{{   Some documentation ... 
  If we get to this point, presumably, we found the root of g(q) which we 
  are calling  qg_2.  This can serve as the lower bound on the root of F(q), 
  the root we ultimately want to find.  
   
  It (qg_2) also turns out to (possibly) be the lower bound for the largest 
  root of f(q) which we are calling qf_2.  To see if this is legitimate, 
  we have to check that f(qg_2) > 0.  This is also an important check on 
  our method because if f(qg_2) is not positive then a solution to the 
  problem may not even exist.  
  }}}  */  
  // Check whether f(qg_2) is positive by calling func5f_p.  
  double f_at_root_of_g ;
  func5f_p( &f_at_root_of_g, qg_2, fpar) ;  

  // Now double check that f(qg_2) > 0.  
  if ( f_at_root_of_g > 0.0 )
  {
    /* 
    The root of g(q) becomes the lower bound for finding the largest root
    of f(q) as well as the lower bound for finding the root of F(q), the
    ultimate root we want.  
    */ 
    qf_l = qg_2 ; 
    ql   = qg_2 ; 
  }
  else if ( f_at_root_of_g > -1.0e-15  &&  f_at_root_of_g <= 0.0 )
  { 
    //{{{
    if (ltrace) 
    {
     printf("  \n") ;
     printf("  MHD_Solver_5:  Problem at the lower bound on the \n");
     printf("  MHD_Solver_5:  largest root of f(q).  This may indicate  \n");
     printf("  MHD_Solver_5:  that a solution to the general primitive \n");
     printf("  MHD_Solver_5:  solve does not exist.  In particular \n");
     printf("  MHD_Solver_5:  the function f(q) is negative and close \n");
     printf("  MHD_Solver_5:  to machine zero, i.e.\n");
     printf("     f_at_root_of_g = %25.20e \n", f_at_root_of_g );
     printf("           alpha_sq = %25.20e \n", alpha_sq );  
     printf("            beta_sq = %25.20e \n", beta_sq );  
     printf("              xi_sq = %25.20e \n", xi_sq );  
     printf("               a_sq = %25.20e \n", a_sq );  
     printf("                 qg = %25.20e \n", qg);  
     printf("                  q = %25.20e \n", q );  
    }
    exit(2) ;
    return -1 ;
    //}}}
  }
  else if ( f_at_root_of_g <= -1.0e-15 )
  { 
    //{{{ do a maple dump and dump state 
    if (ltrace) 
    {
     printf("  \n") ;
     printf("  MHD_Solver_5:  Problem at the lower bound on the \n");
     printf("  MHD_Solver_5:  largest root of f(q).  This may indicate \n");
     printf("  MHD_Solver_5:  that a solution to the general primitive \n");
     printf("  MHD_Solver_5:  solve does not exist.  In particular \n");
     printf("  MHD_Solver_5:  the function f(q) is significantly less \n");
     printf("  MHD_Solver_5:  than 0, i.e.      \n");
     printf("     f_at_root_of_g = %25.20e \n", f_at_root_of_g );
     printf("           Sd[0]    = %25.20e \n", Sd[0] );  
     printf("           Sd[1]    = %25.20e \n", Sd[1] );  
     printf("           Sd[2]    = %25.20e \n", Sd[2] );  
     printf("           Ssq      = %25.20e \n", Ssq );  
     printf("           alpha_sq = %25.20e \n", alpha_sq );  
     printf("            beta_sq = %25.20e \n", beta_sq );  
     printf("              xi_sq = %25.20e \n", xi_sq );  
     printf("               a_sq = %25.20e \n", a_sq );  
     printf("                 qg = %25.20e \n", qg);  
     printf("               qg_l = %25.20e \n", qg_l );  
     printf("               qg_h = %25.20e \n", qg_h );  
     printf("               qg_i = %25.20e \n", qg_i );  
     printf("             qg_l_0 = %25.20e \n", qg_l_0 );  
     printf("             qg_h_0 = %25.20e \n", qg_h_0 );  
     printf("             qg_i_0 = %25.20e \n", qg_i_0 );  
     printf("               qg_2 = %25.20e \n", qg_2 );  
     printf("          g_at_qg_l = %25.20e \n", g_at_qg_l );  
     printf("          g_at_qg_h = %25.20e \n", g_at_qg_h );  
     printf("                 BS = %25.20e \n", BS  );  
     printf("                Bsq = %25.20e \n", Bsq  );  
     printf("      Ssq/(tau+D)^2 = %25.20e \n", Ssq/(tau+D)/(tau+D) );  
     printf("(Ssq+D^2)/(tau+D)^2 = %25.20e \n", (Ssq+D*D)/(tau+D)/(tau+D) );  
     printf("        tau+D-Bsq/2 = %25.20e \n", tau+D-0.5*Bsq );  
     printf("              alpha = %25.20e \n", sqrt(alpha_sq) );  
     printf("              beta  = %25.20e \n", sqrt(beta_sq) );  
     printf("              xi    = %25.20e \n", sqrt(xi_sq) );  
     printf("              delta = %25.20e \n", D/a );  
     printf("         FP_COORD_X = %25.20e \n", fpar[FP_COORD_X] );  
     printf("         FP_COORD_Y = %25.20e \n", fpar[FP_COORD_Y] );  
     printf("         FP_COORD_Z = %25.20e \n", fpar[FP_COORD_Z] );  
     printf("==================== MAPLE DUMP ==============================\n");
     printf("   restart: with(plots): \n");  
     printf("   alpha_sq := %25.20e ; \n", alpha_sq );  
     printf("   beta_sq  := %25.20e ; \n", beta_sq  );  
     printf("     xi_sq  := %25.20e ; \n", xi_sq    );  
     printf("   gamma1   := %25.20e ; \n", gamma    );  
     printf("   delta1   := %25.20e ; \n", D/a      );  
     printf("       qg   := %25.20e ; \n", qg       );  
     printf("   f(q) := (q+2.0*alpha_sq)                                  \n"); 
     printf("    * ( (q+2.0*alpha_sq) * ( -  (q-0.5*gamma1*(1.0-alpha_sq))\n");
     printf("                               *(q-0.5*gamma1*(1.0-alpha_sq))\n");
     printf("                             +  (0.5*gamma1*(1.0-alpha_sq))  \n");
     printf("                               *(0.5*gamma1*(1.0-alpha_sq))  \n");
     printf("                             -  (gamma1-1.0)*beta_sq         \n");
     printf("                           )                                 \n");
     printf("      + (3.0*gamma1-4.0)*alpha_sq*xi_sq  )                   \n");
     printf("    - 2.0*(gamma1-2.0)*alpha_sq*alpha_sq*xi_sq      ;        \n");
     printf("   plot(f(q), q=qg-0.05..qg+0.05) ; \n");  
     printf("   g(q) :=  q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq)          \n");
     printf("         - q*q * xi_sq                                      \n");
     printf("         - (q+2.0*alpha_sq)*(q+2.0*alpha_sq)*(beta_sq-xi_sq);\n");
     printf("   g_1(q) :=  (gamma1-1.)*delta1*(q+2.0*alpha_sq)*sqrt(g(q));\n");
     printf("   fsq(q) :=  f(q)*f(q) ;   \n");
     printf("   plot(g(q), q=qg-0.05..qg+0.05) ; \n");  
     printf("   plot( {f(q), g(q)}, q=qg-0.05..qg+0.05) ; \n");  
     printf("   plot( {fsq(q), g(q)}, q=qg-0.05..qg+0.05) ; \n");  
     printf("   plot( {f(q), g_1(q)}, q=qg-0.05..qg+0.05) ; \n");  
     printf("   plot( {(f(q))^2 - (g_1(q))^2 }, q=qg-0.05..qg+0.05) ; \n");  
     printf("==============================================================\n");
    //}}}



    printf("  MHD_Solver_5:  Now trying to check a generalized inequality \n");
    printf("  MHD_Solver_5:          qg = %25.20e \n", qg ); 
    printf("  MHD_Solver_5:     beta_sq = %25.20e \n", beta_sq ); 
    printf("  MHD_Solver_5:  q(1-alpha^2) + alpha^2 xi^2 (3q+4alpha^2)/(q+2alpha^2)^2 \n"); 
    printf("  MHD_Solver_5:             = %25.20e \n", qg*(1.-alpha_sq) + alpha_sq*xi_sq*(3.*qg+4.*alpha_sq)/pow(qg+2.*alpha_sq,2) ); 
    printf("  MHD_Solver_5:  This last thing should be larger than beta_sq \n");
    } 

    // try something akin to what is in check_Ssq, but for this inequality ...
    int counter0 = 0 ; 
    double Ssq2, Ssq_max,t_min; 
    int i0 ; 
    while ( f_at_root_of_g < 1.e-13  &&  counter0 < 99 ) 
    {  
      Ssq2 = 0.0 ; 
      Ssq_max =   qg * (1. - alpha_sq) 
                +   alpha_sq 
                  * xi_sq 
                  * (3.*qg + 4.*alpha_sq) 
                  / pow( qg + 2.*alpha_sq , 2 ) ; 
      Ssq_max *= a_sq ; 
      t_min = sqrt( (Ssq_max / Ssq) - 1.e-13 ); 
      //printf("  MHD_Solver_5:        counter0 = %3d \n", counter0 ); 
      //printf("  MHD_Solver_5:  qg = %25.20e \n", qg ); 
      //printf("  MHD_Solver_5:  before  Sd[0] = %25.20e \n", Sd[0] ); 
      //printf("  MHD_Solver_5:  before  Sd[1] = %25.20e \n", Sd[1] ); 
      //printf("  MHD_Solver_5:  before  Sd[2] = %25.20e \n", Sd[2] ); 
      //printf("  MHD_Solver_5:  before  Ssq   = %25.20e \n", Ssq ); 
      //printf("  MHD_Solver_5:  before  BS    = %25.20e \n", BS ); 
      //printf("  MHD_Solver_5:  before  alpha_sq = %25.20e \n", alpha_sq ); 
      //printf("  MHD_Solver_5:  before  beta_sq  = %25.20e \n", beta_sq ); 
      //printf("  MHD_Solver_5:  before  xi_sq    = %25.20e \n", xi_sq ); 
      //printf("  MHD_Solver_5:       Ssq_max  = %25.20e \n", Ssq_max); 
      //printf("  MHD_Solver_5:       t_min    = %25.20e \n", t_min ); 
      for ( i0=0; i0<3; i0++ )
      { Sd[i0] *= t_min ; } 
      Ssq2 = square_form(Sd, gu); 
      u[U_SX] = Sd[0] ; 
      u[U_SY] = Sd[1] ; 
      u[U_SZ] = Sd[2] ; 
      //printf("  MHD_Solver_5:  after  Sd[0] = %25.20e \n", Sd[0] ); 
      //printf("  MHD_Solver_5:  after  Sd[1] = %25.20e \n", Sd[1] ); 
      //printf("  MHD_Solver_5:  after  Sd[2] = %25.20e \n", Sd[2] ); 
      BS = Bx * Sd[0] + By * Sd[1] + Bz * Sd[2] ; 
      fpar[FP_SSQ] = Ssq2 ; 
      fpar[FP_BS] = BS ; 
      Ssq = Ssq2 ; 
      //printf("  MHD_Solver_5:  after  Ssq   = %25.20e \n", Ssq ); 
      //printf("  MHD_Solver_5:  after  BS    = %25.20e \n", BS ); 
      beta_sq = Ssq / a_sq ; 
      epsilon_sq = (BS*BS) / (Bsq * a_sq) ; 
      xi_sq = beta_sq - epsilon_sq ; 
      //printf("  MHD_Solver_5:  t_min = %25.20e \n", t_min ); 
      //printf("  MHD_Solver_5:  a_sq = %25.20e \n", a_sq ); 
      //printf("  MHD_Solver_5:  Ssq2 = %25.20e \n", Ssq2 ); 
      //printf("  MHD_Solver_5:  Ssq_max = %25.20e \n", Ssq_max ); 
      //printf("  MHD_Solver_5:  after  alpha_sq = %25.20e \n", alpha_sq ); 
      //printf("  MHD_Solver_5:  after  beta_sq  = %25.20e \n", beta_sq ); 
      //printf("  MHD_Solver_5:  after  xi_sq    = %25.20e \n", xi_sq ); 
      //printf("  MHD_Solver_5:  after  BS = %25.20e \n", BS ); 
    
      checkfornans( &t_min, "t_min", "MHD_Solver_5");  
      checkfornans( &Ssq, "Ssq", "MHD_Solver_5");  
    
      //  we now have to re-solve for the root of g given this shifted value 
      //  of Ssq (and BS).  
      qg_l = 0.5*sqrt( beta_sq - xi_sq ) ;  
      qg_h = 1.0 ; 
      qg = 0.5 * ( qg_l + qg_h ) ;
      qg_l_0 = qg_l; qg_h_0 = qg_h ; qg_i_0 = qg_i;//redundancy 
      gtrace = 0;
      grc = rtsafe(func5g_p_d, &qg, qg_l, qg_h, gtol, fpar, gtrace);
      if (grc < 0)
      { 
        qg_l = qg_l_0;
        qg_h = qg_h_0;
        qg   = qg_i_0;
        gtrace = 1;
        grc = rtsafe(func5g_p_d, &qg, qg_l, qg_h, gtol, fpar, gtrace);
        exit(2);
        return -4;
      }
      // check the inequality again ... 
      qg_2 = qg ; 
      func5f_p( &f_at_root_of_g, qg_2, fpar) ;  
      double g_at_qg_2; 
      func5g_p( &g_at_qg_2, qg_2, fpar) ;  
      double F_at_qg_2; 
      func5_p( &F_at_qg_2, qg_2, fpar) ;  
    
    
      printf("%2d, qg2=%12.7e, f(g2)=%17.12e, g(g2)=%17.12e, F(g2)=%17.12e\n",counter0,qg,f_at_root_of_g,g_at_qg_2,F_at_qg_2 );
    
      counter0 ++ ;
    }

    // Now double check that f(qg_2) > 0.  
    if ( f_at_root_of_g <= 0.0 )
    { 
      printf("MHD_Solver_5:  still problems ... ugh .. \n"); 
      exit(2) ;
      return -1 ;
    } 
 
    qf_l = qg_2 ; // The root of g(q) becomes the lower bound for finding 
    ql   = qg_2 ; // the largest root of f(q) as well as the lower bound 
                    // for finding the root of F(q), the ultimate root we want.

    q = a*qg_2 ; 

#if 1 
    // Knowing q (i.e. hW^2, the enthalpy), we can now get the remaining 
    // primitive variables.  Start with the Lorentz factor, W^2  
    double invWsq = 1.0 - ((2.0*q+Bsq)*BS*BS + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq)) ;
    double invWsq0 = invWsq ; 
  
    if ( fabs(invWsq) < 6.e-16 ) 
    { invWsq = 6.e-16 ; } 
    else if ( invWsq < - 6.e-16 ) 
    { 
     if (ltrace) 
     { 
      printf("  MHD_Solver_5:  invWsq is negative invWsq = %25.20e \n",invWsq);
      printf("  MHD_Solver_5:  ... that's a problem ...  \n"); 
      printf("  MHD_Solver_5:  How is this even possible?  This is \n"); 
      printf("  MHD_Solver_5:  essentially g.  Check again.   \n"); 
     }
      double tmp_q = q/a ; 
      double tmp_g_at_q ; 
      func5g_p( &tmp_g_at_q, tmp_q, fpar );  
     if (ltrace) 
     { 
      printf("  MHD_Solver_5:     q    = %25.20e \n",tmp_q ); 
      printf("  MHD_Solver_5:   g_at_q = %25.20e \n",tmp_g_at_q ); 
      printf("  MHD_Solver_5:  Shift q again ... add a tiny amount. \n"); 
     }
      tmp_q += 6.e-16 ;
      func5g_p( &tmp_g_at_q, tmp_q, fpar );  
     if (ltrace) 
     { 
      printf("  MHD_Solver_5:  new   q    = %25.20e \n",tmp_q ); 
      printf("  MHD_Solver_5:  new g_at_q = %25.20e \n",tmp_g_at_q ); 
     }

      if ( tmp_g_at_q < 0.0 )
      { 
        printf("  MHD_Solver_5:  This didn't work ... \n"); 
        exit(-2); return -4;  
      } 
      q = a*tmp_q ; 
      invWsq = 1.0 - ((2.0*q+Bsq)*BS*BS + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq)) ;
      printf("  MHD_Solver_5:  new invWsq = %25.20e \n",invWsq ); 
    }

    double Su[3];
    form_raise_index(Su, Sd, gu);

    u[V_RHO] = D * sqrt( invWsq ) ; 
    u[V_P]   = (gamma-1.0)/gamma*(q*invWsq - u[V_RHO]);
    u[V_VX]  = (Su[0] + BS*Bx/q)/ (q+Bsq);
    u[V_VY]  = (Su[1] + BS*By/q)/ (q+Bsq);
    u[V_VZ]  = (Su[2] + BS*Bz/q)/ (q+Bsq);
    u[V_BX]  = u[U_BX] ;
    u[V_BY]  = u[U_BY] ;
    u[V_BZ]  = u[U_BZ] ;
    u[V_PSI] = u[U_PSI] ;

    if ( fabs(invWsq0) < 6.e-16 ) 
    {  
      double vu[3] ; 
      vu[0] = u[V_VX] ; 
      vu[1] = u[V_VY] ; 
      vu[2] = u[V_VZ] ; 
    
      for ( int i0=0; i0<3; i0++ )
      {
             if ( vu[i0] >=  6.e-16 ) { vu[i0] -= 6.e-16 ; } 
        else if ( vu[i0] <= -6.e-16 ) { vu[i0] += 6.e-16 ; } 
      }
      if (ltrace) 
      {
       printf("  MHD_Solver_5:  on adjusting invWsq from %25.20e \n",invWsq0 ); 
       printf("  MHD_Solver_5:  to invWsq = %25.20e \n",invWsq);
      }
      /*
      printf("  MHD_Solver_5:  we also adjust vsq by subtracting small \n");
      printf("  MHD_Solver_5:  amounts from the components of the velocity \n"); 
      printf("  MHD_Solver_5:     u[V_VX] = %25.20e \n", u[V_VX] ); 
      printf("  MHD_Solver_5:     u[V_VY] = %25.20e \n", u[V_VY] ); 
      printf("  MHD_Solver_5:     u[V_VZ] = %25.20e \n", u[V_VZ] );   
      printf("  MHD_Solver_5:      vu[0]  = %25.20e \n", vu[0] );   
      printf("  MHD_Solver_5:      vu[1]  = %25.20e \n", vu[1] );   
      printf("  MHD_Solver_5:      vu[2]  = %25.20e \n", vu[2] );   
      double vsq = square_vector(vu, gd) ;
      printf("  MHD_Solver_5:       vsq   = %25.20e \n", vsq );   
*/  
      // save changes to v 
      u[V_VX] = vu[0] ; 
      u[V_VY] = vu[1] ; 
      u[V_VZ] = vu[2] ; 
    }

#endif 

/*
    printf("  MHD_Solver_5: ...dumping output before leaving mhd_solver_5\n");
    printf("         qg_2       = %25.20e \n", qg_2 );  
    printf("         q          = %25.20e \n", q );  
    printf("         invWsq     = %25.20e \n", invWsq );  
    printf("         U[V_RHO]   = %25.20e \n", u[V_RHO] );  
    printf("         U[V_P]     = %25.20e \n", u[V_P] );  
    printf("         U[V_VX]    = %25.20e \n", u[V_VX] );  
    printf("         U[V_VY]    = %25.20e \n", u[V_VY] );  
    printf("         U[V_VZ]    = %25.20e \n", u[V_VZ] );  
    printf("         U[V_BX]    = %25.20e \n", u[V_BX] );  
    printf("         U[V_BY]    = %25.20e \n", u[V_BY] );  
    printf("         U[V_BZ]    = %25.20e \n", u[V_BZ] );  
    printf("         U[V_PSI]   = %25.20e \n", u[V_PSI] );  
    printf("         FP_COORD_X = %25.20e \n", fpar[FP_COORD_X] );  
    printf("         FP_COORD_Y = %25.20e \n", fpar[FP_COORD_Y] );  
    printf("         FP_COORD_Z = %25.20e \n", fpar[FP_COORD_Z] );  
    printf("         FP_COORD_Z = %25.20e \n", fpar[FP_COORD_Z] );  
*/
    if (ltrace) {
    printf("  MHD_Solver_5:  ... going on and hoping this worked ... \n");
    }

    return 0 ; 
  } 
  else
  {
    //{{{  
    if (ltrace) {
    printf("  MHD_Solver_5:  Something weird and unexpected happened. \n");
    printf("     f_at_root_of_g = %25.20e \n", f_at_root_of_g );
    printf("           alpha_sq = %25.20e \n", alpha_sq );  
    printf("            beta_sq = %25.20e \n", beta_sq );  
    printf("              xi_sq = %25.20e \n", xi_sq );  
    printf("               a_sq = %25.20e \n", a_sq );  
    printf("                 qg = %25.20e \n", qg);  
    printf("                  q = %25.20e \n", q );  
    }
    exit(2) ;
    return -1 ;
    //}}}
  }


  /*
  If we get to this point, we may well have established that a solution
  for our primitive variables exists.  
      
  We have also established a lower bound on the largest root of f(q).  We 
  can show analytically that an upper bound for the largest root of f(q) 
  is q=2.  Thus we use it as part of the bracket for finding the root of f(q).
  */
  qf_h = 2.0 ;
  qf_i = 0.5 * ( qf_h + qf_l ) ; // take initial guess as midpoint of bracket
  qf = qf_i ;

  // incorporate a little redundancy in the event things go wrong 
  qf_l_0 = qf_l ;
  qf_h_0 = qf_h ;
  qf_i_0 = qf_i ;

  // again use rtsafe to find the root of f(q) bracketed between the 
  // bounds qf_l and qf_h  
  ftrace = 0;
  frc = rtsafe(func5f_p_d, &qf, qf_l, qf_h, ftol, fpar, ftrace);

  // in the event of failure, a postmortem:  
  if (frc < 0)
  {
    qf_l = qf_l_0;
    qf_h = qf_h_0;
    qf   = qf_i_0;
    ftrace = 1;
    frc = rtsafe(func5f_p_d, &qf, qf_l, qf_h, ftol, fpar, ftrace);
    
    printf("  MHD_Solver_5:  Failure in finding largest root of f. \n"); 
    printf("  MHD_Solver_5:  In func5f_p_d, rtsafe failed. \n"); 
    printf("  MHD_Solver_5:  Should put in some checking code ... \n");
    
    exit(2);
    return -4;
  }

  // With success we identify the root we have found as the largest 
  // root of f(q) and we call it qf_2: 
  qf_2 = qf ; 
 
  // set the bracket for solving for the root of F(q)  
  ql = qg_2 ;  
  qh = qf_2 ;  

  // Check that this bracket makes sense. 
  if ( !(ql < qh  &&  beta_sq < 1.0 ) ) 
  {
    if (ltrace) {
    printf("  MHD_Solver_5:  Problem with the bracket for F(q)=0. \n"); 
    printf("  MHD_Solver_5:          ql = %25.20e \n", ql); 
    printf("  MHD_Solver_5:          qh = %25.20e \n", qh); 
    printf("  MHD_Solver_5:     beta_sq = %25.20e \n", beta_sq); 
    printf("  MHD_Solver_5:    alpha_sq = %25.20e \n", alpha_sq); 
    printf("  MHD_Solver_5:  epsilon_sq = %25.20e \n", epsilon_sq); 
    printf("  MHD_Solver_5:       xi_sq = %25.20e \n", xi_sq); 
    }
  }

  
  // Check that F(q) at ql can be calculated and makes sense.  We will 
  // eventually expect it to be positive, but for the moment we just check 
  // that we can calculate it.    
  rc = func5_p( &F_at_ql, ql, fpar );  
  if (rc < 0)
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5:  Something's amiss with ql.  We could not  \n"); 
    printf("  MHD_Solver_5:  calculate F(ql).  Dumping some values, \n"); 
    printf("               ql = %25.20e \n", ql ); 
    printf("               qg = %25.20e \n", qg ); 
    printf("               qf = %25.20e \n", qf ); 
    } 
  } 
  // Check that F(q) at qh can be calculated and makes sense.  We will 
  // evventually expect it to be positive, but for the moment we just check
  // that we can calculate it. 
  rc = 0 ; 
  rc = func5_p( &F_at_qh, qh, fpar );  
  if (rc < 0)
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5:  Something's amiss with hl.  We could not  \n"); 
    printf("  MHD_Solver_5:  calculate F(qh).  Dumping some values, \n"); 
    printf("               qh = %25.20e \n", ql ); 
    printf("               qg = %25.20e \n", qg ); 
    printf("               qf = %25.20e \n", qf ); 
    } 
  } 
  
  // Now we check that F(ql) and F(qh) really are positive and negative, 
  // respectively.  If so, we should have a bracket for the root of F(q).  
  if ( !(F_at_ql > 0.0  &&  F_at_qh < 0.0) )
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5:  Problem with bracket.  We were expecting \n"); 
    printf("  MHD_Solver_5:  that F(ql) > 0 and F(qh) < 0.  Instead, we \n");
    printf("  MHD_Solver_5:  get  \n");
    printf("  MHD_Solver_5:        ql  = %25.20e \n",ql); 
    printf("  MHD_Solver_5:      F(ql) = %25.20e \n",F_at_ql); 
    printf("  MHD_Solver_5:        qh  = %25.20e \n",qh); 
    printf("  MHD_Solver_5:      F(qh) = %25.20e \n",F_at_qh); 
    printf("  MHD_Solver_5:  Note that there is nothing after this.  If \n");
    printf("  MHD_Solver_5:  we get here, we just stop as we have not \n");
    printf("  MHD_Solver_5:  thought through alternatives, much less  \n");
    printf("  MHD_Solver_5:  implemented any.   \n");
    
    printf("  MHD_Solver_5:  ... Okay, trying to rectify this.  Take  \n");
    printf("  MHD_Solver_5:  the larger value, qh, and add a little to \n");
    printf("  MHD_Solver_5:  it.  In particular, add half the distance \n");
    printf("  MHD_Solver_5:  between it and ql, ie \n");
    printf("  MHD_Solver_5:        qh     = %25.20e \n", qh );
    printf("  MHD_Solver_5:     (qh-ql)/2 = %25.20e \n", (qh-ql)*0.5 );
    }
    qh = qh + 0.5 * (qh - ql) ;  
    if (ltrace) {
        printf("  MHD_Solver_5:  Now try if F(qh_new) is negative.  \n");
    }
    rc = 0 ; 
    rc = func5_p( &F_at_qh, qh, fpar ); 
    if ( F_at_ql > 0.0  &&  F_at_qh < 0.0 )
    { 
      if (ltrace) {
       printf("  MHD_Solver_5:  Hey, that actually worked and F is now \n"); 
       printf("  MHD_Solver_5:     F_at_qh = %25.20e \n", F_at_qh ); 
       printf("  MHD_Solver_5:  We can now go on with a new bracet.  \n"); 
      }
    }
    else if ( F_at_qh > 0.0 )
    { 
      if (ltrace) {
       printf("  MHD_Solver_5:  Well, that didn't work.  Stopping for now.\n");
       printf("  MHD_Solver_5:  I need a new idea.    \n"); 
      }
      exit(2);
      return -4; 
    }
    else 
    { 
      if (ltrace) {
      printf("  MHD_Solver_5:  Something strange, unexpected and weird. \n");
      printf("  MHD_Solver_5:  Stopping.\n");
      } 
      exit(2);
      return -4; 
    }  
  }  


  // With a bracket defined by [ql, qh] = [qg_2, qf_2], we can proceed 
  // with finding the root of F(q).  Take the initial guess to be the midpoint
  // of the bracket. 
  qi = 0.5*(ql + qh);
  q = qi;
 
  // incorporate a little redundancy in the event things go wrong 
  double ql0 = ql;
  double qh0 = qh;
  double q0 = q;

  // And we use rtsafe to find the root of F(q) bracketed between 
  //  ql = qg_2  and  qh = qf_2 
  int xtrace = 0;
  double xtol = 1.0e-14;
  int xrc = rtsafe(func5_p_d, &q, ql, qh, xtol, fpar, xtrace);

  // In the event that rtsafe fails, a postmortem ... 
  if (xrc < 0) 
  {
    // {{{  ... some checking code that needs to be turned on ...  
    printf("  MHD_Solver_5:  The Newton solve (rtsafe) failed  \n");
    printf("  MHD_Solver_5:  when trying to get the root of  F(q).   \n"); 
#if 0
    printf("  MHD_Solver_5:  Dumping the current state,  \n"); 
    printf("      ql  = %25.20e \n",ql); 
    printf("      qh  = %25.20e \n",qh); 
    printf("      qi  = %25.20e \n",qi); 
    printf("      q   = %25.20e \n",q); 
    printf("      ql0 = %25.20e \n",ql0); 
    printf("      qh0 = %25.20e \n",qh0); 
    printf("      q0  = %25.20e \n",q0); 
    printf("             D = %25.20e \n",D);  
    printf("         tau+D = %25.20e \n",a);  
    printf("       D^2/a^2 = %25.20e \n",(D*D)/(a*a));  
    printf("     Ssq/(a*a) = %25.20e \n",Ssq/(a*a));  
    
    double g_at_qg_2, f_at_qg_2,F_at_qg_2 ; 
    double g_at_qf_2, f_at_qf_2,F_at_qf_2 ; 
    func5g_p( &g_at_qg_2, qg_2, fpar) ;  
    func5f_p( &f_at_qg_2, qg_2, fpar) ;  
    func5_p ( &F_at_qg_2, qg_2, fpar) ;  
    func5g_p( &g_at_qf_2, qf_2, fpar) ;  
    func5f_p( &f_at_qf_2, qf_2, fpar) ;  
    func5_p ( &F_at_qf_2, qf_2, fpar) ;  
  
  
    printf("  MHD_Solver_5:          qg_2  = %25.20e \n",qg_2); 
    printf("  MHD_Solver_5:     f_at_qg_2  = %25.20e \n",f_at_qg_2); 
    printf("  MHD_Solver_5:     g_at_qg_2  = %25.20e \n",g_at_qg_2); 
    printf("  MHD_Solver_5:   rhs_at_qg_2  = %25.20e \n",(gamma-1.0)*delta*(qg_2+2.0*alpha_sq)*sqrt(g_at_qg_2)); 
    printf("  MHD_Solver_5: f-rhs_at_qg_2  = %25.20e \n",f_at_qg_2-(gamma-1.0)*delta*(qg_2+2.0*alpha_sq)*sqrt(g_at_qg_2)); 
    printf("  MHD_Solver_5:     F_at_qg_2  = %25.20e \n",F_at_qg_2); 
    printf("  MHD_Solver_5:          qf_2  = %25.20e \n",qf_2); 
    printf("  MHD_Solver_5:     f_at_qf_2  = %25.20e \n",f_at_qf_2); 
    printf("  MHD_Solver_5:     g_at_qf_2  = %25.20e \n",g_at_qf_2); 
    printf("  MHD_Solver_5:   rhs_at_qf_2  = %25.20e \n",(gamma-1.0)*delta*(qf_2+2.0*alpha_sq)*sqrt(g_at_qf_2)); 
    printf("  MHD_Solver_5:f- rhs_at_qf_2  = %25.20e \n",f_at_qf_2-(gamma-1.0)*delta*(qf_2+2.0*alpha_sq)*sqrt(g_at_qf_2)); 
    printf("  MHD_Solver_5:   F_at_qf_2  = %25.20e \n",F_at_qf_2); 
    printf("  MHD_Solver_5:  Recalculating after rtsafe failure ...  \n"); 
#endif
    // }}} 

    ql = ql0;
    qh = qh0;
    q = q0;
    xtrace = 1;
    xrc = rtsafe(func5_p_d, &q, ql, qh, xtol, fpar, xtrace);
    exit(2);
    return -4;
  }

  // If we get to this point, we have had some success and we identify the 
  // most recent root solving to have given us the root of F(q).  To get
  // the corresponding primitive variable hW^2, we have to scale back by 
  // a = tau + D
  q *= a;
 
#if 1 
  // Knowing q (i.e. hW^2, the enthalpy), we can now get the remaining 
  // primitive variables.  Start with the Lorentz factor, W^2  
  double invWsq = 1.0 - ((2.0*q+Bsq)*BS*BS + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq)) ;
 
  double Su[3];
  form_raise_index(Su, Sd, gu);

  u[V_RHO] = D * sqrt( invWsq ) ; 
  u[V_P]   = (gamma-1.0)/gamma*(q*invWsq - u[V_RHO]);
  u[V_VX]  = (Su[0] + BS*Bx/q)/ (q+Bsq);
  u[V_VY]  = (Su[1] + BS*By/q)/ (q+Bsq);
  u[V_VZ]  = (Su[2] + BS*Bz/q)/ (q+Bsq);
  u[V_BX]  = u[U_BX] ;
  u[V_BY]  = u[U_BY] ;
  u[V_BZ]  = u[U_BZ] ;
  u[V_PSI] = u[U_PSI] ;
#endif 

#if 0
  // Knowing q (i.e. hW^2, the enthalpy), we can now get the remaining 
  // primitive variables.  Start with the Lorentz factor, W^2  
  double BS_over_q = BS / q ; 
  double q_pl_Bsq = q + Bsq ; 
  double invWsq =   1.0 
                  - (  (q + q_pl_Bsq) * BS_over_q * BS_over_q 
                      + Ssq
                    )
                      / (q_pl_Bsq * q_pl_Bsq) ; 

  double Su[3];
  form_raise_index(Su, Sd, gu);
 
  double  BS_over_q = BS / q ; 
  double  inv_q_pl_Bsq = 1.0 / q_pl_Bsq ;  
  u[V_RHO] = D * sqrt( invWsq ) ; 
  u[V_P]   = (gamma-1.0)/gamma*(q*invWsq - u[V_RHO]);
  u[V_VX]  = (Su[0] + BS_over_q*Bu[0]) * inv_q_pl_Bsq ; 
  u[V_VY]  = (Su[1] + BS_over_q*Bu[1]) * inv_q_pl_Bsq ; 
  u[V_VZ]  = (Su[2] + BS_over_q*Bu[2]) * inv_q_pl_Bsq ; 
  u[V_BX]  = u[U_BX] ;
  u[V_BY]  = u[U_BY] ;
  u[V_BZ]  = u[U_BZ] ;
  u[V_PSI]  = u[U_PSI] ;
#endif 

  return 0;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the quartic function g(q).  It is part of the 
 *  overall function F(q) whose root gives q, the enthalpy.  The form of 
 *  F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  Note that g(q) sits underneath a square root.  As part of MHD_Solver_5
 *  we need to solve for the largest root of g(q).  We call that qg_2.  
 *  For q < qg_2, sqrt(g) doesn't exist, so we know that the root of F(q)
 *  is bounded from below by qg_2.  However, for now, we just calculate 
 *  g(q).  
 *
 *-------------------------------------------------------------------------*/
int func5g_p(double *g, double q, double *fpar)
{

  double Ssq  = fpar[FP_SSQ];
  double Bsq  = fpar[FP_BSQ] ;
  double BS   = fpar[FP_BS] ;
  
  double D    = fpar[FP_D];
  double tau  = fpar[FP_TAU];

  double  a = tau+D;
  double  a_sq = a*a ;
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ;
  double  xi_sq    = beta_sq - BS*BS / Bsq / a_sq ;

#if 1
  *g =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq)
       - q*q * xi_sq
       - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;
#endif 

#if 0
  double  q_sq = q*q ; 
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 

  *g =   q_sq * ( tmp1_sq - xi_sq ) 
       - tmp1_sq * (beta_sq - xi_sq) ;
#endif 


  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the quartic function g(q) and its derivative 
 *  for use in the root finding routine, rtsafe.  The function g is part  
 *  of the overall function F(q) whose root gives q, the enthalpy.  The 
 *  form of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  Note that g(q) sits underneath a square root.  As part of MHD_Solver_5
 *  we need to solve for the largest root of g(q).  We call that qg_2.  
 *  For q < qg_2, sqrt(g) doesn't exist, so we know that the root of F(q)
 *  is bounded from below by qg_2.  
 *
 *-------------------------------------------------------------------------*/
int func5g_p_d(double *g, double *dg, double q, double *fpar)
{

  double Ssq  = fpar[FP_SSQ];
  double Bsq  = fpar[FP_BSQ] ;
  double BS   = fpar[FP_BS] ;
  
  double D    = fpar[FP_D];
  double tau  = fpar[FP_TAU];

  double  a = tau+D;
  double  a_sq = a*a ;
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ;
  double  xi_sq    = beta_sq - BS*BS / Bsq / a_sq ;

#if 1
  *g =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq)
       - q*q * xi_sq
       - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;

  *dg =   2.0*q*(q+2.0*alpha_sq)*(q+2.0*alpha_sq)
        + 2.0*q*q*(q+2.0*alpha_sq)
        - 2.0*q*xi_sq
        - 2.0*(q+2.0*alpha_sq) * ( beta_sq - xi_sq ) ;
#endif 


#if 0
  double  q_sq = q*q ; 
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 

  *g =   q_sq * ( tmp1_sq - xi_sq ) 
       - tmp1_sq * (beta_sq - xi_sq) ;

  *dg =   2.0 * (   q * (tmp1_sq - xi_sq)  
                  + tmp1 * (q_sq - beta_sq + xi_sq) 
                ) ;
#endif 


  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the quartic function f(q).  It is part of the 
 *  overall function F(q) whose root gives q, the enthalpy.  The form of 
 *  F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  As part of MHD_Solver_5 we need to solve for the largest root of f(q).  
 *  We call that (most of the time) qf_2.  This root will then be an 
 *  upper bound on the the root of F(q).   However, for now, we just 
 *  calculate f(q).  
 *
 *-------------------------------------------------------------------------*/
int func5f_p(double *f, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];
  
  double  Bsq = fpar[FP_BSQ] ;     
  double  BS  = fpar[FP_BS] ;    
                                   
  double  a = tau+D;
  double  a_sq = a*a ; 
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ; 
  double  xi_sq    = beta_sq - BS*BS / (Bsq * a_sq) ;
             
#if 1 
  *f =    (q+2.0*alpha_sq)
         *(   (q+2.0*alpha_sq) * ( -  (q-0.5*gamma*(1.0-alpha_sq))
                                     *(q-0.5*gamma*(1.0-alpha_sq))
                                   +  (0.5*gamma*(1.0-alpha_sq))
                                     *(0.5*gamma*(1.0-alpha_sq))
                                   - (gamma-1.0)*beta_sq
                                 )
            + (3.0*gamma-4.0)*alpha_sq*xi_sq
          )  
       
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq ;
#endif 

#if 0
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp2 = 0.5 * gamma * (1.0 - alpha_sq) ; 
  //double  tmp2_sq = tmp2 * tmp2 ; 
  double  tmp3 = (3.0*gamma - 4.0) * alpha_sq * xi_sq ; 
  //double  tmp4 = - (q-tmp2)*(q-tmp2) + tmp2_sq - (gamma-1.0)*beta_sq ; 
  double  tmp4 = q * ( 2.0*tmp2 - q) - (gamma-1.0)*beta_sq ; 

  *f =   tmp1 * ( tmp1 * tmp4 + tmp3 )  
       
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq ;
#endif 
 
  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the quartic function f(q) and its derivative
 *  for use in the root finding routine, rtsafe.  The function f is part 
 *  of the overall function F(q) whose root gives q, the enthalpy.  The 
 *  form of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  As part of MHD_Solver_5 we need to solve for the largest root of f(q).  
 *  We call that (most of the time) qf_2.  This root will then be an 
 *  upper bound on the the root of F(q).   
 *
 *-------------------------------------------------------------------------*/
int func5f_p_d(double *f, double *df, double q, double *fpar)
{

  double Ssq   = fpar[FP_SSQ];
  double D     = fpar[FP_D];
  double tau   = fpar[FP_TAU];
  double gamma = fpar[FP_GAMMA];
  
  double  Bsq = fpar[FP_BSQ] ;     
  double  BS  = fpar[FP_BS] ;    
                                   
  double  a = tau+D;
  double  a_sq = a*a ; 
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ; 
  double  xi_sq    = beta_sq - BS*BS / (Bsq * a_sq) ;
             
#if 1 
  *f =    (q+2.0*alpha_sq)
         *(   (q+2.0*alpha_sq) * ( -  (q-0.5*gamma*(1.0-alpha_sq))
                                     *(q-0.5*gamma*(1.0-alpha_sq))
                                   +  (0.5*gamma*(1.0-alpha_sq))
                                     *(0.5*gamma*(1.0-alpha_sq))
                                   - (gamma-1.0)*beta_sq
                                 )
            + (3.0*gamma-4.0)*alpha_sq*xi_sq
          )  
       
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq ;
         
  *df =   2.0*(q+2.0*alpha_sq)*( -  (q-0.5*gamma*(1.0-alpha_sq))
                                   *(q-0.5*gamma*(1.0-alpha_sq))
                                 +  (0.5*gamma*(1.0-alpha_sq))
                                   *(0.5*gamma*(1.0-alpha_sq))
                                 - (gamma-1.0)*beta_sq
                               )
        - 2.0*(q+2.0*alpha_sq)*(q+2.0*alpha_sq)*(q-0.5*gamma*(1.0-alpha_sq))
        + (3.0*gamma-4.0)*alpha_sq*xi_sq  ; 
#endif 

#if 0
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp2 = 0.5 * gamma * (1.0 - alpha_sq) ; 
  double  tmp3 = (3.0*gamma - 4.0) * alpha_sq * xi_sq ; 
  double  tmp4 = q * ( 2.0*tmp2 - q) - (gamma-1.0)*beta_sq ; 

  *f =   tmp1 * ( tmp1 * tmp4 + tmp3 )  
       
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq ;
         
  *df =   2.0*tmp1 * ( tmp4 - tmp1*(q-tmp2) ) + tmp3 ; 
#endif 
 
  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the function F(q) whose root we want to find 
 *  to give the enthalpy.  The form of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *
 *-------------------------------------------------------------------------*/
int func5_p(double *f, double q, double *fpar)  
{                                  
                                 
  double  Ssq   = fpar[FP_SSQ]; 
  double  Bsq   = fpar[FP_BSQ] ; 
  double  BS    = fpar[FP_BS] ; 
             
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];
       
  double dis, sqrt_dis ;
           
  double  a = tau+D;
  double  a_sq = a*a ;
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ; 
  double  delta    = D / a ; 
  double  xi_sq    = beta_sq - BS*BS / Bsq / a_sq ; 

  dis =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq) 
        - q*q * xi_sq
        - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;

#if 0
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 
  double  q_sq = q*q ; 
 
  dis =   tmp1_sq * (   q_sq 
                      + beta_sq 
                      - xi_sq ) 
        - q_sq * xi_sq ;   
#endif


  /* {{{ 
  // Originally, if 0 < dis < 1.e-18, we would floor dis to 1.e-18.  However,
  // there were cases, where this affected the bracket on the root.  So we don't
  // do that any more.  We push a small negative value back to the positive 
  // range, but if dis is positive, we don't change it.   
  if ( dis < 1.e-18 ) 
  { 
    dis = 1.e-18; 
  }
  }}} */ 
  
  // If the argument of the square root is a small negative value, 
  // we will cheat, assume that this is just roundoff error, force the 
  // value to be a very small positive number and hope that we can go on. 
  if ( dis > -6.e-16  &&  dis < 0.0 )  
  {
#if 0
     printf("  func5_p:  The argument of a square root is negative\n");
//{{{  ... some tracing if turned on ...  
     printf("  func5_p:  by a very tiny amount (machine zero essentially).\n");
     printf("  func5_p:  We will bump up the value so that it is positive\n");
     printf("  func5_p:  This is admittedly rather ad hoc, but hopefully \n");
     printf("  func5_p:  it will allow us to keep running.  \n");
     printf("  func5_p:     dis = %25.20e \n",dis ) ; 
     printf("           tau  = %15.10e \n",tau) ; 
     printf("            D   = %15.10e \n",D  ) ; 
     printf("          tau+D = %15.10e \n",tau+D  ) ; 
     printf("           Ssq  = %15.10e \n",Ssq ) ; 
     printf("            q   = %15.10e \n",q ) ; 
     printf("           dis  = %15.10e \n",dis) ; 
     printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
     printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif
//}}}
     dis = 3.6e-31 ;
  }
  // If the argument of the square root is negative enough, just return. 
  else if ( dis > -1.e-14  &&  dis <= -6.e-16 )  
  {
     printf("  func5_p:  The argument of a square root is negative\n");
     printf("  func5_p:  by more than machine zero.  This is more \n");
     printf("  func5_p:  difficult to handle.  We will adjust the \n");
     printf("  func5_p:  value of q upward by the amount that the \n");
     printf("  func5_p:  argument of the square root is negative  \n");
     printf("  func5_p:  in hopes that this will allow us to continue  \n");
     printf("  func5_p:  This should work at some level as we are \n");
     printf("  func5_p:  trying to find the value of F(q) at this \n");
     printf("  func5_p:  q value.  If the q that comes is renders \n");
     printf("  func5_p:  a negative argument of the square root, it\n");
     printf("  func5_p:  is probably because it is close to the  \n");
     printf("  func5_p:  root of g anyway.   \n");
     printf("  func5_p:     dis = %25.20e \n",dis ) ; 
//{{{ ... some tracing if turned on ... 
#if 0 
     printf("           tau  = %15.10e \n",tau) ;
     printf("            D   = %15.10e \n",D  ) ;
     printf("          tau+D = %15.10e \n",tau+D  ) ;
     printf("           Ssq  = %15.10e \n",Ssq ) ;
     printf("            q   = %15.10e \n",q ) ;
     printf("           dis  = %15.10e \n",dis) ;
     printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ;
     printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ;
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ;
#endif
//}}}
     
     q += fabs(dis) ; 
     dis =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq) 
           - q*q * xi_sq
           - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;
     if ( dis > -6.e-16  &&  dis < 0.0 )  
     {
       printf("  func5_p:  The argument of the square root is still \n");   
       printf("  func5_p:  negative, even after shifting q.  But it \n"); 
       printf("  func5_p:  is not so much so, that we won't still try \n"); 
       printf("  func5_p:  to go on.   \n"); 
       dis = 3.6e-31;  
     }
     else if ( dis < -6.e-16 )  
     {
       printf("  func5_p:  The argument of the square root is still \n");   
       printf("  func5_p:  negative, even after shifting q; so much \n"); 
       printf("  func5_p:  so that we will stop here.  \n"); 
       exit(-2);
       return -1 ;
     }
  
  }

  sqrt_dis = sqrt(dis) ;

#if 1 
  *f = (   (q+2.0*alpha_sq)
         *(   (q+2.0*alpha_sq) * ( -  (q-0.5*gamma*(1.0-alpha_sq))
                                     *(q-0.5*gamma*(1.0-alpha_sq))
                                   +  (0.5*gamma*(1.0-alpha_sq))
                                     *(0.5*gamma*(1.0-alpha_sq))
                                   - (gamma-1.0)*beta_sq
                                 )
            + (3.0*gamma-4.0)*alpha_sq*xi_sq
          )
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq ) 

       - (gamma-1.0) * delta * (q+2.0*alpha_sq) * sqrt_dis ;
#endif 

#if 0 
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 
  double  tmp2 = 0.5 * gamma * (1.0 - alpha_sq) ; 
  double  q_sq = q*q ; 
  double  tmp3 = (3.0*gamma - 4.0) * alpha_sq * xi_sq ; 
  double  tmp4 = 2.0*(gamma - 2.0) * alpha_sq * alpha_sq * xi_sq ; 
  double  tmp5 = - (q-tmp2)*(q-tmp2) + tmp2*tmp2 - (gamma-1.0)*beta_sq ; 
  double  tmp6 = (gamma - 1.0) * delta ; 
 
  dis =   tmp1_sq * (   q_sq 
                      + beta_sq 
                      - xi_sq ) 
        - q_sq * xi_sq ;   

  if ( dis < 1.e-18 ) { dis = 1.e-18; }
  sqrt_dis = sqrt(dis) ;

  *f =   tmp1 * ( tmp1 * tmp5 + tmp3 ) 
       - tmp4 
       - tmp6 * tmp1 * sqrt_dis ;
#endif 


  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the function F(q) and its derivative whose 
 *  root we want to find to give the enthalpy.  The form of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  where f and g are quartic functions of q.  
 *
 *-------------------------------------------------------------------------*/
int func5_p_d(double *f, double *df, double q, double *fpar)  
{                                  
                                 
  double  Ssq   = fpar[FP_SSQ]; 
  double  Bsq   = fpar[FP_BSQ] ; 
  double  BS    = fpar[FP_BS] ; 
             
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];
       
  double dis, sqrt_dis ;
           
  double  a = tau+D;
  double  a_sq = a*a ;
  double  alpha_sq = 0.5 * Bsq / a ;
  double  beta_sq  = Ssq / a_sq ; 
  double  delta    = D / a ; 
  double  xi_sq    = beta_sq - BS*BS / Bsq / a_sq ; 

  dis =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq) 
        - q*q * xi_sq
        - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;

#if 0
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 
  double  q_sq = q*q ; 
 
  dis =   tmp1_sq * (   q_sq 
                      + beta_sq 
                      - xi_sq ) 
        - q_sq * xi_sq ;   
#endif

  
  if ( dis > -6.e-16  &&  dis < 0.0 )  
  {
#if 0
     printf("  func5_p_d:  The argument of a square root is negative by a\n");
//{{{  ... some tracing if turned on ...  
     printf("  func5_p_d:  very tiny amount (machine zero essentially).\n");
     printf("  func5_p_d:  We will bump up the value so that it is positive\n");
     printf("  func5_p_d:  This is admittedly rather ad hoc, but hopefully \n");
     printf("  func5_p_d:  it will allow us to keep running.  \n");
     printf("  func5_p_d:     dis = %25.20e \n",dis ) ; 
     printf("           tau  = %15.10e \n",tau) ; 
     printf("            D   = %15.10e \n",D  ) ; 
     printf("          tau+D = %15.10e \n",tau+D  ) ; 
     printf("           Ssq  = %15.10e \n",Ssq ) ; 
     printf("            q   = %15.10e \n",q ) ; 
     printf("           dis  = %15.10e \n",dis) ; 
     printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
     printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif
//}}}
     dis = 3.6e-31 ;
  }
  else if ( dis > -1.e-14  &&  dis <= -6.e-16 )  
  {
     printf("  func5_p_d:  The argument of a square root is negative\n");
     printf("  func5_p_d:  by more than machine zero.  This is more \n");
     printf("  func5_p_d:  difficult to handle.  We will adjust the \n");
     printf("  func5_p_d:  value of q upward by the amount that the \n");
     printf("  func5_p_d:  argument of the square root is negative  \n");
     printf("  func5_p_d:  in hopes that this will allow us to continue  \n");
     printf("  func5_p_d:  This should work at some level as we are \n");
     printf("  func5_p_d:  trying to find the value of F(q) at this \n");
     printf("  func5_p_d:  q value.  If the q that comes is renders \n");
     printf("  func5_p_d:  a negative argument of the square root, it\n");
     printf("  func5_p_d:  is probably because it is close to the  \n");
     printf("  func5_p_d:  root of g anyway.   \n");
     printf("  func5_p_d:     dis = %25.20e \n",dis ) ; 
//{{{ ... some tracing if turned on ... 
#if 0 
     printf("           tau  = %15.10e \n",tau) ;
     printf("            D   = %15.10e \n",D  ) ;
     printf("          tau+D = %15.10e \n",tau+D  ) ;
     printf("           Ssq  = %15.10e \n",Ssq ) ;
     printf("            q   = %15.10e \n",q ) ;
     printf("           dis  = %15.10e \n",dis) ;
     printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ;
     printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ;
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ;
#endif
//}}}
     
     q += fabs(dis) ; 
     dis =   q*q * (q+2.0*alpha_sq)*(q+2.0*alpha_sq) 
           - q*q * xi_sq
           - (q+2.0*alpha_sq)*(q+2.0*alpha_sq) * (beta_sq - xi_sq) ;
     if ( dis > -6.e-16  &&  dis < 0.0 )  
     {
       printf("  func5_p:  The argument of the square root is still \n");   
       printf("  func5_p:  negative, even after shifting q.  But it \n"); 
       printf("  func5_p:  is not so much so, that we won't still try \n"); 
       printf("  func5_p:  to go on.   \n"); 
       dis = 3.6e-31;  
     }
     else if ( dis < -6.e-16 )  
     {
       printf("  func5_p:  The argument of the square root is still \n");   
       printf("  func5_p:  negative, even after shifting q; so much \n"); 
       printf("  func5_p:  so that we will stop here.  \n"); 
       exit(-2);
       return -1 ;
     }
  
  }
  
  sqrt_dis = sqrt(dis) ;

#if 1 
  *f =    (q+2.0*alpha_sq)
         *(   (q+2.0*alpha_sq) * ( -  (q-0.5*gamma*(1.0-alpha_sq))
                                     *(q-0.5*gamma*(1.0-alpha_sq))
                                   +  (0.5*gamma*(1.0-alpha_sq))
                                     *(0.5*gamma*(1.0-alpha_sq))
                                   - (gamma-1.0)*beta_sq
                                 )
            + (3.0*gamma-4.0)*alpha_sq*xi_sq
          )
       - 2.0*(gamma-2.0)*alpha_sq*alpha_sq*xi_sq

       - (gamma-1.0) * delta * (q+2.0*alpha_sq) * sqrt_dis ;

  *df =   2.0*(q+2.0*alpha_sq)*( -  (q-0.5*gamma*(1.0-alpha_sq))
                                   *(q-0.5*gamma*(1.0-alpha_sq))
                                 +  (0.5*gamma*(1.0-alpha_sq))
                                   *(0.5*gamma*(1.0-alpha_sq))
                                 - (gamma-1.0)*beta_sq
                               )
        - 2.0*(q+2.0*alpha_sq)*(q+2.0*alpha_sq)*(q-0.5*gamma*(1.0-alpha_sq))
        + (3.0*gamma-4.0)*alpha_sq*xi_sq

       - (gamma-1.0)*delta*(q+2.0*alpha_sq)
          *(   q*(q+2.0*alpha_sq)*(q+2.0*alpha_sq)
             + q*q*(q+2.0*alpha_sq)
             - q*xi_sq
             //+ (beta_sq-xi_sq)*(q+2.0*alpha_sq)
             - (beta_sq-xi_sq)*(q+2.0*alpha_sq)
           ) / sqrt_dis
       - (gamma-1.0)*delta*sqrt_dis ;
#endif 

#if 0 
  double  tmp1 = q + 2.0*alpha_sq ;  
  double  tmp1_sq = tmp1 * tmp1 ; 
  double  tmp2 = 0.5 * gamma * (1.0 - alpha_sq) ; 
  double  q_sq = q*q ; 
  double  tmp3 = (3.0*gamma - 4.0) * alpha_sq * xi_sq ; 
  double  tmp4 = 2.0*(gamma - 2.0) * alpha_sq * alpha_sq * xi_sq ; 
  double  tmp5 = - (q-tmp2)*(q-tmp2) + tmp2*tmp2 - (gamma-1.0)*beta_sq ; 
  double  tmp6 = (gamma - 1.0) * delta ; 
 
  dis =   tmp1_sq * (   q_sq 
                      + beta_sq 
                      - xi_sq ) 
        - q_sq * xi_sq ;   

  sqrt_dis = sqrt(dis) ;

  *f =   tmp1 * ( tmp1 * tmp5 + tmp3 ) 
       - tmp4 
       - tmp6 * tmp1 * sqrt_dis ;

  *df =   2.0 * tmp1 * ( tmp5 - tmp1 * (q-tmp2) )
        + tmp3 

       - tmp6 * (   tmp1
                     * (   q * (tmp1_sq - xi_sq) 
                         //+ tmp1 * ( q_sq + beta_sq - xi_sq )
                         + tmp1 * ( q_sq - beta_sq + xi_sq )
                       ) / sqrt_dis
                  + sqrt_dis 
                ) ;
#endif 


  return 1;
}




/*-------------------------------------------------------------------------
 *
 *  This is an alternative solver to MHD_Solver_5.  In cases where the 
 *  velocity is nonzero, but lies completely along the magnetic field, 
 *  the perpendicular component of the velocity vanishes, 
 *  i.e. (v_perp)^2 = 0 . 
 *
 *  In this case the single equation for F(q)  simplifies somewhat.  In 
 *  particular, the form for F(q) becomes 
 *      F(q) = f(q) - (const) * sqrt(g(q)) = 0 
 *  where  f  and  g  both become quadratics (rather than 
 *  quartics).  This form is essentially the same as in the pure fluid 
 *  case.  Thus this is largely a copy of MHD_Solver_4.  It is not 
 *  exactly the same, but very close.  
 *
 *-------------------------------------------------------------------------*/
int MHD_Solver_5_vperp0(  double *u, 
                          double *xpt, 
                          double gd[3][3], 
                          double gu[3][3],
                          double *fpar      )
{
  const int ltrace = 1;

  double gamma = pars.gamma;

  double Ssq  = fpar[FP_SSQ];
  double Bsq  = fpar[FP_BSQ];
  double BS   = fpar[FP_BS ];
  
  double D    = u[U_D];
  double tau  = u[U_TAU];
  double Bx   = u[U_BX] ;  
  double By   = u[U_BY] ;  
  double Bz   = u[U_BZ] ;  
  
  double Sd[3] ; 
  
  Sd[0] = u[U_SX] ; 
  Sd[1] = u[U_SY] ; 
  Sd[2] = u[U_SZ] ; 

//  int j ; 
  double  qf_1, qf_2, qg_2, qf_max ; //roots of f and g; location of max of f
  //double ql, qh, f_at_ql, f_at_qh, f_at_qh_v2 ;   
  double  ql, qh;
  double  f_at_ql = 0.0 ; // some bizarre iniialization need 
  double  f_at_qh, f_at_qh_v2 ;   
  //double  rescale_factor1 ; 
  double  a = tau + D;  // a combo used to rescale all other variables 
  double  a_sq = a*a;  // a combo used to rescale all other variables 
  double  alpha_sq = 0.5*Bsq/a ; 
  double  delta_sq = (D*D) / a_sq ; 
  double  t1 = 0.5 * gamma * (1.0 - alpha_sq) ;
  double  beta_sq = Ssq/a_sq ; // rescaled momentum; must satisfy 0<=beta_sq<1; if identically zero, is because v^2=0  
  double  t2 = t1*t1 - (gamma-1.0)*beta_sq ;
  // t2 shows up under a square root, so we check to see if it is < 0. 
  if (t2 < 0.0) 
  {
   if (ltrace) {
    printf("  MHD_Solver_5_vperp0:  We have a problem with an upper bound.\n");
    printf("  MHD_Solver_5_vperp0:  The arg of a square root, t2, should\n");
    printf("  MHD_Solver_5_vperp0:  be nonnegative, but in fact calculates\n");
    printf("  MHD_Solver_5_vperp0:  to be t2 = %25.20e\n",t2);
    printf("  MHD_Solver_5_vperp0:        t1 = %25.20e\n",t1);
    printf("  MHD_Solver_5_vperp0:  alpha_sq = %25.20e\n",alpha_sq);
    printf("  MHD_Solver_5_vperp0:   beta_sq = %25.20e\n",beta_sq);
    printf("  MHD_Solver_5_vperp0:  delta_sq = %25.20e\n",delta_sq);
    printf("  MHD_Solver_5_vperp0:      a_sq = %25.20e\n",a_sq);
    printf("  MHD_Solver_5_vperp0:       tau = %25.20e\n",tau);
    printf("  MHD_Solver_5_vperp0:         D = %25.20e\n",D);
    printf("  MHD_Solver_5_vperp0:  FP_COORD_X = %25.20e\n",fpar[FP_COORD_X]);
    printf("  MHD_Solver_5_vperp0:  FP_COORD_Y = %25.20e\n",fpar[FP_COORD_Y]);
    printf("  MHD_Solver_5_vperp0:  FP_COORD_Z = %25.20e\n",fpar[FP_COORD_Z]);
    printf("  MHD_Solver_5_vperp0:  Bottom line, beta^2 is probably too \n");
    printf("  MHD_Solver_5_vperp0:  large.  We need to scale it down.  \n");
    printf("  MHD_Solver_5_vperp0:  First check if the violation is machine\n");
    printf("  MHD_Solver_5_vperp0:  zero small. \n");
   }

    if ( fabs(t2) < 6.e-16  &&  fabs(t2) < (gamma-1.)/gamma*beta_sq ) 
    { 
      beta_sq -= gamma / (gamma - 1.0) * fabs(t2) ;  
      double Ssq_old = Ssq ; 
      Ssq = beta_sq * a_sq ; 
      Sd[0] *= ( Ssq / Ssq_old ) ;  
      Sd[1] *= ( Ssq / Ssq_old ) ;  
      Sd[2] *= ( Ssq / Ssq_old ) ;  
      BS = Bx * Sd[0] + By * Sd[1] + Bz * Sd[2] ; 
      beta_sq = Ssq / a_sq ; 
      t2 = t1*t1 - (gamma-1.0)*beta_sq; 
      if (t2 < 0.0) 
      { 
       if (ltrace) { 
        printf("  MHD_Solver_5_vperp0:  Still does not work.  I am out of\n");
        printf("  MHD_Solver_5_vperp0:  ideas, so we will stop here. \n");
        printf("  MHD_Solver_5_vperp0:        t2 = %25.20e\n",t2);
        printf("  MHD_Solver_5_vperp0:        t1 = %25.20e\n",t1);
        printf("  MHD_Solver_5_vperp0:  alpha_sq = %25.20e\n",alpha_sq);
        printf("  MHD_Solver_5_vperp0:   beta_sq = %25.20e\n",beta_sq);
        printf("  MHD_Solver_5_vperp0:  delta_sq = %25.20e\n",delta_sq);
        printf("  MHD_Solver_5_vperp0:      a_sq = %25.20e\n",a_sq);
        printf("  MHD_Solver_5_vperp0:       tau = %25.20e\n",tau);
        printf("  MHD_Solver_5_vperp0:         D = %25.20e\n",D);
       }

        exit(2); 
      }
      fpar[FP_BS] = BS ; 
      fpar[FP_SSQ] = Ssq ;
      u[U_SX] = Sd[0] ; 
      u[U_SY] = Sd[1] ; 
      u[U_SZ] = Sd[2] ; 
    }
    else 
    { 
     if (ltrace) { 
      printf("  MHD_Solver_5_vperp0:  The naive thing didn't work.\n");
      printf("  MHD_Solver_5_vperp0:  The quantity t2 takes a value out \n");
      printf("  MHD_Solver_5_vperp0:  of range with respect to our idea \n");
      printf("  MHD_Solver_5_vperp0:  of how to handle it. \n");
      exit(2); 
     } 
    } 
  }
  double  sqrt_t2 = sqrt(t2) ; 
  
  // We can calculate the roots of f and g exactly as they are quadratics.   
  qf_1 = t1 - sqrt_t2 ; // the smaller root of f   
  qf_2 = t1 + sqrt_t2 ; // the larger root of f   
  qf_max = t1 ;         // the location of the max of f  
  qg_2 = sqrt(beta_sq); // the larger root of g  


  /*
{{{
  We check whether qg_2 comes at all close to colliding with qf_1.  If  
  it does, we add a small amount to qg_2 so that it is larger than qf_1.  
  We also check if we can use qf_max instead, i.e. whether qg_2 < qf_max.  
  If so, we can use the location of the maximum of f as the lower bound 
  for q.   
}}}
  */ 
  if ( fabs( (qg_2 - qf_1) / (qg_2 + qf_1 ) ) < 1.e-6 )
  { 
   if (ltrace) { 
    printf("  MHD_Solver_5_vperp0:  Roots are colliding near q=0. \n");  
    printf("  MHD_Solver_5_vperp0:  Adding a 1.e-5 piece to the larger \n"); 
    printf("  MHD_Solver_5_vperp0:  hoping this resolves the problem. \n"); 
    qg_2 += 1.e-5 ; 
   }
  }
  
  // Set the lower bound to qg_2 unless qg_2 is smaller than the location of 
  // the maximum of f.  
  ql = fmax( qg_2, qf_max ) ; 

  // This simply initializes the upper bound to what we hope is true.  
  // The following checks this to make sure.   
  qh = qf_2 ; 
  
  /* {{{  
  FIXME  this is in fact mostly wrong as it applies to MHD_Solver_4 but not
         necessarily MHD_Solver_5_vperp0.  We need to fix these cases and 
         this documentation to reflect that.  
         
  We consider possibilities for the relative location of qg_2 and qf_2 as 
  they should be the lower and upper bounds, respectively, for our root, q.
  
  Case 1 is that qg_2 < qf_2 and a physical inequality is obeyed.  All is well
  in this case and the Newton solve should proceed without issue. 
  Case 2 considers the violation of another physical inequality and adjusts 
  (rescales) the components of S in order to enforce the inequality. 
  Case 3 considers the numerical "vanishing" of D/a relative to beta_sq which
  can also numerically violate physical inequalities.  
  Case 4 considers the (seemingly remote but nonetheless realized) possibility
  that qg_2 and qf_2 are exactly the same (to machine precision).  

  No claim is made that these are exhaustive.  They were simply cases that 
  arose in debugging the code and trying to make this primitive solver as 
  robust as possible.  There is a sort of "ad hoc-ness" to some of these.  
  Could they be improved?  Probably.  But for now they seem to work.  
  }}}  */
  if ( qg_2 < qf_2  &&  beta_sq < 1.0 ) // Case 1  
  {
    qh = qf_2 ;  
  }
  //  This is a generalization of an inequality in the pure fluid case (S^2 < (tau+D)^2 - D^2) to the case of MHD with v_perp^2=0.  
  else if ( beta_sq + delta_sq >= (1.0-alpha_sq)*(1.0-alpha_sq) ) // Case 2 
  { 
   if (ltrace) { 
    printf("  MHD_Solver_5_vperp0:  A physical inequality is being    \n");
    printf("  MHD_Solver_5_vperp0:  violated.  Namely, the quantity   \n");
    printf("  MHD_Solver_5_vperp0:     (tau+D-Bsq/2)^2 - Ssq - D^2    \n");
    printf("  MHD_Solver_5_vperp0:  should be greater than 0. Instead \n");
    printf("  MHD_Solver_5_vperp0:  it (rescaled by (tau+D)^2) is \n");
    printf("  MHD_Solver_5_vperp0:     (1-alpha_sq)^2 - beta^2 - delta^2 = %25.20e \n",(1.0-alpha_sq)*(1.0-alpha_sq) - beta_sq - delta_sq);
    /*
    printf("  MHD_Solver_5_vperp0:  where:     \n" ); 
    printf("  MHD_Solver_5_vperp0:            tau = %25.20e \n", tau ); 
    printf("  MHD_Solver_5_vperp0:             D  = %25.20e \n", D   ); 
    printf("  MHD_Solver_5_vperp0:            Bsq = %25.20e \n", Bsq ); 
    printf("  MHD_Solver_5_vperp0:            Ssq = %25.20e \n", Ssq ); 
    printf("  MHD_Solver_5_vperp0:            BS  = %25.20e \n", BS ); 
    printf("  MHD_Solver_5_vperp0:          Sd[0] = %25.20e \n", Sd[0] ); 
    printf("  MHD_Solver_5_vperp0:          Sd[1] = %25.20e \n", Sd[1] ); 
    printf("  MHD_Solver_5_vperp0:          Sd[2] = %25.20e \n", Sd[2] ); 
    printf("  MHD_Solver_5_vperp0:        alpha^2 = %25.20e \n", alpha_sq ); 
    printf("  MHD_Solver_5_vperp0:  (1-alpha^2)^2 = %25.20e \n", (1.-alpha_sq)*(1.-alpha_sq) ); 
    printf("  MHD_Solver_5_vperp0:         beta^2 = %25.20e \n", beta_sq ); 
    printf("  MHD_Solver_5_vperp0:        delta^2 = %25.20e \n", delta_sq ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_X] = %25.20e \n", fpar[FP_COORD_X] ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_Y] = %25.20e \n", fpar[FP_COORD_Y] ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_Z] = %25.20e \n", fpar[FP_COORD_Z] ); 
    printf("  MHD_Solver_5_vperp0:  We will rescale Ssq in hopes this \n"); 
    printf("  MHD_Solver_5_vperp0:  resolves the problem ... \n"); 
      printf("  MHD_Solver_5_vperp0:  \n") ; 
      printf("  MHD_Solver_5_vperp0: But first, a maple dump ...  \n") ; 
      printf("  MHD_Solver_5_vperp0:         ql = %25.20e \n", ql); 
      printf("  MHD_Solver_5_vperp0:         qh = %25.20e \n", qh); 
      printf("  MHD_Solver_5_vperp0:          tau = %25.20e \n", tau); 
      printf("  MHD_Solver_5_vperp0:           D  = %25.20e \n",  D ); 
      printf("  MHD_Solver_5_vperp0:          Ssq = %25.20e \n", Ssq); 
      printf("  MHD_Solver_5_vperp0:          Bsq = %25.20e \n", Bsq); 
      printf("  MHD_Solver_5_vperp0:           BS = %25.20e \n", BS); 
      printf("  MHD_Solver_5_vperp0:     alpha_sq = %25.20e \n", alpha_sq); 
      printf("  MHD_Solver_5_vperp0:      beta_sq = %25.20e \n", beta_sq); 
      //printf("  MHD_Solver_5_vperp0:        xi_sq = %25.20e \n", xi_sq); 
      printf("  MHD_Solver_5_vperp0:  tau+D-Bsq/2 = %25.20e \n", tau+D-0.5*Bsq); 
    printf("  MHD_Solver_5_vperp0:===== MAPLE DUMP ========================\n");
    printf("   restart: with(plots): \n");  
    printf("   alpha_sq := %25.20e ; \n", alpha_sq );  
    printf("   beta_sq  := %25.20e ; \n", beta_sq  );  
    //printf("     xi_sq  := %25.20e ; \n", xi_sq    );  
    printf("   gamma1   := %25.20e ; \n", gamma    );  
    printf("   delta1   := %25.20e ; \n", D/a      );  
    printf("   f(q) :=                          \n"); 
    printf("          -  (q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("            *(q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("          +  (0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("            *(0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("          -  (gamma1-1.0)*beta_sq   ;     \n");
    printf("   plot(f(q), q=-0.5..1) ; \n");  
    printf("   g(q) :=  q*q - beta_sq ;   \n");
    printf("   g_1(q) :=  (gamma1-1.)*delta1*sqrt(g(q));\n");
    printf("   plot(g(q), q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g(q)}, q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g_1(q)}, q=-0.5..1) ; \n");  
    printf("===============================================================\n");
    */ 
    }

    /* 
    We rescale the components of Sd and hence Ssq in order that the 
    inequality is enforced.  

    FIXME  Note that this scaling factor is a bit arbitrary.  It would 
    be well to experiment with the value added in.  
    */

//  try what is in check_Ssq ... 
    double  Ssq2 = 0.0; 
    double  Ssq_max = pow( tau + D - 0.5*Bsq , 2) - D*D ; 
    double  t_min = sqrt( (Ssq_max / Ssq) - 6.e-16 ) ;  
    for ( int i0=0; i0<3; i0++ ) 
    { Sd[i0] *= t_min ; } 
    Ssq2 = square_form(Sd, gu); 
    u[U_SX] = Sd[0] ; 
    u[U_SY] = Sd[1] ; 
    u[U_SZ] = Sd[2] ; 
    BS = Bx * Sd[0] + By * Sd[1] + Bz * Sd[2] ; 
    fpar[FP_SSQ] = Ssq2 ; 
    fpar[FP_BS] = BS ; 
    Ssq = Ssq2 ; 
    beta_sq = Ssq/(a*a); 
    if (ltrace) {
        /*
    printf("  MHD_Solver_5_vperp0:          tau = %25.20e \n", tau); 
    printf("  MHD_Solver_5_vperp0:           D  = %25.20e \n",  D ); 
    printf("  MHD_Solver_5_vperp0:          Ssq = %25.20e \n", Ssq); 
    printf("  MHD_Solver_5_vperp0:          Bsq = %25.20e \n", Bsq); 
    printf("  MHD_Solver_5_vperp0:           BS = %25.20e \n", BS); 
    printf("  MHD_Solver_5_vperp0:     alpha_sq = %25.20e \n", alpha_sq); 
    printf("  MHD_Solver_5_vperp0:      beta_sq = %25.20e \n", beta_sq); 
    */
    printf("  MHD_Solver_5_vperp0:  The inequality again ... \n");
    printf("  MHD_Solver_5_vperp0:     (1-alpha_sq)^2 - beta^2 - delta^2 = %25.20e \n",(1.0-alpha_sq)*(1.0-alpha_sq) - beta_sq - delta_sq);
    printf("  MHD_Solver_5_vperp0:    fpar[FP_VSQ] = %25.20e \n", fpar[FP_VSQ]);
    printf("  MHD_Solver_5_vperp0:         u[V_VX] = %25.20e \n", u[V_VX]); 
    printf("  MHD_Solver_5_vperp0:         u[V_VY] = %25.20e \n", u[V_VY]); 
    printf("  MHD_Solver_5_vperp0:         u[V_VZ] = %25.20e \n", u[V_VZ]); 
    printf("  MHD_Solver_5_vperp0: FP_COORD_X = %25.20e\n",fpar[FP_COORD_X]); 
    printf("  MHD_Solver_5_vperp0: FP_COORD_Y = %25.20e\n",fpar[FP_COORD_Y]); 
    printf("  MHD_Solver_5_vperp0: FP_COORD_Z = %25.20e\n",fpar[FP_COORD_Z]); 
    printf("  MHD_Solver_5_vperp0:      D     = %25.20e \n", D); 
    printf("  MHD_Solver_5_vperp0:      Sd[0] = %25.20e \n", Sd[0]); 
    printf("  MHD_Solver_5_vperp0:      Sd[1] = %25.20e \n", Sd[1]); 
    printf("  MHD_Solver_5_vperp0:      Sd[2] = %25.20e \n", Sd[2]); 
    printf("  MHD_Solver_5_vperp0:      tau   = %25.20e \n", tau); 
    printf("  MHD_Solver_5_vperp0:      Bx    = %25.20e \n", Bx); 
    printf("  MHD_Solver_5_vperp0:      By    = %25.20e \n", By); 
    printf("  MHD_Solver_5_vperp0:      Bz    = %25.20e \n", Bz); 
    }

//  try something different ... 
#if 0
    double D_old = D ; 
    //double tau_old = tau ;
    double a_old = D+tau ; 
    double eps1, eps1_a ; 
    double Smag, Smag_old ; 
    double eps_sq = fabs(   (1.0-alpha_sq)*(1.0-alpha_sq) 
                          - (beta_sq + delta_sq) 
                        ) ; 
    
    eps1 = sqrt(eps_sq) ; 
    eps1_a = 0.5 * eps1 * a_old ; 
   
    // we now add to the density and take away from tau so that tau+D remains the same ... 
    //if ( beta_sq > delta_sq )
    //{
       if ( D_old > eps1_a ) 
       { 
          D  -= eps1_a ;
         tau += eps1_a ; 
       }
    //}
    //else if ( beta_sq < delta_sq ) 
    //{ 
       //D  -= eps1_a ; 
      //tau += eps1_a ; 
    //}
    
    Smag_old = sqrt( Ssq ); 
    //Smag = Smag_old - (D_old/Smag_old)*eps1_a ;  
    Smag = Smag_old - eps1_a ;   
    Sd[0] *= (Smag/Smag_old) ; 
    Sd[1] *= (Smag/Smag_old) ; 
    Sd[2] *= (Smag/Smag_old) ; 
    
    //for ( j=0; j<3; j++ )
    //{  
           //if ( Sd[j] > 0.0  &&  Sd[j] > eps1_a ) 
      //{ Sd[j] = Sd[j] - eps1_a ; } 
      //else if ( Sd[j] > 0.0  &&  Sd[j] < eps1_a ) 
      //{ Sd[j] /= 2.0 ; } 
      //else if ( Sd[j] < 0.0  &&  Sd[j] < - eps1_a ) 
      //{ Sd[j] = Sd[j] + eps1_a; } 
      ////else if ( Sd[j] < 0.0  &&  Sd[j] > - eps1_a ) 
      //{ Sd[j] /= 2.0 ; } 
      //else 
      //{ printf("  MHD_Solver_5_vperp0:  something unexpected (2) ... \n"); } 
    //}
    
    // Recalculate some quantities that use these new values 
    Ssq = square_form(Sd, gu); 
    a = tau + D; 
    alpha_sq = 0.5*Bsq/a ; 
    delta_sq = (D*D)/(a*a) ; 
    beta_sq = Ssq/(a*a) ; 
    BS = Bx*Sd[0] + By*Sd[1] + Bz*Sd[2] ; 

      printf("  MHD_Solver_5_vperp0:  ... just checking ...     \n" ); 
      printf("  MHD_Solver_5_vperp0:     (1-alpha_sq)^2 - beta^2 - delta^2 = %25.20e \n",(1.0-alpha_sq)*(1.0-alpha_sq) - beta_sq - delta_sq);
      printf("  MHD_Solver_5_vperp0:  where:     \n" ); 
      printf("  MHD_Solver_5_vperp0:            tau = %25.20e \n", tau ); 
      printf("  MHD_Solver_5_vperp0:             D  = %25.20e \n", D   ); 
      printf("  MHD_Solver_5_vperp0:            Bsq = %25.20e \n", Bsq ); 
      printf("  MHD_Solver_5_vperp0:            Ssq = %25.20e \n", Ssq ); 
      printf("  MHD_Solver_5_vperp0:             BS = %25.20e \n", BS  ); 
      printf("  MHD_Solver_5_vperp0:          Sd[0] = %25.20e \n", Sd[0] ); 
      printf("  MHD_Solver_5_vperp0:          Sd[1] = %25.20e \n", Sd[1] ); 
      printf("  MHD_Solver_5_vperp0:          Sd[2] = %25.20e \n", Sd[2] ); 
      printf("  MHD_Solver_5_vperp0:        alpha^2 = %25.20e \n", alpha_sq ); 
      printf("  MHD_Solver_5_vperp0:  (1-alpha^2)^2 = %25.20e \n", (1.-alpha_sq)*(1.-alpha_sq) ); 
      printf("  MHD_Solver_5_vperp0:         beta^2 = %25.20e \n", beta_sq ); 
      printf("  MHD_Solver_5_vperp0:        delta^2 = %25.20e \n", delta_sq ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_X] = %25.20e \n", fpar[FP_COORD_X] ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_Y] = %25.20e \n", fpar[FP_COORD_Y] ); 
    printf("  MHD_Solver_5_vperp0: fpar[FP_COORD_Z] = %25.20e \n", fpar[FP_COORD_Z] ); 
#endif

/////////////////////////////////   
#if 0 
    double  eps0 = 6.e-16 ; 
    rescale_factor1 =   1.0 
                      / (   sqrt(   (beta_sq + delta_sq)
                                  / (1.-alpha_sq)
                                  / (1.-alpha_sq) 
                                ) 
                          + eps0) ; 
    
    // Now multiply Sd by the scaling factor. 
    for ( j=0; j<3; j++ )
    { Sd[j] *= rescale_factor1 ; }   
    
    // Recalculate Ssq.  
    Ssq = square_form(Sd, gu); 
    
    // Now we have to recalculate the roots of f and g with this new Ssq.  
    beta_sq = Ssq/(a*a); 
#endif 
    t2 = t1*t1 - (gamma-1.0)*beta_sq ; 
    if (t2 < 0.0) 
    { printf("MHD_Solver_5_vperp0: t2 < 0, i.e. t2=%25.20e\n",t2); }
    
    qf_2 = t1 + sqrt(t2) ; // new root of f; upper bound on q 
    qg_2 = sqrt(beta_sq) ; // new root of g; lower bound on q 
    
    // Check that these new roots are indeed good upper and lower bounds.
    if ( qg_2 < qf_2 )
    { 
      // should we use this instead:  ql = fmax( qg_2, qf_max ); 
      ql = qg_2 ; 
      qh = qf_2 ; 
    } 
    else
    { 
     if (ltrace) { 
      printf("  MHD_Solver_5_vperp0:  Even after rescaling, we still have\n");
      printf("  MHD_Solver_5_vperp0:  a problem with the relative values \n");
      printf("  MHD_Solver_5_vperp0:  of the bounds qg_2 and qf_2.  In  \n");
      printf("  MHD_Solver_5_vperp0:  particular, we should have that  \n"); 
      printf("  MHD_Solver_5_vperp0:  qg_2 < qf_2.  Instead, we have\n"); 
      printf("  MHD_Solver_5_vperp0:      qg_2 = %25.20e \n", qg_2 ); 
      printf("  MHD_Solver_5_vperp0:      qf_2 = %25.20e \n", qf_2 ); 
     }  
    }  
    
    // Because we made a rescaling of the components of S and of Ssq, we need
    // to save these.  
    u[U_D ] = D ;   
    u[U_TAU] = tau ;   
    u[U_SX] = Sd[0] ;   
    u[U_SY] = Sd[1] ;   
    u[U_SZ] = Sd[2] ;   
    fpar[FP_SSQ] = Ssq; 
    fpar[FP_BS ] = BS; 
  
    // check again 
    if ( beta_sq + delta_sq >= (1.0-alpha_sq)*(1.0-alpha_sq) ) 
    {  
     if (ltrace) {  
      printf("  MHD_Solver_5_vperp0: ... even on rescaling S, we find that \n");
      printf("  MHD_Solver_5_vperp0:  the inequality is still not  \n");
      printf("  MHD_Solver_5_vperp0:  satisfied; indeed,  \n");
      printf("  MHD_Solver_5_vperp0:     (tau+D-Bsq/2)^2 - Ssq - D^2    \n");
      printf("  MHD_Solver_5_vperp0:  should be greater than 0. Instead \n");
      printf("  MHD_Solver_5_vperp0:  (the rescaled version) is \n");
      printf("  MHD_Solver_5_vperp0:     (1-alpha_sq)^2 - beta^2 - delta^2 = %25.20e \n",(1.0-alpha_sq)*(1.0-alpha_sq) - beta_sq - delta_sq);
      printf("  MHD_Solver_5_vperp0:  where:     \n" ); 
      printf("  MHD_Solver_5_vperp0:            tau = %25.20e \n", tau ); 
      printf("  MHD_Solver_5_vperp0:             D  = %25.20e \n", D   ); 
      printf("  MHD_Solver_5_vperp0:            Bsq = %25.20e \n", Bsq ); 
      printf("  MHD_Solver_5_vperp0:            Ssq = %25.20e \n", Ssq ); 
      printf("  MHD_Solver_5_vperp0:             BS = %25.20e \n", BS  ); 
      printf("  MHD_Solver_5_vperp0:          Sd[0] = %25.20e \n", Sd[0] ); 
      printf("  MHD_Solver_5_vperp0:          Sd[1] = %25.20e \n", Sd[1] ); 
      printf("  MHD_Solver_5_vperp0:          Sd[2] = %25.20e \n", Sd[2] ); 
      printf("  MHD_Solver_5_vperp0:        alpha^2 = %25.20e \n", alpha_sq ); 
      printf("  MHD_Solver_5_vperp0:  (1-alpha^2)^2 = %25.20e \n", (1.-alpha_sq)*(1.-alpha_sq) ); 
      printf("  MHD_Solver_5_vperp0:         beta^2 = %25.20e \n", beta_sq ); 
      printf("  MHD_Solver_5_vperp0:        delta^2 = %25.20e \n", delta_sq ); 
      } 
      exit(-2);
      return -3;
    } 
  } 
  //FIXME  I didn't think this showed up any more even in the pure fluid (MHD_Solver_4) case.  I don't think it shows up here either ... fix this ... 
  else if ( fabs( (beta_sq + (D/a)*(D/a)) - beta_sq ) <= 6.0e-16 ) // Case 3 
  { 
   if (ltrace) { 
    printf("MHD_Solver_5_vperp0:  D/a is so small as to be lost in \n");
    printf("MHD_Solver_5_vperp0:  roundoff.  If we actually get here, \n");
    printf("MHD_Solver_5_vperp0:  we will need to do something.  In \n");
    printf("MHD_Solver_5_vperp0:  MHD_Solver_4, we subtracted something \n");
    printf("MHD_Solver_5_vperp0:  from Ssq, but I have taken that out as \n");
    printf("MHD_Solver_5_vperp0:  I don't think we will ever get here.  \n");
    printf("MHD_Solver_5_vperp0:  If something else happens, I will deal \n");
    printf("MHD_Solver_5_vperp0:  with it at that time.   \n");
   } 

       // the following is deprecated ... used to be in MHD_Solver_4, but 
       // I don't think it's a case that we ever get to ... 
    /* {{{  
    printf("MHD_Solver_5_vperp0:  roundoff.  We will adjust Ssq in \n");
    printf("MHD_Solver_5_vperp0:  hopes this resolves the problem. \n");

    //Instead of strictly rescaling, in this case, we have found that simply 
    //subtracting a small amount from each of the components of S brought 
    //Ssq back into the physical regime.  

    //  FIXME Again, the value 1.e-15 seems a bit arbitrary.  
    //     It might be well to play with this. 
    for ( j=0; j<3; j++ ) { Sd[j] -= 1.e-15 * a ; }   
    
    // Recalculate Ssq.
    Ssq = square_form(Sd, gu); 
    
    // Now we have to recalculate the roots of f and g with this new Ssq.  
    beta_sq = Ssq/(a*a); 
    t2 = 1.0 - (gamma-1.0)*beta_sq/(t1*t1) ; 
    if (t2 < 0.0) { printf("  MHD_Solver_5_vperp0: t2 = %25.20e < 0 \n",t2); }
    qf_2 = t1*( 1.0 + sqrt(t2)) ; // new root of f; upper bound on q 
    qg_2 = sqrt(beta_sq) ;        // new root of g; lower bound on q  
   
    // Check that these new roots are indeed good upper and lower bounds.
    if ( qg_2 < qf_2 )
    { 
      // should we use this instead:  ql = fmax( qg_2, qf_max ); 
      ql = qg_2 ; 
      qh = qf_2 ; 
    } 
    else
    { 
      printf("  MHD_Solver_5_vperp0:  Even after adjusting (subtracting), \n");
      printf("  MHD_Solver_5_vperp0:  we still have a problem with the    \n");
      printf("  MHD_Solver_5_vperp0:  relative values of the bounds qg_2  \n");
      printf("  MHD_Solver_5_vperp0:  and qf_2.  In particular, we should \n");
      printf("  MHD_Solver_5_vperp0:  have that  qg_2 < qf_2.  Instead, we\n");
      printf("  MHD_Solver_5_vperp0:  have  \n"); 
      printf("  MHD_Solver_5_vperp0:      qg_2 = %25.20e \n", qg_2 );
      printf("  MHD_Solver_5_vperp0:      qf_2 = %25.20e \n", qf_2 );
    }  

    // Because we made a change to the S components and Ssq, we need to save
    // them. 
    u[U_SX] = Sd[0] ;   
    u[U_SY] = Sd[1] ;   
    u[U_SZ] = Sd[2] ;   
    fpar[FP_SSQ] = Ssq; 
    
    // check again 
    printf("  MHD_Solver_5_vperp0:  On adjusting componets of S: \n");
    printf("  MHD_Solver_5_vperp0:      fabs( (beta_sq + (D/a)*(D/a)) - beta_sq ) = %25.20e \n", fabs((beta_sq+(D/a)*(D/a)) - beta_sq) ); 
    printf("  MHD_Solver_5_vperp0:      Ssq/a^2 + D^2/a^2 = %35.30e \n",beta_sq+(D/a)*(D/a));
}}}  */
  } 
  else if ( qg_2 == qf_2 ) // Case 4  
  { 
    qg_2 -= 1.e-16 ; 
    qf_2 += 1.e-16 ; 
    
    ql = qg_2 ;  
    qh = qf_2 ;  
  }    
  else  // this is "none of the above"; if we get here, we don't know why ... 
  { 
//{{{ 
   if (ltrace) { 
    printf(  "MHD_Solver_5_vperp0:   Something unexpected. \n"); 
    printf(  "MHD_Solver_5_vperp0:   There are no contingency plans ...  \n");
    printf("        qg_2 = %25.20e \n", qg_2 ); 
    printf("          qf_2 = %25.20e \n", qf_2 ); 
    printf("     beta_sq = %25.20e \n", beta_sq ); 
    printf("        D    = %25.20e \n", D ); 
    printf("        tau  = %25.20e \n", tau ); 
    printf("        Ssq  = %25.20e \n", Ssq ); 
    printf("    beta_sq + (D/a)^2 = %25.20e \n", beta_sq+(D/a)*(D/a) ); 
    printf("   dump fpar  \n"); 
    printf("      fpar[FP_SSQ]     = %25.20e \n", fpar[FP_SSQ] );  
    printf("      fpar[FP_BSQ]     = %25.20e \n", fpar[FP_BSQ] );  
    printf("      fpar[FP_BS ]     = %25.20e \n", fpar[FP_BS ] );  
    printf("      fpar[FP_D  ]     = %25.20e \n", fpar[FP_D  ] );  
    printf("      fpar[FP_TAU]     = %25.20e \n", fpar[FP_TAU] );  
    printf("      fpar[FP_GAMMA]   = %25.20e \n", fpar[FP_GAMMA] );  
    printf("      fpar[FP_VSQ]     = %25.20e \n", fpar[FP_VSQ] );  
    printf("      fpar[FP_COORD_X] = %25.20e \n", fpar[FP_COORD_X] ); 
    printf("      fpar[FP_COORD_Y] = %25.20e \n", fpar[FP_COORD_Y] ); 
    printf("      fpar[FP_COORD_Z] = %25.20e \n", fpar[FP_COORD_Z] ); 
    printf("      fpar[FP_INDEX_X] = %25.20e \n", fpar[FP_INDEX_X] ); 
   }  
//}}} 
  }  
 

  int rc=0;
  // Now check that at ql, the function F(q) := f(q)-(const)*sqrt(g(q)) 
  // is positive.  First just calculate F(ql).   
  rc = func5_vperp0_p( &f_at_ql, ql, fpar );  
  if (rc < 0)
  {  
    if (ltrace) {
        printf("  MHD_Solver_5_vperp0:  Something's amiss with ql  \n"); 
    printf("  MHD_Solver_5_vperp0:  We couldn't calculate F(ql). \n"); 
//{{{
#if 0
    printf("     ql  = %25.20e \n", ql ); 
    printf("     qg2 = %25.20e \n", qg_2 ); 
    printf("     qf2 = %25.20e \n", qf_2 ); 
#endif
//}}}
   } 
  } 
    
  // Now check that at qh, the function F(q) := f(q)-(const)*sqrt(g(q)) 
  // is negative.  First just calculate F(qh).   
  rc = 0 ; 
  rc = func5_vperp0_p( &f_at_qh, qh, fpar );  
  if (rc < 0)
  { 
    if (ltrace) {
        printf(" MHD_Solver_5_vperp0:  Something's amiss with qh \n");  
    printf(" MHD_Solver_5_vperp0:  We couldn't calculate F(qh). \n"); 
//{{{
#if 0
    printf("   qh  = %25.20e \n", qh ); 
    printf("   qg2 = %25.20e \n", qg_2 ); 
    printf("   qf2 = %25.20e \n", qf_2 ); 
#endif
//}}}
   }
  }
  
  // If, indeed, F(ql) > 0 and F(qh) < 0, then we should have a bracket for 
  // the root.  We check for the failure of these conditions and do 
  // what we can.  
  if ( !(f_at_ql > 0.0  &&  f_at_qh < 0.0) )
  { 
   if (ltrace) { 
//{{{    printf("  MHD_Solver_5_vperp0:  We have a problem with the bracket. \n"); 
    printf("  MHD_Solver_5_vperp0:  at ql = %25.20e, F(ql) should be \n",ql); 
    printf("  MHD_Solver_5_vperp0:  positive.  Instead, F(ql) = %25.20e\n",f_at_ql); 
    printf("  MHD_Solver_5_vperp0:  at qh = %25.20e, F(qh) should be \n",qh); 
    printf("  MHD_Solver_5_vperp0:  negative.  Instead, F(qh) = %25.20e\n",f_at_qh); 
/*    
    printf("  MHD_Solver_5_vperp0:  Sort of at a loss at this point.  \n"); 
    printf("  MHD_Solver_5_vperp0:  Going on, but without much hope.  \n"); 
    printf("  MHD_Solver_5_vperp0:         qg_2 = %25.20e \n", qg_2);
    printf("  MHD_Solver_5_vperp0:         qf_2 = %25.20e \n", qf_2);
    printf("  MHD_Solver_5_vperp0:          tau = %25.20e \n", tau);
    printf("  MHD_Solver_5_vperp0:           D  = %25.20e \n",  D ); 
    printf("  MHD_Solver_5_vperp0:          Ssq = %25.20e \n", Ssq);
    printf("  MHD_Solver_5_vperp0:          Bsq = %25.20e \n", Bsq); 
    printf("  MHD_Solver_5_vperp0:           BS = %25.20e \n", BS);
    printf("  MHD_Solver_5_vperp0:     alpha_sq = %25.20e \n", alpha_sq); 
    printf("  MHD_Solver_5_vperp0:      beta_sq = %25.20e \n", beta_sq);
    printf("  MHD_Solver_5_vperp0:        alpha = %25.20e \n", sqrt(alpha_sq)); 
    printf("  MHD_Solver_5_vperp0:         beta = %25.20e \n", sqrt(beta_sq));
    //printf("  MHD_Solver_5_vperp0:        xi_sq = %25.20e \n", xi_sq); 
    printf("  MHD_Solver_5_vperp0:  tau+D-Bsq/2 = %25.20e \n", tau+D-0.5*Bsq);
    printf("  MHD_Solver_5_vperp0:===== MAPLE DUMP ========================\n");
    printf("   restart: with(plots): \n");
    printf("   alpha_sq := %25.20e ; \n", alpha_sq );  
    printf("   beta_sq  := %25.20e ; \n", beta_sq  );
    //printf("     xi_sq  := %25.20e ; \n", xi_sq    );  
    printf("   gamma1   := %25.20e ; \n", gamma    );  
    printf("   delta1   := %25.20e ; \n", D/a      );  
    printf("   f(q) :=                                  \n"); 
    printf("       -  (q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("         *(q-0.5*gamma1*(1.0-alpha_sq))\n");
    printf("       +  (0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("         *(0.5*gamma1*(1.0-alpha_sq))  \n");
    printf("       -  (gamma1-1.0)*beta_sq ;       \n");
    printf("   plot(f(q), q=-0.5..1) ; \n");  
    printf("   g(q) :=  q*q - beta_sq     ;    \n");
    printf("   g_1(q) :=  (gamma1-1.)*delta1*sqrt(g(q));\n");
    printf("   plot(g(q), q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g(q)}, q=-0.5..1) ; \n");  
    printf("   plot( {f(q), g_1(q)}, q=-0.5..1) ; \n");  
    printf("===============================================================\n");
*/
    } 

//}}}
 
    // it may be that choosing ql = fmax( qg_2, qf_max )  was a mistake 
    // Try the other, namely  
    ql = fmin ( qg_2, qf_max ) ; 
    //  Try the previous with this new value, ie
    rc = func5_vperp0_p( &f_at_ql, ql, fpar ); 
    if (rc < 0) 
    { 
      if (ltrace) { 
      printf("  MHD_Solver_5_vperp0:  Trying again, and still a problem \n");
      printf("  MHD_Solver_5_vperp0:  with ql ...  \n"); 
      }
    }
    if ( !(f_at_ql > 0.0  &&  f_at_qh < 0.0) )
    { 
      if (ltrace) {
          printf("  MHD_Solver_5_vperp0:  ... and they are even worse.  Ugh.\n");
      }
    }
    else 
    { 
      if (ltrace) { 
      printf("  MHD_Solver_5_vperp0:  Looks like the change in ql may have worked ..\n");
      printf("  MHD_Solver_5_vperp0:    COORD_X = %25.20e \n",fpar[FP_COORD_X]);
      printf("  MHD_Solver_5_vperp0:    COORD_Y = %25.20e \n",fpar[FP_COORD_Y]);
      printf("  MHD_Solver_5_vperp0:    COORD_Z = %25.20e \n",fpar[FP_COORD_Z]);
      } 
    } 
}  

  if ( f_at_ql > 0.0  &&  f_at_qh > 0.0 ) 
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5_vperp0:  at the lower and upper bounds, the \n"); 
    printf("  MHD_Solver_5_vperp0:  function is positive so we fail to \n"); 
    printf("  MHD_Solver_5_vperp0:  get a bracket.  \n");  
#if 0
    printf(" the function at each of the lower bounds are:\n"); 
    printf("      ql      = %25.20e \n", ql );  
    printf("    f_at_ql   = %25.20e \n", f_at_ql );  
    printf("      qh      = %25.20e \n", qh );  
    printf("    f_at_qh   = %25.20e \n", f_at_qh );  
    printf(" now for some inequalities/quantities to monitor:\n"); 
    printf("       Ssq/(a*a)    = %25.20e \n", Ssq/(a*a) );  
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a) );  
    printf(" \n"); 
#endif
    printf("  MHD_Solver_5_vperp0:  In an act of sheer desperation, \n"); 
    printf("  MHD_Solver_5_vperp0:  I'm going to shift qh up by 6.e-16, so\n");
    
    qh += 6.e-16 ; 
    
    printf("  MHD_Solver_5_vperp0:      qh = %25.20e \n",qh );
    printf("  MHD_Solver_5_vperp0:  Checking to see if that worked ... \n");
    
    rc = func5_vperp0_p( &f_at_qh_v2, qh, fpar ) ; 
    
    printf("  MHD_Solver_5_vperp0:  The new F(qh) = %25.20e \n",f_at_qh_v2) ; 
    printf("  MHD_Solver_5_vperp0: old      qh = %25.20e \n",qh-6.e-16); 
    printf("  MHD_Solver_5_vperp0: old f_at_qh = %25.20e \n",f_at_qh); 
    printf("  MHD_Solver_5_vperp0: new      qh = %25.20e \n",qh); 
    printf("  MHD_Solver_5_vperp0: new f_at_qh = %25.20e should be neg \n",f_at_qh_v2); 
    }
  }


  /*
  At this point, we should have a legitimate bracket for our root.  
  We proceed with the Newton solve.  
  
  As our initial guess for the root, take the midpoint of the bracket.
  */
  double qi = 0.5*(ql + qh);  
  double q = qi;

  //  Incorporate a little redundancy in the event things go bad.   
  double ql0 = ql;
  double qh0 = qh;
  double q0 = q;

  //  Set trace parameter for rtsafe to none, set the tolerance for 
  //  the Newton solve, and call rtsafe to get the root, q.  
  int xtrace = 0;
  double xtol = 1.0e-14;
  int xrc = rtsafe(func5_vperp0_p_d, &q, ql, qh, xtol, fpar, xtrace);

  // In the event of failure, a postmortem ... 
  if (xrc < 0) 
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vperp0:  The Newton solve (rtsafe) failed.\n");
    printf("  MHD_Solver_5_vperp0:  Current values include  \n"); 
    printf("    ql = %25.20e \n",ql); 
    printf("    qh = %25.20e \n",qh); 
    printf("    qi = %25.20e \n",qi); 
    printf("    q   =%25.20e \n",q); 
    printf("   ql0 = %25.20e \n",ql0); 
    printf("   qh0 = %25.20e \n",qh0); 
    printf("   q0  = %25.20e \n",q0); 
    printf("  MHD_Solver_5_vperp0:  Some conserved quantities and \n"); 
    printf("  MHD_Solver_5_vperp0:  inequalities \n");  
    printf("    D               = %25.20e \n", D);  
    printf("    tau+D           = %25.20e \n", a);  
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));  
    printf("  MHD_Solver_5_vperp0:  After rtsafe failure ... redoing ... \n"); 
    } 

    // Retrying rtsafe but with a different trace parameter so as to get 
    // some more info out of rtsafe.  
    ql = ql0;
    qh = qh0;
    q = q0;
   
    xtrace = 1;
    xrc = rtsafe(func5_vperp0_p_d, &q, ql, qh, xtol, fpar, xtrace);
    exit(2);
    return -4;
  }

 
  //  If we get to this point we may actually have a solution.  
  //  We have to rescale back by a = tau+D.  
  q *= a;
 
  //  Knowing q (:= hW^2), we can now get the remaining primitive variables.  
  //  Start with the Lorentz factor W^2:
  //double vsq = Ssq / (q*q);
  double invWsq = 1.0 - ((2.0*q+Bsq)*BS*BS + q*q*Ssq)/(q*q*(q+Bsq)*(q+Bsq));

  //  We have the "down" components of S; now get the "up" components 
  double Su[3];
  form_raise_index(Su, Sd, gu);

  //  Now calculate rho, pressure, and the "up" components of v^i.  
  u[V_RHO] = D * sqrt( invWsq );
  u[V_P]   = (gamma-1.0)/gamma*(q*invWsq - u[V_RHO]);
  u[V_VX]  = (Su[0] + BS*Bx/q) / (q+Bsq);
  u[V_VY]  = (Su[1] + BS*By/q) / (q+Bsq);
  u[V_VZ]  = (Su[2] + BS*Bz/q) / (q+Bsq);
  u[V_BX]  = u[U_BX] ;
  u[V_BY]  = u[U_BY] ;
  u[V_BZ]  = u[U_BZ] ;
  u[V_PSI] = u[U_PSI] ;


// the following should produce identical primitive variables, but the calc is simpler because we use the fact that v_perp^2 = 0 and hence there is no velocity contribution to S_i . 
/*
  //  If we get to this point we may actually have a solution.  
  //  We have to rescale back by a = tau+D.  
  q *= a;
 
  //  Knowing q (:= hW^2), we can now get the remaining primitive variables.  
  //  Start with the Lorentz factor W^2:
  double vsq = Ssq / (q*q);
  double inv_Wsq = 1.0 - vsq ; 

  //  We have the "down" components of S; now get the "up" components 
  double Su[3];
  form_raise_index(Su, Sd, gu);

  //  Now calculate rho, pressure, and the "up" components of v^i.  
  u[V_RHO] = D * sqrt( inv_Wsq );
  u[V_P]   = (gamma-1.0)/gamma*(q * inv_Wsq - u[V_RHO]);
  u[V_VX]  = Su[0] / q ;
  u[V_VY]  = Su[1] / q ;
  u[V_VZ]  = Su[2] / q ;
  u[V_BX]  = u[U_BX] ;
  u[V_BY]  = u[U_BY] ;
  u[V_BZ]  = u[U_BZ] ;
  u[V_PSI]  = u[U_PSI] ;
*/

  return 0;

}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the function, F(q), whose root we are to find
 *  as part of the primitive solve but appropriate to the case that 
 *    (v_perp)^2  vanishes exactly.  It is used in MHD_Solver_5_vperp0.  
 *
 *-------------------------------------------------------------------------*/
int func5_vperp0_p(double *f, double q, double *fpar)  
{

  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];

  double  dis ; 

  double  a = tau+D;
  double  a_sq = a*a ; 
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq = Ssq / a_sq ; 
  double  t1 = 0.5 * gamma * ( 1.0 - alpha_sq ) ; 
  double  t2 = q - t1 ; 

  dis = q*q - beta_sq ; 
  
  // If the argument of the square root is a small negative value, 
  // we will cheat, assume that this is just roundoff error, force the 
  // value to be positive and hope that we can go on. 
  if ( dis > -6.e-16  &&  dis < 0.0 )  
  { 
    //printf("  func5_vperp0_p:  The argument of a square root is   \n");
    //printf("  func5_vperp0_p:  a tiny bit negative.               \n");
//{{{  ... some tracing if turned on ... 
#if 0 
    printf("  func5_vperp0_p:  Dumping some values for debugging. \n");
    printf("           tau  = %15.10e \n",tau) ; 
    printf("            D   = %15.10e \n",D  ) ; 
    printf("          tau+D = %15.10e \n",tau+D  ) ; 
    printf("           Ssq  = %15.10e \n",Ssq ) ; 
    printf("            q   = %15.10e \n",q ) ; 
    printf("           dis  = %15.10e \n",dis) ; 
    printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
    printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
    printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif 
//}}}
    dis = 3.6e-31 ; 
  }
  // If the argument of the square root is negative enough, return. 
  else if ( dis <= -6.e-16 )  
  {
     printf("  func5_vperp0_p:  The argument of a square root is \n");
     printf("  func5_vperp0_p:  negative.  We will dump some   \n");
//{{{  ... some tracing if turned on ... 
#if 0 
     printf("  func5_vperp0_p:  values for debugging purposes.   \n");
     printf("           tau  = %15.10e \n",tau) ; 
     printf("            D   = %15.10e \n",D  ) ; 
     printf("          tau+D = %15.10e \n",tau+D  ) ; 
     printf("           Ssq  = %15.10e \n",Ssq ) ; 
     printf("            q   = %15.10e \n",q ) ; 
     printf("           dis  = %15.10e \n",dis) ; 
     printf("      q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
     printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif 
//}}} 
     return -1 ; 
  }

  *f  =   t1*t1 
        - t2*t2 
        - (gamma-1.0) * ( beta_sq + (D/a)*sqrt(dis) ) ;

//  double t1 = gamma * ( 1.0 - alpha_sq ) ; 
//  *f  =   q * (t1 - q)  
//        - (gamma-1.0) * ( beta_sq + (D/a)*sqrt(dis) ) ;

  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the function, F(q), whose root we are to find
 *  as part of the primitive solve but appropriate to the case that 
 *   (v_perp)^2  vanishes exactly.  It also gets the derivative, F'(q).  
 *  It is used in MHD_Solver_5_vperp0.  
 *
 *-------------------------------------------------------------------------*/
int func5_vperp0_p_d(double *f, double *df, double q, double *fpar)  
{

  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];

  double  dis ;
  
  double  a = tau+D;
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq = Ssq / (a*a) ; 
  
  dis = q*q - beta_sq ;

  /* 
  If the argument of the square root is a small negative value, 
  we will cheat, assume that this is just roundoff error, force the 
  value to be a very small positive number and hope that we can go on. 
  */
  if ( dis < 0.0  &&  dis > -6.e-16 )
  {
     //printf("  func5_vperp0_p_d:  The argument of a square root is negative\n");
     //printf("  func5_vperp0_p_d:  but only a tiny bit ... \n");
//{{{  ... some tracing if turned on ... 
#if 0
     printf("                   tau  = %15.10e \n",tau) ; 
     printf("                    D   = %15.10e \n",D  ) ; 
     printf("                  tau+D = %15.10e \n",tau+D  ) ; 
     printf("                   Ssq  = %15.10e \n",Ssq ) ; 
     printf("                    q   = %15.10e \n",q ) ; 
     printf("                   dis  = %15.10e \n",dis) ; 
     printf("          q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
     printf("         Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif 
//}}}
     dis = 3.6e-31;  
  }
  // If the argument of the square root is negative enough, just return. 
  else if ( dis <= -6.e-16 )  
  {
     printf("  func5_vperp0_p_d:  The argument of a square root is negative\n");
     printf("  func5_vperp0_p_d:  and too much so to handle.  Die here. \n");
//{{{  ... some tracing if turned on ... 
#if 0
     printf("                   tau  = %15.10e \n",tau) ; 
     printf("                    D   = %15.10e \n",D  ) ; 
     printf("                  tau+D = %15.10e \n",tau+D  ) ; 
     printf("                   Ssq  = %15.10e \n",Ssq ) ; 
     printf("                    q   = %15.10e \n",q ) ; 
     printf("                   dis  = %15.10e \n",dis) ; 
     printf("          q^2 - S^2/a^2 = %15.10e \n",q*q-Ssq/(a*a) ) ; 
     printf("         Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
     printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif
//}}}
     return -1 ;
  }

#if 1 
  *f  = - (q - 0.5*gamma*(1.0-alpha_sq))*(q - 0.5*gamma*(1.0-alpha_sq)) 
        + gamma*gamma/4.0*pow(1.0-alpha_sq,2)  
        - (gamma-1.0)*Ssq/(a*a) - (gamma-1.0)*D/a*sqrt(dis);

  *df = -2.0*(q - 0.5*gamma*(1.0-alpha_sq)) - (gamma-1.0)*D/a*q/sqrt(dis);
#endif 


#if 0
  double  a_sq = a*a ; 
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq  = Ssq / a_sq ; 
  double  delta    = D / a ; 
  double  tmp1 = 0.5*gamma*(1.0-alpha_sq) ; 
  double  tmp2 = q - tmp1 ; 
  double  gam_m1 = gamma - 1.0 ; 
  double  sqrt_dis ; 
  
  sqrt_dis = sqrt( dis ) ; 

  *f  =   tmp1 * tmp1 
        - tmp2 * tmp2 
        - gam_m1 * ( beta_sq + delta*sqrt_dis );

//  double t1 = gamma * ( 1.0 - alpha_sq ) ; 
//  *f  =   q * (t1 - q)  
//        - (gamma-1.0) * ( beta_sq + (D/a)*sqrt(dis) ) ;


  *df = - 2.0*tmp2 - gam_m1 * delta * q/sqrt_dis;
#endif 


  return 1;
}



/*-------------------------------------------------------------------------
 *
 *  This is an alternative solver to MHD_Solver_5.  In cases where the 
 *  velocity is nonzero, but lies completely perpendicular to the magnetic 
 *  field, the parallel component of the velocity vanishes, i.e. 
 *     (v_paral)^2 = 0. 
 *
 *  In particular, if the parallel component of the velocity (to the 
 *  magnetic field) is identically zero, the equation for  q = hW^2  is 
 *  simplified.  The single equation to solve for the enthalpy becomes 
 *       F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  where  f  is a cubic polynomial with respect to q and  g  is a 
 *  quadratic polynomial with respect to q.  While potentially easier to
 *  solve than the full, more general problem, we must still find roots 
 *  of  f(q).  [FIXME:  Actually, it may be the case that we can get away
 *  from finding a root of f(q) and that we can simply bracket the root
 *  of F(q) with the largest root of g(q) and a value of q such that f 
 *  is negative.  I should look at this possibility a bit more closely.]
 *
 *  Importantly, we change variables here due to the form of the equation.
 *  Instead of q being = h_e W^2/(tau+D), we set q = (h_e W^2 + B^2)/(tau+D).
 *  In other notation, instead of x, we use y=x+2*alpha_sq. 
 *  
 *-------------------------------------------------------------------------*/
int MHD_Solver_5_vparal0(  double *u        , 
                           double *xpt      , 
                           double  gd[3][3] , 
                           double  gu[3][3] ,
                           double *fpar        )
{
  const int ltrace = 1;

  double gamma = pars.gamma;

  double Ssq  = fpar[FP_SSQ];
  double Bsq  = fpar[FP_BSQ];
  double BS   = fpar[FP_BS ];
  
  double D    = u[U_D];
  double tau  = u[U_TAU];
  
  double Sd[3] ; 
  
  Sd[0] = u[U_SX] ; 
  Sd[1] = u[U_SY] ; 
  Sd[2] = u[U_SZ] ; 

  double  ql0, qh0, q0; 
  double  q, qi; 
  double  qf_l, qf_h, qf_i, qf ; 
  //double  q_max, q_min; 

  int     ftrace = 0 ; 
  int     frc = 0 ; 
  double  ftol = 1.e-14 ; 

  //int j ; 
  //double  qf_2, qf_3, qg_2 ; //roots of f and g
  //double  qf_2 ; 
  double  qf_3 = 0.0 ;  // for some bizarre reason, the compiler wants this initialized but doesn't weem to care about qf_2 and/or qg_2 
  double  qg_2 ; //roots of f and g
  //double  ql, qh, f_at_ql, f_at_qh, f_at_qh_v2 ;   
  //double  f_at_ql, f_at_qh, f_at_qh_v2 ;   
 
  double  ql = 0.0; 
  double  qh = 0.0; 

  double  a = tau + D;  // a combo used to rescale all other variables 
  double  alpha_sq = 0.5*Bsq/a ; 
  double  beta_sq = Ssq/(a*a) ; 


  /* 
  This beta_sq we just defined is the rescaled momentum.  If the 
  conserved variables are physical, it must satisfy 0<=beta_sq<1. 
  Further, if it is identically zero then v^2=0 and we should not, in fact, 
  be in this routine but rather the one which considers the separate case 
  of v^2=0.  This routine explicitly assumes v^2 is not equal to 0.
  Check:  
  */
  if (beta_sq < 6.e-16) 
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vparal0:  Ssq, the momentum squared, is machine\n");
    printf("  MHD_Solver_5_vparal0:  zero, i.e. \n");
    printf("  MHD_Solver_5_vparal0:     Ssq/(tau+D)^2 = %25.20e \n",beta_sq);
    printf("  MHD_Solver_5_vparal0:        BS/(tau+D) = %25.20e \n",BS/(tau+D));
    printf("  MHD_Solver_5_vparal0:  This is somehow wrong as we should \n" );
    printf("  MHD_Solver_5_vparal0:  be considering the case that \n");  
    printf("  MHD_Solver_5_vparal0:     BS=0 (the parallel component of \n" );
    printf("  MHD_Solver_5_vparal0:  velocity vanishes) but Ssq does not.\n"); 
    }
    exit(2); 
  }
  
  // In the current case, the largest root of g should be a lower bound on the
  // root of F.  We can find it analytically as g is a quadratic in the current
  // case.  
  qg_2 = sqrt(beta_sq); // the larger root of the quadratic g  


  // We need to find the largest root (we'll call it qf_3) of the  
  // cubic  f(q).  There are two other roots, one positive and one
  // negative such that the two positive roots of f(q) enclose qg_2 
  // and we have existence
  // and uniqueness of a solution to F(q) bounded by qg_2 and qf_3.  
  // 
  // So we need to find qf_3.  To do so, we will bound it by qg_2 
  // from below and t1 = alpha_sq*(2-gamma) + gamma from above as it can 
  // be shown that f(t1) < 0 and f(qg_2) > 0 .  Check this.  
  double  t1 = alpha_sq * ( 2.0 - gamma ) + gamma ; 
  double  f_at_qg_2; 
  int rc = 0; 
  rc = func5f_vparal0_p( &f_at_qg_2, qg_2, fpar );  
  if (rc < 0)
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5_vparal0:  Something's amiss with qg_2.  \n"); 
    printf("  MHD_Solver_5_vparal0:  We couldn't calculate f(qg_2).\n"); 
    } 
  } 
  double  f_at_t1; 
  rc = func5f_vparal0_p( &f_at_t1, t1, fpar );  
  if (rc < 0)
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5_vparal0:  Something's amiss with t1. \n");  
    printf("  MHD_Solver_5_vparal0:  We couldn't calculate f(t1).\n"); 
    }
  }
  
  
  // Now we check that the signs of f at qg_2 and t1 make sense, i.e. 
  // f(qg_2) > 0  and  f(t1) < 0 .  If so, we proceed with finding the 
  // root of f(q) that should be bracketed by [qg_2, t1].  
  if ( f_at_qg_2 > 0.0  &&  f_at_t1 < 0.0 )
  {
    qf_l = qg_2 ;  
    qf_h = t1 ;  
    qf_i = 0.5 * (qf_l + qf_h) ; 
    qf = qf_i ; 

    double qf_l0 = qf_l;
    double qf_h0 = qf_h;
    double qf_0 = qf;

    int ftrace = 0;
    double ftol = 1.0e-14;
    int frc = rtsafe(func5f_vparal0_p_d, &qf, qf_l, qf_h, ftol, fpar, ftrace);

    // In the event of failure, a postmortem ... 
    if (frc < 0) 
    {
      printf("  MHD_Solver_5_vparal0:  The Newton solve (rtsafe) failed   \n");
      printf("  MHD_Solver_5_vparal0:  when trying to get the largest     \n");
      printf("  MHD_Solver_5_vparal0:  root of f, what we're calling qf_3.\n");
      printf("  MHD_Solver_5_vparal0:  Current values include  \n"); 
      printf("        qf_l  = %25.20e \n",qf_l); 
      printf("        qf_h  = %25.20e \n",qf_h); 
      printf("        qf_i  = %25.20e \n",qf_i); 
      printf("        qf_l0 = %25.20e \n",qf_l0); 
      printf("        qf_h0 = %25.20e \n",qf_h0); 
      printf("        qf_0  = %25.20e \n",qf_0); 
      printf("    f_at_qf_l = %25.20e \n",f_at_qg_2); 
      printf("    f_at_qf_h = %25.20e \n",f_at_t1); 
      printf("  MHD_Solver_5_vparal0:  Some conserved quantities and \n"); 
      printf("  MHD_Solver_5_vparal0:  inequalities \n");  
      printf("    D               = %25.20e \n", D);  
      printf("    tau+D           = %25.20e \n", a);  
      printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
      printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
      printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));  
      printf("    B^2             = %25.20e \n", Bsq );  
      printf("    B^2/(2(tau+D))  = %25.20e \n", Bsq/(2.*(tau+D)) );  
      printf("    alpha_sq        = %25.20e \n", alpha_sq );  
      printf("    beta_sq         = %25.20e \n", beta_sq );  
      printf("  MHD_Solver_5_vparal0:  After rtsafe failure ... redoing ...\n");

      // Retrying rtsafe but with a different trace parameter so as to get 
      // some more info out of rtsafe.  
      qf_l = qf_l0;
      qf_h = qf_h0;
      qf_i = qf_0;
     
      ftrace = 1;
      frc = rtsafe(func5f_vparal0_p_d, &qf_i, qf_l, qf_h, ftol, fpar, ftrace);
      exit(2);
      return -4;
    }
    
    // Since we seem to have had success, we identify the root we just found 
    // as the largest of the three roots of f that we are looking for:   
    qf_3 = qf; 
  }
  else
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5_vparal0:  The function f is either not \n"); 
    printf("  MHD_Solver_5_vparal0:  positive at qg_2 or it is not  \n"); 
    printf("  MHD_Solver_5_vparal0:  negative at t1.  In particular, \n");
    printf("  MHD_Solver_5_vparal0:  at  qg_2 = %25.20e, f = %25.20e\n",qg_2,f_at_qg_2); 
    printf("  MHD_Solver_5_vparal0:  at   t1  = %25.20e, f = %25.20e\n",t1,f_at_t1); 
    printf("  MHD_Solver_5_vparal0:  You'll need to do some debugging as we don't have an alternative yet ...\n");
    }
  }

    

  // check that the roots make some sense, namely that qg_2 < qf_3  
  if ( qf_3 < qg_2 )
  { 
    if (ltrace) { 
    printf("  MHD_Solver_5_vparal0:  The largest root of f is smaller \n");
    printf("  MHD_Solver_5_vparal0:  than the largest root of g, namely \n");
    printf("  MHD_Solver_5_vparal0:   qf_3=%25.20e is smaller than \n",qf_3); 
    printf("  MHD_Solver_5_vparal0:   qg_2=%25.20e which seems wrong. \n",qg_2);
    printf("  MHD_Solver_5_vparal0:  and may indicate that no solution \n"); 
    printf("  MHD_Solver_5_vparal0:  exits. \n"); 
    } 
    exit(-2);
    return -3; 
  } 

  //  we should now have a bracket for the root of F(q), namely 
  //    [qg_2, qf_3]  
  //  We can now search for the root of F(q).  
  ql = qg_2 ;  
  qh = qf_3 ;  
  qi = 0.5 * (ql + qh) ; 
  q = qi ; 

  ql0 = ql;
  qh0 = qh;
  q0 = q;

  ftrace = 0;
  ftol = 1.0e-14;
  frc = rtsafe(func5_vparal0_p_d, &q, ql, qh, ftol, fpar, ftrace);

  // In the event of failure, a postmortem ... 
  if (frc < 0) 
  {
    printf("  MHD_Solver_5_vparal0:  The Newton solve (rtsafe) failed \n");
    printf("  MHD_Solver_5_vparal0:  when trying to get the root of F.\n");
    printf("  MHD_Solver_5_vparal0:  Current values include  \n"); 
    printf("     ql = %25.20e \n",ql); 
    printf("     qh = %25.20e \n",qh); 
    printf("     qi = %25.20e \n",qi); 
    printf("    ql0 = %25.20e \n",ql0); 
    printf("    qh0 = %25.20e \n",qh0); 
    printf("    q0  = %25.20e \n",q0); 
    printf("   qg_2 = %25.20e \n",qg_2); 
    printf("   qf_3 = %25.20e \n",qf_3); 
    printf("  MHD_Solver_5_vparal0:  Some conserved quantities and \n"); 
    printf("  MHD_Solver_5_vparal0:  inequalities \n");  
    printf("    D               = %25.20e \n", D);  
    printf("    tau+D           = %25.20e \n", a);  
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));  
    printf("    B^2             = %25.20e \n", Bsq );  
    printf("    B^2/(2(tau+D))  = %25.20e \n", Bsq/(2.*(tau+D)) );  
    printf("    alpha_sq        = %25.20e \n", alpha_sq );  
    printf("    beta_sq         = %25.20e \n", beta_sq );  
    printf("  MHD_Solver_5_vparal0:  After rtsafe failure ... redoing ...\n");

    // Retrying rtsafe but with a different trace parameter so as to get 
    // some more info out of rtsafe.  
    ql = ql0;
    qh = qh0;
    qi = q0 ;
   
    ftrace = 1;
    frc = rtsafe(func5_vparal0_p_d, &qi, ql, qh, ftol, fpar, ftrace);
    exit(2);
    return -4;
  }


  // Because we are paranoid, let's check some things.  Note we do nothing
  // other than check and if something is wrong, we complain.  We fix 
  // nothing here ...   
  if ( !(beta_sq < 1.0) ) 
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vparal0:  The basic dominant energy condition \n");
    printf("  MHD_Solver_5_vparal0:  (inequality) is violated.   \n");
    printf("  MHD_Solver_5_vparal0:  Some conserved quantities and \n"); 
    printf("  MHD_Solver_5_vparal0:  inequalities \n");  
    printf("    D               = %25.20e \n", D);  
    printf("    tau+D           = %25.20e \n", a);  
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
    printf("    B^2             = %25.20e \n", Bsq );  
    printf("    B^2/(2(tau+D))  = %25.20e \n", Bsq/(2.*(tau+D)) );  
    printf("    alpha_sq        = %25.20e \n", alpha_sq );  
    printf("    beta_sq         = %25.20e \n", beta_sq );  
    printf("  MHD_Solver_5_vparal0:  This needs some debugging and fixing.\n");
    } 
  } 
  // this is sort of a cheap version of the real inequality (which involves
  // both primitive and conserved variables ...
  if ( !( beta_sq + (D*D)/(a*a) < pow(1.0+alpha_sq,2) ) )
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vparal0:  The inequality S^2+D^2 < (tau+D)^2 \n");
    printf("  MHD_Solver_5_vparal0:  is violated.   \n");
    printf("  MHD_Solver_5_vparal0:  Some conserved quantities and \n"); 
    printf("  MHD_Solver_5_vparal0:  inequalities \n");  
    printf("    D               = %25.20e \n", D);  
    printf("    tau+D           = %25.20e \n", a);  
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));  
    printf("    B^2             = %25.20e \n", Bsq );  
    printf("    B^2/(2(tau+D))  = %25.20e \n", Bsq/(2.*(tau+D)) );  
    printf("    alpha_sq        = %25.20e \n", alpha_sq );  
    printf("    beta_sq         = %25.20e \n", beta_sq );  
    printf("  MHD_Solver_5_vparal0:  This needs some debugging and fixing.\n");
    } 
  } 
  if ( !( alpha_sq < 1.0 ) )
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vparal0:  The basic magnetic inequality \n");
    printf("  MHD_Solver_5_vparal0:    tau+D > B^2/2   is violated.\n");
    printf("  MHD_Solver_5_vparal0:  (Also known as alpha^2 < 1.)  \n");
    printf("  MHD_Solver_5_vparal0:  Some conserved quantities and \n"); 
    printf("  MHD_Solver_5_vparal0:  inequalities \n");  
    printf("    D               = %25.20e \n", D);  
    printf("    tau+D           = %25.20e \n", a);  
    printf("    D^2/a^2         = %25.20e \n", (D*D)/(a*a));  
    printf("    Ssq/(a*a)       = %25.20e \n", Ssq/(a*a));  
    printf("    (Ssq+D^2)/(a*a) = %25.20e \n", (Ssq+D*D)/(a*a));  
    printf("    B^2             = %25.20e \n", Bsq );  
    printf("    B^2/(2(tau+D))  = %25.20e \n", Bsq/(2.*(tau+D)) );  
    printf("    alpha_sq        = %25.20e \n", alpha_sq );  
    printf("    beta_sq         = %25.20e \n", beta_sq );  
    printf("  MHD_Solver_5_vparal0:  This needs some debugging and fixing.\n");
    }
  }
  if ( qg_2 == qf_3 )
  {
    if (ltrace) {
    printf("  MHD_Solver_5_vparal0:  Somehow, the bracket on the root of \n");
    printf("  MHD_Solver_5_vparal0:  F(q) has collapsed to a point:  \n");
    printf("  MHD_Solver_5_vparal0:    qg_2 = %25.20e \n", qg_2);
    printf("  MHD_Solver_5_vparal0:    qf_3 = %25.20e \n", qf_3);
    } 
  } 

  // If we get to this point we may actually have a solution.  
  //
  // Of course, we were actually solving for y=q+2*alpha^2 in all of this, 
  // i.e. a shifted root.  We now caculate q by shifting back.  s
  q -= 2.0 * alpha_sq ; 

  // We have to rescale back by a = tau+D.  
  q *= a;
 
  //  Knowing q (:= hW^2), we can now get the remaining primitive variables.  
  //  Start with v^2:
  double  vsq = Ssq / ((q+Bsq)*(q+Bsq));
  double  inv_Wsq = 1.0 - vsq ; 

  //  We have the "down" components of S; now get the "up" components 
  double Su[3];
  form_raise_index(Su, Sd, gu);

  //  Now calculate rho, pressure, and the "up" components of v^i.  
  u[V_RHO] = D*sqrt( inv_Wsq );
  u[V_P]   = (gamma-1.0)/gamma*(q * inv_Wsq - u[V_RHO]);
  u[V_VX]  = Su[0] / (q+Bsq);
  u[V_VY]  = Su[1] / (q+Bsq);
  u[V_VZ]  = Su[2] / (q+Bsq);
  u[V_BX]  = u[U_BX] ;
  u[V_BY]  = u[U_BY] ;
  u[V_BZ]  = u[U_BZ] ;
  u[V_PSI] = u[U_PSI] ;

  return 0;
}



/*-------------------------------------------------------------------------
 *
 *  This routine calculates the cubic function f(q) in the case that 
 *   (v_paral)^2 = 0  and the velocity is strictly perpendicular to the 
 *  magnetic field.  It is called by MHD_Solver_5_vparal0.  It is part of 
 *  the overall function F(q) whose root gives q, the enthalpy.  The form 
 *  of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  As part of MHD_Solver_5_vparal0 we need to solve for the largest root 
 *  of f(q).  We call that (most of the time) qf_3.  This root will then 
 *  be an upper bound on the the root of F(q).   However, for now, we just 
 *  calculate f(q).  
 *
 *-------------------------------------------------------------------------*/
int func5f_vparal0_p(double *f, double y, double *fpar)  
{

  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];

  double  a = tau+D;
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq = Ssq / (a * a) ; 

  double  two_min_gamma = 2.0 - gamma ; 
  double  t0 = alpha_sq * two_min_gamma ; 
  double  t1 = t0 + gamma ; 
  double  t2 = beta_sq * ( gamma - 1.0 ) ;
  double  t3 = t0 * beta_sq ; 
  
  *f = ((t1 - y) * y - t2) * y - t3 ; 

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *  This routine calculates the cubic function f(q) and its derivative 
 *  for use in the root finding routine, rtsafe, in the case that 
 *   (v_paral)^2 = 0  and the velocity is strictly perpendicular to the 
 *  magnetic field.  It is called by MHD_Solver_5_vparal0.  The function 
 *  f(q) is part of the overall function F(q) whose root gives q, the 
 *  enthalpy.  The form of F(q) is  
 *     F(q) = f(q) - (stuff linear in q) * sqrt(g(q)) = 0 
 *  As part of MHD_Solver_5_vparal0 we need to solve for the largest root 
 *  of f(q).  We call that (most of the time) qf_3.  This root will then 
 *  be an upper bound on the the root of F(q).   
 * 
 *  The name  func5f_vparal0_p_d  stands for the "function f" in 
 *  MHD_Solver_5_vparal0 for the "p"rimitive solve at a "p"oint together 
 *  with the "d"erivative in the case  (v_paral)^2 = 0.   
 *
 *-------------------------------------------------------------------------*/
int func5f_vparal0_p_d(double *f, double *df, double y, double *fpar)  
{

  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];
  
  double  a = tau+D;
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq  = Ssq / (a * a) ; 

  double  two_min_gamma = 2.0 - gamma ; 
  double  t0 = alpha_sq * two_min_gamma ; 
  double  t1 = t0 + gamma ; 
  double  t2 = beta_sq * ( gamma - 1.0 ) ;
  double  t3 = t0 * beta_sq ; 
  
  *f = ((t1 - y) * y - t2) * y - t3 ; 

  *df = y * ( 2.0 * t1 - 3.0 * y ) - t2 ;  

  return 1;
}


/*-------------------------------------------------------------------------
 *
 * 
 *
 *-------------------------------------------------------------------------*/
int func5_vparal0_p(double *f, double y, double *fpar)  
{
  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];

  double  a = tau+D;
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq = Ssq / (a * a) ; 

  double  two_min_gamma = 2.0 - gamma ; 
  double  gamma_min_one = gamma - 1.0 ; 
  double  t0 = alpha_sq * two_min_gamma ; 
  double  t1 = t0 + gamma ; 
  double  t2 = beta_sq * gamma_min_one ;
  double  t3 = t0 * beta_sq ; 
  
  double  sqrt_dis ; 
  double  dis = y*y - beta_sq ;

  // If the argument of the square root is a small negative value, 
  // we will cheat, assume that this is just roundoff error, force the 
  // value to be positive and hope that we go on. 
  if ( dis > -6.e-16  &&  dis < 0.0 )  
  { 
    printf("  func5_vparal0_p:  The argument of a square root is   \n");
    printf("  func5_vparal0_p:  a tiny bit negative.               \n");
//{{{  ... some tracing if turned on ... 
#if 0 
    printf("  func5_vparal0_p:  Dumping some values for debugging. \n");
    printf("            tau  = %15.10e \n",tau) ; 
    printf("             D   = %15.10e \n",D  ) ; 
    printf("           tau+D = %15.10e \n",tau+D  ) ; 
    printf("            Ssq  = %15.10e \n",Ssq ) ; 
    printf("             y   = %15.10e \n",y ) ; 
    printf("            dis  = %15.10e \n",dis) ; 
    printf("       Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
    printf("    (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
#endif 
//}}} 
    dis = 3.6e-31 ;  
  }
  // If the argument of the square root is negative enough, return. 
  else if ( dis <= -6.e-16 )  
  {
    printf("  func5_vparal0_p:  The argument of a square root is \n");
    printf("  func5_vparal0_p:  a negative.  We will dump some   \n");
//{{{  ... some tracing if turned on ...  
#if 0 
    printf("  func5_vparal0_p:  values for debugging purposes.   \n");
    printf("           tau  = %15.10e \n",tau) ; 
    printf("            D   = %15.10e \n",D  ) ; 
    printf("          tau+D = %15.10e \n",tau+D  ) ; 
    printf("           Ssq  = %15.10e \n",Ssq ) ; 
    printf("            y   = %15.10e \n",y ) ; 
    printf("           dis  = %15.10e \n",dis) ; 
    printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
    printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
    return -1 ; 
#endif 
//}}}
  }
  
  sqrt_dis = sqrt( dis ) ; 

  *f = ((t1 - y) * y - t2) * y - t3 
       - gamma_min_one * D/a * y * sqrt_dis ; 

  return 1;
}

/*-------------------------------------------------------------------------
 *
 *  This routine calculates the function, F(q), whose root we are to find
 *  as part of the primitive solve, but appropriate to the case that 
 *   (v_paral)^2  vanishes exactly.  It also calculates the derivative, 
 *   F'(q).  This is called by MHD_Solver_5_vparal0.  Hence the name
 *        func5_vparal0_p_d  
 *  stands for the "function F" in MHD_Solver_5_vparal0 for the 
 *  "p"rimitive solve at a "p"oint together with the "d"erivative in the 
 *  case (v_paral)^2 = 0 .   
 *
 *-------------------------------------------------------------------------*/
int func5_vparal0_p_d(double *f, double *df, double y, double *fpar)  
{

  double  Ssq   = fpar[FP_SSQ];
  double  Bsq   = fpar[FP_BSQ];
  
  double  D     = fpar[FP_D];
  double  tau   = fpar[FP_TAU];
  double  gamma = fpar[FP_GAMMA];
  
  double  a = tau+D;
  double  alpha_sq = 0.5 * Bsq / a ; 
  double  beta_sq  = Ssq / (a * a) ; 
  double  delta = D / a ; 

  double  two_min_gamma = 2.0 - gamma ; 
  double  gamma_min_one = gamma - 1.0 ; 
  double  t0 = alpha_sq * two_min_gamma ; 
  double  t1 = t0 + gamma ; 
  double  t2 = beta_sq * ( gamma - 1.0 ) ;
  double  t3 = t0 * beta_sq ; 
  double  t4 = gamma_min_one * delta ; 

  double  sqrt_dis ; 
  double  dis = y*y - beta_sq ;


  // If the argument of the square root is a small negative value, 
  // we will cheat, assume that this is just roundoff error, force the 
  // value to be positive and hope that we go on. 
  if ( dis > -6.e-16  &&  dis < 0.0 )  
  { 
    //printf("  func5_vparal0_p_d:  The argument of a square root is   \n");
    //printf("  func5_vparal0_p_d:  a tiny bit negative.               \n");
//{{{  ... some tracing if turned on ... 
#if 0
    printf("  func5_vparal0_p_d:  Dumping some values for debugging. \n");
    printf("                    tau  = %25.20e \n", tau) ; 
    printf("                     D   = %25.20e \n", D  ) ; 
    printf("                   tau+D = %25.20e \n", tau+D  ) ; 
    printf("                    Ssq  = %25.20e \n", Ssq ) ; 
    printf("                     y   = %25.20e \n", y ) ; 
    printf("                    y*y  = %25.20e \n", y*y ) ; 
    printf("                 beta_sq = %25.20e \n", beta_sq ) ; 
    printf("                    dis  = %25.20e \n", dis) ; 
    printf("          Ssq/(tau+D)^2  = %25.20e \n", Ssq/(a*a) ) ; 
    printf("    (Ssq+D^2)/(tau+D)^2  = %25.20e \n", (Ssq+D*D)/(a*a) ) ; 
    printf("                    Bsq  = %25.20e \n", Bsq ) ; 
    printf("            tau+D-Bsq/2  = %25.20e \n", tau+D-Bsq/2 ) ; 
#endif 
//}}} 
    dis = 3.6e-31 ;  
  }
  // If the argument of the square root is negative enough, return. 
  else if ( dis <= -6.e-16 )  
  {
    printf("  func5_vparal0_p_d:  The argument of a square root is \n");
    printf("  func5_vparal0_p_d:  a negative.  We will dump some   \n");
//{{{  ... some tracing if turned on ...  
#if 0 
    printf("  func5_vparal0_p_d:  values for debugging purposes.   \n");
    printf("           tau  = %15.10e \n",tau) ; 
    printf("            D   = %15.10e \n",D  ) ; 
    printf("          tau+D = %15.10e \n",tau+D  ) ; 
    printf("           Ssq  = %15.10e \n",Ssq ) ; 
    printf("            y   = %15.10e \n",y ) ; 
    printf("           dis  = %15.10e \n",dis) ; 
    printf("      Ssq/(tau+D)^2  = %15.10e \n",Ssq/(a*a) ) ; 
    printf("   (Ssq+D^2)/(tau+D)^2  = %15.10e \n",(Ssq+D*D)/(a*a) ) ; 
    return -1 ; 
#endif 
//}}}
  }
  sqrt_dis = sqrt( dis ) ; 

  *f =   ((t1 - y) * y - t2) * y - t3 
       - t4 * y * sqrt_dis ; 

  *df =   y * ( 2.0 * t1 - 3.0 * y ) - t2 
        - t4 * ( sqrt_dis + y*y/sqrt_dis ) ;  

  return 1;
}




/*-------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void checkfornans( double *x, char *name, char *routine )
{
    if ( !(*x > 0.0) && !(*x <= 0.0) )
    { fprintf(stderr,"  WARNING: In the routine %s, we get a NAN in the variable %s = %25.20e \n",routine,name,*x); }
}







/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void MathDump(double *fpar)
{

  int ii = (int) fpar[FP_INDEX_X];
  int jj = (int) fpar[FP_INDEX_Y];
  int kk = (int) fpar[FP_INDEX_Z];

  double tau = fpar[FP_TAU];
  double d   = fpar[FP_D];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];
  double Ssq = fpar[FP_SSQ];
  double gamma = fpar[FP_GAMMA];

  fprintf(stdout,"INDEX:  (i,j) = (%d, %d, %d)\n",ii,jj,kk);
  fprintf(stdout,"COORDS: (x,y,z) = (%g, %g, %g)\n",
          fpar[FP_COORD_X],fpar[FP_COORD_Y],fpar[FP_COORD_Z]);
  fprintf(stdout,"---------- MATHEMATICA TRACE ----------\n");
  fprintf(stdout,"tau = %20.16f\n",tau);
  fprintf(stdout,"d = %20.16f\n",d);
  fprintf(stdout,"g = %20.16f\n",gamma);
  fprintf(stdout,"Bsq = %20.16f\n",Bsq);
  fprintf(stdout,"BS = %20.16f\n",BS);
  fprintf(stdout,"Ssq = %20.16f\n",Ssq);
  fprintf(stdout,"dis = 1 - ((2*q+Bsq)*(BS)^2 ");
  fprintf(stdout,"+ q^2*Ssq)/(q^2*(q+Bsq)^2)\n");
  fprintf(stdout,"f = -1/2*BS^2/q^2 ");
  fprintf(stdout,"+ (1/2*Bsq-tau+q/g) ");
  fprintf(stdout,"+ ((g-1)/g*q ");
  fprintf(stdout,"+ Bsq/2)*((2*q+Bsq)*BS^2 ");
  fprintf(stdout,"+ q^2*Ssq)/(q^2*(q+Bsq)^2)-d/g*Sqrt[dis]\n");

  fprintf(stdout,"Wm2[q_] = 1 - ((2*q+Bsq)*(BS)^2 ");
  fprintf(stdout,"+ q^2*Ssq)/(q^2*(q+Bsq)^2)\n");

}
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void MapleDump(double *fpar)
{

  int ii = (int) fpar[FP_INDEX_X];
  int jj = (int) fpar[FP_INDEX_Y];
  int kk = (int) fpar[FP_INDEX_Z];

  double tau = fpar[FP_TAU];
  double d   = fpar[FP_D];
  double Bsq = fpar[FP_BSQ];
  double BS  = fpar[FP_BS];
  double Ssq = fpar[FP_SSQ];
  double gamma = fpar[FP_GAMMA];

  fprintf(stdout,"INDEX:  (i,j) = (%d, %d, %d)\n",ii,jj,kk);
  fprintf(stdout,"COORDS: (x,y,z) = (%g, %g, %g)\n",
          fpar[FP_COORD_X],fpar[FP_COORD_Y],fpar[FP_COORD_Z]);
  fprintf(stdout,"---------- MAPLE TRACE ----------\n");
  fprintf(stdout,"tau := %20.16f;\n",tau);
  fprintf(stdout,"d := %20.16f;\n",d);
  fprintf(stdout,"g := %20.16f;\n",gamma);
  fprintf(stdout,"Bsq := %20.16f;\n",Bsq);
  fprintf(stdout,"BS := %20.16f;\n",BS);
  fprintf(stdout,"Ssq := %20.16f;\n",Ssq);
  fprintf(stdout,"dis := 1 - ((2*q+Bsq)*(BS)**2\n");
  fprintf(stdout,"+ q**2*Ssq)/(q**2*(q+Bsq)**2);\n");
  fprintf(stdout,"f := -1/2*BS**2/q**2\n");
  fprintf(stdout,"+ (1/2*Bsq-tau+q/g)\n");
  fprintf(stdout,"+ ((g-1)/g*q \n");
  fprintf(stdout,"+ Bsq/2)*((2*q+Bsq)*BS**2 \n");
  fprintf(stdout,"+ q**2*Ssq)/(q**2*(q+Bsq)**2)-d/g*sqrt(dis);\n");

}

/*----------------------------------------------------------------------
 *
 *
 *
 *----------------------------------------------------------------------*/
int con_to_prim_axis(coll_point_t *point, const int gen)
{

  const int NC = 3;
  double *u[NC];
  double f[NC];

  index_t index = point->index;
  index_t id;
  for (int i = x_dir; i < n_dim; i++) {
    id.idx[i] = index.idx[i];
  }

  dir_t dir = 0;
  int closest_level = try_closest_level(point, dir);
  if (closest_level == -1) {
    //if there is nothing nearby off axis, we just use the next level
    // up relative to this axis point.
    closest_level = point->level + 1;
  }

  // get the grid spacing
  int h = 1 << (JJ - closest_level);

  //TODO: figure out about this. We perhaps want the additions to occur
  // before and RHS stuff
  u[0] = point->u[gen];
  /* the stencil */
  for (int ll = 1; ll < 3; ll++) {
    coll_point_t *cpm=NULL;
    id.idx[dir] = index.idx[dir] + ll * h;
    assert(check_index(&id) == 1);
    cpm = get_coll_point(&id);
    assert(cpm != NULL);
    u[ll] = cpm->u[gen];
  }

 /* The velocity needs to be extrapolated to the axis. The problem is that
  * the extrapolated velocity may be greater than c. So we need to extrapolate
  * Wv instead. Now in cylindrical coords v^r = 0 on axis, but it is non-zero
  * off axis. v^phi is d(theta)/dt; it's magnitude on axis doesn't matter as
  * it contributes r^2*(v^\phi)^2 to the magnitude of the vector.
  * So I am just going to calculate W using only v^z. */
  f[1] = u[1][V_VY]/sqrt(1.0 - u[1][V_VY]*u[1][V_VY]);
  f[2] = u[2][V_VY]/sqrt(1.0 - u[2][V_VY]*u[2][V_VY]);
  f[0] = (4.0*f[1] - f[2])/3.0;

  u[0][V_RHO] = (4.0*u[1][V_RHO] - u[2][V_RHO])/3.0;
  u[0][V_VX]  = 0.0;
  u[0][V_VY]  = f[0]/sqrt(1.0 + f[0]*f[0]);
  u[0][V_VZ]  = (4.0*u[1][V_VZ] - u[2][V_VZ])/3.0;
  u[0][V_P]   = (4.0*u[1][V_P] - u[2][V_P])/3.0;
  u[0][V_BX]  = 0.0;
  u[0][V_BY]  = (4.0*u[1][V_BY] - u[2][V_BY])/3.0;
  u[0][V_BZ]  = (4.0*u[1][V_BZ] - u[2][V_BZ])/3.0;

  // enforce floor on rho and P on axis.
  u[0][V_RHO] = fmax(pars.vacuum, u[0][V_RHO]);
  u[0][V_P] = fmax(pars.vacuum, u[0][V_P]);

  if (pars.densitized_vars == 0) {
   /* interpolate the conserved variables on axis. Primitive vars should
    * already be interpolated on axis as an initial guess.
    */
    u[0][U_D] = (4.0*u[1][U_D] - u[2][U_D])/3.0;
    u[0][U_SX]  = 0.0;
    u[0][U_SY]  = (4.0*u[1][U_SY] - u[2][U_SY])/3.0;
    u[0][U_SZ]  = (4.0*u[1][U_SZ] - u[2][U_SZ])/3.0;
    u[0][U_TAU] = (4.0*u[1][U_TAU] - u[2][U_TAU])/3.0;
    u[0][U_BX]  = u[0][V_BX];
    u[0][U_BY]  = u[0][V_BY];
    u[0][U_BZ]  = u[0][V_BZ];
    con_to_prim(point->u[gen], point->u[gen], point->coords.pos);
  }

  return 0;
}

// }}}

// rk4 rhs routines {{{
/*-------------------------------------------------------------------------
 *    This routine will verify the existence of the hlle flux stencil points
 *    and will call con_to_prim on any nonessential points that have not had
 *    their prims computed.
 *-------------------------------------------------------------------------*/
void hlle_rhs_stage1(coll_point_t *point, const int gen) {
  for (int dir = x_dir; dir < n_dim; ++dir) {
    int closest_level = get_closest_level(point, dir);
    assert(closest_level >= 1);
    int h = 1 << (JJ - closest_level);

    for (int stcl = 0; stcl < NSTENCIL; ++stcl) {
      //This point must exist, as it is 'point'
      if (stcl == STENCIL_CENTER) {
        continue;
      }

      //what is the index of the stencil point?
      index_t index = point->index;
      index.idx[dir] += (stcl - STENCIL_CENTER) * h;
#ifdef PERIODIC
      index = map_index_into_grid(&index);
#endif

      //check that we are on grid
      if (!check_index(&index)) {
        continue;
      }

      coll_point_t *stcl_point = get_coll_point(&index);
      assert( stcl_point != NULL );

      if (stcl_point->time_stamp < time_stamp) {
        assert(stcl_point->status[CURRENT_STATUS] != essential);
        stcl_point->status[CURRENT_STATUS] = nonessential;
        advance_time_stamp(stcl_point, gen);
      }
    } // stcl points
  } // dir
}


/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void hlle_rhs_stage2(coll_point_t *point, const int gen) {
  for (int ivar = 0; ivar < n_variab; ivar++) {
    point->rhs[gen][ivar] = 0.0;
  }

  // compute the fluxes
  for (dir_t dir = x_dir; dir < n_dim; dir++) {
    num_flux_hlle(point, gen, dir);
  }
}
// }}}

// initial data routines {{{

//  riemann problem {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void riemann_problem(const coord_t *coord, double *u, const int dir) {
#define noTANH_DATA
#ifdef TANH_DATA
 /*
  * A tanh function interpolates between the left and right states.
  * These data are analytic (smooth and differentiable), but do not
  * lead to the same solution as the real shocktube with discontinuous
  * data.
  */
  double w = 0.5 + 0.5*tanh((coord->pos[dir] - pars.id_x0)/pars.id_sigma);
  u[V_RHO] = (1.0 - w) * pars.id_rho_left + w * pars.id_rho_right;
  u[V_VX]  = (1.0 - w) * pars.id_vx_left  + w * pars.id_vx_right;
  u[V_VY]  = (1.0 - w) * pars.id_vy_left  + w * pars.id_vy_right;
  u[V_VZ]  = (1.0 - w) * pars.id_vz_left  + w * pars.id_vz_right;
  u[V_P]   = (1.0 - w) * pars.id_P_left   + w * pars.id_P_right;
  u[V_BX]  = (1.0 - w) * pars.id_Bx_left  + w * pars.id_Bx_right;
  u[V_BY]  = (1.0 - w) * pars.id_By_left  + w * pars.id_By_right;
  u[V_BZ]  = (1.0 - w) * pars.id_Bz_left  + w * pars.id_Bz_right;
  u[V_PSI] = 0.0;
#else
 /*
  * Discontinuous initial data for the Riemann problem.
  */
  if ( coord->pos[dir] < 0.0 ) {
    u[V_RHO] = pars.id_rho_left;
    u[V_VX] = pars.id_vx_left;
    u[V_VY] = pars.id_vy_left;
    u[V_VZ] = pars.id_vz_left;
    u[V_P] = pars.id_P_left;
    u[V_BX] = pars.id_Bx_left;
    u[V_BY] = pars.id_By_left;
    u[V_BZ] = pars.id_Bz_left;
    u[V_PSI] = 0.0;
  } else {
    u[V_RHO] = pars.id_rho_right;
    u[V_VX] = pars.id_vx_right;
    u[V_VY] = pars.id_vy_right;
    u[V_VZ] = pars.id_vz_right;
    u[V_P] = pars.id_P_right;
    u[V_BX] = pars.id_Bx_right;
    u[V_BY] = pars.id_By_right;
    u[V_BZ] = pars.id_Bz_right;
    u[V_PSI] = 0.0;
  }
#endif

  // convert to conserved variables
  prim_to_con(u, coord->pos);
}
// }}}

// riemann problem 2 {{{
/*-------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void riemann_problem2(const coord_t *coord, double *u, const int dir) {
 /*
  * Discontinuous initial data for the Riemann problem. 
  */
  if ( coord->pos[dir] < 0.0 ) {
    u[V_RHO] = pars.id_rho_left;
    u[V_VX ] = pars.id_vx_left;
    u[V_VY ] = pars.id_vy_left;
    u[V_VZ ] = pars.id_vz_left;
    u[V_P  ] = pars.id_P_left;
    u[V_BX ] = pars.id_Bx_left;
    u[V_BY ] = pars.id_By_left;
    u[V_BZ ] = pars.id_Bz_left;
    u[V_PSI] = 0.0;
  } else {
    u[V_RHO] = pars.id_rho_right;
    u[V_VX ] = pars.id_vx_right;
    u[V_VY ] = pars.id_vy_right;
    u[V_VZ ] = pars.id_vz_right;
    u[V_P  ] = pars.id_P_right;
    u[V_BX ] = pars.id_Bx_right;
    u[V_BY ] = pars.id_By_right;
    u[V_BZ ] = pars.id_Bz_right;
    u[V_PSI] = 0.0;
  }

  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

// riemann problem 3 {{{
/*-------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void riemann_problem3(const coord_t *coord, double *u, const int dir) {
 /*
  * Discontinuous initial data for the Riemann problem. 
  */
  if ( coord->pos[dir] < - pars.id_middle_radius ) {
    u[V_RHO] = pars.id_rho_left2;
    u[V_VX ] = pars.id_vx_left2;
    u[V_VY ] = pars.id_vy_left2;
    u[V_VZ ] = pars.id_vz_left2;
    u[V_P  ] = pars.id_P_left2;
    u[V_BX ] = pars.id_Bx_left2;
    u[V_BY ] = pars.id_By_left2;
    u[V_BZ ] = pars.id_Bz_left2;
    u[V_PSI] = 0.0;
  } else if ( coord->pos[dir] >= - pars.id_middle_radius  &&  coord->pos[dir] <= pars.id_middle_radius ) {
    u[V_RHO] = pars.id_rho_mid2;
    u[V_VX ] = pars.id_vx_mid2;
    u[V_VY ] = pars.id_vy_mid2;
    u[V_VZ ] = pars.id_vz_mid2;
    u[V_P  ] = pars.id_P_mid2;
    u[V_BX ] = pars.id_Bx_mid2;
    u[V_BY ] = pars.id_By_mid2;
    u[V_BZ ] = pars.id_Bz_mid2;
    u[V_PSI] = 0.0;
  } else {
    u[V_RHO] = pars.id_rho_right2;
    u[V_VX ] = pars.id_vx_right2;
    u[V_VY ] = pars.id_vy_right2;
    u[V_VZ ] = pars.id_vz_right2;
    u[V_P  ] = pars.id_P_right2;
    u[V_BX ] = pars.id_Bx_right2;
    u[V_BY ] = pars.id_By_right2;
    u[V_BZ ] = pars.id_Bz_right2;
    u[V_PSI] = 0.0;
  }

  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

// riemann problem 2D {{{
/*-------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void riemann_problem_2D(const coord_t *coord, double *u)
{
 /*
  * Discontinuous initial data for the Riemann problem across four quadrants in 2D.
  */
  if ( coord->pos[x_dir] >= 0.0  &&  coord->pos[y_dir] >= 0.0 )
  {
    u[V_RHO] = pars.id_rho_UR;
    u[V_VX ] = pars.id_vx_UR;
    u[V_VY ] = pars.id_vy_UR;
    u[V_VZ ] = pars.id_vz_UR;
    u[V_P  ] = pars.id_P_UR;
    u[V_BX ] = pars.id_Bx_UR;
    u[V_BY ] = pars.id_By_UR;
    u[V_BZ ] = pars.id_Bz_UR;
    u[V_PSI] = 0.0;
  }
  else if ( coord->pos[x_dir] < 0.0  &&  coord->pos[y_dir] >= 0.0 )
  {
    u[V_RHO] = pars.id_rho_UL;
    u[V_VX ] = pars.id_vx_UL;
    u[V_VY ] = pars.id_vy_UL;
    u[V_VZ ] = pars.id_vz_UL;
    u[V_P  ] = pars.id_P_UL;
    u[V_BX ] = pars.id_Bx_UL;
    u[V_BY ] = pars.id_By_UL;
    u[V_BZ ] = pars.id_Bz_UL;
    u[V_PSI] = 0.0;
  }
  else if ( coord->pos[x_dir] < 0.0  &&  coord->pos[y_dir] < 0.0 )
  {
    u[V_RHO] = pars.id_rho_LL;
    u[V_VX ] = pars.id_vx_LL;
    u[V_VY ] = pars.id_vy_LL;
    u[V_VZ ] = pars.id_vz_LL;
    u[V_P  ] = pars.id_P_LL;
    u[V_BX ] = pars.id_Bx_LL;
    u[V_BY ] = pars.id_By_LL;
    u[V_BZ ] = pars.id_Bz_LL;
    u[V_PSI] = 0.0;
  }
  else if ( coord->pos[x_dir] >= 0.0  &&  coord->pos[y_dir] < 0.0 )
  {
    u[V_RHO] = pars.id_rho_LR;
    u[V_VX ] = pars.id_vx_LR;
    u[V_VY ] = pars.id_vy_LR;
    u[V_VZ ] = pars.id_vz_LR;
    u[V_P  ] = pars.id_P_LR;
    u[V_BX ] = pars.id_Bx_LR;
    u[V_BY ] = pars.id_By_LR;
    u[V_BZ ] = pars.id_Bz_LR;
    u[V_PSI] = 0.0;
  }

  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

//  kelvin helmholtz {{{   //FIXME this whole routine needs cleaning up
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
 void kelvin_helmholtz(const coord_t *coord, double *u)
{

  double pi = 3.1415926535897932384626433832 ;
  //double x = coord->pos[x_dir] ; 
  double y = coord->pos[y_dir] ; 

  if ( y < 0.25  &&  y > -0.25 )
  {
    //u[V_RHO] = pars.id_rho_innkh;
    u[V_RHO] = 2.0 ;
    //u[V_VX ] = 0.5 * ( 1.0 + 0.01 * cos(10.0*pi*(coord->pos[x_dir]))*cos(10.0*pi*(coord->pos[y_dir])) ) ;
    u[V_VX ] = 0.5 ;
    //u[V_VY ] = pars.id_vy_innkh;
    //u[V_VY ] = 0.5 * 0.01 * cos(10.0*pi*(coord->pos[x_dir]))*cos(10.0*pi*(coord->pos[y_dir])) ;
    //u[V_VY ] = 0.01 * cos(10.0*pi*(coord->pos[x_dir]))*cos(10.0*pi*(coord->pos[y_dir])) ;
    u[V_VY ] = 0.1 * sin(4.0*pi*(coord->pos[x_dir]))
                   * (   exp( -pow( (coord->pos[y_dir])-0.25 , 2) /0.025 )
                       + exp( -pow( (coord->pos[y_dir])+0.25 , 2) /0.025 ) ) ;
    u[V_VZ ] = 0.0 ; // pars.id_vz_innkh;
    //u[V_P  ] = pars.id_P_innkh;
    u[V_P  ] = 2.5 ;
    u[V_BX ] = 0.0 ; // pars.id_Bx_innkh;
    u[V_BY ] = 0.0 ; // pars.id_By_innkh;
    u[V_BZ ] = 0.0 ; // pars.id_Bz_innkh;
    u[V_PSI] = 0.0;
  }
  else if ( coord->pos[y_dir] >= 0.25  &&  coord->pos[y_dir] <= -0.25 )
  {
    //u[V_RHO] = pars.id_rho_outkh;
    u[V_RHO] = 1.0 ;
    //u[V_VX ] = - 0.5 * ( 1.0 + 0.01 * cos(10.0*pi*coord->pos[x_dir])*cos(10.0*pi*coord->pos[y_dir]) ) ;
    u[V_VX ] = - 0.5 ;
    //u[V_VY ] = pars.id_vy_outkh;
    //u[V_VY ] = - 0.5 * 0.01 * cos(10.0*pi*(coord->pos[x_dir]))*cos(10.0*pi*(coord->pos[y_dir])) ;
    //u[V_VY ] = 0.01 * cos(10.0*pi*(coord->pos[x_dir]))*cos(10.0*pi*(coord->pos[y_dir])) ;
    u[V_VY ] = 0.1 * sin(4.0*pi*(coord->pos[x_dir]))
                   * (   exp( -pow( (coord->pos[y_dir])-0.25 , 2) /0.025 )
                       + exp( -pow( (coord->pos[y_dir])+0.25 , 2) /0.025 ) ) ;
    u[V_VZ ] = 0.0 ; // pars.id_vz_outkh;
    //u[V_P  ] = pars.id_P_outkh;
    u[V_P  ] = 2.5 ;
    u[V_BX ] = 0.0 ; // pars.id_Bx_outkh;
    u[V_BY ] = 0.0 ; // pars.id_By_outkh;
    u[V_BZ ] = 0.0 ; // pars.id_Bz_outkh;
    u[V_PSI] = 0.0;
  }



  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

// kelvin helmholtz ala Radice and Rezzolla {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
 void kelvin_helmholtz_RadRez(const coord_t *coord, double *u)
{

  double pi = 3.1415926535897932384626433832 ;

  if ( coord->pos[y_dir] > 0.0 )
  {
    u[V_RHO] = pars.id_RadRez_rho0 + pars.id_RadRez_rho1 * tanh( ((coord->pos[y_dir]) - 0.5) / pars.id_shear_layer_width ) ;
    u[V_VX ] = pars.id_v_shear * tanh( ((coord->pos[y_dir]) - 0.5) / pars.id_shear_layer_width) ;
    u[V_VY ] = pars.id_A_0 * pars.id_v_shear * sin( 2.0*pi*(coord->pos[x_dir]) ) * exp( - pow( (coord->pos[y_dir]) - 0.5, 2) / pars.id_RadRez_sigma ) ;
    u[V_VZ ] = pars.id_vz_RadRez_top ; // 0.0 ; // pars.id_vz_outkh;
    u[V_P  ] = pars.id_P_RadRez_top ;  // 1.0 ; // pars.id_P_outkh;
    u[V_BX ] = pars.id_Bx_RadRez_top ; // 0.0 ; // pars.id_Bx_outkh;
    u[V_BY ] = pars.id_By_RadRez_top ; // 0.0 ; // pars.id_By_outkh;
    u[V_BZ ] = pars.id_Bz_RadRez_top ; // 0.0 ; // pars.id_Bz_outkh;
    u[V_PSI] = 0.0;
  }
  else if ( coord->pos[y_dir] <= 0.0 )
  {
    u[V_RHO] = pars.id_RadRez_rho0 - pars.id_RadRez_rho1 * tanh( ((coord->pos[y_dir]) + 0.5) / pars.id_shear_layer_width ) ;
    u[V_VX ] = - pars.id_v_shear * tanh( (coord->pos[y_dir] + 0.5) / pars.id_shear_layer_width) ;
    u[V_VY ] = - pars.id_A_0 * pars.id_v_shear * sin( 2.0*pi*(coord->pos[x_dir]) ) * exp( - pow( (coord->pos[y_dir]) + 0.5, 2) / pars.id_RadRez_sigma) ;
    u[V_VZ ] = pars.id_vz_RadRez_bot ; // 0.0 ; // pars.id_vz_outkh;
    u[V_P  ] = pars.id_P_RadRez_bot  ; // 1.0 ; // pars.id_P_innkh;
    u[V_BX ] = pars.id_Bx_RadRez_bot ; // 0.0 ; // pars.id_Bx_innkh;
    u[V_BY ] = pars.id_By_RadRez_bot ; // 0.0 ; // pars.id_By_innkh;
    u[V_BZ ] = pars.id_Bz_RadRez_bot ; // 0.0 ; // pars.id_Bz_innkh;
    u[V_PSI] = 0.0;
  }



  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

// weak field (Carlos' initial data for initial data from had) {{{
/*-------------------------------------------------------------------------
 *
 * Carlos' initial data for initial data from had.
 *
 *-------------------------------------------------------------------------*/
void weak_field(const coord_t *coord, double *u)
{

  const double aa = 0.01;
  const double csigma = 0.1;
  const double A0 = 0.1;

  const double vamp = pars.id_wf_vamp;
  const double rhoamp = pars.id_wf_rhoamp;
  const double Bamp = pars.id_wf_Bamp;
  const double y0 = pars.id_wf_y0;
  const double deltarho = pars.id_wf_deltarho;
  const double pi = acos(-1.0);

  double csign = (y0 > 1.0e-10) ? -1.0 : 1.0;
  double x = coord->pos[x_dir];
  double y = coord->pos[y_dir];

  if (y > 0.0) {
    u[V_RHO] = rhoamp + deltarho * tanh((y-y0)/aa);
    u[V_VX] = vamp * tanh((y-y0)/aa);
    u[V_VY] = A0 * vamp * sin(2.0*pi*x) * exp(-pow((y-y0),2)/csigma);

  }
  else {
    u[V_RHO] = rhoamp + csign * deltarho * tanh((y + y0)/aa);
    u[V_VX]  = csign * vamp * tanh((y + y0)/aa);
    u[V_VY]  = csign * A0 * vamp * sin(2.0*pi*x)
                    * exp(-pow((y + y0),2)/csigma);
  }

  u[V_VZ] = 0.0;
  u[V_P] = 1.0;
  u[V_BX] = 0.0;
  u[V_BY] = 0.0;
  u[V_BZ] = Bamp;
  u[V_PSI] = 0.0;

  prim_to_con(u, coord->pos);

}
//}}}

//  DZBL cylindrical blast {{{
/*-------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void DZBL_cyl_blast(const coord_t *coord, double *u)
{
  double arg;
  arg = (coord->pos[x_dir] - pars.id_x0) * (coord->pos[x_dir] - pars.id_x0);
#if n_dim == 2 || n_dim == 3
  arg += (coord->pos[y_dir] - pars.id_y0)*(coord->pos[y_dir] - pars.id_y0);
#endif
#if n_dim == 3
  arg += (coord->pos[z_dir] - pars.id_z0)*(coord->pos[z_dir] - pars.id_z0);
#endif

  double  rho_coord ;
  rho_coord = sqrt( arg );
  double eps0 = 1.e-14 ;

  /*
  The original test from astro-ph/0210618 had parameters Nx=Ny=250, an 
  initially, outer, static background with rho=1.0, P=0.01, Bx=4.0, and By=0.0.
  The inner region, inside a circle of radius 0.08, had rho=1.0, P=1000, 
  Bx=4.0 and By=0.0.  Gamma = 4/3.   
  */
  if ( rho_coord <= pars.id_inner_radius + eps0 )
  {
    u[V_RHO] = pars.id_rho_inner ;
    u[V_VX ] = pars.id_vx_inner;
    u[V_VY ] = pars.id_vy_inner;
    u[V_VZ ] = pars.id_vz_inner;
    u[V_P  ] = pars.id_P_inner;
    u[V_BX ] = pars.id_Bx_inner;
    u[V_BY ] = pars.id_By_inner;
    u[V_BZ ] = pars.id_Bz_inner;
    u[V_PSI] = 0.0;
  }
  else if ( rho_coord > pars.id_inner_radius )
  {
    u[V_RHO] = pars.id_rho_outer ;
    u[V_VX ] = pars.id_vx_outer;
    u[V_VY ] = pars.id_vy_outer;
    u[V_VZ ] = pars.id_vz_outer;
    u[V_P  ] = pars.id_P_outer;
    u[V_BX ] = pars.id_Bx_outer;
    u[V_BY ] = pars.id_By_outer;
    u[V_BZ ] = pars.id_Bz_outer;
    u[V_PSI] = 0.0;
  }
  else
  { printf("  DZBL_cyl_blast:  Unexpected case in the cylindrical blast wave\n");
    printf("  DZBL_cyl_blast:  of Del Zanna, Bucciantini and Londrillo.\n");
  }


  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

//  relativistic rotor {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void rel_rotor(const coord_t *coord, double *u)
{
  double arg;
  arg = (coord->pos[x_dir] - pars.id_x0) * (coord->pos[x_dir] - pars.id_x0);
#if n_dim == 2 || n_dim == 3
  arg += (coord->pos[y_dir] - pars.id_y0)*(coord->pos[y_dir] - pars.id_y0);
#endif
#if n_dim == 3
  arg += (coord->pos[z_dir] - pars.id_z0)*(coord->pos[z_dir] - pars.id_z0);
#endif

  double  rho_coord ;
  rho_coord = sqrt( arg );

  /*
  The original relativistic rotor as described in astro-ph/0210618 had 
  parameters Nx=Ny=400, an initially, outer, static background with rho=1.0, 
  P=1.0, Bx=1.0, and By=0.0.  The inner region, centered on the axis, is 
  inside a circle of radius 0.10, with rho=10.0, P=1.0, Bx=1.0 and By=0.0.  
  Gamma = 5/3.
  */
  if ( rho_coord <= pars.id_inner_radius )
  {
    u[V_RHO] = pars.id_rho_inner ;
    u[V_VX ] = pars.id_ang_vel * ( - coord->pos[y_dir] ) ;
    u[V_VY ] = pars.id_ang_vel * (   coord->pos[x_dir] ) ;
    u[V_VZ ] = pars.id_vz_inner;
    u[V_P  ] = pars.id_P_inner;
    u[V_BX ] = 0.0 ; // pars.id_Bx_inner;
    u[V_BY ] = 0.0 ; // pars.id_By_inner;
    u[V_BZ ] = 0.0 ; // pars.id_Bz_inner;
    u[V_PSI] = 0.0;
  }
  else if ( rho_coord > pars.id_inner_radius )
  {
    u[V_RHO] = pars.id_rho_outer ;
    u[V_VX ] = pars.id_vx_outer;
    u[V_VY ] = pars.id_vy_outer;
    u[V_VZ ] = pars.id_vz_outer;
    u[V_P  ] = pars.id_P_outer;
    u[V_BX ] = 0.0 ; // pars.id_Bx_outer;
    u[V_BY ] = 0.0 ; // pars.id_By_outer;
    u[V_BZ ] = 0.0 ; // pars.id_Bz_outer;
    u[V_PSI] = 0.0;
  }
  else
  { printf("  rel_rotor:  Unexpected case in the 2D relativistic rotor \n");
    printf("  rel_rotor:  of Del Zanna, Bucciantini and Londrillo.\n");
  }


  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

//  gaussian initial data (1st version) {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void gaussian_data(const coord_t *coord, double *u)
{
  double arg;
  arg = (coord->pos[x_dir] - pars.id_x0) * (coord->pos[x_dir] - pars.id_x0);
#if n_dim == 2 || n_dim == 3
  arg += (coord->pos[y_dir] - pars.id_y0)*(coord->pos[y_dir] - pars.id_y0);
#endif
#if n_dim == 3
  arg += (coord->pos[z_dir] - pars.id_z0)*(coord->pos[z_dir] - pars.id_z0);
#endif

  u[V_RHO] = pars.id_gauss_floor
                + pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));
  u[V_VX] = 0.0;
  u[V_VY] = 0.0;
  u[V_VZ] = 0.0;
  u[V_P] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
  u[V_BX] = pars.id_Bx0;
  u[V_BY] = pars.id_By0;
  u[V_BZ] = pars.id_Bz0;
  u[V_PSI] = 0.0;

  // convert to conserved variables
  prim_to_con(u, coord->pos);
}
//}}}

//  gaussian data 2 {{{
/*------------------------------------------------------------------------
 *    
 *    
 *
 *-------------------------------------------------------------------------*/
void gaussian_data2(const coord_t *coord, double *u)
{
  double arg;
  arg = (coord->pos[x_dir] - pars.id_x0) * (coord->pos[x_dir] - pars.id_x0);
#if n_dim == 2 || n_dim == 3
  arg += (coord->pos[y_dir] - pars.id_y0)*(coord->pos[y_dir] - pars.id_y0);
#endif
#if n_dim == 3
  arg += (coord->pos[z_dir] - pars.id_z0)*(coord->pos[z_dir] - pars.id_z0);
#endif

#if 0 
  double arg2;
  arg2 = (coord->pos[x_dir] - pars.id_x02) * (coord->pos[x_dir] - pars.id_x02);
#if n_dim == 2 || n_dim == 3
  arg2 += (coord->pos[y_dir] - pars.id_y02)*(coord->pos[y_dir] - pars.id_y02);
#endif
#if n_dim == 3
  arg2 += (coord->pos[z_dir] - pars.id_z02)*(coord->pos[z_dir] - pars.id_z02);
#endif
#endif

#if 0
  u[V_RHO] = pars.id_gauss_floor 
                + pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));
  u[V_VX] = pars.id_vx0 ; 
  u[V_VY] = pars.id_vy0 ; 
  u[V_VZ] = pars.id_vz0 ;
  u[V_P] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
  u[V_BX] = pars.id_Bx0;
  u[V_BY] = pars.id_By0;
  u[V_BZ] = pars.id_Bz0;
  u[V_PSI] = 0.0;
#endif


#if 1
// (ewh)
    double tmp ;
    tmp = pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));

  if ( tmp >= pars.id_gauss_floor )
  {
    //u[V_RHO] = pars.id_gauss_floor 
    //            + pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));
    u[V_RHO] = tmp ;
    u[V_VX] = pars.id_vx0;
    u[V_VY] = pars.id_vy0;
    u[V_VZ] = pars.id_vz0;
    u[V_P] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
    u[V_BX] = pars.id_Bx0;
    u[V_BY] = pars.id_By0;
    u[V_BZ] = pars.id_Bz0;
    u[V_PSI] = 0.0;
  }
  else if ( tmp < pars.id_gauss_floor )
  {
    u[V_RHO] = pars.id_gauss_floor ;
    u[V_VX] = 0.0;
    u[V_VY] = 0.0;
    u[V_VZ] = 0.0;
    u[V_P] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
    u[V_BX] = pars.id_Bx0;
    u[V_BY] = pars.id_By0;
    u[V_BZ] = pars.id_Bz0;
    u[V_PSI] = 0.0;
  }
  else
  { fprintf(stderr," unexpected case in if-else in gaussian data \n") ; }
#endif

#if 0
// (ewh)
    double tmp ;
    double tmp2 ;
    tmp = pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));
    tmp2= pars.id_amp2* exp(-arg2/(pars.id_sigma * pars.id_sigma));

  if ( tmp+tmp2 >= pars.id_gauss_floor )
  {
    if ( tmp >= pars.id_gauss_floor )
    {  
      u[V_RHO] = tmp ;
      u[V_VX ] = pars.id_vx0;
      u[V_VY ] = pars.id_vy0;
      u[V_VZ ] = pars.id_vz0;
      u[V_P  ] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
      u[V_BX ] = pars.id_Bx0;
      u[V_BY ] = pars.id_By0;
      u[V_BZ ] = pars.id_Bz0;
      u[V_PSI] = 0.0;
    }
    else if ( tmp2 >= pars.id_gauss_floor )
    {
      u[V_RHO] = tmp2 ;
      u[V_VX ] = - pars.id_vx0;
      u[V_VY ] = - pars.id_vy0;
      u[V_VZ ] = - pars.id_vz0;
      u[V_P  ] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
      u[V_BX ] = pars.id_Bx0;
      u[V_BY ] = pars.id_By0;
      u[V_BZ ] = pars.id_Bz0;
      u[V_PSI] = 0.0;
    }
  }
  else if ( tmp+tmp2 < pars.id_gauss_floor )
  {
    u[V_RHO] = pars.id_gauss_floor ;
    u[V_VX ] = 0.0;
    u[V_VY ] = 0.0;
    u[V_VZ ] = 0.0;
    u[V_P  ] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
    u[V_BX ] = pars.id_Bx0;
    u[V_BY ] = pars.id_By0;
    u[V_BZ ] = pars.id_Bz0;
     u[V_PSI] = 0.0;
  }
  else
  { fprintf(stderr," unexpected case in if-else in gaussian data \n") ; }
#endif

  // convert to conserved variables
  //prim_to_con(u);
  prim_to_con(u, coord->pos);
}
//}}}

//  duffel macfadyen initial data {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void duffel_macfadyen_rt1(const coord_t *coord, double *u)
{

  double rsq = coord->pos[x_dir]*coord->pos[x_dir];
#if n_dim == 2 || n_dim == 3
  rsq += coord->pos[y_dir]*coord->pos[y_dir];
#endif
#if n_dim == 3
  rsq += coord->pos[z_dir]*coord->pos[z_dir];
#endif
  double r = sqrt(rsq);

  const double k0 = 4.0;
  const double r0 = 0.1;
  const double r_min = 0.001;
  const double r_exp = 0.003;

  double e0;
  if (pars.id_k == 0) {
    e0 = 6.0;
  }
  else if (pars.id_k == 1) {
    e0 = 4.0;
  }
  else if (pars.id_k == 2) {
    e0 = 6.0;
  }
  else {
    printf("pars.id_k = %d is out of range.  It must be 0, 1, or 2\n",
            pars.id_k);
    exit(-2);
  }

  if (r < r_min) {
    u[V_RHO] = pars.id_rho0*pow(r0/r_min, k0);
  }
  else if (r < r0) {
    u[V_RHO] = pars.id_rho0*pow(r0/r, k0);
  }
  else {
    u[V_RHO] = pars.id_rho0*pow(r0/r,pars.id_k);
  }

  if (r < r_exp) {
    u[V_P] = u[V_RHO]*e0/3.0;
  }
  else {
    u[V_P] = 1.0e-6 * u[V_RHO];
  }

  u[V_VX] = 0.0;
  u[V_VY] = 0.0;
  u[V_VZ] = 0.0;
  u[V_BX] = 0.0;
  u[V_BY] = 0.0;
  u[V_BZ] = 0.0;
  u[V_PSI] = 0.0;

  // convert to conserved variables
  prim_to_con(u, coord->pos);

}
// }}}

//{{{  duffel macfadyen initial data with magnetic field  
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void duffel_macfadyen_rt1_mag(const coord_t *coord, double *u)
{

  double rsq = coord->pos[x_dir]*coord->pos[x_dir];
#if n_dim == 2 || n_dim == 3
  rsq += coord->pos[y_dir]*coord->pos[y_dir];
#endif
#if n_dim == 3
  rsq += coord->pos[z_dir]*coord->pos[z_dir];
#endif
  double r = sqrt(rsq);

  //double phi_id = atan2(coord->pos[y_dir] , coord->pos[x_dir]) ; 

  const double k0 = 4.0;
  const double r0 = 0.1;
  const double r_min = 0.001;
  const double r_exp = 0.003;

  double e0;
  if (pars.id_k == 0) {
    e0 = 6.0;
  }
  else if (pars.id_k == 1) {
    e0 = 4.0;
  }
  else if (pars.id_k == 2) {
    e0 = 6.0;
  }
  else {
    printf("pars.id_k = %d is out of range.  It must be 0, 1, or 2\n",
            pars.id_k);
    exit(-2);
  }

  if (r < r_min) {
    u[V_RHO] = pars.id_rho0*pow(r0/r_min, k0);
  }
  else if (r < r0) {
    u[V_RHO] = pars.id_rho0*pow(r0/r, k0);
  }
  else {
    u[V_RHO] = pars.id_rho0*pow(r0/r,pars.id_k);
  }

  if (r < r_exp) {
    u[V_P] = u[V_RHO]*e0/3.0;
  }
  else {
    u[V_P] = 1.0e-6 * u[V_RHO];
  }

  u[V_VX] = 0.0;
  u[V_VY] = 0.0;
  u[V_VZ] = 0.0;
/*  
  if ( r < r0 ) // should this be r_min instead?  
  { 
    u[V_BX] = 0.0 ;
    u[V_BY] = 2.0 * pars.id_dufmac_Bamp / ( r0 * r0 * r0 ) ; 
  }
  else if ( r >= r0 ) 
  { 
    u[V_BX] = 3.0 * pars.id_dufmac_Bamp 
                  * coord->pos[x_dir] 
                  * coord->pos[y_dir] 
                  / ( r*r*r*r*r + 1.e-14 ) ;
    u[V_BY] =       pars.id_dufmac_Bamp 
                  * (   2.0 * coord->pos[y_dir] * coord->pos[y_dir] 
                      -       coord->pos[x_dir] * coord->pos[x_dir]  ) 
                  / ( r*r*r*r*r + 1.e-14 );
  }
*/
  u[V_BX] = 0.0;
  u[V_BY] = pars.id_dufmac_Bamp ;
  //u[V_BX] = 0.0;
  //u[V_BY] = 0.0;  
  u[V_BZ] = 0.0;
  u[V_PSI] = 0.0;

  // convert to conserved variables
  prim_to_con(u, coord->pos);

}
//}}}



//  gaussian initial data to test the psi equation {{{
/*-------------------------------------------------------------------------
 *
 *
 *
 *-------------------------------------------------------------------------*/
void gaussian_data_psitest(const coord_t *coord, double *u)
{
  double arg;
  arg = (coord->pos[x_dir] - pars.id_x0) * (coord->pos[x_dir] - pars.id_x0);
#if n_dim == 2 || n_dim == 3
  arg += (coord->pos[y_dir] - pars.id_y0)*(coord->pos[y_dir] - pars.id_y0);
#endif
#if n_dim == 3
  arg += (coord->pos[z_dir] - pars.id_z0)*(coord->pos[z_dir] - pars.id_z0);
#endif

  u[V_RHO] = pars.id_gauss_floor
                + pars.id_amp * exp(-arg/(pars.id_sigma * pars.id_sigma));
  u[V_VX] = 0.0;
  u[V_VY] = 0.0;
  u[V_VZ] = 0.0;
  u[V_P] = pars.id_kappa * pow(u[V_RHO], pars.gamma);
  u[V_BX] = pars.id_Bx0;
  u[V_BY] = pars.id_By0;
  u[V_BZ] = pars.id_Bz0;
  //u[V_PSI] = exp(-0.5*(pow((coord->pos[x_dir])/pars.id_sigma,2) - (coord->pos[x_dir])/pars.id_sigma )) ;
  u[V_PSI] = exp(-(arg/pow(pars.id_sigma,2) - 0.5*(coord->pos[x_dir])/pars.id_sigma )) ;
  //u[V_PSI] = exp(-0.5*(arg/pow(pars.id_sigma,2))) ;

  // convert to conserved variables
  prim_to_con(u, coord->pos);
}
//}}}


//}}}


#endif
