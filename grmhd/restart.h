#ifndef __RESTART_H__
#define __RESTART_H__


#include "hawaii-param.h"
#include "stats.h"


int write_restart_file(double t_curr, unsigned time_stamp,
                        time_stats_t *ts, int snap);
int read_restart_file(double *t_curr, unsigned *time_stamp,
                        time_stats_t *ts, int *snap);
void complete_wavelet_stencils();


#endif // __RESTART_H__
