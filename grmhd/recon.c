#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <assert.h>
#include "hawaii.h"
#include "hawaii-rmhd.h"

#define SQR(x) ((x)*(x))

// WENO5 reconstruction {{{
const double weno_eps = 1.0e-20;
int  do_wenoz = 1; 
int  do_adaptive_epsilon = 0;

/*----------------------------------------------------------------------
 *
 *   weno5_reconstruction 
 *
 *----------------------------------------------------------------------*/
void weno5_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim])
{
  double ulf[NSTENCIL], urf[NSTENCIL], uf[NSTENCIL];

  // reconstruct the primitive variables. Including each component of v^i
  for (int m = 0; m < n_prims; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = u[i][m];
    }
    GRHydro_WENOReconstruct1d(NSTENCIL, uf, urf, ulf);
    for (int i = 0; i < NSTENCIL; i++) {
      ul[i][m] = ulf[i];
      ur[i][m] = urf[i];
    }
  }

  // enforce the floor
  for (int i = 0; i < NSTENCIL; i++) {
    ul[i][V_RHO] = fmax(pars.vacuum, ul[i][V_RHO]);
    ur[i][V_RHO] = fmax(pars.vacuum, ur[i][V_RHO]);
    ul[i][V_P] = fmax(pars.vacuum, ul[i][V_P]);
    ur[i][V_P] = fmax(pars.vacuum, ur[i][V_P]);
  }

  // reconstruct Wv, and overwrite components of v^i
  double vec[3], wv[NSTENCIL][3], wvl[NSTENCIL][3], wvr[NSTENCIL][3];

  for (int i = 0; i < NSTENCIL; i++) {
    recon_wv(vec, u[i], pos[i]);
    wv[i][0] = vec[0];
    wv[i][1] = vec[1];
    wv[i][2] = vec[2];
  }
  for (int m = 0; m < 3; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = wv[i][m];
    }
    GRHydro_WENOReconstruct1d(NSTENCIL, uf, urf, ulf);
    for (int i = 0; i < NSTENCIL; i++) {
      wvl[i][m] = ulf[i];
      wvr[i][m] = urf[i];
    }
  }

  for (int i = 1; i < NSTENCIL-1; i++) {
    double gd[3][3];
    load_metric(gd, ul[i], pos_h[i]);
    double wvl3[3], wvr3[3];
    for (int m = 0; m < 3; m++) {
      wvl3[m] = wvl[i][m];
      wvr3[m] = wvr[i][m];
    }
    double wl = sqrt(1.0 + square_vector(wvl3, gd));
    double wr = sqrt(1.0 + square_vector(wvr3, gd));
    for (int m = 0; m < 3; m++) {
      ul[i][V_VX+m] = wvl3[m]/ wl;
      ur[i][V_VX+m] = wvr3[m]/ wr;
    }
  }

}

/**
   WENO5 reconstruction operator.
   Supports standard WENO5 (with and without adaptive epsilon), and WENO-z.
*/
/*----------------------------------------------------------------------
 *
 *    GRHydro_WENOReconstruct1d: WENO5 reconstruction
 *
 *----------------------------------------------------------------------*/
void GRHydro_WENOReconstruct1d (
      const int nx, const double* const restrict a,
      double* const restrict aminus, double* const restrict aplus)
{
   
#define A(i_) (a[ijk[i_]])
#define Aplus(i_) (aplus[ijk[i_]])
#define Aminus(i_) (aminus[ijk[i_]])
   
   for (int i=2; i < nx-2; ++i)
   {
      const int ijk[5] = { i-2, i-1, i  , i+1, i+2 };
                       
   
         
   
      assert(! (do_wenoz && do_adaptive_epsilon) && "Adaptive_epsilon not supported for WENO-Z");

      if (do_wenoz)
      {
         static const double 
                         weno_coeffs[3][5] = { { 2.0/6.0, -7.0/6.0, 11.0/6.0, 0,        0 }, 
                                               { 0,       -1.0/6.0, 5.0/6.0,  2.0/6.0,  0 },
                                               { 0,        0,       2.0/6.0,  5.0/6.0, -1.0/6.0 } };
      
         const double beta1 = 13.0/12.0*SQR(A(0)-2.0*A(1)+A(2)) + 1.0/4.0*SQR(A(0)-4.0*A(1)+3.0*A(2));
         const double beta2 = 13.0/12.0*SQR(A(1)-2.0*A(2)+A(3)) + 1.0/4.0*SQR(A(1)-A(3));
         const double beta3 = 13.0/12.0*SQR(A(2)-2.0*A(3)+A(4)) + 1.0/4.0*SQR(3.0*A(2)-4.0*A(3)+A(4));
            
            
         //    Compute weights according to weno-z alorithm
         const double wbarplus1 = 1.0/10.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta1));
         const double wbarplus2 = 3.0/5.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta2));
         const double wbarplus3 = 3.0/10.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta3));

         const double wbarminus1 = 3.0/10.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta1));
         const double wbarminus2 = 3.0/5.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta2));
         const double wbarminus3 = 1.0/10.0 * (1.0 + fabs(beta1-beta3) / (weno_eps + beta3));
         
         const double iwbarplussum = 1.0 / (wbarplus1 + wbarplus2 + wbarplus3);
         
         const double wplus1 = wbarplus1 * iwbarplussum;
         const double wplus2 = wbarplus2 * iwbarplussum;
         const double wplus3 = wbarplus3 * iwbarplussum;
         
         const double iwbarminussum = 1.0 / (wbarminus1 + wbarminus2 + wbarminus3);
         
         const double wminus1 = wbarminus1 * iwbarminussum;
         const double wminus2 = wbarminus2 * iwbarminussum;
         const double wminus3 = wbarminus3 * iwbarminussum;
         
         //    Calculate the reconstruction
         Aplus(3) = 0;
         Aminus(2) = 0;
         for (int j=0; j < 5; ++j) {
               Aplus(3) += (wplus1 * weno_coeffs[0][j]
                          + wplus2 * weno_coeffs[1][j]
                          + wplus3 * weno_coeffs[2][j]) * A(j);
               Aminus(2) += (wminus1 * weno_coeffs[2][4-j]
                           + wminus2 * weno_coeffs[1][4-j]
                           + wminus3 * weno_coeffs[0][4-j]) * A(j);
         }
      } else {
         
         static const double beta_shu[3][6] = { { 4.0/3.0,  -19.0/3.0, 25.0/3.0, 11.0/3.0, -31.0/3.0, 10.0/3.0 },
                                      { 4.0/3.0,  -13.0/3.0, 13.0/3.0, 5.0/3.0,  -13.0/3.0, 4.0/3.0 },
                                      { 10.0/3.0, -31.0/3.0, 25.0/3.0, 11.0/3.0, -19.0/3.0, 4.0/3.0 } };
         static const double weno_coeffs[3][5] = { { 3.0/8.0, -5.0/4.0, 15.0/8.0, 0,      0 },
                                         { 0,       -1.0/8.0, 3.0/4.0,  3.0/8.0, 0 },
                                         { 0,       0,        3.0/8.0,  3.0/4.0, -1.0/8.0 } };
                                      
         //    Compute smoothness indicators
         //    This is from Tchekhovskoy et al 2007 (WHAM code paper).
         double beta1  = beta_shu[0][0]*SQR(A(0))
                  + beta_shu[0][1]*A(0)*A(1)
                  + beta_shu[0][2]*SQR(A(1))
                  + beta_shu[0][3]*A(0)*A(2)
                  + beta_shu[0][4]*A(1)*A(2)
                  + beta_shu[0][5]*SQR(A(2));
         
         double beta2  = beta_shu[1][0]*SQR(A(1))
                  + beta_shu[1][1]*A(1)*A(2)
                  + beta_shu[1][2]*SQR(A(2))
                  + beta_shu[1][3]*A(1)*A(3)
                  + beta_shu[1][4]*A(2)*A(3)
                  + beta_shu[1][5]*SQR(A(3));
         
         double beta3  = beta_shu[2][0]*SQR(A(2))
                  + beta_shu[2][1]*A(2)*A(3)
                  + beta_shu[2][2]*SQR(A(3))
                  + beta_shu[2][3]*A(2)*A(4)
                  + beta_shu[2][4]*A(3)*A(4)
                  + beta_shu[2][5]*SQR(A(4));
         
         
         if (do_adaptive_epsilon) {
            const double vnorm = (SQR(A(0)) + SQR(A(1)) + SQR(A(2)) + SQR(A(3)) + SQR(A(4)));
               
            beta1 += 100.0*weno_eps*(vnorm + 1.0);
            beta2 += 100.0*weno_eps*(vnorm + 1.0);
            beta3 += 100.0*weno_eps*(vnorm + 1.0);
               
            const double ibetanorm = 1.0 / (beta1 + beta2 + beta3);
               
            beta1 *= ibetanorm;
            beta2 *= ibetanorm;
            beta3 *= ibetanorm;
         }
         
         const double wbarplus1 = 1.0/16.0 / SQR(weno_eps + beta1);
         const double wbarplus2 = 5.0/8.0 / SQR(weno_eps + beta2);
         const double wbarplus3 = 5.0/16.0 / SQR(weno_eps + beta3);
         
         const double iwbarplussum = 1.0 / (wbarplus1 + wbarplus2 + wbarplus3);
         
         const double wplus1 = wbarplus1 * iwbarplussum;
         const double wplus2 = wbarplus2 * iwbarplussum;
         const double wplus3 = wbarplus3 * iwbarplussum;

         const double wbarminus1 = 5.0/16.0 / SQR(weno_eps + beta1);
         const double wbarminus2 = 5.0/8.0 / SQR(weno_eps + beta2);
         const double wbarminus3 = 1.0/16.0 / SQR(weno_eps + beta3);
         
         const double iwbarminussum = 1.0 / (wbarminus1 + wbarminus2 + wbarminus3);
         
         const double wminus1 = wbarminus1 * iwbarminussum;
         const double wminus2 = wbarminus2 * iwbarminussum;
         const double wminus3 = wbarminus3 * iwbarminussum;
                                         
         //    Calculate the reconstruction
         Aplus(3) = 0;
         Aminus(2) = 0;
         for (int j=0; j < 5; ++j) {
               Aplus(3) += (wplus1 * weno_coeffs[0][j]
                          + wplus2 * weno_coeffs[1][j]
                          + wplus3 * weno_coeffs[2][j]) * A(j);
               Aminus(2) += (wminus1 * weno_coeffs[2][4-j]
                           + wminus2 * weno_coeffs[1][4-j]
                           + wminus3 * weno_coeffs[0][4-j]) * A(j);
         }
      }
   }

#if 1
   aplus[0] = a[0];
   aplus[1] = a[0];
   aplus[2] = a[1];
   aminus[0] = a[0];
   aminus[1] = a[1];

   aminus[nx-2] = a[nx-2];
   aminus[nx-1] = a[nx-1];
   aplus[nx-1] = a[nx-2];
#endif


}
// }}}

// MP5 Reconstruction {{{

int do_MP5_adaptive_epsilon = 1;

/*----------------------------------------------------------------------
 *
 *   grhmp5_reconstruction 
 *
 *----------------------------------------------------------------------*/
void grhmp5_reconstruction(double ul[NSTENCIL][n_variab + n_aux],
                        double ur[NSTENCIL][n_variab + n_aux],
                        double *u[NSTENCIL],
                        double *pos[NSTENCIL], double pos_h[NSTENCIL][n_dim])
{
  double ulf[NSTENCIL], urf[NSTENCIL], uf[NSTENCIL];

  // reconstruct the primitive variables. Including each component of v^i
  for (int m = 0; m < n_prims; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = u[i][m];
    }
    GRHydro_MP5Reconstruct1d(NSTENCIL, uf, urf, ulf);
    for (int i = 0; i < NSTENCIL; i++) {
      ul[i][m] = ulf[i];
      ur[i][m] = urf[i];
    }
  }

  // enforce the floor
  for (int i = 0; i < NSTENCIL; i++) 
  {
    ul[i][V_RHO] = fmax(pars.vacuum, ul[i][V_RHO]);
    ur[i][V_RHO] = fmax(pars.vacuum, ur[i][V_RHO]);
    ul[i][V_P] = fmax(pars.vacuum, ul[i][V_P]);
    ur[i][V_P] = fmax(pars.vacuum, ur[i][V_P]);
  }

  // reconstruct Wv, and overwrite components of v^i
  double vec[3], wv[NSTENCIL][3], wvl[NSTENCIL][3], wvr[NSTENCIL][3];

  for (int i = 0; i < NSTENCIL; i++) {
    recon_wv(vec, u[i], pos[i]);
    wv[i][0] = vec[0];
    wv[i][1] = vec[1];
    wv[i][2] = vec[2];
  }
  for (int m = 0; m < 3; m++) {
    for (int i = 0; i < NSTENCIL; i++) {
      uf[i] = wv[i][m];
    }
    GRHydro_MP5Reconstruct1d(NSTENCIL, uf, urf, ulf);
    for (int i = 0; i < NSTENCIL; i++) {
      wvl[i][m] = ulf[i];
      wvr[i][m] = urf[i];
    }
  }

  for (int i = 1; i < NSTENCIL-1; i++) {
    double gd[3][3];
    load_metric(gd, ul[i], pos_h[i]);
    double wvl3[3], wvr3[3];
    for (int m = 0; m < 3; m++) {
      wvl3[m] = wvl[i][m];
      wvr3[m] = wvr[i][m];
    }
    double wl = sqrt(1.0 + square_vector(wvl3, gd));
    double wr = sqrt(1.0 + square_vector(wvr3, gd));
    for (int m = 0; m < 3; m++) {
      ul[i][V_VX+m] = wvl3[m]/ wl;
      ur[i][V_VX+m] = wvr3[m]/ wr;
    }
  }

}


/*----------------------------------------------------------------------
 *
 *    MP5
 *
 *----------------------------------------------------------------------*/
//static inline double SQR (double const x) { return x*x; }

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

#define MIN3(a,b,c) (MIN(a, MIN(b,c)))
#define MIN4(a,b,c,d) (MIN(a, MIN(b,MIN(c,d))))
#define MAX3(a,b,c) (MAX(a, MAX(b,c)))

#define MINMOD(x,y)  \
     (0.5*(copysign(1.0,x) + copysign(1.0,y)) * MIN(fabs(x), fabs(y)))

#define MINMOD4(w,x,y,z) \
     (0.125*( copysign(1.0,w)+copysign(1.0,x) )*fabs( (copysign(1.0,w)+copysign(1.0,y)) * (copysign(1.0,w)+copysign(1.0,z)) )*MIN4(fabs(w), fabs(x), fabs(y), fabs(z)))

/*----------------------------------------------------------------------
 *
 *    
 *
 *----------------------------------------------------------------------*/
static inline double MP5(const double am2, 
                            const double am1, 
                            const double a, 
                            const double ap1, 
                            const double ap2,
                            const double anorm,
                            const double mp5_eps,
                            const double mp5_alpha
                            )
{
   const double vl = (2.0*am2 - 13.0*am1 + 47.0*a + 27.0*ap1 - 3.0*ap2) * 1.0/60.0;
   const double vmp = a + MINMOD( ap1-a, mp5_alpha*(a-am1) );
   if ((vl-a)*(vl-vmp) <= mp5_eps*anorm)
      return vl;
   else {
      const double djm1 = am2 -2.0*am1 + a;
      const double dj   = am1 -2.0*a + ap1;
      const double djp1 = a -2.0*ap1 + ap2;
      const double dm4jph = MINMOD4(4.0*dj-djp1, 4.0*djp1-dj, dj, djp1);
      const double dm4jmh = MINMOD4(4.0*dj-djm1, 4.0*djm1-dj, dj, djm1);
      const double vul = a + mp5_alpha*(a-am1);
      const double vav = 0.5*(a+ap1);
      const double vmd = vav - 0.5*dm4jph;
      const double vlc = a + 0.5*(a-am1) + 4.0/3.0*dm4jmh;
      const double vmin = MAX(MIN3(a,ap1,vmd), MIN3(a,vul,vlc));
      const double vmax = MIN(MAX3(a,ap1,vmd), MAX3(a,vul,vlc));
      return vl + MINMOD(vmin-vl, vmax-vl);
   }
   return 0;
}


/*----------------------------------------------------------------------
 *
 *    MP5 reconstruction
 *
 *----------------------------------------------------------------------*/
void GRHydro_MP5Reconstruct1d (const int nx,
                         const double* const restrict a,
                         double* const restrict aminus,
                         double* const restrict aplus
                        )
{
#define A(i_) (a[ijk[i_]])
#define Aplus(i_) (aplus[ijk[i_]])
#define Aminus(i_) (aminus[ijk[i_]])

   const double mp5_alpha = 4.0;
   const double mp5_eps = 1.0e-10;

   
   for (int i=2; i < nx-2; ++i)
   {
      const int ijk[5] = { i-2, i-1, i, i+1, i+2 };
                       
      if (!do_MP5_adaptive_epsilon) {
                         
         Aplus(3)  = MP5(A(0), A(1), A(2), A(3), A(4), 1.0, mp5_eps, mp5_alpha);
         Aminus(2) = MP5(A(4), A(3), A(2), A(1), A(0), 1.0, mp5_eps, mp5_alpha);
   
      } else {
         
         const double anorm = sqrt(SQR(A(0)) + SQR(A(1)) + SQR(A(2)) + SQR(A(3)) + SQR(A(4)));
         
         Aplus(3)  = MP5(A(0), A(1), A(2), A(3), A(4), anorm, mp5_eps, mp5_alpha);
         Aminus(2) = MP5(A(4), A(3), A(2), A(1), A(0), anorm, mp5_eps, mp5_alpha);
         
      }

   }

   aplus[0] = a[0];
   aplus[1] = a[0];
   aplus[2] = a[1];
   aminus[0] = a[0];
   aminus[1] = a[1];

   aminus[nx-2] = a[nx-2];
   aminus[nx-1] = a[nx-1];
   aplus[nx-1] = a[nx-2];

}


