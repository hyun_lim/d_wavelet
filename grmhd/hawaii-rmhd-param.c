#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hawaii.h"

#ifdef RMHD

#ifdef RNPL
#include "bbhutil.h"

char *read_line(FILE *fin) {
    char *buffer;
    char *tmp;
    int read_chars = 0;
    int bufsize = 512;
    char *line = malloc(bufsize);

    if ( !line ) {
        return NULL;
    }

    buffer = line;

    while ( fgets(buffer, bufsize - read_chars, fin) ) {
        read_chars = strlen(line);

        if ( line[read_chars - 1] == '\n' ) {
            line[read_chars - 1] = '\0';
            return line;
        }

        else {
            bufsize = 2 * bufsize;
            tmp = realloc(line, bufsize);
            if ( tmp ) {
                line = tmp;
                buffer = line + read_chars;
            }
            else {
                free(line);
                return NULL;
            }
        }
    }
    return NULL;
}

int read_param_file(char *pfile)
{

  int ok = 1;
  const double huge  = 1.0e14;
  const double small = 1.0e-14;

  ok *= read_int_param(pfile, "max_iter", &pars.max_iter, 1, 1, 5000000);
  ok *= read_int_param(pfile, "prune_frequency", &pars.prune_frequency,1,1,50);
  ok *= read_real_param(pfile, "epsilon", &pars.epsilon, 1.0e-3, small, 0.5);

  pars.t0 = 0.0; // hard code the initial time.

 /* note. If "tf" can not be read, then we assume that the parameter file
  * does not exist, and we abort. Some parameter needs to trigger the abort.
  */
  if (get_param(pfile, "tf", "double", 1, &pars.tf) != 1) {
    printf("\n-------------------------------------------------------------\n");
    printf("The parameter 'tf' must be explicitly set in the file %s.\n",pfile);
    printf("Either the file %s does not exist,\n",pfile);
    printf("or the parameter 'tf' must be set in the file.\n");
    printf("\n-------------------------------------------------------------\n");
    return -1;
  }

  ok *= read_real_param(pfile, "xmin", &pars.xmin, 0.0, -huge, huge);
  ok *= read_real_param(pfile, "xmax", &pars.xmax, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "ymin", &pars.ymin, 0.0, -huge, huge);
  ok *= read_real_param(pfile, "ymax", &pars.ymax, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "zmin", &pars.zmin, 0.0, -huge, huge);
  ok *= read_real_param(pfile, "zmax", &pars.zmax, 1.0, -huge, huge);
  ok *= read_real_param(pfile, "cfl", &pars.cfl, 0.1, small, 0.5);

  // relativistic fluid parameters
  ok *= read_real_param(pfile, "gamma", &pars.gamma, 1.6, 1.0, 2.0);
  ok *= read_real_param(pfile, "vacuum", &pars.vacuum, 1.0e-10, small, 1.0);
  ok *= read_int_param(pfile, "contoprimwarn", &pars.contoprimwarn, 1, 0, 1);
  ok *= read_int_param(pfile, "eos_solver", &pars.eos_solver, 1, 1, 5);
  ok *= read_int_param(pfile, "use_wave_speeds", &pars.use_wave_speeds, 1, 0, 1);
  ok *= read_int_param(pfile, "reconstruct_Wv", &pars.reconstruct_Wv, 1, 0, 1);
  ok *= read_int_param(pfile, "densitized_vars", &pars.densitized_vars, 1, 0, 1);

  // Restart related parameter
  ok *= read_real_param(pfile, "time_between_restarts",
                          &pars.time_between_restarts, 3600.0, 0, huge);
  // Statistics related parameters
  ok *= read_real_param(pfile, "max_runtime", &pars.max_runtime,
                          86400.0, 0.0, huge);
  ok *= read_int_param(pfile, "stats_output_frequency",
                          &pars.stats_output_frequency, 0, 0, 1000000);
  ok *= read_int_param(pfile, "cons_test_frequency",
                          &pars.cons_test_frequency, 0, 0, 1000000);


  // boundary conditions
  char **bcs;
  bcs = (char **) malloc(2*n_dim*sizeof(char *));
  for (int i = 0; i < 2*n_dim; i++) {
    bcs[i] = (char *) malloc(32*sizeof(char));
  }
#ifdef PERIODIC
  for (int i = 0; i < 2 * n_dim; ++i) {
    pars.bcs[i] = bc_periodic;
  }
  printf("## all boundary conditions are periodic\n");
#else
  if (get_str_param(pfile, "boundaries", bcs, 2*n_dim)!= 1) {
    printf("## setting all boundaries to outflow\n");
    for(int i = 0; i < 2*n_dim; i++) {
      pars.bcs[i] = bc_outflow;
    }
  }
  else {
    for (int i = 0; i < 2*n_dim; i++) {
      if ( strcmp(bcs[i],"outflow") == 0 ) {
        pars.bcs[i] = bc_outflow;
        printf("## i=%d, bc_outflow\n",i);
      }
      else if ( strcmp(bcs[i],"wall") == 0) {
        pars.bcs[i] = bc_wall;
        printf("## i=%d, bc_wall\n",i);
      }
      else if ( strcmp(bcs[i],"axis") == 0) {
        pars.bcs[i] = bc_axis;
        printf("## i=%d, bc_axis\n",i);
      }
      else {
        pars.bcs[i] = bc_unknown;
        printf("## i=%d, bc_unknown\n",i);
      }
    }
  }
#endif


  // read in reconstruction
  if (get_str_param(pfile, "reconstruction", bcs, 1) != 1) {
    pars.reconstruction = recon_type_mp5;
    printf("## Default MP5 reconstruction\n");
  }
  else {
    if ( strcmp(bcs[0], "Minmod") == 0 ) {
      pars.reconstruction = recon_type_minmod;
      printf("## Minmod reconstruction\n");
    }
    else if ( strcmp(bcs[0], "PPM") == 0 ) {
      pars.reconstruction = recon_type_ppm;
      printf("## PPM reconstruction\n");
    }
    else if ( strcmp(bcs[0], "MP5") == 0 ) {
      pars.reconstruction = recon_type_mp5;
      printf("## MP5 reconstruction\n");
    }
    else if ( strcmp(bcs[0], "GRHMP5") == 0 ) {
      pars.reconstruction = recon_type_grhmp5;
      printf("## GRHMP5 reconstruction\n");
    }
    else if ( strcmp(bcs[0], "WENO5") == 0 ) {
      pars.reconstruction = recon_type_weno5;
      printf("## WENO5 reconstruction\n");
    }
    else {
      printf("Unknown reconstruction type %s. Die here.\n",bcs[0]);
      exit(2);
    }
  }

  for (int i = 0; i < 2*n_dim; i++) {
    free(bcs[i]);
  }
  free(bcs);

  // output fields
  ok *= read_int_param(pfile, "output_by_level", &pars.output_by_level, 0, 0, 1);
  ok *= read_int_param(pfile, "output_frequency", &pars.output_frequency,
                       0, 0, 1000000);
  ok *= read_int_param(pfile, "print_frequency", &pars.print_frequency,
                       1, 0, 1000000);

  // output fields
  // find out how many fields for which output is requested
  FILE *fp;
  char *line;
  fp = fopen(pfile,"r");
  int count = 0;
  if ( fp ) {
    while ( (line = read_line(fp))) {
      if ( strstr(line,"output_functions") ) {
        // count how many fields listed here
        const char *tmp = line;
        while( (tmp=strstr(tmp,"\"")) ) {
          count++;
          tmp++;
        }
      }
    }
    fclose(fp);
  }
  int num_req_fields = count/2;
  pars.num_req_fields = num_req_fields;

  // output fields
  // initialize output off
  for (int i=0;i<n_variab+n_aux;i++) {
    pars.output_fields[i] = no_output;
  }

  char **output_fields;
  output_fields = (char **) malloc((num_req_fields)*sizeof(char *));
  for (int i = 0; i < num_req_fields ; i++) {
    output_fields[i] = (char *) malloc(32*sizeof(char));
  }

  if (get_str_param(pfile, "output_functions", output_fields, num_req_fields)!= 1) {
    for(int i = 0; i < n_variab+n_aux; i++) {
      pars.output_fields[i] = no_output;
    }
  }
  else {
    for (int i = 0; i < num_req_fields; i++) {
      if ( strcmp(output_fields[i],"rho") == 0 ) {
        pars.output_fields[V_RHO] = normal_output;
        printf(" rho output selected\n");
      }
      if ( strcmp(output_fields[i],"vx") == 0 ) {
        pars.output_fields[V_VX] = normal_output;
        printf(" vx output selected\n");
      }
      if ( strcmp(output_fields[i],"vy") == 0 ) {
        pars.output_fields[V_VY] = normal_output;
        printf(" vy output selected\n");
      }
      if ( strcmp(output_fields[i],"vz") == 0 ) {
        pars.output_fields[V_VZ] = normal_output;
        printf(" vz output selected\n");
      }
      if ( strcmp(output_fields[i],"p") == 0 ) {
        pars.output_fields[V_P] = normal_output;
        printf(" p output selected\n");
      }
      if ( strcmp(output_fields[i],"Bx") == 0 ) {
        pars.output_fields[V_BX] = normal_output;
        printf(" Bx output selected\n");
      }
      if ( strcmp(output_fields[i],"By") == 0 ) {
        pars.output_fields[V_BY] = normal_output;
        printf(" By output selected\n");
      }
      if ( strcmp(output_fields[i],"Bz") == 0 ) {
        pars.output_fields[V_BZ] = normal_output;
        printf(" Bz output selected\n");
      }
      if ( strcmp(output_fields[i],"psi") == 0 ) {
        pars.output_fields[V_PSI] = normal_output;
        printf(" psi output selected\n");
      }
      if ( strcmp(output_fields[i],"D") == 0 ) {
        pars.output_fields[U_D] = normal_output;
        printf(" D output selected\n");
      }
      if ( strcmp(output_fields[i],"Sx") == 0 ) {
        pars.output_fields[U_SX] = normal_output;
        printf(" Sx output selected\n");
      }
      if ( strcmp(output_fields[i],"Sy") == 0 ) {
        pars.output_fields[U_SY] = normal_output;
        printf(" Sy output selected\n");
      }
      if ( strcmp(output_fields[i],"Sz") == 0 ) {
        pars.output_fields[U_SZ] = normal_output;
        printf(" Sz output selected\n");
      }
      if ( strcmp(output_fields[i],"tau") == 0 ) {
        pars.output_fields[U_TAU] = normal_output;
        printf(" tau output selected\n");
      }
    }
  }

  for (int i = 0; i < num_req_fields; i++) {
    free(output_fields[i]);
  }
  free(output_fields);

  // initial data parameters
  ok *= read_int_param(pfile, "id_type", &pars.id_type, 1, 1, 20);

  ok *= read_int_param(pfile, "id_indep_coord", &pars.id_indep_coord, 0, 0, 2);

  ok *= read_real_param(pfile, "id_x0", &pars.id_x0, 0.0, -1.0e2,1.0e2);
  ok *= read_real_param(pfile, "id_y0", &pars.id_y0, 0.0, -1.0e2,1.0e2);
  ok *= read_real_param(pfile, "id_z0", &pars.id_z0, 0.0, -1.0e2,1.0e2);
  ok *= read_real_param(pfile, "id_sigma", &pars.id_sigma, 0.1, 1.0e-6,1.0e1);
  ok *= read_real_param(pfile, "id_amp", &pars.id_amp, 1.0, -1.0e-6,1.0e9);
  ok *= read_real_param(pfile, "id_kappa", &pars.id_kappa, 1.0, 1.0e-6, 1.0e9);
  ok *= read_real_param(pfile, "id_gauss_floor", &pars.id_gauss_floor,
                        0.1, 1.0e-15,1.0e1);
  ok *= read_real_param(pfile, "id_vx0", &pars.id_vx0, 0.0, -0.5,0.5);
  ok *= read_real_param(pfile, "id_vy0", &pars.id_vy0, 0.0, -0.5,0.5);
  ok *= read_real_param(pfile, "id_vz0", &pars.id_vz0, 0.0, -0.5,0.5);
  ok *= read_real_param(pfile, "id_Bx0", &pars.id_Bx0, 0.0, -1.0e2,1.0e2);
  ok *= read_real_param(pfile, "id_By0", &pars.id_By0, 0.0, -1.0e2,1.0e2);
  ok *= read_real_param(pfile, "id_Bz0", &pars.id_Bz0, 0.0, -1.0e2,1.0e2);

  ok *= read_real_param(pfile, "id_rho_left", &pars.id_rho_left,1.0,small,huge);
  ok *= read_real_param(pfile, "id_vx_left", &pars.id_vx_left, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vy_left", &pars.id_vy_left, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vz_left", &pars.id_vz_left, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_P_left", &pars.id_P_left, 1.0,small,huge);
  ok *= read_real_param(pfile, "id_Bx_left", &pars.id_Bx_left, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_By_left", &pars.id_By_left, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_Bz_left", &pars.id_Bz_left, 1.0, -huge,huge);

  ok *= read_real_param(pfile,"id_rho_right",&pars.id_rho_right,1.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_right",&pars.id_vx_right,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_right",&pars.id_vy_right,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_right",&pars.id_vz_right,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_right",&pars.id_P_right,1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_right",&pars.id_Bx_right,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_By_right",&pars.id_By_right,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_Bz_right",&pars.id_Bz_right,1.0,-huge,huge);

  ok *= read_real_param(pfile,"id_rho_left2",&pars.id_rho_left2,1.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_left2",&pars.id_vx_left2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_left2",&pars.id_vy_left2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_left2",&pars.id_vz_left2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_left2",&pars.id_P_left2, 1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_left2",&pars.id_Bx_left2, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_By_left2",&pars.id_By_left2, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_Bz_left2",&pars.id_Bz_left2, 1.0, -huge,huge);

  ok *= read_real_param(pfile, "id_rho_mid2",&pars.id_rho_mid2,1.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_mid2",&pars.id_vx_mid2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_mid2",&pars.id_vy_mid2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_mid2",&pars.id_vz_mid2, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_mid2",&pars.id_P_mid2, 1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_mid2",&pars.id_Bx_mid2, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_By_mid2",&pars.id_By_mid2, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_Bz_mid2",&pars.id_Bz_mid2, 1.0, -huge,huge);

  ok *= read_real_param(pfile,"id_rho_right2",&pars.id_rho_right2,1.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_right2",&pars.id_vx_right2,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_right2",&pars.id_vy_right2,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_right2",&pars.id_vz_right2,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_right2",&pars.id_P_right2, 1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_right2",&pars.id_Bx_right2,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_By_right2",&pars.id_By_right2,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_Bz_right2",&pars.id_Bz_right2,1.0,-huge,huge);

  ok *= read_real_param(pfile,"id_middle_radius",&pars.id_middle_radius,1.0,-huge,huge);

  ok *= read_real_param(pfile,"id_rho0",&pars.id_rho0, 0.001, 0.0, huge);
  ok *= read_int_param(pfile, "id_k", &pars.id_k, 1, 0, 2);

  ok *= read_real_param(pfile,"id_rho_inner",&pars.id_rho_inner,0.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_inner",&pars.id_vx_inner,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_inner",&pars.id_vy_inner,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_inner",&pars.id_vz_inner,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_ang_vel",&pars.id_ang_vel,0.0,-10.0,10.0);
  ok *= read_real_param(pfile,"id_P_inner",&pars.id_P_inner, 0.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_inner",&pars.id_Bx_inner,0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_By_inner",&pars.id_By_inner,0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_Bz_inner",&pars.id_Bz_inner,0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_inner_radius",&pars.id_inner_radius,0.1,0.0,1.0);

  ok *= read_real_param(pfile, "id_rho_outer",&pars.id_rho_outer,0.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_outer",&pars.id_vx_outer,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_outer",&pars.id_vy_outer,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_outer",&pars.id_vz_outer,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_outer" ,&pars.id_P_outer ,0.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_outer",&pars.id_Bx_outer,0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_By_outer",&pars.id_By_outer,0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_Bz_outer",&pars.id_Bz_outer,0.0,-huge,huge);

  ok *= read_real_param(pfile,"divB_damping",&pars.divB_damping,0.0,-10.0,10.0);

  ok *= read_real_param(pfile, "id_rho_UR", &pars.id_rho_UR,1.0,small,huge);
  ok *= read_real_param(pfile, "id_vx_UR", &pars.id_vx_UR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vy_UR", &pars.id_vy_UR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vz_UR", &pars.id_vz_UR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_P_UR", &pars.id_P_UR, 1.0,small,huge);
  ok *= read_real_param(pfile, "id_Bx_UR", &pars.id_Bx_UR, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_By_UR", &pars.id_By_UR, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_Bz_UR", &pars.id_Bz_UR, 1.0, -huge,huge);

  ok *= read_real_param(pfile, "id_rho_UL", &pars.id_rho_UL,1.0,small,huge);
  ok *= read_real_param(pfile, "id_vx_UL", &pars.id_vx_UL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vy_UL", &pars.id_vy_UL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vz_UL", &pars.id_vz_UL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_P_UL", &pars.id_P_UL, 1.0,small,huge);
  ok *= read_real_param(pfile, "id_Bx_UL", &pars.id_Bx_UL, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_By_UL", &pars.id_By_UL, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_Bz_UL", &pars.id_Bz_UL, 1.0, -huge,huge);

  ok *= read_real_param(pfile, "id_rho_LL", &pars.id_rho_LL,1.0,small,huge);
  ok *= read_real_param(pfile, "id_vx_LL", &pars.id_vx_LL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vy_LL", &pars.id_vy_LL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vz_LL", &pars.id_vz_LL, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_P_LL", &pars.id_P_LL, 1.0,small,huge);
  ok *= read_real_param(pfile, "id_Bx_LL", &pars.id_Bx_LL, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_By_LL", &pars.id_By_LL, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_Bz_LL", &pars.id_Bz_LL, 1.0, -huge,huge);

  ok *= read_real_param(pfile, "id_rho_LR", &pars.id_rho_LR,1.0,small,huge);
  ok *= read_real_param(pfile, "id_vx_LR", &pars.id_vx_LR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vy_LR", &pars.id_vy_LR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_vz_LR", &pars.id_vz_LR, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile, "id_P_LR", &pars.id_P_LR, 1.0,small,huge);
  ok *= read_real_param(pfile, "id_Bx_LR", &pars.id_Bx_LR, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_By_LR", &pars.id_By_LR, 1.0, -huge,huge);
  ok *= read_real_param(pfile, "id_Bz_LR", &pars.id_Bz_LR, 1.0, -huge,huge);

  ok *= read_real_param(pfile,"id_rho_innkh",&pars.id_rho_innkh,1.0,small,huge);
  ok *= read_real_param(pfile,"id_vx_innkh",&pars.id_vx_innkh, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_innkh",&pars.id_vy_innkh, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_innkh",&pars.id_vz_innkh, 0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_innkh", &pars.id_P_innkh, 1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_innkh",&pars.id_Bx_innkh, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_By_innkh",&pars.id_By_innkh, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_Bz_innkh",&pars.id_Bz_innkh, 1.0, -huge,huge);
  ok *= read_real_param(pfile,"id_rho_innkh",&pars.id_rho_innkh,1.0,small,huge);

  ok *= read_real_param(pfile,"id_vx_outkh",&pars.id_vx_outkh,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vy_outkh",&pars.id_vy_outkh,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_vz_outkh",&pars.id_vz_outkh,0.0,-1.0,1.0);
  ok *= read_real_param(pfile,"id_P_outkh", &pars.id_P_outkh, 1.0,small,huge);
  ok *= read_real_param(pfile,"id_Bx_outkh",&pars.id_Bx_outkh,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_By_outkh",&pars.id_By_outkh,1.0,-huge,huge);
  ok *= read_real_param(pfile,"id_Bz_outkh",&pars.id_Bz_outkh,1.0,-huge,huge);

  ok *= read_real_param(pfile, "id_dufmac_Bamp", &pars.id_dufmac_Bamp, 0.1, 0.0,10.0);

  // Radice-Rezzolla Kelvin-Helmholtz Parameters
  ok *= read_real_param(pfile, "id_RadRez_rho0", &pars.id_RadRez_rho0, 0.505, 0.0, 1.0);
  ok *= read_real_param(pfile, "id_RadRez_rho1", &pars.id_RadRez_rho1, 0.495, 0.0, 1.0);
  ok *= read_real_param(pfile, "id_v_shear", &pars.id_v_shear,0.5,0.001,0.999);
  ok *= read_real_param(pfile, "id_A_0", &pars.id_A_0, 0.1, -1.0,1.0);
  ok *= read_real_param(pfile, "id_RadRez_sigma", &pars.id_RadRez_sigma, 0.1, 0.001,1.0);
  ok *= read_real_param(pfile, "id_shear_layer_width", &pars.id_shear_layer_width, 0.01, 0.001,0.999);
  ok *= read_real_param(pfile, "id_vz_RadRez_bot", &pars.id_vz_RadRez_bot, 0.0, -0.999,0.999);
  ok *= read_real_param(pfile, "id_P_RadRez_bot", &pars.id_P_RadRez_bot, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_Bx_RadRez_bot", &pars.id_Bx_RadRez_bot, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_By_RadRez_bot", &pars.id_By_RadRez_bot, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_Bz_RadRez_bot", &pars.id_Bz_RadRez_bot, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_vz_RadRez_top", &pars.id_vz_RadRez_top, 0.0, -0.999,0.999);
  ok *= read_real_param(pfile, "id_P_RadRez_top", &pars.id_P_RadRez_top, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_Bx_RadRez_top", &pars.id_Bx_RadRez_top, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_By_RadRez_top", &pars.id_By_RadRez_top, 0.0, 0.0,10.0);
  ok *= read_real_param(pfile, "id_Bz_RadRez_top", &pars.id_Bz_RadRez_top, 0.0, 0.0,10.0);

  // weak field parameters 
  ok *= read_real_param(pfile,"id_wf_rhoamp",&pars.id_wf_rhoamp,
                        0.505, small, huge);
  ok *= read_real_param(pfile, "id_wf_vamp", &pars.id_wf_vamp, 0.5, -1.0, 1.0);
  ok *= read_real_param(pfile, "id_wf_Bamp", &pars.id_wf_Bamp, 0.0,-huge,huge);
  ok *= read_real_param(pfile,"id_wf_deltarho",&pars.id_wf_deltarho,
                        0.495, small, huge);
  ok *= read_real_param(pfile, "id_wf_y0", &pars.id_wf_y0, 0.0, -huge, huge);


  return 1;
}

int read_int_param(char* pfile, char *name, int *var, const int def_val,
                   const int min_val, const int max_val)
{
  if (get_param(pfile, name, "long", 1, var) != 1) {
    *var = def_val;
  }
  if (*var > max_val) {
    printf("%s = %d is out of range. max(%s) = %d\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %d is out of range. min(%s) = %d\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}

int read_real_param(char* pfile, char *name, double *var, const double def_val,
                   const double min_val, const double max_val)
{
  if (get_param(pfile, name, "double", 1, var) != 1) {
    *var = def_val;
  }
  if (*var > max_val) {
    printf("%s = %g is out of range. max(%s) = %g\n",name,*var,name,max_val);
    return 0;
  }
  if (*var < min_val) {
    printf("%s = %g is out of range. min(%s) = %g\n",name,*var,name,min_val);
    return 0;
  }
  return 1;
}
#else
int default_params()
{
  // no par file read-in supported; hard code parameters
  pars.max_iter = 2000;
  pars.prune_frequency = 1;
  pars.epsilon = 1.e-3;

  pars.t0 = 0.0; // hard code the initial time.
  pars.tf = 2.0;
  pars.xmin = -1.0;
  pars.xmax = 1.0;
  pars.ymin = -1.0;
  pars.ymax = 1.0;
  pars.zmin = -1.0;
  pars.zmax = 1.0;
  pars.cfl = 0.2;

  pars.output_by_level = 0;
  pars.output_frequency = 10;
  pars.output_fields[V_P] = normal_output;
  pars.output_fields[V_RHO] = normal_output;
  pars.num_req_fields = 2;

  // relativistic fluid parameters
  pars.gamma = 1.333333333333333333;
  pars.reconstruct_Wv = 1;
  pars.vacuum = 1.e-10;
  pars.contoprimwarn = 1;
  pars.use_wave_speeds = 1;

  pars.id_type = 2;

  pars.id_amp   = 1.0;
  pars.id_x0    = 0.0;
  pars.id_y0    = 0.0;
  pars.id_z0    = 0.0;
  pars.id_sigma = 0.04;
  pars.id_kappa = 1000.0;
  pars.id_gauss_floor = 0.1;

  for(int i = 0; i < 2*n_dim; i++) {
    pars.bcs[i] = bc_outflow;
  }

  pars.id_rho_left = 1.0;
  pars.id_vx_left = 0.3;
  pars.id_vy_left = 0.0;
  pars.id_vz_left = 0.0;
  pars.id_P_left = 10.0;
  pars.id_Bx_left = 1.0;
  pars.id_By_left = 2.0;
  pars.id_Bz_left = 0.0;

  pars.id_rho_right = 1.0;
  pars.id_vx_right = -0.2;
  pars.id_vy_right = 0.0;
  pars.id_vz_right = 0.0;
  pars.id_P_right = 0.1;
  pars.id_Bx_right = 1.0;
  pars.id_By_right = 1.0;
  pars.id_Bz_right = 0.2;

  return 1;
}
#endif

#endif
