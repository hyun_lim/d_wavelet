#ifndef __CONSERVATIVE_H__
#define __CONSERVATIVE_H__


#include "hawaii.h"


//This routine will go over the points in the grid, and will sum the conserved
// fields. The end result will be a picture of the state of the conservation in
// the code.
void test_fluid_conservation(double tcurr, unsigned stamp);


void compute_point_contribution(double *vals, const coll_point_t *point);


bool open_cons_file(int freq, int restart_flag);


void close_cons_file();


#endif
