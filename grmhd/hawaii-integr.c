#include <stdlib.h>
#include <stdio.h>
#include "hawaii.h"
#include <math.h>
#ifdef RMHD
#include "hawaii-rmhd.h"
#endif

unsigned time_stamp;

#ifdef FORWARD_EULER
void apply_time_integrator(const double t, const double dt) {
  const int gen = 0;
  compute_rhs(t, gen);
  time_stamp++;

  FOR (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    forward_euler_helper(point, dt);
  }
  FOR (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *ptr = coll_points->array[i];
    while (ptr != NULL) {
      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
        forward_euler_helper(point, dt);
      }
      ptr = ptr->next;
    }
  }

  //TODO: Stage reset helper no longer exists. If you really want to so
  // FORWARD_EULER you will have to redo this part.
  //
  // As is, this will not compile.
  //
  // Actually, I think that the compute_rhs function will take care of this,
  // so we can safely omit this. NOTE: That is an untested assertion.
  /*
  for (int i = 0; i < npts_in_array; i++) {
    coll_point_t *point = &coll_points->array[i];
    if (point->level == 1 && point->status[CURRENT_STATUS] == essential)
      stage_reset_helper(point);
  }
  */
}

void forward_euler_helper(coll_point_t *point, const double dt) {
  const int gen = 0;
  double *u = point->u[gen];
  double *rhs = point->rhs[gen];
  for (int i = 0; i < n_variab ; i++)
    u[i+n_prims] += dt * rhs[i];

#ifdef RMHD
  point->c2p = con_to_prim(u, point->u[0], point->coords.pos);
#endif
  point->time_stamp++;
}
#endif

#ifdef RK4


void apply_time_integrator(const double t, const double dt)
{
  void(*rk4_helpers[4])(coll_point_t *, const double) =
    {rk4_helper1, rk4_helper2, rk4_helper3, rk4_helper4};

  const wavelet_trans_t type = primary_set_wavelet;

  for (int gen = 0; gen < n_gen; gen++) {
    //TODO: Be aware that this is commented out because we are not using the
    // wavelet derivative, and so the wavelet coefficients are not needed
    // until grid adaptation. If you are going to do something with the
    // wavelet coefficients in the rhs computation, then add these back.
    //
    //if (gen) {
    //  for (int i = 0; i < npts_in_array; i++) {
    //    coll_point_t *point = &coll_points->array[i];
    //    if (point->level == 1) {
    //      wavelet_trans(point, type, gen, primary_mask);
    //    }
    //  }
    //  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    //    hash_entry_t *ptr = coll_points->hash_table[i];
    //    while (ptr != NULL) {
    //      if (ptr->point->status[CURRENT_STATUS] > nonessential) {
    //        wavelet_trans(ptr->point, type, gen, primary_mask);
    //      }
    //      ptr = ptr->next;
    //    }
    //  }
    //}


    compute_rhs(t, gen);
    time_stamp++;

    void(*rk4_helper)(coll_point_t *, const double) = rk4_helpers[gen];

    FOR (int i = 0; i < npts_in_array; i++) {
      coll_point_t *point = &coll_points->array[i];
      rk4_helper(point, dt);
    }
    FOR (int i = 0; i < HASH_TBL_SIZE; ++i) {
      hash_entry_t *ptr = coll_points->hash_table[i];
      while (ptr != NULL) {
        if (ptr->point->status[CURRENT_STATUS] > nonessential) {
          rk4_helper(ptr->point, dt);
        }
        ptr = ptr->next;
      }
    }
  }

#ifdef CYL
  //loop over possible axial indices and apply c2p_axis if the point
  // exists
  int skip_at_max = skip_at_level(max_level);
  index_t c2pa_index;
  c2pa_index.idx[0] = 0;

  FOR (int idx = 0; idx <= max_index[1]; idx += skip_at_max) {
    c2pa_index.idx[1] = idx;
    coll_point_t *axis_point = get_coll_point(&c2pa_index);
    if (axis_point != NULL &&
                axis_point->status[CURRENT_STATUS] > nonessential) {
      con_to_prim_axis(axis_point, 0);
    }
  }
#endif
}

void rk4_helper1(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *u_next = point->u[1];
  double *rhs = point->rhs[0];
  for (int i = 0; i < n_variab; i++)
    u_next[i+n_prims] = u_curr[i+n_prims] + 0.5 * dt * rhs[i];
  point->time_stamp++;
#ifdef RMHD
#ifdef CYL
  // don't call con_to_prim on axis
  if (point->index.idx[0] != 0) {
    point->c2p = con_to_prim(u_next, point->u[0], point->coords.pos);
  }
#else
  point->c2p = con_to_prim(u_next, point->u[0], point->coords.pos);
#endif
#endif
}

void rk4_helper2(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *u_next = point->u[2];
  double *rhs = point->rhs[1];
  for (int i = 0; i < n_variab; i++)
    u_next[i+n_prims] = u_curr[i+n_prims] + 0.5 * dt * rhs[i];
  point->time_stamp++;
#ifdef RMHD
#ifdef CYL
  // don't call con_to_prim on axis
  if (point->index.idx[0] != 0) {
    point->c2p = con_to_prim(u_next, point->u[1], point->coords.pos);
  }
#else
  point->c2p = con_to_prim(u_next, point->u[1], point->coords.pos);
#endif
#endif
}

void rk4_helper3(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *u_next = point->u[3];
  double *rhs = point->rhs[2];
  for (int i = 0; i < n_variab; i++)
    u_next[i+n_prims] = u_curr[i+n_prims] + dt * rhs[i];
  point->time_stamp++;
#ifdef RMHD
#ifdef CYL
  // don't call con_to_prim on axis
  if (point->index.idx[0] != 0) {
    point->c2p = con_to_prim(u_next, point->u[2], point->coords.pos);
  }
#else
  point->c2p = con_to_prim(u_next, point->u[2], point->coords.pos);
#endif
#endif
}

void rk4_helper4(coll_point_t *point, const double dt) {
  double *u_curr = point->u[0];
  double *k1 = point->rhs[0];
  double *k2 = point->rhs[1];
  double *k3 = point->rhs[2];
  double *k4 = point->rhs[3];

  for (int i = 0; i < n_variab; i++)
    u_curr[i+n_prims] += dt * (k1[i] + 2.0 * k2[i] + 2.0 * k3[i] + k4[i]) / 6.0;

  point->time_stamp++;
#ifdef RMHD
#ifdef CYL
  // don't call con_to_prim on axis
  if (point->index.idx[0] != 0) {
    point->c2p = con_to_prim(u_curr, point->u[3], point->coords.pos);
  }
#else
/*
  double u1[n_variab+n_aux];
  double u2[n_variab+n_aux];
  double du[n_variab+n_aux];
  for (int m = 0; m < n_variab+n_aux; m++) {
    u1[m] = u_curr[m];
  }

*/
  point->c2p = con_to_prim(u_curr, point->u[3], point->coords.pos);
/*
  for (int m = 0; m < n_prims + n_aux; m++) {
    u2[m] = u_curr[m];
  }
  prim_to_con(u2, point->coords.pos);

  for (int m = n_prims; m < n_prims + n_cons; m++) {
    du[m] = u2[m] - u1[m];
    if (fabs(du[m]) > 1.0e-6) {
      printf("*** u1[%d] = %g, u2[%d] = %g, du = %g\n",m,u1[m],m,u2[m],du[m]);
    }
  }
  if (u_curr[V_VX] > 0.9) {
    printf("*** bad velocity v=%g at %d\n",u_curr[V_VX],point->index.idx[0]);
  }
*/
#endif
#endif
}
#endif
