// =============================================================================
//  points2mesh
//
//  Copyright (c) 2015, Trustees of Indiana University,
//  All rights reserved.
//
//  This software may be modified and distributed under the terms of the BSD
//  license.  See the LICENSE file for details.
//
//  This software was created at the Indiana University Center for Research in
//  Extreme Scale Technologies (CREST).
//
//  Authors:
//    Jackson DeBuhr, Indiana University <jdebuhr [at] indiana.edu>
// =============================================================================


/// @file 
/// This is a utility to take the output of the hawaii code and add points to
/// the grid so that it will be easy to display the data as a set of quads.
///
/// There are going to be some restrictions on the use of this utility. The
/// utility will need to have the same set of compile-time constants as the
/// hawaii code itself. See points2mesh.h for details about these. Also, we
/// are only going to set this up with a four point stencil, as that is what
/// we are using. If you need more, there will be a signifcant update needed
/// for this utility.
///
/// The utility is easy to use. Just provide the input file as the first 
/// argument, and the output file name as the second argument.


#include <stdio.h>

#include "points2mesh.h"


int file_mask[n_variab + n_aux];


int main(int argc, char **argv) {
  //test usage
  if (argc < 3) {
    fprintf(stderr, "Usage: %s <input file> <output file>\n", argv[0]);
    return -1;
  }
  
  storage_init();
  
  if (create_grid_from_file(argv[1])) {
    fprintf(stderr, "Error while creating grid from input file.\n");
    return -1;
  }
  
  if (create_extra_points()) {
    fprintf(stderr, "Error write adding points to the grid.\n");
    return -1;
  }
  
  if (write_grid_to_file(argv[2])) {
    fprintf(stderr, "Error writing extended grid to file\n");
    return -1;
  }
  
  storage_cleanup();
  
  //done
  return 0;
}


int create_grid_from_file(char *fname) {
  //TODO: Matt will implement this.
  
  //Basically you have to read in each point from the file, and then add that
  // point to the grid. You will need to compute the index from the location 
  // of the point.
  
  //Currently, the utility uses a collocation point type that only contains the
  // field values (no RHS, no derivs, no wavelet coefficients).
  
  //When you read in the file, we shall want to fill out the file_mask
  // variable with the fields that were actually in the file. This file_mask
  // will then serve as the mask for the interpolation and for indicating which
  // fields should be included in the output file.
  
  //You will also need to set L_dim in this routine as well. This will
  // be needed for some of the index computations. If this value is not in 
  // the output file, then we can change this to be a compile-time constant.
  //Same comment for x_min[]
}


int create_extra_points(void) {
}


int write_grid_to_file(char *fname) {
  //TODO: Matt will implement this
}


//////// GRID RELATED DEFINITIONS ETC... ////////


#if n_dim == 1
const index_t neighbor_offset[2] = { {{-1}}, {{1}} }; 
#elif n_dim == 2
const index_t neighbor_offset[8] = { 
  {{-1, -1}}, {{0, -1}}, {{1, -1}}, {{-1, 0}}, 
  {{1, 0}}, {{-1, 1}}, {{0, 1}}, {{1, 1}} }; 
#elif n_dim == 3
const index_t neighbor_offset[26] = 
  { {{-1, -1, -1}}, {{0, -1, -1}}, {{1, -1, -1}}, {{-1, 0, -1}}, {{0, 0, -1}}, 
    {{1, 0, -1}}, {{-1, 1, -1}}, {{0, 1, -1}}, {{1, 1, -1}}, {{-1, -1, 0}}, 
    {{0, -1, 0}}, {{1, -1, 0}}, {{-1, 0, 0}}, {{1, 0, 0}}, {{-1, 1, 0}},     
    {{0, 1, 0}}, {{1, 1, 0}}, {{-1, -1, 1}}, {{0, -1, 1}}, {{1, -1, 1}},
    {{-1, 0, 1}}, {{0, 0, 1}}, {{1, 0, 1}}, {{-1, 1, 1}}, {{0, 1, 1}}, 
    {{1, 1, 1}} }; 
#endif

const int base_step_size = 1 << JJ;
const int level_one_step_size = 1 << (JJ - 1); 
const int ns[3] = {ns_x, ns_y, ns_z}; 
const int max_index[3] = {ns_x * (1 << JJ), 
                          ns_y * (1 << JJ), 
                          ns_z * (1 << JJ)}; 
const int npts_in_array = (2 * ns_x + 1) * (2 * ns_y + 1) * (2 * ns_z + 1); 
hawaii_storage_t *coll_points = NULL; 
int max_level;


static inline uint64_t hash(const uint64_t k) {
  return (k % HASH_TBL_SIZE);
}


uint64_t split(const unsigned k) {
#if n_dim == 1
  uint64_t split = k;
#elif n_dim == 2
  uint64_t split = k & 0xffffffff;
  split = (split | split << 16) & 0xffff0000ffff;
  split = (split | split << 8) & 0xff00ff00ff00ff;
  split = (split | split << 4) & 0xf0f0f0f0f0f0f0f;
  split = (split | split << 2) & 0x3333333333333333;
  split = (split | split << 1) & 0x5555555555555555;
#elif n_dim == 3
  uint64_t split = k & 0x1fffff; 
  split = (split | split << 32) & 0x1f00000000ffff;
  split = (split | split << 16) & 0x1f0000ff0000ff;
  split = (split | split << 8)  & 0x100f00f00f00f00f;
  split = (split | split << 4)  & 0x10c30c30c30c30c3;
  split = (split | split << 2)  & 0x1249249249249249;
#endif
  return split;
}


uint64_t morton_key(const index_t *index) {
  uint64_t key = 0;
#if n_dim == 1
  key = index->idx[x_dir]; 
#elif n_dim == 2
  key |= split(index->idx[x_dir]) | split(index->idx[y_dir]) << 1;
#elif n_dim == 3
  key |= split(index->idx[x_dir]) | split(index->idx[y_dir]) << 1 | 
    split(index->idx[z_dir]) << 2;
#endif
  return key;
}


void storage_init(void) {
  coll_points = calloc(1, sizeof(hawaii_storage_t));
  assert(coll_points != NULL);
  coll_points->array = calloc(npts_in_array, sizeof(coll_point_t)); 
  assert(coll_points->array != NULL);
}


void storage_cleanup(void) {
  free(coll_points->array);
  for (int i = 0; i < HASH_TBL_SIZE; i++) {
    hash_entry_t *ptr = coll_points->hash_table[i]; 
    while (ptr != NULL) {
      hash_entry_t *tmp = ptr->next;
      free(ptr->point); 
      free(ptr);
      ptr = tmp; 
    }
  }
  free(coll_points);
}


coll_point_t *get_coll_point(const index_t *index) {
  coll_point_t *retval = NULL; 
  bool stored_in_array = true; 
  for (dir_t dir = x_dir; dir < n_dim; dir++) 
    stored_in_array &= (index->idx[dir] % level_one_step_size == 0); 

  if (stored_in_array) {
    retval = &coll_points->array[index_to_linear(index)]; 
  } else {
    uint64_t mkey = morton_key(index); 
    uint64_t hidx = hash(mkey); 
    hash_entry_t *curr = coll_points->hash_table[hidx]; 
    while (curr != NULL) {
      if (curr->mkey == mkey) {
        retval = curr->point; 
        break;
      }
      curr = curr->next;
    }
  }

  return retval;
}


coll_point_t *add_coll_point(const index_t *index, int *flag) {
  coll_point_t *retval = get_coll_point(index); 

  if (retval != NULL) {
    *flag = 1; // the point already exists
  } else {
    *flag = 0; // the point does not exist
    uint64_t mkey = morton_key(index); 
    uint64_t hidx = hash(mkey); 
    hash_entry_t *h_entry = calloc(1, sizeof(hash_entry_t)); 
    retval = calloc(1, sizeof(coll_point_t)); 
    assert(h_entry != NULL);
    assert(retval != NULL);
    h_entry->point = retval; 
    h_entry->mkey = mkey;
    h_entry->next = coll_points->hash_table[hidx]; 
    coll_points->hash_table[hidx] = h_entry;
  }

  return retval;
}



//////// WAVELET RELATED DEFINITIONS ////////


double scale = 1.0 / (1 << JJ); 
const int wavelet_offset[4] = {-3, -1, 1, 3}; 
const double wavelet_factor[4] = {-1.0 / 16.0, 9.0 / 16.0, 
                                    9.0 / 16.0, -1.0 / 16.0};


//NOTE: This assumes that the incoming index is just one off the grid at 
// whatever relevant level we are talking about. If this is not true, this will
// not work.
//
// Further, the 'offness' of the point will give the step size to find the 
// needed points in the grid.
void ext_func(const index_t *index, const int mask[n_variab + n_aux],
              const int gen, double var[n_variab + n_aux]) {
  //figure out the first out of bounds index direction
  int idir;
  int delta = 0;
  for (idir = x_dir; idir < n_dim; ++idir) {
    if (index->idx[idir] < 0) {
      delta = -index->idx[idir];
      break;
    } else if (index->idx[idir] > max_index[idir]) {
      delta = -(index->idx[idir] - max_index[idir]);  
      //delta is negative so we step into the grid 
      break;
    }
  }
  assert(idir < n_dim);
  
  //loop over the needed points, collecting their field values
  double vals[4][n_variab + n_aux] = {{0.0}};
  index_t stcl = *index;
  for (int loop = 0; loop < 4; ++loop) {
    //step to next point
    stcl.idx[idir] += delta;
    
    //get the value - from the grid or from extension
    if (check_index(&stcl)) {
      coll_point_t *stcl_point = get_coll_point(&stcl);
      memcpy(vals[loop], stcl_point->u[gen], 
                    sizeof(double) * (n_variab + n_aux));
    } else {
      ext_func(&stcl, mask, gen, vals[loop]);
    }
  }
  
  //now add them up
  for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
    if (mask[ivar]) {
      for (int loop = 0; loop < 4; ++loop) {
        var[ivar] = 4.0 * vals[0][ivar] - 6.0 * vals[1][ivar]
                    + 4.0 * vals[2][ivar] - vals[3][ivar];
      }
    }
  }
}


void wavelet_trans(coll_point_t *point, 
                   const int gen, const int mask[n_variab + n_aux]) {
  if (point->level == 0) {
    return;
  }

  int h = 1 << (JJ - point->level);      //spacing at the level of the point
  int h2 = 2 * h;                        //spacing at the next coarser level
  
  //how many directions should we interpolate?
  int interp_count = 0;
  int dirs[n_dim];
  for (int idir = 0; idir < n_dim; ++idir) {
    dirs[idir] = -1;
    if (point->index.idx[idir] % h2) {
      dirs[interp_count] = idir;
      ++interp_count;
    }
  }
  
  
  //work through the cases to compute approx
  double approx[n_variab + n_aux] = {0.0};
  double buffer[n_variab + n_aux] = {0.0};
  double *contrib;
  if (interp_count == 1) {
    for (int loopa = 0; loopa < 4; ++loopa) {
      //shift to correct location
      index_t stcl = point->index;
      stcl.idx[dirs[0]] += h * wavelet_offset[loopa];
      
      //get the point, or extension
      if (check_index(&stcl)) {
        coll_point_t *stcl_point = get_coll_point(&stcl);
        assert(stcl_point->level < point->level);
        contrib = stcl_point->u[gen];
      } else {
        ext_func(&stcl, mask, gen, buffer);
        contrib = buffer;
      }
      
      //contribute to approx
      for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
        if (mask[ivar]) {
          approx[ivar] += wavelet_factor[loopa] * contrib[ivar];
        }
      }
    }
  }
#if n_dim > 1 
  else if (interp_count == 2) {
    for (int loopa = 0; loopa < 4; ++loopa) {
      index_t stcla = point->index;
      stcla.idx[dirs[0]] += h * wavelet_offset[loopa];
      for (int loopb = 0; loopb < 4; ++loopb) {
        index_t stcl = stcla;
        stcl.idx[dirs[1]] += h * wavelet_offset[loopb];
        
        //get the point
        if (check_index(&stcl)) {
          coll_point_t *stcl_point = get_coll_point(&stcl);
          assert(stcl_point->level < point->level);
          contrib = stcl_point->u[gen];
        } else {
          ext_func(&stcl, mask, gen, buffer);
          contrib = buffer;
        }
        
        //contribute
        for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
          if (mask[ivar]) {
            approx[ivar] += wavelet_factor[loopa] 
                            * wavelet_factor[loopb] * contrib[ivar];
          }
        }
      }
    }
  } 
#endif
#if n_dim > 2
  else if (interp_count == 3) {
    for (int loopa = 0; loopa < 4; ++loopa) {
      index_t stcla = point->index;
      stcla.idx[dirs[0]] += h * wavelet_offset[loopa];
      for (int loopb = 0; loopb < 4; ++loopb) {
        index_t stclb = stcla;
        stclb.idx[dirs[1]] += h * wavelet_offset[loopb];
        for (int loopc = 0; loopc < 4; ++loopc) {
          index_t stcl = stclb;
          stcl.idx[dirs[2]] += h * wavelet_offset[loopc];
          
          //get the point
          if (check_index(&stcl)) {
            coll_point_t *stcl_point = get_coll_point(&stcl);
            assert(stcl_point->level < point->level);
            contrib = stcl_point->u[gen];
          } else {
            ext_func(&stcl, mask, gen, buffer);
            contrib = buffer;
          }
          
          //contribute
          for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
            if (mask[ivar]) {
              approx[ivar] += wavelet_factor[loopa] 
                            * wavelet_factor[loopb] 
                            * wavelet_factor[loopc] * contrib[ivar];
            }
          }
        }
      }
    }
  } 
#endif
  else {
    assert(0 && "Problem identifying interpolation directions for wavelet");
  }
  
  //set the point values based on the approximation
  for (int ivar = 0; ivar < n_variab + n_aux; ++ivar) {
    point->u[ivar] = approx[ivar];
  }
}


void check_wavelet_stencil(coll_point_t *point, const int gen) {
  // Step size of the point's refinement level 
  const int h = 1 << (JJ - point->level); 
  const int h2 = 2 * h;
  const int upper_limit = wavelet_offset[3]; 
  const int lower_limit = wavelet_offset[0];
  const int delta_limit = upper_limit - lower_limit;
  
  //how many directions should we interpolate?
  int interp_count = 0;
  int dirs[n_dim];
  for (int idir = 0; idir < n_dim; ++idir) {
    dirs[idir] = -1;
    if (point->index.idx[idir] % h2) {
      dirs[interp_count] = idir;
      ++interp_count;
    }
  }
  
  //get the starting index for each direction
  int starting[n_dim];
  for (int idir = 0; idir < interp_count; ++idir) {
    starting[idir] = point->index.idx[dirs[idir]] + h * wavelet_offset[0];
    if (starting[idir] < 0) {
      starting[idir] = 0;
    } else if ((starting[idir] + delta_limit * h) > max_index[dirs[idir]]) {
      starting[idir] = max_index[dirs[idir]] - delta_limit * h;
    }
  }
  
  //loop over stencil to check for existence
  if (interp_count == 1) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {  
      //get index
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      //check point
      int flag; 
      coll_point_t *temp = add_coll_point(&stcl, &flag); 
      if (!flag) {
        create_nonessential_point(temp, &stcl);
      } 
    }
  } 
#if n_dim > 1
  else if (interp_count == 2) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      for (int loopb = 0; loopb < 4; ++loopb) {
        stcl.idx[dirs[1]] = starting[1] + h2 * loopb;
        
        int flag; 
        coll_point_t *temp = add_coll_point(&stcl, &flag); 
        if (!flag) {
          create_nonessential_point(temp, &stcl);
        }
      }
    }
  } 
#endif
#if n_dim > 2
  else if (interp_count == 3) {
    index_t stcl = point->index;
    for (int loopa = 0; loopa < 4; ++loopa) {
      stcl.idx[dirs[0]] = starting[0] + h2 * loopa;
      for (int loopb = 0; loopb < 4; ++loopb) {
        stcl.idx[dirs[1]] = starting[1] + h2 * loopb;
        for (int loopc = 0; loopc < 4; ++loopc) {
          stcl.idx[dirs[2]] = starting[2] + h2 * loopc;
          
          int flag; 
          coll_point_t *temp = add_coll_point(&stcl, &flag); 
          if (!flag) {
            create_nonessential_point(temp, &stcl);
          } 
        }
      }
    }
  } 
#endif
  else {
    assert(0 && "Problem with checking wavelet stencils");
  }
}


//////// INDEX RELATED DEFINITIONS ////////


double L_dim[n_dim]; 
double x_min[n_dim];


index_t linear_to_index(const int rank) {
  index_t retval; 
#if n_dim == 1
  retval.idx[x_dir] = rank * level_one_step_size;
#elif n_dim == 2
  div_t temp = div(rank, 2 * ns_y + 1);
  retval.idx[x_dir] = temp.quot * level_one_step_size; 
  retval.idx[y_dir] = temp.rem * level_one_step_size; 
#elif n_dim == 3
  div_t temp = div(rank, (2 * ns_y + 1) * (2 * ns_z + 1)); 
  retval.idx[x_dir] = temp.quot * level_one_step_size; 
  temp = div(temp.rem, 2 * ns_z + 1);
  retval.idx[y_dir] = temp.quot * level_one_step_size; 
  retval.idx[z_dir] = temp.rem * level_one_step_size;
#endif
  return retval;
}


int index_to_linear(const index_t *index) {
  int rank; 
#if n_dim == 1
  rank = index->idx[x_dir] / level_one_step_size; 
#elif n_dim == 2
  int ix = index->idx[x_dir] / level_one_step_size; 
  int iy = index->idx[y_dir] / level_one_step_size; 
  rank = ix * (2 * ns_y + 1) + iy;
#elif n_dim == 3
  int ix = index->idx[x_dir] / level_one_step_size; 
  int iy = index->idx[y_dir] / level_one_step_size; 
  int iz = index->idx[z_dir] / level_one_step_size; 
  rank = ix * (2 * ns_y + 1) * (2 * ns_z + 1) + 
    iy * (2 * ns_z + 1) + iz; 
#endif 
  return rank; 
}


index_t add_index(const index_t *lhs, const index_t *rhs, const int scalar) {
  index_t retval; 
  for (dir_t dir = x_dir; dir < n_dim; dir++) 
    retval.idx[dir] = lhs->idx[dir] + rhs->idx[dir] * scalar; 
  return retval; 
}


bool check_index(const index_t *index) {
  bool retval = true; 
  for (dir_t dir = x_dir; dir < n_dim; dir++) 
    retval &= (index->idx[dir] >= 0) && (index->idx[dir] <= max_index[dir]);
  return retval; 
}


coord_t index_to_coordinate(const index_t *index) {
  coord_t coord; 

  coord.pos[x_dir] = L_dim[x_dir] * index->idx[x_dir] / max_index[x_dir] 
                   + pars.xmin; 
  
#if n_dim == 2 || n_dim == 3
  coord.pos[y_dir] = L_dim[y_dir] * index->idx[y_dir] / max_index[y_dir] 
                   + pars.ymin; 
#endif
#if n_dim == 3
  coord.pos[z_dir] = L_dim[z_dir] * index->idx[z_dir] / max_index[z_dir] 
                   + pars.zmin; 
#endif 

  return coord;
}


index_t coordinate_to_index(coord_t x) {
  index_t retval;
  
  retval.idx[x_dir] = 
            max_index[x_dir] * ((x.pos[x_dir] - x_min[x_dir]) / L_dim[x_dir]);
#if n_dim > 1
  retval.idx[y_dir] = 
            max_index[y_dir] * ((x.pos[y_dir] - x_min[y_dir]) / L_dim[y_dir]);
#endif
#if n_dim > 2
  retval.idx[z_dir] = 
            max_index[z_dir] * ((x.pos[z_dir] - x_min[z_dir]) / L_dim[z_dir]);
#endif

  return retval;
}


