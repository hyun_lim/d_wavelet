// =============================================================================
//  points2mesh
//
//  Copyright (c) 2015, Trustees of Indiana University,
//  All rights reserved.
//
//  This software may be modified and distributed under the terms of the BSD
//  license.  See the LICENSE file for details.
//
//  This software was created at the Indiana University Center for Research in
//  Extreme Scale Technologies (CREST).
//
//  Authors:
//    Jackson DeBuhr, Indiana University <jdebuhr [at] indiana.edu>
// =============================================================================


#define ns_x 10    // number of points in the x-direction of the base grid 
#define ns_y 10    // number of points in the y-direction of the base grid 
#define ns_z 0     // number of points in the z-direction of the base grid 
#define JJ   11    // maximum number of resolution levels to use 
#define n_dim 2    // dimensionality of the problem
#define n_variab 9 // number of primary variables 
#define n_aux 18   // number of auxiliary variables 

#define HASH_TBL_SIZE 1572869


typedef enum {
  x_dir = 0, 
  y_dir = 1, 
  z_dir = 2
} dir_t; 

typedef struct index_t {
  int idx[n_dim]; 
} index_t; 

typedef struct coord_t {
  double pos[n_dim]; 
} coord_t; 

typedef struct coll_point_t {
  double u[n_variab + n_aux];
  index_t index; 
  coord_t coords; 
  int level; 
} coll_point_t; 

typedef struct hash_entry_t{
  coll_point_t *point; 
  uint64_t mkey;  
  struct hash_entry_t *next; 
} hash_entry_t; 

typedef struct {
  coll_point_t *array; 
  hash_entry_t *hash_table[HASH_TBL_SIZE]; 
} hawaii_storage_t; 

typedef enum {
  extend_upper = 1, 
  extend_lower = 2
} ext_func_t; 


#if n_dim == 1
#define n_neighbors 2
#elif n_dim == 2
#define n_neighbors 8
#elif n_dim == 3
#define n_neighbors 26
#endif 
extern const index_t neighbor_offset[n_neighbors];
extern int max_level; 
extern const int max_index[3]; 
extern const int ns[3]; 
extern const int base_step_size; 
extern const int level_one_step_size; 
extern const int npts_in_array; 


///
///
///
extern int file_mask[n_variab + n_aux];


extern double L_dim[n_dim]; 
extern double x_min[n_dim];


//////// MAIN USER INTERFACE ////////

/// @brief Read in a file and create a grid based on that data
///
/// This will return a nonzero value on an error.
///
/// TODO: Matt will implement this.
int create_grid_from_file(char *fname);


/// @brief Add points to the grid to allow for quad output
///
/// This will return nonzero on error.
int create_extra_points(void);


/// @brief Write the current grid to file.
///
/// This will return a nonzero on an error.
///
/// TODO: Matt will implement this.
int write_grid_to_file(char *fname);


//////// GRID RELATED INTERFACE ////////

uint64_t morton_key(const index_t *index);
void storage_init(void);
void storage_cleanup(void);
coll_point_t *get_coll_point(const index_t *index);
coll_point_t *add_coll_point(const index_t *index, int *flag);


//////// WAVELET RELATED INTERFACE ////////

void wavelet_trans(coll_point_t *point, 
                   const int gen, const int mask[n_variab + n_aux]);
void ext_func(const index_t *index, 
              const int mask[n_variab + n_aux], const int gen, 
              double var[n_variab + n_aux]);
void check_wavelet_stencil(coll_point_t *point, const int gen);



//////// INDEX RELATED INTERFACE ////////

index_t linear_to_index(const int rank);
int index_to_linear(const index_t *index);
index_t add_index(const index_t *lhs, const index_t *rhs, const int scalar);
bool check_index(const index_t *index);
coord_t index_to_coordinate(const index_t *index);
index_t coordinate_to_index(coord_t x);


