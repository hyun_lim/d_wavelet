#ifndef HAWAII_RMHD_PARAM_H
#define HAWAII_RMHD_PARAM_H

#include "hawaii-rmhd.h"

typedef enum {
  bc_unknown = 0,
  bc_outflow = 1,
  bc_wall = 2,
  bc_axis = 3,
  bc_periodic = 4
} boundary_condition_t;

// this can be expanded to delineate types of output
typedef enum {
  no_output = 0,
  normal_output = 1
} output_condition_t;

typedef enum {
  recon_type_minmod = 0,
  recon_type_ppm = 1,
  recon_type_mp5 = 2,
  recon_type_grhmp5 = 3,
  recon_type_weno5 = 4
} recon_t;

typedef struct {
 /* simulation parameters */
  int max_iter; // max number of allowed iterations in time integrator
  double t0;    // initial time stamp of the simulation
  double tf;    // final time stamp of the simulation
  double xmin, ymin, zmin;
  double xmax, ymax, zmax;
  double cfl;

  int prune_frequency;

  double epsilon;

  int output_by_level;
  int output_frequency;
  int print_frequency;

 /* related to restart files */
  double time_between_restarts;

 /* related to general statistics */
  double max_runtime;
  int stats_output_frequency;

 /* related to conservation testing */
  int cons_test_frequency;

 /* internally used variables */
  int base_step_size;
  int max_index_x;
  int num_req_fields;

 /* fluid parameters */
  int contoprimwarn;
  int use_wave_speeds;
  int reconstruct_Wv;
  recon_t reconstruction;
  double gamma;
  double vacuum;
  int eos_solver;
  int densitized_vars;

  /* output */
  int output_fields[n_variab+n_aux];

 /* boundary conditions */
  boundary_condition_t bcs[6];

 /*----------------------------------------------------
  * initial data parameters
  *----------------------------------------------------*/
  int id_type;

  int id_indep_coord;

  // Gaussian initial data
  double id_x0;
  double id_y0;
  double id_z0;
  double id_x02;
  double id_y02;
  double id_z02;
  double id_sigma;
  double id_amp;
  double id_amp2;
  double id_kappa;
  double id_gauss_floor;
  double id_vx0;
  double id_vy0;
  double id_vz0;
  double id_Bx0;
  double id_By0;
  double id_Bz0;

  // Riemann problem
  double id_rho_left;
  double id_vx_left;
  double id_vy_left;
  double id_vz_left;
  double id_P_left;
  double id_Bx_left;
  double id_By_left;
  double id_Bz_left;

  double id_rho_right;
  double id_vx_right;
  double id_vy_right;
  double id_vz_right;
  double id_P_right;
  double id_Bx_right;
  double id_By_right;
  double id_Bz_right;

  // 2D Riemann problem
  double id_rho_UR;
  double id_vx_UR;
  double id_vy_UR;
  double id_vz_UR;
  double id_P_UR;
  double id_Bx_UR;
  double id_By_UR;
  double id_Bz_UR;

  double id_rho_UL;
  double id_vx_UL;
  double id_vy_UL;
  double id_vz_UL;
  double id_P_UL;
  double id_Bx_UL;
  double id_By_UL;
  double id_Bz_UL;

  double id_rho_LL;
  double id_vx_LL;
  double id_vy_LL;
  double id_vz_LL;
  double id_P_LL;
  double id_Bx_LL;
  double id_By_LL;
  double id_Bz_LL;

  double id_rho_LR;
  double id_vx_LR;
  double id_vy_LR;
  double id_vz_LR;
  double id_P_LR;
  double id_Bx_LR;
  double id_By_LR;
  double id_Bz_LR;

  // Riemann problem with three parts 
  double id_rho_left2;
  double id_vx_left2;
  double id_vy_left2;
  double id_vz_left2;
  double id_P_left2;
  double id_Bx_left2;
  double id_By_left2;
  double id_Bz_left2;

  double id_rho_mid2;
  double id_vx_mid2;
  double id_vy_mid2;
  double id_vz_mid2;
  double id_P_mid2;
  double id_Bx_mid2;
  double id_By_mid2;
  double id_Bz_mid2;

  double id_rho_right2;
  double id_vx_right2;
  double id_vy_right2;
  double id_vz_right2;
  double id_P_right2;
  double id_Bx_right2;
  double id_By_right2;
  double id_Bz_right2;

  double id_middle_radius;

  // cylindrical blast wave and relativistic rotor 
  double id_rho_inner;
  double id_vx_inner;
  double id_vy_inner;
  double id_vz_inner;
  double id_P_inner;
  double id_Bx_inner;
  double id_By_inner;
  double id_Bz_inner;
  double id_inner_radius;
 double id_ang_vel;

  double id_rho_outer;
  double id_vx_outer;
  double id_vy_outer;
  double id_vz_outer;
  double id_P_outer;
  double id_Bx_outer;
  double id_By_outer;
  double id_Bz_outer;

  double divB_damping ;


  //kelvin helmholtz 
  double id_rho_innkh;
  double id_vx_innkh;
  double id_vy_innkh;
  double id_vz_innkh;
  double id_P_innkh;
  double id_Bx_innkh;
  double id_By_innkh;
  double id_Bz_innkh;

  double id_rho_outkh;
  double id_vx_outkh;
  double id_vy_outkh;
  double id_vz_outkh;
  double id_P_outkh;
  double id_Bx_outkh;
  double id_By_outkh;
  double id_Bz_outkh;

  //kelvin helmholtz ala Radice and Rezzolla 
  double id_RadRez_rho0 ;
  double id_RadRez_rho1 ;
  double id_shear_layer_width ;
  double id_v_shear ;
  double id_A_0 ;
  double id_RadRez_sigma ;
  double id_vz_RadRez_top ;
  double id_P_RadRez_top ;
  double id_Bx_RadRez_top ;
  double id_By_RadRez_top ;
  double id_Bz_RadRez_top ;
  double id_vz_RadRez_bot ;
  double id_P_RadRez_bot ;
  double id_Bx_RadRez_bot ;
  double id_By_RadRez_bot ;
  double id_Bz_RadRez_bot ;

  // Weak field initial data from had
  double id_wf_rhoamp;
  double id_wf_vamp;
  double id_wf_Bamp;
  double id_wf_deltarho;
  double id_wf_y0;

  // Duffel-MacFadyen 2012 TESS paper
  int id_k;
  double id_rho0;

  double id_dufmac_Bamp;
} Pars;

extern Pars pars;

int read_param_file(char *pfile);
int default_params();
int read_int_param(char* pfile, char *name, int *var, const int def_val,
                   const int min_val, const int max_val);
int read_real_param(char* pfile, char *name, double *var, const double def_val,
                   const double min_val, const double max_val);


#endif
