#ifndef HAWAII_TYPES_H
#define HAWAII_TYPES_H

#include <stdbool.h>
#include <stdint.h>
#include "hawaii-param.h"

#ifdef CILK
#include "libsync/locks.h"
#include "libsync/sync.h"
#endif

typedef enum {
  essential    = 3,
  neighboring  = 2,
  nonessential = 1,
  uninitialized = 0
} coll_status_t;

typedef enum {
  x_dir = 0,
  y_dir = 1,
  z_dir = 2
} dir_t;

typedef struct index_t {
  int idx[n_dim];
} index_t;

typedef struct coord_t {
  double pos[n_dim];
} coord_t;


typedef struct coll_point_t {
  double u[n_gen][n_variab + n_aux];
  double du[n_gen][n_deriv];
  double rhs[n_gen][n_rhs];
#ifdef XRHS
  double wavelet[n_gen][n_variab + n_aux + n_rhs];
#else
  double wavelet[n_gen][n_variab + n_aux];
#endif
  index_t index;
  coord_t coords;
  int level;
  coll_status_t status[2];

  unsigned time_stamp; // It tracks the number of times right-hand side has been
                       // computed. The value is incremented by n_gen each time
                       // the integrator is applied.
#ifdef RMHD
  int c2p;
#endif
} coll_point_t;

#define CURRENT_STATUS 0
#define FUTURE_STATUS 1

#if n_dim == 1
#define n_neighbors 2
#elif n_dim == 2
#define n_neighbors 8
#elif n_dim == 3
#define n_neighbors 26
#endif
extern const index_t neighbor_offset[n_neighbors];

#endif
