#include "restart.h"

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "hawaii-storage.h"
#include "hawaii-types.h"
#include "hawaii-utils.h"


typedef struct {
  double current_time;
  unsigned time_stamp;
  time_stats_t ts;
  int next_output_file_number;
  int number_of_points;
} restart_header_t;


typedef struct {
  double u[n_variab + n_aux];         //the gen zero data
  double wavelet[n_variab + n_aux];
  index_t index;
  coll_status_t status;
} restart_point_t;


void to_restart_header(double t_curr,
                       unsigned time_stamp,
                       time_stats_t *ts,
                       int snap, restart_header_t *head) {
  head->current_time = t_curr;
  head->time_stamp = time_stamp;
  head->ts = *ts;
  head->next_output_file_number = snap;
  head->number_of_points = count_points(nonessential, time_stamp);
}


void from_restart_header(restart_header_t *head,
                         double *t_curr,
                         unsigned *time_stamp,
                         time_stats_t *ts,
                         int *snap,
                         int *numpoints) {
  *t_curr = head->current_time;
  *time_stamp = head->time_stamp;
  *ts = head->ts;
  *snap = head->next_output_file_number;
  *numpoints = head->number_of_points;
}


void to_restart_point(coll_point_t *source, restart_point_t *dest) {
  memcpy(dest->u, source->u[0], sizeof(double) * (n_variab + n_aux));
  memcpy(dest->wavelet, source->wavelet[0],
                                sizeof(double) * (n_variab + n_aux));
  dest->index = source->index;
  dest->status = source->status[CURRENT_STATUS];
}


void from_restart_point(restart_point_t *source,
                        unsigned stamp, coll_point_t *dest) {
  memset(dest, 0, sizeof(*dest));
  memcpy(dest->u[0], source->u, sizeof(double) * (n_variab + n_aux));
  memcpy(dest->wavelet[0], source->wavelet,
                                sizeof(double) * (n_variab + n_aux));
  dest->index = source->index;
  dest->coords = set_coordinate(&dest->index);
  dest->level = get_level(&dest->index);
  dest->status[CURRENT_STATUS] = source->status;
  dest->status[FUTURE_STATUS] = uninitialized;
  dest->time_stamp = stamp;
}


int write_restart_file(double t_curr, unsigned time_stamp,
                        time_stats_t *ts, int snap) {
  //make a backup of the current restart file, if it exists
  FILE *test_file = fopen("rmhd_restart.dat","rb");
  if (test_file != NULL) {
    fclose(test_file);
    if (rename("rmhd_restart.dat", "rmhd_restart.dat.back")) {
      perror("Error backing up restart file");
      return 1;
    }
  }

  //create the new file
  FILE *restart = fopen("rmhd_restart.dat","wb");
  if (restart == NULL) {
    perror("Error during restart file creation");
    return 2;
  }

  //create the header
  restart_header_t head;
  to_restart_header(t_curr, time_stamp, ts, snap, &head);
  if (1 != fwrite(&head, sizeof(head), 1, restart)) {
    perror("Error writing restart file");
    fclose(restart);
    return 3;
  }

  //loop over the points and put them out
  restart_point_t rpoint;
  for (int i = 0; i < npts_in_array; ++i) {
    to_restart_point(&coll_points->array[i], &rpoint);
    if (1 != fwrite(&rpoint, sizeof(rpoint), 1, restart)) {
      perror("Error writing restart file");
      fclose(restart);
      return 4;
    }
  }
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *curr = coll_points->hash_table[i];
    while (curr != NULL) {
      if (curr->point->time_stamp == time_stamp &&
                curr->point->status[CURRENT_STATUS] >= nonessential) {
        to_restart_point(curr->point, &rpoint);
        if (1 != fwrite(&rpoint, sizeof(rpoint), 1, restart)) {
          perror("Error writing restart file");
          fclose(restart);
          return 5;
        }
      }
      curr = curr->next;
    }
  }

  fclose(restart);

  return 0;
}


int read_restart_file(double *t_curr, unsigned *time_stamp,
                        time_stats_t *ts, int *snap) {
  fprintf(stdout, "Reading in rmhd_restart.dat...\n");
  fflush(stdout);

  FILE *restart = fopen("rmhd_restart.dat", "rb");
  if (restart == NULL) {
    perror("Error reading restart file");
    return 1;
  }

  //read in the header
  restart_header_t head;
  if (1 != fread(&head, sizeof(head), 1, restart)) {
    perror("Error reading restart file header");
    fclose(restart);
    return 2;
  }

  //take action based on the header
  int numpoints;
  from_restart_header(&head, t_curr, time_stamp, ts, snap, &numpoints);

  //read in the points
  restart_point_t rpoint;
  int flag;
  for (int i = 0; i < numpoints; ++i) {
    if (1 != fread(&rpoint, sizeof(rpoint), 1, restart)) {
      perror("Error reading points in restart file");
      fclose(restart);
      return 3;
    }

    //put the point where it goes...
    //the first npts_in_array points go in the array
    if (i < npts_in_array) {
      from_restart_point(&rpoint, *time_stamp, &coll_points->array[i]);
    } else {
      coll_point_t *point = add_coll_point(&rpoint.index, &flag);
      assert(flag == 0);
      from_restart_point(&rpoint, *time_stamp, point);
      max_level = (max_level > point->level ? max_level : point->level);
    }
  }

  fclose(restart);

  return 0;
}


void complete_wavelet_stencils() {
  //TODO: when putting out also the nonessential points, this should not
  // really do anything... is there a way to test this?
  //the level 1 points will have complete stencils by default

  //loop over hash table and complete each
  const int gen = 0;
  for (int i = 0; i < HASH_TBL_SIZE; ++i) {
    hash_entry_t *curr = coll_points->hash_table[i];
    while (curr != NULL) {
      check_wavelet_stencil(curr->point, gen);
      curr = curr->next;
    }
  }
}
